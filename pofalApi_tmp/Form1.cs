﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml;
using System.Web;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pofalApi_tmp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string key = "5700bd3294a875944eb1365d585e0898";
            string title = textBox1.Text;
            string queryurl = string.Format("https://dapi.kakao.com/v3/search/book" +
                "?sort=accuracy" +
                "&page=1" +
                "&size=10" +
                "&target=title" +
                "&query={0}" + 
                "Authorization:{1}", HttpUtility.UrlEncode(title), key);

            XmlDocument xdoc = new XmlDocument();   // Xml 문서 개체 생성
            xdoc.PreserveWhitespace = true;         // 원본의 공백 유지
            xdoc.Load(queryurl);                    // Xml 문서 개체에 사이트 로딩
            richTextBox1.Text = xdoc.InnerXml;
        }
    }
}
