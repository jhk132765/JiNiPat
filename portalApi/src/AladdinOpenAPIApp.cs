using System;
using System.Text;
using AladdinOpenAPI;

namespace AladdinOpenAPIApp
{
    public class AladdinOpenAPIApp
    {
        static void Main(string[] args)
        {
            if(args==null || args.Length!=2)
            {
                Console.WriteLine("AladdinOpenAPIApp.exe [ttbkey] [도서 검색어]\n위의 형식으로 입력해주세요");
                return;
            }

            try
            {
                AladdinService service = new AladdinService(args[0]);
                ItemSearchRequest request = service.CreateItemSearchRequest();
                request.Query = args[1];
                request.SearchTarget = ItemSearchTarget.Book;
                ItemResponse response = request.GetItemResponse();
                if (response==null || response.TotalResults==0)
                {
                    Console.WriteLine("결과가 없습니다.");
                    return;
                }
                Console.WriteLine("### 총 {0}건의 검색결과 ###", response.TotalResults);
                foreach (Item item in response.Items)
                {
                    Console.WriteLine("* {0}\n{1}\n", item.Title, item.Link);
                }
            }
            catch(OpenAPIException apiEx)
            {
                Console.WriteLine("### Error가 발생하였습니다.\nMessage: {0}", apiEx.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine("### !예상치 못한 Error가 발생하였습니다.\nMessage: {0}", ex.Message);
            }
        }
    }
}
