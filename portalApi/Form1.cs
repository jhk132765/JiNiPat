﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace portalApi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] cb_list = { "책 제목 + 저자", "책 제목", "저자", "출판사" };
            comboBox1.Items.AddRange(cb_list);
            comboBox1.SelectedIndex = 0;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
            string type = string.Empty;
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    type = "Keyword"; break;
                case 1:
                    type = "Title"; break;
                case 2:
                    type = "Author"; break;
                case 3:
                    type = "Publisher"; break;
                default:
                    break;
            }
            // 쿼리 생성
            string key = "ttbgloriabook1512001";
            string title = textBox1.Text;
            string site = "http://www.aladin.co.kr/ttb/api/ItemSearch.aspx";
            string query = string.Format("{0}?query={1}&TTBKey={2}&output=xml&querytype={3}&MaxResults={4}", 
                site, WebUtility.UrlEncode(title), key, "Keyword", 30.ToString());

            // 쿼리를 입력인자로 WebRequest 개채 생성
            WebRequest request = WebRequest.Create(query);

            // WebRequest개체의 헤더에 인증키 포함시키기.
            // request.Headers.Add("Authorization", header);

            // WebResponse개체를 통해 서비스 요청.
            WebResponse response = request.GetResponse();

            // 결과문자열 확인
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            String xml = reader.ReadToEnd();
            stream.Close();
            // xml형식을 json형식으로 변환
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            var json = JsonConvert.SerializeXmlNode(doc);
            // richTextBox1.Text = json;

            // json형식 분석을 위해 JavaScriptSerializer 개체 생성
            JavaScriptSerializer js = new JavaScriptSerializer();

            // 런타임에 개체를 확인하여 사용할수 있는 dynamic을 이용해 역직렬화
            dynamic dob = js.Deserialize<dynamic>(json);
            
            // "object"내에 있는것을 얻어오기 위해 다시 dynamic변수에 참조
            dynamic docs = dob["object"]["item"];
            int length = 0;

            try {
                // docs는 요소 컬렉션으로 object로 변환.
                object[] buf = docs;
                length = buf.Length;
            }
            catch { 
                object buf = docs; length = 1; 
            }
            string book_name = string.Empty;
            string author = string.Empty;
            string book_comp = string.Empty;
            string isbn = string.Empty;
            string isbn13 = string.Empty;

            for (int a = 0; a < length; a++)
            {
                if (length == 1) {
                    book_name = docs["title"];
                    author = docs["author"];
                    book_comp = docs["publisher"];
                    isbn = docs["isbn"];
                    isbn13 = docs["isbn13"];
                }
                else {
                    book_name = docs[a]["title"];
                    author = docs[a]["author"];
                    book_comp = docs[a]["publisher"];
                    isbn = docs[a]["categoryName"];
                    isbn13 = docs[a]["isbn13"];
                }
                richTextBox1.Text += string.Format("{0}. title = {1} / author = {2} / book_comp = {3} / isbn = {4} / isbn13 = {5}\n"
                    , a.ToString(), book_name, author, book_comp, isbn, isbn13);
            }
        }
            private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(null, null);
            }
        }
    }
}
