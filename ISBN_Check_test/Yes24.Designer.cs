﻿
namespace ISBN_Check_test
{
    partial class Yes24
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_change = new System.Windows.Forms.Button();
            this.before_book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.after_book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.before_author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.after_author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.before_book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.after_book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cb_bracket = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.before_book_name,
            this.after_book_name,
            this.before_author,
            this.after_author,
            this.before_book_comp,
            this.after_book_comp,
            this.price});
            this.dataGridView1.Location = new System.Drawing.Point(13, 41);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1064, 661);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(900, 9);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 1;
            this.btn_Close.Text = "닫   기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_change
            // 
            this.btn_change.Location = new System.Drawing.Point(819, 9);
            this.btn_change.Name = "btn_change";
            this.btn_change.Size = new System.Drawing.Size(75, 23);
            this.btn_change.TabIndex = 1;
            this.btn_change.Text = "엑셀 변환";
            this.btn_change.UseVisualStyleBackColor = true;
            this.btn_change.Click += new System.EventHandler(this.btn_change_Click);
            // 
            // before_book_name
            // 
            this.before_book_name.HeaderText = "도서명 [전]";
            this.before_book_name.Name = "before_book_name";
            this.before_book_name.ReadOnly = true;
            this.before_book_name.Width = 200;
            // 
            // after_book_name
            // 
            this.after_book_name.HeaderText = "도서명 [후]";
            this.after_book_name.Name = "after_book_name";
            this.after_book_name.ReadOnly = true;
            this.after_book_name.Width = 200;
            // 
            // before_author
            // 
            this.before_author.HeaderText = "저자 [전]";
            this.before_author.Name = "before_author";
            this.before_author.ReadOnly = true;
            // 
            // after_author
            // 
            this.after_author.HeaderText = "저자 [후]";
            this.after_author.Name = "after_author";
            this.after_author.ReadOnly = true;
            // 
            // before_book_comp
            // 
            this.before_book_comp.HeaderText = "출판사 [전]";
            this.before_book_comp.Name = "before_book_comp";
            this.before_book_comp.ReadOnly = true;
            this.before_book_comp.Width = 150;
            // 
            // after_book_comp
            // 
            this.after_book_comp.HeaderText = "출판사 [후]";
            this.after_book_comp.Name = "after_book_comp";
            this.after_book_comp.ReadOnly = true;
            this.after_book_comp.Width = 150;
            // 
            // price
            // 
            this.price.HeaderText = "가격";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // cb_bracket
            // 
            this.cb_bracket.AutoSize = true;
            this.cb_bracket.Location = new System.Drawing.Point(296, 13);
            this.cb_bracket.Name = "cb_bracket";
            this.cb_bracket.Size = new System.Drawing.Size(104, 16);
            this.cb_bracket.TabIndex = 2;
            this.cb_bracket.Text = "괄호 제거 여부";
            this.cb_bracket.UseVisualStyleBackColor = true;
            this.cb_bracket.CheckedChanged += new System.EventHandler(this.cb_bracket_CheckedChanged);
            // 
            // Yes24
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 714);
            this.Controls.Add(this.cb_bracket);
            this.Controls.Add(this.btn_change);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Yes24";
            this.Text = "Yes24";
            this.Load += new System.EventHandler(this.Yes24_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_change;
        private System.Windows.Forms.DataGridViewTextBoxColumn before_book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn after_book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn before_author;
        private System.Windows.Forms.DataGridViewTextBoxColumn after_author;
        private System.Windows.Forms.DataGridViewTextBoxColumn before_book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn after_book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.CheckBox cb_bracket;
    }
}