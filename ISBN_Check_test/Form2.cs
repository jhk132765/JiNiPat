﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISBN_Check_test
{
    public partial class Form2 : Form
    {
        Form1 f1;
        public int row;
        public string Call_API = string.Empty;
        int rowidx;
        public Form2(Form1 _f1)
        {
            InitializeComponent();
            f1 = _f1;
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            dataGridView1.Focus();

            Sort_data(f1.dataGridView1);

            // 비슷한거 색깔표시
            same_chk();
        }
        private void Sort_data(DataGridView f1GridView)
        {
            tb_book_name.Text = f1GridView.Rows[row].Cells["book_name"].Value.ToString();
            tb_author.Text = f1GridView.Rows[row].Cells["author"].Value.ToString();
            tb_book_comp.Text = f1GridView.Rows[row].Cells["book_comp"].Value.ToString();

            if (f1GridView.Rows[row].Cells["price"].Value != null)
                tb_price.Text = f1GridView.Rows[row].Cells["price"].Value.ToString();
             
            else { tb_price.Text = ""; }

            string data = f1GridView.Rows[row].Cells["Column1"].Value.ToString();
            inputGrid(data);
        }
        private void inputGrid(string data)
        {
            dataGridView1.Rows.Clear();

            // 도서명 / 저자 / 출판사 / isbn / 출간일 / 카테고리 / 품절여부
            string[] tmp = data.Split('|');
            string[] grid = { "", "", "", "", "", "", "", "" };

            for (int a = 0; a < tmp.Length; a++)
            {
                if (a % 8 == 0) { grid[0] = tmp[a]; }
                if (a % 8 == 1) { grid[1] = tmp[a]; }
                if (a % 8 == 2) { grid[2] = tmp[a]; }
                if (a % 8 == 3) { grid[3] = tmp[a]; }
                if (a % 8 == 4) { grid[4] = tmp[a]; }
                if (a % 8 == 5) { grid[5] = change_Date_type(tmp[a]); }
                if (a % 8 == 6) { grid[6] = Aladin_CategorySort(tmp[a]); }
                if (a % 8 == 7) { grid[7] = tmp[a]; dataGridView1.Rows.Add(grid); }
            }
        }
        private string change_Date_type(string date) 
        {
            if (Call_API == "알라딘")
            {
                try
                {
                    return String.Format("{0:yyyy/MM/dd}",
                    DateTime.Parse(date.Remove(date.IndexOf(" G"))));
                }
                catch { return date; }
            }
            if (Call_API == "네이버")
                try
                {
                    return DateTime.ParseExact(date, "yyyyMMdd", null).ToString("yyyy-MM-dd");
                }
                catch { return ""; }
            else
                return date;
        }
        private void same_chk()
        {
            // 도서명, 저자, 출판사, ISBN,  정가
            string[] ori_data = { tb_book_name.Text, 
                                  tb_author.Text, 
                                  tb_book_comp.Text, 
                                  tb_isbn.Text, 
                                  tb_price.Text.Replace(",","") };

            for(int a= 0; a < dataGridView1.Rows.Count; a++)
            {
                int chk_idx = 0;

                if(dataGridView1.Rows[a].Cells["book_name"].Value.ToString() == ori_data[0]) {
                    chk_idx++;
                }
                if(dataGridView1.Rows[a].Cells["author"].Value.ToString().Contains(ori_data[1]) == true) {
                    chk_idx++;
                }
                if(dataGridView1.Rows[a].Cells["book_comp"].Value.ToString() == ori_data[2]) {
                    chk_idx++;
                }
                if (chk_idx >= 2) {
                    int pay = Convert.ToInt32(dataGridView1.Rows[a].Cells["price"].Value.ToString());
                    int price = 0;
                    if (ori_data[4] != "")
                        price = Convert.ToInt32(ori_data[4]);

                    if (price - 5000 <= pay && pay <= price + 5000)
                        dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Yellow;

                    else 
                        dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e != null) { rowidx = e.RowIndex; }
            string book_name = dataGridView1.Rows[rowidx].Cells["book_name"].Value.ToString();
            string author = dataGridView1.Rows[rowidx].Cells["author"].Value.ToString();
            string book_comp = dataGridView1.Rows[rowidx].Cells["book_comp"].Value.ToString();
            string isbn = dataGridView1.Rows[rowidx].Cells["isbn"].Value.ToString();
            string price = dataGridView1.Rows[rowidx].Cells["price"].Value.ToString();
            string Date = dataGridView1.Rows[rowidx].Cells["pubDate"].Value.ToString();
            string category = dataGridView1.Rows[rowidx].Cells["category"].Value.ToString();
            string sold = dataGridView1.Rows[rowidx].Cells["sold_out"].Value.ToString();

            f1.dataGridView1.Rows[row].Cells["book_name"].Value = book_name;
            f1.dataGridView1.Rows[row].Cells["author"].Value = author;
            f1.dataGridView1.Rows[row].Cells["book_comp"].Value = book_comp;
            f1.dataGridView1.Rows[row].Cells["isbn"].Value = isbn;
            f1.dataGridView1.Rows[row].Cells["price2"].Value = price;
            f1.dataGridView1.Rows[row].Cells["pubDate"].Value = Date;
            f1.dataGridView1.Rows[row].Cells["category"].Value = category;
            f1.dataGridView1.Rows[row].Cells["sold_out"].Value = sold;

            f1.dataGridView1.Rows[row].DefaultCellStyle.BackColor = Color.Yellow;

            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter) { dataGridView1_CellDoubleClick(null, null); }
            if (e.KeyCode == Keys.Up) {
                rowidx--;
                if (rowidx < 0) 
                    rowidx = 0;
            }
            if (e.KeyCode == Keys.Down) { 
                rowidx++;
                if (rowidx > dataGridView1.Rows.Count - 1)
                    rowidx = dataGridView1.Rows.Count - 1;
            }
            if (e.KeyCode == Keys.Escape) {
                this.Close();
            }
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowidx = e.RowIndex;
        }

        /// <summary>
        /// 재검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ReSearch_Click(object sender, EventArgs e)
        {
            string[] param = { "title", "author", "publisher", "isbn13", "priceStandard",
                               "pubDate", "categoryName", "stockStatus", };
            API api = new API();

            string type = "Title";
            string query = tb_book_name.Text;

            string aladin = api.Aladin(query, type, param);
            if (aladin == "") return;

            inputGrid(aladin);
        }

        string Aladin_CategorySort(string insert)
        {
            try
            {
                // 도서 분류 필요한 데이터로 재정리
                int top = insert.IndexOf('>');
                int mid = insert.IndexOf('>', top + 1);
                int bot = insert.IndexOf('>', mid + 1);
                if (bot < 0) { insert = insert.Substring(top + 1); }
                else { insert = insert.Substring(top + 1, bot - top - 1); }
                return insert;
            }
            catch
            {
                return insert;
            }
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }
    }
}
