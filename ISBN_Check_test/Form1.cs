﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using WindowsFormsApp1;
using System.Text.RegularExpressions;

namespace ISBN_Check_test
{
    public partial class Form1 : Form
    {
        private int rowidx;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            string[] api_list = { "다음", "네이버", "알라딘" };
            cb_api.Items.AddRange(api_list);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            for(int a = 0; a < dataGridView1.Rows.Count - 1; a++)
            {
                if (dataGridView1.Rows[a].DefaultCellStyle.BackColor != Color.Yellow) {
                    dataGridView1.Rows[a].Cells["Column1"].Value = "";
                    dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Empty;
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            button2_Click(null, null);
            if(cb_api.SelectedIndex == -1) { MessageBox.Show("조건이 선택되지 않았습니다."); return; }
            if(cb_filter.SelectedIndex == -1) { MessageBox.Show("조건이 선택되지 않았습니다."); return; }
            
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            int start = Convert.ToInt32(start_idx.Text) - 1;
            int end = Convert.ToInt32(end_idx.Text) - 1;

            progressBar1.Style = ProgressBarStyle.Continuous;
            progressBar1.Minimum = start;
            progressBar1.Maximum = end;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            if (start_idx.Text != "1" || end_idx.Text != dataGridView1.Rows.Count.ToString()) {
                start = Convert.ToInt32(start_idx.Text) - 1;
                end = Convert.ToInt32(end_idx.Text) - 1;
            }

            switch (cb_api.SelectedIndex)
            {
                case 0:
                    Daum_API(dataGridView1, start, end);
                    break;
                case 1:
                    Naver_API(dataGridView1, start, end);
                    break;
                case 2:
                    Aladin_API(dataGridView1, start, end);
                    break;
            }

            stopwatch.Stop();

            TimeSpan ts = stopwatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            label1.Text = elapsedTime;
            rowidx = 0;

            // 총 검색 횟수, 일치, 중복
            MessageBox.Show("검색이 완료되었습니다!");

            dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[0];
            this.ActiveControl = dataGridView1;
        }
        /// <summary>
        /// 알라딘 API
        /// </summary>
        /// <param name="gridview"></param>
        private void Aladin_API(DataGridView gridview, int start, int end)
        {
            // 도서명 / 저자 / 출판사 / isbn / 정가
            // 발행일 / 도서분류 / 재고
            string[] param = { "title", "author", "publisher", "isbn13", "priceStandard", 
                               "pubDate", "categoryName", "stockStatus", };
            API api = new API();
            for (int a = start; a < end; a++)
            {
                process_Sub();
                string type;
                switch (cb_filter.SelectedIndex)
                {
                    case 0:
                        type = "Keyword";
                        break;
                    case 1:
                        type = "Title";
                        break;
                    case 2:
                        type = "Author";
                        break;
                    case 3:
                        type = "Publisher";
                        break;
                    case 4:
                        type = "ISBN13";
                        break;
                    default:
                        type = "";
                        break;
                }
                // string query = dataGridView1.Rows[a].Cells["isbn"].Value.ToString();
                string query = Set_query(type, a);
                if (gridview.Rows[a].DefaultCellStyle.BackColor == Color.Yellow)
                    continue;
                else if (gridview.Rows[a].DefaultCellStyle.BackColor == Color.LightGray)
                    gridview.Rows[a].DefaultCellStyle.BackColor = Color.Empty;

                // string aladin = api.Aladin(query, "ISBN13", param);
                string aladin = api.Aladin(query, type, param);
                insert_By_Aladin(aladin, a);
                try
                {
                    richTextBox1.Text = aladin;
                }
                catch { }
            }
        }
        string Set_query(string type, int idx)
        {
            string result = string.Empty;

            if (type == "Keyword")
                result = dataGridView1.Rows[idx].Cells["book_name"].Value.ToString() +
                         dataGridView1.Rows[idx].Cells["author"].Value.ToString();

            if (type == "Title")
                result = dataGridView1.Rows[idx].Cells["book_name"].Value.ToString();

            if (type == "Author")
                result = dataGridView1.Rows[idx].Cells["author"].Value.ToString();

            if (type == "Publisher")
                result = dataGridView1.Rows[idx].Cells["book_comp"].Value.ToString();

            if (type == "ISBN13")
                result = dataGridView1.Rows[idx].Cells["isbn"].Value.ToString();

            return result;
        }
        private void Naver_API(DataGridView gridview, int start, int end)
        {
            // 도서명 / 저자 / 출판사 / isbn / 정가
            // 발행일 / 도서분류 / 재고
            string[] param = { "title", "author", "publisher", "isbn", "price",
                               "pubdate", "discount" };
            API api = new API();
            List<string> L_type = new List<string>();
            List<string> L_Array = new List<string>();

            for (int a = start; a < end; a++)
            {
                L_type.Clear();
                L_Array.Clear();
                process_Sub();
                if (gridview.Rows[a].DefaultCellStyle.BackColor == Color.Yellow)
                    continue;
                else if (gridview.Rows[a].DefaultCellStyle.BackColor == Color.LightGray)
                    gridview.Rows[a].DefaultCellStyle.BackColor = Color.Empty;

                #region 필터적용
                switch (cb_filter.SelectedIndex)
                {
                    case 0:
                        L_type.Add("d_titl");
                        L_Array.Add(gridview.Rows[a].Cells["book_name"].Value.ToString());
                        break;

                    case 1:
                        L_type.Add("d_auth");
                        L_Array.Add(gridview.Rows[a].Cells["author"].Value.ToString());
                        break;

                    case 2:
                        L_type.Add("d_publ");
                        L_Array.Add(gridview.Rows[a].Cells["book_comp"].Value.ToString());
                        break;

                    case 3:
                        L_type.Add("d_titl");
                        L_type.Add("d_auth");
                        L_Array.Add(gridview.Rows[a].Cells["book_name"].Value.ToString());
                        L_Array.Add(gridview.Rows[a].Cells["author"].Value.ToString());
                        break;

                    case 4:
                        L_type.Add("d_titl");
                        L_type.Add("d_publ");
                        L_Array.Add(gridview.Rows[a].Cells["book_name"].Value.ToString());
                        L_Array.Add(gridview.Rows[a].Cells["book_comp"].Value.ToString());
                        break;

                    case 5:
                        L_type.Add("d_auth");
                        L_type.Add("d_publ");
                        L_Array.Add(gridview.Rows[a].Cells["author"].Value.ToString());
                        L_Array.Add(gridview.Rows[a].Cells["book_comp"].Value.ToString());
                        break;

                    case 6:
                        L_type.Add("d_titl");
                        L_type.Add("d_auth");
                        L_type.Add("d_publ");
                        L_Array.Add(gridview.Rows[a].Cells["book_name"].Value.ToString());
                        L_Array.Add(gridview.Rows[a].Cells["author"].Value.ToString());
                        L_Array.Add(gridview.Rows[a].Cells["book_comp"].Value.ToString());
                        break;
                }
                #endregion

                string[] arrayType = L_type.ToArray();
                string[] arrayValue = L_Array.ToArray();
                string result = api.Naver(arrayValue, arrayType, param);
                insert_By_Naver(result, a);
                Thread.Sleep(700);
            }
        }
        private void Daum_API(DataGridView gridview, int start, int end)
        {
            string[] param = { "title", "authors", "publisher", "isbn", "price",
                               "datetime", "status" };
            string type = string.Empty;
            string query = string.Empty;
            API api = new API();
            
            for(int a = start; a < end; a++)
            {
                process_Sub();
                if (gridview.Rows[a].DefaultCellStyle.BackColor == Color.Yellow)
                    continue;
                else if (gridview.Rows[a].DefaultCellStyle.BackColor == Color.LightGray)
                    gridview.Rows[a].DefaultCellStyle.BackColor = Color.Empty;

                switch (cb_filter.SelectedIndex)
                {
                    case 0:
                        type = "title";
                        query = gridview.Rows[a].Cells["book_name"].Value.ToString();
                        break;
                    case 1:
                        type = "person";
                        query = gridview.Rows[a].Cells["author"].Value.ToString();
                        break;
                    case 2:
                        type = "publisher";
                        query = gridview.Rows[a].Cells["book_comp"].Value.ToString();
                        break;
                }
                if (query == "") { continue; }

                string result = api.Daum(query, type, param);
                richTextBox1.Text = result;
                insert_By_Daum(result, a);
            }
        }
        private void process_Sub()
        {
            progressBar1.PerformStep();
        }
        void insert_By_Aladin(string data, int row)
        {
            if (row > 0) { dataGridView1.Rows[row - 1].Selected = false; }
            dataGridView1.Rows[row].Selected = true;

            string[] insert = data.Split('|');

            if (data == "") {  return; }

            // pubDate형 보기편하게 DateTime형으로 재정리
            string newstring = "";
            try
            {
                newstring = String.Format("{0:yyyy/MM/dd}",
                    DateTime.Parse(insert[5].Remove(insert[5].IndexOf(" G"))));
            }
            catch(Exception ex) { MessageBox.Show(data); }

            for (int a = 0; a < insert.Length; a++)
            {
                if (a % 8 == 6) { insert[a] = Aladin_CategorySort(insert[a]); }
            }

            dataGridView1.Rows[row].Cells["Column1"].Value += string.Join("|", insert) + "|";
            dataGridView1.Rows[row].DefaultCellStyle.BackColor = Color.LightGray;
            if (cb_filter.SelectedItem.ToString() == "별치조사")
                input_api_aladin(insert, row, newstring);
            input_api(insert, row, newstring);
        }
        string Aladin_CategorySort(string insert)
        {
            // 도서 분류 필요한 데이터로 재정리
            int top = insert.IndexOf('>');
            int mid = insert.IndexOf('>', top + 1);
            int bot = insert.IndexOf('>', mid + 1);

            if (bot < 0) { insert = insert.Substring(top + 1); }
            else { insert = insert.Substring(top + 1, bot - top - 1); }

            return insert;
        }
        void input_api_aladin(string[] data, int row, string date)
        {
            dataGridView1.Rows[row].Cells["book_name"].Value = data[0];
            dataGridView1.Rows[row].Cells["author"].Value = data[1];
            dataGridView1.Rows[row].Cells["book_comp"].Value = data[2];
            dataGridView1.Rows[row].Cells["price2"].Value = data[4];
            dataGridView1.Rows[row].Cells["pubDate"].Value = date;
            dataGridView1.Rows[row].Cells["category"].Value = data[6];
        }
        void insert_By_Naver(string value, int row)
        {
            if (row > 0) { dataGridView1.Rows[row - 1].Selected = false; }
            dataGridView1.Rows[row].Selected = true;

            if (value == "") return;

            value = value.Replace("<b>", "");
            value = value.Replace("</b>", "");

            string[] sp_data = value.Split('\t');
            string[] grid = { "", "", "", "", "", "", "", "" };

            #region 분류작업
            /* 0 : 도서명
             * 1 : 저자
             * 2 : 출판사
             * 3 : ISBN
             * 4 : 판매가
             * 5 : 출간일
             * 6 : 카테고리
             * 7 : 품절/절판 */
            for (int a = 0; a < sp_data.Length-1; a++)
            {
                string[] data = sp_data[a].Split('|');
                int idx = data.Length - 2;
                grid[0] = data[0];
                grid[1] = data[1];
                for (int b = 2; b < idx - 4; b++)
                {
                    grid[1] += ", " + data[b];
                }
                grid[2] = data[idx - 4];
                if (data[idx - 3].Contains(" ") == true)
                {
                    string[] isbn = data[idx - 3].Split(' ');
                    grid[3] = isbn[1];
                }
                else
                    grid[3] = data[idx - 3];

                grid[4] = data[idx - 2];
                grid[5] = data[idx - 1];

                if (data[idx] == "")
                    grid[7] = "절판";
                else
                    grid[7] = "";

                dataGridView1.Rows[row].Cells["Column1"].Value += string.Join("|", grid) + "|";
                dataGridView1.Rows[row].DefaultCellStyle.BackColor = Color.LightGray;
            }
            #endregion

            if (sp_data.Length > 10) return;

            if (row > 0) { dataGridView1.Rows[row - 1].Selected = false; }
            dataGridView1.Rows[row].Selected = true;
            string newstring = "";
            try
            {
                newstring = DateTime.ParseExact(grid[5], "yyyyMMdd", null).ToString("yyyy-MM-dd");
            }
            catch { }

            input_api(grid, row, newstring);
        }
        void insert_By_Daum(string value, int row)
        {
            if (row > 0) { dataGridView1.Rows[row - 1].Selected = false; }
            dataGridView1.Rows[row].Selected = true;

            if (value == "") return;

            string[] sp_data = value.Split('\n');
            string[] grid = { "", "", "", "", "", "", "", "" };

            for (int a = 0; a < sp_data.Length-1; a++)
            {
                string[] data = sp_data[a].Split('|');
                grid[0] = data[0];
                grid[1] = data[1];
                grid[2] = data[2];
                string[] tmp_isbn = data[3].Split(' ');
                if (tmp_isbn.Length < 2)
                    grid[3] = data[3].Replace(" ", "");

                else
                    grid[3] = tmp_isbn[1];

                grid[4] = data[4];
                try
                {
                    grid[5] = data[5].Substring(0, 10);
                }
                catch { }
                grid[7] = data[6];

                dataGridView1.Rows[row].Cells["Column1"].Value += string.Join("|", grid) + "|";
                dataGridView1.Rows[row].DefaultCellStyle.BackColor = Color.LightGray;
            }

            if (sp_data.Length > 10) return;

            if (row > 0) dataGridView1.Rows[row - 1].Selected = false;
            dataGridView1.Rows[row].Selected = true;

            bool[] chk = { false, false, false };
            // string newstring = DateTime.ParseExact(grid[5], "yyyyMMdd", null).ToString("yyyy-MM-dd");
            string newstring = grid[5];

            input_api(grid, row, newstring);
        }
        /// <summary>
        /// API에서 가져온 데이터가 요구한 데이터와 일치하는지 알아보는 함수
        /// </summary>
        /// <param name="value">데이터</param>
        /// <param name="idx">Grid의 row인덱스번호</param>
        /// <param name="date">날짜</param>
        void input_api(string[] value, int idx, string date)
        {
            bool[] chk = { false, false, false };

            string book_name = dataGridView1.Rows[idx].Cells["book_name"].Value.ToString();
            string author = dataGridView1.Rows[idx].Cells["author"].Value.ToString();
            string book_comp = dataGridView1.Rows[idx].Cells["book_comp"].Value.ToString();

            if (value[0] == book_name) chk[0] = true;

            if (value[1].Contains(author)==true) chk[1] = true;
            else if (author.Contains(value[1])==true) chk[1] = true;
            else if (value[1] == author) chk[1] = true;

            if (value[2].Contains(book_comp) == true) chk[2] = true;
            else if (book_comp.Contains(value[2]) == true) chk[2] = true;
            else if (value[2] == book_comp) chk[2] = true;

            if (chk[0] == true && chk[1] == true && chk[2] == true) {

                dataGridView1.Rows[idx].Cells["isbn"].Value = value[3];
                dataGridView1.Rows[idx].Cells["price2"].Value = value[4];
                dataGridView1.Rows[idx].Cells["pubDate"].Value = date;
                //if (cb_api.SelectedIndex == 2)
                dataGridView1.Rows[idx].Cells["category"].Value = value[6];
                dataGridView1.Rows[idx].Cells["sold_out"].Value = value[7];
                dataGridView1.Rows[idx].DefaultCellStyle.BackColor = Color.Yellow;
            }
            count_res();
        }
        #region 검색갯수 계산
        private void count_res()
        {
            String_Text st = new String_Text();
            int count = dataGridView1.Rows.Count;
            for (int a = 0; a < count; a++)
            {
                string search_data = dataGridView1.Rows[a].Cells["Column1"].Value.ToString();
                int tmp_count = st.Char_count(search_data, '|');
                int lcount = tmp_count / 8;
                dataGridView1.Rows[a].Cells["count"].Value = lcount.ToString();
            }
        }
        #endregion
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();

            if ((e.Shift && e.KeyCode == Keys.Insert) || (e.Control && e.KeyCode == Keys.V)) {
                sg.Excel_to_DataGridView(sender, e);
            }
            else if (e.KeyCode == Keys.Delete) {
                sg.DataGrid_to_Delete(sender, e);
            }
            if (e.KeyCode == Keys.Enter) { dataGridView1_CellDoubleClick(null, null); rowidx++; }
            if (e.KeyCode == Keys.Up) {
                rowidx--;
                if (rowidx < 0)
                    rowidx = 0;
            }
            if (e.KeyCode == Keys.Down) {
                rowidx++;
                if (rowidx > dataGridView1.Rows.Count - 1)
                    rowidx = dataGridView1.Rows.Count - 1;
            }
            end_idx.Text = dataGridView1.Rows.Count.ToString();
        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();

            sg.Print_Grid_Num(sender, e);
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            rowidx = e.RowIndex;
            if (rowidx < 0) return;
            if (dataGridView1.Rows[e.RowIndex].Cells["Column1"].Value == null)
                dataGridView1.Rows[e.RowIndex].Cells["Column1"].Value = "";
            
            richTextBox1.Text = dataGridView1.Rows[e.RowIndex].Cells["Column1"].Value.ToString();
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows[rowidx].Cells["Column1"].Value == null || 
                dataGridView1.Rows[rowidx].Cells["Column1"].Value.ToString() == "") { return; }
            Form2 f2 = new Form2(this);
            f2.row = rowidx;
            f2.Call_API = cb_api.Text;
            f2.Show();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                e.Handled = true;
        }
        private void cb_api_SelectedIndexChanged(object sender, EventArgs e)
        {
            cb_filter.Items.Clear();
            if (cb_api.SelectedIndex == 0){
                string[] daum = { "도서명", "저자", "출판사" };
                cb_filter.Items.AddRange(daum);
            }
            else if (cb_api.SelectedIndex == 1) {
                string[] naver = { "도서명", "저자", "출판사", 
                                   "도서명 + 저자", "도서명 + 출판사", "저자 + 출판사", 
                                   "도서명 + 저자 + 출판사" };
                cb_filter.Items.AddRange(naver);
            }
            else if (cb_api.SelectedIndex == 2) {
                string[] aladin = { "도서명 + 저자", "도서명", "저자", "출판사", "별치조사" };
                cb_filter.Items.AddRange(aladin);
            }
            Must_Col(cb_api.SelectedIndex);
        }
        private void Must_Col(int cb_idx)
        {
            if (cb_idx == 0 || cb_idx == 1 || cb_idx == 2)
            {
                dataGridView1.Columns["isbn"].DefaultCellStyle.BackColor = Color.Empty;
                dataGridView1.Columns["book_name"].DefaultCellStyle.BackColor = Color.FromArgb(234, 226, 202);
                dataGridView1.Columns["author"].DefaultCellStyle.BackColor = Color.FromArgb(234, 226, 202);
                dataGridView1.Columns["book_comp"].DefaultCellStyle.BackColor = Color.FromArgb(234, 226, 202);
            }
            else {
                return;
            }
        }
        private void btn_Yes24_Click(object sender, EventArgs e)
        {
            Yes24 yes24 = new Yes24(this);
            yes24.Show();
        }

        private void cb_filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (cb_filter.SelectedItem.ToString() == "별치조사")
            {
                dataGridView1.Columns["isbn"].DefaultCellStyle.BackColor = Color.FromArgb(234, 226, 202);
                dataGridView1.Columns["book_name"].DefaultCellStyle.BackColor = Color.Empty;
                dataGridView1.Columns["author"].DefaultCellStyle.BackColor = Color.Empty;
                dataGridView1.Columns["book_comp"].DefaultCellStyle.BackColor = Color.Empty;
            }
        }
    }
}
