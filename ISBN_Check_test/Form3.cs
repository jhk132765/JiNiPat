﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISBN_Check_test
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        private void Form3_Load(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            API api = new API();
            // 도서명, 저자, 출판사
            string[] param = { "title", "author", "publisher", "isbn13", "priceStandard",
                               "pubDate", "categoryName" };
            richTextBox1.Text = api.Aladin_lookup(tb_author.Text, "ISBN13", param);
            string[] ArrayValue = { tb_book_name.Text, tb_author.Text, tb_book_comp.Text };

            // 도서명 / 저자 / 출판사 / isbn / 정가
            // 발행일 / 도서분류 / 재고
            // string[] param = { "title", "authors", "publisher", "isbn", "price",
            //                    "datetime", "status" };
            // string result = api.Daum(ArrayValue, param);
            // richTextBox1.Text = result;
            // input_Grid(result);
        }
        private void input_Grid(string value)
        {
            string[] sp_data = value.Split('|');

            string[] grid = { "", "", "", "", "", "", "" };

            for (int a = 0; a < sp_data.Length; a++)
            {
                if (a % 7 == 0) { grid[0] = sp_data[a]; }   // 도서명
                if (a % 7 == 1) { grid[1] = sp_data[a]; }   // 저자
                if (a % 7 == 2) { grid[2] = sp_data[a]; }   // 출판사
                if (a % 7 == 3) {
                    string[] tmp_isbn = sp_data[a].Split('|');
                    if (tmp_isbn.Length < 2) 
                        grid[3] = sp_data[a].Replace(" ", "");
                    
                    else
                        grid[3] = tmp_isbn[1]; }   // isbn
                if (a % 7 == 4) { grid[4] = sp_data[a]; }   // 가격
                if (a % 7 == 5) {
                    sp_data[a] = sp_data[a].Substring(0, sp_data[a].IndexOf('T'));
                    grid[5] = sp_data[a]; }   // 출간일
                if (a % 7 == 6) { grid[6] = sp_data[a];
                    dataGridView1.Rows.Add(grid);
                }   // 판매여부
            }
        }
        private void tb_book_name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                button1_Click(null, null);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            // yyyyMMddhhmmss

            string tmp = "yyyyMMddhhmmss";
            string msg = string.Empty;

            msg += tmp.Substring(0, 4) + "\n" + tmp.Substring(4, 2) + "\n" + tmp.Substring(6, 2) + "\n" + tmp.Substring(8,2) + "\n" + tmp.Substring(10,2) + "\n" + tmp.Substring(12,2);

            MessageBox.Show(msg);
        }
    }
}
