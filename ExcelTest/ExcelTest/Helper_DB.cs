﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ServerText
{
    /// <summary>
    /// DB접속을 도와주는 클래스
    /// </summary>
    class Helper_DB
    {
        // 접속
        MySqlConnection conn = new MySqlConnection();

        // 쿼리
        MySqlCommand sqlcmd = new MySqlCommand();
        MySqlDataReader sd;

        /// <summary>
        /// DB를 사용하고 싶을 때 미리 저장된 DB의 기본 접속정보를 이용하여 DB에 접근한다.
        /// </summary>
        public void DBcon()     // DB접속 함수
        {
            try
            {
                // DB 접속 정보
                string connectionString = "Server = 192.168.123.191;"
                                        + "database=unimarc;"
                                        + "uid=root;"
                                        + "pwd=Admin21234;";

                // 접속정보 적용
                conn.ConnectionString = connectionString;
            }
            catch (Exception)
            {
                sqlcmd.Connection.Close();
            }
        }

        /// <summary>
        /// DBcon이 선진행되어야함.
        /// </summary>
        /// <param name="DB_Table_Name"></param>
        /// <returns>검색된 결과값이 반환됨.</returns>
        public string DB_Search(string DB_Table_Name)       // DB 
        {
            string cmd = "SELECT * FROM ";
            cmd += DB_Table_Name;
            string result = "";

            // DB연결
            conn.Open();
            // 쿼리 맵핑
            sqlcmd.CommandText = cmd;
            // 쿼리 날릴 곳은 conn, 즉 아까 연결한 DB
            sqlcmd.Connection = conn;
            // 쿼리 날리기, sqlDataReader에 결과값 저장
            sd = sqlcmd.ExecuteReader();

            // 한줄씩 불러오기
            while (sd.Read())
            {
                // user_id, user_pw 순의 컬럼이 있다고 가정
                // 첫 컬럼 sd[0]으로 불러오기
                result = sd[0].ToString();
                // 다음 컬럼은 컬럼명으로 불러오기
                result += "|" + sd["RATING"].ToString();
            }
            return result;
        }
    }
}
