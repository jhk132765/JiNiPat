﻿namespace ExcelTest
{
    partial class Marc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label98 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label99 = new System.Windows.Forms.Label();
            this.text008col = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label101 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label102 = new System.Windows.Forms.Label();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.text008gov = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.GridView020 = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text041a = new System.Windows.Forms.TextBox();
            this.text041b = new System.Windows.Forms.TextBox();
            this.text041h = new System.Windows.Forms.TextBox();
            this.text041k = new System.Windows.Forms.TextBox();
            this.text546a = new System.Windows.Forms.TextBox();
            this.textKDC4 = new System.Windows.Forms.TextBox();
            this.textDDC21 = new System.Windows.Forms.TextBox();
            this.textKDC5 = new System.Windows.Forms.TextBox();
            this.textDDC22 = new System.Windows.Forms.TextBox();
            this.textKDC6 = new System.Windows.Forms.TextBox();
            this.textDDC23 = new System.Windows.Forms.TextBox();
            this.text245a = new System.Windows.Forms.TextBox();
            this.text940a = new System.Windows.Forms.TextBox();
            this.text245b = new System.Windows.Forms.TextBox();
            this.text245x = new System.Windows.Forms.TextBox();
            this.text250a = new System.Windows.Forms.TextBox();
            this.text245n = new System.Windows.Forms.TextBox();
            this.text245p = new System.Windows.Forms.TextBox();
            this.GridView246 = new System.Windows.Forms.DataGridView();
            this.Column10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.invertCheck = new System.Windows.Forms.CheckBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.basicHeadBox = new System.Windows.Forms.TextBox();
            this.text245d = new System.Windows.Forms.TextBox();
            this.text245e = new System.Windows.Forms.TextBox();
            this.text700a = new System.Windows.Forms.TextBox();
            this.text900a = new System.Windows.Forms.TextBox();
            this.text507a = new System.Windows.Forms.TextBox();
            this.GridView710 = new System.Windows.Forms.DataGridView();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridView910 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text260a = new System.Windows.Forms.TextBox();
            this.text260b = new System.Windows.Forms.TextBox();
            this.text260c = new System.Windows.Forms.TextBox();
            this.text260g = new System.Windows.Forms.TextBox();
            this.text300a = new System.Windows.Forms.TextBox();
            this.text300b = new System.Windows.Forms.TextBox();
            this.text536a = new System.Windows.Forms.TextBox();
            this.text586a = new System.Windows.Forms.TextBox();
            this.GridView440 = new System.Windows.Forms.DataGridView();
            this.text440a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440p = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440v = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440x = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.text525a = new System.Windows.Forms.TextBox();
            this.text653a = new System.Windows.Forms.TextBox();
            this.GridView490 = new System.Windows.Forms.DataGridView();
            this.text490a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text490v = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text500a = new System.Windows.Forms.TextBox();
            this.GridView505 = new System.Windows.Forms.DataGridView();
            this.text505n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text505t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text505d = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text505e = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text504a = new System.Windows.Forms.TextBox();
            this.text505a = new System.Windows.Forms.TextBox();
            this.text650a = new System.Windows.Forms.TextBox();
            this.text521a = new System.Windows.Forms.TextBox();
            this.text520a = new System.Windows.Forms.TextBox();
            this.text300c1 = new System.Windows.Forms.TextBox();
            this.text300e = new System.Windows.Forms.TextBox();
            this.text300c2 = new System.Windows.Forms.TextBox();
            this.col008res = new System.Windows.Forms.Label();
            this.gov008res = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.Btn_Helper = new System.Windows.Forms.Button();
            this.Btn_interlock = new System.Windows.Forms.Button();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridView020)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView246)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView710)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView910)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView440)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView490)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView505)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.AcceptsTab = true;
            this.richTextBox1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.richTextBox1.Location = new System.Drawing.Point(1334, 6);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(284, 482);
            this.richTextBox1.TabIndex = 32;
            this.richTextBox1.Text = "메모장";
            this.richTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyDown);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button2.Location = new System.Drawing.Point(1286, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(42, 101);
            this.button2.TabIndex = 1;
            this.button2.Text = ">>";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button3.Location = new System.Drawing.Point(1286, 113);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(42, 101);
            this.button3.TabIndex = 1;
            this.button3.Text = "<<";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.richTextBox2.Location = new System.Drawing.Point(1334, 494);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(284, 120);
            this.richTextBox2.TabIndex = 32;
            this.richTextBox2.Text = "Remark1";
            this.richTextBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyDown);
            // 
            // richTextBox3
            // 
            this.richTextBox3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.richTextBox3.Location = new System.Drawing.Point(1334, 620);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(284, 120);
            this.richTextBox3.TabIndex = 32;
            this.richTextBox3.Text = "Remark2";
            this.richTextBox3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(348, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "입력일자";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(403, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 204;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(517, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 206;
            this.label2.Text = "이용자";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(558, 14);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(79, 20);
            this.comboBox1.TabIndex = 207;
            this.comboBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(650, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 206;
            this.label3.Text = "자료형식";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(701, 14);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(79, 20);
            this.comboBox2.TabIndex = 207;
            this.comboBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(803, 18);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(53, 12);
            this.label98.TabIndex = 14;
            this.label98.Text = "내용형식";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(857, 14);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(118, 20);
            this.comboBox3.TabIndex = 207;
            this.comboBox3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(1100, 18);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(29, 12);
            this.label99.TabIndex = 206;
            this.label99.Text = "대학";
            // 
            // text008col
            // 
            this.text008col.Location = new System.Drawing.Point(1129, 14);
            this.text008col.Name = "text008col";
            this.text008col.Size = new System.Drawing.Size(41, 21);
            this.text008col.TabIndex = 204;
            this.text008col.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text008col_KeyDown);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(602, 47);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(53, 12);
            this.label100.TabIndex = 206;
            this.label100.Text = "문학형식";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(653, 43);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(79, 20);
            this.comboBox4.TabIndex = 207;
            this.comboBox4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(741, 47);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(29, 12);
            this.label101.TabIndex = 206;
            this.label101.Text = "전기";
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(771, 43);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(101, 20);
            this.comboBox5.TabIndex = 207;
            this.comboBox5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(887, 47);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(29, 12);
            this.label102.TabIndex = 206;
            this.label102.Text = "언어";
            // 
            // comboBox6
            // 
            this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(917, 43);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(101, 20);
            this.comboBox6.TabIndex = 207;
            this.comboBox6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // text008gov
            // 
            this.text008gov.Location = new System.Drawing.Point(1057, 43);
            this.text008gov.Name = "text008gov";
            this.text008gov.Size = new System.Drawing.Size(41, 21);
            this.text008gov.TabIndex = 204;
            this.text008gov.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text008col_KeyDown);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(1028, 47);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(29, 12);
            this.label103.TabIndex = 206;
            this.label103.Text = "기관";
            // 
            // GridView020
            // 
            this.GridView020.AllowDrop = true;
            this.GridView020.AllowUserToResizeRows = false;
            this.GridView020.BackgroundColor = System.Drawing.SystemColors.Window;
            this.GridView020.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView020.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column1,
            this.Column2,
            this.Column3});
            this.GridView020.Location = new System.Drawing.Point(350, 71);
            this.GridView020.Name = "GridView020";
            this.GridView020.RowHeadersWidth = 30;
            this.GridView020.RowTemplate.Height = 23;
            this.GridView020.Size = new System.Drawing.Size(395, 71);
            this.GridView020.TabIndex = 208;
            this.GridView020.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.GridView020.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragDrop);
            this.GridView020.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragOver);
            this.GridView020.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseDown);
            this.GridView020.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseMove);
            // 
            // Column4
            // 
            this.Column4.HeaderText = "세트";
            this.Column4.Name = "Column4";
            this.Column4.Width = 35;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "020a";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "020g";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "020c";
            this.Column3.Name = "Column3";
            // 
            // text041a
            // 
            this.text041a.Location = new System.Drawing.Point(760, 71);
            this.text041a.Name = "text041a";
            this.text041a.Size = new System.Drawing.Size(94, 21);
            this.text041a.TabIndex = 209;
            this.text041a.Text = "041a";
            // 
            // text041b
            // 
            this.text041b.Location = new System.Drawing.Point(1099, 71);
            this.text041b.Name = "text041b";
            this.text041b.Size = new System.Drawing.Size(94, 21);
            this.text041b.TabIndex = 209;
            this.text041b.Text = "041b";
            // 
            // text041h
            // 
            this.text041h.Location = new System.Drawing.Point(986, 71);
            this.text041h.Name = "text041h";
            this.text041h.Size = new System.Drawing.Size(94, 21);
            this.text041h.TabIndex = 209;
            this.text041h.Text = "041h";
            // 
            // text041k
            // 
            this.text041k.Location = new System.Drawing.Point(873, 71);
            this.text041k.Name = "text041k";
            this.text041k.Size = new System.Drawing.Size(94, 21);
            this.text041k.TabIndex = 209;
            this.text041k.Text = "041k";
            // 
            // text546a
            // 
            this.text546a.AcceptsReturn = true;
            this.text546a.AcceptsTab = true;
            this.text546a.Location = new System.Drawing.Point(760, 98);
            this.text546a.Multiline = true;
            this.text546a.Name = "text546a";
            this.text546a.Size = new System.Drawing.Size(305, 44);
            this.text546a.TabIndex = 209;
            this.text546a.Text = "546a 언어주기";
            // 
            // textKDC4
            // 
            this.textKDC4.Location = new System.Drawing.Point(350, 148);
            this.textKDC4.Name = "textKDC4";
            this.textKDC4.Size = new System.Drawing.Size(112, 21);
            this.textKDC4.TabIndex = 210;
            this.textKDC4.Text = "KDC 4";
            // 
            // textDDC21
            // 
            this.textDDC21.Location = new System.Drawing.Point(760, 148);
            this.textDDC21.Name = "textDDC21";
            this.textDDC21.Size = new System.Drawing.Size(112, 21);
            this.textDDC21.TabIndex = 210;
            this.textDDC21.Text = "DDC 21";
            // 
            // textKDC5
            // 
            this.textKDC5.Location = new System.Drawing.Point(495, 148);
            this.textKDC5.Name = "textKDC5";
            this.textKDC5.Size = new System.Drawing.Size(112, 21);
            this.textKDC5.TabIndex = 210;
            this.textKDC5.Text = "KDC 5";
            // 
            // textDDC22
            // 
            this.textDDC22.Location = new System.Drawing.Point(905, 148);
            this.textDDC22.Name = "textDDC22";
            this.textDDC22.Size = new System.Drawing.Size(112, 21);
            this.textDDC22.TabIndex = 210;
            this.textDDC22.Text = "DDC 22";
            // 
            // textKDC6
            // 
            this.textKDC6.Location = new System.Drawing.Point(640, 148);
            this.textKDC6.Name = "textKDC6";
            this.textKDC6.Size = new System.Drawing.Size(112, 21);
            this.textKDC6.TabIndex = 210;
            this.textKDC6.Text = "KDC 6";
            // 
            // textDDC23
            // 
            this.textDDC23.Location = new System.Drawing.Point(1050, 148);
            this.textDDC23.Name = "textDDC23";
            this.textDDC23.Size = new System.Drawing.Size(112, 21);
            this.textDDC23.TabIndex = 210;
            this.textDDC23.Text = "DDC 23";
            // 
            // text245a
            // 
            this.text245a.Location = new System.Drawing.Point(350, 175);
            this.text245a.Name = "text245a";
            this.text245a.Size = new System.Drawing.Size(402, 21);
            this.text245a.TabIndex = 211;
            this.text245a.Text = "245a 서명";
            // 
            // text940a
            // 
            this.text940a.Location = new System.Drawing.Point(350, 197);
            this.text940a.Name = "text940a";
            this.text940a.Size = new System.Drawing.Size(402, 21);
            this.text940a.TabIndex = 211;
            this.text940a.Text = "940a 로컬서명";
            // 
            // text245b
            // 
            this.text245b.Location = new System.Drawing.Point(760, 175);
            this.text245b.Name = "text245b";
            this.text245b.Size = new System.Drawing.Size(402, 21);
            this.text245b.TabIndex = 211;
            this.text245b.Text = "245b";
            // 
            // text245x
            // 
            this.text245x.Location = new System.Drawing.Point(760, 197);
            this.text245x.Name = "text245x";
            this.text245x.Size = new System.Drawing.Size(200, 21);
            this.text245x.TabIndex = 211;
            this.text245x.Text = "245x";
            // 
            // text250a
            // 
            this.text250a.Location = new System.Drawing.Point(962, 197);
            this.text250a.Name = "text250a";
            this.text250a.Size = new System.Drawing.Size(200, 21);
            this.text250a.TabIndex = 211;
            this.text250a.Text = "250a";
            // 
            // text245n
            // 
            this.text245n.Location = new System.Drawing.Point(350, 219);
            this.text245n.Name = "text245n";
            this.text245n.Size = new System.Drawing.Size(94, 21);
            this.text245n.TabIndex = 211;
            this.text245n.Text = "245n";
            // 
            // text245p
            // 
            this.text245p.Location = new System.Drawing.Point(462, 219);
            this.text245p.Name = "text245p";
            this.text245p.Size = new System.Drawing.Size(290, 21);
            this.text245p.TabIndex = 211;
            this.text245p.Text = "245p";
            // 
            // GridView246
            // 
            this.GridView246.AllowDrop = true;
            this.GridView246.AllowUserToResizeRows = false;
            this.GridView246.BackgroundColor = System.Drawing.SystemColors.Window;
            this.GridView246.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView246.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column10,
            this.Column5,
            this.Column6,
            this.Column9,
            this.Column7,
            this.Column8});
            this.GridView246.Location = new System.Drawing.Point(760, 238);
            this.GridView246.Name = "GridView246";
            this.GridView246.RowHeadersWidth = 30;
            this.GridView246.RowTemplate.Height = 23;
            this.GridView246.Size = new System.Drawing.Size(568, 71);
            this.GridView246.TabIndex = 208;
            this.GridView246.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView2_DataError);
            this.GridView246.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.GridView246.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragDrop);
            this.GridView246.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragOver);
            this.GridView246.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseDown);
            this.GridView246.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseMove);
            // 
            // Column10
            // 
            this.Column10.DisplayStyleForCurrentCellOnly = true;
            this.Column10.HeaderText = "지시기호";
            this.Column10.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E"});
            this.Column10.Name = "Column10";
            this.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column10.Width = 77;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "246i";
            this.Column5.Name = "Column5";
            this.Column5.Width = 80;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "246a";
            this.Column6.Name = "Column6";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "246b";
            this.Column9.Name = "Column9";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "246n";
            this.Column7.Name = "Column7";
            this.Column7.Width = 50;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "246p";
            this.Column8.Name = "Column8";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton4);
            this.panel1.Controls.Add(this.invertCheck);
            this.panel1.Controls.Add(this.radioButton3);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.basicHeadBox);
            this.panel1.Location = new System.Drawing.Point(350, 246);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(402, 60);
            this.panel1.TabIndex = 214;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(282, 36);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(71, 16);
            this.radioButton4.TabIndex = 207;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "작자미상";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // invertCheck
            // 
            this.invertCheck.AutoSize = true;
            this.invertCheck.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.invertCheck.Location = new System.Drawing.Point(294, 10);
            this.invertCheck.Name = "invertCheck";
            this.invertCheck.Size = new System.Drawing.Size(48, 16);
            this.invertCheck.TabIndex = 213;
            this.invertCheck.Text = "생성";
            this.invertCheck.UseVisualStyleBackColor = true;
            this.invertCheck.MouseClick += new System.Windows.Forms.MouseEventHandler(this.invertCheck_MouseClick);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(189, 36);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(59, 16);
            this.radioButton3.TabIndex = 207;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "회의명";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(96, 36);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(59, 16);
            this.radioButton2.TabIndex = 207;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "단체명";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(3, 36);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(59, 16);
            this.radioButton1.TabIndex = 207;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "개인명";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // basicHeadBox
            // 
            this.basicHeadBox.Location = new System.Drawing.Point(0, 8);
            this.basicHeadBox.Name = "basicHeadBox";
            this.basicHeadBox.Size = new System.Drawing.Size(285, 21);
            this.basicHeadBox.TabIndex = 211;
            this.basicHeadBox.Text = "기본표목";
            // 
            // text245d
            // 
            this.text245d.Location = new System.Drawing.Point(350, 315);
            this.text245d.Name = "text245d";
            this.text245d.Size = new System.Drawing.Size(253, 21);
            this.text245d.TabIndex = 211;
            this.text245d.Text = "245d";
            // 
            // text245e
            // 
            this.text245e.Location = new System.Drawing.Point(609, 315);
            this.text245e.Name = "text245e";
            this.text245e.Size = new System.Drawing.Size(719, 21);
            this.text245e.TabIndex = 211;
            this.text245e.Text = "245e";
            // 
            // text700a
            // 
            this.text700a.Location = new System.Drawing.Point(350, 337);
            this.text700a.Name = "text700a";
            this.text700a.Size = new System.Drawing.Size(585, 21);
            this.text700a.TabIndex = 211;
            this.text700a.Text = "700a";
            // 
            // text900a
            // 
            this.text900a.Location = new System.Drawing.Point(350, 359);
            this.text900a.Name = "text900a";
            this.text900a.Size = new System.Drawing.Size(585, 21);
            this.text900a.TabIndex = 211;
            this.text900a.Text = "900a";
            // 
            // text507a
            // 
            this.text507a.Location = new System.Drawing.Point(941, 345);
            this.text507a.Name = "text507a";
            this.text507a.Size = new System.Drawing.Size(387, 21);
            this.text507a.TabIndex = 211;
            this.text507a.Text = "507a";
            // 
            // GridView710
            // 
            this.GridView710.AllowDrop = true;
            this.GridView710.AllowUserToResizeRows = false;
            this.GridView710.BackgroundColor = System.Drawing.SystemColors.Window;
            this.GridView710.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView710.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column11,
            this.Column12});
            this.GridView710.Location = new System.Drawing.Point(605, 384);
            this.GridView710.Name = "GridView710";
            this.GridView710.RowHeadersWidth = 30;
            this.GridView710.RowTemplate.Height = 23;
            this.GridView710.Size = new System.Drawing.Size(360, 71);
            this.GridView710.TabIndex = 208;
            this.GridView710.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.GridView710.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragDrop);
            this.GridView710.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragOver);
            this.GridView710.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseDown);
            this.GridView710.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseMove);
            // 
            // Column11
            // 
            this.Column11.HeaderText = "710a";
            this.Column11.Name = "Column11";
            this.Column11.Width = 150;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "710b";
            this.Column12.Name = "Column12";
            this.Column12.Width = 150;
            // 
            // GridView910
            // 
            this.GridView910.AllowDrop = true;
            this.GridView910.AllowUserToResizeRows = false;
            this.GridView910.BackgroundColor = System.Drawing.SystemColors.Window;
            this.GridView910.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView910.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.GridView910.Location = new System.Drawing.Point(968, 384);
            this.GridView910.Name = "GridView910";
            this.GridView910.RowHeadersWidth = 30;
            this.GridView910.RowTemplate.Height = 23;
            this.GridView910.Size = new System.Drawing.Size(360, 71);
            this.GridView910.TabIndex = 208;
            this.GridView910.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.GridView910.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragDrop);
            this.GridView910.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragOver);
            this.GridView910.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseDown);
            this.GridView910.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseMove);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "910a";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "910b";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // text260a
            // 
            this.text260a.Location = new System.Drawing.Point(350, 384);
            this.text260a.Name = "text260a";
            this.text260a.Size = new System.Drawing.Size(96, 21);
            this.text260a.TabIndex = 210;
            this.text260a.Text = "260a";
            // 
            // text260b
            // 
            this.text260b.Location = new System.Drawing.Point(350, 409);
            this.text260b.Name = "text260b";
            this.text260b.Size = new System.Drawing.Size(96, 21);
            this.text260b.TabIndex = 210;
            this.text260b.Text = "260b";
            // 
            // text260c
            // 
            this.text260c.Location = new System.Drawing.Point(350, 434);
            this.text260c.Name = "text260c";
            this.text260c.Size = new System.Drawing.Size(96, 21);
            this.text260c.TabIndex = 210;
            this.text260c.Text = "260c";
            // 
            // text260g
            // 
            this.text260g.Location = new System.Drawing.Point(350, 459);
            this.text260g.Name = "text260g";
            this.text260g.Size = new System.Drawing.Size(96, 21);
            this.text260g.TabIndex = 210;
            this.text260g.Text = "260g";
            // 
            // text300a
            // 
            this.text300a.Location = new System.Drawing.Point(476, 384);
            this.text300a.Name = "text300a";
            this.text300a.Size = new System.Drawing.Size(126, 21);
            this.text300a.TabIndex = 210;
            this.text300a.Text = "300a";
            // 
            // text300b
            // 
            this.text300b.Location = new System.Drawing.Point(476, 409);
            this.text300b.Name = "text300b";
            this.text300b.Size = new System.Drawing.Size(126, 21);
            this.text300b.TabIndex = 210;
            this.text300b.Text = "300b";
            // 
            // text536a
            // 
            this.text536a.Location = new System.Drawing.Point(350, 694);
            this.text536a.Name = "text536a";
            this.text536a.Size = new System.Drawing.Size(487, 21);
            this.text536a.TabIndex = 210;
            this.text536a.Text = "536a";
            // 
            // text586a
            // 
            this.text586a.Location = new System.Drawing.Point(350, 719);
            this.text586a.Name = "text586a";
            this.text586a.Size = new System.Drawing.Size(487, 21);
            this.text586a.TabIndex = 210;
            this.text586a.Text = "586a";
            // 
            // GridView440
            // 
            this.GridView440.AllowDrop = true;
            this.GridView440.AllowUserToResizeRows = false;
            this.GridView440.BackgroundColor = System.Drawing.SystemColors.Window;
            this.GridView440.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView440.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.text440a,
            this.text440n,
            this.text440p,
            this.text440v,
            this.Column13,
            this.text440x});
            this.GridView440.Location = new System.Drawing.Point(350, 486);
            this.GridView440.Name = "GridView440";
            this.GridView440.RowHeadersWidth = 30;
            this.GridView440.RowTemplate.Height = 23;
            this.GridView440.Size = new System.Drawing.Size(610, 71);
            this.GridView440.TabIndex = 208;
            this.GridView440.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.GridView440.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragDrop);
            this.GridView440.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragOver);
            this.GridView440.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseDown);
            this.GridView440.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseMove);
            // 
            // text440a
            // 
            this.text440a.HeaderText = "440a";
            this.text440a.Name = "text440a";
            // 
            // text440n
            // 
            this.text440n.HeaderText = "440n";
            this.text440n.Name = "text440n";
            this.text440n.Width = 50;
            // 
            // text440p
            // 
            this.text440p.HeaderText = "440p";
            this.text440p.Name = "text440p";
            // 
            // text440v
            // 
            this.text440v.HeaderText = "440v [숫자]";
            this.text440v.Name = "text440v";
            // 
            // Column13
            // 
            this.Column13.HeaderText = "440v [문자]";
            this.Column13.Name = "Column13";
            // 
            // text440x
            // 
            this.text440x.HeaderText = "440x";
            this.text440x.Name = "text440x";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(446, 389);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "링크";
            // 
            // text525a
            // 
            this.text525a.Location = new System.Drawing.Point(605, 461);
            this.text525a.Name = "text525a";
            this.text525a.Size = new System.Drawing.Size(360, 21);
            this.text525a.TabIndex = 211;
            this.text525a.Text = "525a";
            // 
            // text653a
            // 
            this.text653a.Location = new System.Drawing.Point(968, 461);
            this.text653a.Name = "text653a";
            this.text653a.Size = new System.Drawing.Size(360, 21);
            this.text653a.TabIndex = 211;
            this.text653a.Text = "653a";
            // 
            // GridView490
            // 
            this.GridView490.AllowDrop = true;
            this.GridView490.AllowUserToResizeRows = false;
            this.GridView490.BackgroundColor = System.Drawing.SystemColors.Window;
            this.GridView490.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView490.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.text490a,
            this.text490v});
            this.GridView490.Location = new System.Drawing.Point(968, 486);
            this.GridView490.Name = "GridView490";
            this.GridView490.RowHeadersWidth = 30;
            this.GridView490.RowTemplate.Height = 23;
            this.GridView490.Size = new System.Drawing.Size(360, 71);
            this.GridView490.TabIndex = 208;
            this.GridView490.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.GridView490.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragDrop);
            this.GridView490.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragOver);
            this.GridView490.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseDown);
            this.GridView490.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseMove);
            // 
            // text490a
            // 
            this.text490a.HeaderText = "490a";
            this.text490a.Name = "text490a";
            this.text490a.Width = 150;
            // 
            // text490v
            // 
            this.text490v.HeaderText = "490v";
            this.text490v.Name = "text490v";
            this.text490v.Width = 150;
            // 
            // text500a
            // 
            this.text500a.Location = new System.Drawing.Point(350, 563);
            this.text500a.Multiline = true;
            this.text500a.Name = "text500a";
            this.text500a.Size = new System.Drawing.Size(487, 48);
            this.text500a.TabIndex = 211;
            this.text500a.Text = "500a";
            // 
            // GridView505
            // 
            this.GridView505.AllowDrop = true;
            this.GridView505.AllowUserToResizeRows = false;
            this.GridView505.BackgroundColor = System.Drawing.SystemColors.Window;
            this.GridView505.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView505.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.text505n,
            this.text505t,
            this.text505d,
            this.text505e});
            this.GridView505.Location = new System.Drawing.Point(350, 617);
            this.GridView505.Name = "GridView505";
            this.GridView505.RowHeadersWidth = 30;
            this.GridView505.RowTemplate.Height = 23;
            this.GridView505.Size = new System.Drawing.Size(410, 71);
            this.GridView505.TabIndex = 208;
            this.GridView505.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.GridView505.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragDrop);
            this.GridView505.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView_DragOver);
            this.GridView505.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseDown);
            this.GridView505.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseMove);
            // 
            // text505n
            // 
            this.text505n.HeaderText = "505n";
            this.text505n.Name = "text505n";
            this.text505n.Width = 50;
            // 
            // text505t
            // 
            this.text505t.HeaderText = "505t";
            this.text505t.Name = "text505t";
            // 
            // text505d
            // 
            this.text505d.HeaderText = "505d";
            this.text505d.Name = "text505d";
            // 
            // text505e
            // 
            this.text505e.HeaderText = "505e";
            this.text505e.Name = "text505e";
            // 
            // text504a
            // 
            this.text504a.Location = new System.Drawing.Point(766, 617);
            this.text504a.Name = "text504a";
            this.text504a.Size = new System.Drawing.Size(562, 21);
            this.text504a.TabIndex = 210;
            this.text504a.Text = "504a";
            // 
            // text505a
            // 
            this.text505a.Location = new System.Drawing.Point(766, 642);
            this.text505a.Multiline = true;
            this.text505a.Name = "text505a";
            this.text505a.Size = new System.Drawing.Size(562, 46);
            this.text505a.TabIndex = 210;
            this.text505a.Text = "505a";
            // 
            // text650a
            // 
            this.text650a.Location = new System.Drawing.Point(841, 694);
            this.text650a.Name = "text650a";
            this.text650a.Size = new System.Drawing.Size(487, 21);
            this.text650a.TabIndex = 210;
            this.text650a.Text = "650a";
            // 
            // text521a
            // 
            this.text521a.Location = new System.Drawing.Point(841, 719);
            this.text521a.Name = "text521a";
            this.text521a.Size = new System.Drawing.Size(487, 21);
            this.text521a.TabIndex = 210;
            this.text521a.Text = "521a";
            // 
            // text520a
            // 
            this.text520a.Location = new System.Drawing.Point(842, 563);
            this.text520a.Multiline = true;
            this.text520a.Name = "text520a";
            this.text520a.Size = new System.Drawing.Size(486, 48);
            this.text520a.TabIndex = 210;
            this.text520a.Text = "520a";
            // 
            // text300c1
            // 
            this.text300c1.Location = new System.Drawing.Point(476, 434);
            this.text300c1.Name = "text300c1";
            this.text300c1.Size = new System.Drawing.Size(60, 21);
            this.text300c1.TabIndex = 210;
            this.text300c1.Text = "c세로";
            // 
            // text300e
            // 
            this.text300e.Location = new System.Drawing.Point(476, 459);
            this.text300e.Name = "text300e";
            this.text300e.Size = new System.Drawing.Size(126, 21);
            this.text300e.TabIndex = 210;
            this.text300e.Text = "300e";
            // 
            // text300c2
            // 
            this.text300c2.Location = new System.Drawing.Point(542, 434);
            this.text300c2.Name = "text300c2";
            this.text300c2.Size = new System.Drawing.Size(60, 21);
            this.text300c2.TabIndex = 210;
            this.text300c2.Text = "c가로";
            // 
            // col008res
            // 
            this.col008res.AutoSize = true;
            this.col008res.ForeColor = System.Drawing.Color.Blue;
            this.col008res.Location = new System.Drawing.Point(1171, 18);
            this.col008res.Name = "col008res";
            this.col008res.Size = new System.Drawing.Size(13, 12);
            this.col008res.TabIndex = 206;
            this.col008res.Text = "  ";
            // 
            // gov008res
            // 
            this.gov008res.AutoSize = true;
            this.gov008res.ForeColor = System.Drawing.Color.Blue;
            this.gov008res.Location = new System.Drawing.Point(1100, 47);
            this.gov008res.Name = "gov008res";
            this.gov008res.Size = new System.Drawing.Size(13, 12);
            this.gov008res.TabIndex = 206;
            this.gov008res.Text = "  ";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(350, 46);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(112, 16);
            this.checkBox1.TabIndex = 213;
            this.checkBox1.Text = "회의간행물 여부";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(474, 46);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(112, 16);
            this.checkBox2.TabIndex = 213;
            this.checkBox2.Text = "기념논문집 여부";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // comboBox7
            // 
            this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(976, 14);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(118, 20);
            this.comboBox7.TabIndex = 207;
            this.comboBox7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // Btn_Helper
            // 
            this.Btn_Helper.Location = new System.Drawing.Point(1203, 17);
            this.Btn_Helper.Name = "Btn_Helper";
            this.Btn_Helper.Size = new System.Drawing.Size(75, 23);
            this.Btn_Helper.TabIndex = 215;
            this.Btn_Helper.Text = "도움말";
            this.Btn_Helper.UseVisualStyleBackColor = true;
            this.Btn_Helper.Click += new System.EventHandler(this.Btn_Helper_Click);
            // 
            // Btn_interlock
            // 
            this.Btn_interlock.Location = new System.Drawing.Point(1073, 109);
            this.Btn_interlock.Name = "Btn_interlock";
            this.Btn_interlock.Size = new System.Drawing.Size(137, 23);
            this.Btn_interlock.TabIndex = 216;
            this.Btn_interlock.Text = "태그연동 (041-546)";
            this.Btn_interlock.UseVisualStyleBackColor = true;
            this.Btn_interlock.Click += new System.EventHandler(this.Btn_interlock_Click);
            // 
            // dataGridView8
            // 
            this.dataGridView8.AllowUserToResizeColumns = false;
            this.dataGridView8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20});
            this.dataGridView8.Location = new System.Drawing.Point(2, 14);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.RowHeadersVisible = false;
            this.dataGridView8.RowHeadersWidth = 10;
            this.dataGridView8.RowTemplate.Height = 23;
            this.dataGridView8.Size = new System.Drawing.Size(340, 726);
            this.dataGridView8.TabIndex = 217;
            this.dataGridView8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView8_KeyDown);
            // 
            // Column14
            // 
            this.Column14.HeaderText = "N";
            this.Column14.Name = "Column14";
            this.Column14.Width = 25;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "S";
            this.Column15.Name = "Column15";
            this.Column15.Width = 25;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "도서명";
            this.Column16.Name = "Column16";
            // 
            // Column17
            // 
            this.Column17.HeaderText = "저자";
            this.Column17.Name = "Column17";
            this.Column17.Width = 50;
            // 
            // Column18
            // 
            this.Column18.HeaderText = "출판사";
            this.Column18.Name = "Column18";
            this.Column18.Width = 50;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "정가";
            this.Column19.Name = "Column19";
            this.Column19.Width = 50;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "비고";
            this.Column20.Name = "Column20";
            // 
            // Marc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1630, 753);
            this.Controls.Add(this.dataGridView8);
            this.Controls.Add(this.Btn_interlock);
            this.Controls.Add(this.Btn_Helper);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.text940a);
            this.Controls.Add(this.text507a);
            this.Controls.Add(this.text250a);
            this.Controls.Add(this.text245p);
            this.Controls.Add(this.text245e);
            this.Controls.Add(this.text500a);
            this.Controls.Add(this.text653a);
            this.Controls.Add(this.text900a);
            this.Controls.Add(this.text525a);
            this.Controls.Add(this.text700a);
            this.Controls.Add(this.text245d);
            this.Controls.Add(this.text245n);
            this.Controls.Add(this.text245x);
            this.Controls.Add(this.text245b);
            this.Controls.Add(this.text245a);
            this.Controls.Add(this.textDDC23);
            this.Controls.Add(this.textDDC22);
            this.Controls.Add(this.textDDC21);
            this.Controls.Add(this.text260g);
            this.Controls.Add(this.text260c);
            this.Controls.Add(this.text260b);
            this.Controls.Add(this.text521a);
            this.Controls.Add(this.text520a);
            this.Controls.Add(this.text586a);
            this.Controls.Add(this.text650a);
            this.Controls.Add(this.text536a);
            this.Controls.Add(this.text505a);
            this.Controls.Add(this.text300e);
            this.Controls.Add(this.text300b);
            this.Controls.Add(this.text504a);
            this.Controls.Add(this.text300c2);
            this.Controls.Add(this.text300c1);
            this.Controls.Add(this.text300a);
            this.Controls.Add(this.text260a);
            this.Controls.Add(this.textKDC6);
            this.Controls.Add(this.textKDC5);
            this.Controls.Add(this.textKDC4);
            this.Controls.Add(this.text546a);
            this.Controls.Add(this.text041k);
            this.Controls.Add(this.text041h);
            this.Controls.Add(this.text041b);
            this.Controls.Add(this.text041a);
            this.Controls.Add(this.GridView910);
            this.Controls.Add(this.GridView710);
            this.Controls.Add(this.GridView490);
            this.Controls.Add(this.GridView505);
            this.Controls.Add(this.GridView440);
            this.Controls.Add(this.GridView246);
            this.Controls.Add(this.GridView020);
            this.Controls.Add(this.comboBox6);
            this.Controls.Add(this.comboBox5);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.comboBox7);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.col008res);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.gov008res);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.text008col);
            this.Controls.Add(this.text008gov);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richTextBox3);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "Marc";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Marc_Load);
            this.Shown += new System.EventHandler(this.Marc_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.GridView020)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView246)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView710)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView910)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView440)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView490)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView505)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.RichTextBox richTextBox1;
        public System.Windows.Forms.RichTextBox richTextBox2;
        public System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.DataGridView GridView020;
        private System.Windows.Forms.TextBox text041a;
        private System.Windows.Forms.TextBox text041b;
        private System.Windows.Forms.TextBox text041h;
        private System.Windows.Forms.TextBox text041k;
        private System.Windows.Forms.TextBox text546a;
        private System.Windows.Forms.TextBox textKDC4;
        private System.Windows.Forms.TextBox textDDC21;
        private System.Windows.Forms.TextBox textKDC5;
        private System.Windows.Forms.TextBox textDDC22;
        private System.Windows.Forms.TextBox textKDC6;
        private System.Windows.Forms.TextBox textDDC23;
        private System.Windows.Forms.TextBox text245a;
        private System.Windows.Forms.TextBox text940a;
        private System.Windows.Forms.TextBox text245b;
        private System.Windows.Forms.TextBox text245x;
        private System.Windows.Forms.TextBox text250a;
        private System.Windows.Forms.TextBox text245n;
        private System.Windows.Forms.TextBox text245p;
        private System.Windows.Forms.DataGridView GridView246;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox text245d;
        private System.Windows.Forms.TextBox text245e;
        private System.Windows.Forms.TextBox text700a;
        private System.Windows.Forms.TextBox text900a;
        private System.Windows.Forms.TextBox text507a;
        private System.Windows.Forms.DataGridView GridView710;
        private System.Windows.Forms.DataGridView GridView910;
        private System.Windows.Forms.TextBox text260a;
        private System.Windows.Forms.TextBox text260b;
        private System.Windows.Forms.TextBox text260c;
        private System.Windows.Forms.TextBox text260g;
        private System.Windows.Forms.TextBox text300a;
        private System.Windows.Forms.TextBox text300b;
        private System.Windows.Forms.TextBox text536a;
        private System.Windows.Forms.TextBox text586a;
        private System.Windows.Forms.DataGridView GridView440;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox text525a;
        private System.Windows.Forms.TextBox text653a;
        private System.Windows.Forms.DataGridView GridView490;
        private System.Windows.Forms.TextBox text500a;
        private System.Windows.Forms.DataGridView GridView505;
        private System.Windows.Forms.DataGridViewTextBoxColumn text505n;
        private System.Windows.Forms.DataGridViewTextBoxColumn text505t;
        private System.Windows.Forms.DataGridViewTextBoxColumn text505d;
        private System.Windows.Forms.DataGridViewTextBoxColumn text505e;
        private System.Windows.Forms.TextBox text504a;
        private System.Windows.Forms.TextBox text505a;
        private System.Windows.Forms.TextBox text650a;
        private System.Windows.Forms.TextBox text521a;
        private System.Windows.Forms.TextBox text520a;
        private System.Windows.Forms.CheckBox invertCheck;
        private System.Windows.Forms.TextBox text300c1;
        private System.Windows.Forms.TextBox text300e;
        private System.Windows.Forms.DataGridViewTextBoxColumn text490a;
        private System.Windows.Forms.DataGridViewTextBoxColumn text490v;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.TextBox basicHeadBox;
        private System.Windows.Forms.TextBox text300c2;
        public System.Windows.Forms.TextBox text008col;
        public System.Windows.Forms.TextBox text008gov;
        public System.Windows.Forms.Label col008res;
        public System.Windows.Forms.Label gov008res;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440a;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440n;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440p;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440v;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440x;
        private System.Windows.Forms.Button Btn_Helper;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.Button Btn_interlock;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
    }
}