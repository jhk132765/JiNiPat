﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelTest
{
    public partial class findNchange : Form
    {
        public bool buttonClick1 = false;
        public bool buttonClick2 = false;
        string find = "";
        string change = "";
        Marc mac;
        public findNchange(Marc _mac)
        {
            InitializeComponent();
            mac = _mac;
        }

        private void findNchange_Load(object sender, EventArgs e)
        {
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buttonClick1 = true;
            find = this.textBox1.Text;
            change = this.textBox2.Text;
            textChange(find, change);
            mac.Color_change("▼");
            mac.Color_change("▲");
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            buttonClick2 = true;
            Close();
        }

        public void textChange(string ftext, string ctext)
        {
            var temp="";
            if (mac.call == "richTextBox1")
            {
                //mac.richTextBox1.Text = mac.richTextBox1.Text.Replace(ftext, ctext);
                temp = mac.Controls[mac.call].Text.Replace(ftext, ctext);
                mac.Controls[mac.call].Text = temp;
            }
            else
            {
                // mac.text245e.Text = mac.text245e.Text.Replace(ftext, ctext);
                mac.Controls[mac.call].Text = mac.Controls[mac.call].Text.Replace(ftext, ctext);
            }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }
    }
}
