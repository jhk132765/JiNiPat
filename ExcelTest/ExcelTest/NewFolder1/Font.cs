﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelTest
{
    public partial class Font : Form
    {
        public Font()
        {
            InitializeComponent();
            this.Load += Form1_Load;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // richTextBox1.LanguageOption = RichTextBoxLanguageOptions.UIFonts;
            foreach(FontFamily oneFontFamily in FontFamily.Families)
            {
                comboBox1.Items.Add(oneFontFamily.Name);
            }
            comboBox1.Text = this.richTextBox1.Font.Name.ToString();
            comboBox2.Text = this.richTextBox1.Font.Size.ToString();

            richTextBox1.Focus();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //richTextBox1.SelectionFont = new Font(comboBox1.Text, 12, FontStyle.Bold);
            
            label1.Text = comboBox1.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}