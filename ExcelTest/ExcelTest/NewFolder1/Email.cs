﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelTest
{
    public partial class Email : Form
    {
        Mail mail = new Mail("jhk132765@naver.com");
        public Email()
        {
            InitializeComponent();
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string toAddress = this.textBox1.Text;
            string subject = this.textBox2.Text;
            string body = this.richTextBox1.Text;
            if(toAddress=="")
            {
                MessageBox.Show("받을 메일을 입력해 주세요.");
            }
            if (subject == "")
            {
                MessageBox.Show("메일 제목을 입력해 주세요.");
            }
            if (body == "")
            {
                MessageBox.Show("메일 내용을 입력해 주세요.");
            }
            mail.SetToAddress(toAddress);
            MessageBox.Show(mail.SendEmail(subject, body));
        }
    }
}
