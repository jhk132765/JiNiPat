﻿namespace ExcelTest
{
    partial class SubString
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lab_Regex_1 = new System.Windows.Forms.Label();
            this.lab_Regex_2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_1_1 = new System.Windows.Forms.TextBox();
            this.tb_2_1 = new System.Windows.Forms.TextBox();
            this.tb_3_1 = new System.Windows.Forms.TextBox();
            this.tb_4_1 = new System.Windows.Forms.TextBox();
            this.tb_1_2 = new System.Windows.Forms.TextBox();
            this.tb_2_2 = new System.Windows.Forms.TextBox();
            this.tb_3_2 = new System.Windows.Forms.TextBox();
            this.tb_4_2 = new System.Windows.Forms.TextBox();
            this.tb_1_3 = new System.Windows.Forms.TextBox();
            this.tb_2_3 = new System.Windows.Forms.TextBox();
            this.tb_3_3 = new System.Windows.Forms.TextBox();
            this.tb_4_3 = new System.Windows.Forms.TextBox();
            this.btn_Regex = new System.Windows.Forms.Button();
            this.btn_Substring = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lab_Regex_1
            // 
            this.lab_Regex_1.AutoSize = true;
            this.lab_Regex_1.Location = new System.Drawing.Point(197, 114);
            this.lab_Regex_1.Name = "lab_Regex_1";
            this.lab_Regex_1.Size = new System.Drawing.Size(45, 12);
            this.lab_Regex_1.TabIndex = 0;
            this.lab_Regex_1.Text = "추출 전";
            // 
            // lab_Regex_2
            // 
            this.lab_Regex_2.AutoSize = true;
            this.lab_Regex_2.Location = new System.Drawing.Point(377, 114);
            this.lab_Regex_2.Name = "lab_Regex_2";
            this.lab_Regex_2.Size = new System.Drawing.Size(45, 12);
            this.lab_Regex_2.TabIndex = 0;
            this.lab_Regex_2.Text = "추출 후";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(551, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "부분 출력";
            // 
            // tb_1_1
            // 
            this.tb_1_1.Location = new System.Drawing.Point(169, 140);
            this.tb_1_1.Name = "tb_1_1";
            this.tb_1_1.Size = new System.Drawing.Size(100, 21);
            this.tb_1_1.TabIndex = 1;
            // 
            // tb_2_1
            // 
            this.tb_2_1.Location = new System.Drawing.Point(169, 167);
            this.tb_2_1.Name = "tb_2_1";
            this.tb_2_1.Size = new System.Drawing.Size(100, 21);
            this.tb_2_1.TabIndex = 1;
            // 
            // tb_3_1
            // 
            this.tb_3_1.Location = new System.Drawing.Point(169, 194);
            this.tb_3_1.Name = "tb_3_1";
            this.tb_3_1.Size = new System.Drawing.Size(100, 21);
            this.tb_3_1.TabIndex = 1;
            // 
            // tb_4_1
            // 
            this.tb_4_1.Location = new System.Drawing.Point(169, 221);
            this.tb_4_1.Name = "tb_4_1";
            this.tb_4_1.Size = new System.Drawing.Size(100, 21);
            this.tb_4_1.TabIndex = 1;
            // 
            // tb_1_2
            // 
            this.tb_1_2.Location = new System.Drawing.Point(349, 140);
            this.tb_1_2.Name = "tb_1_2";
            this.tb_1_2.Size = new System.Drawing.Size(100, 21);
            this.tb_1_2.TabIndex = 1;
            // 
            // tb_2_2
            // 
            this.tb_2_2.Location = new System.Drawing.Point(349, 167);
            this.tb_2_2.Name = "tb_2_2";
            this.tb_2_2.Size = new System.Drawing.Size(100, 21);
            this.tb_2_2.TabIndex = 1;
            // 
            // tb_3_2
            // 
            this.tb_3_2.Location = new System.Drawing.Point(349, 194);
            this.tb_3_2.Name = "tb_3_2";
            this.tb_3_2.Size = new System.Drawing.Size(100, 21);
            this.tb_3_2.TabIndex = 1;
            // 
            // tb_4_2
            // 
            this.tb_4_2.Location = new System.Drawing.Point(349, 221);
            this.tb_4_2.Name = "tb_4_2";
            this.tb_4_2.Size = new System.Drawing.Size(100, 21);
            this.tb_4_2.TabIndex = 1;
            // 
            // tb_1_3
            // 
            this.tb_1_3.Location = new System.Drawing.Point(529, 140);
            this.tb_1_3.Name = "tb_1_3";
            this.tb_1_3.Size = new System.Drawing.Size(100, 21);
            this.tb_1_3.TabIndex = 1;
            // 
            // tb_2_3
            // 
            this.tb_2_3.Location = new System.Drawing.Point(529, 167);
            this.tb_2_3.Name = "tb_2_3";
            this.tb_2_3.Size = new System.Drawing.Size(100, 21);
            this.tb_2_3.TabIndex = 1;
            // 
            // tb_3_3
            // 
            this.tb_3_3.Location = new System.Drawing.Point(529, 194);
            this.tb_3_3.Name = "tb_3_3";
            this.tb_3_3.Size = new System.Drawing.Size(100, 21);
            this.tb_3_3.TabIndex = 1;
            // 
            // tb_4_3
            // 
            this.tb_4_3.Location = new System.Drawing.Point(529, 221);
            this.tb_4_3.Name = "tb_4_3";
            this.tb_4_3.Size = new System.Drawing.Size(100, 21);
            this.tb_4_3.TabIndex = 1;
            // 
            // btn_Regex
            // 
            this.btn_Regex.Location = new System.Drawing.Point(255, 262);
            this.btn_Regex.Name = "btn_Regex";
            this.btn_Regex.Size = new System.Drawing.Size(75, 23);
            this.btn_Regex.TabIndex = 2;
            this.btn_Regex.Text = "추출";
            this.btn_Regex.UseVisualStyleBackColor = true;
            this.btn_Regex.Click += new System.EventHandler(this.btn_Regex_Click);
            // 
            // btn_Substring
            // 
            this.btn_Substring.Location = new System.Drawing.Point(451, 262);
            this.btn_Substring.Name = "btn_Substring";
            this.btn_Substring.Size = new System.Drawing.Size(75, 23);
            this.btn_Substring.TabIndex = 2;
            this.btn_Substring.Text = "부분추출";
            this.btn_Substring.UseVisualStyleBackColor = true;
            this.btn_Substring.Click += new System.EventHandler(this.btn_Substring_Click);
            // 
            // SubString
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_Substring);
            this.Controls.Add(this.btn_Regex);
            this.Controls.Add(this.tb_4_3);
            this.Controls.Add(this.tb_4_2);
            this.Controls.Add(this.tb_4_1);
            this.Controls.Add(this.tb_3_3);
            this.Controls.Add(this.tb_3_2);
            this.Controls.Add(this.tb_3_1);
            this.Controls.Add(this.tb_2_3);
            this.Controls.Add(this.tb_2_2);
            this.Controls.Add(this.tb_2_1);
            this.Controls.Add(this.tb_1_3);
            this.Controls.Add(this.tb_1_2);
            this.Controls.Add(this.tb_1_1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lab_Regex_2);
            this.Controls.Add(this.lab_Regex_1);
            this.Name = "SubString";
            this.Text = "SubString";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lab_Regex_1;
        private System.Windows.Forms.Label lab_Regex_2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_1_1;
        private System.Windows.Forms.TextBox tb_2_1;
        private System.Windows.Forms.TextBox tb_3_1;
        private System.Windows.Forms.TextBox tb_4_1;
        private System.Windows.Forms.TextBox tb_1_2;
        private System.Windows.Forms.TextBox tb_2_2;
        private System.Windows.Forms.TextBox tb_3_2;
        private System.Windows.Forms.TextBox tb_4_2;
        private System.Windows.Forms.TextBox tb_1_3;
        private System.Windows.Forms.TextBox tb_2_3;
        private System.Windows.Forms.TextBox tb_3_3;
        private System.Windows.Forms.TextBox tb_4_3;
        private System.Windows.Forms.Button btn_Regex;
        private System.Windows.Forms.Button btn_Substring;
    }
}