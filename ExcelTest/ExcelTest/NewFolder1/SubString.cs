﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ExcelTest
{
    public partial class SubString : Form
    {
        public SubString()
        {
            InitializeComponent();
            tb_Clear();
        }

        private void tb_Clear()
        {
            tb_1_1.Text = "2017.02.01";
            tb_2_1.Text = "17-02-01";
            tb_3_1.Text = "2017년02월01일";
            tb_4_1.Text = "17 02 01";
        }

        private void btn_Regex_Click(object sender, EventArgs e)
        {
            string StrTemp1 = Regex.Replace(tb_1_1.Text, @"\D", "");
            string StrTemp2 = Regex.Replace(tb_2_1.Text, @"\D", "");
            string StrTemp3 = Regex.Replace(tb_3_1.Text, @"\D", "");
            string StrTemp4 = Regex.Replace(tb_4_1.Text, @"\D", "");

            tb_1_2.Text = StrTemp1;
            tb_2_2.Text = StrTemp2;
            tb_3_2.Text = StrTemp3;
            tb_4_2.Text = StrTemp4;
        }

        private void btn_Substring_Click(object sender, EventArgs e)
        {
            if((tb_1_2.Text != "") && (tb_1_2.Text != "") &&(tb_1_2.Text!="") && (tb_1_2.Text != ""))
            {
                tb_1_3.Text = tb_1_2.Text.Substring(2);
                tb_2_3.Text = tb_2_2.Text.Substring(2, 4);
                tb_3_3.Text = tb_3_2.Text.Substring(2, 6);
                tb_4_3.Text = tb_4_2.Text.Substring(2, 3);
            }
        }
    }
}
