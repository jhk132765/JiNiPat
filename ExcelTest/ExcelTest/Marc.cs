﻿using Microsoft.Office.Interop.Excel;
using ServerText;
using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelTest
{
    public partial class Marc : Form
    {
        public string find;
        public string change;
        public string call = "";
        string tbName = "";
        public int checkCout = 0;
        public bool gridViewChk = false;        // True / False
        Help008Tag tag008 = new Help008Tag();
        public Marc()
        {
            InitializeComponent();
        }

        private void Marc_Load(object sender, EventArgs e)
        {
            // ComboBox Item Setting
            // 이용자 9
            string[] combo1 = { "일반", "유아", "아동", "초등", "중학생",
                                "고등학생", "성인용 19금", "특수계층", "미상" };
            comboBox1.Items.AddRange(combo1);
            comboBox1.SelectedIndex = 0;

            // 자료형식 3
            string[] combo2 = { "해당무", "큰 글자", "점자" };
            comboBox2.Items.AddRange(combo2);
            comboBox2.SelectedIndex = 0;

            // 내용형식 28
            string[] combo3 = { "해당무", "만화/그래픽노블", "사전", "백과사전", "평론",
                                "통계자료", "명감", "연감", "족보", "조약, 협정문",
                                "학위논문", "법률논문", "법령집", "판례집 및 판례요약집", "판결보도 및 판결 평석",
                                "별쇄본", "역서(전체)", "음반목록", "영화작품목록", "초록",
                                "서지", "목록", "편람", "색인", "조사보고",
                                "기술보고서", "프로그램화된 텍스트", "표준/표준해설자료"};
            comboBox3.Items.AddRange(combo3);
            comboBox7.Items.AddRange(combo3);
            comboBox3.SelectedIndex = 0;
            comboBox7.SelectedIndex = 0;

            // 문학형식 16
            string[] combo4 = { "해당무", "소설", "추리소설", "단편소설", "시",
                                "수필", "희곡/시나리오", "문집", "기행문/일기문/수기", "평론",
                                "논픽션", "연설문", "논설문", "향가/시조", "풍자문학",
                                "서간문학" };
            comboBox4.Items.AddRange(combo4);
            comboBox4.SelectedIndex = 0;

            // 전기 5
            string[] combo5 = { "해당무", "자서전", "개인전기서", "전기물의 합저서", "전기적 정보가 포함된 자료" };
            comboBox5.Items.AddRange(combo5);
            comboBox5.SelectedIndex = 0;

            // 언어 36
            string[] combo6 = { "한국어", "영어", "일본어", "중국어", "독일어",
                                "프랑스어", "러시아어", "스페인어", "이탈리아어", "네덜란드어",
                                "핀란드어", "스웨덴어", "포르투갈어", "노르웨이어", "그리스어",
                                "체코어", "폴란드어", "다국어", "말레이시아어", "몽골어",
                                "미얀마(버마어)", "베트남어", "슬로베니아어", "아랍어", "아프리카어",
                                "에스토니아어", "우즈베키스탄어", "우크라이나어", "인도(마라티어)", "캄보디아어",
                                "태국어", "터키어", "투르크메니스탄어", "티베르어", "필리핀(타갈로그어)",
                                "헝가리어" };
            comboBox6.Items.AddRange(combo6);
            comboBox6.SelectedIndex = 0;

            radioButton1.Checked = true;         // 기본표목 라디오박스 초기값설정 (개인명)

            GridView020.Rows.Add(200);
            GridView246.Rows.Add(200);
            GridView710.Rows.Add(30);
            GridView910.Rows.Add(30);
            GridView440.Rows.Add(10);
            GridView490.Rows.Add(10);
            GridView505.Rows.Add(200);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string result;
            richTextBox1.Text = "";
            string mid = "\t--\t";
            string Sort = "▼";
            string End = "▲\n";
            string dlatl;
            DataGridView data020 = GridView020;
            DataGridView data246 = GridView246;
            DataGridView data710 = GridView710;
            DataGridView data910 = GridView910;
            DataGridView data440 = GridView440;
            DataGridView data490 = GridView490;
            DataGridView data505 = GridView505;
            string[] data020a = { "", "", "" };
            string[] data246a = { "", "", "", "", "" };
            string[] data710a = { "", "" };
            string[] data910a = { "", "" };
            string[] data440a = { "", "", "", "", "", "" };
            string[] data490a = { "", "" };
            string[] data505a = { "", "", "", "" };
            int count = 0, a, b;

            result = "메모장\n008\t\t200604s20192020ulk      b    000a  kor  ";
            result = Create_008(result);

            {
                // 020 Tag
                for (a = 0; a < data020.RowCount - 1; a++) 
                {
                    gridViewChk = Convert.ToBoolean(data020.Rows[a].Cells[0].Value);
                    for (b = 0; b < 3; b++)
                    {
                        if (b == 0) { data020a[b] = Sort + "a" + data020.Rows[a].Cells[b + 1].FormattedValue.ToString(); }
                        if (b == 1) { data020a[b] = Sort + "g" + data020.Rows[a].Cells[b + 1].FormattedValue.ToString(); }
                        if (b == 2) { data020a[b] = Sort + "c" + data020.Rows[a].Cells[b + 1].FormattedValue.ToString(); }
                        if (data020.Rows[a].Cells[b + 1].FormattedValue.ToString() != "" && count == 0) {
                            if (gridViewChk == false) { result += "020\t--\t"; }
                            else if (gridViewChk == true) { result += "020\t1-\t"; }
                            count = 1;
                        }
                        if (data020.Rows[a].Cells[b + 1].FormattedValue.ToString() != "") {
                            result += data020a[b];
                        }
                    }
                    result += End;
                    count = 0;
                }

                // 041 Tag
                if (text041a.Text != "" && text041a.Text != "041a" || text041k.Text != "" && text041k.Text != "041k" ||
                    text041h.Text != "" && text041h.Text != "041h" || text041b.Text != "" && text041b.Text != "041b") {

                    result += "041" + mid;
                    if (text041a.Text != "" && text041a.Text != "041a") { result += Sort + "a" + text041a.Text; }
                    if (text041b.Text != "" && text041b.Text != "041b") { result += Sort + "b" + text041b.Text; }
                    if (text041h.Text != "" && text041h.Text != "041h") { result += Sort + "h" + text041h.Text; }
                    if (text041k.Text != "" && text041k.Text != "041k") { result += Sort + "k" + text041k.Text; }
                    result += End;
                }

                // 056 Tag _ KDC
                if (textKDC4.Text != "" && textKDC4.Text != "KDC 4") {
                    if (textKDC4.Text != "" && textKDC4.Text != "KDC 4") {
                        result += "056" + mid + Sort + "a" + textKDC4.Text + Sort + "24" + End;
                    }
                }
                if (textKDC5.Text != "" && textKDC5.Text != "KDC 5") {
                    if (textKDC5.Text != "" && textKDC5.Text != "KDC 5") {
                        result += "056" + mid + Sort + "a" + textKDC5.Text + Sort + "25" + End;
                    }
                }
                if (textKDC6.Text != "" && textKDC6.Text != "KDC 6") {
                    if (textKDC6.Text != "" && textKDC6.Text != "KDC 6") {
                        result += "056" + mid + Sort + "a" + textKDC6.Text + Sort + "26" + End;
                    }
                }

                // 082 Tag _ DDC
                if (textDDC21.Text != "" && textDDC21.Text != "DDC 21") {
                    if (textDDC21.Text != "" && textDDC21.Text != "DDC 21") {
                        result += "082" + mid + Sort + "a" + textDDC21.Text + Sort + "221" + End;
                    }
                }
                if (textDDC22.Text != "" && textDDC22.Text != "DDC 22") {
                    if (textDDC22.Text != "" && textDDC22.Text != "DDC 22") {
                        result += "082" + mid + Sort + "a" + textDDC22.Text + Sort + "222" + End;
                    }
                }
                if (textDDC23.Text != "" && textDDC23.Text != "DDC 23") {
                    if (textDDC23.Text != "" && textDDC23.Text != "DDC 23") {
                        result += "082" + mid + Sort + "a" + textDDC23.Text + Sort + "223" + End;
                    }
                }

                // 100 Tag 개인명
                if (radioButton1.Checked == true) {
                    if (basicHeadBox.Text != "" && basicHeadBox.Text != "기본표목") {
                        result += "100" + mid + Sort + "a" + basicHeadBox.Text + End;
                    }
                }

                // 110 Tag 단체명
                if (radioButton2.Checked == true) {
                    if (basicHeadBox.Text != "" && basicHeadBox.Text != "기본표목") {
                        result += "110" + mid + Sort + "a" + basicHeadBox.Text + End;
                    }
                }

                // 111 Tag 회의명
                if (radioButton3.Checked == true) {
                    if (basicHeadBox.Text != "" && basicHeadBox.Text != "기본표목") {
                        result += "111" + mid + Sort + "a" + basicHeadBox.Text + End;
                    }
                }

                // 245 Tag
                if (text245a.Text != "" && text245a.Text != "245a 서명" || text245b.Text != "" && text245b.Text != "245b" ||
                    text245d.Text != "" && text245d.Text != "245d" || text245e.Text != "" && text245e.Text != "245e" ||
                    text245n.Text != "" && text245n.Text != "245n" || text245p.Text != "" && text245p.Text != "245p" ||
                    text245x.Text != "" && text245x.Text != "245x") {

                    result += "245" + mid;
                    if (text245a.Text != "" && text245a.Text != "245a 서명") { result += Sort + "a" + text245a.Text; }
                    if (text245b.Text != "" && text245b.Text != "245b") { result += Sort + "b" + text245b.Text; }
                    if (text245d.Text != "" && text245d.Text != "245d") { result += Sort + "d" + text245d.Text; }
                    if (text245e.Text != "" && text245e.Text != "245e") { result += Sort + "e" + text245e.Text; }
                    if (text245n.Text != "" && text245n.Text != "245n") { result += Sort + "n" + text245n.Text; }
                    if (text245p.Text != "" && text245p.Text != "245p") { result += Sort + "p" + text245p.Text; }
                    if (text245x.Text != "" && text245x.Text != "245x") { result += Sort + "x" + text245x.Text; }
                    result += End;
                }

                // 246 Tag
                for (a = 0; a < data246.RowCount - 1; a++)
                {
                    for (b = 0; b < 5; b++)
                    {
                        if (b == 1) { data246a[b] = Sort + "a" + data246.Rows[a].Cells[b + 1].FormattedValue.ToString(); }
                        if (b == 2) { data246a[b] = Sort + "b" + data246.Rows[a].Cells[b + 1].FormattedValue.ToString(); }
                        if (b == 0) { data246a[b] = Sort + "i" + data246.Rows[a].Cells[b + 1].FormattedValue.ToString(); }
                        if (b == 3) { data246a[b] = Sort + "n" + data246.Rows[a].Cells[b + 1].FormattedValue.ToString(); }
                        if (b == 4) { data246a[b] = Sort + "p" + data246.Rows[a].Cells[b + 1].FormattedValue.ToString(); }
                        
                        if (data246.Rows[a].Cells[b + 1].FormattedValue.ToString() != "" && count == 0) {

                            dlatl = data246.Rows[a].Cells[0].FormattedValue.ToString();
                            result += "246";
                            if (dlatl == "A") { result += mid; }
                            else { result += "\t" + dlatl + " \t"; }
                            count = 1;
                        }
                        if (data246.Rows[a].Cells[b + 1].FormattedValue.ToString() != "") {
                            result += data246a[b];
                        }
                    }
                    result += End;
                    count = 0;
                }

                // 250 Tag
                if (text250a.Text != "" && text250a.Text != "250a") { result += "250" + mid + Sort + "a" + text250a.Text + End; }

                // 260 Tag
                if (text260a.Text != "" && text260a.Text != "260a" || text260b.Text != "" && text260b.Text != "260b" ||
                    text260c.Text != "" && text260c.Text != "260c" || text260g.Text != "" && text260g.Text != "260g") {

                    result += "260" + mid;
                    if (text260a.Text != "" && text260a.Text != "260a") { result += Sort + "a" + text260a.Text; }
                    if (text260b.Text != "" && text260b.Text != "260b")  {
                        if (text260b.Text.Contains(Sort) == true) { 
                            dlatl = text260b.Text.Replace(Sort, Sort + "b");
                            result += dlatl;
                        }
                        else { result += Sort + "b" + text260b.Text; }
                    }
                    if (text260c.Text != "" && text260c.Text != "260c") { result += Sort + "c" + text260c.Text; }
                    if (text260g.Text != "" && text260g.Text != "260g") { result += Sort + "g" + text260g.Text; }
                    result += End;
                }

                // 300 Tag
                if (text300a.Text != "" && text300a.Text != "300a" || text300b.Text != "" && text300b.Text != "300b" ||
                    text300c1.Text != "" && text300c1.Text != "c세로" || text300c2.Text != "" && text300c2.Text != "c가로" ||
                    text300e.Text != "" && text300e.Text != "300e") {

                    result += "300" + mid;
                    if (text300a.Text != "" && text300a.Text != "300a") { result += Sort + "a" + text300a.Text; }
                    if (text300b.Text != "" && text300b.Text != "300b") { result += Sort + "b" + text300b.Text; }
                    if (text300c1.Text != "" && text300c1.Text != "c세로") { result += Sort + "c" + text300c1.Text; }
                    if (text300c2.Text != "" && text300c2.Text != "c가로") { result += "x" + text300c2.Text; }
                    if (text300e.Text != "" && text300e.Text != "300e") { result += Sort + "e" + text300e.Text; }
                    result += End;
                }

                // 440 Tag
                for (a = 0; a < data440.RowCount - 1; a++)
                {
                    for (b = 0; b < 6; b++)
                    {
                        if (b == 0) { data440a[b] = Sort + "a" + data440.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 1) { data440a[b] = Sort + "n" + data440.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 2) { data440a[b] = Sort + "p" + data440.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 3) { data440a[b] = Sort + "v" + data440.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 4)
                        {
                            if (data440.Rows[a].Cells[3].FormattedValue.ToString() == "") { data440a[b] = Sort + "v" + data440.Rows[a].Cells[b].FormattedValue.ToString(); }
                            else { data440a[b] = " - " + data440.Rows[a].Cells[b].FormattedValue.ToString(); }
                        }
                        if (b == 5) { data440a[b] = Sort + "x" + data440.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (data440.Rows[a].Cells[b].FormattedValue.ToString() != "" && count == 0) {
                            result += "440" + mid;
                            count = 1;
                        }
                        if (data440.Rows[a].Cells[b].FormattedValue.ToString() != "") {
                            result += data440a[b];
                        }
                    }
                    result += End;
                    count = 0;
                }

                // 490 Tag
                for (a = 0; a < data490.RowCount - 1; a++)
                {
                    for (b = 0; b < 2; b++)
                    {
                        if (b == 0) { data490a[b] = Sort + "a" + data490.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 1) { data490a[b] = Sort + "v" + data490.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (data490.Rows[a].Cells[b].FormattedValue.ToString() != "" && count == 0) {
                            result += "490" + mid;
                            count = 1;
                        }
                        if (data490.Rows[a].Cells[b].FormattedValue.ToString() != "") {
                            result += data490a[b];
                        }
                    }
                    result += End;
                    count = 0;
                }

                // 500 Tag
                if (text500a.Text != "" && text500a.Text != "500a") {
                    string delEnt = text500a.Text.Replace("\r\n", "");
                    result += "500" + mid + Sort + "a" + delEnt + End;
                }

                // 504 Tag
                if (text504a.Text != "" && text504a.Text != "504a") { 
                    result += "504" + mid + Sort + "a" + text504a.Text + End; 
                }

                // 505 Tag
                if (text505a.Text != "" && text505a.Text != "505a") {
                    string delEnt = text505a.Text.Replace("\r\n", "");
                    result += "505" + mid + Sort + "a" + delEnt;
                    count = 505;
                }
                for (a = 0; a < data505.RowCount - 1; a++)
                {
                    for (b = 0; b < 4; b++)
                    {
                        if (b == 0) { data505a[b] = Sort + "n" + data505.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 1) { data505a[b] = Sort + "t" + data505.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 2) { data505a[b] = Sort + "d" + data505.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 3) { data505a[b] = Sort + "e" + data505.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (data505.Rows[a].Cells[b].FormattedValue.ToString() != "" && count == 0) {
                            result += "505" + mid + data505a[b];
                            count = 1;
                        }
                        else if (data505.Rows[a].Cells[b].FormattedValue.ToString() != "") {
                            result += data505a[b];
                        }
                    }
                    result += End;
                    count = 0;
                }

                // 507 Tag
                if (text507a.Text != "" && text507a.Text != "507a") { 
                    result += "507" + mid + Sort + "a" + text507a.Text + End; 
                }

                // 520 Tag
                if (text520a.Text != "" && text520a.Text != "520a") {
                    string delEnt = text520a.Text.Replace("\r\n", "");
                    result += "520" + mid + Sort + "a" + delEnt + End;
                }

                // 521 Tag
                if (text521a.Text != "" && text521a.Text != "521a") { 
                    result += "521" + mid + Sort + "a" + text521a.Text + End; 
                }

                // 525 Tag
                if (text525a.Text != "" && text525a.Text != "525a") { 
                    result += "525" + mid + Sort + "a" + text525a.Text + End; 
                }

                // 536 Tag
                if (text536a.Text != "" && text536a.Text != "536a") { 
                    result += "536" + mid + Sort + "a" + text536a.Text + End; 
                }

                // 546 Tag
                if (text546a.Text != "" && text546a.Text != "546a 언어주기") { 
                    if(text546a.Text.Contains("\r\n") == true) {
                        string[] temp546a = text546a.Text.Split('\n');
                        for (a = 0; a < temp546a.Length; a++)
                        {
                            temp546a[a] = temp546a[a].Replace("\r", "");
                            result += "546" + mid + Sort + "a" + temp546a[a] + End;
                        }
                    }
                    else { result += "546" + mid + Sort + "a" + text546a.Text + End; }
                }

                // 586 Tag
                if (text586a.Text != "" && text586a.Text != "586a") { 
                    result += "586" + mid + Sort + "a" + text586a.Text + End; 
                }

                // 650 Tag
                if (text650a.Text != "" && text650a.Text != "650a") { 
                    result += "650" + mid + Sort + "a" + text650a.Text + End; 
                }

                // 653 Tag
                if (text653a.Text != "" && text653a.Text != "653a") { 
                    result += "653" + mid + Sort + "a" + text653a.Text + End; 
                }

                // 700 Tag
                if (text700a.Text != "" && text700a.Text != "700a") { 
                    result += "700" + mid + Sort + "a" + text700a.Text + End; 
                }

                // 710 Tag
                for (a = 0; a < data710.RowCount - 1; a++)
                {
                    for (b = 0; b < 2; b++)
                    {
                        if (b == 0) { data710a[b] = Sort + "a" + data710.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 1) { data710a[b] = Sort + "b" + data710.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (data710.Rows[a].Cells[b].FormattedValue.ToString() != "" && count == 0) {
                            result += "710" + mid;
                            count = 1;
                        }
                        if (data710.Rows[a].Cells[b].FormattedValue.ToString() != "") {
                            result += data710a[b];
                        }
                    }
                    result += End;
                    count = 0;
                }

                // 900 Tag
                if (text900a.Text != "" && text900a.Text != "900a") { 
                    result += "900" + mid + Sort + "a" + text900a.Text + End; 
                }

                // 910 Tag
                for (a = 0; a < data910.RowCount - 1; a++)
                {
                    for (b = 0; b < 2; b++)
                    {
                        if (b == 0) { data910a[b] = Sort + "a" + data910.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (b == 1) { data910a[b] = Sort + "b" + data910.Rows[a].Cells[b].FormattedValue.ToString(); }
                        if (data910.Rows[a].Cells[b].FormattedValue.ToString() != "" && count == 0) {
                            result += "910" + mid;
                            count = 1;
                        }
                        if (data910.Rows[a].Cells[b].FormattedValue.ToString() != "") {
                            result += data910a[b];
                        }
                    }
                    result += End;
                    count = 0;
                }

                // 940 Tag
                if (text940a.Text != "" && text940a.Text != "940a 로컬서명") { 
                    result += "940" + mid + Sort + "a" + text940a.Text + End; 
                }
            }           // richTextBox1에 마크를 만드는 코드
            richTextBox1.Text = result.Replace("\n▲", "");
            Color_change("▲");
            Color_change("▼");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox2.Text = "";
            richTextBox3.Text = "";
            List<string> mynum = new List<string>();
            List<string> symbol = new List<string>();
            List<string> tempList = new List<string>();
            List<string> mylist = new List<string>();
            string richBox = richTextBox1.Text.Replace("\n", ""); ;
            string tempname = "";
            string tempsymbol = "";
            char[] delimiter = { '▲', '\n', '\t' };
            string[] ss;
            string[] arr = richBox.Split(delimiter);
            int count = 0, a = 0, b = 0, c = 0, d = 0, namecout = 0, symcout = 0, gridcout=0;

            foreach (string element in arr)
            {
                if (count != 0) { richTextBox2.Text += '\n'; }
                richTextBox2.Text += count + " : [" + element + "]";
                if (count == 0 || count % 3 == 0) {
                    mynum.Add(element);
                    if (a == 0) { tempname = mynum[0]; }
                    else if (mynum[a] != element)  { tempname = element; }
                    a++;
                }
                if (count == 1 || count % 3 == 1) {
                    symbol.Add(element);
                    if (b == 0) { tempsymbol = symbol[0]; }
                    else if (symbol[b] != element) { tempsymbol = element; }
                    b++;
                }
                if (count == 2 || count % 3 == 2) {
                    tempList.Add(element);
                    if(tempname == "020" || tempname == "246" || tempname == "440" || 
                       tempname == "490" || tempname == "505" || tempname == "710" || tempname == "910") {

                        GridReturn(tempname, tempsymbol, element, gridcout);
                        gridcout++;
                    }
                    
                    ss = tempList[0].Split('▼');
                    if (c == 0) { }
                    else if (tempList[c] != tempList[c - 1]) {
                        ss = tempList[c].Split('▼');
                    }
                    foreach (string dlatl in ss)
                    {
                        if (dlatl == "") { }
                        else {

                            if (mynum[d] != tempname || namecout == 0) { mynum.Insert(d, tempname); namecout++; }
                            else if (mynum[d] == tempname) { mynum.Add(tempname); namecout = 0; }
                            if (symbol[d] != tempsymbol || symcout == 0) { symbol.Insert(d, tempsymbol); symcout++; }
                            else if (symbol[d] == tempsymbol) { symbol.Add(tempsymbol); symcout = 0; }
                            mylist.Add(dlatl);
                            d++;
                        }
                    }
                    c++;
                }
                count++;
            }
            count = 0;
            for (int num = 0; num < mylist.Count; num++)
            {
                richTextBox3.Text += num + " : [" + mynum[num] + "] [" + symbol[num] + "] [" + mylist[num] + "] ["+a+"]\n";
                
                if(mynum[num] == "056" || mynum[num] == "082") {
                    if (count == 1) {
                        ReturnToBox(mynum[num], symbol[num], mylist[num-1], mylist[num]);
                        count = 0;
                    }
                    else { count = 1; }
                }
                else { ReturnToBox(mynum[num], symbol[num], mylist[num]); }
            }
            Color_change("▲");
            Color_change("▼");
        }


        // TODO: Alt+x 입력 시 "▼78"이 출력됨. 아스키코드의 문제로 보여지나 정확한 원인은 확인불가. + 색상이 파란색으로 고정됨.
        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            call = ((System.Windows.Forms.RichTextBox)sender).Name;
            richTextBox1.LanguageOption = 0;
            if (e.Alt)
            {
                if (e.KeyValue == 48 || e.KeyValue == 96)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼0";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 49 || e.KeyValue == 97)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼1";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 50 || e.KeyValue == 98)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼2";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 51 || e.KeyValue == 99)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼3";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 52 || e.KeyValue == 100)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼4";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 53 || e.KeyValue == 101)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼5";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 54 || e.KeyValue == 102)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼6";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 55 || e.KeyValue == 103)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼7";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 56 || e.KeyValue == 104)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼8";
                    richTextBox1.SelectionColor = Color.Black;
                }
                else if (e.KeyValue == 57 || e.KeyValue == 105)
                {
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.SelectedText = "▼9";
                    richTextBox1.SelectionColor = Color.Black;
                }

                switch (e.KeyValue)
                {
                    case 65:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼a";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 66:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼b";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 67:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼c";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 68:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼d";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 69:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼e";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 70:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼f";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 71:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼g";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 72:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼h";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 73:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼i";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 74:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼j";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 75:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼k";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 76:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼l";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 77:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼m";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 78:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼n";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 79:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼o";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 80:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼p";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 81:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼q";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 82:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼r";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 83:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼s";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 84:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼t";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 85:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼u";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 86:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼v";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 87:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼w";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 88:            // TODO: 입력시 코드값?이 출력됨 X쪽의 문제인지 아스키코드의 문제인지 확인 불가.
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼";
                        richTextBox1.SelectedText = "x";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 89:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼y";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    case 90:
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.SelectedText = "▼z";
                        richTextBox1.SelectionColor = Color.Black;
                        break;
                    default:
                        break;
                }

            }

            if (e.KeyCode == Keys.Enter)
            {
                richTextBox1.SelectionColor = Color.Red;
                richTextBox1.SelectedText = "▲";
                richTextBox1.SelectionColor = Color.Black;
            }

            if (e.Control) {
                switch (e.KeyValue)
                {
                    case 70:        // f
                        string inputText = Microsoft.VisualBasic.Interaction.InputBox("찾을 단어를 입력해주세요.", "찾기(Ctrl+F)", "");
                        Color_change(inputText);
                        break;
                    case 72:        // h
                        findNchange fnc = new findNchange(this);
                        fnc.Show();
                        break;
                }
            }
        }

        private void comboBox1_MouseClick(object sender, MouseEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = true;
        }

        private void invertCheck_MouseClick(object sender, MouseEventArgs e)
        {
            string tmpStr = text245d.Text;
            if (tmpStr[tmpStr.Length-1] != ',') { tmpStr = basic_Replace(tmpStr); }
            else { tmpStr = tmpStr.Remove(tmpStr.Length - 1); }
            basicHeadBox.Text = tmpStr;

            string[] invert = basicHeadBox.Text.Split(' ');

            for(int a = 0; a < invert.Length; a++)
            {
                if (invert[a][invert[a].Length - 1] == ',')
                {
                    invert[a] = invert[a].Substring(0, invert[a].Length - 1);
                }
            }

            if (invertCheck.Checked == true)
            {
                for(int a = 0; a < invert.Length; a++)
                {
                    if (a == 0) 
                    { 
                        if(invert[a][invert[a].Length-1] != ',') { invert[a] += ","; }
                        basicHeadBox.Text = invert[a] + " ";
                        if(invert[a][invert[a].Length-1] == ',') { invert[a] = invert[a].Substring(0, invert.Length - 1); }
                    }
                    else
                    {
                        if (invert[a][invert[a].Length - 1] != ',') { invert[a] += ","; }
                        basicHeadBox.Text += invert[a] + " "; 
                        if (invert[a][invert[a].Length - 1] == ',') { invert[a] = invert[a].Substring(0, invert.Length - 1); }
                    }
                }
            }
            else if (invertCheck.Checked == false)
            {
                for (int a = invert.Length-1; a >= 0; a--)
                {
                    if (a == invert.Length - 1)
                    {
                        if (invert[a][invert[a].Length - 1] != ',') { invert[a] += ","; }
                        basicHeadBox.Text = invert[a] + " ";
                        if (invert[a][invert[a].Length - 1] == ',') { invert[a] = invert[a].Substring(0, invert.Length - 1); }
                    }
                    else
                    {
                        if (invert[a][invert[a].Length - 1] != ',') { invert[a] += ","; }
                        basicHeadBox.Text += invert[a] + " ";
                        if (invert[a][invert[a].Length - 1] == ',') { invert[a] = invert[a].Substring(0, invert.Length - 1); }
                    }
                }
            }
            if(basicHeadBox.Text[basicHeadBox.Text.Length-1]==' ')
            {
                basicHeadBox.Text = basicHeadBox.Text.Substring(0, basicHeadBox.Text.Length - 1);
            }
            if (basicHeadBox.Text[basicHeadBox.Text.Length - 1] == ',')
            {
                basicHeadBox.Text = basicHeadBox.Text.Substring(0, basicHeadBox.Text.Length - 1);
            }
        }

        /// <summary>
        /// DB실행함수.
        /// </summary>
        public void DB_Loader()
        {
            Helper_DB db = new Helper_DB();
            db.DBcon();
        }

        /// <summary>
        /// 245d에서 " 역할어,"를 잘라내는 함수
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public string basic_Replace(string strValue)
        {
            basicHeadBox.Text = "";
            string result = strValue + ",";
            string[] Role = { "글", "그림", "지음", "글·그림", "편", "엮음", "저", "씀" };
            string[] gl = { "글.그림", "글그림", "그림글", "그림.글" };
            for(int a = 0; a < gl.Length; a++) { result = result.Replace(gl[a], "글·그림"); }
            for(int a = 0; a < Role.Length; a++) { result = result.Replace(" "+Role[a]+",", ""); }

            return result;
        }

        private void text008col_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tbName = ((System.Windows.Forms.TextBox)sender).Name;
                call = ((System.Windows.Forms.TextBox)sender).Text;
                Helper008 helper = new Helper008(this);
                helper.Who_Call(tbName, call);
                helper.Show();
            }
        }
    

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                checkBox1.Text = "회의간행물";
            }
            else if (checkBox1.Checked == false)
            {
                checkBox1.Text = "회의간행물x";
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                checkBox2.Text = "기념논문집";
            }
            else if (checkBox2.Checked == false)
            {
                checkBox2.Text = "기념논문집x";
            }
        }

        /// <summary>
        /// 색상 변경함수
        /// </summary>
        /// <param name="strTarget"></param>
        public void Color_change(string strTarget)
        {
            Regex regex = new Regex(strTarget);
            MatchCollection mc = regex.Matches(richTextBox1.Text);
            richTextBox1.Select(0, richTextBox1.Text.Length);
            richTextBox1.SelectionBackColor = Color.White;

            int ICursorPosition = richTextBox1.SelectionStart;
            foreach (Match m in mc)
            {
                int istartidx = m.Index;
                int istopidx = m.Length + 1;
                int istopidx1 = m.Length;

                if (strTarget == "▼" || strTarget == "▲") { richTextBox1.Select(istartidx, istopidx); }
                else { richTextBox1.Select(istartidx, istopidx1); }
                
                if (strTarget == "▼") { richTextBox1.SelectionColor = Color.Blue; }
                else if (strTarget == "▲") { richTextBox1.SelectionColor = Color.Red; }
                else { richTextBox1.SelectionBackColor = Color.Orange; }     // TODO: 색상 변경될수 있음.

                richTextBox1.SelectionStart = ICursorPosition;
                if (strTarget == "▼" || strTarget == "▲") { richTextBox1.SelectionColor = Color.Black; }
                else { richTextBox1.SelectionBackColor = Color.Empty; }
            }
        }

        private void Btn_interlock_Click(object sender, EventArgs e)
        {
            // 언어 36
            string[] combo6 = { "한국어", "영어", "일본어", "중국어", "독일어",
                                "프랑스어", "러시아어", "스페인어", "이탈리아어", "네덜란드어",
                                "핀란드어", "스웨덴어", "포르투갈어", "노르웨이어", "그리스어",
                                "체코어", "폴란드어", "다국어", "말레이시아어", "몽골어",
                                "버마어", "베트남어", "슬로베니아어", "아랍어", "아프리카어",
                                "에스토니아어", "우즈베키스탄어", "우크라이나어", "마라티어", "캄보디아어",
                                "태국어", "터키어", "투르크메니스탄어", "티베르어", "타갈로그어",
                                "헝가리어" };

            string[] combo6_res = { "kor", "eng", "jpn", "chi", "ger",
                                    "fre", "rus", "spa", "ita", "dut",
                                    "fin", "swe", "por", "nor", "grc",
                                    "cze", "pol", "mul", "may", "mon",
                                    "bur", "vie", "slv", "ara", "afr",
                                    "est", "uzb", "ukr", "mar", "cam",
                                    "tha", "tur", "tuk", "tib", "tag",
                                    "hun" };
            string result = "";
            string[] tempStr = { text041a.Text, "", ""};

            if (text041h.Text != "" && text041h.Text != "041h" && text041a.Text != "" && text041a.Text != "041a") {
                int tmpcout = tempStr[0].IndexOf(',');
                tempStr[1] = tempStr[0].Substring(0, tmpcout);
                tempStr[2] = tempStr[0].Substring(tmpcout);
                for (int a = 0; a < combo6.Length; a++) { 
                    if (text041h.Text.Contains(combo6_res[a]) == true) {
                        result = combo6[a]+" 원작을 ";
                    }
                }
                if (text041a.Text.Contains(',') == true) {
                    for (int a = 0; a < combo6.Length; a++) {
                        if (tempStr[1].Contains(combo6_res[a]) == true) {
                            result += combo6[a] + "로 번역\r\n";
                        }
                    }
                    for (int a = 0; a < combo6.Length; a++) {
                        if (tempStr[1].Contains(combo6_res[a]) == true) {
                            result += "본문은 " + combo6[a] + ", ";
                        }
                    }
                    for (int a = 0; a < combo6.Length; a++) {
                        if (tempStr[2].Contains(combo6_res[a]) == true) {
                            result += combo6[a] + "로 혼합수록되어 있음";
                        }
                    }
                }
                else {
                    for (int a = 0; a < combo6.Length; a++) {
                        if (text041a.Text.Contains(combo6_res[a]) == true) {
                            result += combo6[a] + "로 번역";
                        }
                    }
                }
                text546a.Text = result;
            }
            else if (text041h.Text != "" && text041h.Text != "041h" && text041a.Text != "" && text041a.Text != "041a" && text041k.Text != "" && text041a.Text != "041k")
            {
                for (int a = 0; a < combo6.Length; a++)
                {
                    if (text041k.Text.Contains(combo6_res[a]) == true)
                    {
                        result = combo6[a] + "로 번역된 ";
                    }
                }
                for (int a = 0; a < combo6.Length; a++)
                {
                    if (text041h.Text.Contains(combo6_res[a]) == true)
                    {
                        result += combo6[a] + " 원작을 ";
                    }
                }
                for (int a = 0; a < combo6.Length; a++)
                {
                    if (text041a.Text.Contains(combo6_res[a]) == true)
                    {
                        result += combo6[a] + "로 중역";
                    }
                }
                text546a.Text = result;
            }
            else if (text041a.Text != "" && text041a.Text != "041a" && text041a.Text.Contains(',') == true) {
                int tmpcout = tempStr[0].IndexOf(',');
                tempStr[1] = tempStr[0].Substring(0, tmpcout);
                tempStr[2] = tempStr[0].Substring(tmpcout);
                for (int a = 0; a < combo6.Length; a++) {
                    if (tempStr[1].Contains(combo6_res[a]) == true) {
                        richTextBox1.Text = "\n" + tempStr[1];
                        result = "본문은 " + combo6[a] + ", ";
                        break;
                    }
                }
                for (int a = 0; a < combo6.Length; a++) {
                    if (tempStr[2].Contains(combo6_res[a]) == true) {
                        richTextBox1.Text = "\n" + tempStr[2];
                        result += combo6[a] + "로 혼합수록되어 있음";
                        break;
                    }
                }
                text546a.Text = result;
            }
        }

        private void Btn_Helper_Click(object sender, EventArgs e)
        {
            // TODO: 폼 새로 추가해서 메모장 단축키 및 기타 편의기능 설명추가할것.
        }

        private void dataGridView2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // ((System.Windows.Forms.DataGridView)sender).Rows.Add();
        }

        /// <summary>
        /// 008 만드는 함수
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public string Create_008(string strValue)
        {
            richTextBox2.Text = richTextBox1.Text;
            char[] six = { ' ',' ' };     // TODO: 발행년유형 구하는 로직 다시 짜야함.
            if (richTextBox1.Text.IndexOf("008") > 0)
            {
                int cout = richTextBox1.Text.IndexOf("008") + 11;       // 발행년유형
                int cc = cout + 26;                                     // 목록전거
                six[0] = richTextBox1.Text[cout];
                six[1] = richTextBox1.Text[cc];
            }

            string result = "008\t  \t";
            string[] combo1_res = {" ", "a", "j", "b", "c",
                                   "d", "e", "f", "z" };
            string[] combo2_res = { " ", "d", "f" };
            string[] combo3_res = { " ", "6", "d", "e", "o",
                                    "s", "r", "y", "j", "z",
                                    "m", "g", "l", "v", "w",
                                    "2", "5", "k", "q", "a",
                                    "b", "c", "f", "i", "n",
                                    "t", "p", "u" };
            string[] combo4_res = { " ", "f", "k", "j", "p",
                                    "e", "d", "v", "m", "u",
                                    "l", "s", "t", "w", "h",
                                    "i" };
            string[] combo5_res = { " ", "a", "b", "c", "d" };
            string[] combo6_res = { "kor", "eng", "jpn", "chi", "ger",
                                    "fre", "rus", "spa", "ita", "dut",
                                    "fin", "swe", "por", "nor", "grc",
                                    "cze", "pol", "mul", "may", "mon",
                                    "bur", "vie", "slv", "ara", "afr",
                                    "est", "uzb", "ukr", "mar", "cam",
                                    "tha", "tur", "tuk", "tib", "tag",
                                    "hun" };
            string timenow = System.DateTime.Now.ToString("yy/MM/dd");
            timenow = timenow.Replace("-", "");
            result += timenow.Replace(" ", "");         // 입력일자(00-05)

            if (six[0] == 's' || six[0] == 'm') {       // 발행년유형(6) 발행년1(07-10) 발행년2(11-14)
                string tempStr = Regex.Replace(text260c.Text, @"[^0-9]", "");
                if (tempStr.Length == 4) { result += "s" + tempStr + "    "; }
                else if (tempStr.Length == 8) { result += "m" + tempStr; }
            } 
            else { result += six[0]; }
            result += tag008.Country_008(text260a.Text);        // 발행국(15-17)
            result += tag008.Picture_008(text300b.Text);        // 삽화표시(18-21)
            int Sel = comboBox1.SelectedIndex;
            result += combo1_res[Sel];                          // 이용대상자수준(22)
            Sel = comboBox2.SelectedIndex;
            result += combo2_res[Sel];                          // 개별자료형태(23)
            Sel = comboBox3.SelectedIndex;
            result += combo3_res[Sel];                          // 내용형식1(24)
            Sel = comboBox7.SelectedIndex;
            result += combo3_res[Sel];                          // 내용형식2(25)
            result += col008res.Text + " ";                     // 한국대학부호(26-27) 수정레코드(28)
            if (checkBox1.Checked == true) { result += "1"; } else { result += "0"; }       // 회의간행물(29)
            if (checkBox2.Checked == true) { result += "1"; } else { result += "0"; }       // 기념논문집(30)
            if (text504a.Text.Contains("색인") == true) { result += "1"; } else { result += "0"; }    // 색인(31)
            if (six[1] == ' ' || six[1] == 'a' || six[1] == 'c' || six[1] == 'd')  { result += six[1]; }    // 목록전거(32)
            else { result += "u"; }
            Sel = comboBox4.SelectedIndex;
            result += combo4_res[Sel];              // 문학형식(33)
            Sel = comboBox5.SelectedIndex;
            result += combo5_res[Sel];              // 전기(34)
            Sel = comboBox6.SelectedIndex;
            result += combo6_res[Sel];              // 언어(35-37)
            result += gov008res.Text + "▲\n";        // 한국정부기관부호(38-39)
            return result;
        }

        /// <summary>
        /// GridView에 다시 집어넣는거
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="sym"></param>
        /// <param name="list"></param>
        public void GridReturn(string tag, string sym, string list, int count = 0)
        {
            if (tag == "020")
            {
                if (sym.Contains("1") == true)
                {
                    GridView020.Rows[count].Cells[0].Value = true; 
                }
                else
                {
                    GridView020.Rows[count].Cells[0].Value = false; 
                }
                int a020 = list.IndexOf("▼");
                int c020 = list.IndexOf("▼", a020 + 1);
                int g020 = 0;
                string[] text020 = { "", "", "" };
                if (c020 < 0) { c020 = list.IndexOf("▼", list.Length); g020 = -1; }
                else { g020 = list.IndexOf("▼", c020 + 1); }

                if (c020 < 0) { text020[0] = list.Substring(a020 + 1); }
                else { text020[0] = list.Substring(a020 + 1, c020 - 1); }

                if (g020 < 0) { text020[1] = list.Substring(c020 + 1); }
                else
                {
                    text020[1] = list.Substring(c020 + 1, g020 - 1 - c020);
                    text020[2] = list.Substring(g020 + 1);
                }
                for (int i = 0; i < 3; i++)
                {
                    if (text020[i] == "") { GridView020.Rows[count].Cells[i].Value = ""; break; }
                    if (text020[i][0] == 'a')
                    {
                        GridView020[1,count].Value = text020[i].Substring(1);
                    }
                    if (text020[i][0] == 'g')
                    {
                        GridView020[2,count].Value = text020[i].Substring(1);
                    }
                    if (text020[i][0] == 'c')
                    {
                        GridView020[3,count].Value = text020[i].Substring(1);
                    }
                }
            }
            if (tag == "246")
            {
                if (sym.Contains("B") == true) { GridView246.Rows[count].Cells[0].Value = "B"; }
                else if (sym.Contains("C") == true) { GridView246.Rows[count].Cells[0].Value = "C"; }
                else if (sym.Contains("D") == true) { GridView246.Rows[count].Cells[0].Value = "D"; }
                else if (sym.Contains("E") == true) { GridView246.Rows[count].Cells[0].Value = "E"; }
                else { GridView246.Rows[count].Cells[0].Value = "A"; }

                int i246 = list.IndexOf("▼");
                int a246 = list.IndexOf("▼", i246 + 1);
                int b246 = 0, n246 = 0, p246 = 0;
                string[] text246 = { "", "", "", "", "" };
                if (a246 < 0) { a246 = list.IndexOf("▼", list.Length); b246 = -1; }
                else { b246 = list.IndexOf("▼", a246 + 1); }
                if (b246 < 0) { b246 = list.IndexOf("▼", list.Length); n246 = -1; }
                else { n246 = list.IndexOf("▼", b246 + 1); }
                if (n246 < 0) { n246 = list.IndexOf("▼", list.Length); p246 = -1; }
                else { p246 = list.IndexOf("▼", n246 + 1); }

                if (a246 < 0) { text246[0] = list.Substring(i246 + 1); }
                else { text246[0] = list.Substring(i246 + 1, a246 - 1); }

                if (b246 < 0) { text246[1] = list.Substring(a246 + 1); }
                else { text246[1] = list.Substring(a246 + 1, b246 - 1 - a246); }

                if (n246 < 0) { text246[2] = list.Substring(b246 + 1); }
                else { text246[2] = list.Substring(b246 + 1, n246 - 1 - b246); }

                if (p246 < 0) { text246[3] = list.Substring(n246 + 1); }
                else
                {
                    text246[3] = list.Substring(n246 + 1, p246 - 1 - n246);
                    text246[4] = list.Substring(p246 + 1);
                }
                for (int i = 0; i < 5; i++)
                {
                    if (text246[i] == "") { GridView246.Rows[count].Cells[i].Value = ""; break; }
                    if (text246[i][0] == 'i') { GridView246.Rows[count].Cells[1].Value = text246[i].Substring(1); }
                    if (text246[i][0] == 'a') { GridView246.Rows[count].Cells[2].Value = text246[i].Substring(1); }
                    if (text246[i][0] == 'b') { GridView246.Rows[count].Cells[3].Value = text246[i].Substring(1); }
                    if (text246[i][0] == 'n') { GridView246.Rows[count].Cells[4].Value = text246[i].Substring(1); }
                    if (text246[i][0] == 'p') { GridView246.Rows[count].Cells[5].Value = text246[i].Substring(1); }
                }
            }
            if (tag == "440")
            {
                int a440 = list.IndexOf("▼");
                int n440 = list.IndexOf("▼", a440 + 1);
                int p440 = 0, v440 = 0, x440 = 0;
                string[] text440 = { "", "", "", "", "" };

                if (n440 < 0) { n440 = list.IndexOf("▼", list.Length); p440 = -1; }
                else { p440 = list.IndexOf("▼", n440 + 1); }
                if (p440 < 0) { p440 = list.IndexOf("▼", list.Length); v440 = -1; }
                else { v440 = list.IndexOf("▼", p440 + 1); }
                if (v440 < 0) { v440 = list.IndexOf("▼", list.Length); x440 = -1; }
                else { x440 = list.IndexOf("▼", v440 + 1); }

                if (n440 < 0) { text440[0] = list.Substring(a440 + 1); }
                else { text440[0] = list.Substring(a440 + 1, n440 - 1); }

                if (p440 < 0) { text440[1] = list.Substring(n440 + 1); }
                else { text440[1] = list.Substring(n440 + 1, p440 - 1 - n440); }

                if (v440 < 0) { text440[2] = list.Substring(p440 + 1); }
                else { text440[2] = list.Substring(p440 + 1, v440 - 1 - p440); }

                if (x440 < 0) { text440[3] = list.Substring(v440 + 1); }
                else
                {
                    text440[3] = list.Substring(v440 + 1, x440 - 1 - v440);
                    text440[4] = list.Substring(x440 + 1);
                }

                for (int i = 0; i < 5; i++)
                {
                    if (text440[i] == "")
                    {
                        if (i != 4) { GridView440.Rows[count].Cells[i].Value = ""; }
                        break;
                    }
                    if (text440[i][0] == 'a') { GridView440.Rows[count].Cells[0].Value = text440[i].Substring(1); }
                    if (text440[i][0] == 'n') { GridView440.Rows[count].Cells[1].Value = text440[i].Substring(1); }
                    if (text440[i][0] == 'p') { GridView440.Rows[count].Cells[2].Value = text440[i].Substring(1); }
                    if (text440[i][0] == 'v')
                    {
                        if (text440[i].Contains(" - ") == true)
                        {
                            int str440 = text440[i].IndexOf(" - ");
                            string[] v440a = { text440[i].Substring(0, str440), text440[i].Substring(str440 + 3) };
                            GridView440.Rows[count].Cells[3].Value = v440a[0].Substring(1);
                            GridView440.Rows[count].Cells[4].Value = v440a[1].Substring(0);
                        }
                        else { GridView440.Rows[count].Cells[3].Value = text440[i].Substring(1); }
                    }
                    if (text440[i][0] == 'x') { GridView440.Rows[count].Cells[5].Value = text440[i].Substring(1); }
                }
            }
            if (tag == "490")
            {
                int a490 = list.IndexOf("▼");
                int b490 = list.IndexOf("▼", a490 + 1);
                string[] text490 = { "", "" };

                if (b490 < 0) { text490[0] = list.Substring(a490 + 1); }
                else
                {
                    text490[0] = list.Substring(a490 + 1, b490 - 1 - a490);
                    text490[1] = list.Substring(b490 + 1);
                }

                for (int i = 0; i < 2; i++)
                {
                    if (text490[i] == "") { GridView490.Rows[count].Cells[i].Value = ""; break; }
                    if (text490[i][0] == 'a') { GridView490.Rows[count].Cells[0].Value = text490[i].Substring(1); }
                    if (text490[i][0] == 'b') { GridView490.Rows[count].Cells[1].Value = text490[i].Substring(1); }
                }
            }
            if (tag == "505")
            {
                int n505 = list.IndexOf("▼");
                int t505 = list.IndexOf("▼", n505 + 1);
                int d505 = 0;
                int e505 = 0;
                string[] text505 = { "", "", "", "" };

                if (t505 < 0) { t505 = list.IndexOf("▼", list.Length); d505 = -1; }
                else { d505 = list.IndexOf("▼", t505 + 1); }
                if (d505 < 0) { d505 = list.IndexOf("▼", list.Length); e505 = -1; }
                else { e505 = list.IndexOf("▼", d505 + 1); }

                if (t505 < 0) { text505[0] = list.Substring(n505 + 1); }
                else { text505[0] = list.Substring(n505 + 1, t505 - 1); }

                if (d505 < 0) { text505[1] = list.Substring(t505 + 1); }
                else { text505[1] = list.Substring(t505 + 1, d505 - 1 - t505); }

                if (e505 < 0) { text505[2] = list.Substring(d505 + 1); }
                else
                {
                    text505[2] = list.Substring(d505 + 1, e505 - 1 - d505);
                    text505[3] = list.Substring(e505 + 1);
                }

                for (int i = 0; i < 4; i++)
                {
                    if (text505[i] == "") { GridView505.Rows[count].Cells[i].Value = ""; break; }
                    if (text505[i][0] == 'n') { GridView505.Rows[count].Cells[0].Value = text505[i].Substring(1); }
                    else if (text505[i][0] == 't') { GridView505.Rows[count].Cells[1].Value = text505[i].Substring(1); }
                    else if (text505[i][0] == 'd') { GridView505.Rows[count].Cells[2].Value = text505[i].Substring(1); }
                    else if (text505[i][0] == 'e') { GridView505.Rows[count].Cells[3].Value = text505[i].Substring(1); }
                }
            }
            if (tag == "710")
            {
                int a710 = list.IndexOf("▼");
                int b710 = list.IndexOf("▼", a710 + 1);
                string[] text710 = { "", "" };

                if (b710 < 0) { text710[0] = list.Substring(a710 + 1); }
                else
                {
                    text710[0] = list.Substring(a710 + 1, b710 - 1 - a710);
                    text710[1] = list.Substring(b710 + 1);
                }

                for (int i = 0; i < 2; i++)
                {
                    if (text710[i] == "") { GridView710.Rows[count].Cells[i].Value = ""; break; }
                    if (text710[i][0] == 'a') { GridView710.Rows[count].Cells[0].Value = text710[i].Substring(1); }
                    if (text710[i][0] == 'b') { GridView710.Rows[count].Cells[1].Value = text710[i].Substring(1); }
                }
            }
            if (tag == "910")
            {
                int a910 = list.IndexOf("▼");
                int b910 = list.IndexOf("▼", a910 + 1);
                string[] text910 = { "", "" };

                if (b910 < 0) { text910[0] = list.Substring(a910 + 1); }
                else
                {
                    text910[0] = list.Substring(a910 + 1, b910 - 1 - a910);
                    text910[1] = list.Substring(b910 + 1);
                }

                for (int i = 0; i < 2; i++)
                {
                    if (text910[i] == "") { GridView910.Rows[count].Cells[i].Value = ""; break; }
                    if (text910[i][0] == 'a') { GridView910.Rows[count].Cells[0].Value = text910[i].Substring(1); }
                    if (text910[i][0] == 'b') { GridView910.Rows[count].Cells[1].Value = text910[i].Substring(1); }
                }
            }
        }

        /// <summary>
        /// 메모장에서 수정한 마크를 텍스트박스로 전달하는 함수
        /// </summary>
        /// <param name="num"></param>
        /// <param name="sym"></param>
        /// <param name="list"></param>
        public void ReturnToBox(string num, string sym, string list, string list2 = "")
        {
            {
                // 041 Tag
                if (num == "041")
                {
                    if (list[0] == 'a') { text041a.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'k') { text041k.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'h') { text041h.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'b') { text041b.Text = list.Substring(1, list.Length - 1); }
                }

                // 056 Tag _ KDC
                if (num == "056")
                {
                    if (list2[list2.Length - 1] == '4') { textKDC4.Text = list.Substring(1, list.Length - 1); }
                    if (list2[list2.Length - 1] == '5') { textKDC5.Text = list.Substring(1, list.Length - 1); }
                    if (list2[list2.Length - 1] == '6') { textKDC6.Text = list.Substring(1, list.Length - 1); }
                }

                // 082 Tag _ DDC
                if (num == "082")
                {
                    if (list2[list2.Length - 1] == '1') { textDDC21.Text = list.Substring(1, list.Length - 1); }
                    if (list2[list2.Length - 1] == '2') { textDDC22.Text = list.Substring(1, list.Length - 1); }
                    if (list2[list2.Length - 1] == '3') { textDDC23.Text = list.Substring(1, list.Length - 1); }
                }

                // 100 Tag
                if (num == "100")
                {
                    if(list[0] == 'a') { basicHeadBox.Text = list.Substring(1, list.Length - 1); }
                    radioButton1.Checked = true;
                }

                // 110 Tag
                if (num == "110")
                {
                    if(list[0] == 'a') { basicHeadBox.Text = list.Substring(1, list.Length - 1); }
                    radioButton2.Checked = true;
                }

                // 111 Tag
                if (num == "111")
                {
                    if(list[0] == 'a') { basicHeadBox.Text = list.Substring(1, list.Length - 1); }
                    radioButton3.Checked = true;
                }

                // 245 Tag
                if (num == "245")
                {
                    if (list[0] == 'a') { text245a.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'b') { text245b.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'd') { text245d.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'e') { text245e.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'n') { text245n.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'p') { text245p.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'x') { text245x.Text = list.Substring(1, list.Length - 1); }
                }

                // 250 Tag
                if (num == "250")
                {
                    if (list[0] == 'a') { text250a.Text = list.Substring(1, list.Length - 1); }
                }

                // 260 Tag
                if (num == "260")
                {
                    if (list[0] == 'a') { text260a.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'b') { text260b.Text += "▼" + list.Substring(1, list.Length - 1); }
                    if (list[0] == 'c') { text260c.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'g') { text260g.Text = list.Substring(1, list.Length - 1); }
                }

                // 300 Tag
                if (num == "300")
                {
                    string[] tmpStr;
                    if (list[0] == 'a') { text300a.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'b') { text300b.Text = list.Substring(1, list.Length - 1); }
                    if (list[0] == 'c')
                    {

                        if (list.Contains("x") == true)
                        {
                            tmpStr = list.Split('x');
                            text300c1.Text = tmpStr[0].Substring(1, tmpStr[0].Length - 1);
                            text300c2.Text = tmpStr[1];
                        }
                        else { text300a.Text = list.Substring(1, list.Length - 1); }
                    }
                    if (list[0] == 'e') { text300e.Text = list.Substring(1, list.Length - 1); }
                }

                // 500 Tag
                if (num == "500")
                {
                    if (list[0] == 'a') { text500a.Text = list.Substring(1, list.Length - 1); }
                }
                // 504 Tag
                if (num == "504")
                {
                    if (list[0] == 'a') { text504a.Text = list.Substring(1, list.Length - 1); }
                }

                // 505 Tag
                if (num == "505")
                {
                    if (list[0] == 'a') { text505a.Text = list.Substring(1, list.Length - 1); }
                }

                // 507 Tag
                if (num == "507")
                {
                    if (list[0] == 'a') { text507a.Text = list.Substring(1, list.Length - 1); }
                }

                // 520 Tag
                if (num == "520")
                {
                    if (list[0] == 'a') { text520a.Text = list.Substring(1, list.Length - 1); }
                }

                // 521 Tag
                if (num == "521")
                {
                    if (list[0] == 'a') { text521a.Text = list.Substring(1, list.Length - 1); }
                }

                // 525 Tag
                if (num == "525")
                {
                    if (list[0] == 'a') { text525a.Text = list.Substring(1, list.Length - 1); }
                }

                // 536 Tag
                if (num == "536")
                {
                    if (list[0] == 'a') { text536a.Text = list.Substring(1, list.Length - 1); }
                }

                // 546 Tag
                if (num == "546")
                {
                    if (list[0] == 'a') { text546a.Text = list.Substring(1, list.Length - 1); }
                }

                // 586 Tag
                if (num == "586")
                {
                    if (list[0] == 'a') { text586a.Text = list.Substring(1, list.Length - 1); }
                }

                // 650 Tag
                if (num == "650")
                {
                    if (list[0] == 'a') { text650a.Text = list.Substring(1, list.Length - 1); }
                }

                // 653 Tag
                if (num == "653")
                {
                    if (list[0] == 'a') { text653a.Text = list.Substring(1, list.Length - 1); }
                }

                // 700 Tag
                if (num == "700")
                {
                    if (list[0] == 'a') { text700a.Text = list.Substring(1, list.Length - 1); }
                }

                // 900 Tag
                if (num == "900")
                {
                    if (list[0] == 'a') { text900a.Text = list.Substring(1, list.Length - 1); }
                }

                // 940 Tag
                if (num == "940")
                {
                    if (list[0] == 'a') { text940a.Text = list.Substring(1, list.Length - 1); }
                }
            }       // 이 안에 태그있다
            Color_change("▲");
            Color_change("▼");
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(e.RowBounds.Location.X,
                        e.RowBounds.Location.Y,
                        ((System.Windows.Forms.DataGridView)sender).RowHeadersWidth - 4,
                        e.RowBounds.Height);

            TextRenderer.DrawText(e.Graphics,
                        (e.RowIndex + 1).ToString(),
                        ((System.Windows.Forms.DataGridView)sender).RowHeadersDefaultCellStyle.Font,
                        rect,
                        ((System.Windows.Forms.DataGridView)sender).RowHeadersDefaultCellStyle.ForeColor,
                        TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private System.Drawing.Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;
        private void dataGridView_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                if(dragBoxFromMouseDown!=System.Drawing.Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    DragDropEffects dropEffects = ((System.Windows.Forms.DataGridView)sender).DoDragDrop(((System.Windows.Forms.DataGridView)sender).Rows[rowIndexFromMouseDown], DragDropEffects.Move);
                }
            }
        }
        private void dataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            rowIndexFromMouseDown = ((System.Windows.Forms.DataGridView)sender).HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                Size dragSize = SystemInformation.DragSize;
                dragBoxFromMouseDown = new System.Drawing.Rectangle(new System.Drawing.Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
            }
            else { dragBoxFromMouseDown = System.Drawing.Rectangle.Empty; }
        }
        private void dataGridView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }
        private void dataGridView_DragDrop(object sender, DragEventArgs e)
        {
            System.Drawing.Point clientPoint = ((DataGridView)sender).PointToClient(new System.Drawing.Point(e.X, e.Y));
            rowIndexOfItemUnderMouseToDrop = ((DataGridView)sender).HitTest(clientPoint.X, clientPoint.Y).RowIndex;
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(typeof(DataGridViewRow)) as DataGridViewRow;
                ((DataGridView)sender).Rows.RemoveAt(rowIndexFromMouseDown);
                ((DataGridView)sender).Rows.Insert(rowIndexOfItemUnderMouseToDrop, rowToMove);
            }
        }

        private void dataGridView8_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                for (int a = 0; a < dataGridView8.Rows.Count - 1; a++)
                {
                    if(dataGridView8.Rows[a].Selected == true)
                    {
                        dataGridView8.Rows.Remove(dataGridView8.Rows[a]);
                        break;
                    }
                }
            }
        }

        private void Marc_Shown(object sender, EventArgs e)
        {
            GridView020.CurrentCell = null;
            GridView246.CurrentCell = null;
            GridView710.CurrentCell = null;
            GridView910.CurrentCell = null;
            GridView440.CurrentCell = null;
            GridView490.CurrentCell = null;
            GridView505.CurrentCell = null;
        }
    }
}
 