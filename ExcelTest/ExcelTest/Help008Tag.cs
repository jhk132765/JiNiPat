﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelTest
{
    class Help008Tag
    {

        /// <summary>
        /// 나라 코드 입력
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public string Country_008(string strValue)
        {
            string ulk = "서울", bnk = "부산", tgk = "대구", ick = "인천", kjk = "광주", tjk = "대전", usk = "울산", sjk = "세종";
            string[] ggk = { "경기도", "경기", "수원", "성남", "의정부", "안양", "부천", "광명", "평택", "송탄", "동두천", "안산", "고양", "과천", "구리", "미금", "남양주", "오산", "시흥", "군포", "의왕", "하남", "용인", "파주", "이천", "안성", "김포", "양주", "여주", "화성", "연천", "포천", "가평", "양평", "강화", "옹진" };
            string[] gak = { "강원도", "강원", "춘천", "원주", "강릉", "동해", "태백", "속초", "삼척", "양양", "철원", "화천", "양구", "인제", "홍천", "횡성", "평창", "영월", "정선" };
            string[] hbk = { "충청북도", "충북", "충주", "제천", "단양", "음성", "진천", "괴산", "증평", "청주", "청원", "보은", "영동", "옥천" };
            string[] hck = { "충청남도", "충남", "천안", "아산", "예산", "홍성", "청양", "당진", "서산", "태안", "보령", "서천" };
            string[] gbk = { "경상북도", "경북", "울진", "봉화", "영주", "영양", "영덕", "안동", "문경", "예천", "의성", "청송", "울릉", "독도", "김천", "상주", "구미", "성주", "칠곡", "군위", "고령", "청도", "경산", "포항", "경주", "영천" };
            string[] gnk = { "경상남도", "경남", "거창", "함양", "합천", "창녕", "의령", "산청", "하동", "진주", "사천", "함안", "고성", "남해", "통영", "창원", "마산", "진해", "거제", "김해", "양산", "밀양" };
            string[] jbk = { "전라북도", "전북", "전주", "무주", "완주", "진안", "익산", "장수", "군산", "김제", "부안", "고창", "정읍", "임실", "순창", "남원" };
            string[] jnk = { "전라남도", "전남", "담양", "장성", "영광", "함평", "나주", "순천", "광양", "구례", "곡성", "화순", "여수", "보성", "고흥", "목포", "무안", "신안", "영암", "장흥", "강진", "해남", "진도", "완도" };
            string[] jjk = { "제주도", "제주", "서귀포" };
            string[] us = { "new york", "boston", "los angeles", "san francisco", "washington", "san diego" };
            string[] uk = { "london", "oxford", "manchester", "chesterfield", "lincoln", "hong kong", "glasgow", "edinburgh", "leeds", "liverpool", "sheffield", "birmingham" };
            string result = "";

            strValue = strValue.ToLower();
            if (strValue.Contains(ulk) == true) { result = "ulk"; } // 서울
            if (strValue.Contains(bnk) == true) { result = "bnk"; } // 부산
            if (strValue.Contains(tgk) == true) { result = "tgk"; } // 대구
            if (strValue.Contains(ick) == true) { result = "ick"; } // 인천
            if (strValue.Contains(kjk) == true) { result = "kjk"; } // 광주
            if (strValue.Contains(tjk) == true) { result = "tjk"; } // 대전
            if (strValue.Contains(usk) == true) { result = "usk"; } // 울산
            if (strValue.Contains(sjk) == true) { result = "sjk"; } // 세종
            for (int a = 0; a < ggk.Length; a++) { if (strValue.Contains(ggk[a]) == true) { result = "ggk"; } }  // 경기
            for (int a = 0; a < gak.Length; a++) { if (strValue.Contains(gak[a]) == true) { result = "gak"; } }  // 강원
            for (int a = 0; a < hbk.Length; a++) { if (strValue.Contains(hbk[a]) == true) { result = "hbk"; } }  // 충북
            for (int a = 0; a < hck.Length; a++) { if (strValue.Contains(hck[a]) == true) { result = "hck"; } }  // 충남
            for (int a = 0; a < gbk.Length; a++) { if (strValue.Contains(gbk[a]) == true) { result = "gbk"; } }  // 경북
            for (int a = 0; a < gnk.Length; a++) { if (strValue.Contains(gnk[a]) == true) { result = "gnk"; } }  // 경남
            for (int a = 0; a < jbk.Length; a++) { if (strValue.Contains(jbk[a]) == true) { result = "jbk"; } }  // 전북
            for (int a = 0; a < jnk.Length; a++) { if (strValue.Contains(jnk[a]) == true) { result = "jnk"; } }  // 전남
            for (int a = 0; a < jjk.Length; a++) { if (strValue.Contains(jjk[a]) == true) { result = "jjk"; } }  // 제주
            for (int a = 0; a < us.Length; a++) { if (strValue.Contains(us[a]) == true) { result = "us"; } }  // 미국
            for (int a = 0; a < uk.Length; a++) { if (strValue.Contains(uk[a]) == true) { result = "uk"; } }  // 영국

            return result;
        }

        public string Picture_008(string strValue)
        {
            string result = "";
            string[] dpfk = { "삽화", "지도", "초상화", "표", "설계도",
                              "도판", "악보", "영인물", "문장", "계보",
                              "서식", "양식", "견본", "녹음자료", "사진",
                              "채색장식", 
                              "ill", "map", "photo" };
            string[] tlqkf = { "a", "b", "c", "d", "e",
                               "f", "g", "h", "i", "j",
                               "k", "k", "l", "m", "o",
                               "p",
                               "a", "b", "o" };
            strValue = strValue.ToLower();
            string[] str1 = strValue.Split(',');
            for(int a = 0; a < str1.Length; a++)
            {
                for (int b = 0; b < dpfk.Length; b++)
                {
                    if(str1[a].Contains(dpfk[b]) == true) { result += tlqkf[b]; }
                }
            }
            if (result.Length > 4)
            {
                result = result.Substring(0, 4);
            }
            else
            {
                for(int a = 0; a < 4; a++)
                {
                    if (result.Length < 4) { result += " "; }
                    else { break; }
                }
            }

            return result;
        }
    }
}
