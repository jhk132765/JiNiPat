import requests
from bs4 import BeautifulSoup

source = requests.get("https://www.naver.com/").text
soup = BeautifulSoup(source, "html.parser")
hotKeys = soup.select(" .ah_roll .ah_item")

for key in hotKeys:
    print(key.select_one(".ah_r").get_text()
          + ". "
          + key.select_one(".ah_k").get_text())