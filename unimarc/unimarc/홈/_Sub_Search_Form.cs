﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Home
{
    public partial class _Sub_Search_Form : Form
    {
        Transaction_manage tran;
        Helper_DB db = new Helper_DB();
        int grididx=-333;
        public _Sub_Search_Form(Transaction_manage _tran)
        {
            InitializeComponent();
            tran = _tran;
        }
        private void _Sub_Search_Form_Load(object sender, EventArgs e)
        {
            db.DBcon();
            string search = "`idx`, `c_sangho`, `c_gu`, `c_boss`, `c_bubin`, " +
                            "`c_uptae`, `c_jongmok`, `c_tel`, `c_fax`, `c_email`, " +
                            "`c_man`, `c_mantel`, `c_user`, `c_zip`, `c_addr`, " +
                            "`c_dlsID`, `c_dlsPW`, `c_division`, `c_label`, `c_program`, " +
                            "`c_etc`";
            string cmd = db.DB_Contains("Client", tran.compidx, "c_sangho", tran.tb_sangho.Text, search);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] dbtext1 = db_res.Split('|');
            string[] result = { "", "", "", "", "",
                                "", "", "", "", "",
                                "", "", "", "", "",
                                "", "", "", "", "",
                                "", "" };
            for (int a = 0; a < dbtext1.Length; a++)
            {
                if (a % 21 == 0)  { result[0]  = dbtext1[a]; }             // idx
                if (a % 21 == 1)  { result[1]  = dbtext1[a]; }             // 거래처명
                if (a % 21 == 2)  { result[2]  = dbtext1[a]; }             // 구분
                if (a % 21 == 3)  { result[3]  = dbtext1[a]; }             // 대표자명
                if (a % 21 == 4)  { result[4]  = dbtext1[a]; }             // 등록번호
                if (a % 21 == 5)  { result[5]  = dbtext1[a]; }             // 업태
                if (a % 21 == 6)  { result[6]  = dbtext1[a]; }             // 종목
                if (a % 21 == 7)  { result[7]  = dbtext1[a]; }             // 전화번호
                if (a % 21 == 8)  { result[8]  = dbtext1[a]; }             // 팩스
                if (a % 21 == 9)  { result[9]  = dbtext1[a]; }             // 메일
                if (a % 21 == 10) { result[10] = dbtext1[a]; }             // 담당자정보1
                if (a % 21 == 11) { result[11] = dbtext1[a]; }             // 담당자정보2
                if (a % 21 == 12) { result[12] = dbtext1[a]; }             // 담당직원
                if (a % 21 == 13) { result[13] = dbtext1[a]; }             // 우편번호
                if (a % 21 == 14) { result[14] = dbtext1[a]; }             // 주소
                if (a % 21 == 15) { result[15] = dbtext1[a]; }             // 아이디
                if (a % 21 == 16) { result[16] = dbtext1[a]; }             // 비밀번호
                if (a % 21 == 17) { result[17] = dbtext1[a]; }             // 용지칸수
                if (a % 21 == 18) { result[18] = dbtext1[a]; }             // 띠라벨
                if (a % 21 == 19) { result[19] = dbtext1[a]; }             // 사용프로그램
                if (a % 21 == 20) { result[20] = dbtext1[a]; dataGridView1.Rows.Add(result); } // 비고
            }
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            tran.btn_add_Click(null, null);
            if (grididx < 0) { grididx = e.RowIndex; }
            tran.tb_sangho.Text = dataGridView1.Rows[grididx].Cells["sangho"].Value.ToString();
            if(dataGridView1.Rows[grididx].Cells["gubun"].Value.ToString() == "서점") {
                tran.cb_gubun2.SelectedItem = "서점";
            }
            if(dataGridView1.Rows[grididx].Cells["gubun"].Value.ToString() == "학교") {
                tran.cb_gubun2.SelectedItem = "학교";
            }
            if(dataGridView1.Rows[grididx].Cells["gubun"].Value.ToString() == "도서관") {
                tran.cb_gubun2.SelectedItem = "도서관";
            }
            if(dataGridView1.Rows[grididx].Cells["gubun"].Value.ToString() == "기타") {
                tran.cb_gubun2.SelectedItem = "기타";
            }
            tran.tb_tel.Text = dataGridView1.Rows[grididx].Cells["tel"].Value.ToString();
            tran.tb_fax.Text = dataGridView1.Rows[grididx].Cells["fax"].Value.ToString();
            tran.tb_man.Text = dataGridView1.Rows[grididx].Cells["man"].Value.ToString();
            tran.tb_man1.Text = dataGridView1.Rows[grididx].Cells["mantel"].Value.ToString();
            tran.tb_addr.Text = dataGridView1.Rows[grididx].Cells["addr"].Value.ToString();
            tran.rtb_etc.Text = dataGridView1.Rows[grididx].Cells["bigo"].Value.ToString();

            tran.tb_boss.Text = dataGridView1.Rows[grididx].Cells["boss"].Value.ToString();
            tran.tb_bubin.Text = dataGridView1.Rows[grididx].Cells["bubin"].Value.ToString();
            tran.tb_division.Text = dataGridView1.Rows[grididx].Cells["division"].Value.ToString();
            tran.tb_jongmok.Text = dataGridView1.Rows[grididx].Cells["jongmok"].Value.ToString();
            tran.tb_label.Text = dataGridView1.Rows[grididx].Cells["label"].Value.ToString();
            tran.tb_mail.Text = dataGridView1.Rows[grididx].Cells["mail"].Value.ToString();
            tran.tb_program.Text = dataGridView1.Rows[grididx].Cells["program"].Value.ToString();
            tran.tb_id.Text = dataGridView1.Rows[grididx].Cells["DLSid"].Value.ToString();
            tran.tb_pw.Text = dataGridView1.Rows[grididx].Cells["DLSpw"].Value.ToString();
            tran.tb_uptae.Text = dataGridView1.Rows[grididx].Cells["uptae"].Value.ToString();
            tran.tb_zip.Text = dataGridView1.Rows[grididx].Cells["post_num"].Value.ToString();
            tran.tb_user.Text = dataGridView1.Rows[grididx].Cells["assumer"].Value.ToString();
            Close();
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                grididx = dataGridView1.CurrentRow.Index;
                dataGridView1_CellDoubleClick(null, null);
            }
            if(e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
    }
}
