﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Home
{
    public partial class User_Infor : Form
    {
        Helper_DB db = new Helper_DB();
        Main main;
        public User_Infor(Main _main)
        {
            InitializeComponent();
            main = _main;
        }
        private void User_Infor_Load(object sender, EventArgs e)
        {
            db.DBcon();
            string[] print = { "일반띠지출력", "입고수량만큼 띠지출력", "입고시 한부씩 입고처리" };
            cb_print.Items.AddRange(print);
            cb_print.SelectedIndex = 0;

            string[] statement = { "세로", "가로" };
            cb_statement.Items.AddRange(statement);
            cb_statement.SelectedIndex = 0;

            string[] kind = { "일반 방식", "정가위 출력", "폰트작게" };
            cb_kind.Items.AddRange(kind);
            cb_kind.SelectedIndex = 0;

            string Area =
                "`comp_name`, `boss`, `bubin`, `cobin`, `uptae`, " +
                "`jongmok`, `zip`, `addr`, `tel`, `fax`, " +
                "`bank_comp`, `bank_no`, `email`, `barea`, `smtp`, " +
                "`port`, `email_ID`, `email_PW`, `print`, `statement`, `kind`";
            string cmd = db.DB_Select_Search(Area, "Comp", "idx", main.com_idx);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] data = db_res.Split('|');
            made_Form(data);
        }
        private void made_Form(string[] strValue)
        {
            tb_sangho.Text = strValue[0];
            tb_boss.Text = strValue[1];
            tb_bubin.Text = strValue[2];
            tb_cobin.Text = strValue[3];
            tb_uptae.Text = strValue[4];
            tb_jongmok.Text = strValue[5];
            tb_zip.Text = strValue[6];
            tb_addr.Text = strValue[7];
            tb_tel.Text = strValue[8];
            tb_fax.Text = strValue[9];
            tb_bank_comp.Text = strValue[10];
            tb_bank_no.Text = strValue[11];
            tb_email.Text = strValue[12];
            tb_barea.Text = strValue[13];
            tb_smtp.Text = strValue[14];
            tb_port.Text = strValue[15];
            tb_email_id.Text = strValue[16];
            tb_email_pw.Text = strValue[17];
            cb_print.SelectedIndex = Convert.ToInt32(strValue[18]);
            cb_statement.SelectedIndex = Convert.ToInt32(strValue[19]);
            cb_kind.SelectedIndex = Convert.ToInt32(strValue[20]);
        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btn_save_Click(object sender, EventArgs e)
        {
            string[] temp = {
                tb_boss.Text, tb_bubin.Text, tb_cobin.Text, tb_uptae.Text, tb_jongmok.Text,
                tb_zip.Text, tb_addr.Text, tb_tel.Text, tb_fax.Text, tb_bank_comp.Text,
                tb_bank_no.Text, tb_email.Text, tb_barea.Text, tb_smtp.Text, tb_port.Text,
                tb_email_id.Text, tb_email_pw.Text,
                cb_print.SelectedIndex.ToString(),
                cb_statement.SelectedIndex.ToString(),
                cb_kind.SelectedIndex.ToString()};
            string[] Area ={
                "boss", "bubin", "cobin", "uptae", "jongmok",
                "zip", "addr", "tel", "fax", "bank_comp",
                "bank_no", "email", "barea", "smtp", "port",
                "email_ID", "email_PW", "print", "statement", "kind"};
            string[] search_col = { "idx", "comp_name" };
            string[] search_name = { main.com_idx, tb_sangho.Text };
            string U_cmd = db.More_Update("Comp", Area, temp, search_col, search_name);
            db.DB_Send_CMD_reVoid(U_cmd);
            MessageBox.Show("저장되었습니다.");
        }
        private void tb_port_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back))) {
                e.Handled = true;
            }
        }
        private void btn_mailtest_Click(object sender, EventArgs e)
        {
            Email mail = new Email();
            // mail.Send_Test_mail("글로리아북", tb_email.Text, "테스트 메일입니다", "테스트 메일입니다");
        }

        private void tb_sangho_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) {
                btn_close_Click(null, null);
            }
        }
    }
}
