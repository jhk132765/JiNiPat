﻿namespace WindowsFormsApp1.Home
{
    partial class Order_manage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label12 = new System.Windows.Forms.Label();
            this.cb_search_filter = new System.Windows.Forms.ComboBox();
            this.chk_stopUse = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_bank_comp = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_bank_no = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.send_chk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sangho = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bubin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uptae = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jongmok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pw = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.site = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emchk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.btn_Lookup = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Add = new System.Windows.Forms.Button();
            this.tb_jongmok = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_uptae = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_bubin = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_addr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_search = new System.Windows.Forms.TextBox();
            this.tb_boss = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_sangho = new System.Windows.Forms.TextBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.tb_tel = new System.Windows.Forms.TextBox();
            this.tb_fax = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_bank_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chk_send = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cb_gubun = new System.Windows.Forms.ComboBox();
            this.tb_barea = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chk_email = new System.Windows.Forms.CheckBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.tb_IndexWord = new System.Windows.Forms.TextBox();
            this.btn_DeleteIW = new System.Windows.Forms.Button();
            this.btn_AddIW = new System.Windows.Forms.Button();
            this.btn_ExcelIW = new System.Windows.Forms.Button();
            this.btn_SaveIW = new System.Windows.Forms.Button();
            this.dgv_IndexWord = new System.Windows.Forms.DataGridView();
            this.IndexWord = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IndexPersent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_memo = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rtb_etc = new System.Windows.Forms.RichTextBox();
            this.tb_site = new System.Windows.Forms.TextBox();
            this.tb_zip = new System.Windows.Forms.TextBox();
            this.lbl_idx = new System.Windows.Forms.Label();
            this.btn_SitePage = new System.Windows.Forms.Button();
            this.tb_pw = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tb_id = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tb_persent = new System.Windows.Forms.NumericUpDown();
            this.btn_batchProcess = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel_batchProcess = new System.Windows.Forms.Panel();
            this.btn_ProcessExit = new System.Windows.Forms.Button();
            this.btn_batchProcessing = new System.Windows.Forms.Button();
            this.dgv_WordSUB = new System.Windows.Forms.DataGridView();
            this.IndexWordSub = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IndexPersentSub = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_IndexWord)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_persent)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel_batchProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_WordSUB)).BeginInit();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 12);
            this.label12.TabIndex = 73;
            this.label12.Text = "검     색";
            // 
            // cb_search_filter
            // 
            this.cb_search_filter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_search_filter.FormattingEnabled = true;
            this.cb_search_filter.Location = new System.Drawing.Point(81, 9);
            this.cb_search_filter.Name = "cb_search_filter";
            this.cb_search_filter.Size = new System.Drawing.Size(99, 20);
            this.cb_search_filter.TabIndex = 70;
            // 
            // chk_stopUse
            // 
            this.chk_stopUse.AutoSize = true;
            this.chk_stopUse.Location = new System.Drawing.Point(252, 5);
            this.chk_stopUse.Name = "chk_stopUse";
            this.chk_stopUse.Size = new System.Drawing.Size(48, 28);
            this.chk_stopUse.TabIndex = 69;
            this.chk_stopUse.Text = "사용\n중지";
            this.chk_stopUse.UseVisualStyleBackColor = true;
            this.chk_stopUse.CheckedChanged += new System.EventHandler(this.chk_stopUse_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(501, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 68;
            this.label8.Text = "전화번호";
            // 
            // tb_bank_comp
            // 
            this.tb_bank_comp.Location = new System.Drawing.Point(332, 60);
            this.tb_bank_comp.Name = "tb_bank_comp";
            this.tb_bank_comp.Size = new System.Drawing.Size(100, 21);
            this.tb_bank_comp.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(290, 64);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 66;
            this.label9.Text = "은행명";
            // 
            // tb_bank_no
            // 
            this.tb_bank_no.Location = new System.Drawing.Point(81, 60);
            this.tb_bank_no.Name = "tb_bank_no";
            this.tb_bank_no.Size = new System.Drawing.Size(203, 21);
            this.tb_bank_no.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 64;
            this.label10.Text = "송금계좌";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.send_chk,
            this.sangho,
            this.boss,
            this.bubin,
            this.uptae,
            this.jongmok,
            this.zip,
            this.addr,
            this.tel,
            this.fax,
            this.bank_no,
            this.bank_comp,
            this.bank_name,
            this.gu,
            this.barea,
            this.etc_1,
            this.etc_2,
            this.email,
            this.id,
            this.pw,
            this.site,
            this.emchk});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(13, 325);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersWidth = 21;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(899, 376);
            this.dataGridView1.TabIndex = 63;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_IndexWord_RowPostPaint);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.ReadOnly = true;
            this.idx.Visible = false;
            // 
            // send_chk
            // 
            this.send_chk.HeaderText = "M";
            this.send_chk.Name = "send_chk";
            this.send_chk.ReadOnly = true;
            this.send_chk.Width = 40;
            // 
            // sangho
            // 
            this.sangho.HeaderText = "상     호";
            this.sangho.Name = "sangho";
            this.sangho.Width = 350;
            // 
            // boss
            // 
            this.boss.HeaderText = "대표자명";
            this.boss.Name = "boss";
            // 
            // bubin
            // 
            this.bubin.HeaderText = "등록번호";
            this.bubin.Name = "bubin";
            this.bubin.Visible = false;
            // 
            // uptae
            // 
            this.uptae.HeaderText = "업태";
            this.uptae.Name = "uptae";
            this.uptae.Visible = false;
            // 
            // jongmok
            // 
            this.jongmok.HeaderText = "종목";
            this.jongmok.Name = "jongmok";
            this.jongmok.Visible = false;
            // 
            // zip
            // 
            this.zip.HeaderText = "우편번호";
            this.zip.Name = "zip";
            this.zip.Visible = false;
            // 
            // addr
            // 
            this.addr.HeaderText = "주소";
            this.addr.Name = "addr";
            this.addr.Visible = false;
            // 
            // tel
            // 
            this.tel.HeaderText = "전화번호";
            this.tel.Name = "tel";
            this.tel.Width = 130;
            // 
            // fax
            // 
            this.fax.HeaderText = "팩스번호";
            this.fax.Name = "fax";
            this.fax.Width = 130;
            // 
            // bank_no
            // 
            this.bank_no.HeaderText = "계좌번호";
            this.bank_no.Name = "bank_no";
            this.bank_no.Visible = false;
            // 
            // bank_comp
            // 
            this.bank_comp.HeaderText = "은행명";
            this.bank_comp.Name = "bank_comp";
            this.bank_comp.Visible = false;
            // 
            // bank_name
            // 
            this.bank_name.HeaderText = "예금주명";
            this.bank_name.Name = "bank_name";
            this.bank_name.Visible = false;
            // 
            // gu
            // 
            this.gu.HeaderText = "유형";
            this.gu.Name = "gu";
            // 
            // barea
            // 
            this.barea.HeaderText = "배송처";
            this.barea.Name = "barea";
            this.barea.Visible = false;
            // 
            // etc_1
            // 
            this.etc_1.HeaderText = "비고1";
            this.etc_1.Name = "etc_1";
            this.etc_1.Visible = false;
            // 
            // etc_2
            // 
            this.etc_2.HeaderText = "비고2";
            this.etc_2.Name = "etc_2";
            this.etc_2.Visible = false;
            // 
            // email
            // 
            this.email.HeaderText = "이메일";
            this.email.Name = "email";
            this.email.Visible = false;
            // 
            // id
            // 
            this.id.HeaderText = "아이디";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // pw
            // 
            this.pw.HeaderText = "패스워드";
            this.pw.Name = "pw";
            this.pw.Visible = false;
            // 
            // site
            // 
            this.site.HeaderText = "사이트";
            this.site.Name = "site";
            this.site.Visible = false;
            // 
            // emchk
            // 
            this.emchk.HeaderText = "이메일체크";
            this.emchk.Name = "emchk";
            this.emchk.Visible = false;
            // 
            // btn_Delete
            // 
            this.btn_Delete.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_Delete.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_Delete.Location = new System.Drawing.Point(623, 8);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(75, 23);
            this.btn_Delete.TabIndex = 62;
            this.btn_Delete.Text = "삭  제";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_Lookup
            // 
            this.btn_Lookup.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_Lookup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_Lookup.Location = new System.Drawing.Point(377, 8);
            this.btn_Lookup.Name = "btn_Lookup";
            this.btn_Lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_Lookup.TabIndex = 61;
            this.btn_Lookup.Text = "조  회";
            this.btn_Lookup.UseVisualStyleBackColor = true;
            this.btn_Lookup.Click += new System.EventHandler(this.btn_Lookup_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_Save.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_Save.Location = new System.Drawing.Point(541, 8);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 60;
            this.btn_Save.Text = "저  장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Add
            // 
            this.btn_Add.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_Add.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_Add.Location = new System.Drawing.Point(459, 8);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(75, 23);
            this.btn_Add.TabIndex = 59;
            this.btn_Add.Text = "추  가";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // tb_jongmok
            // 
            this.tb_jongmok.Location = new System.Drawing.Point(783, 9);
            this.tb_jongmok.Name = "tb_jongmok";
            this.tb_jongmok.Size = new System.Drawing.Size(100, 21);
            this.tb_jongmok.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(751, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 54;
            this.label6.Text = "종목";
            // 
            // tb_uptae
            // 
            this.tb_uptae.Location = new System.Drawing.Point(635, 9);
            this.tb_uptae.Name = "tb_uptae";
            this.tb_uptae.Size = new System.Drawing.Size(100, 21);
            this.tb_uptae.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(604, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 52;
            this.label5.Text = "업태";
            // 
            // tb_bubin
            // 
            this.tb_bubin.Location = new System.Drawing.Point(489, 9);
            this.tb_bubin.Name = "tb_bubin";
            this.tb_bubin.Size = new System.Drawing.Size(100, 21);
            this.tb_bubin.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(435, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 50;
            this.label4.Text = "등록번호";
            // 
            // tb_addr
            // 
            this.tb_addr.Location = new System.Drawing.Point(149, 35);
            this.tb_addr.Name = "tb_addr";
            this.tb_addr.Size = new System.Drawing.Size(338, 21);
            this.tb_addr.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 12);
            this.label3.TabIndex = 48;
            this.label3.Text = "주     소";
            // 
            // tb_search
            // 
            this.tb_search.Location = new System.Drawing.Point(185, 9);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(177, 21);
            this.tb_search.TabIndex = 47;
            this.tb_search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_search_KeyDown);
            // 
            // tb_boss
            // 
            this.tb_boss.Location = new System.Drawing.Point(353, 9);
            this.tb_boss.Name = "tb_boss";
            this.tb_boss.Size = new System.Drawing.Size(71, 21);
            this.tb_boss.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(306, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 44;
            this.label1.Text = "대표자";
            // 
            // tb_sangho
            // 
            this.tb_sangho.Location = new System.Drawing.Point(81, 9);
            this.tb_sangho.Name = "tb_sangho";
            this.tb_sangho.Size = new System.Drawing.Size(165, 21);
            this.tb_sangho.TabIndex = 0;
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(15, 13);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(49, 12);
            this.NameLabel.TabIndex = 42;
            this.NameLabel.Text = "상     호";
            // 
            // tb_tel
            // 
            this.tb_tel.Location = new System.Drawing.Point(558, 34);
            this.tb_tel.Name = "tb_tel";
            this.tb_tel.Size = new System.Drawing.Size(117, 21);
            this.tb_tel.TabIndex = 7;
            // 
            // tb_fax
            // 
            this.tb_fax.Location = new System.Drawing.Point(756, 34);
            this.tb_fax.Name = "tb_fax";
            this.tb_fax.Size = new System.Drawing.Size(127, 21);
            this.tb_fax.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(695, 38);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 83;
            this.label16.Text = "팩스번호";
            // 
            // tb_bank_name
            // 
            this.tb_bank_name.Location = new System.Drawing.Point(484, 60);
            this.tb_bank_name.Name = "tb_bank_name";
            this.tb_bank_name.Size = new System.Drawing.Size(94, 21);
            this.tb_bank_name.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(442, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 85;
            this.label2.Text = "예금주";
            // 
            // chk_send
            // 
            this.chk_send.AutoSize = true;
            this.chk_send.Location = new System.Drawing.Point(585, 62);
            this.chk_send.Name = "chk_send";
            this.chk_send.Size = new System.Drawing.Size(72, 16);
            this.chk_send.TabIndex = 87;
            this.chk_send.Text = "송금유무";
            this.chk_send.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(659, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 89;
            this.label7.Text = "유형";
            // 
            // cb_gubun
            // 
            this.cb_gubun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gubun.FormattingEnabled = true;
            this.cb_gubun.Location = new System.Drawing.Point(692, 60);
            this.cb_gubun.Name = "cb_gubun";
            this.cb_gubun.Size = new System.Drawing.Size(66, 20);
            this.cb_gubun.TabIndex = 12;
            // 
            // tb_barea
            // 
            this.tb_barea.Location = new System.Drawing.Point(806, 60);
            this.tb_barea.Name = "tb_barea";
            this.tb_barea.Size = new System.Drawing.Size(77, 21);
            this.tb_barea.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(764, 64);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 90;
            this.label11.Text = "배송처";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 137);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 92;
            this.label13.Text = "비   고";
            // 
            // tb_email
            // 
            this.tb_email.Location = new System.Drawing.Point(81, 85);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(281, 21);
            this.tb_email.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 12);
            this.label15.TabIndex = 96;
            this.label15.Text = "이 메 일";
            // 
            // chk_email
            // 
            this.chk_email.AutoSize = true;
            this.chk_email.Location = new System.Drawing.Point(372, 81);
            this.chk_email.Name = "chk_email";
            this.chk_email.Size = new System.Drawing.Size(72, 28);
            this.chk_email.TabIndex = 98;
            this.chk_email.Text = "주문서로\n메일발송";
            this.chk_email.UseVisualStyleBackColor = true;
            // 
            // textBox17
            // 
            this.textBox17.Enabled = false;
            this.textBox17.Location = new System.Drawing.Point(512, 85);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(107, 21);
            this.textBox17.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Enabled = false;
            this.label17.Location = new System.Drawing.Point(454, 89);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 99;
            this.label17.Text = "비고금액";
            // 
            // textBox18
            // 
            this.textBox18.Enabled = false;
            this.textBox18.Location = new System.Drawing.Point(692, 85);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(107, 21);
            this.textBox18.TabIndex = 16;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Enabled = false;
            this.label18.Location = new System.Drawing.Point(634, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 101;
            this.label18.Text = "북센코드";
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Enabled = false;
            this.checkBox4.Location = new System.Drawing.Point(811, 82);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(84, 28);
            this.checkBox4.TabIndex = 103;
            this.checkBox4.Text = "입고작업시\n장부미생성";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // btn_close
            // 
            this.btn_close.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_close.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_close.Location = new System.Drawing.Point(785, 8);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 105;
            this.btn_close.Text = "닫  기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.button3.Enabled = false;
            this.button3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button3.Location = new System.Drawing.Point(-1, 52);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 23);
            this.button3.TabIndex = 106;
            this.button3.Text = "색인어전체삭제";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(23, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(165, 19);
            this.label19.TabIndex = 108;
            this.label19.Text = "색  인  어  등  록";
            // 
            // tb_IndexWord
            // 
            this.tb_IndexWord.Location = new System.Drawing.Point(0, 24);
            this.tb_IndexWord.Name = "tb_IndexWord";
            this.tb_IndexWord.Size = new System.Drawing.Size(140, 21);
            this.tb_IndexWord.TabIndex = 0;
            this.tb_IndexWord.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_IndexWord_KeyDown);
            // 
            // btn_DeleteIW
            // 
            this.btn_DeleteIW.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_DeleteIW.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_DeleteIW.Location = new System.Drawing.Point(167, 81);
            this.btn_DeleteIW.Name = "btn_DeleteIW";
            this.btn_DeleteIW.Size = new System.Drawing.Size(45, 23);
            this.btn_DeleteIW.TabIndex = 115;
            this.btn_DeleteIW.Text = "삭  제";
            this.btn_DeleteIW.UseVisualStyleBackColor = true;
            this.btn_DeleteIW.Click += new System.EventHandler(this.btn_DeleteIW_Click);
            // 
            // btn_AddIW
            // 
            this.btn_AddIW.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_AddIW.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_AddIW.Location = new System.Drawing.Point(-1, 81);
            this.btn_AddIW.Name = "btn_AddIW";
            this.btn_AddIW.Size = new System.Drawing.Size(45, 23);
            this.btn_AddIW.TabIndex = 114;
            this.btn_AddIW.Text = "추 가";
            this.btn_AddIW.UseVisualStyleBackColor = true;
            this.btn_AddIW.Click += new System.EventHandler(this.btn_AddIW_Click);
            // 
            // btn_ExcelIW
            // 
            this.btn_ExcelIW.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_ExcelIW.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_ExcelIW.Location = new System.Drawing.Point(111, 81);
            this.btn_ExcelIW.Name = "btn_ExcelIW";
            this.btn_ExcelIW.Size = new System.Drawing.Size(45, 23);
            this.btn_ExcelIW.TabIndex = 113;
            this.btn_ExcelIW.Text = "엑 셀";
            this.btn_ExcelIW.UseVisualStyleBackColor = true;
            this.btn_ExcelIW.Click += new System.EventHandler(this.btn_ExcelIW_Click);
            // 
            // btn_SaveIW
            // 
            this.btn_SaveIW.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_SaveIW.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_SaveIW.Location = new System.Drawing.Point(55, 81);
            this.btn_SaveIW.Name = "btn_SaveIW";
            this.btn_SaveIW.Size = new System.Drawing.Size(45, 23);
            this.btn_SaveIW.TabIndex = 2;
            this.btn_SaveIW.Text = "저 장";
            this.btn_SaveIW.UseVisualStyleBackColor = true;
            this.btn_SaveIW.Click += new System.EventHandler(this.btn_SaveIW_Click);
            // 
            // dgv_IndexWord
            // 
            this.dgv_IndexWord.AllowUserToAddRows = false;
            this.dgv_IndexWord.AllowUserToDeleteRows = false;
            this.dgv_IndexWord.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_IndexWord.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_IndexWord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_IndexWord.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IndexWord,
            this.IndexPersent});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_IndexWord.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_IndexWord.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_IndexWord.Location = new System.Drawing.Point(919, 123);
            this.dgv_IndexWord.MultiSelect = false;
            this.dgv_IndexWord.Name = "dgv_IndexWord";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_IndexWord.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_IndexWord.RowHeadersWidth = 21;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_IndexWord.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_IndexWord.RowTemplate.Height = 23;
            this.dgv_IndexWord.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_IndexWord.Size = new System.Drawing.Size(213, 578);
            this.dgv_IndexWord.TabIndex = 116;
            this.dgv_IndexWord.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_IndexWord_CellClick);
            this.dgv_IndexWord.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_IndexWord_RowPostPaint);
            // 
            // IndexWord
            // 
            this.IndexWord.HeaderText = "색 인 어";
            this.IndexWord.Name = "IndexWord";
            this.IndexWord.Width = 120;
            // 
            // IndexPersent
            // 
            this.IndexPersent.HeaderText = "%";
            this.IndexPersent.Name = "IndexPersent";
            this.IndexPersent.Width = 50;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tb_memo);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.tb_search);
            this.panel1.Controls.Add(this.btn_Add);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.btn_Lookup);
            this.panel1.Controls.Add(this.btn_Delete);
            this.panel1.Controls.Add(this.cb_search_filter);
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Location = new System.Drawing.Point(13, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(899, 41);
            this.panel1.TabIndex = 117;
            // 
            // tb_memo
            // 
            this.tb_memo.Location = new System.Drawing.Point(704, 8);
            this.tb_memo.Name = "tb_memo";
            this.tb_memo.Size = new System.Drawing.Size(75, 23);
            this.tb_memo.TabIndex = 74;
            this.tb_memo.Text = "메모장";
            this.tb_memo.UseVisualStyleBackColor = true;
            this.tb_memo.Click += new System.EventHandler(this.tb_memo_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.rtb_etc);
            this.panel2.Controls.Add(this.tb_site);
            this.panel2.Controls.Add(this.NameLabel);
            this.panel2.Controls.Add(this.tb_sangho);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.tb_zip);
            this.panel2.Controls.Add(this.tb_boss);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.tb_addr);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.tb_bubin);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.tb_uptae);
            this.panel2.Controls.Add(this.lbl_idx);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.tb_jongmok);
            this.panel2.Controls.Add(this.btn_SitePage);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.tb_bank_no);
            this.panel2.Controls.Add(this.checkBox4);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.tb_pw);
            this.panel2.Controls.Add(this.textBox18);
            this.panel2.Controls.Add(this.tb_bank_comp);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.tb_id);
            this.panel2.Controls.Add(this.textBox17);
            this.panel2.Controls.Add(this.chk_stopUse);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.tb_tel);
            this.panel2.Controls.Add(this.chk_email);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.tb_email);
            this.panel2.Controls.Add(this.tb_fax);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.tb_bank_name);
            this.panel2.Controls.Add(this.chk_send);
            this.panel2.Controls.Add(this.cb_gubun);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.tb_barea);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(13, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(899, 257);
            this.panel2.TabIndex = 0;
            // 
            // rtb_etc
            // 
            this.rtb_etc.Location = new System.Drawing.Point(81, 134);
            this.rtb_etc.Name = "rtb_etc";
            this.rtb_etc.Size = new System.Drawing.Size(802, 118);
            this.rtb_etc.TabIndex = 20;
            this.rtb_etc.Text = "";
            // 
            // tb_site
            // 
            this.tb_site.Location = new System.Drawing.Point(493, 110);
            this.tb_site.Name = "tb_site";
            this.tb_site.Size = new System.Drawing.Size(162, 21);
            this.tb_site.TabIndex = 19;
            // 
            // tb_zip
            // 
            this.tb_zip.Location = new System.Drawing.Point(81, 35);
            this.tb_zip.Name = "tb_zip";
            this.tb_zip.Size = new System.Drawing.Size(63, 21);
            this.tb_zip.TabIndex = 5;
            // 
            // lbl_idx
            // 
            this.lbl_idx.AutoSize = true;
            this.lbl_idx.Location = new System.Drawing.Point(783, 114);
            this.lbl_idx.Name = "lbl_idx";
            this.lbl_idx.Size = new System.Drawing.Size(22, 12);
            this.lbl_idx.TabIndex = 54;
            this.lbl_idx.Text = "idx";
            // 
            // btn_SitePage
            // 
            this.btn_SitePage.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_SitePage.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_SitePage.Location = new System.Drawing.Point(661, 109);
            this.btn_SitePage.Name = "btn_SitePage";
            this.btn_SitePage.Size = new System.Drawing.Size(110, 23);
            this.btn_SitePage.TabIndex = 105;
            this.btn_SitePage.Text = "사이트 바로가기";
            this.btn_SitePage.UseVisualStyleBackColor = true;
            this.btn_SitePage.Click += new System.EventHandler(this.btn_SitePage_Click);
            // 
            // tb_pw
            // 
            this.tb_pw.Location = new System.Drawing.Point(273, 110);
            this.tb_pw.Name = "tb_pw";
            this.tb_pw.Size = new System.Drawing.Size(126, 21);
            this.tb_pw.TabIndex = 18;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(215, 114);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 101;
            this.label21.Text = "비밀번호";
            // 
            // tb_id
            // 
            this.tb_id.Location = new System.Drawing.Point(81, 110);
            this.tb_id.Name = "tb_id";
            this.tb_id.Size = new System.Drawing.Size(121, 21);
            this.tb_id.TabIndex = 17;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 114);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 99;
            this.label20.Text = "아이디";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(419, 114);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 12);
            this.label22.TabIndex = 99;
            this.label22.Text = "도메인 주소";
            // 
            // tb_persent
            // 
            this.tb_persent.Location = new System.Drawing.Point(145, 24);
            this.tb_persent.Name = "tb_persent";
            this.tb_persent.Size = new System.Drawing.Size(66, 21);
            this.tb_persent.TabIndex = 1;
            this.tb_persent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_persent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_persent_KeyDown);
            // 
            // btn_batchProcess
            // 
            this.btn_batchProcess.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_batchProcess.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_batchProcess.Location = new System.Drawing.Point(111, 52);
            this.btn_batchProcess.Name = "btn_batchProcess";
            this.btn_batchProcess.Size = new System.Drawing.Size(101, 23);
            this.btn_batchProcess.TabIndex = 106;
            this.btn_batchProcess.Text = "색인어일괄입력";
            this.btn_batchProcess.UseVisualStyleBackColor = true;
            this.btn_batchProcess.Click += new System.EventHandler(this.btn_batchProcess_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.tb_persent);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.btn_batchProcess);
            this.panel3.Controls.Add(this.tb_IndexWord);
            this.panel3.Controls.Add(this.btn_SaveIW);
            this.panel3.Controls.Add(this.btn_DeleteIW);
            this.panel3.Controls.Add(this.btn_ExcelIW);
            this.panel3.Controls.Add(this.btn_AddIW);
            this.panel3.Location = new System.Drawing.Point(919, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(213, 105);
            this.panel3.TabIndex = 118;
            // 
            // panel_batchProcess
            // 
            this.panel_batchProcess.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_batchProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_batchProcess.Controls.Add(this.btn_ProcessExit);
            this.panel_batchProcess.Controls.Add(this.btn_batchProcessing);
            this.panel_batchProcess.Controls.Add(this.dgv_WordSUB);
            this.panel_batchProcess.Location = new System.Drawing.Point(1138, 12);
            this.panel_batchProcess.Name = "panel_batchProcess";
            this.panel_batchProcess.Size = new System.Drawing.Size(221, 429);
            this.panel_batchProcess.TabIndex = 119;
            this.panel_batchProcess.Visible = false;
            // 
            // btn_ProcessExit
            // 
            this.btn_ProcessExit.Location = new System.Drawing.Point(141, 3);
            this.btn_ProcessExit.Name = "btn_ProcessExit";
            this.btn_ProcessExit.Size = new System.Drawing.Size(75, 23);
            this.btn_ProcessExit.TabIndex = 117;
            this.btn_ProcessExit.Text = "나 가 기";
            this.btn_ProcessExit.UseVisualStyleBackColor = true;
            this.btn_ProcessExit.Click += new System.EventHandler(this.btn_ProcessExit_Click);
            // 
            // btn_batchProcessing
            // 
            this.btn_batchProcessing.Location = new System.Drawing.Point(3, 3);
            this.btn_batchProcessing.Name = "btn_batchProcessing";
            this.btn_batchProcessing.Size = new System.Drawing.Size(75, 23);
            this.btn_batchProcessing.TabIndex = 117;
            this.btn_batchProcessing.Text = "일괄입력";
            this.btn_batchProcessing.UseVisualStyleBackColor = true;
            this.btn_batchProcessing.Click += new System.EventHandler(this.btn_batchProcessing_Click);
            // 
            // dgv_WordSUB
            // 
            this.dgv_WordSUB.AllowUserToDeleteRows = false;
            this.dgv_WordSUB.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_WordSUB.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_WordSUB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_WordSUB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IndexWordSub,
            this.IndexPersentSub});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_WordSUB.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_WordSUB.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_WordSUB.Location = new System.Drawing.Point(3, 32);
            this.dgv_WordSUB.MultiSelect = false;
            this.dgv_WordSUB.Name = "dgv_WordSUB";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_WordSUB.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgv_WordSUB.RowHeadersWidth = 21;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_WordSUB.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgv_WordSUB.RowTemplate.Height = 23;
            this.dgv_WordSUB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_WordSUB.Size = new System.Drawing.Size(213, 392);
            this.dgv_WordSUB.TabIndex = 116;
            this.dgv_WordSUB.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_IndexWord_RowPostPaint);
            this.dgv_WordSUB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_IndexWord_KeyDown);
            // 
            // IndexWordSub
            // 
            this.IndexWordSub.HeaderText = "색 인 어";
            this.IndexWordSub.Name = "IndexWordSub";
            this.IndexWordSub.Width = 120;
            // 
            // IndexPersentSub
            // 
            this.IndexPersentSub.HeaderText = "%";
            this.IndexPersentSub.Name = "IndexPersentSub";
            this.IndexPersentSub.Width = 50;
            // 
            // Order_manage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 718);
            this.Controls.Add(this.panel_batchProcess);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgv_IndexWord);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Order_manage";
            this.Text = "주문처 관리";
            this.Load += new System.EventHandler(this.Order_manage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_IndexWord)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_persent)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel_batchProcess.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_WordSUB)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cb_search_filter;
        private System.Windows.Forms.CheckBox chk_stopUse;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_bank_comp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_bank_no;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.Button btn_Lookup;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.TextBox tb_jongmok;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_uptae;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_bubin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_addr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_search;
        private System.Windows.Forms.TextBox tb_boss;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_sangho;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox tb_tel;
        private System.Windows.Forms.TextBox tb_fax;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_bank_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chk_send;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cb_gubun;
        private System.Windows.Forms.TextBox tb_barea;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chk_email;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tb_IndexWord;
        private System.Windows.Forms.Button btn_DeleteIW;
        private System.Windows.Forms.Button btn_AddIW;
        private System.Windows.Forms.Button btn_ExcelIW;
        private System.Windows.Forms.Button btn_SaveIW;
        private System.Windows.Forms.DataGridView dgv_IndexWord;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_SitePage;
        private System.Windows.Forms.TextBox tb_pw;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tb_id;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tb_zip;
        private System.Windows.Forms.TextBox tb_site;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.RichTextBox rtb_etc;
        private System.Windows.Forms.Button tb_memo;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn send_chk;
        private System.Windows.Forms.DataGridViewTextBoxColumn sangho;
        private System.Windows.Forms.DataGridViewTextBoxColumn boss;
        private System.Windows.Forms.DataGridViewTextBoxColumn bubin;
        private System.Windows.Forms.DataGridViewTextBoxColumn uptae;
        private System.Windows.Forms.DataGridViewTextBoxColumn jongmok;
        private System.Windows.Forms.DataGridViewTextBoxColumn zip;
        private System.Windows.Forms.DataGridViewTextBoxColumn addr;
        private System.Windows.Forms.DataGridViewTextBoxColumn tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn fax;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn gu;
        private System.Windows.Forms.DataGridViewTextBoxColumn barea;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn pw;
        private System.Windows.Forms.DataGridViewTextBoxColumn site;
        private System.Windows.Forms.DataGridViewTextBoxColumn emchk;
        private System.Windows.Forms.NumericUpDown tb_persent;
        private System.Windows.Forms.DataGridViewTextBoxColumn IndexWord;
        private System.Windows.Forms.DataGridViewTextBoxColumn IndexPersent;
        private System.Windows.Forms.Label lbl_idx;
        private System.Windows.Forms.Button btn_batchProcess;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel_batchProcess;
        private System.Windows.Forms.DataGridView dgv_WordSUB;
        private System.Windows.Forms.Button btn_ProcessExit;
        private System.Windows.Forms.Button btn_batchProcessing;
        private System.Windows.Forms.DataGridViewTextBoxColumn IndexWordSub;
        private System.Windows.Forms.DataGridViewTextBoxColumn IndexPersentSub;
    }
}