﻿namespace WindowsFormsApp1.Home
{
    partial class _Sub_Search_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sangho = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gubun = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bubin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uptae = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jongmok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.man = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mantel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assumer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.post_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DLSid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DLSpw = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.division = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.program = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.sangho,
            this.gubun,
            this.boss,
            this.bubin,
            this.uptae,
            this.jongmok,
            this.tel,
            this.fax,
            this.mail,
            this.man,
            this.mantel,
            this.assumer,
            this.post_num,
            this.addr,
            this.DLSid,
            this.DLSpw,
            this.division,
            this.label,
            this.program,
            this.bigo});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.RowHeadersWidth = 21;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(898, 225);
            this.dataGridView1.TabIndex = 42;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.ReadOnly = true;
            this.idx.Visible = false;
            // 
            // sangho
            // 
            this.sangho.HeaderText = "거래처명";
            this.sangho.Name = "sangho";
            this.sangho.ReadOnly = true;
            this.sangho.Width = 150;
            // 
            // gubun
            // 
            this.gubun.HeaderText = "구분";
            this.gubun.Name = "gubun";
            this.gubun.ReadOnly = true;
            this.gubun.Width = 80;
            // 
            // boss
            // 
            this.boss.HeaderText = "대표자명";
            this.boss.Name = "boss";
            this.boss.ReadOnly = true;
            // 
            // bubin
            // 
            this.bubin.HeaderText = "등록번호";
            this.bubin.Name = "bubin";
            this.bubin.ReadOnly = true;
            this.bubin.Visible = false;
            // 
            // uptae
            // 
            this.uptae.HeaderText = "업태";
            this.uptae.Name = "uptae";
            this.uptae.ReadOnly = true;
            this.uptae.Visible = false;
            // 
            // jongmok
            // 
            this.jongmok.HeaderText = "종목";
            this.jongmok.Name = "jongmok";
            this.jongmok.ReadOnly = true;
            this.jongmok.Visible = false;
            // 
            // tel
            // 
            this.tel.HeaderText = "전화번호";
            this.tel.Name = "tel";
            this.tel.ReadOnly = true;
            this.tel.Visible = false;
            // 
            // fax
            // 
            this.fax.HeaderText = "팩스";
            this.fax.Name = "fax";
            this.fax.ReadOnly = true;
            this.fax.Visible = false;
            // 
            // mail
            // 
            this.mail.HeaderText = "메일";
            this.mail.Name = "mail";
            this.mail.ReadOnly = true;
            this.mail.Visible = false;
            // 
            // man
            // 
            this.man.HeaderText = "담당자";
            this.man.Name = "man";
            this.man.ReadOnly = true;
            // 
            // mantel
            // 
            this.mantel.HeaderText = "담당자 정보";
            this.mantel.Name = "mantel";
            this.mantel.ReadOnly = true;
            this.mantel.Width = 220;
            // 
            // assumer
            // 
            this.assumer.HeaderText = "담당직원";
            this.assumer.Name = "assumer";
            this.assumer.ReadOnly = true;
            this.assumer.Visible = false;
            // 
            // post_num
            // 
            this.post_num.HeaderText = "우편번호";
            this.post_num.Name = "post_num";
            this.post_num.ReadOnly = true;
            this.post_num.Visible = false;
            // 
            // addr
            // 
            this.addr.HeaderText = "주소";
            this.addr.Name = "addr";
            this.addr.ReadOnly = true;
            this.addr.Width = 200;
            // 
            // DLSid
            // 
            this.DLSid.HeaderText = "아이디";
            this.DLSid.Name = "DLSid";
            this.DLSid.ReadOnly = true;
            this.DLSid.Visible = false;
            // 
            // DLSpw
            // 
            this.DLSpw.HeaderText = "비밀번호";
            this.DLSpw.Name = "DLSpw";
            this.DLSpw.ReadOnly = true;
            this.DLSpw.Visible = false;
            // 
            // division
            // 
            this.division.HeaderText = "용지칸수";
            this.division.Name = "division";
            this.division.ReadOnly = true;
            this.division.Visible = false;
            // 
            // label
            // 
            this.label.HeaderText = "띠라벨";
            this.label.Name = "label";
            this.label.ReadOnly = true;
            this.label.Visible = false;
            // 
            // program
            // 
            this.program.HeaderText = "사용프로그램";
            this.program.Name = "program";
            this.program.ReadOnly = true;
            this.program.Visible = false;
            // 
            // bigo
            // 
            this.bigo.HeaderText = "비고";
            this.bigo.Name = "bigo";
            this.bigo.ReadOnly = true;
            this.bigo.Visible = false;
            // 
            // _Sub_Search_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 225);
            this.Controls.Add(this.dataGridView1);
            this.Name = "_Sub_Search_Form";
            this.Text = "_Sub_Search_Form";
            this.Load += new System.EventHandler(this._Sub_Search_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn sangho;
        private System.Windows.Forms.DataGridViewTextBoxColumn gubun;
        private System.Windows.Forms.DataGridViewTextBoxColumn boss;
        private System.Windows.Forms.DataGridViewTextBoxColumn bubin;
        private System.Windows.Forms.DataGridViewTextBoxColumn uptae;
        private System.Windows.Forms.DataGridViewTextBoxColumn jongmok;
        private System.Windows.Forms.DataGridViewTextBoxColumn tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn fax;
        private System.Windows.Forms.DataGridViewTextBoxColumn mail;
        private System.Windows.Forms.DataGridViewTextBoxColumn man;
        private System.Windows.Forms.DataGridViewTextBoxColumn mantel;
        private System.Windows.Forms.DataGridViewTextBoxColumn assumer;
        private System.Windows.Forms.DataGridViewTextBoxColumn post_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn addr;
        private System.Windows.Forms.DataGridViewTextBoxColumn DLSid;
        private System.Windows.Forms.DataGridViewTextBoxColumn DLSpw;
        private System.Windows.Forms.DataGridViewTextBoxColumn division;
        private System.Windows.Forms.DataGridViewTextBoxColumn label;
        private System.Windows.Forms.DataGridViewTextBoxColumn program;
        private System.Windows.Forms.DataGridViewTextBoxColumn bigo;
    }
}