﻿namespace WindowsFormsApp1.Home
{
    partial class Transaction_manage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            this.NameLabel = new System.Windows.Forms.Label();
            this.tb_sangho = new System.Windows.Forms.TextBox();
            this.tb_addr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_man = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_fax = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_tel = new System.Windows.Forms.TextBox();
            this.cb_gubun2 = new System.Windows.Forms.ComboBox();
            this.rtb_etc = new System.Windows.Forms.RichTextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sangho = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gubun = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bubin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uptae = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jongmok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.man = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mantel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assumer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.post_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dlsArea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DLSid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DLSpw = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.division = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.program = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tb_man1 = new System.Windows.Forms.TextBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.tb_id = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_pw = new System.Windows.Forms.TextBox();
            this.tb_zip = new System.Windows.Forms.TextBox();
            this.tb_boss = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_bubin = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_uptae = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_jongmok = new System.Windows.Forms.TextBox();
            this.tb_mail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_user = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_division = new System.Windows.Forms.TextBox();
            this.tb_label = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_program = new System.Windows.Forms.TextBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.cb_gubun1 = new System.Windows.Forms.ComboBox();
            this.btn_marc_expand = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_idx = new System.Windows.Forms.Label();
            this.cb_dlsArea = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.marcGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marcLabel = new System.Windows.Forms.Label();
            this.gearLabel = new System.Windows.Forms.Label();
            this.gearGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tb_marcEmpty = new System.Windows.Forms.Button();
            this.tb_gearEmpty = new System.Windows.Forms.Button();
            this.tb_marcSave = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marcGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gearGrid)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.NameLabel.Location = new System.Drawing.Point(21, 10);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(49, 12);
            this.NameLabel.TabIndex = 0;
            this.NameLabel.Text = "업 체 명";
            // 
            // tb_sangho
            // 
            this.tb_sangho.Location = new System.Drawing.Point(74, 6);
            this.tb_sangho.Name = "tb_sangho";
            this.tb_sangho.Size = new System.Drawing.Size(138, 21);
            this.tb_sangho.TabIndex = 1;
            this.tb_sangho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_addr
            // 
            this.tb_addr.Location = new System.Drawing.Point(164, 102);
            this.tb_addr.Name = "tb_addr";
            this.tb_addr.Size = new System.Drawing.Size(388, 21);
            this.tb_addr.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(19, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 24;
            this.label3.Text = "주      소";
            // 
            // tb_man
            // 
            this.tb_man.Location = new System.Drawing.Point(74, 78);
            this.tb_man.Name = "tb_man";
            this.tb_man.Size = new System.Drawing.Size(194, 21);
            this.tb_man.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(3, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 12);
            this.label4.TabIndex = 19;
            this.label4.Text = "담당자 정보";
            // 
            // tb_fax
            // 
            this.tb_fax.Location = new System.Drawing.Point(358, 54);
            this.tb_fax.Name = "tb_fax";
            this.tb_fax.Size = new System.Drawing.Size(194, 21);
            this.tb_fax.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(303, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "팩      스";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(21, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 12);
            this.label7.TabIndex = 27;
            this.label7.Text = "아 이 디";
            // 
            // btn_delete
            // 
            this.btn_delete.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_delete.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_delete.Location = new System.Drawing.Point(852, 129);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 34);
            this.btn_delete.TabIndex = 42;
            this.btn_delete.Text = "삭  제";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_save
            // 
            this.btn_save.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_save.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_save.Location = new System.Drawing.Point(852, 67);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 34);
            this.btn_save.TabIndex = 40;
            this.btn_save.Text = "저  장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_add
            // 
            this.btn_add.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_add.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_add.Location = new System.Drawing.Point(852, 5);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(75, 34);
            this.btn_add.TabIndex = 39;
            this.btn_add.Text = "추  가";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(303, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "구      분";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(19, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "전화번호";
            // 
            // tb_tel
            // 
            this.tb_tel.Location = new System.Drawing.Point(74, 54);
            this.tb_tel.Name = "tb_tel";
            this.tb_tel.Size = new System.Drawing.Size(194, 21);
            this.tb_tel.TabIndex = 14;
            // 
            // cb_gubun2
            // 
            this.cb_gubun2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gubun2.FormattingEnabled = true;
            this.cb_gubun2.Location = new System.Drawing.Point(457, 6);
            this.cb_gubun2.Name = "cb_gubun2";
            this.cb_gubun2.Size = new System.Drawing.Size(95, 20);
            this.cb_gubun2.TabIndex = 4;
            // 
            // rtb_etc
            // 
            this.rtb_etc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtb_etc.Location = new System.Drawing.Point(74, 174);
            this.rtb_etc.Name = "rtb_etc";
            this.rtb_etc.Size = new System.Drawing.Size(762, 186);
            this.rtb_etc.TabIndex = 38;
            this.rtb_etc.Text = "";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.sangho,
            this.gubun,
            this.boss,
            this.bubin,
            this.uptae,
            this.jongmok,
            this.tel,
            this.fax,
            this.mail,
            this.man,
            this.mantel,
            this.assumer,
            this.post_num,
            this.addr,
            this.dlsArea,
            this.DLSid,
            this.DLSpw,
            this.division,
            this.label,
            this.program,
            this.bigo});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridView1.RowHeadersWidth = 21;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(967, 475);
            this.dataGridView1.TabIndex = 44;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.ReadOnly = true;
            this.idx.Visible = false;
            // 
            // sangho
            // 
            this.sangho.HeaderText = "업체명";
            this.sangho.Name = "sangho";
            this.sangho.ReadOnly = true;
            this.sangho.Width = 150;
            // 
            // gubun
            // 
            this.gubun.HeaderText = "구분";
            this.gubun.Name = "gubun";
            this.gubun.ReadOnly = true;
            this.gubun.Width = 80;
            // 
            // boss
            // 
            this.boss.HeaderText = "대표자명";
            this.boss.Name = "boss";
            this.boss.ReadOnly = true;
            // 
            // bubin
            // 
            this.bubin.HeaderText = "등록번호";
            this.bubin.Name = "bubin";
            this.bubin.ReadOnly = true;
            // 
            // uptae
            // 
            this.uptae.HeaderText = "업태";
            this.uptae.Name = "uptae";
            this.uptae.ReadOnly = true;
            // 
            // jongmok
            // 
            this.jongmok.HeaderText = "종목";
            this.jongmok.Name = "jongmok";
            this.jongmok.ReadOnly = true;
            // 
            // tel
            // 
            this.tel.HeaderText = "전화번호";
            this.tel.Name = "tel";
            this.tel.ReadOnly = true;
            // 
            // fax
            // 
            this.fax.HeaderText = "팩스";
            this.fax.Name = "fax";
            this.fax.ReadOnly = true;
            // 
            // mail
            // 
            this.mail.HeaderText = "메일";
            this.mail.Name = "mail";
            this.mail.ReadOnly = true;
            // 
            // man
            // 
            this.man.HeaderText = "담당자";
            this.man.Name = "man";
            this.man.ReadOnly = true;
            this.man.Width = 80;
            // 
            // mantel
            // 
            this.mantel.HeaderText = "담당자 정보";
            this.mantel.Name = "mantel";
            this.mantel.ReadOnly = true;
            this.mantel.Width = 160;
            // 
            // assumer
            // 
            this.assumer.HeaderText = "담당직원";
            this.assumer.Name = "assumer";
            this.assumer.ReadOnly = true;
            // 
            // post_num
            // 
            this.post_num.HeaderText = "우편번호";
            this.post_num.Name = "post_num";
            this.post_num.ReadOnly = true;
            // 
            // addr
            // 
            this.addr.HeaderText = "주소";
            this.addr.Name = "addr";
            this.addr.ReadOnly = true;
            this.addr.Width = 200;
            // 
            // dlsArea
            // 
            this.dlsArea.HeaderText = "DLS지역";
            this.dlsArea.Name = "dlsArea";
            this.dlsArea.ReadOnly = true;
            // 
            // DLSid
            // 
            this.DLSid.HeaderText = "아이디";
            this.DLSid.Name = "DLSid";
            this.DLSid.ReadOnly = true;
            // 
            // DLSpw
            // 
            this.DLSpw.HeaderText = "비밀번호";
            this.DLSpw.Name = "DLSpw";
            this.DLSpw.ReadOnly = true;
            // 
            // division
            // 
            this.division.HeaderText = "용지칸수";
            this.division.Name = "division";
            this.division.ReadOnly = true;
            // 
            // label
            // 
            this.label.HeaderText = "띠라벨";
            this.label.Name = "label";
            this.label.ReadOnly = true;
            // 
            // program
            // 
            this.program.HeaderText = "사용프로그램";
            this.program.Name = "program";
            this.program.ReadOnly = true;
            // 
            // bigo
            // 
            this.bigo.HeaderText = "비고";
            this.bigo.Name = "bigo";
            this.bigo.ReadOnly = true;
            this.bigo.Visible = false;
            // 
            // tb_man1
            // 
            this.tb_man1.Location = new System.Drawing.Point(271, 78);
            this.tb_man1.Name = "tb_man1";
            this.tb_man1.Size = new System.Drawing.Size(281, 21);
            this.tb_man1.TabIndex = 21;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(214, 5);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 2;
            this.btn_search.Text = "검  색";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // tb_id
            // 
            this.tb_id.Location = new System.Drawing.Point(74, 126);
            this.tb_id.Name = "tb_id";
            this.tb_id.Size = new System.Drawing.Size(194, 21);
            this.tb_id.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(19, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 37;
            this.label5.Text = "비      고";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(303, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 29;
            this.label8.Text = "비밀번호";
            // 
            // tb_pw
            // 
            this.tb_pw.Location = new System.Drawing.Point(358, 126);
            this.tb_pw.Name = "tb_pw";
            this.tb_pw.Size = new System.Drawing.Size(194, 21);
            this.tb_pw.TabIndex = 30;
            // 
            // tb_zip
            // 
            this.tb_zip.Location = new System.Drawing.Point(74, 102);
            this.tb_zip.Name = "tb_zip";
            this.tb_zip.Size = new System.Drawing.Size(87, 21);
            this.tb_zip.TabIndex = 25;
            // 
            // tb_boss
            // 
            this.tb_boss.Location = new System.Drawing.Point(642, 6);
            this.tb_boss.Name = "tb_boss";
            this.tb_boss.Size = new System.Drawing.Size(194, 21);
            this.tb_boss.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(587, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 5;
            this.label9.Text = "대표자명";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(19, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 7;
            this.label10.Text = "등록번호";
            // 
            // tb_bubin
            // 
            this.tb_bubin.Location = new System.Drawing.Point(74, 30);
            this.tb_bubin.Name = "tb_bubin";
            this.tb_bubin.Size = new System.Drawing.Size(138, 21);
            this.tb_bubin.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(303, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 9;
            this.label11.Text = "업      태";
            // 
            // tb_uptae
            // 
            this.tb_uptae.Location = new System.Drawing.Point(358, 30);
            this.tb_uptae.Name = "tb_uptae";
            this.tb_uptae.Size = new System.Drawing.Size(194, 21);
            this.tb_uptae.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(587, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 11;
            this.label12.Text = "종      목";
            // 
            // tb_jongmok
            // 
            this.tb_jongmok.Location = new System.Drawing.Point(642, 30);
            this.tb_jongmok.Name = "tb_jongmok";
            this.tb_jongmok.Size = new System.Drawing.Size(194, 21);
            this.tb_jongmok.TabIndex = 12;
            // 
            // tb_mail
            // 
            this.tb_mail.Location = new System.Drawing.Point(642, 54);
            this.tb_mail.Name = "tb_mail";
            this.tb_mail.Size = new System.Drawing.Size(194, 21);
            this.tb_mail.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(587, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 17;
            this.label13.Text = "메      일";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(587, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 22;
            this.label14.Text = "담당직원";
            // 
            // tb_user
            // 
            this.tb_user.Location = new System.Drawing.Point(642, 78);
            this.tb_user.Name = "tb_user";
            this.tb_user.Size = new System.Drawing.Size(194, 21);
            this.tb_user.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label15.Location = new System.Drawing.Point(19, 154);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 31;
            this.label15.Text = "용지칸수";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(305, 154);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 12);
            this.label16.TabIndex = 33;
            this.label16.Text = "띠 라 벨";
            // 
            // tb_division
            // 
            this.tb_division.Location = new System.Drawing.Point(74, 150);
            this.tb_division.Name = "tb_division";
            this.tb_division.Size = new System.Drawing.Size(194, 21);
            this.tb_division.TabIndex = 32;
            // 
            // tb_label
            // 
            this.tb_label.Location = new System.Drawing.Point(358, 150);
            this.tb_label.Name = "tb_label";
            this.tb_label.Size = new System.Drawing.Size(194, 21);
            this.tb_label.TabIndex = 34;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(563, 154);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 12);
            this.label17.TabIndex = 35;
            this.label17.Text = "사용프로그램";
            // 
            // tb_program
            // 
            this.tb_program.Location = new System.Drawing.Point(642, 150);
            this.tb_program.Name = "tb_program";
            this.tb_program.Size = new System.Drawing.Size(194, 21);
            this.tb_program.TabIndex = 36;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(852, 191);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 34);
            this.btn_close.TabIndex = 43;
            this.btn_close.Text = "닫  기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // cb_gubun1
            // 
            this.cb_gubun1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gubun1.FormattingEnabled = true;
            this.cb_gubun1.Location = new System.Drawing.Point(358, 6);
            this.cb_gubun1.Name = "cb_gubun1";
            this.cb_gubun1.Size = new System.Drawing.Size(95, 20);
            this.cb_gubun1.TabIndex = 45;
            this.cb_gubun1.SelectedIndexChanged += new System.EventHandler(this.cb_gubun1_SelectedIndexChanged);
            // 
            // btn_marc_expand
            // 
            this.btn_marc_expand.Location = new System.Drawing.Point(852, 253);
            this.btn_marc_expand.Name = "btn_marc_expand";
            this.btn_marc_expand.Size = new System.Drawing.Size(75, 74);
            this.btn_marc_expand.TabIndex = 46;
            this.btn_marc_expand.Text = "마크 비고 >>";
            this.btn_marc_expand.UseVisualStyleBackColor = true;
            this.btn_marc_expand.Click += new System.EventHandler(this.btn_marc_expand_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(969, 844);
            this.panel1.TabIndex = 47;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 367);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(967, 475);
            this.panel3.TabIndex = 49;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lbl_idx);
            this.panel2.Controls.Add(this.NameLabel);
            this.panel2.Controls.Add(this.cb_dlsArea);
            this.panel2.Controls.Add(this.btn_add);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.tb_sangho);
            this.panel2.Controls.Add(this.btn_save);
            this.panel2.Controls.Add(this.btn_marc_expand);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cb_gubun1);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btn_delete);
            this.panel2.Controls.Add(this.btn_close);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.cb_gubun2);
            this.panel2.Controls.Add(this.tb_tel);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.tb_bubin);
            this.panel2.Controls.Add(this.rtb_etc);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.tb_jongmok);
            this.panel2.Controls.Add(this.btn_search);
            this.panel2.Controls.Add(this.tb_addr);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.tb_uptae);
            this.panel2.Controls.Add(this.tb_id);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.tb_user);
            this.panel2.Controls.Add(this.tb_division);
            this.panel2.Controls.Add(this.tb_man);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.tb_mail);
            this.panel2.Controls.Add(this.tb_pw);
            this.panel2.Controls.Add(this.tb_man1);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.tb_boss);
            this.panel2.Controls.Add(this.tb_label);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.tb_zip);
            this.panel2.Controls.Add(this.tb_program);
            this.panel2.Controls.Add(this.tb_fax);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(967, 367);
            this.panel2.TabIndex = 48;
            // 
            // lbl_idx
            // 
            this.lbl_idx.AutoSize = true;
            this.lbl_idx.Location = new System.Drawing.Point(21, 233);
            this.lbl_idx.Name = "lbl_idx";
            this.lbl_idx.Size = new System.Drawing.Size(0, 12);
            this.lbl_idx.TabIndex = 48;
            // 
            // cb_dlsArea
            // 
            this.cb_dlsArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_dlsArea.FormattingEnabled = true;
            this.cb_dlsArea.Location = new System.Drawing.Point(642, 126);
            this.cb_dlsArea.Name = "cb_dlsArea";
            this.cb_dlsArea.Size = new System.Drawing.Size(194, 20);
            this.cb_dlsArea.TabIndex = 47;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(587, 130);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 12);
            this.label18.TabIndex = 24;
            this.label18.Text = "DLS지역";
            // 
            // marcGrid
            // 
            this.marcGrid.AllowUserToAddRows = false;
            this.marcGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.marcGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.marcGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.marcGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.marcGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.marcGrid.Location = new System.Drawing.Point(0, 0);
            this.marcGrid.Name = "marcGrid";
            this.marcGrid.RowHeadersWidth = 31;
            this.marcGrid.RowTemplate.Height = 23;
            this.marcGrid.Size = new System.Drawing.Size(830, 502);
            this.marcGrid.TabIndex = 48;
            this.marcGrid.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.marcGrid_RowPostPaint);
            this.marcGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grid_KeyDown);
            // 
            // Column1
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle28;
            this.Column1.HeaderText = "내용";
            this.Column1.Name = "Column1";
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "내용";
            this.Column2.Name = "Column2";
            this.Column2.Width = 450;
            // 
            // Column3
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle29;
            this.Column3.HeaderText = "내용";
            this.Column3.Name = "Column3";
            this.Column3.Width = 150;
            // 
            // marcLabel
            // 
            this.marcLabel.AutoSize = true;
            this.marcLabel.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.marcLabel.ForeColor = System.Drawing.Color.Red;
            this.marcLabel.Location = new System.Drawing.Point(9, 6);
            this.marcLabel.Name = "marcLabel";
            this.marcLabel.Size = new System.Drawing.Size(130, 21);
            this.marcLabel.TabIndex = 49;
            this.marcLabel.Text = "마 크  부 분";
            // 
            // gearLabel
            // 
            this.gearLabel.AutoSize = true;
            this.gearLabel.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gearLabel.ForeColor = System.Drawing.Color.Red;
            this.gearLabel.Location = new System.Drawing.Point(9, 6);
            this.gearLabel.Name = "gearLabel";
            this.gearLabel.Size = new System.Drawing.Size(130, 21);
            this.gearLabel.TabIndex = 49;
            this.gearLabel.Text = "장 비  부 분";
            // 
            // gearGrid
            // 
            this.gearGrid.AllowUserToAddRows = false;
            this.gearGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gearGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.gearGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gearGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.gearGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gearGrid.Location = new System.Drawing.Point(0, 0);
            this.gearGrid.Name = "gearGrid";
            this.gearGrid.RowHeadersWidth = 31;
            this.gearGrid.RowTemplate.Height = 23;
            this.gearGrid.Size = new System.Drawing.Size(830, 272);
            this.gearGrid.TabIndex = 48;
            this.gearGrid.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.marcGrid_RowPostPaint);
            this.gearGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grid_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridViewTextBoxColumn1.HeaderText = "내용";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "내용";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 450;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewTextBoxColumn3.HeaderText = "내용";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // tb_marcEmpty
            // 
            this.tb_marcEmpty.Location = new System.Drawing.Point(719, 5);
            this.tb_marcEmpty.Name = "tb_marcEmpty";
            this.tb_marcEmpty.Size = new System.Drawing.Size(101, 23);
            this.tb_marcEmpty.TabIndex = 50;
            this.tb_marcEmpty.Text = "마크 내용지우기";
            this.tb_marcEmpty.UseVisualStyleBackColor = true;
            this.tb_marcEmpty.Click += new System.EventHandler(this.tb_Empty_Click);
            // 
            // tb_gearEmpty
            // 
            this.tb_gearEmpty.Location = new System.Drawing.Point(717, 5);
            this.tb_gearEmpty.Name = "tb_gearEmpty";
            this.tb_gearEmpty.Size = new System.Drawing.Size(101, 23);
            this.tb_gearEmpty.TabIndex = 50;
            this.tb_gearEmpty.Text = "장비 내용지우기";
            this.tb_gearEmpty.UseVisualStyleBackColor = true;
            this.tb_gearEmpty.Click += new System.EventHandler(this.tb_Empty_Click);
            // 
            // tb_marcSave
            // 
            this.tb_marcSave.Location = new System.Drawing.Point(140, 5);
            this.tb_marcSave.Name = "tb_marcSave";
            this.tb_marcSave.Size = new System.Drawing.Size(56, 23);
            this.tb_marcSave.TabIndex = 50;
            this.tb_marcSave.Text = "저 장";
            this.tb_marcSave.UseVisualStyleBackColor = true;
            this.tb_marcSave.Click += new System.EventHandler(this.tb_marcSave_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(969, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(830, 844);
            this.panel4.TabIndex = 51;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.gearGrid);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 572);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(830, 272);
            this.panel8.TabIndex = 54;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.gearLabel);
            this.panel7.Controls.Add(this.tb_gearEmpty);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 537);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(830, 35);
            this.panel7.TabIndex = 53;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.marcGrid);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 35);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(830, 502);
            this.panel6.TabIndex = 52;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.marcLabel);
            this.panel5.Controls.Add(this.tb_marcEmpty);
            this.panel5.Controls.Add(this.tb_marcSave);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(830, 35);
            this.panel5.TabIndex = 51;
            // 
            // Transaction_manage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1799, 844);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Name = "Transaction_manage";
            this.Text = "납품 / 거래처 관리";
            this.Load += new System.EventHandler(this.Transaction_manage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marcGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gearGrid)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_search;
        public System.Windows.Forms.TextBox tb_sangho;
        public System.Windows.Forms.TextBox tb_addr;
        public System.Windows.Forms.TextBox tb_man;
        public System.Windows.Forms.TextBox tb_fax;
        public System.Windows.Forms.TextBox tb_tel;
        public System.Windows.Forms.ComboBox cb_gubun2;
        public System.Windows.Forms.RichTextBox rtb_etc;
        public System.Windows.Forms.TextBox tb_man1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox tb_id;
        public System.Windows.Forms.TextBox tb_pw;
        public System.Windows.Forms.TextBox tb_zip;
        public System.Windows.Forms.TextBox tb_boss;
        public System.Windows.Forms.TextBox tb_bubin;
        public System.Windows.Forms.TextBox tb_uptae;
        public System.Windows.Forms.TextBox tb_jongmok;
        public System.Windows.Forms.TextBox tb_mail;
        public System.Windows.Forms.TextBox tb_user;
        public System.Windows.Forms.TextBox tb_division;
        public System.Windows.Forms.TextBox tb_label;
        public System.Windows.Forms.TextBox tb_program;
        private System.Windows.Forms.Button btn_close;
        public System.Windows.Forms.ComboBox cb_gubun1;
        private System.Windows.Forms.Button btn_marc_expand;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView marcGrid;
        private System.Windows.Forms.Label marcLabel;
        private System.Windows.Forms.Label gearLabel;
        private System.Windows.Forms.DataGridView gearGrid;
        private System.Windows.Forms.Button tb_marcEmpty;
        private System.Windows.Forms.Button tb_gearEmpty;
        private System.Windows.Forms.Button tb_marcSave;
        private System.Windows.Forms.ComboBox cb_dlsArea;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn sangho;
        private System.Windows.Forms.DataGridViewTextBoxColumn gubun;
        private System.Windows.Forms.DataGridViewTextBoxColumn boss;
        private System.Windows.Forms.DataGridViewTextBoxColumn bubin;
        private System.Windows.Forms.DataGridViewTextBoxColumn uptae;
        private System.Windows.Forms.DataGridViewTextBoxColumn jongmok;
        private System.Windows.Forms.DataGridViewTextBoxColumn tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn fax;
        private System.Windows.Forms.DataGridViewTextBoxColumn mail;
        private System.Windows.Forms.DataGridViewTextBoxColumn man;
        private System.Windows.Forms.DataGridViewTextBoxColumn mantel;
        private System.Windows.Forms.DataGridViewTextBoxColumn assumer;
        private System.Windows.Forms.DataGridViewTextBoxColumn post_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn addr;
        private System.Windows.Forms.DataGridViewTextBoxColumn dlsArea;
        private System.Windows.Forms.DataGridViewTextBoxColumn DLSid;
        private System.Windows.Forms.DataGridViewTextBoxColumn DLSpw;
        private System.Windows.Forms.DataGridViewTextBoxColumn division;
        private System.Windows.Forms.DataGridViewTextBoxColumn label;
        private System.Windows.Forms.DataGridViewTextBoxColumn program;
        private System.Windows.Forms.DataGridViewTextBoxColumn bigo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl_idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel8;
    }
}