﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Home
{
    public partial class Home_User_manage : Form
    {
        public string User_Name { get; internal set; }
        bool overlap = false;
        Helper_DB _DB = new Helper_DB();
        Main main;
        string comp_name = string.Empty;
        string Table_User = "User_Data";

        public Home_User_manage(Main _main)
        {
            InitializeComponent();
            main = _main;
            comp_name = main.toolStripLabel2.Text;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            _DB.DBcon();
            tb_Affil.Text = comp_name;
            tb_Affil.Enabled = false;
            btn_lookup_Click(null, null);
        }
        /// <summary>
        /// DB에 저장된 사용자 데이터를 dataGridView1로 입력하는 함수.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_lookup_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string Area = "`name`, `position`, `Phone`, `ID`, `PW`";
            string cmd = _DB.DB_Select_Search(Area, "User_Data", "affil", comp_name);
            string db_res = _DB.DB_Send_CMD_Search(cmd);
            string[] data = db_res.Split('|');
            string[] grid = { "", "", "", "", "", "" };
            for (int a = 0; a < data.Length; a++)
            {
                if (a % 5 == 0) { grid[0] = data[a]; }
                if (a % 5 == 1) { grid[1] = data[a]; }
                if (a % 5 == 2) { grid[2] = data[a]; }
                if (a % 5 == 3) { grid[3] = data[a]; }
                if (a % 5 == 4) { grid[4] = data[a];
                    dataGridView1.Rows.Add(grid);
                }
            }
        }

        private void chk_Div_ListInput_Click(object sender, EventArgs e)
        {
            
        }

        private void chk_Enabled(object sender, EventArgs e)
        {
            CheckBox Target = sender as CheckBox;

            bool IsCheck = Target.Checked;

            CheckBox[] Enabled = { chk_Div, chk_Acc, chk_Marc, chk_Manage };

            CheckBox[] Div = {
                chk_Div_ListInput, chk_Div_ListLookup, chk_Div_ListTotal, chk_Div_OrderInput, chk_Div_Inven,
                chk_Div_Stock, chk_Div_Return 
            };
            CheckBox[] Acc = { 
                chk_Acc_SendMoneyList, chk_Acc_SendMoneyInput, chk_Acc_Buy, chk_Acc_Sales, chk_Acc_PartTime
            };
            CheckBox[] Marc = {
                chk_Marc_Setup, chk_Marc_Work, chk_Marc_Input, chk_Marc_CopyCheck, chk_Marc_Option,
                chk_Marc_DLS, chk_Marc_ETC
            };
            CheckBox[] Manage = {
                chk_Manage_User, chk_Manage_Client, chk_Manage_Purchase, chk_Manage_Book
            };

            for(int a= 0; a < Enabled.Length; a++)
            {
                if (Target == Enabled[a])
                {
                    if (a % 4 == 0) { Enable_Sub(Div, IsCheck); }
                    if (a % 4 == 1) { Enable_Sub(Acc, IsCheck); }
                    if (a % 4 == 2) { Enable_Sub(Marc, IsCheck); }
                    if (a % 4 == 3) { Enable_Sub(Manage, IsCheck); }
                }
            }
        }

        private void Enable_Sub(CheckBox[] Target, bool IsCheck)
        {
            for (int a = 0; a < Target.Length; a++)
            {
                Target[a].Checked = IsCheck;
            }
        }

        /// <summary>
        /// 권한 설정 0일경우 체크없음, 1일경우 활성화만, 2일경우 활성화와 수정체크
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;

            if (row < 0) return;

            tb_ID.Text = dataGridView1.Rows[row].Cells["ID"].Value.ToString();
            tb_PW.Text = dataGridView1.Rows[row].Cells["PW"].Value.ToString();
            tb_Name.Text = dataGridView1.Rows[row].Cells["Per_Name"].Value.ToString();
            tb_Affil.Text = comp_name;
            tb_position.Text = dataGridView1.Rows[row].Cells["Rank"].Value.ToString();
            tb_Phone.Text = dataGridView1.Rows[row].Cells["Phone"].Value.ToString();

            User_Access_Set();
        }
        #region CellClick_Sub
        private void User_Access_Set()
        {
            CheckBox[] chkBox = {
                chk_Div_ListInput, chk_Div_ListLookup, chk_Div_ListTotal, chk_Div_OrderInput, chk_Div_Inven,
                chk_Div_Stock, chk_Div_Return,

                chk_Acc_SendMoneyList, chk_Acc_SendMoneyInput, chk_Acc_Buy, chk_Acc_Sales, chk_Acc_PartTime,

                chk_Marc_Setup, chk_Marc_Work, chk_Marc_Input, chk_Marc_CopyCheck, chk_Marc_Option,
                chk_Marc_DLS, chk_Marc_ETC,

                chk_Manage_User, chk_Manage_Client, chk_Manage_Purchase, chk_Manage_Book
            };


            string Area =
                "`Div_ListInput`, `Div_ListLookup`, `Div_ListTotal`, `Div_OrderInput`, `Div_Inven`," +
                "`Div_Stock`, `Div_Return`," +

                "`Acc_SendMoneyList`, `Acc_SendMoneyInput`, `Acc_Buy`, `Acc_Sales`, `Acc_PartTime`," +

                "`Marc_Setup`, `Marc_Work`, `Marc_Input`, `Marc_CopyCheck`, `Marc_Option`," +
                "`Marc_DLS`, `Marc_ETC`," +

                "`Manage_User`, `Manage_Client`, `Manage_Purchase`, `Manage_Book`";
            string cmd = _DB.DB_Select_Search(Area, "User_Access", "id", tb_ID.Text);
            string res = _DB.DB_Send_CMD_Search(cmd);
            string[] IsCheck = res.Split('|');

            for (int a = 0; a < chkBox.Length; a++)
            {
                if (IsCheck[a] == "1")
                    chkBox[a].Checked = true;
                else
                    chkBox[a].Checked = false;
            }
        }
        #endregion

        private void btn_IDOverlap_Click(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(tb_ID.Text, @"^[a-zA-Z0-9]"))
            {
                MessageBox.Show("영어와 숫자 조합으로 작성해주세요!");
                overlap = false;
                return;
            }

            string cmd = _DB.DB_Search(Table_User, "id", tb_ID.Text);
            string db_res = _DB.DB_Send_CMD_Search(cmd);
            if (db_res == "") { MessageBox.Show("사용가능한 아이디입니다. [" + tb_ID.Text + "]"); overlap = true; }
            else { MessageBox.Show("중복된 아이디입니다. [" + tb_ID.Text + "]"); overlap = false; }
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (tb_ID.Text == "" || tb_PW.Text == "" || tb_Name.Text == "") {
                MessageBox.Show("성명 / 아이디 / 비밀번호 를 채워주세요.");
                return;
            }

            if (!CheckSelect()) {
                if (!overlap) {
                    MessageBox.Show("아이디 중복확인을 해주세요.");
                    return;
                }
            }

            bool IsUpdate = true;
            string cmd = _DB.DB_Search(Table_User, "id", tb_ID.Text);
            string db_res = _DB.DB_Send_CMD_Search(cmd);
            if (db_res == "") IsUpdate = false;

            CheckBox[] chkBox = {
                chk_Div_ListInput, chk_Div_ListLookup, chk_Div_ListTotal, chk_Div_OrderInput, chk_Div_Inven,
                chk_Div_Stock, chk_Div_Return,

                chk_Acc_SendMoneyList, chk_Acc_SendMoneyInput, chk_Acc_Buy, chk_Acc_Sales, chk_Acc_PartTime,

                chk_Marc_Setup, chk_Marc_Work, chk_Marc_Input, chk_Marc_CopyCheck, chk_Marc_Option,
                chk_Marc_DLS, chk_Marc_ETC,

                chk_Manage_User, chk_Manage_Client, chk_Manage_Purchase, chk_Manage_Book
            };

            string[] Update_Col = { 
                "id", "Div_ListInput", "Div_ListLookup", "Div_ListTotal", "Div_OrderInput", "Div_Inven",
                "Div_Stock", "Div_Return",

                "Acc_SendMoneyList", "Acc_SendMoneyInput", "Acc_Buy", "Acc_Sales", "Acc_PartTime",

                "Marc_Setup", "Marc_Work", "Marc_Input", "Marc_CopyCheck", "Marc_Option",
                "Marc_DLS", "Marc_ETC",

                "Manage_User", "Manage_Client", "Manage_Purchase", "Manage_Book"
            };

            List<string> tmp_Update_data = new List<string>();
            tmp_Update_data.Add(tb_ID.Text);
            for (int a = 0; a < chkBox.Length; a++)
            {
                bool IsCheck = chkBox[a].Checked;
                string tmp_data = "0";
                if (IsCheck)
                    tmp_data = "1";

                tmp_Update_data.Add(tmp_data);
            }

            string[] Update_Data = tmp_Update_data.ToArray();

            if (!IsUpdate)
            {
                string[] Insert_Col = { "ID", "PW", "name", "affil", "position", "Phone" };
                string[] Insert_Data = { tb_ID.Text, tb_PW.Text, tb_Name.Text, tb_Affil.Text, tb_position.Text, tb_Phone.Text };

                cmd = _DB.DB_INSERT("User_Data", Insert_Col, Insert_Data);
                _DB.DB_Send_CMD_reVoid(cmd);
                cmd = _DB.DB_INSERT("User_Access", Update_Col, Update_Data);
                _DB.DB_Send_CMD_reVoid(cmd);
                cmd = string.Format("INSERT INTO `User_ShortCut` (`id`) VALUES ('{0}')", tb_ID.Text);
                _DB.DB_Send_CMD_reVoid(cmd);
            }
            else
            {
                string[] Search_Col = { "ID" };
                string[] Search_Data = { tb_ID.Text };
                string[] Update1_Col = { "ID", "PW", "name", "affil", "position", "Phone" };
                string[] Update1_Data = { tb_ID.Text, tb_PW.Text, tb_Name.Text, tb_Affil.Text, tb_position.Text, tb_Phone.Text };

                cmd = _DB.More_Update("User_Data", Update1_Col, Update1_Data, Search_Col, Search_Data);
                _DB.DB_Send_CMD_reVoid(cmd);
                cmd = _DB.More_Update("User_Access", Update_Col, Update_Data, Search_Col, Search_Data);
                _DB.DB_Send_CMD_reVoid(cmd);
            }

            MessageBox.Show("저장되었습니다!");
            main.isAccess();
            btn_lookup_Click(null, null);
        }

        private bool CheckSelect()
        {
            int row = dataGridView1.CurrentCell.RowIndex;

            string[] GridData = {
                dataGridView1.Rows[row].Cells["Per_name"].Value.ToString(),
                dataGridView1.Rows[row].Cells["Rank"].Value.ToString(),
                dataGridView1.Rows[row].Cells["Phone"].Value.ToString(),
                dataGridView1.Rows[row].Cells["ID"].Value.ToString()
            };
            string[] BoxData = {
                tb_Name.Text, tb_position.Text, tb_Phone.Text, tb_ID.Text
            };

            int count = 0;
            foreach (string data in GridData)
            {
                if (data != BoxData[count])
                    return false;
                count++;
            }
            return true;
        }

        private void btn_Empty_Click(object sender, EventArgs e)
        {
            tb_ID.Text = "";
            tb_PW.Text = "";
            tb_Name.Text = "";
            tb_Affil.Text = comp_name;
            tb_position.Text = "";
            tb_Phone.Text = "";
        }

        private void btn_Del_Click(object sender, EventArgs e)
        {
            int row = dataGridView1.CurrentRow.Index;
            string D_cmd = _DB.DB_Delete(Table_User, "ID", tb_ID.Text, "PW", tb_PW.Text);
            _DB.DB_Send_CMD_reVoid(D_cmd);
            dataGridView1.Rows.Remove(dataGridView1.Rows[row]);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}