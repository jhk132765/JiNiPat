﻿namespace WindowsFormsApp1.Home
{
    partial class Home_User_manage
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home_User_manage));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_IDOverlap = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Del = new System.Windows.Forms.Button();
            this.btn_Empty = new System.Windows.Forms.Button();
            this.tb_Affil = new System.Windows.Forms.TextBox();
            this.tb_Phone = new System.Windows.Forms.TextBox();
            this.tb_PW = new System.Windows.Forms.TextBox();
            this.tb_ID = new System.Windows.Forms.TextBox();
            this.tb_position = new System.Windows.Forms.TextBox();
            this.tb_Name = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Per_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_lookup = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.chk_Manage_Book = new System.Windows.Forms.CheckBox();
            this.chk_Manage_Purchase = new System.Windows.Forms.CheckBox();
            this.chk_Manage_Client = new System.Windows.Forms.CheckBox();
            this.chk_Manage_User = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.chk_Manage = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.chk_Marc_CopyCheck = new System.Windows.Forms.CheckBox();
            this.chk_Marc_Input = new System.Windows.Forms.CheckBox();
            this.chk_Marc = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.chk_Marc_Work = new System.Windows.Forms.CheckBox();
            this.chk_Marc_Setup = new System.Windows.Forms.CheckBox();
            this.chk_Marc_Option = new System.Windows.Forms.CheckBox();
            this.chk_Marc_DLS = new System.Windows.Forms.CheckBox();
            this.chk_Marc_ETC = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chk_Acc_Sales = new System.Windows.Forms.CheckBox();
            this.chk_Acc_Buy = new System.Windows.Forms.CheckBox();
            this.chk_Acc_SendMoneyInput = new System.Windows.Forms.CheckBox();
            this.chk_Acc_SendMoneyList = new System.Windows.Forms.CheckBox();
            this.chk_Acc_PartTime = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chk_Acc = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chk_Div_OrderInput = new System.Windows.Forms.CheckBox();
            this.chk_Div_ListTotal = new System.Windows.Forms.CheckBox();
            this.chk_Div_ListLookup = new System.Windows.Forms.CheckBox();
            this.chk_Div_ListInput = new System.Windows.Forms.CheckBox();
            this.chk_Div_Stock = new System.Windows.Forms.CheckBox();
            this.chk_Div_Inven = new System.Windows.Forms.CheckBox();
            this.chk_Div_Return = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chk_Div = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Save
            // 
            resources.ApplyResources(this.btn_Save, "btn_Save");
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_IDOverlap
            // 
            resources.ApplyResources(this.btn_IDOverlap, "btn_IDOverlap");
            this.btn_IDOverlap.Name = "btn_IDOverlap";
            this.btn_IDOverlap.UseVisualStyleBackColor = true;
            this.btn_IDOverlap.Click += new System.EventHandler(this.btn_IDOverlap_Click);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // btn_Del
            // 
            resources.ApplyResources(this.btn_Del, "btn_Del");
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.UseVisualStyleBackColor = true;
            this.btn_Del.Click += new System.EventHandler(this.btn_Del_Click);
            // 
            // btn_Empty
            // 
            resources.ApplyResources(this.btn_Empty, "btn_Empty");
            this.btn_Empty.Name = "btn_Empty";
            this.btn_Empty.UseVisualStyleBackColor = true;
            this.btn_Empty.Click += new System.EventHandler(this.btn_Empty_Click);
            // 
            // tb_Affil
            // 
            resources.ApplyResources(this.tb_Affil, "tb_Affil");
            this.tb_Affil.Name = "tb_Affil";
            // 
            // tb_Phone
            // 
            resources.ApplyResources(this.tb_Phone, "tb_Phone");
            this.tb_Phone.Name = "tb_Phone";
            // 
            // tb_PW
            // 
            resources.ApplyResources(this.tb_PW, "tb_PW");
            this.tb_PW.Name = "tb_PW";
            // 
            // tb_ID
            // 
            resources.ApplyResources(this.tb_ID, "tb_ID");
            this.tb_ID.Name = "tb_ID";
            // 
            // tb_position
            // 
            resources.ApplyResources(this.tb_position, "tb_position");
            this.tb_position.Name = "tb_position";
            // 
            // tb_Name
            // 
            resources.ApplyResources(this.tb_Name, "tb_Name");
            this.tb_Name.Name = "tb_Name";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Per_name,
            this.Rank,
            this.Phone,
            this.ID,
            this.PW});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            resources.ApplyResources(this.dataGridView1, "dataGridView1");
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // Per_name
            // 
            resources.ApplyResources(this.Per_name, "Per_name");
            this.Per_name.Name = "Per_name";
            this.Per_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Rank
            // 
            resources.ApplyResources(this.Rank, "Rank");
            this.Rank.Name = "Rank";
            this.Rank.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Phone
            // 
            resources.ApplyResources(this.Phone, "Phone");
            this.Phone.Name = "Phone";
            this.Phone.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ID
            // 
            resources.ApplyResources(this.ID, "ID");
            this.ID.Name = "ID";
            this.ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PW
            // 
            resources.ApplyResources(this.PW, "PW");
            this.PW.Name = "PW";
            // 
            // btn_lookup
            // 
            resources.ApplyResources(this.btn_lookup, "btn_lookup");
            this.btn_lookup.Name = "btn_lookup";
            this.btn_lookup.UseVisualStyleBackColor = true;
            this.btn_lookup.Click += new System.EventHandler(this.btn_lookup_Click);
            // 
            // btn_close
            // 
            resources.ApplyResources(this.btn_close, "btn_close");
            this.btn_close.Name = "btn_close";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel5);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // tableLayoutPanel5
            // 
            resources.ApplyResources(this.tableLayoutPanel5, "tableLayoutPanel5");
            this.tableLayoutPanel5.Controls.Add(this.label29, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.label28, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label27, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.chk_Manage_Book, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.chk_Manage_Purchase, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.chk_Manage_Client, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.chk_Manage_User, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label26, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.chk_Manage, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label32, 0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.Name = "label28";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // chk_Manage_Book
            // 
            resources.ApplyResources(this.chk_Manage_Book, "chk_Manage_Book");
            this.chk_Manage_Book.Name = "chk_Manage_Book";
            this.chk_Manage_Book.UseVisualStyleBackColor = true;
            this.chk_Manage_Book.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Manage_Purchase
            // 
            resources.ApplyResources(this.chk_Manage_Purchase, "chk_Manage_Purchase");
            this.chk_Manage_Purchase.Name = "chk_Manage_Purchase";
            this.chk_Manage_Purchase.UseVisualStyleBackColor = true;
            this.chk_Manage_Purchase.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Manage_Client
            // 
            resources.ApplyResources(this.chk_Manage_Client, "chk_Manage_Client");
            this.chk_Manage_Client.Name = "chk_Manage_Client";
            this.chk_Manage_Client.UseVisualStyleBackColor = true;
            this.chk_Manage_Client.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Manage_User
            // 
            resources.ApplyResources(this.chk_Manage_User, "chk_Manage_User");
            this.chk_Manage_User.Name = "chk_Manage_User";
            this.chk_Manage_User.UseVisualStyleBackColor = true;
            this.chk_Manage_User.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.Name = "label26";
            // 
            // chk_Manage
            // 
            resources.ApplyResources(this.chk_Manage, "chk_Manage");
            this.chk_Manage.Name = "chk_Manage";
            this.chk_Manage.UseVisualStyleBackColor = true;
            this.chk_Manage.Click += new System.EventHandler(this.chk_Enabled);
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label32.Name = "label32";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel4);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // tableLayoutPanel4
            // 
            resources.ApplyResources(this.tableLayoutPanel4, "tableLayoutPanel4");
            this.tableLayoutPanel4.Controls.Add(this.label25, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.label24, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.label23, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.label22, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label21, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label20, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.chk_Marc_CopyCheck, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.chk_Marc_Input, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.chk_Marc, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.chk_Marc_Work, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.chk_Marc_Setup, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.chk_Marc_Option, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.chk_Marc_DLS, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.chk_Marc_ETC, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.label19, 0, 1);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // chk_Marc_CopyCheck
            // 
            resources.ApplyResources(this.chk_Marc_CopyCheck, "chk_Marc_CopyCheck");
            this.chk_Marc_CopyCheck.Name = "chk_Marc_CopyCheck";
            this.chk_Marc_CopyCheck.UseVisualStyleBackColor = true;
            this.chk_Marc_CopyCheck.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Marc_Input
            // 
            resources.ApplyResources(this.chk_Marc_Input, "chk_Marc_Input");
            this.chk_Marc_Input.Name = "chk_Marc_Input";
            this.chk_Marc_Input.UseVisualStyleBackColor = true;
            this.chk_Marc_Input.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Marc
            // 
            resources.ApplyResources(this.chk_Marc, "chk_Marc");
            this.chk_Marc.Name = "chk_Marc";
            this.chk_Marc.UseVisualStyleBackColor = true;
            this.chk_Marc.Click += new System.EventHandler(this.chk_Enabled);
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label33.Name = "label33";
            // 
            // chk_Marc_Work
            // 
            resources.ApplyResources(this.chk_Marc_Work, "chk_Marc_Work");
            this.chk_Marc_Work.Name = "chk_Marc_Work";
            this.chk_Marc_Work.UseVisualStyleBackColor = true;
            this.chk_Marc_Work.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Marc_Setup
            // 
            resources.ApplyResources(this.chk_Marc_Setup, "chk_Marc_Setup");
            this.chk_Marc_Setup.Name = "chk_Marc_Setup";
            this.chk_Marc_Setup.UseVisualStyleBackColor = true;
            this.chk_Marc_Setup.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Marc_Option
            // 
            resources.ApplyResources(this.chk_Marc_Option, "chk_Marc_Option");
            this.chk_Marc_Option.Name = "chk_Marc_Option";
            this.chk_Marc_Option.UseVisualStyleBackColor = true;
            this.chk_Marc_Option.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Marc_DLS
            // 
            resources.ApplyResources(this.chk_Marc_DLS, "chk_Marc_DLS");
            this.chk_Marc_DLS.Name = "chk_Marc_DLS";
            this.chk_Marc_DLS.UseVisualStyleBackColor = true;
            this.chk_Marc_DLS.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Marc_ETC
            // 
            resources.ApplyResources(this.chk_Marc_ETC, "chk_Marc_ETC");
            this.chk_Marc_ETC.Name = "chk_Marc_ETC";
            this.chk_Marc_ETC.UseVisualStyleBackColor = true;
            this.chk_Marc_ETC.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel3);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            resources.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
            this.tableLayoutPanel3.Controls.Add(this.label18, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label17, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.chk_Acc_Sales, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.chk_Acc_Buy, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.chk_Acc_SendMoneyInput, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.chk_Acc_SendMoneyList, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.chk_Acc_PartTime, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label15, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.chk_Acc, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label31, 0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // chk_Acc_Sales
            // 
            resources.ApplyResources(this.chk_Acc_Sales, "chk_Acc_Sales");
            this.chk_Acc_Sales.Name = "chk_Acc_Sales";
            this.chk_Acc_Sales.UseVisualStyleBackColor = true;
            this.chk_Acc_Sales.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Acc_Buy
            // 
            resources.ApplyResources(this.chk_Acc_Buy, "chk_Acc_Buy");
            this.chk_Acc_Buy.Name = "chk_Acc_Buy";
            this.chk_Acc_Buy.UseVisualStyleBackColor = true;
            this.chk_Acc_Buy.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Acc_SendMoneyInput
            // 
            resources.ApplyResources(this.chk_Acc_SendMoneyInput, "chk_Acc_SendMoneyInput");
            this.chk_Acc_SendMoneyInput.Name = "chk_Acc_SendMoneyInput";
            this.chk_Acc_SendMoneyInput.UseVisualStyleBackColor = true;
            this.chk_Acc_SendMoneyInput.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Acc_SendMoneyList
            // 
            resources.ApplyResources(this.chk_Acc_SendMoneyList, "chk_Acc_SendMoneyList");
            this.chk_Acc_SendMoneyList.Name = "chk_Acc_SendMoneyList";
            this.chk_Acc_SendMoneyList.UseVisualStyleBackColor = true;
            this.chk_Acc_SendMoneyList.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Acc_PartTime
            // 
            resources.ApplyResources(this.chk_Acc_PartTime, "chk_Acc_PartTime");
            this.chk_Acc_PartTime.Name = "chk_Acc_PartTime";
            this.chk_Acc_PartTime.UseVisualStyleBackColor = true;
            this.chk_Acc_PartTime.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // chk_Acc
            // 
            resources.ApplyResources(this.chk_Acc, "chk_Acc");
            this.chk_Acc.Name = "chk_Acc";
            this.chk_Acc.UseVisualStyleBackColor = true;
            this.chk_Acc.Click += new System.EventHandler(this.chk_Enabled);
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label31.Name = "label31";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.chk_Div_OrderInput, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.chk_Div_ListTotal, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.chk_Div_ListLookup, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.chk_Div_ListInput, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.chk_Div_Stock, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.chk_Div_Inven, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.chk_Div_Return, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.chk_Div, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label30, 0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // chk_Div_OrderInput
            // 
            resources.ApplyResources(this.chk_Div_OrderInput, "chk_Div_OrderInput");
            this.chk_Div_OrderInput.Name = "chk_Div_OrderInput";
            this.chk_Div_OrderInput.UseVisualStyleBackColor = true;
            this.chk_Div_OrderInput.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Div_ListTotal
            // 
            resources.ApplyResources(this.chk_Div_ListTotal, "chk_Div_ListTotal");
            this.chk_Div_ListTotal.Name = "chk_Div_ListTotal";
            this.chk_Div_ListTotal.UseVisualStyleBackColor = true;
            this.chk_Div_ListTotal.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Div_ListLookup
            // 
            resources.ApplyResources(this.chk_Div_ListLookup, "chk_Div_ListLookup");
            this.chk_Div_ListLookup.Name = "chk_Div_ListLookup";
            this.chk_Div_ListLookup.UseVisualStyleBackColor = true;
            this.chk_Div_ListLookup.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Div_ListInput
            // 
            resources.ApplyResources(this.chk_Div_ListInput, "chk_Div_ListInput");
            this.chk_Div_ListInput.Name = "chk_Div_ListInput";
            this.chk_Div_ListInput.UseVisualStyleBackColor = true;
            this.chk_Div_ListInput.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Div_Stock
            // 
            resources.ApplyResources(this.chk_Div_Stock, "chk_Div_Stock");
            this.chk_Div_Stock.Name = "chk_Div_Stock";
            this.chk_Div_Stock.UseVisualStyleBackColor = true;
            this.chk_Div_Stock.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Div_Inven
            // 
            resources.ApplyResources(this.chk_Div_Inven, "chk_Div_Inven");
            this.chk_Div_Inven.Name = "chk_Div_Inven";
            this.chk_Div_Inven.UseVisualStyleBackColor = true;
            this.chk_Div_Inven.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // chk_Div_Return
            // 
            resources.ApplyResources(this.chk_Div_Return, "chk_Div_Return");
            this.chk_Div_Return.Name = "chk_Div_Return";
            this.chk_Div_Return.UseVisualStyleBackColor = true;
            this.chk_Div_Return.Click += new System.EventHandler(this.chk_Div_ListInput_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // chk_Div
            // 
            resources.ApplyResources(this.chk_Div, "chk_Div");
            this.chk_Div.Name = "chk_Div";
            this.chk_Div.UseVisualStyleBackColor = true;
            this.chk_Div.Click += new System.EventHandler(this.chk_Enabled);
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label30.Name = "label30";
            // 
            // Home_User_manage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_lookup);
            this.Controls.Add(this.btn_IDOverlap);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tb_Name);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tb_position);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tb_ID);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.tb_PW);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tb_Phone);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_Affil);
            this.Controls.Add(this.btn_Del);
            this.Controls.Add(this.btn_Empty);
            this.Name = "Home_User_manage";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_Empty;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_IDOverlap;
        private System.Windows.Forms.Button btn_Del;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.TextBox tb_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_Phone;
        private System.Windows.Forms.TextBox tb_PW;
        private System.Windows.Forms.TextBox tb_ID;
        private System.Windows.Forms.TextBox tb_position;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_Affil;
        private System.Windows.Forms.Button btn_lookup;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox chk_Div_ListInput;
        private System.Windows.Forms.CheckBox chk_Div_OrderInput;
        private System.Windows.Forms.CheckBox chk_Div_ListTotal;
        private System.Windows.Forms.CheckBox chk_Div_ListLookup;
        private System.Windows.Forms.CheckBox chk_Div_Stock;
        private System.Windows.Forms.CheckBox chk_Div_Inven;
        private System.Windows.Forms.CheckBox chk_Div_Return;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.CheckBox chk_Marc_CopyCheck;
        private System.Windows.Forms.CheckBox chk_Marc_Input;
        private System.Windows.Forms.CheckBox chk_Marc_Work;
        private System.Windows.Forms.CheckBox chk_Marc_Setup;
        private System.Windows.Forms.CheckBox chk_Marc_Option;
        private System.Windows.Forms.CheckBox chk_Marc_DLS;
        private System.Windows.Forms.CheckBox chk_Marc_ETC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.CheckBox chk_Acc_Sales;
        private System.Windows.Forms.CheckBox chk_Acc_Buy;
        private System.Windows.Forms.CheckBox chk_Acc_SendMoneyInput;
        private System.Windows.Forms.CheckBox chk_Acc_SendMoneyList;
        private System.Windows.Forms.CheckBox chk_Acc_PartTime;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.CheckBox chk_Manage_Book;
        private System.Windows.Forms.CheckBox chk_Manage_Purchase;
        private System.Windows.Forms.CheckBox chk_Manage_Client;
        private System.Windows.Forms.CheckBox chk_Manage_User;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chk_Manage;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox chk_Marc;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.CheckBox chk_Acc;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.CheckBox chk_Div;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Per_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rank;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PW;
    }
}

