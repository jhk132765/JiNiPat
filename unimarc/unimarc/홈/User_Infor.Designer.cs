﻿namespace WindowsFormsApp1.Home
{
    partial class User_Infor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.tb_boss = new System.Windows.Forms.TextBox();
            this.tb_email_pw = new System.Windows.Forms.TextBox();
            this.tb_email_id = new System.Windows.Forms.TextBox();
            this.tb_bank_comp = new System.Windows.Forms.TextBox();
            this.tb_port = new System.Windows.Forms.TextBox();
            this.tb_smtp = new System.Windows.Forms.TextBox();
            this.tb_bank_no = new System.Windows.Forms.TextBox();
            this.tb_fax = new System.Windows.Forms.TextBox();
            this.tb_tel = new System.Windows.Forms.TextBox();
            this.tb_barea = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.tb_addr = new System.Windows.Forms.TextBox();
            this.tb_zip = new System.Windows.Forms.TextBox();
            this.tb_jongmok = new System.Windows.Forms.TextBox();
            this.tb_uptae = new System.Windows.Forms.TextBox();
            this.tb_cobin = new System.Windows.Forms.TextBox();
            this.tb_bubin = new System.Windows.Forms.TextBox();
            this.tb_sangho = new System.Windows.Forms.TextBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cb_print = new System.Windows.Forms.ComboBox();
            this.cb_statement = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cb_kind = new System.Windows.Forms.ComboBox();
            this.btn_mailtest = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-1, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(59, 28);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "등록번호";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(-1, -1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(59, 28);
            this.panel2.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "상      호";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(-1, 80);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(59, 28);
            this.panel3.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "회사주소";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(-1, 53);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(59, 28);
            this.panel4.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "업      태";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(-1, 134);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(59, 28);
            this.panel5.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "은 행 명";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(-1, 107);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(59, 28);
            this.panel6.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "전화번호";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label7);
            this.panel7.Location = new System.Drawing.Point(-1, 188);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(59, 28);
            this.panel7.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "S M T P";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label8);
            this.panel8.Location = new System.Drawing.Point(-1, 161);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(59, 28);
            this.panel8.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "이 메 일";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label9);
            this.panel9.Location = new System.Drawing.Point(-1, 215);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(59, 28);
            this.panel9.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(0, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "이메일I.D";
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.tb_boss);
            this.panel12.Controls.Add(this.tb_email_pw);
            this.panel12.Controls.Add(this.tb_email_id);
            this.panel12.Controls.Add(this.tb_bank_comp);
            this.panel12.Controls.Add(this.tb_port);
            this.panel12.Controls.Add(this.tb_smtp);
            this.panel12.Controls.Add(this.tb_bank_no);
            this.panel12.Controls.Add(this.tb_fax);
            this.panel12.Controls.Add(this.tb_tel);
            this.panel12.Controls.Add(this.tb_barea);
            this.panel12.Controls.Add(this.tb_email);
            this.panel12.Controls.Add(this.tb_addr);
            this.panel12.Controls.Add(this.tb_zip);
            this.panel12.Controls.Add(this.tb_jongmok);
            this.panel12.Controls.Add(this.tb_uptae);
            this.panel12.Controls.Add(this.tb_cobin);
            this.panel12.Controls.Add(this.tb_bubin);
            this.panel12.Controls.Add(this.tb_sangho);
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.panel19);
            this.panel12.Controls.Add(this.panel18);
            this.panel12.Controls.Add(this.panel1);
            this.panel12.Controls.Add(this.panel11);
            this.panel12.Controls.Add(this.panel10);
            this.panel12.Controls.Add(this.panel8);
            this.panel12.Controls.Add(this.panel17);
            this.panel12.Controls.Add(this.panel3);
            this.panel12.Controls.Add(this.panel9);
            this.panel12.Controls.Add(this.panel4);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Controls.Add(this.panel7);
            this.panel12.Controls.Add(this.panel15);
            this.panel12.Controls.Add(this.panel5);
            this.panel12.Controls.Add(this.panel14);
            this.panel12.Controls.Add(this.panel6);
            this.panel12.Location = new System.Drawing.Point(14, 40);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(450, 244);
            this.panel12.TabIndex = 4;
            // 
            // tb_boss
            // 
            this.tb_boss.Location = new System.Drawing.Point(289, 3);
            this.tb_boss.Name = "tb_boss";
            this.tb_boss.Size = new System.Drawing.Size(150, 21);
            this.tb_boss.TabIndex = 0;
            this.tb_boss.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_email_pw
            // 
            this.tb_email_pw.Location = new System.Drawing.Point(289, 218);
            this.tb_email_pw.Name = "tb_email_pw";
            this.tb_email_pw.PasswordChar = '*';
            this.tb_email_pw.Size = new System.Drawing.Size(150, 21);
            this.tb_email_pw.TabIndex = 0;
            this.tb_email_pw.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_email_id
            // 
            this.tb_email_id.Location = new System.Drawing.Point(64, 218);
            this.tb_email_id.Name = "tb_email_id";
            this.tb_email_id.Size = new System.Drawing.Size(150, 21);
            this.tb_email_id.TabIndex = 0;
            this.tb_email_id.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_bank_comp
            // 
            this.tb_bank_comp.Location = new System.Drawing.Point(64, 137);
            this.tb_bank_comp.Name = "tb_bank_comp";
            this.tb_bank_comp.Size = new System.Drawing.Size(150, 21);
            this.tb_bank_comp.TabIndex = 0;
            this.tb_bank_comp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_port
            // 
            this.tb_port.Location = new System.Drawing.Point(289, 191);
            this.tb_port.Name = "tb_port";
            this.tb_port.Size = new System.Drawing.Size(150, 21);
            this.tb_port.TabIndex = 0;
            this.tb_port.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            this.tb_port.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_port_KeyPress);
            // 
            // tb_smtp
            // 
            this.tb_smtp.Location = new System.Drawing.Point(64, 191);
            this.tb_smtp.Name = "tb_smtp";
            this.tb_smtp.Size = new System.Drawing.Size(150, 21);
            this.tb_smtp.TabIndex = 0;
            this.tb_smtp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_bank_no
            // 
            this.tb_bank_no.Location = new System.Drawing.Point(289, 137);
            this.tb_bank_no.Name = "tb_bank_no";
            this.tb_bank_no.Size = new System.Drawing.Size(150, 21);
            this.tb_bank_no.TabIndex = 0;
            this.tb_bank_no.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_fax
            // 
            this.tb_fax.Location = new System.Drawing.Point(289, 110);
            this.tb_fax.Name = "tb_fax";
            this.tb_fax.Size = new System.Drawing.Size(150, 21);
            this.tb_fax.TabIndex = 0;
            this.tb_fax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_tel
            // 
            this.tb_tel.Location = new System.Drawing.Point(64, 110);
            this.tb_tel.Name = "tb_tel";
            this.tb_tel.Size = new System.Drawing.Size(150, 21);
            this.tb_tel.TabIndex = 0;
            this.tb_tel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_barea
            // 
            this.tb_barea.Location = new System.Drawing.Point(289, 164);
            this.tb_barea.Name = "tb_barea";
            this.tb_barea.Size = new System.Drawing.Size(150, 21);
            this.tb_barea.TabIndex = 0;
            this.tb_barea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_email
            // 
            this.tb_email.Location = new System.Drawing.Point(64, 164);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(150, 21);
            this.tb_email.TabIndex = 0;
            this.tb_email.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_addr
            // 
            this.tb_addr.Location = new System.Drawing.Point(115, 83);
            this.tb_addr.Name = "tb_addr";
            this.tb_addr.Size = new System.Drawing.Size(324, 21);
            this.tb_addr.TabIndex = 0;
            this.tb_addr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_zip
            // 
            this.tb_zip.Location = new System.Drawing.Point(64, 83);
            this.tb_zip.Name = "tb_zip";
            this.tb_zip.Size = new System.Drawing.Size(45, 21);
            this.tb_zip.TabIndex = 0;
            this.tb_zip.Text = "  -";
            this.tb_zip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_jongmok
            // 
            this.tb_jongmok.Location = new System.Drawing.Point(289, 57);
            this.tb_jongmok.Name = "tb_jongmok";
            this.tb_jongmok.Size = new System.Drawing.Size(150, 21);
            this.tb_jongmok.TabIndex = 0;
            this.tb_jongmok.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_uptae
            // 
            this.tb_uptae.Location = new System.Drawing.Point(64, 57);
            this.tb_uptae.Name = "tb_uptae";
            this.tb_uptae.Size = new System.Drawing.Size(150, 21);
            this.tb_uptae.TabIndex = 0;
            this.tb_uptae.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_cobin
            // 
            this.tb_cobin.Location = new System.Drawing.Point(289, 30);
            this.tb_cobin.Name = "tb_cobin";
            this.tb_cobin.Size = new System.Drawing.Size(150, 21);
            this.tb_cobin.TabIndex = 0;
            this.tb_cobin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_bubin
            // 
            this.tb_bubin.Location = new System.Drawing.Point(64, 30);
            this.tb_bubin.Name = "tb_bubin";
            this.tb_bubin.Size = new System.Drawing.Size(150, 21);
            this.tb_bubin.TabIndex = 0;
            this.tb_bubin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // tb_sangho
            // 
            this.tb_sangho.Enabled = false;
            this.tb_sangho.Location = new System.Drawing.Point(64, 3);
            this.tb_sangho.Name = "tb_sangho";
            this.tb_sangho.Size = new System.Drawing.Size(150, 21);
            this.tb_sangho.TabIndex = 0;
            this.tb_sangho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_sangho_KeyDown);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.label18);
            this.panel19.Location = new System.Drawing.Point(223, -1);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(59, 28);
            this.panel19.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(2, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "대표자명";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.label17);
            this.panel18.Location = new System.Drawing.Point(223, 26);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(59, 28);
            this.panel18.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(2, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "법인번호";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.label21);
            this.panel11.Location = new System.Drawing.Point(223, 134);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(59, 28);
            this.panel11.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 0;
            this.label21.Text = "계좌번호";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.label10);
            this.panel10.Location = new System.Drawing.Point(223, 107);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(59, 28);
            this.panel10.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "팩스번호";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.label16);
            this.panel17.Location = new System.Drawing.Point(223, 161);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(59, 28);
            this.panel17.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "발 송 처";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.label12);
            this.panel13.Location = new System.Drawing.Point(223, 188);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(59, 28);
            this.panel13.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "P O R T";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.label14);
            this.panel15.Location = new System.Drawing.Point(223, 215);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(59, 28);
            this.panel15.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(-1, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "이메일PW";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.label13);
            this.panel14.Location = new System.Drawing.Point(223, 53);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(59, 28);
            this.panel14.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(2, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "종      목";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(17, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 16);
            this.label11.TabIndex = 0;
            this.label11.Text = "사업자정보";
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(340, 9);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(59, 25);
            this.btn_save.TabIndex = 5;
            this.btn_save.Text = "저  장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(405, 9);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(59, 25);
            this.btn_close.TabIndex = 5;
            this.btn_close.Text = "닫  기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 294);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "띠지 출력 방식";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 318);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "거래명세서양식";
            // 
            // cb_print
            // 
            this.cb_print.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_print.FormattingEnabled = true;
            this.cb_print.Location = new System.Drawing.Point(95, 290);
            this.cb_print.Name = "cb_print";
            this.cb_print.Size = new System.Drawing.Size(135, 20);
            this.cb_print.TabIndex = 6;
            // 
            // cb_statement
            // 
            this.cb_statement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_statement.FormattingEnabled = true;
            this.cb_statement.Location = new System.Drawing.Point(95, 314);
            this.cb_statement.Name = "cb_statement";
            this.cb_statement.Size = new System.Drawing.Size(135, 20);
            this.cb_statement.TabIndex = 6;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(239, 294);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "띠지 종류";
            // 
            // cb_kind
            // 
            this.cb_kind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_kind.FormattingEnabled = true;
            this.cb_kind.Location = new System.Drawing.Point(305, 290);
            this.cb_kind.Name = "cb_kind";
            this.cb_kind.Size = new System.Drawing.Size(150, 20);
            this.cb_kind.TabIndex = 6;
            // 
            // btn_mailtest
            // 
            this.btn_mailtest.Location = new System.Drawing.Point(356, 316);
            this.btn_mailtest.Name = "btn_mailtest";
            this.btn_mailtest.Size = new System.Drawing.Size(98, 25);
            this.btn_mailtest.TabIndex = 5;
            this.btn_mailtest.Text = "메일전송테스트";
            this.btn_mailtest.UseVisualStyleBackColor = true;
            this.btn_mailtest.Click += new System.EventHandler(this.btn_mailtest_Click);
            // 
            // User_Infor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 348);
            this.Controls.Add(this.cb_statement);
            this.Controls.Add(this.cb_kind);
            this.Controls.Add(this.cb_print);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_mailtest);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel12);
            this.Name = "User_Infor";
            this.Text = "사업자 정보";
            this.Load += new System.EventHandler(this.User_Infor_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox tb_boss;
        private System.Windows.Forms.TextBox tb_email_pw;
        private System.Windows.Forms.TextBox tb_email_id;
        private System.Windows.Forms.TextBox tb_bank_comp;
        private System.Windows.Forms.TextBox tb_port;
        private System.Windows.Forms.TextBox tb_smtp;
        private System.Windows.Forms.TextBox tb_fax;
        private System.Windows.Forms.TextBox tb_tel;
        private System.Windows.Forms.TextBox tb_barea;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.TextBox tb_addr;
        private System.Windows.Forms.TextBox tb_zip;
        private System.Windows.Forms.TextBox tb_jongmok;
        private System.Windows.Forms.TextBox tb_uptae;
        private System.Windows.Forms.TextBox tb_cobin;
        private System.Windows.Forms.TextBox tb_bubin;
        private System.Windows.Forms.TextBox tb_sangho;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cb_print;
        private System.Windows.Forms.ComboBox cb_statement;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cb_kind;
        private System.Windows.Forms.Button btn_mailtest;
        private System.Windows.Forms.TextBox tb_bank_no;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label21;
    }
}