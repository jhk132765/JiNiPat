﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using WindowsFormsApp1.Delivery;
using UniMarc.회계;

namespace WindowsFormsApp1.Account
{
    // 송금 등록
    public partial class Remit_reg2 : Form
    {
        Main main;
        Helper_DB db = new Helper_DB();
        public string compidx = "";
        public Remit_reg2(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        private void Remit_reg2_Load(object sender, EventArgs e)
        {
            db.DBcon();

            start_Date.Value = new DateTime(int.Parse(DateTime.Now.ToString("yyyy")),
                                            int.Parse(DateTime.Now.ToString("MM")), 1);

            string[] remunerate = { "미지급", "지급", "전체" };
            cb_remunerate.Items.AddRange(remunerate);
            cb_remunerate.SelectedIndex = 0;

            btn_lookup_Click(null, null);
        }
        private void btn_Add_Click(object sender, EventArgs e)
        {
            list_Date.Value = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
            tb_purchase.Text = "";
            tb_now_money.Text = "";

            tb_bank.Text = "";
            tb_bank_code.Text = "";
            tb_bank_num.Text = "";
            tb_bank_boss.Text = "";
            tb_send_money.Text = "";
            tb_etc.Text = "";
        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid _Grid = new Skill_Grid();
            _Grid.Print_Grid_Num(sender, e);
        }
        private void tb_purchase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Order_input_Search search = new Order_input_Search(this);
                search.Where_Open = "Order";
                search.TopMost = true;
                search.Show();
            }
        }
        public void mk_base(string purchase)
        {
            if (purchase == "") { return; }
            string[] sear_tbl = { "comparyidx", "sangho" };
            string[] sear_col = { compidx, purchase };
            string Area = "`bank_comp`, `bank_code`, `bank_no`, `bank_name`";
            string cmd = db.More_DB_Search("Purchase", sear_tbl, sear_col, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] db_data = db_res.Split('|');

            tb_bank.Text = db_data[0];
            tb_bank_code.Text = db_data[1];
            tb_bank_num.Text = db_data[2];
            tb_bank_boss.Text = db_data[3];
        }
        private void btn_save_Click(object sender, EventArgs e)
        {
            string date = list_Date.Value.ToString().Substring(0, 10);
            // 회사idx 등록일자 매입거래처 은행명 은행코드 
            // 계좌번호 예금주 송금금액 비고
            string[] input_table = {"compidx", "date_reg", "purchase", "bank_comp", "bank_code",
                                    "bank_num", "bank_name", "send_money", "etc", "payment", 
                                    "reg_man" };
            string[] input_data = { compidx, date, tb_purchase.Text, tb_bank.Text, tb_bank_code.Text,
                                    tb_bank_num.Text, tb_bank_boss.Text, tb_send_money.Text, tb_etc.Text, "False", 
                                    main.User };
            string Incmd = db.DB_INSERT("Remit_reg", input_table, input_data);
            db.DB_Send_CMD_reVoid(Incmd);

            MessageBox.Show("성공적으로 저장되었습니다.");
            Add_Grid();
        }
        void Add_Grid()
        {
            string date = list_Date.Value.ToString().Substring(0, 10);
            string[] grid_data = { date, tb_purchase.Text, tb_bank.Text, tb_bank_num.Text, tb_bank_boss.Text,
                                   tb_send_money.Text, "", tb_etc.Text };
            dataGridView1.Rows.Add(grid_data);
        }
        private void btn_delete_Click(object sender, EventArgs e)
        {
            string date = list_Date.Value.ToString().Substring(0, 10);
            string target_idx = "compidx";
            string[] target_table = {"date_reg", "purchase", "bank_comp", "bank_code",
                                     "bank_num", "bank_name", "send_money", "etc" };
            string[] target_data = { date, tb_purchase.Text, tb_bank.Text, tb_bank_code.Text,
                                     tb_bank_num.Text, tb_bank_boss.Text, tb_send_money.Text, tb_etc.Text };

            if (MessageBox.Show("정말 삭제하시겠습니까?", "경고", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string cmd = db.DB_Delete_More_term("Remit_reg", target_idx, compidx, target_table, target_data);
                db.DB_Send_CMD_reVoid(cmd);
                MessageBox.Show("삭제되었습니다.");
            }
        }
        private void btn_lookup_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string Area = "`date_reg`, `purchase`, `bank_comp`, `bank_code`, `bank_num`, " +
                          "`bank_name`, `send_money`, `date_pay`, `etc`";
            List<string> list_tbl = new List<string>();
            List<string> list_col = new List<string>();
            list_tbl.Add("compidx");
            list_col.Add(compidx);
            if (cb_remunerate.SelectedIndex == 0 || cb_remunerate.SelectedIndex == 1)
            {
                list_tbl.Add("payment");
                if (cb_remunerate.SelectedIndex == 0) { list_col.Add("False"); }
                if (cb_remunerate.SelectedIndex == 1) { list_col.Add("True"); }
            }
            string[] Search_tbl = list_tbl.ToArray();
            string[] Search_col = list_col.ToArray();
            string cmd = db.More_DB_Search("Remit_reg", Search_tbl, Search_col, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] db_data = db_res.Split('|');
            lookup_grid(db_data);
        }
        void lookup_grid(string[] data)
        {
            string[] grid = { "", "", "", "", "", "", "", "", "" };
            for (int a = 0; a < data.Length; a++)
            {
                if (a % 9 == 0) { grid[0] = data[a]; }
                if (a % 9 == 1) { grid[1] = data[a]; }
                if (a % 9 == 2) { grid[2] = data[a]; }
                if (a % 9 == 3) { grid[3] = data[a]; }
                if (a % 9 == 4) { grid[4] = data[a]; }
                if (a % 9 == 5) { grid[5] = data[a]; }
                if (a % 9 == 6) { grid[6] = data[a]; }
                if (a % 9 == 7) { grid[7] = data[a]; }
                if (a % 9 == 8)
                {
                    grid[8] = data[a];
                    if (filter_date(grid[0]) == true) { dataGridView1.Rows.Add(grid); }
                }
            }
        }
        bool filter_date(string date)
        {
            string str_start = start_Date.Value.ToString().Substring(0, 10);
            string str_end = end_Date.Value.ToString().Substring(0, 10);
            string target = date.Substring(0, 10);

            if (DateTime.Parse(str_start) <= DateTime.Parse(target) &&
                DateTime.Parse(target) <= DateTime.Parse(str_end))
            {
                return true;
            }
            return false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;

            string date = dataGridView1.Rows[row].Cells["date_res"].Value.ToString();
            date = Regex.Replace(date, @"\D", "");

            int date_year = Convert.ToInt32(date.Substring(0, 4));
            int date_month = Convert.ToInt32(date.Substring(4, 2));
            int date_day = Convert.ToInt32(date.Substring(6, 2));

            list_Date.Value = new DateTime(date_year, date_month, date_day);
            tb_purchase.Text = dataGridView1.Rows[row].Cells["purchase"].Value.ToString();
            tb_bank.Text = dataGridView1.Rows[row].Cells["bank_comp"].Value.ToString();
            tb_bank_code.Text = dataGridView1.Rows[row].Cells["bank_code"].Value.ToString();
            tb_bank_num.Text = dataGridView1.Rows[row].Cells["bank_num"].Value.ToString();
            tb_bank_boss.Text = dataGridView1.Rows[row].Cells["bank_name"].Value.ToString();
            tb_send_money.Text = dataGridView1.Rows[row].Cells["send_money"].Value.ToString();
            tb_etc.Text = dataGridView1.Rows[row].Cells["etc"].Value.ToString();
        }

        private void btn_excel_Click(object sender, EventArgs e)
        {
            Excel_text et = new Excel_text();
            string[] Excel_title = { "은행코드", "계좌번호", "", "금액", "예금주" };
            string[,] inputExcel = new string[dataGridView1.RowCount, 5];
            for (int a = 0; a < dataGridView1.RowCount; a++)
            {
                inputExcel[a, 0] = dataGridView1.Rows[a].Cells["bank_code"].Value.ToString();
                inputExcel[a, 1] = dataGridView1.Rows[a].Cells["bank_num"].Value.ToString();
                inputExcel[a, 3] = dataGridView1.Rows[a].Cells["send_money"].Value.ToString();
                inputExcel[a, 4] = dataGridView1.Rows[a].Cells["bank_name"].Value.ToString();
            }
            et.Mk_Excel(Excel_title, inputExcel);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
