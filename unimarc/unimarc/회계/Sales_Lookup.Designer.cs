﻿namespace WindowsFormsApp1.Account
{
    partial class Sales_Lookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.End_Date = new System.Windows.Forms.DateTimePicker();
            this.Start_Date = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_clt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Lookup = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.out_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.in_per = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.out_per = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.in_price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.out_price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // End_Date
            // 
            this.End_Date.CustomFormat = "yyyy-MM-dd";
            this.End_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.End_Date.Location = new System.Drawing.Point(173, 9);
            this.End_Date.Name = "End_Date";
            this.End_Date.Size = new System.Drawing.Size(85, 21);
            this.End_Date.TabIndex = 39;
            // 
            // Start_Date
            // 
            this.Start_Date.CustomFormat = "yyyy-MM-dd";
            this.Start_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Start_Date.Location = new System.Drawing.Point(70, 9);
            this.Start_Date.Name = "Start_Date";
            this.Start_Date.Size = new System.Drawing.Size(85, 21);
            this.Start_Date.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 37;
            this.label2.Text = "조회기간";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(560, 9);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(280, 21);
            this.textBox2.TabIndex = 78;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(505, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 77;
            this.label3.Text = "비고내용";
            // 
            // tb_clt
            // 
            this.tb_clt.Location = new System.Drawing.Point(344, 9);
            this.tb_clt.Name = "tb_clt";
            this.tb_clt.Size = new System.Drawing.Size(145, 21);
            this.tb_clt.TabIndex = 76;
            this.tb_clt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(277, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 75;
            this.label1.Text = "매출거래처";
            // 
            // btn_Lookup
            // 
            this.btn_Lookup.Location = new System.Drawing.Point(858, 8);
            this.btn_Lookup.Name = "btn_Lookup";
            this.btn_Lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_Lookup.TabIndex = 79;
            this.btn_Lookup.Text = "조    회";
            this.btn_Lookup.UseVisualStyleBackColor = true;
            this.btn_Lookup.Click += new System.EventHandler(this.btn_Lookup_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(945, 8);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 81;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.out_date,
            this.clt,
            this.tel,
            this.content,
            this.count,
            this.total,
            this.in_per,
            this.out_per,
            this.in_price,
            this.out_price,
            this.dif,
            this.etc});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(12, 58);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 20;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1151, 421);
            this.dataGridView1.TabIndex = 82;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // out_date
            // 
            this.out_date.HeaderText = "출고일자";
            this.out_date.Name = "out_date";
            this.out_date.Width = 80;
            // 
            // clt
            // 
            this.clt.HeaderText = "매출거래처";
            this.clt.Name = "clt";
            this.clt.Width = 150;
            // 
            // tel
            // 
            this.tel.HeaderText = "전화번호";
            this.tel.Name = "tel";
            // 
            // content
            // 
            this.content.HeaderText = "내용";
            this.content.Name = "content";
            this.content.Width = 200;
            // 
            // count
            // 
            this.count.HeaderText = "수량";
            this.count.Name = "count";
            this.count.Width = 40;
            // 
            // total
            // 
            this.total.HeaderText = "총액";
            this.total.Name = "total";
            this.total.Width = 80;
            // 
            // in_per
            // 
            this.in_per.HeaderText = "입고율";
            this.in_per.Name = "in_per";
            this.in_per.Width = 60;
            // 
            // out_per
            // 
            this.out_per.HeaderText = "출고율";
            this.out_per.Name = "out_per";
            this.out_per.Width = 60;
            // 
            // in_price
            // 
            this.in_price.HeaderText = "매입금액";
            this.in_price.Name = "in_price";
            this.in_price.Width = 80;
            // 
            // out_price
            // 
            this.out_price.HeaderText = "매출금액";
            this.out_price.Name = "out_price";
            this.out_price.Width = 80;
            // 
            // dif
            // 
            this.dif.HeaderText = "차이";
            this.dif.Name = "dif";
            this.dif.Width = 80;
            // 
            // etc
            // 
            this.etc.HeaderText = "비고";
            this.etc.Name = "etc";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.Start_Date);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.End_Date);
            this.panel1.Controls.Add(this.btn_Lookup);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.tb_clt);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1051, 40);
            this.panel1.TabIndex = 83;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(157, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 12);
            this.label4.TabIndex = 37;
            this.label4.Text = "~";
            // 
            // Sales_Lookup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 491);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Sales_Lookup";
            this.Text = " 매출 조회";
            this.Load += new System.EventHandler(this.Sales_Book_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker End_Date;
        private System.Windows.Forms.DateTimePicker Start_Date;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox tb_clt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn out_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn clt;
        private System.Windows.Forms.DataGridViewTextBoxColumn tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn content;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn in_per;
        private System.Windows.Forms.DataGridViewTextBoxColumn out_per;
        private System.Windows.Forms.DataGridViewTextBoxColumn in_price;
        private System.Windows.Forms.DataGridViewTextBoxColumn out_price;
        private System.Windows.Forms.DataGridViewTextBoxColumn dif;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
        public System.Windows.Forms.Button btn_Lookup;
    }
}