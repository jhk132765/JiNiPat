﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Delivery;

namespace WindowsFormsApp1.Account
{
    public partial class Purchase_Book : Form
    {
        Main main;
        Purchase_Aggregation pa;
        Helper_DB db = new Helper_DB();
        public bool call = false;
        public string compidx;
        bool chk_ac = false;
        public Purchase_Book(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        public Purchase_Book(Purchase_Aggregation _pa)
        {
            InitializeComponent();
            pa = _pa;
            compidx = pa.compidx;
            call = true;
        }
        private void Purchase_Book_Load(object sender, EventArgs e)
        {
            db.DBcon();
            if (pa == null) {
                start_Date.Value = new DateTime(int.Parse(DateTime.Now.ToString("yyyy")),
                                                int.Parse(DateTime.Now.ToString("MM")), 1);
            }
        }
        public void btn_Lookup_Click(object sender, EventArgs e)
        {
            if(call != false) { db.DBcon(); }
            dataGridView1.Rows.Clear();
            if (tb_purchase.Text == "") { return; }
            string Select_Area = "`buy_date`, `book_name`, `count`, `persent`, `buy_money`, `payment`, `etc`";
            string[] Search_col = { "compidx", "purchase" };
            string[] Search_data = { compidx, tb_purchase.Text };

            string cmd = db.More_DB_Search("Buy_ledger", Search_col, Search_data, Select_Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] db_data = db_res.Split('|');
            input_Grid(db_data);
        }
        private void input_Grid(string[] db_data)
        {
            /* 매입일자 내용 수량 입고율 매입금액
             * 결제금액 현잔액 비고 */
            string[] grid = { "", "", "", "", "0", 
                              "0", "0", "" };

            for (int a = 0; a < db_data.Length - 1; a++) 
            { 
                if (a % 7 == 0) { grid[0] = db_data[a].Substring(0, 10); }  // 매입일자
                if (a % 7 == 1) { grid[1] = db_data[a]; }                   // 내용
                if (a % 7 == 2) { grid[2] = db_data[a]; }                   // 수량
                if (a % 7 == 3) { grid[3] = db_data[a]; }                   // 입고율
                if (a % 7 == 4) { grid[4] = db_data[a]; }                   // 매입금액
                if (a % 7 == 5) { grid[5] = db_data[a]; }                   // 결제금액
                if (a % 7 == 6) { grid[7] = db_data[a]; Grid_Sub(grid); }   // 비고
            }
        }
        /* Grid 1행에 매입일자 쪽에 "이월미수금" 단어가 표출되며,
         * 현 잔액에 이전에 있었던 거래 금액들이 계산되어 표출된다.
         */
        private void Grid_Sub(string[] grid)
        {
            string str_start = start_Date.Value.ToString().Substring(0, 10);
            string str_end = end_Date.Value.ToString().Substring(0, 10);

            bool chk_grid = false;

            if (DateTime.Parse(str_start) <= DateTime.Parse(grid[0]) &&
                DateTime.Parse(str_end) >= DateTime.Parse(grid[0])) {
                string[] account = { "이월미수금", "", "", "", "",
                                     "", grid[6], "" };
                if (dataGridView1.Rows.Count == 0) {
                    account[6] = String.Format("{0:#,###}", Convert.ToInt32(account[6]));
                    dataGridView1.Rows.Add(account);
                    chk_ac = true;
                }
                chk_grid = true;
            }
            // 현잔액 = 매입금액 - 결제금액
            int buy = Convert.ToInt32(grid[4]);
            int pay = Convert.ToInt32(grid[5]);
            int remain = Convert.ToInt32(grid[6].Replace(",", ""));
            remain += buy - pay;
            grid[6] = remain.ToString();

            if (chk_grid == true) {
                grid[2] = String.Format("{0:#,###}", Convert.ToInt32(grid[2]));
                grid[4] = String.Format("{0:#,###}", Convert.ToInt32(grid[4]));
                grid[5] = String.Format("{0:#,###}", Convert.ToInt32(grid[5]));
                grid[6] = String.Format("{0:#,###}", Convert.ToInt32(grid[6]));
                dataGridView1.Rows.Add(grid);
            }
        }
        private void btn_Print_Click(object sender, EventArgs e)
        {

        }
        private void tb_purchase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                /* TODO: 완성시 주석 해제
                Order_input_Search search = new Order_input_Search(this);
                search.Where_Open = "Order";
                search.TopMost = true;
                search.Show(); */
                btn_Lookup_Click(null, null);
            }
        }
        private void btn_Close_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
