﻿namespace WindowsFormsApp1.Account
{
    partial class Sales_Input
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.persent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ISBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chk_Add5000 = new System.Windows.Forms.CheckBox();
            this.btn_Total = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Input = new System.Windows.Forms.Button();
            this.tb_count = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_book_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_isbn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Add = new System.Windows.Forms.Button();
            this.tb_persent = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_total = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_book_comp = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_price = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_author = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_etc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_clt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.out_date = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_tel = new System.Windows.Forms.Label();
            this.btn_Close = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.book_name,
            this.author,
            this.book_comp,
            this.price,
            this.persent,
            this.count,
            this.total,
            this.ISBN});
            this.dataGridView1.Location = new System.Drawing.Point(12, 170);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 20;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1020, 370);
            this.dataGridView1.TabIndex = 91;
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 390;
            // 
            // author
            // 
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            this.book_comp.Width = 200;
            // 
            // price
            // 
            this.price.HeaderText = "정가";
            this.price.Name = "price";
            this.price.Width = 80;
            // 
            // persent
            // 
            this.persent.HeaderText = "출고율";
            this.persent.Name = "persent";
            this.persent.Width = 70;
            // 
            // count
            // 
            this.count.HeaderText = "부수";
            this.count.Name = "count";
            this.count.Width = 60;
            // 
            // total
            // 
            this.total.HeaderText = "합계금액";
            this.total.Name = "total";
            this.total.Width = 80;
            // 
            // ISBN
            // 
            this.ISBN.HeaderText = "ISBN";
            this.ISBN.Name = "ISBN";
            this.ISBN.Visible = false;
            // 
            // chk_Add5000
            // 
            this.chk_Add5000.AutoSize = true;
            this.chk_Add5000.Location = new System.Drawing.Point(858, 60);
            this.chk_Add5000.Name = "chk_Add5000";
            this.chk_Add5000.Size = new System.Drawing.Size(72, 28);
            this.chk_Add5000.TabIndex = 90;
            this.chk_Add5000.Text = "칸수일괄\n입력";
            this.chk_Add5000.UseVisualStyleBackColor = true;
            this.chk_Add5000.CheckedChanged += new System.EventHandler(this.chk_Add5000_CheckedChanged);
            // 
            // btn_Total
            // 
            this.btn_Total.Location = new System.Drawing.Point(933, 61);
            this.btn_Total.Name = "btn_Total";
            this.btn_Total.Size = new System.Drawing.Size(75, 23);
            this.btn_Total.TabIndex = 9;
            this.btn_Total.Text = "합계계산";
            this.btn_Total.UseVisualStyleBackColor = true;
            this.btn_Total.Click += new System.EventHandler(this.btn_Total_Click);
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(773, 61);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(75, 23);
            this.btn_Clear.TabIndex = 8;
            this.btn_Clear.Text = "칸비우기";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_Input
            // 
            this.btn_Input.Location = new System.Drawing.Point(693, 61);
            this.btn_Input.Name = "btn_Input";
            this.btn_Input.Size = new System.Drawing.Size(75, 23);
            this.btn_Input.TabIndex = 7;
            this.btn_Input.Text = "입   력";
            this.btn_Input.UseVisualStyleBackColor = true;
            this.btn_Input.Click += new System.EventHandler(this.btn_Input_Click);
            // 
            // tb_count
            // 
            this.tb_count.Location = new System.Drawing.Point(387, 62);
            this.tb_count.Name = "tb_count";
            this.tb_count.Size = new System.Drawing.Size(43, 21);
            this.tb_count.TabIndex = 6;
            this.tb_count.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_price_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(356, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 85;
            this.label9.Text = "부수";
            // 
            // tb_book_name
            // 
            this.tb_book_name.Location = new System.Drawing.Point(293, 8);
            this.tb_book_name.Name = "tb_book_name";
            this.tb_book_name.Size = new System.Drawing.Size(388, 21);
            this.tb_book_name.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(242, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 12);
            this.label5.TabIndex = 82;
            this.label5.Text = "도 서 명";
            // 
            // tb_isbn
            // 
            this.tb_isbn.Location = new System.Drawing.Point(67, 8);
            this.tb_isbn.Name = "tb_isbn";
            this.tb_isbn.Size = new System.Drawing.Size(163, 21);
            this.tb_isbn.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 12);
            this.label4.TabIndex = 80;
            this.label4.Text = "ISBN";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(606, 32);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 79;
            this.btn_Save.Text = "저   장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(521, 32);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(75, 23);
            this.btn_Add.TabIndex = 78;
            this.btn_Add.Text = "추   가";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // tb_persent
            // 
            this.tb_persent.Location = new System.Drawing.Point(298, 62);
            this.tb_persent.Name = "tb_persent";
            this.tb_persent.Size = new System.Drawing.Size(43, 21);
            this.tb_persent.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(255, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 71;
            this.label8.Text = "출고율";
            // 
            // tb_total
            // 
            this.tb_total.Location = new System.Drawing.Point(518, 62);
            this.tb_total.Name = "tb_total";
            this.tb_total.ReadOnly = true;
            this.tb_total.Size = new System.Drawing.Size(163, 21);
            this.tb_total.TabIndex = 75;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(463, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 70;
            this.label11.Text = "합계금액";
            // 
            // tb_book_comp
            // 
            this.tb_book_comp.Location = new System.Drawing.Point(343, 35);
            this.tb_book_comp.Name = "tb_book_comp";
            this.tb_book_comp.Size = new System.Drawing.Size(338, 21);
            this.tb_book_comp.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(300, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 69;
            this.label10.Text = "출판사";
            // 
            // tb_price
            // 
            this.tb_price.Location = new System.Drawing.Point(67, 62);
            this.tb_price.Name = "tb_price";
            this.tb_price.Size = new System.Drawing.Size(163, 21);
            this.tb_price.TabIndex = 4;
            this.tb_price.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_price_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 12);
            this.label7.TabIndex = 68;
            this.label7.Text = "정    가";
            // 
            // tb_author
            // 
            this.tb_author.Location = new System.Drawing.Point(67, 35);
            this.tb_author.Name = "tb_author";
            this.tb_author.Size = new System.Drawing.Size(213, 21);
            this.tb_author.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 12);
            this.label6.TabIndex = 67;
            this.label6.Text = "저    자";
            // 
            // tb_etc
            // 
            this.tb_etc.Location = new System.Drawing.Point(67, 33);
            this.tb_etc.Name = "tb_etc";
            this.tb_etc.Size = new System.Drawing.Size(449, 21);
            this.tb_etc.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 12);
            this.label3.TabIndex = 66;
            this.label3.Text = "비    고";
            // 
            // tb_clt
            // 
            this.tb_clt.Location = new System.Drawing.Point(353, 7);
            this.tb_clt.Name = "tb_clt";
            this.tb_clt.Size = new System.Drawing.Size(163, 21);
            this.tb_clt.TabIndex = 0;
            this.tb_clt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_pur_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(282, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 64;
            this.label2.Text = "매출거래처";
            // 
            // out_date
            // 
            this.out_date.CustomFormat = "yyyy-MM-dd";
            this.out_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.out_date.Location = new System.Drawing.Point(67, 7);
            this.out_date.Name = "out_date";
            this.out_date.Size = new System.Drawing.Size(85, 21);
            this.out_date.TabIndex = 62;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 61;
            this.label1.Text = "매출일자";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_tel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.out_date);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tb_clt);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tb_etc);
            this.panel1.Controls.Add(this.btn_Add);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1020, 61);
            this.panel1.TabIndex = 0;
            // 
            // lbl_tel
            // 
            this.lbl_tel.AutoSize = true;
            this.lbl_tel.Location = new System.Drawing.Point(522, 11);
            this.lbl_tel.Name = "lbl_tel";
            this.lbl_tel.Size = new System.Drawing.Size(0, 12);
            this.lbl_tel.TabIndex = 80;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(691, 32);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 79;
            this.btn_Close.Text = "닫   기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.tb_author);
            this.panel2.Controls.Add(this.chk_Add5000);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.btn_Total);
            this.panel2.Controls.Add(this.tb_price);
            this.panel2.Controls.Add(this.btn_Clear);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.btn_Input);
            this.panel2.Controls.Add(this.tb_book_comp);
            this.panel2.Controls.Add(this.tb_count);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.tb_total);
            this.panel2.Controls.Add(this.tb_book_name);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.tb_persent);
            this.panel2.Controls.Add(this.tb_isbn);
            this.panel2.Location = new System.Drawing.Point(12, 72);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1020, 92);
            this.panel2.TabIndex = 1;
            // 
            // Sales_Input
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 552);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Sales_Input";
            this.Text = "매출입력";
            this.Load += new System.EventHandler(this.Sales_Input_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.CheckBox chk_Add5000;
        private System.Windows.Forms.Button btn_Total;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_Input;
        private System.Windows.Forms.TextBox tb_count;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_book_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_isbn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.TextBox tb_persent;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_book_comp;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_price;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_author;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_etc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker out_date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tb_total;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn persent;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISBN;
        public System.Windows.Forms.TextBox tb_clt;
        public System.Windows.Forms.Label lbl_tel;
    }
}