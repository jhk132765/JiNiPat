﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// 추가된 참조
using WindowsFormsApp1.Delivery;
using WindowsFormsApp1.회계;

namespace WindowsFormsApp1.Account
{
    public partial class Sales_Lookup : Form
    {
        public string compidx;
        Main main;
        Helper_DB db = new Helper_DB();
        public Sales_Lookup(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        private void Sales_Book_Load(object sender, EventArgs e)
        {
            db.DBcon();
            Start_Date.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
        public void btn_Lookup_Click(object sender, EventArgs e)
        {
            string table = "`date`, `client`, `tel`, `book_name`, `in_per`, " +
                           "`out_per`, `in_price`, `out_price`, `total`, `etc`";
            string start = Start_Date.Text.Substring(0, 10);
            string end = End_Date.Text.Substring(0, 10);

            string cmd = db.Search_Date("Sales", table, "date", start, end, compidx);
            string db_res = db.DB_Send_CMD_Search(cmd);
            input_Grid(db_res);
        }
        private void input_Grid(string value)
        {
            /* 조회기간, 매출거래처, 전화번호, 내용, 수량, 총액
             * 입고율, 출고율, 매입금액, 매출금액, 차이, 비고 */
            string[] grid = { "", "", "", "", "", "", 
                              "", "", "", "", "", "" };

            string[] data = { "", "", "", "", "",
                              "", "", "", "", "" };

            string[] ary = value.Split('|');
            int cot = 10;
            for(int a = 0; a < ary.Length; a++)
            {
                if (a % cot == 0) { data[0] = ary[a]; }
                if (a % cot == 1) { data[1] = ary[a]; }
                if (a % cot == 2) { data[2] = ary[a]; }
                if (a % cot == 3) { data[3] = ary[a]; }
                if (a % cot == 4) { data[4] = ary[a]; }
                if (a % cot == 5) { data[5] = ary[a]; }
                if (a % cot == 6) { data[6] = ary[a]; }
                if (a % cot == 7) { data[7] = ary[a]; }
                if (a % cot == 8) { data[8] = ary[a]; }
                if (a % cot == 9) { data[9] = ary[a];
                    if (tb_clt.Text != "")
                    {
                        if (data[1] != tb_clt.Text) { continue; }
                    }
                    grid_data_check(grid, data);
                }
            }
        }
        private void grid_data_check(string[] grid, string[] data)
        {
            /* data: 0.출고일자 / 1.거래처 / 2.전화번호 / 3.내용 / 4.입고율 / 
             *       5.출고율 / 6.입고금액 / 7.출고금액 / 8.합계 / 9.비고
             *       
             *       
             * grid: 0.조회기간 / 1.거래처 / 2.전화번호 / 3.내용 / 4.수량 / 
             *       5.총합계 / 6.입고율 / 7.출고율 / 8.매입금액 / 9.매출금액 / 10.차이 / 11.비고
             *       
             *       out_date / clt / tel / content / count / 
             *       total / in_per / out_per / in_price / out_price / dif / etc
             */
            if (data[3] == "" && data[5] == "0") { return; }

            if (dataGridView1.Rows.Count <= 0)
            {
                base_Setting(grid, data);
                return;
            }

            for(int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (dataGridView1.Rows[a].Cells["out_date"].Value.ToString() == data[0] &&
                    dataGridView1.Rows[a].Cells["clt"].Value.ToString() == data[1] &&
                    dataGridView1.Rows[a].Cells["tel"].Value.ToString() == data[2]) {

                    int count = Convert.ToInt32(dataGridView1.Rows[a].Cells["count"].Value.ToString());
                    int total = Convert.ToInt32(dataGridView1.Rows[a].Cells["total"].Value.ToString());
                    int in_per = Convert.ToInt32(dataGridView1.Rows[a].Cells["in_per"].Value.ToString());
                    int out_per = Convert.ToInt32(dataGridView1.Rows[a].Cells["out_per"].Value.ToString());
                    
                    count++;
                    total = total + Convert.ToInt32(data[8]);
                    in_per = (in_per + Convert.ToInt32(data[4])) / count;
                    out_per = (out_per + Convert.ToInt32(data[5])) / count;
                    int in_price = persent(in_per, total);
                    int out_price = persent(out_per, total);
                    int dif = out_price - in_price;

                    dataGridView1.Rows[a].Cells["content"].Value = data[3];
                    dataGridView1.Rows[a].Cells["count"].Value = count.ToString();
                    dataGridView1.Rows[a].Cells["total"].Value = total.ToString();
                    dataGridView1.Rows[a].Cells["in_per"].Value = in_per.ToString();
                    dataGridView1.Rows[a].Cells["out_per"].Value = out_per.ToString();
                    dataGridView1.Rows[a].Cells["in_price"].Value = in_price.ToString();
                    dataGridView1.Rows[a].Cells["out_price"].Value = out_price.ToString();
                    dataGridView1.Rows[a].Cells["dif"].Value = dif.ToString();
                    return;
                }
                else
                {
                    base_Setting(grid, data);
                    return;
                }
            }
        }
        private void base_Setting(string[] grid, string[] data)
        {
            grid[0] = data[0];
            grid[1] = data[1];
            grid[2] = data[2];
            grid[3] = data[3];
            grid[4] = "1";
            grid[5] = data[8];
            grid[6] = data[4];
            grid[7] = data[5];
            grid[8] = data[6];
            grid[9] = data[7];
            int dif = Convert.ToInt32(grid[9]) - Convert.ToInt32(grid[8]);
            grid[10] = dif.ToString();
            grid[11] = data[9];

            dataGridView1.Rows.Add(grid);
            return;
        }
        private int persent(int per, int total)
        {
            return per * total / 100;
        }
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                Commodity_Search cs = new Commodity_Search(this);
                cs.Clinet_name = tb_clt.Text;
                cs.Show();
            }
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Sales_Detail sd = new Sales_Detail(this);
            string out_date = dataGridView1.Rows[e.RowIndex].Cells["out_date"].Value.ToString().Substring(0, 10).Replace("-", "");
            int yesr = Convert.ToInt32(out_date.Substring(0, 4));
            int month = Convert.ToInt32(out_date.Substring(4, 2));
            int day = Convert.ToInt32(out_date.Substring(6, 2));
            sd.MdiParent = main;
            sd.WindowState = FormWindowState.Maximized;
            sd.Out_Date.Value = new DateTime(yesr, month, day);
            sd.tb_clt.Text = dataGridView1.Rows[e.RowIndex].Cells["clt"].Value.ToString();
            sd.Show();
        }
    }
}
