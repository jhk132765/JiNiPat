﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Account
{
    public partial class Remit_reg : Form
    {
        Main main;
        Helper_DB db = new Helper_DB();
        string compidx;
        public Remit_reg(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        private void Remit_reg_Load(object sender, EventArgs e)
        {
            db.DBcon();
            start_Date.Value = new DateTime(int.Parse(DateTime.Now.ToString("yyyy")),
                                            int.Parse(DateTime.Now.ToString("MM")), 1);

            string[] combo_list = { "미지급", "지급", "전체" };
            cb_remunerate.Items.AddRange(combo_list);
            cb_remunerate.SelectedIndex = 0;

        }
        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            Add_Row_CheckBox(sender, e, 1);
        }
        private void btn_lookup_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string Area = "`idx`, `date_reg`, `date_pay`, `purchase`, `bank_comp`, " +
                          "`bank_code`, `bank_num`, `bank_name`, `send_money`, `reg_man`, " +
                          "`etc`";
            List<string> list_tbl = new List<string>();
            List<string> list_col = new List<string>();
            list_tbl.Add("compidx");
            list_col.Add(compidx);
            if (cb_remunerate.SelectedIndex == 0|| cb_remunerate.SelectedIndex == 1) {
                list_tbl.Add("payment");
                if (cb_remunerate.SelectedIndex == 0) { list_col.Add("False"); }
                if (cb_remunerate.SelectedIndex == 1) { list_col.Add("True"); }
            }
            string[] Search_tbl = list_tbl.ToArray();
            string[] Search_col = list_col.ToArray();
            string cmd = db.More_DB_Search("Remit_reg", Search_tbl, Search_col, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] db_data = db_res.Split('|');
            lookup_grid(db_data);
            
        }
        void lookup_grid(string[] data)
        {//                    0   1    2   3   4   5   6   7   8   9   10  11
            string[] test = { "", null, "", "", "", "", "", "", "", "", "", "" };
            for(int a = 0; a < data.Length; a++)
            {
                if (a % 11 == 0) { test[0] = data[a]; }
                if (a % 11 == 1) { test[2] = data[a]; }
                if (a % 11 == 2) { test[3] = data[a]; }
                if (a % 11 == 3) { test[4] = data[a]; }
                if (a % 11 == 4) { test[5] = data[a]; }
                if (a % 11 == 5) { test[6] = data[a]; }
                if (a % 11 == 6) { test[7] = data[a]; }
                if (a % 11 == 7) { test[8] = data[a]; }
                if (a % 11 == 8) { test[9] = data[a]; }
                if (a % 11 == 9) { test[10] = data[a]; }
                if (a % 11 == 10) { test[11] = data[a]; dataGridView1.Rows.Add(test); }
            }
        }
        private void btn_complete_Click(object sender, EventArgs e)
        {
            int[] row_count = grid_chk();
            string update_tbl = "payment";
            string update_col = "True";
            string where_tbl = "idx";
            for(int a = 0; a < row_count.Length; a++)
            {
                string where_col = dataGridView1.Rows[row_count[a]].Cells[0].Value.ToString();
                string U_cmd = db.DB_Update("Remit_reg", update_tbl, update_col, where_tbl, where_col);
                db.DB_Send_CMD_reVoid(U_cmd);
                MessageBox.Show(where_col);
            }
        }
        int[] grid_chk()
        {
            List<int> result_list = new List<int>();
            for(int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                bool chk = Convert.ToBoolean(dataGridView1.Rows[a].Cells[1].Value);
                if (chk) { result_list.Add(a); }
            }
            int[] result = result_list.ToArray();

            return result;
        }
        public void Add_Row_CheckBox(object sender, DataGridViewCellPaintingEventArgs e, int colCount)
        {
            if (e.ColumnIndex == colCount && e.RowIndex == -1)
            {
                e.PaintBackground(e.ClipBounds, false);

                Point pt = e.CellBounds.Location;

                int nChkBoxWidth = 15;
                int nChkBoxHeight = 15;
                int offsetX = (e.CellBounds.Width - nChkBoxWidth) / 2;
                int offsetY = (e.CellBounds.Height - nChkBoxHeight) / 2;

                pt.X += offsetX;
                pt.Y += offsetY;

                CheckBox cb = new CheckBox();
                cb.Size = new Size(nChkBoxWidth, nChkBoxHeight);
                cb.Location = pt;
                cb.CheckedChanged += new EventHandler(datagridview_checkBox_Click);
                ((DataGridView)sender).Controls.Add(cb);

                e.Handled = true;
            }
        }
        private void datagridview_checkBox_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                r.Cells["chkbox"].Value = ((CheckBox)sender).Checked;
            }
        }
        private void btn_Excel_Click(object sender, EventArgs e)
        {
            Excel_text et = new Excel_text();
            string[] Excel_title = { "은행코드", "계좌번호", "", "금액", "예금주" };
            string[,] inputExcel = new string[dataGridView1.RowCount, 5];
            for (int a = 0; a < dataGridView1.RowCount; a++)
            {
                inputExcel[a, 0] = dataGridView1.Rows[a].Cells["bank_code"].Value.ToString();
                inputExcel[a, 1] = dataGridView1.Rows[a].Cells["bank_num"].Value.ToString();
                inputExcel[a, 3] = dataGridView1.Rows[a].Cells["send_money"].Value.ToString();
                inputExcel[a, 4] = dataGridView1.Rows[a].Cells["bank_name"].Value.ToString();
            }
            et.Mk_Excel(Excel_title, inputExcel);
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            int col = e.ColumnIndex;

            if (col == 1) {
                if (Convert.ToBoolean(dataGridView1.Rows[row].Cells[1].Value)) {
                    dataGridView1.Rows[row].Cells[col].Value = false;
                }
                else { dataGridView1.Rows[row].Cells[col].Value = true; }
            }
            else {
                // 우측 텍스트박스에 데이터 삽입
                tb_list_date.Text = dataGridView1.Rows[row].Cells["list_date"].Value.ToString();
                tb_date_pay.Text = dataGridView1.Rows[row].Cells["date_pay"].Value.ToString();
                tb_purchase.Text = dataGridView1.Rows[row].Cells["purchase"].Value.ToString();
                tb_bank_comp.Text = dataGridView1.Rows[row].Cells["bank_comp"].Value.ToString();
                tb_bank_code.Text = dataGridView1.Rows[row].Cells["bank_code"].Value.ToString();
                tb_bank_num.Text = dataGridView1.Rows[row].Cells["bank_num"].Value.ToString();
                tb_bank_name.Text = dataGridView1.Rows[row].Cells["bank_name"].Value.ToString();
                tb_send_money.Text = dataGridView1.Rows[row].Cells["send_money"].Value.ToString();
                tb_charge.Text = dataGridView1.Rows[row].Cells["charge"].Value.ToString();
                tb_etc.Text = dataGridView1.Rows[row].Cells["etc"].Value.ToString();
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
