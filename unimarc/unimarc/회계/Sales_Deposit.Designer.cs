﻿namespace WindowsFormsApp1.Account
{
    partial class Sales_Deposit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rb_clt = new System.Windows.Forms.RadioButton();
            this.rb_Date = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Start_Date = new System.Windows.Forms.DateTimePicker();
            this.btn_Close = new System.Windows.Forms.Button();
            this.End_Date = new System.Windows.Forms.DateTimePicker();
            this.btn_Lookup = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Grid0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tb_grid1 = new System.Windows.Forms.TextBox();
            this.tb_grid2 = new System.Windows.Forms.TextBox();
            this.tb_grid3 = new System.Windows.Forms.TextBox();
            this.tb_grid4 = new System.Windows.Forms.TextBox();
            this.tb_grid5 = new System.Windows.Forms.TextBox();
            this.tb_grid6 = new System.Windows.Forms.TextBox();
            this.tb_grid7 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rb_clt);
            this.panel1.Controls.Add(this.rb_Date);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.Start_Date);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.End_Date);
            this.panel1.Controls.Add(this.btn_Lookup);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(691, 40);
            this.panel1.TabIndex = 84;
            // 
            // rb_clt
            // 
            this.rb_clt.AutoSize = true;
            this.rb_clt.Location = new System.Drawing.Point(377, 12);
            this.rb_clt.Name = "rb_clt";
            this.rb_clt.Size = new System.Drawing.Size(71, 16);
            this.rb_clt.TabIndex = 82;
            this.rb_clt.TabStop = true;
            this.rb_clt.Text = "거래처별";
            this.rb_clt.UseVisualStyleBackColor = true;
            this.rb_clt.CheckedChanged += new System.EventHandler(this.radio_Button_CheckedChanged);
            // 
            // rb_Date
            // 
            this.rb_Date.AutoSize = true;
            this.rb_Date.Location = new System.Drawing.Point(309, 12);
            this.rb_Date.Name = "rb_Date";
            this.rb_Date.Size = new System.Drawing.Size(59, 16);
            this.rb_Date.TabIndex = 82;
            this.rb_Date.TabStop = true;
            this.rb_Date.Text = "일자별";
            this.rb_Date.UseVisualStyleBackColor = true;
            this.rb_Date.CheckedChanged += new System.EventHandler(this.radio_Button_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(161, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 12);
            this.label4.TabIndex = 37;
            this.label4.Text = "~";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 37;
            this.label2.Text = "조회기간";
            // 
            // Start_Date
            // 
            this.Start_Date.CustomFormat = "yyyy-MM-dd";
            this.Start_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Start_Date.Location = new System.Drawing.Point(70, 9);
            this.Start_Date.Name = "Start_Date";
            this.Start_Date.Size = new System.Drawing.Size(85, 21);
            this.Start_Date.TabIndex = 38;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(598, 8);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 81;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // End_Date
            // 
            this.End_Date.CustomFormat = "yyyy-MM-dd";
            this.End_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.End_Date.Location = new System.Drawing.Point(181, 9);
            this.End_Date.Name = "End_Date";
            this.End_Date.Size = new System.Drawing.Size(85, 21);
            this.End_Date.TabIndex = 39;
            // 
            // btn_Lookup
            // 
            this.btn_Lookup.Location = new System.Drawing.Point(507, 8);
            this.btn_Lookup.Name = "btn_Lookup";
            this.btn_Lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_Lookup.TabIndex = 79;
            this.btn_Lookup.Text = "조    회";
            this.btn_Lookup.UseVisualStyleBackColor = true;
            this.btn_Lookup.Click += new System.EventHandler(this.btn_Lookup_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Grid0,
            this.Grid1,
            this.Grid2,
            this.Grid3,
            this.Grid4,
            this.Grid5,
            this.Grid6,
            this.Grid7});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(12, 58);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 20;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(691, 492);
            this.dataGridView1.TabIndex = 85;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // Grid0
            // 
            this.Grid0.HeaderText = "거래처명";
            this.Grid0.Name = "Grid0";
            // 
            // Grid1
            // 
            this.Grid1.HeaderText = "이월미수금";
            this.Grid1.Name = "Grid1";
            this.Grid1.Width = 90;
            // 
            // Grid2
            // 
            this.Grid2.HeaderText = "건수";
            this.Grid2.Name = "Grid2";
            this.Grid2.Width = 50;
            // 
            // Grid3
            // 
            this.Grid3.HeaderText = "권수";
            this.Grid3.Name = "Grid3";
            this.Grid3.Width = 50;
            // 
            // Grid4
            // 
            this.Grid4.HeaderText = "매출금액";
            this.Grid4.Name = "Grid4";
            this.Grid4.Width = 90;
            // 
            // Grid5
            // 
            this.Grid5.HeaderText = "입금액";
            this.Grid5.Name = "Grid5";
            this.Grid5.Width = 90;
            // 
            // Grid6
            // 
            this.Grid6.HeaderText = "기간잔고";
            this.Grid6.Name = "Grid6";
            this.Grid6.Width = 90;
            // 
            // Grid7
            // 
            this.Grid7.HeaderText = "최종잔고";
            this.Grid7.Name = "Grid7";
            this.Grid7.Width = 90;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(13, 556);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(20, 21);
            this.textBox1.TabIndex = 86;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(33, 556);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 21);
            this.textBox2.TabIndex = 86;
            this.textBox2.Text = "합계";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_grid1
            // 
            this.tb_grid1.Enabled = false;
            this.tb_grid1.Location = new System.Drawing.Point(133, 556);
            this.tb_grid1.Name = "tb_grid1";
            this.tb_grid1.Size = new System.Drawing.Size(90, 21);
            this.tb_grid1.TabIndex = 86;
            this.tb_grid1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_grid1.TextChanged += new System.EventHandler(this.tb_grid7_TextChanged);
            // 
            // tb_grid2
            // 
            this.tb_grid2.Enabled = false;
            this.tb_grid2.Location = new System.Drawing.Point(223, 556);
            this.tb_grid2.Name = "tb_grid2";
            this.tb_grid2.Size = new System.Drawing.Size(50, 21);
            this.tb_grid2.TabIndex = 86;
            this.tb_grid2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_grid2.TextChanged += new System.EventHandler(this.tb_grid7_TextChanged);
            // 
            // tb_grid3
            // 
            this.tb_grid3.Enabled = false;
            this.tb_grid3.Location = new System.Drawing.Point(273, 556);
            this.tb_grid3.Name = "tb_grid3";
            this.tb_grid3.Size = new System.Drawing.Size(50, 21);
            this.tb_grid3.TabIndex = 86;
            this.tb_grid3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_grid3.TextChanged += new System.EventHandler(this.tb_grid7_TextChanged);
            // 
            // tb_grid4
            // 
            this.tb_grid4.Enabled = false;
            this.tb_grid4.Location = new System.Drawing.Point(323, 556);
            this.tb_grid4.Name = "tb_grid4";
            this.tb_grid4.Size = new System.Drawing.Size(90, 21);
            this.tb_grid4.TabIndex = 86;
            this.tb_grid4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_grid4.TextChanged += new System.EventHandler(this.tb_grid7_TextChanged);
            // 
            // tb_grid5
            // 
            this.tb_grid5.Enabled = false;
            this.tb_grid5.Location = new System.Drawing.Point(413, 556);
            this.tb_grid5.Name = "tb_grid5";
            this.tb_grid5.Size = new System.Drawing.Size(90, 21);
            this.tb_grid5.TabIndex = 86;
            this.tb_grid5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_grid5.TextChanged += new System.EventHandler(this.tb_grid7_TextChanged);
            // 
            // tb_grid6
            // 
            this.tb_grid6.Enabled = false;
            this.tb_grid6.Location = new System.Drawing.Point(503, 556);
            this.tb_grid6.Name = "tb_grid6";
            this.tb_grid6.Size = new System.Drawing.Size(90, 21);
            this.tb_grid6.TabIndex = 86;
            this.tb_grid6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_grid6.TextChanged += new System.EventHandler(this.tb_grid7_TextChanged);
            // 
            // tb_grid7
            // 
            this.tb_grid7.Enabled = false;
            this.tb_grid7.Location = new System.Drawing.Point(593, 556);
            this.tb_grid7.Name = "tb_grid7";
            this.tb_grid7.Size = new System.Drawing.Size(90, 21);
            this.tb_grid7.TabIndex = 86;
            this.tb_grid7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_grid7.TextChanged += new System.EventHandler(this.tb_grid7_TextChanged);
            // 
            // Sales_Deposit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 583);
            this.Controls.Add(this.tb_grid7);
            this.Controls.Add(this.tb_grid6);
            this.Controls.Add(this.tb_grid5);
            this.Controls.Add(this.tb_grid4);
            this.Controls.Add(this.tb_grid3);
            this.Controls.Add(this.tb_grid2);
            this.Controls.Add(this.tb_grid1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Name = "Sales_Deposit";
            this.Text = "매출집계";
            this.Load += new System.EventHandler(this.Sales_Deposit_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rb_clt;
        private System.Windows.Forms.RadioButton rb_Date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Close;
        public System.Windows.Forms.Button btn_Lookup;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grid0;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grid3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grid4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grid5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grid6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grid7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox tb_grid1;
        private System.Windows.Forms.TextBox tb_grid2;
        private System.Windows.Forms.TextBox tb_grid3;
        private System.Windows.Forms.TextBox tb_grid4;
        private System.Windows.Forms.TextBox tb_grid5;
        private System.Windows.Forms.TextBox tb_grid6;
        private System.Windows.Forms.TextBox tb_grid7;
        public System.Windows.Forms.DateTimePicker Start_Date;
        public System.Windows.Forms.DateTimePicker End_Date;
    }
}