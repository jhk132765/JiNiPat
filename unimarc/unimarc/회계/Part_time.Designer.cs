﻿
namespace UniMarc.회계
{
    partial class Part_time
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rb_pay = new System.Windows.Forms.RadioButton();
            this.rb_minpay = new System.Windows.Forms.RadioButton();
            this.end_30min = new System.Windows.Forms.CheckBox();
            this.start_30min = new System.Windows.Forms.CheckBox();
            this.btn_Add = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.cb_work_time = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_end_hour = new System.Windows.Forms.ComboBox();
            this.cb_start_hour = new System.Windows.Forms.ComboBox();
            this.set_date = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.start_date = new System.Windows.Forms.DateTimePicker();
            this.end_date = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_name = new System.Windows.Forms.ComboBox();
            this.btn_Lookup = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_pay = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_pay = new System.Windows.Forms.TextBox();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Per_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.start = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.end = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.today = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.work = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.del_chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.work_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.start2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.end2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.all_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time_pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.work2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tb_pay);
            this.panel1.Controls.Add(this.rb_pay);
            this.panel1.Controls.Add(this.rb_minpay);
            this.panel1.Controls.Add(this.end_30min);
            this.panel1.Controls.Add(this.start_30min);
            this.panel1.Controls.Add(this.cb_work_time);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cb_end_hour);
            this.panel1.Controls.Add(this.cb_start_hour);
            this.panel1.Controls.Add(this.set_date);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(908, 42);
            this.panel1.TabIndex = 0;
            // 
            // rb_pay
            // 
            this.rb_pay.AutoSize = true;
            this.rb_pay.Location = new System.Drawing.Point(678, 12);
            this.rb_pay.Name = "rb_pay";
            this.rb_pay.Size = new System.Drawing.Size(71, 16);
            this.rb_pay.TabIndex = 2;
            this.rb_pay.TabStop = true;
            this.rb_pay.Text = "직접입력";
            this.rb_pay.UseVisualStyleBackColor = true;
            this.rb_pay.CheckedChanged += new System.EventHandler(this.rb_pay_CheckedChanged);
            // 
            // rb_minpay
            // 
            this.rb_minpay.AutoSize = true;
            this.rb_minpay.Location = new System.Drawing.Point(601, 6);
            this.rb_minpay.Name = "rb_minpay";
            this.rb_minpay.Size = new System.Drawing.Size(71, 28);
            this.rb_minpay.TabIndex = 2;
            this.rb_minpay.TabStop = true;
            this.rb_minpay.Text = "최저시급\n8,720원";
            this.rb_minpay.UseVisualStyleBackColor = true;
            this.rb_minpay.CheckedChanged += new System.EventHandler(this.rb_pay_CheckedChanged);
            // 
            // end_30min
            // 
            this.end_30min.AutoSize = true;
            this.end_30min.Location = new System.Drawing.Point(335, 12);
            this.end_30min.Name = "end_30min";
            this.end_30min.Size = new System.Drawing.Size(48, 16);
            this.end_30min.TabIndex = 2;
            this.end_30min.Text = "30분";
            this.end_30min.UseVisualStyleBackColor = true;
            // 
            // start_30min
            // 
            this.start_30min.AutoSize = true;
            this.start_30min.Location = new System.Drawing.Point(192, 12);
            this.start_30min.Name = "start_30min";
            this.start_30min.Size = new System.Drawing.Size(48, 16);
            this.start_30min.TabIndex = 2;
            this.start_30min.Text = "30분";
            this.start_30min.UseVisualStyleBackColor = true;
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(597, 60);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(75, 23);
            this.btn_Add.TabIndex = 23;
            this.btn_Add.Text = "추   가";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(759, 60);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 23;
            this.btn_Save.Text = "저   장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(840, 60);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 23;
            this.btn_Close.Text = "닫   기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // cb_work_time
            // 
            this.cb_work_time.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_work_time.FormattingEnabled = true;
            this.cb_work_time.Location = new System.Drawing.Point(458, 9);
            this.cb_work_time.Name = "cb_work_time";
            this.cb_work_time.Size = new System.Drawing.Size(121, 20);
            this.cb_work_time.TabIndex = 22;
            this.cb_work_time.SelectedIndexChanged += new System.EventHandler(this.cb_work_time_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(395, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "업무 구분";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(241, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "~";
            // 
            // cb_end_hour
            // 
            this.cb_end_hour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_end_hour.FormattingEnabled = true;
            this.cb_end_hour.Location = new System.Drawing.Point(258, 9);
            this.cb_end_hour.Name = "cb_end_hour";
            this.cb_end_hour.Size = new System.Drawing.Size(71, 20);
            this.cb_end_hour.TabIndex = 21;
            // 
            // cb_start_hour
            // 
            this.cb_start_hour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_start_hour.FormattingEnabled = true;
            this.cb_start_hour.Location = new System.Drawing.Point(115, 9);
            this.cb_start_hour.Name = "cb_start_hour";
            this.cb_start_hour.Size = new System.Drawing.Size(71, 20);
            this.cb_start_hour.TabIndex = 21;
            // 
            // set_date
            // 
            this.set_date.CustomFormat = "yyyy-MM-dd";
            this.set_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.set_date.Location = new System.Drawing.Point(12, 9);
            this.set_date.Name = "set_date";
            this.set_date.Size = new System.Drawing.Size(85, 21);
            this.set_date.TabIndex = 20;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.date,
            this.Per_name,
            this.start,
            this.end,
            this.minus,
            this.today,
            this.pay,
            this.work,
            this.del_chk});
            this.dataGridView1.Location = new System.Drawing.Point(12, 89);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 30;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(908, 146);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValidated);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.work_date,
            this.name1,
            this.start2,
            this.end2,
            this.all_time,
            this.time_pay,
            this.total,
            this.work2});
            this.dataGridView2.Location = new System.Drawing.Point(12, 315);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersWidth = 30;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(908, 151);
            this.dataGridView2.TabIndex = 1;
            // 
            // start_date
            // 
            this.start_date.CustomFormat = "yyyy-MM-01";
            this.start_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.start_date.Location = new System.Drawing.Point(12, 9);
            this.start_date.Name = "start_date";
            this.start_date.Size = new System.Drawing.Size(85, 21);
            this.start_date.TabIndex = 20;
            // 
            // end_date
            // 
            this.end_date.CustomFormat = "yyyy-MM-dd";
            this.end_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_date.Location = new System.Drawing.Point(116, 9);
            this.end_date.Name = "end_date";
            this.end_date.Size = new System.Drawing.Size(85, 21);
            this.end_date.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(99, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "~";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(252, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "이 름";
            // 
            // cb_name
            // 
            this.cb_name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_name.FormattingEnabled = true;
            this.cb_name.Location = new System.Drawing.Point(296, 10);
            this.cb_name.Name = "cb_name";
            this.cb_name.Size = new System.Drawing.Size(121, 20);
            this.cb_name.TabIndex = 22;
            // 
            // btn_Lookup
            // 
            this.btn_Lookup.Location = new System.Drawing.Point(479, 9);
            this.btn_Lookup.Name = "btn_Lookup";
            this.btn_Lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_Lookup.TabIndex = 23;
            this.btn_Lookup.Text = "조   회";
            this.btn_Lookup.UseVisualStyleBackColor = true;
            this.btn_Lookup.Click += new System.EventHandler(this.btn_Lookup_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbl_pay);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.btn_Lookup);
            this.panel2.Controls.Add(this.cb_name);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.end_date);
            this.panel2.Controls.Add(this.start_date);
            this.panel2.Location = new System.Drawing.Point(12, 266);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(908, 41);
            this.panel2.TabIndex = 0;
            // 
            // lbl_pay
            // 
            this.lbl_pay.AutoSize = true;
            this.lbl_pay.Location = new System.Drawing.Point(714, 14);
            this.lbl_pay.Name = "lbl_pay";
            this.lbl_pay.Size = new System.Drawing.Size(11, 12);
            this.lbl_pay.TabIndex = 24;
            this.lbl_pay.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(648, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "총 지급액 : ";
            // 
            // tb_pay
            // 
            this.tb_pay.Location = new System.Drawing.Point(751, 9);
            this.tb_pay.Name = "tb_pay";
            this.tb_pay.Size = new System.Drawing.Size(107, 21);
            this.tb_pay.TabIndex = 24;
            this.tb_pay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_pay_KeyPress);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(678, 60);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(75, 23);
            this.btn_Delete.TabIndex = 25;
            this.btn_Delete.Text = "삭   제";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // date
            // 
            this.date.HeaderText = "날짜";
            this.date.Name = "date";
            this.date.Width = 80;
            // 
            // Per_name
            // 
            this.Per_name.HeaderText = "이름";
            this.Per_name.Name = "Per_name";
            this.Per_name.Width = 200;
            // 
            // start
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.start.DefaultCellStyle = dataGridViewCellStyle2;
            this.start.HeaderText = "근무시작";
            this.start.Name = "start";
            this.start.Width = 65;
            // 
            // end
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.end.DefaultCellStyle = dataGridViewCellStyle3;
            this.end.HeaderText = "근무종료";
            this.end.Name = "end";
            this.end.Width = 65;
            // 
            // minus
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.minus.DefaultCellStyle = dataGridViewCellStyle4;
            this.minus.HeaderText = "업무제외";
            this.minus.Name = "minus";
            this.minus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // today
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.today.DefaultCellStyle = dataGridViewCellStyle5;
            this.today.HeaderText = "금일근무";
            this.today.Name = "today";
            // 
            // pay
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.pay.DefaultCellStyle = dataGridViewCellStyle6;
            this.pay.HeaderText = "시급";
            this.pay.Name = "pay";
            this.pay.Width = 60;
            // 
            // work
            // 
            this.work.HeaderText = "작업내용";
            this.work.Name = "work";
            this.work.Width = 150;
            // 
            // del_chk
            // 
            this.del_chk.HeaderText = "삭제";
            this.del_chk.Name = "del_chk";
            this.del_chk.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.del_chk.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.del_chk.Width = 40;
            // 
            // work_date
            // 
            this.work_date.HeaderText = "날짜";
            this.work_date.Name = "work_date";
            this.work_date.ReadOnly = true;
            this.work_date.Width = 80;
            // 
            // name1
            // 
            this.name1.HeaderText = "이름";
            this.name1.Name = "name1";
            this.name1.ReadOnly = true;
            this.name1.Width = 200;
            // 
            // start2
            // 
            this.start2.HeaderText = "근무시작";
            this.start2.Name = "start2";
            this.start2.ReadOnly = true;
            this.start2.Width = 65;
            // 
            // end2
            // 
            this.end2.HeaderText = "근무종료";
            this.end2.Name = "end2";
            this.end2.ReadOnly = true;
            this.end2.Width = 65;
            // 
            // all_time
            // 
            this.all_time.HeaderText = "총 근무시간";
            this.all_time.Name = "all_time";
            this.all_time.ReadOnly = true;
            // 
            // time_pay
            // 
            this.time_pay.HeaderText = "시급";
            this.time_pay.Name = "time_pay";
            this.time_pay.ReadOnly = true;
            this.time_pay.Width = 80;
            // 
            // total
            // 
            this.total.HeaderText = "지급금액";
            this.total.Name = "total";
            this.total.ReadOnly = true;
            // 
            // work2
            // 
            this.work2.HeaderText = "작업내용";
            this.work2.Name = "work2";
            this.work2.ReadOnly = true;
            this.work2.Width = 150;
            // 
            // Part_time
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 478);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Save);
            this.Name = "Part_time";
            this.Text = "파트타임 직원관리";
            this.Load += new System.EventHandler(this.Part_time_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker set_date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_end_hour;
        private System.Windows.Forms.ComboBox cb_start_hour;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cb_work_time;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.CheckBox end_30min;
        private System.Windows.Forms.CheckBox start_30min;
        private System.Windows.Forms.DateTimePicker start_date;
        private System.Windows.Forms.DateTimePicker end_date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_name;
        private System.Windows.Forms.Button btn_Lookup;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbl_pay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rb_minpay;
        private System.Windows.Forms.RadioButton rb_pay;
        private System.Windows.Forms.TextBox tb_pay;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Per_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn start;
        private System.Windows.Forms.DataGridViewTextBoxColumn end;
        private System.Windows.Forms.DataGridViewTextBoxColumn minus;
        private System.Windows.Forms.DataGridViewTextBoxColumn today;
        private System.Windows.Forms.DataGridViewTextBoxColumn pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn work;
        private System.Windows.Forms.DataGridViewCheckBoxColumn del_chk;
        private System.Windows.Forms.DataGridViewTextBoxColumn work_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn name1;
        private System.Windows.Forms.DataGridViewTextBoxColumn start2;
        private System.Windows.Forms.DataGridViewTextBoxColumn end2;
        private System.Windows.Forms.DataGridViewTextBoxColumn all_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn time_pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn work2;
    }
}