﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// 추가된 참조
using WindowsFormsApp1.Delivery;

namespace WindowsFormsApp1.회계
{
    public partial class Sales_In_Pay : Form
    {
        Main main;
        Helper_DB db = new Helper_DB();
        public string compidx;
        bool add_chk = false;
        private int row = -1;
        public Sales_In_Pay(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        private void Sales_In_Pay_Load(object sender, EventArgs e)
        {
            db.DBcon();
            string[] combo = { "현금", "통장", "카드", "어음" };
            cb_gubun.Items.AddRange(combo);
            Start_Date.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
        private void btn_Add_Click(object sender, EventArgs e)
        {
            Pay_In_Date.Value = End_Date.Value;
            tb_clt.Text = "";
            tb_price.Text = "";
            tb_etc.Text = "";
            cb_gubun.SelectedIndex = -1;

            add_chk = true;
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (tb_clt.Text == "") { MessageBox.Show("거래처를 입력해주세요."); return; }
            if (tb_price.Text == "") { MessageBox.Show("금액을 입력해주세요."); return; }
            if (cb_gubun.SelectedIndex < 0) { MessageBox.Show("구분을 선택해주세요."); return; }

            string date = Pay_In_Date.Text.Substring(0, 10);
            if (row < 0 || !add_chk) {
                string[] Area = { "compidx", "date", "client", "deposit", "gu", "etc" };
                string[] Data = { compidx, date, tb_clt.Text, tb_price.Text, cb_gubun.Text, tb_etc.Text };

                string Incmd = db.DB_INSERT("Sales", Area, Data);
                db.DB_Send_CMD_reVoid(Incmd);
                insert_data(date);
            }
            else {
                string[] edit_col = { "date", "client", "deposit", "gu", "etc" };
                string[] edit_data = { date, tb_clt.Text, tb_price.Text, cb_gubun.Text, tb_etc.Text };
                string[] sear_col = { "idx" };
                string[] sear_data = { dataGridView1.Rows[row].Cells["idx"].Value.ToString() };

                string U_cmd = db.More_Update("Sales", edit_col, edit_data, sear_col, sear_data);
                db.DB_Send_CMD_reVoid(U_cmd);
                update_data(date);
            }
            add_chk = false;
            btn_Lookup_Click(null, e);
        }
        #region Btn_Save_Click_Sub
        private void insert_data(string date)
        {
            string[] grid = { "", date, tb_clt.Text, tb_price.Text, cb_gubun.Text, tb_etc.Text };
            dataGridView1.Rows.Add(grid);
        }
        private void update_data(string date)
        {
            dataGridView1.Rows[row].Cells["date"].Value = date;
            dataGridView1.Rows[row].Cells["client"].Value = tb_clt.Text;
            dataGridView1.Rows[row].Cells["deposit"].Value = tb_price.Text;
            dataGridView1.Rows[row].Cells["gu"].Value = cb_gubun.SelectedItem;
            dataGridView1.Rows[row].Cells["etc"].Value = tb_etc.Text;
        }
        #endregion
        private void btn_Delete_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("삭제하시겠습니까?", "삭제", MessageBoxButtons.YesNo) == DialogResult.No) { return; }
            
            string D_cmd = db.DB_Delete("Sales", "compidx", compidx, "idx", dataGridView1.Rows[row].Cells["idx"].Value.ToString());
            db.DB_Send_CMD_reVoid(D_cmd);
            dataGridView1.Rows.Remove(dataGridView1.Rows[row]);
            
            add_chk = false;
        }
        private void btn_Lookup_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string table = "`idx`, `date`, `client`, `deposit`, `gu`, `etc`";
            string start = Start_Date.Text.Substring(0, 10);
            string end = End_Date.Text.Substring(0, 10);

            string cmd = db.Search_Date("Sales", table, "date", start, end, compidx);
            string db_res = db.DB_Send_CMD_Search(cmd);
            made_grid(db_res);
            add_chk = false;
            Print_Total();
        }
        #region Btn_Lookup_Click_Sub
        private void made_grid(string value)
        {
            string[] grid = { "", "", "", "", "", "" };
            string[] ary = value.Split('|');

            int cot = 6;
            for (int a = 0; a < ary.Length; a++) 
            {
                if (a % cot == 0) { grid[0] = ary[a]; }
                if (a % cot == 1) { grid[1] = ary[a]; }
                if (a % cot == 2) { grid[2] = ary[a]; }
                if (a % cot == 3) { grid[3] = ary[a]; }
                if (a % cot == 4) { grid[4] = ary[a]; }
                if (a % cot == 5) { grid[5] = ary[a];
                    if (grid[4] != "") {
                        dataGridView1.Rows.Add(grid);
                    }
                }
            }
        }
        private void Print_Total()
        {
            if (dataGridView1.Rows.Count <= 0) { return; }
            int total = 0;
            for(int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                total += Convert.ToInt32(dataGridView1.Rows[a].Cells["price"].Value.ToString());
            }
            lbl_total.Text = total.ToString();
        }
        #endregion
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            row = e.RowIndex;
            if(e.RowIndex < 0) { return; }
            input_Text();
        }
        #region Grid_CellClick_Sub
        private void input_Text()
        {
            string date = dataGridView1.Rows[row].Cells["date"].Value.ToString().Replace("-", "");
            int year = Convert.ToInt32(date.Substring(0, 4));
            int month = Convert.ToInt32(date.Substring(4, 2));
            int day = Convert.ToInt32(date.Substring(6, 2));

            Pay_In_Date.Value = new DateTime(year, month, day);
            tb_clt.Text = dataGridView1.Rows[row].Cells["clt"].Value.ToString();
            tb_price.Text = dataGridView1.Rows[row].Cells["price"].Value.ToString();
            cb_gubun.SelectedItem = dataGridView1.Rows[row].Cells["gu"].Value.ToString();
            tb_etc.Text = dataGridView1.Rows[row].Cells["etc"].Value.ToString();
        }
        #endregion
        private void tb_clt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Commodity_Search cs = new Commodity_Search(this);
                cs.Clinet_name = tb_clt.Text;
                cs.Show();
            }
        }
        private void tb_price_KeyPress(object sender, KeyPressEventArgs e)
        {
            String_Text st = new String_Text();
            st.Only_Int(sender, e);
        }
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
