﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// 참조 추가
using WindowsFormsApp1.Delivery;

namespace WindowsFormsApp1.Account
{
    public partial class Sales_Input : Form
    {
        Main main;
        public string compidx;

        Helper_DB db = new Helper_DB();
        public Sales_Input(Main _main)
        {
            InitializeComponent();
            main = _main;
        }
        private void Sales_Input_Load(object sender, EventArgs e)
        {
            db.DBcon();
            compidx = main.com_idx;
        }
        private void btn_Add_Click(object sender, EventArgs e)
        {
            if (sender != null) { 
                // 위 패널
                tb_clt.Text = "";
                tb_etc.Text = "";
            }
            // 아래 패널
            tb_isbn.Text = "";
            tb_book_name.Text = "";
            tb_author.Text = "";
            tb_book_comp.Text = "";
            tb_price.Text = "";
            tb_persent.Text = "";
            tb_count.Text = "";
            tb_total.Text = "";
        }
        private void btn_Clear_Click(object sender, EventArgs e)
        {
            btn_Add_Click(null, null);
        }
        private void btn_Total_Click(object sender, EventArgs e)
        {
            if (tb_price.Text != "" && tb_count.Text != "")
            {
                int price = Convert.ToInt32(tb_price.Text);
                int count = Convert.ToInt32(tb_count.Text);
                int total = price * count;

                tb_total.Text = total.ToString();
            }
        }
        private void btn_Input_Click(object sender, EventArgs e)
        {
            btn_Total_Click(null, null);

            if (tb_book_name.Text == "") { MessageBox.Show("도서명이 입력해주세요."); return; }
            // 도서명/저자/출판사/정가/출고율/부수/합계금액/ISBN
            string[] grid = { 
                tb_book_name.Text,
                tb_author.Text,
                tb_book_comp.Text,
                tb_price.Text,
                tb_persent.Text,
                tb_count.Text,
                tb_total.Text,
                tb_isbn.Text
            };

            dataGridView1.Rows.Add(grid);

            btn_Clear_Click(null, null);
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (tb_clt.Text == "") { MessageBox.Show("거래처를 입력해주세요."); return; }
            if (dataGridView1.Rows.Count <= 0) { MessageBox.Show("저장할 내용이 없습니다."); return; }

            string date = out_date.Text.Substring(0, 10);

            string[] col_name = { "compidx", "date", "client", "isbn", "book_name", 
                                  "author", "book_comp", "price", "count", "total", 
                                  "out_per", "etc", "tel", "out_price" };
            for(int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                string[] set_data = { compidx, date, tb_clt.Text,
                    dataGridView1.Rows[a].Cells["ISBN"].Value.ToString(),
                    dataGridView1.Rows[a].Cells["book_name"].Value.ToString(),
                    dataGridView1.Rows[a].Cells["author"].Value.ToString(),
                    dataGridView1.Rows[a].Cells["book_comp"].Value.ToString(),
                    dataGridView1.Rows[a].Cells["price"].Value.ToString(),
                    dataGridView1.Rows[a].Cells["count"].Value.ToString(),
                    dataGridView1.Rows[a].Cells["total"].Value.ToString(),
                    dataGridView1.Rows[a].Cells["persent"].Value.ToString(),
                    tb_etc.Text,
                    lbl_tel.Text,
                    out_price(a)
                };
                string Incmd = db.DB_INSERT("Sales", col_name, set_data);
                db.DB_Send_CMD_reVoid(Incmd);
            }
        }
        private string out_price(int idx)
        {
            int total = Convert.ToInt32(dataGridView1.Rows[idx].Cells["total"].Value.ToString());
            int per = Convert.ToInt32(dataGridView1.Rows[idx].Cells["persent"].Value.ToString());

            double tmp = total * per * 0.01;
            return Convert.ToInt32(tmp).ToString();
        }
        private void chk_Add5000_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_Add5000.Checked) { dataGridView1.Rows.Add(5000); }
            else { 
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
            }
        }
        private void tb_price_KeyPress(object sender, KeyPressEventArgs e)
        {
            String_Text st = new String_Text();
            st.Only_Int(sender, e);
        }
        private void tb_pur_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Commodity_Search cs = new Commodity_Search(this);
                cs.Clinet_name = tb_clt.Text;
                cs.Show();
            }
        }
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
