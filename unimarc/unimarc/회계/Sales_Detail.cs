﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Account;

namespace WindowsFormsApp1.회계
{
    public partial class Sales_Detail : Form
    {
        public string compidx;
        Helper_DB db = new Helper_DB();
        Main main;
        Sales_Book sb;
        Sales_Lookup sl;
        public Sales_Detail(Sales_Book _sb)
        {
            InitializeComponent();
            sb = _sb;
            compidx = sb.compidx;
        }
        public Sales_Detail(Sales_Lookup _sl)
        {
            InitializeComponent();
            sl = _sl;
            compidx = sl.compidx;
        }
        private void Sales_Detail_Load(object sender, EventArgs e)
        {
            db.DBcon();

            btn_lookup_Click(null, null);
        }
        private void btn_per_change_Click(object sender, EventArgs e)
        {
            string per = tb_out_per.Text;

            if (dataGridView1.Rows.Count < 0) { return; }
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                dataGridView1.Rows[a].Cells["out_per"].Value = per;
                cal_total(a);
            }
        }
        void cal_total(int row)
        {
            int price = Convert.ToInt32(dataGridView1.Rows[row].Cells["price"].Value.ToString());
            int count = Convert.ToInt32(dataGridView1.Rows[row].Cells["count"].Value.ToString());
            int o_per = Convert.ToInt32(dataGridView1.Rows[row].Cells["out_per"].Value.ToString());

            double res_do = price * count * o_per / 100;
            int res = Convert.ToInt32(res_do);
            dataGridView1.Rows[row].Cells["out_price"].Value = res.ToString();
        }
        private void btn_lookup_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string area = "`idx`, `book_name`, `author`, `book_comp`, `price`, " +
                          "`count`, `out_per`, `in_per`, `list_date`, `list_name`, " +
                          "`out_price`, `in_price`, `etc`";

            string[] search_col = { "compidx", "date", "client" };
            string[] search_data = { compidx, Out_Date.Value.ToString().Substring(0, 10), tb_clt.Text };
            string cmd = db.More_DB_Search("Sales", search_col, search_data, area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] ary = db_res.Split('|');
            input_grid(ary);
        }
        void input_grid(string[] db_data)
        {
            string[] grid = { "", "", "", "", "",
                              "", "", "", "", "",
                              "", "", "" };
            int cnt = 13;
            for (int a = 0; a < db_data.Length; a++)
            {
                if (a % cnt == 0) { grid[0] = db_data[a]; }
                if (a % cnt == 1) { grid[1] = db_data[a]; }
                if (a % cnt == 2) { grid[2] = db_data[a]; }
                if (a % cnt == 3) { grid[3] = db_data[a]; }
                if (a % cnt == 4) { grid[4] = db_data[a]; }
                if (a % cnt == 5) { grid[5] = db_data[a]; }
                if (a % cnt == 6) { grid[6] = db_data[a]; }
                if (a % cnt == 7) { grid[7] = db_data[a]; }
                if (a % cnt == 8) { grid[8] = db_data[a]; }
                if (a % cnt == 9) { grid[9] = db_data[a]; }
                if (a % cnt == 10) { grid[10] = db_data[a]; }
                if (a % cnt == 11) { grid[11] = db_data[a]; }
                if (a % cnt == 12)
                {
                    grid[12] = db_data[a];
                    dataGridView1.Rows.Add(grid);
                }
            }
        }
        private void btn_save_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("저장하시겠습니까?", "저장", MessageBoxButtons.YesNo) == DialogResult.No) {
                return;
            }
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                string[] search_col = { "idx", "compidx" };
                string[] search_data = { dataGridView1.Rows[a].Cells["idx"].Value.ToString(), compidx };
                string[] edit_col = { "out_per", "out_price" };
                string[] edit_data = { dataGridView1.Rows[a].Cells["out_per"].Value.ToString(), 
                                       dataGridView1.Rows[a].Cells["out_price"].Value.ToString() };

                string U_cmd = db.More_Update("Sales", edit_col, edit_data, search_col, search_data);
                db.DB_Send_CMD_reVoid(U_cmd);
            }
            MessageBox.Show("저장되었습니다.", "저장", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("삭제하시겠습니까?", "삭제", MessageBoxButtons.YesNo) == DialogResult.No) {
                return;
            }
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                string D_cmd = db.DB_Delete("Sales", "compidx", compidx, "idx", dataGridView1.Rows[a].Cells["idx"].Value.ToString());
                db.DB_Send_CMD_reVoid(D_cmd);
            }
            MessageBox.Show("삭제되었습니다.", "삭제", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }
        private void tb_out_per_KeyPress(object sender, KeyPressEventArgs e)
        {
            String_Text st = new String_Text();
            st.Only_Int(sender, e);
        }

        private void tb_out_per_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { 
                btn_per_change_Click(null, null);
            }
        }
    }
}
// 목록일자 + " " + 납품처
// 
// 2021-04-29 전남도립(희)4-3
// 2021-04-29 주은금호작은m
// 2021-05-11 대동고
// 2021-04-28 화정남초
// 2021-04-22 큰별초
// 2021-04-28 금구초
// 2021-04-27 정원유치원추가
// 2021-04-28 여수신기초m
// 2021-04-22 송원여상
// 2021-05-10 송원고국어
// 2021-05-18 원주대(급)
// 2021-04-27 장성고m
// 2021-05-03 여수진남초m
// 2021-04-16 운남고m
// 2021-05-11 죽곡유치원추가
// 2021-05-06 조선이공대2차k
// 2021-04-27 서구공립6개관
// 
// 
// 목록일자 + "일자 " + 납품처 + "납품목록"
// 
// 2021-04-29일자 전남도립(희)4-3납품목록
// 2021-04-29일자 주은금호작은m납품목록
// 2021-05-11일자 대동고납품목록
// 2021-04-28일자 화정남초납품목록
// 2021-04-22일자 큰별초납품목록
// 2021-04-28일자 금구초납품목록
// 2021-04-27일자 정원유치원추가납품목록
// 2021-04-28일자 여수신기초m납품목록
// 2021-04-22일자 송원여상납품목록
// 2021-05-10일자 송원고국어납품목록
// 2021-05-18일자 원주대(급)납품목록
// 2021-04-27일자 장성고m납품목록
// 2021-05-03일자 여수진남초m납품목록
// 2021-04-16일자 운남고m납품목록
// 2021-05-11일자 죽곡유치원추가납품목록
// 2021-05-06일자 조선이공대2차k납품목록
// 2021-04-27일자 서구공립6개관납품목록