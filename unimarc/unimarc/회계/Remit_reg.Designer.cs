﻿namespace WindowsFormsApp1.Account
{
    partial class Remit_reg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkbox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.list_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.send_money = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.start_Date = new System.Windows.Forms.DateTimePicker();
            this.end_Date = new System.Windows.Forms.DateTimePicker();
            this.cb_remunerate = new System.Windows.Forms.ComboBox();
            this.btn_lookup = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btn_Excel = new System.Windows.Forms.Button();
            this.btn_Print = new System.Windows.Forms.Button();
            this.btn_complete = new System.Windows.Forms.Button();
            this.tb_list_date = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_date_pay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_purchase = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_bank_comp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_bank_code = new System.Windows.Forms.TextBox();
            this.tb_bank_num = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_bank_name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_send_money = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_charge = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_etc = new System.Windows.Forms.RichTextBox();
            this.btn_close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.chkbox,
            this.list_date,
            this.date_pay,
            this.purchase,
            this.bank_comp,
            this.bank_code,
            this.bank_num,
            this.bank_name,
            this.send_money,
            this.charge,
            this.etc});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(12, 38);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 21;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1023, 332);
            this.dataGridView1.TabIndex = 29;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView1_CellPainting);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.Visible = false;
            this.idx.Width = 50;
            // 
            // chkbox
            // 
            this.chkbox.HeaderText = "";
            this.chkbox.Name = "chkbox";
            this.chkbox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chkbox.Width = 40;
            // 
            // list_date
            // 
            this.list_date.HeaderText = "등록일자";
            this.list_date.Name = "list_date";
            this.list_date.Width = 85;
            // 
            // date_pay
            // 
            this.date_pay.HeaderText = "결제일자";
            this.date_pay.Name = "date_pay";
            this.date_pay.Width = 85;
            // 
            // purchase
            // 
            this.purchase.HeaderText = "거래처";
            this.purchase.Name = "purchase";
            // 
            // bank_comp
            // 
            this.bank_comp.HeaderText = "은행명";
            this.bank_comp.Name = "bank_comp";
            // 
            // bank_code
            // 
            this.bank_code.HeaderText = "코드";
            this.bank_code.Name = "bank_code";
            this.bank_code.Width = 40;
            // 
            // bank_num
            // 
            this.bank_num.HeaderText = "계좌번호";
            this.bank_num.Name = "bank_num";
            this.bank_num.Width = 140;
            // 
            // bank_name
            // 
            this.bank_name.HeaderText = "예금주";
            this.bank_name.Name = "bank_name";
            this.bank_name.Width = 70;
            // 
            // send_money
            // 
            this.send_money.HeaderText = "송금금액";
            this.send_money.Name = "send_money";
            // 
            // charge
            // 
            this.charge.HeaderText = "등록자";
            this.charge.Name = "charge";
            this.charge.Width = 70;
            // 
            // etc
            // 
            this.etc.HeaderText = "비고";
            this.etc.Name = "etc";
            this.etc.Width = 150;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "등록일자";
            // 
            // start_Date
            // 
            this.start_Date.CustomFormat = "yyyy-MM-dd";
            this.start_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.start_Date.Location = new System.Drawing.Point(77, 10);
            this.start_Date.Name = "start_Date";
            this.start_Date.Size = new System.Drawing.Size(85, 21);
            this.start_Date.TabIndex = 11;
            // 
            // end_Date
            // 
            this.end_Date.CustomFormat = "yyyy-MM-dd";
            this.end_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_Date.Location = new System.Drawing.Point(168, 10);
            this.end_Date.Name = "end_Date";
            this.end_Date.Size = new System.Drawing.Size(85, 21);
            this.end_Date.TabIndex = 11;
            // 
            // cb_remunerate
            // 
            this.cb_remunerate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_remunerate.FormattingEnabled = true;
            this.cb_remunerate.Location = new System.Drawing.Point(259, 10);
            this.cb_remunerate.Name = "cb_remunerate";
            this.cb_remunerate.Size = new System.Drawing.Size(71, 20);
            this.cb_remunerate.TabIndex = 31;
            // 
            // btn_lookup
            // 
            this.btn_lookup.Location = new System.Drawing.Point(336, 9);
            this.btn_lookup.Name = "btn_lookup";
            this.btn_lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_lookup.TabIndex = 17;
            this.btn_lookup.Text = "조   회";
            this.btn_lookup.UseVisualStyleBackColor = true;
            this.btn_lookup.Click += new System.EventHandler(this.btn_lookup_Click);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(417, 9);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(77, 23);
            this.button5.TabIndex = 18;
            this.button5.Text = "은행용 엑셀";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btn_Excel
            // 
            this.btn_Excel.Location = new System.Drawing.Point(498, 9);
            this.btn_Excel.Name = "btn_Excel";
            this.btn_Excel.Size = new System.Drawing.Size(75, 23);
            this.btn_Excel.TabIndex = 18;
            this.btn_Excel.Text = "엑   셀";
            this.btn_Excel.UseVisualStyleBackColor = true;
            this.btn_Excel.Click += new System.EventHandler(this.btn_Excel_Click);
            // 
            // btn_Print
            // 
            this.btn_Print.Location = new System.Drawing.Point(579, 9);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(75, 23);
            this.btn_Print.TabIndex = 18;
            this.btn_Print.Text = "인   쇄";
            this.btn_Print.UseVisualStyleBackColor = true;
            // 
            // btn_complete
            // 
            this.btn_complete.Location = new System.Drawing.Point(660, 9);
            this.btn_complete.Name = "btn_complete";
            this.btn_complete.Size = new System.Drawing.Size(75, 23);
            this.btn_complete.TabIndex = 18;
            this.btn_complete.Text = "송금 완료";
            this.btn_complete.UseVisualStyleBackColor = true;
            this.btn_complete.Click += new System.EventHandler(this.btn_complete_Click);
            // 
            // tb_list_date
            // 
            this.tb_list_date.Location = new System.Drawing.Point(1052, 35);
            this.tb_list_date.Name = "tb_list_date";
            this.tb_list_date.Size = new System.Drawing.Size(128, 21);
            this.tb_list_date.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(1052, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 33;
            this.label1.Text = "등록일자";
            // 
            // tb_date_pay
            // 
            this.tb_date_pay.Location = new System.Drawing.Point(1192, 35);
            this.tb_date_pay.Name = "tb_date_pay";
            this.tb_date_pay.Size = new System.Drawing.Size(128, 21);
            this.tb_date_pay.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(1192, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 33;
            this.label2.Text = "결제일자";
            // 
            // tb_purchase
            // 
            this.tb_purchase.Location = new System.Drawing.Point(1052, 82);
            this.tb_purchase.Name = "tb_purchase";
            this.tb_purchase.Size = new System.Drawing.Size(128, 21);
            this.tb_purchase.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(1052, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 12);
            this.label3.TabIndex = 33;
            this.label3.Text = "거래처";
            // 
            // tb_bank_comp
            // 
            this.tb_bank_comp.Location = new System.Drawing.Point(1192, 82);
            this.tb_bank_comp.Name = "tb_bank_comp";
            this.tb_bank_comp.Size = new System.Drawing.Size(80, 21);
            this.tb_bank_comp.TabIndex = 32;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(1192, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 12);
            this.label4.TabIndex = 33;
            this.label4.Text = "은행명 / 코드";
            // 
            // tb_bank_code
            // 
            this.tb_bank_code.Location = new System.Drawing.Point(1278, 82);
            this.tb_bank_code.Name = "tb_bank_code";
            this.tb_bank_code.Size = new System.Drawing.Size(42, 21);
            this.tb_bank_code.TabIndex = 32;
            // 
            // tb_bank_num
            // 
            this.tb_bank_num.Location = new System.Drawing.Point(1052, 129);
            this.tb_bank_num.Name = "tb_bank_num";
            this.tb_bank_num.Size = new System.Drawing.Size(128, 21);
            this.tb_bank_num.TabIndex = 32;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(1052, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 12);
            this.label5.TabIndex = 33;
            this.label5.Text = "계좌번호";
            // 
            // tb_bank_name
            // 
            this.tb_bank_name.Location = new System.Drawing.Point(1192, 129);
            this.tb_bank_name.Name = "tb_bank_name";
            this.tb_bank_name.Size = new System.Drawing.Size(128, 21);
            this.tb_bank_name.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(1192, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 12);
            this.label6.TabIndex = 33;
            this.label6.Text = "예금주";
            // 
            // tb_send_money
            // 
            this.tb_send_money.Location = new System.Drawing.Point(1052, 177);
            this.tb_send_money.Name = "tb_send_money";
            this.tb_send_money.Size = new System.Drawing.Size(128, 21);
            this.tb_send_money.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(1052, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 12);
            this.label7.TabIndex = 33;
            this.label7.Text = "송금금액";
            // 
            // tb_charge
            // 
            this.tb_charge.Location = new System.Drawing.Point(1192, 177);
            this.tb_charge.Name = "tb_charge";
            this.tb_charge.Size = new System.Drawing.Size(128, 21);
            this.tb_charge.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(1192, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 12);
            this.label8.TabIndex = 33;
            this.label8.Text = "등록자";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(1052, 209);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 12);
            this.label10.TabIndex = 33;
            this.label10.Text = "비고";
            // 
            // tb_etc
            // 
            this.tb_etc.Location = new System.Drawing.Point(1052, 225);
            this.tb_etc.Name = "tb_etc";
            this.tb_etc.Size = new System.Drawing.Size(268, 145);
            this.tb_etc.TabIndex = 34;
            this.tb_etc.Text = "";
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(741, 9);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 55;
            this.btn_close.Text = "닫   기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // Remit_reg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1342, 430);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.tb_etc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_bank_code);
            this.Controls.Add(this.tb_charge);
            this.Controls.Add(this.tb_send_money);
            this.Controls.Add(this.tb_bank_name);
            this.Controls.Add(this.tb_bank_num);
            this.Controls.Add(this.tb_bank_comp);
            this.Controls.Add(this.tb_purchase);
            this.Controls.Add(this.tb_date_pay);
            this.Controls.Add(this.tb_list_date);
            this.Controls.Add(this.cb_remunerate);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_complete);
            this.Controls.Add(this.btn_Print);
            this.Controls.Add(this.btn_Excel);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btn_lookup);
            this.Controls.Add(this.end_Date);
            this.Controls.Add(this.start_Date);
            this.Controls.Add(this.label9);
            this.Name = "Remit_reg";
            this.Text = "송금내역조회";
            this.Load += new System.EventHandler(this.Remit_reg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker start_Date;
        private System.Windows.Forms.DateTimePicker end_Date;
        private System.Windows.Forms.ComboBox cb_remunerate;
        private System.Windows.Forms.Button btn_lookup;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btn_Excel;
        private System.Windows.Forms.Button btn_Print;
        private System.Windows.Forms.Button btn_complete;
        private System.Windows.Forms.TextBox tb_list_date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_date_pay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_purchase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_bank_comp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_bank_code;
        private System.Windows.Forms.TextBox tb_bank_num;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_bank_name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_send_money;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_charge;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox tb_etc;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchase;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn send_money;
        private System.Windows.Forms.DataGridViewTextBoxColumn charge;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
        private System.Windows.Forms.Button btn_close;
    }
}