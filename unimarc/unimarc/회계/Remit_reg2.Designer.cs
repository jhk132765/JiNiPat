﻿namespace WindowsFormsApp1.Account
{
    partial class Remit_reg2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cb_remunerate = new System.Windows.Forms.ComboBox();
            this.btn_delete = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.date_res = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bank_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.send_money = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tb_bank_code = new System.Windows.Forms.TextBox();
            this.tb_bank = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_excel = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_lookup = new System.Windows.Forms.Button();
            this.btn_Add = new System.Windows.Forms.Button();
            this.tb_send_money = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_etc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_bank_boss = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_bank_num = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_now_money = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_purchase = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.end_Date = new System.Windows.Forms.DateTimePicker();
            this.start_Date = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.list_Date = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cb_remunerate
            // 
            this.cb_remunerate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_remunerate.FormattingEnabled = true;
            this.cb_remunerate.Location = new System.Drawing.Point(515, 224);
            this.cb_remunerate.Name = "cb_remunerate";
            this.cb_remunerate.Size = new System.Drawing.Size(71, 20);
            this.cb_remunerate.TabIndex = 14;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(185, 223);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.TabIndex = 11;
            this.btn_delete.Text = "삭   제";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.date_res,
            this.purchase,
            this.bank_comp,
            this.bank_code,
            this.bank_num,
            this.bank_name,
            this.send_money,
            this.date_pay,
            this.etc});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(23, 252);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 21;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(860, 150);
            this.dataGridView1.TabIndex = 17;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // date_res
            // 
            this.date_res.HeaderText = "등록일자";
            this.date_res.Name = "date_res";
            this.date_res.Width = 85;
            // 
            // purchase
            // 
            this.purchase.HeaderText = "거래처";
            this.purchase.Name = "purchase";
            // 
            // bank_comp
            // 
            this.bank_comp.HeaderText = "은행명";
            this.bank_comp.Name = "bank_comp";
            // 
            // bank_code
            // 
            this.bank_code.HeaderText = "은행코드";
            this.bank_code.Name = "bank_code";
            this.bank_code.Visible = false;
            // 
            // bank_num
            // 
            this.bank_num.HeaderText = "계좌번호";
            this.bank_num.Name = "bank_num";
            this.bank_num.Width = 130;
            // 
            // bank_name
            // 
            this.bank_name.HeaderText = "예금주";
            this.bank_name.Name = "bank_name";
            // 
            // send_money
            // 
            this.send_money.HeaderText = "송금금액";
            this.send_money.Name = "send_money";
            // 
            // date_pay
            // 
            this.date_pay.HeaderText = "결제일자";
            this.date_pay.Name = "date_pay";
            this.date_pay.Width = 85;
            // 
            // etc
            // 
            this.etc.HeaderText = "비고";
            this.etc.Name = "etc";
            this.etc.Width = 120;
            // 
            // tb_bank_code
            // 
            this.tb_bank_code.Location = new System.Drawing.Point(254, 77);
            this.tb_bank_code.Name = "tb_bank_code";
            this.tb_bank_code.Size = new System.Drawing.Size(108, 21);
            this.tb_bank_code.TabIndex = 4;
            // 
            // tb_bank
            // 
            this.tb_bank.Location = new System.Drawing.Point(85, 77);
            this.tb_bank.Name = "tb_bank";
            this.tb_bank.Size = new System.Drawing.Size(163, 21);
            this.tb_bank.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 53;
            this.label4.Text = "은행명";
            // 
            // btn_excel
            // 
            this.btn_excel.Location = new System.Drawing.Point(673, 223);
            this.btn_excel.Name = "btn_excel";
            this.btn_excel.Size = new System.Drawing.Size(75, 23);
            this.btn_excel.TabIndex = 16;
            this.btn_excel.Text = "엑   셀";
            this.btn_excel.UseVisualStyleBackColor = true;
            this.btn_excel.Click += new System.EventHandler(this.btn_excel_Click);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(104, 223);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 9;
            this.btn_save.Text = "저   장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_lookup
            // 
            this.btn_lookup.Location = new System.Drawing.Point(592, 223);
            this.btn_lookup.Name = "btn_lookup";
            this.btn_lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_lookup.TabIndex = 15;
            this.btn_lookup.Text = "조   회";
            this.btn_lookup.UseVisualStyleBackColor = true;
            this.btn_lookup.Click += new System.EventHandler(this.btn_lookup_Click);
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(23, 223);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(75, 23);
            this.btn_Add.TabIndex = 10;
            this.btn_Add.Text = "추   가";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // tb_send_money
            // 
            this.tb_send_money.Location = new System.Drawing.Point(85, 133);
            this.tb_send_money.Name = "tb_send_money";
            this.tb_send_money.Size = new System.Drawing.Size(163, 21);
            this.tb_send_money.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 41;
            this.label8.Text = "송금금액";
            // 
            // tb_etc
            // 
            this.tb_etc.Location = new System.Drawing.Point(85, 161);
            this.tb_etc.Multiline = true;
            this.tb_etc.Name = "tb_etc";
            this.tb_etc.Size = new System.Drawing.Size(663, 56);
            this.tb_etc.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 12);
            this.label7.TabIndex = 42;
            this.label7.Text = "비    고";
            // 
            // tb_bank_boss
            // 
            this.tb_bank_boss.Location = new System.Drawing.Point(436, 105);
            this.tb_bank_boss.Name = "tb_bank_boss";
            this.tb_bank_boss.Size = new System.Drawing.Size(280, 21);
            this.tb_bank_boss.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(389, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 40;
            this.label5.Text = "예금주";
            // 
            // tb_bank_num
            // 
            this.tb_bank_num.Location = new System.Drawing.Point(85, 105);
            this.tb_bank_num.Name = "tb_bank_num";
            this.tb_bank_num.Size = new System.Drawing.Size(280, 21);
            this.tb_bank_num.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 39;
            this.label6.Text = "계좌번호";
            // 
            // tb_now_money
            // 
            this.tb_now_money.BackColor = System.Drawing.SystemColors.Info;
            this.tb_now_money.Location = new System.Drawing.Point(436, 49);
            this.tb_now_money.Name = "tb_now_money";
            this.tb_now_money.Size = new System.Drawing.Size(150, 21);
            this.tb_now_money.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(385, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 12);
            this.label2.TabIndex = 38;
            this.label2.Text = "현 잔액";
            // 
            // tb_purchase
            // 
            this.tb_purchase.Location = new System.Drawing.Point(85, 49);
            this.tb_purchase.Name = "tb_purchase";
            this.tb_purchase.Size = new System.Drawing.Size(277, 21);
            this.tb_purchase.TabIndex = 1;
            this.tb_purchase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_purchase_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 37;
            this.label3.Text = "매입거래처";
            // 
            // end_Date
            // 
            this.end_Date.CustomFormat = "yyyy-MM-dd";
            this.end_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_Date.Location = new System.Drawing.Point(424, 224);
            this.end_Date.Name = "end_Date";
            this.end_Date.Size = new System.Drawing.Size(85, 21);
            this.end_Date.TabIndex = 13;
            // 
            // start_Date
            // 
            this.start_Date.CustomFormat = "yyyy-MM-dd";
            this.start_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.start_Date.Location = new System.Drawing.Point(333, 224);
            this.start_Date.Name = "start_Date";
            this.start_Date.Size = new System.Drawing.Size(85, 21);
            this.start_Date.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(272, 228);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 33;
            this.label9.Text = "등록일자";
            // 
            // list_Date
            // 
            this.list_Date.CustomFormat = "yyyy-MM-dd";
            this.list_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.list_Date.Location = new System.Drawing.Point(85, 21);
            this.list_Date.Name = "list_Date";
            this.list_Date.Size = new System.Drawing.Size(85, 21);
            this.list_Date.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 32;
            this.label1.Text = "등록일자";
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(755, 223);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 54;
            this.btn_close.Text = "닫   기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // Remit_reg2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 423);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.cb_remunerate);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tb_bank_code);
            this.Controls.Add(this.tb_bank);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_excel);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_lookup);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.tb_send_money);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tb_etc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tb_bank_boss);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_bank_num);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tb_now_money);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_purchase);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.end_Date);
            this.Controls.Add(this.start_Date);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.list_Date);
            this.Controls.Add(this.label1);
            this.Name = "Remit_reg2";
            this.Text = "송금등록";
            this.Load += new System.EventHandler(this.Remit_reg2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_remunerate;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox tb_bank_code;
        private System.Windows.Forms.TextBox tb_bank;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_excel;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_lookup;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.TextBox tb_send_money;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_etc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_bank_boss;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_bank_num;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_now_money;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker end_Date;
        private System.Windows.Forms.DateTimePicker start_Date;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker list_Date;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox tb_purchase;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_res;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchase;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn bank_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn send_money;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
        private System.Windows.Forms.Button btn_close;
    }
}