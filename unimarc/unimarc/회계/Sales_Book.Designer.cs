﻿
namespace WindowsFormsApp1.회계
{
    partial class Sales_Book
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_clt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Start_Date = new System.Windows.Forms.DateTimePicker();
            this.btn_Close = new System.Windows.Forms.Button();
            this.End_Date = new System.Windows.Forms.DateTimePicker();
            this.btn_Lookup = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.out_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.out_per = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.out_price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.in_price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.now_money = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tb_count = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tb_out = new System.Windows.Forms.TextBox();
            this.tb_in = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tb_clt);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.Start_Date);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.End_Date);
            this.panel1.Controls.Add(this.btn_Lookup);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(778, 40);
            this.panel1.TabIndex = 93;
            // 
            // tb_clt
            // 
            this.tb_clt.Location = new System.Drawing.Point(359, 9);
            this.tb_clt.Name = "tb_clt";
            this.tb_clt.Size = new System.Drawing.Size(208, 21);
            this.tb_clt.TabIndex = 83;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(292, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 82;
            this.label1.Text = "매출거래처";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(161, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 12);
            this.label4.TabIndex = 37;
            this.label4.Text = "~";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 37;
            this.label2.Text = "조회기간";
            // 
            // Start_Date
            // 
            this.Start_Date.CustomFormat = "yyyy-MM-dd";
            this.Start_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Start_Date.Location = new System.Drawing.Point(70, 9);
            this.Start_Date.Name = "Start_Date";
            this.Start_Date.Size = new System.Drawing.Size(85, 21);
            this.Start_Date.TabIndex = 38;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(684, 8);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 81;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // End_Date
            // 
            this.End_Date.CustomFormat = "yyyy-MM-dd";
            this.End_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.End_Date.Location = new System.Drawing.Point(181, 9);
            this.End_Date.Name = "End_Date";
            this.End_Date.Size = new System.Drawing.Size(85, 21);
            this.End_Date.TabIndex = 39;
            // 
            // btn_Lookup
            // 
            this.btn_Lookup.Location = new System.Drawing.Point(593, 8);
            this.btn_Lookup.Name = "btn_Lookup";
            this.btn_Lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_Lookup.TabIndex = 79;
            this.btn_Lookup.Text = "조    회";
            this.btn_Lookup.UseVisualStyleBackColor = true;
            this.btn_Lookup.Click += new System.EventHandler(this.btn_Lookup_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.out_date,
            this.content,
            this.count,
            this.out_per,
            this.out_price,
            this.in_price,
            this.now_money,
            this.etc});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(12, 58);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 20;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(960, 421);
            this.dataGridView1.TabIndex = 94;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // out_date
            // 
            this.out_date.HeaderText = "출고일자";
            this.out_date.Name = "out_date";
            this.out_date.Width = 80;
            // 
            // content
            // 
            this.content.HeaderText = "내용";
            this.content.Name = "content";
            this.content.Width = 300;
            // 
            // count
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.count.DefaultCellStyle = dataGridViewCellStyle8;
            this.count.HeaderText = "수량";
            this.count.Name = "count";
            this.count.Width = 40;
            // 
            // out_per
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.out_per.DefaultCellStyle = dataGridViewCellStyle9;
            this.out_per.HeaderText = "출고율";
            this.out_per.Name = "out_per";
            this.out_per.Width = 60;
            // 
            // out_price
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.out_price.DefaultCellStyle = dataGridViewCellStyle10;
            this.out_price.HeaderText = "매출금액";
            this.out_price.Name = "out_price";
            this.out_price.Width = 80;
            // 
            // in_price
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.in_price.DefaultCellStyle = dataGridViewCellStyle11;
            this.in_price.HeaderText = "입금액";
            this.in_price.Name = "in_price";
            this.in_price.Width = 80;
            // 
            // now_money
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.now_money.DefaultCellStyle = dataGridViewCellStyle12;
            this.now_money.HeaderText = "현잔액";
            this.now_money.Name = "now_money";
            this.now_money.Width = 80;
            // 
            // etc
            // 
            this.etc.HeaderText = "비고";
            this.etc.Name = "etc";
            this.etc.Width = 200;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(32, 488);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(81, 21);
            this.textBox2.TabIndex = 95;
            this.textBox2.Text = "합계";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(112, 488);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(301, 21);
            this.textBox1.TabIndex = 95;
            // 
            // tb_count
            // 
            this.tb_count.Enabled = false;
            this.tb_count.Location = new System.Drawing.Point(412, 488);
            this.tb_count.Name = "tb_count";
            this.tb_count.Size = new System.Drawing.Size(41, 21);
            this.tb_count.TabIndex = 95;
            this.tb_count.Text = "0";
            this.tb_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_count.TextChanged += new System.EventHandler(this.tb_count_TextChanged);
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(452, 488);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(61, 21);
            this.textBox6.TabIndex = 95;
            // 
            // tb_out
            // 
            this.tb_out.Enabled = false;
            this.tb_out.Location = new System.Drawing.Point(512, 488);
            this.tb_out.Name = "tb_out";
            this.tb_out.Size = new System.Drawing.Size(81, 21);
            this.tb_out.TabIndex = 95;
            this.tb_out.Text = "0";
            this.tb_out.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_out.TextChanged += new System.EventHandler(this.tb_count_TextChanged);
            // 
            // tb_in
            // 
            this.tb_in.Enabled = false;
            this.tb_in.Location = new System.Drawing.Point(592, 488);
            this.tb_in.Name = "tb_in";
            this.tb_in.Size = new System.Drawing.Size(81, 21);
            this.tb_in.TabIndex = 95;
            this.tb_in.Text = "0";
            this.tb_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tb_in.TextChanged += new System.EventHandler(this.tb_count_TextChanged);
            // 
            // textBox9
            // 
            this.textBox9.Enabled = false;
            this.textBox9.Location = new System.Drawing.Point(672, 488);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(81, 21);
            this.textBox9.TabIndex = 95;
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(752, 488);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(202, 21);
            this.textBox10.TabIndex = 95;
            // 
            // Sales_Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 517);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.tb_in);
            this.Controls.Add(this.tb_out);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.tb_count);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Name = "Sales_Book";
            this.Text = "매출 장부";
            this.Load += new System.EventHandler(this.Sales_Book_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Close;
        public System.Windows.Forms.Button btn_Lookup;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tb_count;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox tb_out;
        private System.Windows.Forms.TextBox tb_in;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        public System.Windows.Forms.DateTimePicker Start_Date;
        public System.Windows.Forms.DateTimePicker End_Date;
        public System.Windows.Forms.TextBox tb_clt;
        private System.Windows.Forms.DataGridViewTextBoxColumn out_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn content;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn out_per;
        private System.Windows.Forms.DataGridViewTextBoxColumn out_price;
        private System.Windows.Forms.DataGridViewTextBoxColumn in_price;
        private System.Windows.Forms.DataGridViewTextBoxColumn now_money;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
    }
}