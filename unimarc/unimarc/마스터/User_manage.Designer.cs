﻿namespace WindowsFormsApp1
{
    partial class User_manage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_pw = new System.Windows.Forms.TextBox();
            this.tb_id = new System.Windows.Forms.TextBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.tb_boss = new System.Windows.Forms.TextBox();
            this.tb_bank_comp = new System.Windows.Forms.TextBox();
            this.tb_bank_no = new System.Windows.Forms.TextBox();
            this.tb_fax = new System.Windows.Forms.TextBox();
            this.tb_tel = new System.Windows.Forms.TextBox();
            this.tb_barea = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.tb_addr = new System.Windows.Forms.TextBox();
            this.tb_zip = new System.Windows.Forms.TextBox();
            this.tb_jongmok = new System.Windows.Forms.TextBox();
            this.tb_uptae = new System.Windows.Forms.TextBox();
            this.tb_cobin = new System.Windows.Forms.TextBox();
            this.tb_bubin = new System.Windows.Forms.TextBox();
            this.tb_sangho = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.cb_IPList = new System.Windows.Forms.ComboBox();
            this.panel12.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(423, 5);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(59, 25);
            this.btn_close.TabIndex = 12;
            this.btn_close.Text = "닫  기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(358, 5);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(59, 25);
            this.btn_save.TabIndex = 14;
            this.btn_save.Text = "저  장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(10, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(138, 16);
            this.label11.TabIndex = 10;
            this.label11.Text = "신규 사업자 등록";
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.cb_IPList);
            this.panel12.Controls.Add(this.button1);
            this.panel12.Controls.Add(this.tb_pw);
            this.panel12.Controls.Add(this.tb_id);
            this.panel12.Controls.Add(this.panel16);
            this.panel12.Controls.Add(this.panel20);
            this.panel12.Controls.Add(this.tb_boss);
            this.panel12.Controls.Add(this.tb_bank_comp);
            this.panel12.Controls.Add(this.tb_bank_no);
            this.panel12.Controls.Add(this.tb_fax);
            this.panel12.Controls.Add(this.tb_tel);
            this.panel12.Controls.Add(this.tb_barea);
            this.panel12.Controls.Add(this.tb_email);
            this.panel12.Controls.Add(this.tb_addr);
            this.panel12.Controls.Add(this.tb_zip);
            this.panel12.Controls.Add(this.tb_jongmok);
            this.panel12.Controls.Add(this.tb_uptae);
            this.panel12.Controls.Add(this.tb_cobin);
            this.panel12.Controls.Add(this.tb_bubin);
            this.panel12.Controls.Add(this.tb_sangho);
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.panel19);
            this.panel12.Controls.Add(this.panel18);
            this.panel12.Controls.Add(this.panel1);
            this.panel12.Controls.Add(this.panel11);
            this.panel12.Controls.Add(this.panel10);
            this.panel12.Controls.Add(this.panel8);
            this.panel12.Controls.Add(this.panel17);
            this.panel12.Controls.Add(this.panel3);
            this.panel12.Controls.Add(this.panel4);
            this.panel12.Controls.Add(this.panel5);
            this.panel12.Controls.Add(this.panel14);
            this.panel12.Controls.Add(this.panel6);
            this.panel12.Location = new System.Drawing.Point(7, 36);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(475, 244);
            this.panel12.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(178, 190);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(69, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "중복확인";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_pw
            // 
            this.tb_pw.BackColor = System.Drawing.SystemColors.Info;
            this.tb_pw.Location = new System.Drawing.Point(318, 191);
            this.tb_pw.Name = "tb_pw";
            this.tb_pw.PasswordChar = '*';
            this.tb_pw.Size = new System.Drawing.Size(150, 21);
            this.tb_pw.TabIndex = 1;
            // 
            // tb_id
            // 
            this.tb_id.BackColor = System.Drawing.SystemColors.Info;
            this.tb_id.Location = new System.Drawing.Point(64, 191);
            this.tb_id.Name = "tb_id";
            this.tb_id.Size = new System.Drawing.Size(107, 21);
            this.tb_id.TabIndex = 2;
            this.tb_id.TextChanged += new System.EventHandler(this.tb_id_TextChanged);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.label15);
            this.panel16.Location = new System.Drawing.Point(-1, 188);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(59, 28);
            this.panel16.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "만들 ID";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.label19);
            this.panel20.Location = new System.Drawing.Point(252, 188);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(59, 28);
            this.panel20.TabIndex = 4;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "만들 PW";
            // 
            // tb_boss
            // 
            this.tb_boss.Location = new System.Drawing.Point(318, 3);
            this.tb_boss.Name = "tb_boss";
            this.tb_boss.Size = new System.Drawing.Size(150, 21);
            this.tb_boss.TabIndex = 0;
            // 
            // tb_bank_comp
            // 
            this.tb_bank_comp.Location = new System.Drawing.Point(64, 137);
            this.tb_bank_comp.Name = "tb_bank_comp";
            this.tb_bank_comp.Size = new System.Drawing.Size(182, 21);
            this.tb_bank_comp.TabIndex = 0;
            // 
            // tb_bank_no
            // 
            this.tb_bank_no.Location = new System.Drawing.Point(318, 137);
            this.tb_bank_no.Name = "tb_bank_no";
            this.tb_bank_no.Size = new System.Drawing.Size(150, 21);
            this.tb_bank_no.TabIndex = 0;
            // 
            // tb_fax
            // 
            this.tb_fax.Location = new System.Drawing.Point(318, 110);
            this.tb_fax.Name = "tb_fax";
            this.tb_fax.Size = new System.Drawing.Size(150, 21);
            this.tb_fax.TabIndex = 0;
            // 
            // tb_tel
            // 
            this.tb_tel.BackColor = System.Drawing.SystemColors.Info;
            this.tb_tel.Location = new System.Drawing.Point(64, 110);
            this.tb_tel.Name = "tb_tel";
            this.tb_tel.Size = new System.Drawing.Size(182, 21);
            this.tb_tel.TabIndex = 0;
            // 
            // tb_barea
            // 
            this.tb_barea.Location = new System.Drawing.Point(318, 164);
            this.tb_barea.Name = "tb_barea";
            this.tb_barea.Size = new System.Drawing.Size(150, 21);
            this.tb_barea.TabIndex = 0;
            // 
            // tb_email
            // 
            this.tb_email.BackColor = System.Drawing.SystemColors.Info;
            this.tb_email.Location = new System.Drawing.Point(64, 164);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(182, 21);
            this.tb_email.TabIndex = 0;
            // 
            // tb_addr
            // 
            this.tb_addr.Location = new System.Drawing.Point(144, 83);
            this.tb_addr.Name = "tb_addr";
            this.tb_addr.Size = new System.Drawing.Size(324, 21);
            this.tb_addr.TabIndex = 0;
            // 
            // tb_zip
            // 
            this.tb_zip.Location = new System.Drawing.Point(64, 83);
            this.tb_zip.Name = "tb_zip";
            this.tb_zip.Size = new System.Drawing.Size(74, 21);
            this.tb_zip.TabIndex = 0;
            this.tb_zip.Text = "  -";
            // 
            // tb_jongmok
            // 
            this.tb_jongmok.Location = new System.Drawing.Point(318, 57);
            this.tb_jongmok.Name = "tb_jongmok";
            this.tb_jongmok.Size = new System.Drawing.Size(150, 21);
            this.tb_jongmok.TabIndex = 0;
            // 
            // tb_uptae
            // 
            this.tb_uptae.Location = new System.Drawing.Point(64, 57);
            this.tb_uptae.Name = "tb_uptae";
            this.tb_uptae.Size = new System.Drawing.Size(182, 21);
            this.tb_uptae.TabIndex = 0;
            // 
            // tb_cobin
            // 
            this.tb_cobin.Location = new System.Drawing.Point(318, 30);
            this.tb_cobin.Name = "tb_cobin";
            this.tb_cobin.Size = new System.Drawing.Size(150, 21);
            this.tb_cobin.TabIndex = 0;
            // 
            // tb_bubin
            // 
            this.tb_bubin.Location = new System.Drawing.Point(64, 30);
            this.tb_bubin.Name = "tb_bubin";
            this.tb_bubin.Size = new System.Drawing.Size(182, 21);
            this.tb_bubin.TabIndex = 0;
            // 
            // tb_sangho
            // 
            this.tb_sangho.BackColor = System.Drawing.SystemColors.Info;
            this.tb_sangho.Location = new System.Drawing.Point(64, 3);
            this.tb_sangho.Name = "tb_sangho";
            this.tb_sangho.Size = new System.Drawing.Size(182, 21);
            this.tb_sangho.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(-1, -1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(59, 28);
            this.panel2.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "상      호";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.label18);
            this.panel19.Location = new System.Drawing.Point(252, -1);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(59, 28);
            this.panel19.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(2, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "대표자명";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.label17);
            this.panel18.Location = new System.Drawing.Point(252, 26);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(59, 28);
            this.panel18.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(2, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "법인번호";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-1, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(59, 28);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "등록번호";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.label21);
            this.panel11.Location = new System.Drawing.Point(252, 134);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(59, 28);
            this.panel11.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 0;
            this.label21.Text = "계좌번호";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.label10);
            this.panel10.Location = new System.Drawing.Point(252, 107);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(59, 28);
            this.panel10.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "팩스번호";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label8);
            this.panel8.Location = new System.Drawing.Point(-1, 161);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(59, 28);
            this.panel8.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "이 메 일";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.label16);
            this.panel17.Location = new System.Drawing.Point(252, 161);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(59, 28);
            this.panel17.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "발 송 처";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(-1, 80);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(59, 28);
            this.panel3.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "회사주소";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(-1, 53);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(59, 28);
            this.panel4.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "업      태";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(-1, 134);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(59, 28);
            this.panel5.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "은 행 명";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.label13);
            this.panel14.Location = new System.Drawing.Point(252, 53);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(59, 28);
            this.panel14.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(2, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "종      목";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(-1, 107);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(59, 28);
            this.panel6.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "전화번호";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label7);
            this.panel7.Location = new System.Drawing.Point(7, 252);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(59, 28);
            this.panel7.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "허가 IP";
            // 
            // cb_IPList
            // 
            this.cb_IPList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_IPList.FormattingEnabled = true;
            this.cb_IPList.Location = new System.Drawing.Point(64, 218);
            this.cb_IPList.Name = "cb_IPList";
            this.cb_IPList.Size = new System.Drawing.Size(182, 20);
            this.cb_IPList.TabIndex = 6;
            // 
            // User_manage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 299);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel12);
            this.Name = "User_manage";
            this.Text = "신규 사업자 등록";
            this.Load += new System.EventHandler(this.User_manage_Load);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox tb_boss;
        private System.Windows.Forms.TextBox tb_bank_comp;
        private System.Windows.Forms.TextBox tb_bank_no;
        private System.Windows.Forms.TextBox tb_fax;
        private System.Windows.Forms.TextBox tb_tel;
        private System.Windows.Forms.TextBox tb_barea;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.TextBox tb_addr;
        private System.Windows.Forms.TextBox tb_zip;
        private System.Windows.Forms.TextBox tb_jongmok;
        private System.Windows.Forms.TextBox tb_uptae;
        private System.Windows.Forms.TextBox tb_cobin;
        private System.Windows.Forms.TextBox tb_bubin;
        private System.Windows.Forms.TextBox tb_sangho;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_pw;
        private System.Windows.Forms.TextBox tb_id;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cb_IPList;
    }
}