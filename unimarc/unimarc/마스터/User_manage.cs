﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class User_manage : Form
    {
        Helper_DB db = new Helper_DB();
        Main main;
        bool isIDcheck = false;

        public User_manage(Main _main)
        {
            InitializeComponent();
            main = _main;
        }

        private void User_manage_Load(object sender, EventArgs e)
        {
            db.DBcon();
            button1.ForeColor = Color.Red;

            IP ip = new IP();
            string[] IPList = { "ALL", ip.GetIP };
            cb_IPList.Items.AddRange(IPList);
            cb_IPList.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ID = tb_id.Text;

            if (ID == "")
            {
                MessageBox.Show("ID칸이 비어있습니다!");
                return;
            }
            if (!System.Text.RegularExpressions.Regex.IsMatch(ID, @"^[a-zA-Z0-9]"))
            {
                MessageBox.Show("영어와 숫자 조합으로 작성해주세요!");
                isIDcheck = false;
                return;
            }

            string cmd = string.Format("SELECT * FROM `User_Data` WHERE `ID` = \"{0}\";", ID);
            string res = db.DB_Send_CMD_Search(cmd);
            if (res == "") {
                MessageBox.Show("사용가능한 이이디입니다. [" + ID + "]");
                isIDcheck = true;
                button1.ForeColor = Color.Blue;
            }
            else {
                MessageBox.Show("중복된 아이디입니다. [" + ID + "]");
                isIDcheck = false;
            }
        }

        private void tb_id_TextChanged(object sender, EventArgs e)
        {
            isIDcheck = false;
            button1.ForeColor = Color.Red;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            string[] NeedText = {
                tb_sangho.Text, tb_tel.Text, tb_email.Text, tb_id.Text, tb_pw.Text
            };

            foreach (string CheckText in NeedText)
            {
                if (CheckText == "") {
                    MessageBox.Show("노란 박스는 꼭 채워주세요!");
                    return;
                }
            }

            string cmd_sanghoCheck = string.Format("SELECT * FROM `Comp` WHERE `comp_name` = \"{0}\"", tb_sangho.Text);
            string res = db.DB_Send_CMD_Search(cmd_sanghoCheck);
            if (res != "") {
                MessageBox.Show("상호명이 중복되었습니다.\n다시 확인해주세요.");
                return;
            }
            if (!isIDcheck)
            {
                MessageBox.Show("아이디 중복확인 먼저 진행해주세요.");
                return;
            }

            string[] InsertCol = {
                "comp_name", "boss", "bubin", "cobin", "uptae",
                "jongmok", "zip", "addr", "tel", "fax",
                "bank_comp", "bank_no", "email", "barea", "grade"
            };  // 15

            string[] InsertData = {
                tb_sangho.Text, tb_boss.Text, tb_bubin.Text, tb_cobin.Text, tb_uptae.Text,
                tb_jongmok.Text, tb_zip.Text, tb_addr.Text, tb_tel.Text, tb_fax.Text,
                tb_bank_comp.Text, tb_bank_no.Text, tb_email.Text, tb_barea.Text, "외부업체"
            };  // 15

            db.DB_Send_CMD_reVoid(db.DB_INSERT("Comp", InsertCol, InsertData));

            // IP 적용
            string[] IP_Col = { "compidx", "IP" };
            string[] IP_Data = { db.chk_comp(tb_sangho.Text), cb_IPList.Text };
            
            db.DB_Send_CMD_reVoid(db.DB_INSERT("Comp_IP", IP_Col, IP_Data));

            InsertAccount(tb_sangho.Text);
        }

        private void InsertAccount(string compName)
        {
            string ID = tb_id.Text;
            string PW = tb_pw.Text;
            string boss = tb_boss.Text;

            string[] InsertData = { ID, PW, boss, compName };
            string[] InsertCol = { "ID", "PW", "name", "affil" };

            string[] InsertUserSubData = { ID };
            string[] InsertUserSubCol = { "id" };

            db.DB_Send_CMD_reVoid(db.DB_INSERT("User_Data", InsertCol, InsertData));

            // User_ShortCut, User_Access 추가.
            db.DB_Send_CMD_reVoid(db.DB_INSERT("User_ShortCut", InsertUserSubCol, InsertUserSubData));
            db.DB_Send_CMD_reVoid(db.DB_INSERT("User_Access", InsertUserSubCol, InsertUserSubData));
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
