﻿namespace WindowsFormsApp1.Mac
{
    partial class Set_Macro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cb_EncodingType = new System.Windows.Forms.ComboBox();
            this.btn_ApplyMacro = new System.Windows.Forms.Button();
            this.cb_SearchList = new System.Windows.Forms.ComboBox();
            this.btn_ListRemove = new System.Windows.Forms.Button();
            this.btn_ListSave = new System.Windows.Forms.Button();
            this.btn_AddList = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.MacroGrid = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TagNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Macro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MacroGrid)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Controls.Add(this.cb_EncodingType);
            this.panel1.Controls.Add(this.btn_ApplyMacro);
            this.panel1.Controls.Add(this.cb_SearchList);
            this.panel1.Controls.Add(this.btn_ListRemove);
            this.panel1.Controls.Add(this.btn_ListSave);
            this.panel1.Controls.Add(this.btn_AddList);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(900, 35);
            this.panel1.TabIndex = 1;
            // 
            // cb_EncodingType
            // 
            this.cb_EncodingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_EncodingType.FormattingEnabled = true;
            this.cb_EncodingType.Items.AddRange(new object[] {
            "ANSI",
            "UTF-8",
            "UniCode"});
            this.cb_EncodingType.Location = new System.Drawing.Point(581, 7);
            this.cb_EncodingType.Name = "cb_EncodingType";
            this.cb_EncodingType.Size = new System.Drawing.Size(81, 20);
            this.cb_EncodingType.TabIndex = 10;
            // 
            // btn_ApplyMacro
            // 
            this.btn_ApplyMacro.Location = new System.Drawing.Point(668, 6);
            this.btn_ApplyMacro.Name = "btn_ApplyMacro";
            this.btn_ApplyMacro.Size = new System.Drawing.Size(78, 23);
            this.btn_ApplyMacro.TabIndex = 5;
            this.btn_ApplyMacro.Text = "매크로 적용";
            this.btn_ApplyMacro.UseVisualStyleBackColor = true;
            this.btn_ApplyMacro.Click += new System.EventHandler(this.btn_ApplyMacro_Click);
            // 
            // cb_SearchList
            // 
            this.cb_SearchList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_SearchList.FormattingEnabled = true;
            this.cb_SearchList.Location = new System.Drawing.Point(72, 7);
            this.cb_SearchList.Name = "cb_SearchList";
            this.cb_SearchList.Size = new System.Drawing.Size(238, 20);
            this.cb_SearchList.TabIndex = 4;
            this.cb_SearchList.SelectedIndexChanged += new System.EventHandler(this.cb_SearchList_SelectedIndexChanged);
            // 
            // btn_ListRemove
            // 
            this.btn_ListRemove.Location = new System.Drawing.Point(466, 6);
            this.btn_ListRemove.Name = "btn_ListRemove";
            this.btn_ListRemove.Size = new System.Drawing.Size(75, 23);
            this.btn_ListRemove.TabIndex = 3;
            this.btn_ListRemove.Text = "목록 제거";
            this.btn_ListRemove.UseVisualStyleBackColor = true;
            this.btn_ListRemove.Click += new System.EventHandler(this.btn_ListRemove_Click);
            // 
            // btn_ListSave
            // 
            this.btn_ListSave.Location = new System.Drawing.Point(391, 6);
            this.btn_ListSave.Name = "btn_ListSave";
            this.btn_ListSave.Size = new System.Drawing.Size(75, 23);
            this.btn_ListSave.TabIndex = 3;
            this.btn_ListSave.Text = "목록 저장";
            this.btn_ListSave.UseVisualStyleBackColor = true;
            this.btn_ListSave.Click += new System.EventHandler(this.btn_ListSave_Click);
            // 
            // btn_AddList
            // 
            this.btn_AddList.Location = new System.Drawing.Point(316, 6);
            this.btn_AddList.Name = "btn_AddList";
            this.btn_AddList.Size = new System.Drawing.Size(75, 23);
            this.btn_AddList.TabIndex = 3;
            this.btn_AddList.Text = "목록 생성";
            this.btn_AddList.UseVisualStyleBackColor = true;
            this.btn_AddList.Click += new System.EventHandler(this.btn_AddList_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(813, 6);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 2;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "목록 검색";
            // 
            // MacroGrid
            // 
            this.MacroGrid.AllowUserToAddRows = false;
            this.MacroGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MacroGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.MacroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MacroGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.TagNum,
            this.Macro,
            this.Check});
            this.MacroGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MacroGrid.Location = new System.Drawing.Point(0, 0);
            this.MacroGrid.Name = "MacroGrid";
            this.MacroGrid.ReadOnly = true;
            this.MacroGrid.RowTemplate.Height = 23;
            this.MacroGrid.Size = new System.Drawing.Size(900, 572);
            this.MacroGrid.TabIndex = 0;
            this.MacroGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MacroGrid_CellClick);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.ReadOnly = true;
            this.idx.Visible = false;
            this.idx.Width = 60;
            // 
            // TagNum
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TagNum.DefaultCellStyle = dataGridViewCellStyle2;
            this.TagNum.HeaderText = "태그번호";
            this.TagNum.Name = "TagNum";
            this.TagNum.ReadOnly = true;
            // 
            // Macro
            // 
            this.Macro.HeaderText = "매크로";
            this.Macro.Name = "Macro";
            this.Macro.ReadOnly = true;
            this.Macro.Width = 700;
            // 
            // Check
            // 
            this.Check.FalseValue = "F";
            this.Check.HeaderText = "V";
            this.Check.Name = "Check";
            this.Check.ReadOnly = true;
            this.Check.TrueValue = "T";
            this.Check.Width = 40;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.MacroGrid);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 35);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(900, 572);
            this.panel3.TabIndex = 3;
            // 
            // Set_Macro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 607);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "Set_Macro";
            this.Text = "반출 옵션 선택";
            this.Load += new System.EventHandler(this.Set_Macro_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MacroGrid)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView MacroGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_ListSave;
        private System.Windows.Forms.Button btn_AddList;
        private System.Windows.Forms.Button btn_ListRemove;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Save_Check;
        private System.Windows.Forms.ComboBox cb_SearchList;
        private System.Windows.Forms.Button btn_ApplyMacro;
        private System.Windows.Forms.ComboBox cb_EncodingType;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn TagNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn Macro;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Check;
    }
}