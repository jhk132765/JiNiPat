﻿namespace WindowsFormsApp1.Mac
{
    partial class Check_ISBN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.search_book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.search_author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.search_book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pubDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.persent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sold_out = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.image = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.api_data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_lookup = new System.Windows.Forms.Button();
            this.cb_filter = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_list_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_api = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btn_yes24 = new System.Windows.Forms.Button();
            this.Check_Marc = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_ComparePrice = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.num,
            this.isbn,
            this.book_name,
            this.search_book_name,
            this.author,
            this.search_author,
            this.book_comp,
            this.search_book_comp,
            this.count,
            this.unit,
            this.total,
            this.condition,
            this.price,
            this.etc,
            this.pubDate,
            this.persent,
            this.category,
            this.sold_out,
            this.image,
            this.api_data});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridView1.RowHeadersWidth = 20;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1630, 591);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.Visible = false;
            this.idx.Width = 50;
            // 
            // num
            // 
            this.num.HeaderText = "번호";
            this.num.Name = "num";
            this.num.Width = 50;
            // 
            // isbn
            // 
            this.isbn.HeaderText = "ISBN13";
            this.isbn.Name = "isbn";
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 130;
            // 
            // search_book_name
            // 
            this.search_book_name.HeaderText = "검색 도서명";
            this.search_book_name.Name = "search_book_name";
            // 
            // author
            // 
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            // 
            // search_author
            // 
            this.search_author.HeaderText = "검색 저자";
            this.search_author.Name = "search_author";
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            // 
            // search_book_comp
            // 
            this.search_book_comp.HeaderText = "검색 출판사";
            this.search_book_comp.Name = "search_book_comp";
            // 
            // count
            // 
            this.count.HeaderText = "수량";
            this.count.Name = "count";
            this.count.Visible = false;
            this.count.Width = 50;
            // 
            // unit
            // 
            this.unit.HeaderText = "단가";
            this.unit.Name = "unit";
            this.unit.Width = 70;
            // 
            // total
            // 
            this.total.HeaderText = "합계";
            this.total.Name = "total";
            this.total.Visible = false;
            this.total.Width = 70;
            // 
            // condition
            // 
            this.condition.HeaderText = "상태";
            this.condition.Name = "condition";
            this.condition.Visible = false;
            // 
            // price
            // 
            this.price.HeaderText = "정가";
            this.price.Name = "price";
            this.price.Width = 50;
            // 
            // etc
            // 
            this.etc.HeaderText = "비고";
            this.etc.Name = "etc";
            // 
            // pubDate
            // 
            this.pubDate.HeaderText = "발행일";
            this.pubDate.Name = "pubDate";
            // 
            // persent
            // 
            this.persent.HeaderText = "%";
            this.persent.Name = "persent";
            this.persent.Width = 50;
            // 
            // category
            // 
            this.category.HeaderText = "도서분류";
            this.category.Name = "category";
            // 
            // sold_out
            // 
            this.sold_out.HeaderText = "품절/절판";
            this.sold_out.Name = "sold_out";
            // 
            // image
            // 
            this.image.HeaderText = "이미지";
            this.image.Name = "image";
            this.image.Visible = false;
            // 
            // api_data
            // 
            this.api_data.HeaderText = "api_data";
            this.api_data.Name = "api_data";
            this.api_data.Visible = false;
            // 
            // btn_lookup
            // 
            this.btn_lookup.Location = new System.Drawing.Point(693, 4);
            this.btn_lookup.Name = "btn_lookup";
            this.btn_lookup.Size = new System.Drawing.Size(99, 23);
            this.btn_lookup.TabIndex = 1;
            this.btn_lookup.Text = "ISBN 자동 조회";
            this.btn_lookup.UseVisualStyleBackColor = true;
            this.btn_lookup.Click += new System.EventHandler(this.btn_lookup_Click);
            // 
            // cb_filter
            // 
            this.cb_filter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_filter.FormattingEnabled = true;
            this.cb_filter.Location = new System.Drawing.Point(577, 5);
            this.cb_filter.Name = "cb_filter";
            this.cb_filter.Size = new System.Drawing.Size(100, 20);
            this.cb_filter.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "목록 명";
            // 
            // tb_list_name
            // 
            this.tb_list_name.Enabled = false;
            this.tb_list_name.Location = new System.Drawing.Point(54, 5);
            this.tb_list_name.Name = "tb_list_name";
            this.tb_list_name.Size = new System.Drawing.Size(213, 21);
            this.tb_list_name.TabIndex = 4;
            this.tb_list_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_list_name_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(518, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "검색 조건";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(796, 4);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(99, 23);
            this.btn_Save.TabIndex = 1;
            this.btn_Save.Text = "전 체  저 장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(1105, 4);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(99, 23);
            this.btn_Close.TabIndex = 1;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(285, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "검색 엔진";
            // 
            // cb_api
            // 
            this.cb_api.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_api.FormattingEnabled = true;
            this.cb_api.Location = new System.Drawing.Point(344, 5);
            this.cb_api.Name = "cb_api";
            this.cb_api.Size = new System.Drawing.Size(160, 20);
            this.cb_api.TabIndex = 6;
            this.cb_api.SelectedIndexChanged += new System.EventHandler(this.cb_api_SelectedIndexChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(1241, 4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(219, 23);
            this.progressBar1.TabIndex = 7;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1632, 100);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            // 
            // btn_yes24
            // 
            this.btn_yes24.Location = new System.Drawing.Point(1002, 4);
            this.btn_yes24.Name = "btn_yes24";
            this.btn_yes24.Size = new System.Drawing.Size(99, 23);
            this.btn_yes24.TabIndex = 1;
            this.btn_yes24.Text = "예스 양식 반출";
            this.btn_yes24.UseVisualStyleBackColor = true;
            this.btn_yes24.Click += new System.EventHandler(this.btn_yes24_Click);
            // 
            // Check_Marc
            // 
            this.Check_Marc.AutoSize = true;
            this.Check_Marc.Checked = true;
            this.Check_Marc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Check_Marc.Enabled = false;
            this.Check_Marc.Location = new System.Drawing.Point(1488, 7);
            this.Check_Marc.Name = "Check_Marc";
            this.Check_Marc.Size = new System.Drawing.Size(132, 16);
            this.Check_Marc.TabIndex = 9;
            this.Check_Marc.Text = "마크ISBN 동시 저장";
            this.Check_Marc.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_ComparePrice);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Check_Marc);
            this.panel1.Controls.Add(this.btn_lookup);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.cb_api);
            this.panel1.Controls.Add(this.btn_yes24);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cb_filter);
            this.panel1.Controls.Add(this.tb_list_name);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1632, 31);
            this.panel1.TabIndex = 10;
            // 
            // btn_ComparePrice
            // 
            this.btn_ComparePrice.Location = new System.Drawing.Point(899, 4);
            this.btn_ComparePrice.Name = "btn_ComparePrice";
            this.btn_ComparePrice.Size = new System.Drawing.Size(99, 23);
            this.btn_ComparePrice.TabIndex = 10;
            this.btn_ComparePrice.Text = "정 가  대 조";
            this.btn_ComparePrice.UseVisualStyleBackColor = true;
            this.btn_ComparePrice.Click += new System.EventHandler(this.btn_ComparePrice_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 624);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1632, 100);
            this.panel2.TabIndex = 11;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 31);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1632, 593);
            this.panel3.TabIndex = 12;
            // 
            // Check_ISBN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1632, 724);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Check_ISBN";
            this.Text = "ISBN 조회";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Check_ISBN_FormClosing);
            this.Load += new System.EventHandler(this.Check_ISBN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_lookup;
        private System.Windows.Forms.ComboBox cb_filter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_api;
        private System.Windows.Forms.ProgressBar progressBar1;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btn_yes24;
        public System.Windows.Forms.TextBox tb_list_name;
        private System.Windows.Forms.CheckBox Check_Marc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn search_book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn search_author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn search_book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn condition;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
        private System.Windows.Forms.DataGridViewTextBoxColumn pubDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn persent;
        private System.Windows.Forms.DataGridViewTextBoxColumn category;
        private System.Windows.Forms.DataGridViewTextBoxColumn sold_out;
        private System.Windows.Forms.DataGridViewTextBoxColumn image;
        private System.Windows.Forms.DataGridViewTextBoxColumn api_data;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_ComparePrice;
    }
}