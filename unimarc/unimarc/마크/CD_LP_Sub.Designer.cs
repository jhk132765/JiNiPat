﻿
namespace UniMarc.마크
{
    partial class CD_LP_Sub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Btn_Close = new System.Windows.Forms.Button();
            this.Btn_Marc = new System.Windows.Forms.Button();
            this.Btn_Refresh = new System.Windows.Forms.Button();
            this.Btn_Aladin = new System.Windows.Forms.Button();
            this.Btn_KyoBo = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.webBrowser1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1258, 696);
            this.panel2.TabIndex = 4;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(1258, 696);
            this.webBrowser1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Btn_Close);
            this.panel1.Controls.Add(this.Btn_Marc);
            this.panel1.Controls.Add(this.Btn_Refresh);
            this.panel1.Controls.Add(this.Btn_Aladin);
            this.panel1.Controls.Add(this.Btn_KyoBo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1258, 34);
            this.panel1.TabIndex = 3;
            // 
            // Btn_Close
            // 
            this.Btn_Close.Location = new System.Drawing.Point(713, 5);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(75, 23);
            this.Btn_Close.TabIndex = 0;
            this.Btn_Close.Text = "닫    기";
            this.Btn_Close.UseVisualStyleBackColor = true;
            this.Btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // Btn_Marc
            // 
            this.Btn_Marc.Location = new System.Drawing.Point(632, 5);
            this.Btn_Marc.Name = "Btn_Marc";
            this.Btn_Marc.Size = new System.Drawing.Size(75, 23);
            this.Btn_Marc.TabIndex = 0;
            this.Btn_Marc.Text = "마    크";
            this.Btn_Marc.UseVisualStyleBackColor = true;
            this.Btn_Marc.Click += new System.EventHandler(this.Btn_Marc_Click);
            // 
            // Btn_Refresh
            // 
            this.Btn_Refresh.Location = new System.Drawing.Point(551, 5);
            this.Btn_Refresh.Name = "Btn_Refresh";
            this.Btn_Refresh.Size = new System.Drawing.Size(75, 23);
            this.Btn_Refresh.TabIndex = 0;
            this.Btn_Refresh.Text = "새로고침";
            this.Btn_Refresh.UseVisualStyleBackColor = true;
            this.Btn_Refresh.Click += new System.EventHandler(this.Btn_Refresh_Click);
            // 
            // Btn_Aladin
            // 
            this.Btn_Aladin.Location = new System.Drawing.Point(86, 5);
            this.Btn_Aladin.Name = "Btn_Aladin";
            this.Btn_Aladin.Size = new System.Drawing.Size(75, 23);
            this.Btn_Aladin.TabIndex = 0;
            this.Btn_Aladin.Text = "알 라 딘";
            this.Btn_Aladin.UseVisualStyleBackColor = true;
            this.Btn_Aladin.Click += new System.EventHandler(this.Btn_Aladin_Click);
            // 
            // Btn_KyoBo
            // 
            this.Btn_KyoBo.Location = new System.Drawing.Point(5, 5);
            this.Btn_KyoBo.Name = "Btn_KyoBo";
            this.Btn_KyoBo.Size = new System.Drawing.Size(75, 23);
            this.Btn_KyoBo.TabIndex = 0;
            this.Btn_KyoBo.Text = "교    보";
            this.Btn_KyoBo.UseVisualStyleBackColor = true;
            this.Btn_KyoBo.Click += new System.EventHandler(this.Btn_KyoBo_Click);
            // 
            // CD_LP_Sub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 730);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "CD_LP_Sub";
            this.Text = "CD_LP_Sub";
            this.Load += new System.EventHandler(this.CD_LP_Sub_Load);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Btn_Close;
        public System.Windows.Forms.Button Btn_Marc;
        private System.Windows.Forms.Button Btn_Refresh;
        public System.Windows.Forms.Button Btn_Aladin;
        public System.Windows.Forms.Button Btn_KyoBo;
    }
}