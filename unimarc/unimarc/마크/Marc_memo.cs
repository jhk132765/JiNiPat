﻿using ExcelTest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniMarc.마크
{
    public partial class Marc_memo : Form
    {
        Marc marc;
        AddMarc am;
        public Marc_memo(Marc _marc)
        {
            InitializeComponent();
            marc = _marc;
        }
        public Marc_memo(AddMarc _am)
        {
            InitializeComponent();
            am = _am;
        }

        private void Marc_memo_Load(object sender, EventArgs e)
        {
            string[] com_List =
            {
                "하나막", "코리스" 
            };
            cb_List.Items.AddRange(com_List);
        }

        private void btn_ReturnMarc_Click(object sender, EventArgs e)
        {
            string text = richTextBox1.Text;
            string result = "";
            int idx = cb_List.SelectedIndex;
            if (idx < 0)
                return;
            try
            {
                switch (idx)
                {
                    case 0:
                        result = Hana(text);
                        break;
                    case 1:
                        result = Kolis(text) + "\n";
                        break;
                    default:
                        break;
                }
            }
            catch { }

            richTextBox1.Text = "";

            if (marc != null)
                marc.richTextBox1.Text = result.Replace("↔", "");
            else if (am != null)
                am.richTextBox1.Text = result.Replace("↔", "");
        }
        #region UniMarc 양식에 맞춰 변경
        string Hana(string text)
        {
            string[] arr = text.Split('\n');

            for (int a = 0; a < arr.Length - 1; a++)
            {
                arr[a] = arr[a].Insert(5, "\t");
                arr[a] = arr[a].Insert(3, "\t");
            }
            return string.Join("▲\n", arr);
        }
        string Kolis(string text)
        {
            string[] arr = text.Split('\n');

            for (int a = 0; a < arr.Length; a++)
            {
                string[] tag = arr[a].Split('\t');
                if (arr[a] == "") break;

                if (tag[1].Length == 0)
                    tag[1] = "  ";

                else if (tag[1].Length == 1)
                    tag[1] += " ";

                arr[a] = string.Join("\t", tag);
            }
            return string.Join("\n", arr);
        }
        #endregion

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                switch (e.KeyValue)
                {
                    case 72:        // h
                        findNchange fnc = new findNchange(this);
                        fnc.TopMost = true;
                        fnc.Show();
                        break;
                    default:
                        break;
                }
            }
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            double Opacity = trackBar1.Value * 5 * 0.01;

            this.Opacity = Opacity;
        }
    }
}
