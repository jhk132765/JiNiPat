﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using UniMarc.마크;

namespace ExcelTest
{
    public partial class findNchange : Form
    {
        public bool buttonClick1 = false;
        public bool buttonClick2 = false;
        string find = "";
        string change = "";
        String_Text st = new String_Text();
        AddMarc am;
        Marc mac;
        Marc_memo mmm;

        public findNchange(AddMarc _am)
        {
            InitializeComponent();
            am = _am;
        }
        public findNchange(Marc _mac)
        {
            InitializeComponent();
            mac = _mac;
        }
        public findNchange(Marc_memo _mmm)
        {
            InitializeComponent();
            mmm = _mmm;
        }

        private void findNchange_Load(object sender, EventArgs e)
        {

        }
        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buttonClick1 = true;
            find = textBox1.Text;
            change = textBox2.Text;
            textChange(find, change);
            if (mac != null) {
                st.Color_change("▼", mac.richTextBox1);
                st.Color_change("▲", mac.richTextBox1);
            }
            if (am != null) {
                st.Color_change("▼", am.richTextBox1);
                st.Color_change("▲", am.richTextBox1);
            }
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            buttonClick2 = true;
            Close();
        }

        public void textChange(string ftext, string ctext)
        {
            if (mac != null) mac.richTextBox1.Text = mac.richTextBox1.Text.Replace(ftext, ctext);
            if (mmm != null) mmm.richTextBox1.Text = mmm.richTextBox1.Text.Replace(ftext, ctext);
            if (am != null) am.richTextBox1.Text = am.richTextBox1.Text.Replace(ftext, ctext);
            
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }
    }
}
