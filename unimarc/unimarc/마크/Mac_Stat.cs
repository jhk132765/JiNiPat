﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UniMarc.마크;

namespace WindowsFormsApp1.Mac
{
    public partial class Mac_Stat : Form
    {
        Helper_DB db = new Helper_DB();
        string compidx;
        Main main;
        public Mac_Stat(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }

        private void Mac_Stat_Load(object sender, EventArgs e)
        {
            db.DBcon();

            string[] com1 = { "도서명", "출판사", "ISBN", "전체" };
            cb_Search.Items.AddRange(com1);
            cb_Search.SelectedIndex = 0;

            string[] com2 = { "도서명", "ISBN", "출판사" };
            cb_Sort.Items.AddRange(com2);

            string cmd = string.Format("SELECT `name` FROM `User_Data` WHERE `affil` = (SELECT `comp_name` FROM `Comp` WHERE `idx` = {0})", compidx);

            string res = db.self_Made_Cmd(cmd);
            string[] User = res.Split('|');
            User[User.Length - 1] = "전체";
            cb_Charge.Items.AddRange(User);
            cb_Charge.SelectedIndex = 0;

            Start_Date.Checked = false;
            End_Date.Checked = false;
        }

        private void tb_Search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_Lookup_Click(null, null);
            }
        }

        private void btn_Lookup_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string cmd = Make_CMD();
            string res = db.DB_Send_CMD_Search(cmd);
            string[] ary_res = res.Split('|');

            string[] grid = { "", "", "", "", "", "", "", "" };
            int cout = grid.Length;
            for (int a = 0; a < ary_res.Length; a++)
            {
                if (a % cout == 0) { grid[0] = ary_res[a]; }
                if (a % cout == 1) { grid[1] = ary_res[a]; }
                if (a % cout == 2) { grid[2] = ary_res[a]; }
                if (a % cout == 3) { grid[3] = ary_res[a]; }
                if (a % cout == 4) { grid[4] = ary_res[a]; }
                if (a % cout == 5) { grid[5] = ary_res[a]; }
                if (a % cout == 6) { grid[6] = ary_res[a]; }
                if (a % cout == 7)
                {
                    grid[7] = ary_res[a];
                    dataGridView1.Rows.Add(grid);
                }
            }
        }
        #region btn_Lookup_Sub
        /// <summary>
        /// 필터링을 하면서 DB에 건네줄 CMD를 만들어냄
        /// </summary>
        /// <returns>DB CMD</returns>
        string Make_CMD()
        {
            string Area = "`idx`, `ISBN`, `서명`, `저자`, `출판사`, `008tag`, `user`, `date`";
            string cmd = string.Format("SELECT {0} FROM `Marc` WHERE `compidx` = {1}", Area, compidx);

            if (tb_Search.Text != "")
            {
                // 필터
                switch (cb_Search.SelectedIndex)
                {
                    case 0:     // 도서명
                        cmd += string.Format(" AND `서명` LIKE '%{0}%'", tb_Search.Text);
                        break;

                    case 1:     // 출판사
                        cmd += string.Format(" AND `출판사` LIKE '%{0}%'", tb_Search.Text);
                        break;

                    case 2:     // ISBN
                        cmd += string.Format(" AND `ISBN` LIKE '%{0}%'", tb_Search.Text);
                        break;

                    case 3:     // 전체
                        break;

                    default:
                        break;
                }
            }

            if (cb_Charge.Text != "전체")
            {
                cmd += string.Format(" AND `user` = '{0}'", cb_Charge.Text);
            }

            if (Start_Date.Checked)
            {
                string start_Date = Start_Date.Value.ToString("yyyy-MM-dd");
                start_Date += " 00:00:00";
                cmd += string.Format(" AND `date` > '{0}'", start_Date);
            }
            if (End_Date.Checked)
            {
                string end_Date = End_Date.Value.ToString("yyyy-MM-dd");
                end_Date += " 23:59:59";
                cmd += string.Format(" AND `date` <= '{0}'", end_Date);
            }
            return cmd + " ORDER BY `date` DESC LIMIT 10000;";
        }
        #endregion

        private void btn_Stats_Click(object sender, EventArgs e)
        {
            Mac_Stat_Stat mss = new Mac_Stat_Stat(this);
            mss.compidx = compidx;
            mss.Show();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }

        private void cb_Sort_SelectedIndexChanged(object sender, EventArgs e)
        {
            //[0] "도서명" / [1] "ISBN" / [2] "출판사"
            int SortIdx = cb_Sort.SelectedIndex;

            switch (SortIdx)
            {
                case 0: dataGridView1.Sort(book_name, ListSortDirection.Ascending); break;
                case 1: dataGridView1.Sort(ISBN, ListSortDirection.Ascending); break;
                case 2: dataGridView1.Sort(book_comp, ListSortDirection.Ascending); break;
            }

        }
    }
}
