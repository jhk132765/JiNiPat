﻿
namespace UniMarc.마크
{
    partial class All_Book_manage_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_setName = new System.Windows.Forms.TextBox();
            this.tb_setCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_setYear = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_setISBN = new System.Windows.Forms.TextBox();
            this.tb_setPrice = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.series_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sub_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.series_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ISBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "* 세트 명";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "구성 수량";
            // 
            // tb_setName
            // 
            this.tb_setName.Location = new System.Drawing.Point(74, 12);
            this.tb_setName.Name = "tb_setName";
            this.tb_setName.Size = new System.Drawing.Size(583, 21);
            this.tb_setName.TabIndex = 1;
            // 
            // tb_setCount
            // 
            this.tb_setCount.Location = new System.Drawing.Point(74, 39);
            this.tb_setCount.Name = "tb_setCount";
            this.tb_setCount.Size = new System.Drawing.Size(232, 21);
            this.tb_setCount.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(362, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "출간 연도";
            // 
            // tb_setYear
            // 
            this.tb_setYear.Location = new System.Drawing.Point(425, 39);
            this.tb_setYear.Name = "tb_setYear";
            this.tb_setYear.Size = new System.Drawing.Size(232, 21);
            this.tb_setYear.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "세트 ISBN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(362, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "세트 정가";
            // 
            // tb_setISBN
            // 
            this.tb_setISBN.Location = new System.Drawing.Point(74, 66);
            this.tb_setISBN.Name = "tb_setISBN";
            this.tb_setISBN.Size = new System.Drawing.Size(232, 21);
            this.tb_setISBN.TabIndex = 1;
            // 
            // tb_setPrice
            // 
            this.tb_setPrice.Location = new System.Drawing.Point(425, 66);
            this.tb_setPrice.Name = "tb_setPrice";
            this.tb_setPrice.Size = new System.Drawing.Size(232, 21);
            this.tb_setPrice.TabIndex = 1;
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.book_name,
            this.series_title,
            this.sub_title,
            this.series_num,
            this.author,
            this.book_comp,
            this.price,
            this.ISBN});
            this.dataGridView1.Location = new System.Drawing.Point(12, 93);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(966, 222);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(160, 321);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(100, 30);
            this.btn_Save.TabIndex = 3;
            this.btn_Save.Text = "저   장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(425, 321);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(100, 30);
            this.btn_Close.TabIndex = 3;
            this.btn_Close.Text = "닫   기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // book_name
            // 
            this.book_name.HeaderText = "* 도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 150;
            // 
            // series_title
            // 
            this.series_title.HeaderText = "총서명";
            this.series_title.Name = "series_title";
            // 
            // sub_title
            // 
            this.sub_title.HeaderText = "부서명";
            this.sub_title.Name = "sub_title";
            // 
            // series_num
            // 
            this.series_num.HeaderText = "총서번호";
            this.series_num.Name = "series_num";
            // 
            // author
            // 
            this.author.HeaderText = "* 저자";
            this.author.Name = "author";
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "* 출판사";
            this.book_comp.Name = "book_comp";
            this.book_comp.Width = 150;
            // 
            // price
            // 
            this.price.HeaderText = "* 정가";
            this.price.Name = "price";
            // 
            // ISBN
            // 
            this.ISBN.HeaderText = "ISBN";
            this.ISBN.Name = "ISBN";
            // 
            // All_Book_manage_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 358);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tb_setPrice);
            this.Controls.Add(this.tb_setYear);
            this.Controls.Add(this.tb_setISBN);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_setCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_setName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "All_Book_manage_Add";
            this.Text = "전집 목록 생성";
            this.Load += new System.EventHandler(this.All_Book_manage_Add_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_setName;
        private System.Windows.Forms.TextBox tb_setCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_setYear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_setISBN;
        private System.Windows.Forms.TextBox tb_setPrice;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn series_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn sub_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn series_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISBN;
    }
}