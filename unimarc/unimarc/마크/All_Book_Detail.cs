﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Mac;
using WindowsFormsApp1;

namespace UniMarc.마크
{
    public partial class All_Book_Detail : Form
    {
        public int row;
        public int col;

        /// <summary>
        /// 세트명[0] / 세트ISBN[1] / 세트수량[2] / 세트정가[3]
        /// </summary>
        public string[] data = { "", "", "", "" };

        string compidx;
        Helper_DB db = new Helper_DB();
        All_Book_manage manage;
        public All_Book_Detail(All_Book_manage _manage)
        {
            InitializeComponent();
            manage = _manage;
            compidx = manage.compidx;
        }

        private void All_Book_Detail_Load(object sender, EventArgs e)
        {
            this.Text += string.Format("  『{0}』", data[0]);
            db.DBcon();

            string table = "Set_Book";
            string Area = "`idx`, `book_name`, `series_title`, `sub_title`, `series_num`, `author`, `book_comp`, `isbn`, `price`";
            string[] search_table = { "set_name", "set_isbn", "set_count", "set_price" };
            string cmd = db.More_DB_Search(table, search_table, data, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] db_data = db_res.Split('|');

            input_grid(db_data);
        }
        #region Load_Sub
        private void input_grid(string[] data)
        {
            string[] grid = { "", "", "", "", "", 
                              "", "", "", "" };
            for(int a = 0; a < data.Length; a++)
            {
                switch (a % grid.Length) 
                {
                    case 0:
                        grid[0] = data[a];
                        break;
                    case 1:
                        grid[1] = data[a];
                        break;
                    case 2:
                        grid[2] = data[a];
                        break;
                    case 3:
                        grid[3] = data[a];
                        break;
                    case 4:
                        grid[4] = data[a];
                        break;
                    case 5:
                        grid[5] = data[a];
                        break;
                    case 6:
                        grid[6] = data[a];
                        break;
                    case 7:
                        grid[7] = data[a];
                        break;
                    case 8:
                        grid[8] = data[a];
                        dataGridView1.Rows.Add(grid);
                        break;
                }
            }
        }
        #endregion

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }
    }
}
