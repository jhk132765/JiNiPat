﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UniMarc.마크;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;

namespace ExcelTest
{
    public partial class Marc : Form
    {
        /// <summary>
        /// isbn / 도서명 / 저자 / 출판사 / 가격
        /// </summary>
        string[] data_book = { "", "", "", "", "" };
        int SaveRowIdx = -1;
        public string user_name;
        string compidx = UniMarc.Properties.Settings.Default.compidx;
        public string find;
        public string change;
        public string call = "";
        private string data008 = string.Empty;
        string tbName = "";
        public int checkCout = 0;
        public bool gridViewChk = false;        // True / False
        Helper_DB db = new Helper_DB();
        Help008Tag tag008 = new Help008Tag();
        Skill_Search_Text search_Text = new Skill_Search_Text();
        String_Text st = new String_Text();
        Mac_List ml;

        public Marc()
        {
            InitializeComponent();
        }
        public Marc(Mac_List _ml)
        {
            InitializeComponent();
            ml = _ml;
            user_name = ml.user_name;
        }

        private void Marc_Load(object sender, EventArgs e)
        {
            #region ComboBox Item Setting
            // 이용자 9
            comboBox1.Items.AddRange(tag008.combo1);

            // 자료형식 3
            comboBox2.Items.AddRange(tag008.combo2);

            // 내용형식 28
            comboBox3.Items.AddRange(tag008.combo3);
            comboBox7.Items.AddRange(tag008.combo3);

            // 문학형식 16
            comboBox4.Items.AddRange(tag008.combo4);

            // 전기 5
            comboBox5.Items.AddRange(tag008.combo5);

            // 언어 36
            comboBox6.Items.AddRange(tag008.combo6);

            // 콤보박스 정렬및 필터
            string[] combo8 = { "등급", "복본" };
            comboBox8.Items.AddRange(combo8);

            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
            comboBox5.SelectedIndex = 0;
            comboBox6.SelectedIndex = 0;
            comboBox7.SelectedIndex = 0;
            comboBox8.SelectedIndex = 0;

            #endregion
        }

        public void input_list(string l_idx, string value)
        {
            db.DBcon();

            lbl_BookList.Text = value;

            string Area =
                // 0 1 2 3
                "olb.idx, olb.isbn_marc, olb.header, olb.num, " +
                // 4 5 6 7 8 9
                "olb.book_name, olb.author, olb.book_comp, olb.`count`, olb.price, olb.image_url, " +
                // 10 11 12 13 14 15 16
                "mac.idx, mac.marc, mac.marc_chk, mac.marc1, mac.marc_chk1, mac.marc2, mac.marc_chk2, " +
                // 17 18 19 20
                "mac.grade, mac.compidx, mac.`user`, mac.date";

            string CMD = string.Format("" +
                "SELECT {0} " +
                "FROM Obj_List_Book AS olb " +
                    "LEFT JOIN Marc AS mac " +
                    // "ON if (olb.m_idx = 0, mac.ISBN = olb.isbn_marc, mac.idx = olb.m_idx) " +
                    "ON mac.ISBN = olb.isbn_marc " +
                "WHERE olb.l_idx = {1} " +
                "GROUP BY olb.book_name " +
                "ORDER BY olb.idx ASC;", Area, l_idx);
            string db_res = db.DB_Send_CMD_Search(CMD);
            string[] db_data = db_res.Split('|');

            string[] grid = {
                "", "", "", "", "",
                "", "", "", "", "",
                "", "", "V", "", "" };

            string[] MarcData = { "", "", "", "", "", "" };

            for (int a = 0; a < db_data.Length; a++)
            {
                if (a % 21 == 00) grid[0] = db_data[a];
                if (a % 21 == 01) grid[1] = db_data[a];
                if (a % 21 == 02) grid[2] = db_data[a];
                if (a % 21 == 03) grid[2] += db_data[a];
                if (a % 21 == 04) grid[3] = db_data[a];
                if (a % 21 == 05) grid[4] = db_data[a];
                if (a % 21 == 06) grid[5] = db_data[a];
                if (a % 21 == 07) grid[6] = db_data[a];
                if (a % 21 == 08) grid[7] = db_data[a];
                if (a % 21 == 09) grid[8] = db_data[a];
                if (a % 21 == 10) grid[9] = db_data[a];
                if (a % 21 == 11) MarcData[0] = db_data[a];
                if (a % 21 == 12) MarcData[1] = db_data[a];
                if (a % 21 == 13) MarcData[2] = db_data[a];
                if (a % 21 == 14) MarcData[3] = db_data[a];
                if (a % 21 == 15) MarcData[4] = db_data[a];
                if (a % 21 == 16) MarcData[5] = db_data[a];
                if (a % 21 == 17)
                {
                    grid[10] = RealMarc(MarcData);
                    if (grid[10] == "") grid[11] = "3";
                    else grid[11] = db_data[a];
                }
                if (a % 21 == 18) {
                    if (db_data[a] == "") grid[13] = "";
                    else if (compidx != db_data[a]) {
                        string FindCompCmd = string.Format("SELECT `comp_name` FROM `Comp` WHERE `idx` = {0}", db_data[a]);
                        grid[13] = db.DB_Send_CMD_Search(FindCompCmd).Replace("|", "");
                    }
                }
                if (a % 21 == 19) {
                    if (grid[13] == "")
                        grid[13] = db_data[a];
                }
                if (a % 21 == 20)
                {
                    grid[14] = db_data[a];
                    List_Book.Rows.Add(grid);
                }
            }

            for (int a = 0; a < List_Book.Rows.Count; a++)
            {
                string Grade = List_Book.Rows[a].Cells["grade"].Value.ToString();
                string SaveDate = List_Book.Rows[a].Cells["SaveDate"].Value.ToString();
                List_Book.Rows[a].DefaultCellStyle.ForeColor = SetGradeColor(Grade);
                SaveDateCheck(SaveDate, a);
            }

        }

        private string RealMarc(string[] MarcData)
        {
            string result = "";
            if (MarcData[1] == "1")
                result = MarcData[0];

            if (MarcData[3] == "1")
                result = MarcData[2];

            if (MarcData[5] == "1")
                result = MarcData[4];

            return result;
        }

        /// <summary>
        /// 가져온 목록을 책목록과 연동하여 Grid에 집어넣음.
        /// </summary>
        /// <param name="date">목록일자</param>
        /// <param name="value">목록명</param>
        public void input_list(string l_idx, string value, string C_idx)
        {
            db.DBcon();
            string Area = "`idx`, `isbn_marc`, `header`, `num`, `book_name`, `author`, `book_comp`, `count`, `pay`, `image_url`, `m_idx`";
            string[] sear_tbl = { "l_idx", "compidx" };
            string[] sear_col = { l_idx, C_idx };

            lbl_BookList.Text = value;

            string cmd =
                string.Format("SELECT {0} " +
                "FROM {1} " +
                "WHERE `{2}` = \"{4}\" AND `{3}` = \"{5}\"" +
                "ORDER BY `idx` ASC;", Area, "Obj_List_Book", sear_tbl[0], sear_tbl[1], sear_col[0], sear_col[1]);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] db_data = db_res.Split('|');
            string[] grid = {
                "", "", "", "", "",
                "", "", "", "", "",
                "", "", "V", "", "" };

            for (int a = 0; a < db_data.Length; a++)
            {
                if (a % 11 == 0) { grid[0] = db_data[a]; }   // 0: idx
                if (a % 11 == 1) { grid[1] = db_data[a]; }   // 1: isbn
                if (a % 11 == 2) { grid[2] = db_data[a]; }   // 2: header
                if (a % 11 == 3) { grid[2] += db_data[a]; }  // 2: num
                if (a % 11 == 4) { grid[3] = db_data[a]; }   // 3: book_num
                if (a % 11 == 5) { grid[4] = db_data[a]; }   // 4: author
                if (a % 11 == 6) { grid[5] = db_data[a]; }   // 5: book_comp
                if (a % 11 == 7) { grid[6] = db_data[a]; }   // 6: count
                if (a % 11 == 8) { grid[7] = db_data[a]; }   // 7: pay
                if (a % 11 == 9) { grid[8] = db_data[a]; }   // 8: image_url
                if (a % 11 == 10) { grid[9] = db_data[a];    // 9: m_idx
                    List_Book.Rows.Add(grid);
                }
            }
            chk_Marc();
        }

        /// <summary>
        /// 마크 유무 확인하는 함수
        /// </summary>
        void chk_Marc()
        {
            for (int a = 0; a < List_Book.Rows.Count; a++)
            {
                string Area =
                    // 0 1
                    "`idx`, `compidx`, " +
                    // 2 3 4 5 6 7
                    "`marc`, `marc_chk`, `marc1`, `marc_chk1`, `marc2`, `marc_chk2`, " +
                    // 8 9 10 11
                    "`grade`, `008tag`, `user`, `date`";

                string Table = "Marc";

                string[] sear_tbl = { "ISBN" };
                string[] sear_col = { List_Book.Rows[a].Cells["ISBN13"].Value.ToString() };

                if (List_Book.Rows[a].Cells["marc_idx"].Value.ToString() != "0") {
                    sear_tbl[0] = "idx";
                    sear_col[0] = List_Book.Rows[a].Cells["marc_idx"].Value.ToString();
                }

                string Chk_Cmd = string.Format("SELECT {0} FROM {1} WHERE `{2}` = \"{3}\" ORDER BY FIELD(`compidx`, {4}) DESC;",
                    Area, Table, sear_tbl[0], sear_col[0], compidx);

                string Chk_Res = db.DB_Send_CMD_Search(Chk_Cmd);
                string[] Chk_Arr = Chk_Res.Split('|');

                bool isMyData = true;
                if (Chk_Arr.Length < 2) {
                    List_Book.Rows[a].Cells["grade"].Value = "3";
                    List_Book.Rows[a].DefaultCellStyle.ForeColor = Color.Red;
                    continue;
                }

                if (Chk_Arr[1] != compidx) 
                    isMyData = false;
                
                string[] MarcData = { Chk_Arr[2], Chk_Arr[4], Chk_Arr[6] };
                string[] CheckData = { Chk_Arr[3], Chk_Arr[5], Chk_Arr[7] };
                
                List_Book.Rows[a].DefaultCellStyle.ForeColor = SetGradeColor(Chk_Arr[8], isMyData);
                List_Book.Rows[a].Cells["marc_idx"].Value = Chk_Arr[0];
                List_Book.Rows[a].Cells["db_marc"].Value = NewestMarc(MarcData, CheckData);
                List_Book.Rows[a].Cells["grade"].Value = Chk_Arr[8];
                text008.Text = Chk_Arr[9];
                List_Book.Rows[a].Cells["user"].Value = Chk_Arr[10];
                List_Book.Rows[a].Cells["SaveDate"].Value = Chk_Arr[11];
                
                if (isMyData)
                    SaveDateCheck(Chk_Arr[11], a);
                
                else {
                    string FindCompCmd = string.Format("SELECT `comp_name` FROM `Comp` WHERE `idx` = {0}", Chk_Arr[1]);
                    List_Book.Rows[a].Cells["user"].Value = db.DB_Send_CMD_Search(FindCompCmd).Replace("|", "");
                    List_Book.Rows[a].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
        }

        private string NewestMarc(string[] marc, string[] marc_chk )
        {
            string result = "";
            int count = 0;
            foreach (string chk in marc_chk)
            {
                if (chk == "1")
                    result = marc[count];

                count++;
            }
            return result;
        }

        private Color SetGradeColor(string Grade, bool isMyData = true)
        {
            if (!isMyData)
                return Color.Orange;

            switch (Grade)
            {
                case "0": // A
                    return Color.Blue;
                    
                case "1": // B
                    return Color.Black;
                    
                case "2": // C
                    return Color.Gray;
                    
                case "3": // D
                    return Color.Red;

                default:
                    return Color.Black;
            }
        }

        /// <summary>
        /// 마지막 저장시각 14일이전일 경우 배경 색 변경
        /// </summary>
        /// <param name="Date">마지막 저장시각</param>
        /// <param name="row">해당 행</param>
        private void SaveDateCheck(string Date, int row)
        {
            DateTime SaveDate = DateTime.ParseExact(Date, "yyyy-MM-dd HH:mm:ss",
                System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            DateTime TargetDate = DateTime.Today.AddDays(-14);

            int result = DateTime.Compare(SaveDate, TargetDate);
            
            if (result >= 0)    // SaveDate가 같거나 큼
                List_Book.Rows[row].DefaultCellStyle.BackColor = Color.Yellow;

            else                // TargetDate가 큼
                List_Book.Rows[row].DefaultCellStyle.BackColor = Color.White;

        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            call = ((RichTextBox)sender).Name;
            richTextBox1.LanguageOption = 0;

            if (e.Control) {
                switch (e.KeyValue)
                {
                    case 70:        // f
                        string value = "";
                        if (search_Text.InputBox("찾을 단어를 입력해주세요.", "찾기(Ctrl+F)", ref value) == DialogResult.OK)
                            st.Color_change(value, richTextBox1);
                        break;

                    case 72:        // h
                        findNchange fnc = new findNchange(this);
                        fnc.Show();
                        break;

                    default:
                        break;
                }
            }

            if (e.KeyCode == Keys.F9)
                SaveGrade(Keys.F9);
            else if (e.KeyCode == Keys.F10)
                SaveGrade(Keys.F10);
            else if (e.KeyCode == Keys.F11)
                SaveGrade(Keys.F11);
            else if (e.KeyCode == Keys.F12)
                SaveGrade(Keys.F12);
            else if (e.KeyCode == Keys.F3)
                richTextBox1.SelectedText = "▼";
            else if (e.KeyCode == Keys.F4)
                richTextBox1.SelectedText = "▲";
        }

        /// <summary>
        /// F9~F12로 등급별 저장
        /// </summary>
        /// <param name="key">F9~F12</param>
        private void SaveGrade(Keys key)
        {
            switch (key)
            {
                case Keys.F9:
                    cb_grade.SelectedItem = "A (F9)";
                    Btn_Save_Click(null, null);
                    break;
                case Keys.F10:
                    cb_grade.SelectedItem = "B (F10)";
                    Btn_Save_Click(null, null);
                    break;
                case Keys.F11:
                    cb_grade.SelectedItem = "C (F11)";
                    Btn_Save_Click(null, null);
                    break;
                case Keys.F12:
                    cb_grade.SelectedItem = "D (F12)";
                    Btn_Save_Click(null, null);
                    break;
            }
        }

        private void etc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
            {
                ((RichTextBox)sender).Text += DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            }
        }

        private void comboBox1_MouseClick(object sender, MouseEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = true;
        }

        private void text008col_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tbName = ((TextBox)sender).Name;
                call = ((TextBox)sender).Text;
                Helper008 helper = new Helper008(this);
                helper.Who_Call(tbName, call);
                helper.Show();
            }
        }
        private void Btn_Memo_Click(object sender, EventArgs e)
        {
            Marc_memo memo = new Marc_memo(this);
            memo.StartPosition = FormStartPosition.Manual;
            memo.TopMost = true;
            memo.Location = new Point(1018, 8);
            memo.Show();
        }
        private void Btn_preview_Click(object sender, EventArgs e)
        {
            Marc_Preview mp = new Marc_Preview();
            mp.isbn = data_book[0];
            mp.richTextBox1.Text = richTextBox1.Text;
            mp.Show();
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (SaveRowIdx < 0) {
                MessageBox.Show("마크가 선택되지않았습니다.");
                return;
            }
            int TabIndex = tabControl1.SelectedIndex;
            int grade = cb_grade.SelectedIndex;
            if (TabIndex == 1)
            {
                MessageBox.Show("[칸채우기]가 아닌 [마크 편집] 탭에서 저장해주세요!");
                return;
            }
            if (grade == 3) {
                MessageBox.Show("등급을 설정해주세요. (C 이상)");
                return;
            }
            string table_name = "Marc";
            string BaseText = richTextBox1.Text;
            string lblisbn = lbl_ISBN.Text.Replace("[", "").Replace("]", "");

            if (!BaseText.EndsWith("\n"))
                BaseText += "\n";

            if (!isPass(BaseText)) {
                MessageBox.Show("입력된 마크의 상태를 확인해주세요.");
                return;
            }

            if (BaseText.IndexOf(lblisbn) < 0) {
                MessageBox.Show("ISBN 상태를 확인해주세요.");
                return;
            }
            string tag056 = Tag056();
            string[] grid_data = {
                List_Book.Rows[SaveRowIdx].Cells["ISBN13"].Value.ToString(),
                List_Book.Rows[SaveRowIdx].Cells["book_name"].Value.ToString(),
                List_Book.Rows[SaveRowIdx].Cells["author"].Value.ToString(),
                List_Book.Rows[SaveRowIdx].Cells["book_comp"].Value.ToString(),
                List_Book.Rows[SaveRowIdx].Cells["pay"].Value.ToString(),
                List_Book.Rows[SaveRowIdx].Cells["url"].Value.ToString() };
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string orimarc = st.made_Ori_marc(richTextBox1).Replace(@"\", "￦");
            string Midx = List_Book.Rows[SaveRowIdx].Cells["marc_idx"].Value.ToString();
            // 필수태그 확인
            if (!isMustTag(orimarc))
            {
                richTextBox1.Text = BaseText;
                return;
            }

            string SaveDate = List_Book.Rows[SaveRowIdx].Cells["SaveDate"].Value.ToString();
            string SaveUser = List_Book.Rows[SaveRowIdx].Cells["user"].Value.ToString();
            bool IsCoverDate = false;

            if (SaveDate != "")
            {
                // 마지막 수정일로부터 2일이 지났는지, 마지막 저장자가 사용자인지 확인
                TimeSpan sp = spspsp(SaveDate, date);
                IsCoverDate = IsCoverData(sp.Days, SaveUser);

                if (IsCoverDate) {
                    etc2.Text = etc2.Text.Replace(SaveDate, date);
                }
                else {
                    etc2.Text += string.Format("{0}\t{1}\n", date, user_name);
                }
            }
            else
            {
                etc2.Text += string.Format("{0}\t{1}\n", date, user_name);
            }

            // true일 경우 INSERT, false일 경우 UPDATE
            bool isNewData = true;

            if (Midx != "")
                isNewData = false;

            if (List_Book.Rows[SaveRowIdx].DefaultCellStyle.ForeColor == Color.Orange ||
                List_Book.Rows[SaveRowIdx].DefaultCellStyle.ForeColor == Color.Red)
            {
                string[] Insert_tbl = {
                    "ISBN", "서명", "저자", "출판사",
                    "가격", "marc", "비고1", "비고2", "url",
                    "grade", "marc_chk", "user", "division", "008tag",
                    "date", "compidx" };
                string[] Insert_col = {
                    grid_data[0], grid_data[1], grid_data[2], grid_data[3],
                    grid_data[4], orimarc, etc1.Text, etc2.Text, grid_data[5],
                    grade.ToString(), "1", user_name, tag056, text008.Text,
                    date, compidx };

                string Incmd = db.DB_INSERT(table_name, Insert_tbl, Insert_col);
                db.DB_Send_CMD_reVoid(Incmd);
            }
            else
            {
                string[] Edit_tbl = {
                    "compidx", "marc", "marc_chk", "marc_chk1", "비고1",
                    "비고2", "url", "division", "008tag", "date",
                    "user", "grade" };
                string[] Edit_col = {
                    compidx, orimarc, "1", "0", etc1.Text,
                    etc2.Text, grid_data[5], tag056, text008.Text, date,
                    user_name, grade.ToString() };
                string[] Sear_tbl = { "idx" };
                string[] Sear_col = { Midx };

                int marcChk = Sub_marc_chk(grid_data[0]);
                if (IsCoverDate)
                    marcChk--;

                switch (marcChk)
                {
                    case 0:
                        Edit_tbl[1] = "marc1";
                        Edit_tbl[2] = "marc_chk1";
                        Edit_tbl[3] = "marc_chk";
                        break;
                    case 1:
                        Edit_tbl[1] = "marc2";
                        Edit_tbl[2] = "marc_chk2";
                        Edit_tbl[3] = "marc_chk1";
                        break;
                    case 2:
                        Edit_tbl[1] = "marc";
                        Edit_tbl[2] = "marc_chk";
                        Edit_tbl[3] = "marc_chk2";
                        break;
                    default:
                        Edit_tbl[1] = "marc";
                        Edit_tbl[2] = "marc_chk";
                        Edit_tbl[3] = "marc_chk2";
                        break;
                }

                string U_cmd = db.More_Update(table_name, Edit_tbl, Edit_col, Sear_tbl, Sear_col);
                db.DB_Send_CMD_reVoid(U_cmd);
            }

            List_Book.Rows[SaveRowIdx].Cells["grade"].Value = grade.ToString();
            List_Book.Rows[SaveRowIdx].Cells["SaveDate"].Value = date;
            List_Book.Rows[SaveRowIdx].Cells["user"].Value = user_name;
            List_Book.Rows[SaveRowIdx].Cells["db_marc"].Value = orimarc;

            List_Book.Rows[SaveRowIdx].DefaultCellStyle.ForeColor = SetGradeColor(grade.ToString());

            if (isNewData) {
                string MidxQuery = string.Format("SELECT `idx` FROM Marc WHERE isbn = {0} AND `compidx` = {1};", grid_data[0], compidx);
                Midx = db.DB_Send_CMD_Search(MidxQuery).Replace("|", "");
                List_Book.Rows[SaveRowIdx].Cells["marc_idx"].Value = Midx;
            }

            string UpdateListIndex = string.Format("UPDATE `Obj_List_Book` SET `m_idx` = {0} WHERE `idx` = {1};",
                Midx, List_Book.Rows[SaveRowIdx].Cells["list_idx"].Value.ToString());
            db.DB_Send_CMD_reVoid(UpdateListIndex);
            MessageBox.Show("저장되었습니다!");
        }

        #region Save_Click_Sub

        /// <summary>
        /// 마크 저장시 사용하며, 마지막 수정일과 수정자를 가져와 덮어씌울지 백업데이터를 만들지 구분
        /// </summary>
        /// <param name="TimeSpanDaysValue">저장할 마크의 마지막 수정일</param>
        /// <param name="user">저장할 마크의 마지막 수정자</param>
        /// <returns>마지막 수정일로부터 2일이 지나지않고, 마지막 수정자와 해당 유저가 동일 할 경우 true 반환</returns>
        private bool IsCoverData(int TimeSpanDaysValue, string user)
        {
            if (TimeSpanDaysValue < -1)
                return false;
            if (user != user_name)
                return false;

            return true;
        }

        private TimeSpan spspsp(string LastDate, string SaveDate)
        {
            DateTime date1 = Convert.ToDateTime(LastDate);
            DateTime date2 = Convert.ToDateTime(SaveDate);

            TimeSpan result = date1 - date2;
            return result;
        }

        /// <summary>
        /// 마크 오류체크 (형식체크)
        /// </summary>
        /// <param name="BaseData">richTextBox에 들어가있는 텍스트</param>
        /// <returns>True / False</returns>
        private bool isPass(string BaseData)
        {
            string[] EnterSplit = BaseData.Split('\n');

            foreach (string Data in EnterSplit)
            {
                if (Data == "")
                    continue;

                string[] DataSplit = Data.Split('\t');
                if (DataSplit.Length == 3)
                {
                    if (DataSplit[1].Length == 2)
                    {
                        continue;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private bool isMustTag(string orimarc)
        {
            String_Text st = new String_Text();
            string[] SearchTag = {
                "056a", "0562", "245a", "245d", "260a", "260b", "260c", "300a", "300c", "653a"
            };

            string[] Tag = st.Take_Tag(orimarc, SearchTag);

            int count = 0;
            string msg = "";
            bool isTag = true;
            foreach (string tag in Tag)
            {
                if (tag == "")
                {
                    msg += SearchTag[count] + " ";
                    isTag = false;
                }
                count++;
            }

            if (!isTag)
            {
                MessageBox.Show(msg + "태그가 없습니다.");
                return false;
            }

            bool is1XX = false;
            string[] AuthorTag = { "100a", "110a", "111a" };
            Tag = st.Take_Tag(orimarc, AuthorTag);

            foreach (string author in Tag)
            {
                if (author != "")
                    is1XX = true;
            }

            if (!is1XX)
            {
                MessageBox.Show("기본표목이 존재하지않습니다.");
                return false;
            }

            bool is7XX = false;
            AuthorTag[0] = "700a";
            AuthorTag[1] = "710a";
            AuthorTag[2] = "711a";
            Tag = st.Take_Tag(orimarc, AuthorTag);

            foreach (string author in Tag)
            {
                if (author != "")
                    is7XX = true;
            }

            if (!is7XX)
            {
                MessageBox.Show("부출표목이 존재하지않습니다.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 분류기호(056)추출 & 008태그 마크에 삽입
        /// </summary>
        /// <returns></returns>
        string Tag056()
        {
            string marc = richTextBox1.Text;
            string[] temp = marc.Split('\n');
            List<string> target = temp.ToList();
            btn_Reflesh008_Click(null, null);
            bool isEight = false;
            bool eight_chk = false;
            string tag056 = string.Empty;
            int count = 0;

            for (int a = 0; a < target.Count - 1; a++)
            {
                string[] tmp = target[a].Split('\t');
                string tag = tmp[0];
                if (tag == "") break;
                int eight = Convert.ToInt32(tag.Substring(0, 3));
                if (eight == 008)
                {
                    count = a;
                    isEight = true;
                }
                else if (eight > 008 && !eight_chk)
                {
                    count = a;
                    eight_chk = true;
                }
                if (tag == "056")
                {
                    tag056 = GetMiddelString(tmp[2], "▼a", "▼");
                }
            }
            if (!isEight)
                target.Insert(count, string.Format("{0}\t{1}\t{2}▲", "008", "  ", text008.Text));

            richTextBox1.Text = string.Join("\n", target.ToArray());
            return tag056;
        }

        private int Sub_marc_chk(string isbn)
        {
            string Area = "`marc_chk`, `marc_chk1`, `marc_chk2`";
            string cmd = db.DB_Select_Search(Area, "Marc", "ISBN", isbn);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] chk_ary = db_res.Split('|');

            for (int a = 0; a < chk_ary.Length; a++)
            {
                if (chk_ary[a] == "1") {
                    return a;
                }
            }
            return 0;
        }
        #endregion

        private void List_Book_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) { return; }
            int row_idx = e.RowIndex;
            SaveRowIdx = row_idx;
            int col_idx = e.ColumnIndex;

            string isbn = List_Book.Rows[row_idx].Cells["ISBN13"].Value.ToString();
            if (isbn != "") {
                string CountQuery = string.Format("SELECT Count(isbn) FROM Marc WHERE isbn = {0} GROUP BY isbn;", isbn);
                string CountResult = db.self_Made_Cmd(CountQuery).Replace("|", "");

                if (CountResult == "")
                    btn_CopySelect.Text = "0";

                if (CountResult == "0") {
                    btn_CopySelect.Enabled = false;
                    btn_CopySelect.BackColor = Color.Silver;
                }
                else {
                    btn_CopySelect.Enabled = true;
                    btn_CopySelect.BackColor = Color.Khaki;
                }

                btn_CopySelect.Text = CountResult;
            }

            tabControl1.SelectedIndex = 0;

            if (check_V(row_idx, col_idx))
                return;

            Save_data(row_idx);
            richTextBox1.Text = "";

            bool check_Marc = click_Marc(row_idx);

            if (!check_Marc)
                richTextBox1.Text = Make_Empty();
            else
                ReadRemark(row_idx);

            input_picture(row_idx);
            Create_008();
            st.Color_change("▼", richTextBox1);
            st.Color_change("▲", richTextBox1);

            int grade = 2;
            if (List_Book.Rows[row_idx].Cells["grade"].Value != null) {
                grade = Convert.ToInt32(List_Book.Rows[row_idx].Cells["grade"].Value.ToString());
            }
            cb_grade.SelectedIndex = grade;
            OnSaveData(row_idx);
        }
        #region CellClick_Sub

        void OnSaveData(int row)
        {
            string User = List_Book.Rows[row].Cells["user"].Value.ToString();
            string Date = List_Book.Rows[row].Cells["SaveDate"].Value.ToString();

            lbl_SaveData.Text = string.Format("[{0}] [{1}]", User, Date);
            lbl_ISBN.Text = "[" + List_Book.Rows[row].Cells["ISBN13"].Value.ToString() + "]";
        }
        void ReadRemark(int row)
        {
            string[] sear_tbl = { "idx" };
            string[] sear_col = { List_Book.Rows[row].Cells["marc_idx"].Value.ToString() };
            string cmd = db.More_DB_Search("Marc", sear_tbl, sear_col, "`비고1`, `비고2`");
            string res = db.DB_Send_CMD_Search(cmd);

            string[] ary = res.Split('|');
            if (res.Length < 1) {
                etc1.Text = res;
                etc2.Text = "";
                return;
            }
            etc1.Text = ary[0];
            etc2.Text = ary[1];
        }

        bool check_V(int row, int col)
        {
            string name = List_Book.Columns[col].Name;
            if (name == "colCheck")
            {
                if (List_Book.Rows[row].Cells[col].Value.ToString() == "V")
                    List_Book.Rows[row].Cells[col].Value = "";

                else
                    List_Book.Rows[row].Cells["colCheck"].Value = "V";

                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// 마크데이터가 있는지 확인하고 메모장으로 출력
        /// </summary>
        /// <param name="row">해당 데이터의 row값</param>
        /// <returns></returns>
        bool click_Marc(int row)
        {
            // 마크 데이터
            string Marc_data = List_Book.Rows[row].Cells["db_marc"].Value.ToString();

            if (Marc_data.Length < 3) return false;

            string result = string.Empty;

            List<string> TagNum = new List<string>();   // 태그번호
            List<string> field = new List<string>();    // 가변길이필드 저장

            // 특수기호 육안으로 확인하기 쉽게 변환
            Marc_data = Marc_data.Replace("", "▼");
            Marc_data = Marc_data.Replace("", "▲");
            Marc_data = Marc_data.Replace("￦", "\\");
            // string leader = Marc_data.Substring(0, 24);

            int startidx = 0;
            string[] data = Marc_data.Substring(24).Split('▲'); // 리더부를 제외한 디렉터리, 가변길이필드 저장

            // List에 필요한 데이터 집어넣는 작업.
            for (int a = 1; a < data.Length - 1; a++)
            {
                TagNum.Add(data[0].Substring(startidx, 3));
                startidx += 12;
                field.Add(data[a] + "▲");
            }

            // List에 들어간 데이터를 메모장에 출력시키는 작업.
            for (int a = 0; a < TagNum.Count; a++)
            {
                string res = TagNum[a];
                if (TagNum[a] == "008")
                {
                    text008.Text = field[a].Replace("▲", "");
                    data008 = text008.Text;
                    continue;
                }
                else { }
                if (field[a].IndexOf("▼") == -1)
                {
                    res += "\t  \t" + field[a];
                }
                else
                {
                    string temp = field[a].Insert(2, "\t");
                    res += "\t" + temp;
                }
                result += res + "\n";
            }
            richTextBox1.Text = result;
            return true;
        }
        /// <summary>
        /// 데이터 임시저장
        /// </summary>
        /// <param name="row">저장할 데이터가 있는 표의 Row값</param>
        void Save_data(int row)
        {
            data_book[0] = List_Book.Rows[row].Cells["ISBN13"].Value.ToString();
            data_book[1] = List_Book.Rows[row].Cells["book_name"].Value.ToString();
            data_book[2] = List_Book.Rows[row].Cells["author"].Value.ToString();
            data_book[3] = List_Book.Rows[row].Cells["book_comp"].Value.ToString();
            data_book[4] = List_Book.Rows[row].Cells["pay"].Value.ToString();
        }
        void input_picture(int row)
        {
            try
            {
                string isbn = List_Book.Rows[row].Cells["ISBN13"].Value.ToString();
                string isbn3 = isbn.Substring(isbn.Length - 3, 3);
                pictureBox1.ImageLocation = "http://image.kyobobook.co.kr/images/book/xlarge/" + isbn3 + "/x" + isbn + ".jpg";
            }
            catch
            {
                pictureBox1.Image = null;
            }
        }
        private string Make_Empty()
        {
            string yyMMdd = DateTime.Now.ToString("yyMMdd");
            string yyyy = DateTime.Now.ToString("yyyy");
            string Empty_008 = yyMMdd + "s" + yyyy + "                  000   kor  ▲";
            text008.Text = Empty_008.Replace("▲", "");
            data008 = text008.Text;
            string Empty_text = string.Format(
                "020\t  \t▼a{1}▼c\\{5}▲\n" +
                "056\t  \t▼a▼25▲\n" +
                "100\t  \t▼a▲\n" +
                "245\t  \t▼a{2}▼d{3}▲\n" +
                "260\t  \t▼b{4}▲\n" +
                "300\t  \t▼a▼c▲\n" +
                "653\t  \t▼a▲\n" +
                "700\t  \t▼a▲\n" +
                "950\t  \t▼b\\{5}▲\n",
                Empty_008, data_book[0], data_book[1], data_book[2], data_book[3], data_book[4]);
            etc1.Text = "";
            etc2.Text = "";
            return Empty_text;
        }
        /// <summary>
        /// 008 각각의 박스에 대입하는 함수
        /// </summary>
        /// <returns></returns>
        public void Create_008()
        {
            string data = text008.Text;

            if (data == "" || data == null) { return; }

            string[] Tag008 = {
            // 입력일자 발행년유형 발행년1 발행년2 발행국 
                "", "", "", "", "",
            // 삽화표시 이용대상자수준 개별자료형태 내용형식1 내용형식2 
                "", "", "", "", "",
            // 한국대학부호 수정레코드 회의간행물 기념논문집 색인 
                "", "", "", "", "",
            // 목록전거 문학형식 전기 언어 한국정부기관부호
                "", "", "", "", ""
            };

            // 사전에 선언된 string배열에 맞는 데이터를 배정.
            int tmp_years = Convert.ToInt32(data.Substring(0, 2));
            int now_years = Convert.ToInt32(DateTime.Now.ToString("yy"));
            string century = "20";

            if (tmp_years > now_years)
                century = "19";

            Tag008[0] = century
                      + data.Substring(0, 6);           // 입력일자 (00-05)
            Tag008[1] = data.Substring(6, 1);           // 발행년유형 (6)
            Tag008[2] = data.Substring(7, 4);           // 발행년1 (07-10)
            Tag008[3] = data.Substring(11, 4);          // 발행년2 (11-14)
            Tag008[4] = data.Substring(15, 3);          // 발행국 (15-17)

            Tag008[5] = data.Substring(18, 4);          // 삽화표시 (18-21)
            Tag008[6] = data.Substring(22, 1);          // 이용대상자수준 (22) v
            Tag008[7] = data.Substring(23, 1);          // 개별자료형태 (23) v
            Tag008[8] = data.Substring(24, 1);          // 내용형식1 (24) v
            Tag008[9] = data.Substring(25, 1);          // 내용형식2 (25) v

            Tag008[10] = data.Substring(26, 2);         // 한국대학부호 (26-27)
            Tag008[11] = data.Substring(28, 1);         // 수정레코드 (28)
            Tag008[12] = data.Substring(29, 1);         // 회의간행물 (29) c
            Tag008[13] = data.Substring(30, 1);         // 기념논문집 (30) c
            Tag008[14] = data.Substring(31, 1);         // 색인 (31)

            Tag008[15] = data.Substring(32, 1);         // 목록전거 (32)
            Tag008[16] = data.Substring(33, 1);         // 문학형식 (33) v
            Tag008[17] = data.Substring(34, 1);         // 전기 (34) v
            Tag008[18] = data.Substring(35, 3);         // 언어 (35-37) v
            Tag008[19] = data.Substring(38, 2);         // 한국정부기관부호 (38-39)

            // 배열에 들어간 데이터로 콤보박스를 꾸미는 작업.
            int year = Convert.ToInt32(Tag008[0].Substring(0, 4));
            int month = Convert.ToInt32(Tag008[0].Substring(4, 2));
            int day = Convert.ToInt32(Tag008[0].Substring(6, 2));
            input_date.Value = new DateTime(year, month, day);      // 입력일자

            comboBox1.SelectedIndex = tag008.User_008(Tag008[6]);   // 이용대상자수준
            comboBox2.SelectedIndex = tag008.DataType_008(Tag008[7]);   // 개별자료형태
            comboBox3.SelectedIndex = tag008.Format_008(Tag008[8]);   // 내용형식1
            comboBox7.SelectedIndex = tag008.Format_008(Tag008[9]);   // 내용형식2
            comboBox4.SelectedIndex = tag008.Literary_008(Tag008[16]);   // 문학형식
            comboBox5.SelectedIndex = tag008.Biography_008(Tag008[17]);   // 전기
            comboBox6.SelectedIndex = tag008.Language_008(Tag008[18]);   // 언어

            checkBox1.Checked = tag008.CheckBox_008(Tag008[12]);    // 회의간행물
            checkBox2.Checked = tag008.CheckBox_008(Tag008[13]);    // 기념논문집

            col008res.Text = Tag008[10];
            gov008res.Text = Tag008[19];

            return;
        }
        #endregion

        #region 008 관련 함수들

        private void btn_Reflesh008_Click(object sender, EventArgs e)
        {
            string data = text008.Text;
            string oriMarc = st.made_Ori_marc(richTextBox1).Replace("\\", "￦");

            if (data == "" || data == null) { return; }

            // 입력일자 발행년유형 발행년1 발행년2 발행국 
            // 삽화표시 이용대상자수준 개별자료형태 내용형식1 내용형식2 
            // 한국대학부호 수정레코드 회의간행물 기념논문집 색인 
            // 목록전거 문학형식 전기 언어 한국정부기관부호

            #region 사전에 선언된 string배열에 맞는 데이터를 배정.
            // 참조할 태그 배열선언
            string[] SearchTag = { "260c", "260a", "300b" };
            string[] ContentTag = st.Take_Tag(oriMarc, SearchTag);

            // 입력일자 (00-05)
            string day;
            if (input_date.Checked)
                day = string.Format("{0}{1}{2}",
                DateTime.Now.ToString("yy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"));
            else
                day = input_date.Value.ToString("yy") + input_date.Value.ToString("MM") + input_date.Value.ToString("dd");

            // 발행년유형 (6), 발행년1 (07-10), 발행년2 (11-14)
            string DateSet = pubDateSet(ContentTag[0]);

            // 발행국 (15-17)
            string Country = pubCountry(ContentTag[1]);

            // 삽화표시 (18-21)
            string Picture = tag008.Picture_008(ContentTag[2]);

            string tmp = day + DateSet + Country + Picture;


            char[] textArray = data.ToCharArray();
            for (int a = 0; a < tmp.Length; a++) {
                textArray[a] = tmp[a];
            }
            text008.Text = new string(textArray);

            // 이용대상자수준 (22) v
            // 개별자료형태 (23) v
            // 내용형식1 (24) v
            // 내용형식2 (25) v
            // 한국대학부호 (26-27)
            // 수정레코드 (28)
            // 회의간행물 (29) c
            // 기념논문집 (30) c
            // 색인 (31)
            // 목록전거 (32)
            // 문학형식 (33) v
            // 전기 (34) v
            // 언어 (35-37) v
            // 한국정부기관부호 (38-39)
            #endregion
        }

        /// <summary>
        /// 발행년유형 / 발행년1 / 발행년2 를 260c태그로 확인.
        /// </summary>
        /// <param name="ContentTag">260c의 태그내용</param>
        /// <returns></returns>
        private string pubDateSet(string ContentTag)
        {
            string Result = "";

            string pubDate = Regex.Replace(ContentTag, @"[^0-9]", "");

            if (pubDate.Length < 3) {
                MessageBox.Show("260c가 인식되지않습니다.");
                return "false";
            }
            else if (pubDate.Length < 5)
                Result = "s" + pubDate + "    ";
            else
                Result = "m" + pubDate;

            return Result;
        }

        /// <summary>
        /// 발행국명 을 260a태그로 확인.
        /// </summary>
        /// <param name="ContentTag">260a의 태그내용</param>
        /// <returns></returns>
        private string pubCountry(string ContentTag)
        {
            string Result = "";

            ContentTag = ContentTag.Replace("[", "").Replace("]", "").Trim();

            // string cmd = db.DB_Select_Search("`Code`", "Tag008_Country", "Area", ContentTag);
            string cmd = string.Format(
                "SELECT `Code` FROM `Tag008_Country` WHERE `Area` <= \"{0}\" ORDER BY `Area` DESC LIMIT 1;", ContentTag);
            string res = db.DB_Send_CMD_Search(cmd).Replace("|", "");

            if (res == "") {
                MessageBox.Show("260a가 인식되지않습니다.");
                return "false";
            }
            else if (res.Length < 3)
                Result = res + " ";
            else
                Result = res;

            return Result;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (text008.Text.Length < 3) { return; }

            string text = text008.Text;
            string comboName = ((ComboBox)sender).Name;
            int comboIdx = ((ComboBox)sender).SelectedIndex;

            text008.Text = tag008.Combo_Change(text, comboName, comboIdx);
            Apply_Main_marc();
        }

        private void col008res_TextChanged(object sender, EventArgs e)
        {
            if (text008.Text.Length < 3) { return; }
            string text = text008.Text;
            string iText = ((Label)sender).Text;
            if (iText.Length < 1) { return; }
            char[] textArray = text.ToCharArray();
            char[] inputArray = iText.ToCharArray();

            switch (((Label)sender).Name)
            {
                case "col008res":
                    textArray[26] = inputArray[0];
                    textArray[27] = inputArray[1];
                    break;
                case "gov008res":
                    textArray[38] = inputArray[0];
                    textArray[39] = inputArray[1];
                    break;
            }
            text = new string(textArray);
            text008.Text = text;
            Apply_Main_marc();
        }

        private void input_date_ValueChanged(object sender, EventArgs e)
        {
            if (text008.Text.Length < 3) { return; }
            string date = input_date.Value.ToString();
            date = date.Substring(0, 10);
            date = date.Replace("-", "");
            date = date.Remove(0, 2);

            text008.Text = text008.Text.Remove(0, 6);
            text008.Text = date + text008.Text;

            Apply_Main_marc();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            string name = ((CheckBox)sender).Name;
            if (((CheckBox)sender).Checked == true)
            {
                switch (name)
                {
                    case "checkBox1":
                        ((CheckBox)sender).Text = "회의간행물o";
                        break;
                    case "checkBox2":
                        ((CheckBox)sender).Text = "기념논문집o";
                        break;
                }
            }
            else if (((CheckBox)sender).Checked == false)
            {
                switch (name)
                {
                    case "checkBox1":
                        ((CheckBox)sender).Text = "회의간행물x";
                        break;
                    case "checkBox2":
                        ((CheckBox)sender).Text = "기념논문집x";
                        break;
                }
            }
            switch (name)
            {
                case "checkBox1":
                    Publication(checkBox1.Checked, 29);
                    break;
                case "checkBox2":
                    Publication(checkBox2.Checked, 30);
                    break;
            }
        }

        void Publication(bool check, int idx)
        {
            if (text008.Text == "") { return; }
            char[] ArrayChar = text008.Text.ToCharArray();
            if (check == false) { ArrayChar[idx] = '0'; }
            else if (check == true) { ArrayChar[idx] = '1'; }
            text008.Text = new string(ArrayChar);
            Apply_Main_marc();
        }

        /// <summary>
        /// 008text가 메인 텍스트박스에 적용됨.
        /// </summary>
        private void Apply_Main_marc()
        {
            string text = text008.Text;
            richTextBox1.Text = richTextBox1.Text.Replace(data008, text);
            data008 = text;
        }
        #endregion

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            int row = List_Book.CurrentCell.RowIndex;

            Zoom_Picture zp = new Zoom_Picture();
            zp.url = pictureBox1.ImageLocation;
            zp.ISBN = List_Book.Rows[row].Cells["ISBN13"].Value.ToString();
            zp.Show();
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            if (List_Book.RowCount < 0) return;

            Search_ReSet();

            bool isSort = rb_Sort.Checked;
            int combo = comboBox8.SelectedIndex;        // 0: 등급 / 1: 복본

            if (isSort) {
                if (combo == 0) {
                    List_Book.Sort(List_Book.Columns["grade"], System.ComponentModel.ListSortDirection.Ascending);
                }
                else {
                    List_Book.Sort(List_Book.Columns["ISBN13"], System.ComponentModel.ListSortDirection.Ascending);
                }
            }
            else {
                int comboIdx;
                if (combo == 0) {
                    comboIdx = comboBox9.SelectedIndex;
                    Search_Filter("grade", comboIdx);
                }
                else {      // 수정필요

                }
            }
        }
        #region Search_Click_Sub
        private void Search_ReSet()
        {
            DataGridViewBand reSet;
            for (int a = 0; a < List_Book.RowCount; a++)
            {
                reSet = List_Book.Rows[a];
                reSet.Visible = true;
            }
        }

        private void Search_Filter(string target, int comboIdx)
        {
            int count = List_Book.Rows.Count;
            DataGridViewBand band;
            if (comboIdx == 0) {
                for (int a = 0; a < count; a++)
                {
                    band = List_Book.Rows[a];
                    band.Visible = true;
                }
                return;
            }

            comboIdx--;
            for (int a = 0; a < count; a++)
            {
                if (List_Book.Rows[a].Cells[target].Value.ToString() != comboIdx.ToString()) {
                    band = List_Book.Rows[a];
                    band.Visible = false;
                }
            }
        }
        #endregion

        private void btn_CopySelect_Click(object sender, EventArgs e)
        {
            int row = List_Book.CurrentCell.RowIndex;
            string isbn = List_Book.Rows[row].Cells["ISBN13"].Value.ToString();

            MarcCopySelect copySelect = new MarcCopySelect(this);
            copySelect.MarcFormRowIndex = row;
            copySelect.Init("isbn", isbn);
            copySelect.Show();
        }

        /// <summary>
        /// 선택된 마크에 대한 정보를 그리드뷰에 저장.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="GridData">[0] idx, [1] compidx, [2] user, [3] date, [4] grade, [5] tag008, [6] marc </param>
        public void SelectMarc_Sub(int row, string[] GridData)
        {
            List_Book.Rows[row].Cells["marc_idx"].Value = GridData[0];
            List_Book.Rows[row].Cells["user"].Value = GridData[2];
            List_Book.Rows[row].Cells["SaveDate"].Value = GridData[3];
            List_Book.Rows[row].Cells["grade"].Value = GridData[4];
            text008.Text = GridData[5];
            List_Book.Rows[row].Cells["db_marc"].Value = GridData[6];

            List_Book.Rows[row].DefaultCellStyle.ForeColor= SetGradeColor(GridData[4]);
            List_Book.Rows[row].DefaultCellStyle.BackColor = Color.Yellow;

            bool checkMarc = click_Marc(row);
            if (checkMarc)
            {
                ReadRemark(row);
                OnSaveData(row);
                st.Color_change("▼", richTextBox1);
                st.Color_change("▲", richTextBox1);
            }
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox9.Items.Clear();

            ComboBox cb = sender as ComboBox;
            if (cb.SelectedIndex == 0) {
                comboBox9.Enabled = true;
                string[] grade = { "전체", "A", "B", "C", "D" };
                comboBox9.Items.AddRange(grade);
                comboBox9.SelectedIndex = 0;
            }
            else
                comboBox9.Enabled = false;
        }

        private void Radio_Sort_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            string text = rb.Text;
            if (text == "정렬")
                comboBox9.Enabled = false;

            else
                comboBox9.Enabled = true;
        }


        private void btn_mk_marcList_Click(object sender, EventArgs e)
        {
            Marc_mkList mkList = new Marc_mkList(this);
            mkList.Show();
        }
        #region 마크생성목록_Sub
        public bool Check_BackColor(int row)
        {
            if (List_Book.Rows[row].DefaultCellStyle.ForeColor != Color.Red)
                return true;

            return false;
        }
        public bool Check_List_V(int row)
        {
            if (List_Book.Rows[row].Cells["colCheck"].Value.ToString() == "V")
                return true;

            return false;
        }
        #endregion

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            bool isCheck = checkBox3.Checked;
            if (isCheck)
            {
                for (int a = 0; a < List_Book.Rows.Count; a++)
                {
                    List_Book.Rows[a].Cells["colCheck"].Value = "V";
                }
            }
            else
            {
                for (int a = 0; a < List_Book.Rows.Count; a++)
                {
                    List_Book.Rows[a].Cells["colCheck"].Value = "";
                }
            }
        }

        private void List_Book_KeyDown(object sender, KeyEventArgs e)
        {
            int row = List_Book.CurrentCell.RowIndex;
            if (e.KeyCode == Keys.Space)
            {
                if (List_Book.Rows[row].Cells["colCheck"].Value.ToString() == "V")
                    List_Book.Rows[row].Cells["colCheck"].Value = "";

                else
                    List_Book.Rows[row].Cells["colCheck"].Value = "V";
            }
        }

        private void FillTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (e.KeyCode == Keys.F3)
            {
                int line = tb.SelectionStart;
                tb.Text = tb.Text.Insert(line, "▽");
                tb.SelectionStart = line + 1;
            }
        }


        #region DataGridView 드래그 행이동 이벤트 함수

        Skill_Grid sg = new Skill_Grid();

        private void List_Book_MouseMove(object sender, MouseEventArgs e)
        {
            sg.MouseMove(sender, e);
        }

        private void List_Book_MouseDown(object sender, MouseEventArgs e)
        {
            sg.MouseDown(sender, e);
        }

        private void List_Book_DragOver(object sender, DragEventArgs e)
        {
            sg.DragOver(sender, e);
        }

        private void List_Book_DragDrop(object sender, DragEventArgs e)
        {
            sg.DragDrop(sender, e);
        }

        #endregion

        private void btn_FillBlank_Click(object sender, EventArgs e)
        {
            // tabControl1.SelectedIndex = 1;
            int row = List_Book.CurrentRow.Index;
            string ISBN = List_Book.Rows[row].Cells["ISBN13"].Value.ToString();

            if (ISBN == "" || ISBN == null)
            {
                MessageBox.Show("ISBN이 존재하지않습니다!");
                return;
            }

            UniMarc.Marc_FillBlank fb = new UniMarc.Marc_FillBlank(this);
            for (int a = 0; a < List_Book.Rows.Count; a++)
            {
                if (List_Book.Rows[a].DefaultCellStyle.ForeColor == Color.Red)
                {
                    string[] GridData =
                    {
                        a.ToString(),
                        List_Book.Rows[a].Cells["ISBN13"].Value.ToString(),
                        List_Book.Rows[a].Cells["book_name"].Value.ToString(),
                        List_Book.Rows[a].Cells["author"].Value.ToString(),
                        List_Book.Rows[a].Cells["book_comp"].Value.ToString(),
                        List_Book.Rows[a].Cells["pay"].Value.ToString(),
                        ""
                    };
                    fb.InitFillBlank(GridData);
                }
            }
            fb.ISBN = ISBN;
            fb.Show();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TabIndex = tabControl1.SelectedIndex;

            // 종단기호 제거
            string Text = richTextBox1.Text.Replace("▲", "");

            string[] SplitLine = Text.Split('\n');

            switch (TabIndex)
            {
                case 0:     // 칸채우기 -> 메모장
                    InputMemo(SplitLine);
                    st.Color_change("▼", richTextBox1);
                    st.Color_change("▲", richTextBox1);
                    break;
                case 1:     // 메모장 -> 칸채우기
                    // 칸을 채우기 전에 텍스트박스와 그리드뷰를 비움.
                    TextReset();
                    foreach (string Line in SplitLine)
                    {
                        if (Line == "") break;

                        // [0]:태그번호, [1]:지시기호, [2]:마크내용
                        string[] SplitTag = Line.Split('\t');
                        InputTotal(SplitTag);
                    }
                    break;
            }
        }

        #region 칸채우기 SUB

        public void TextReset()
        {
            TextBox[] tb = {
                textDDC21, textDDC22, textDDC23, textKDC4, textKDC5, textKDC6,
                text041a, text041b, text041h, text041k,

                basicHeadBox,

                text245a, text245b, text245e, text245n, text245p, text245d, text245x,

                text250a,

                text260a, text260b, text260c, text260g,

                text300a, text300b, text300c1, text300c2, text300e,

                text500a, text504a, text505a, text507a, text507t,
                text520a, text521a, text525a, text536a, text546a,
                text586a, text650a, text653a, text700a, text710a,
                text900a, text910a, text940a
            };
            DataGridView[] dgv = {
                GridView020, GridView246, GridView440, GridView490, GridView505
            };

            foreach (TextBox box in tb)
            {
                box.Text = "";
            }
            foreach (DataGridView grid in dgv)
            {
                grid.Rows.Clear();

                grid.Rows.Add(50);

                if (grid.Name.IndexOf("505") > 0)
                    grid.Rows.Add(50);
            }
        }

        /// <summary>
        /// 칸채우기에 값을 집어넣는 함수
        /// </summary>
        /// <param name="SplitTag">[0]:태그번호, [1]:지시기호, [2]:마크내용</param>
        public void InputTotal(string[] SplitTag)
        {
            string TagNum = SplitTag[0];
            string Jisi = SplitTag[1];
            string Marc = SplitTag[2];

            if (TagNum == "020") Grid020(Marc);
            if (TagNum == "041") Text041(Marc);
            if (TagNum == "056") Text056(Marc);
            if (TagNum == "082") Text082(Marc);
            if (TagNum == "100" || TagNum == "110" || TagNum == "111") Text100(Marc, TagNum);
            if (TagNum == "245") Text245(Marc);
            if (TagNum == "246") Grid246(Jisi, Marc);
            if (TagNum == "250") InputOneTextBox(text250a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "260") Text260(Marc);
            if (TagNum == "300") Text300(Marc);
            if (TagNum == "440") Grid440(Marc);
            if (TagNum == "490") Grid490(Marc);
            if (TagNum == "500") InputOneTextBox(text500a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "504") InputOneTextBox(text504a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "505") InputOneTextBox(text505a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "505") Grid505(Marc);
            if (TagNum == "507") Text507(Marc);
            if (TagNum == "520") InputOneTextBox(text520a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "521") InputOneTextBox(text521a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "525") InputOneTextBox(text525a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "536") InputOneTextBox(text536a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "546") InputOneTextBox(text546a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "586") InputOneTextBox(text586a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "650") InputOneTextBox(text650a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "653") InputOneTextBox(text653a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "700") InputOneTextBox(text700a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "710") Text710And910(Marc, TagNum);
            if (TagNum == "900") InputOneTextBox(text900a, GetMiddelString(Marc, "▼a", "▼"));
            if (TagNum == "910") Text710And910(Marc, TagNum);
            if (TagNum == "940") InputOneTextBox(text940a, GetMiddelString(Marc, "▼a", "▼"));
        }

        #region Grid_Input

        /// <summary>
        /// 020Grid에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag"></param>
        private void Grid020(string SplitTag)
        {
            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string g = GetMiddelString(SplitTag, "▼g", "▼");
            string c = GetMiddelString(SplitTag, "▼c", "▼");
            object[] Grid = { false, a, g, c };

            if (a.Contains("세트"))
                Grid[0] = true;

            for (int count = 0; count < GridView020.Rows.Count; count++)
            {
                if (GridView020.Rows[count].Cells["CheckSet"].Value == null && 
                    GridView020.Rows[count].Cells["Text020a"].Value == null &&
                    GridView020.Rows[count].Cells["Text020g"].Value == null &&
                    GridView020.Rows[count].Cells["Text020c"].Value == null) {

                    GridView020.Rows[count].Cells["CheckSet"].Value = Grid[0];
                    GridView020.Rows[count].Cells["Text020a"].Value = Grid[1];
                    GridView020.Rows[count].Cells["Text020g"].Value = Grid[2];
                    GridView020.Rows[count].Cells["Text020c"].Value = Grid[3].ToString().Replace(@"\", "").Replace("￦", "");
                    break;
                }
            }

            //GridView020.Rows.Add(Grid);
        }

        /// <summary>
        /// 246Grid에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag"></param>
        private void Grid246(string Jisi, string SplitTag)
        {
            string i = GetMiddelString(SplitTag, "▼i", "▼");
            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string b = GetMiddelString(SplitTag, "▼b", "▼");
            string n = GetMiddelString(SplitTag, "▼n", "▼");
            string p = GetMiddelString(SplitTag, "▼p", "▼");

            object[] Grid = { Jisi, i, a, b, n, p };
            for (int count = 0; count < GridView246.Rows.Count; count++)
            {
                if (GridView246.Rows[count].Cells["Text246Jisi"].Value == null &&
                    GridView246.Rows[count].Cells["Text246i"].Value == null &&
                    GridView246.Rows[count].Cells["Text246a"].Value == null &&
                    GridView246.Rows[count].Cells["Text246b"].Value == null &&
                    GridView246.Rows[count].Cells["Text246n"].Value == null &&
                    GridView246.Rows[count].Cells["Text246p"].Value == null)
                {

                    GridView246.Rows[count].Cells["Text246Jisi"].Value = Grid[0];
                    GridView246.Rows[count].Cells["Text246i"].Value = Grid[1];
                    GridView246.Rows[count].Cells["Text246a"].Value = Grid[2];
                    GridView246.Rows[count].Cells["Text246b"].Value = Grid[3];
                    GridView246.Rows[count].Cells["Text246n"].Value = Grid[4];
                    GridView246.Rows[count].Cells["Text246p"].Value = Grid[5];
                    break;
                }
            }

            //GridView246.Rows.Add(Grid);
        }

        /// <summary>
        /// 440Grid에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag"></param>
        private void Grid440(string SplitTag)
        {
            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string n = GetMiddelString(SplitTag, "▼n", "▼");
            string p = GetMiddelString(SplitTag, "▼p", "▼");
            string v = GetMiddelString(SplitTag, "▼v", "▼");
            string x = GetMiddelString(SplitTag, "▼x", "▼");

            // string vNum = System.Text.RegularExpressions.Regex.Replace(v, @"[^0-9]", "");
            // string vTxt = System.Text.RegularExpressions.Regex.Replace(v, @"\d", "");

            object[] Grid = { a, n, p, v, "", x };
            for (int count = 0; count < GridView440.Rows.Count; count++)
            {
                if (GridView440.Rows[count].Cells["text440a"].Value == null &&
                    GridView440.Rows[count].Cells["text440n"].Value == null &&
                    GridView440.Rows[count].Cells["text440p"].Value == null &&
                    GridView440.Rows[count].Cells["text440vNum"].Value == null &&
                    GridView440.Rows[count].Cells["text440vTxt"].Value == null &&
                    GridView440.Rows[count].Cells["text440x"].Value == null)
                {
                    
                    GridView440.Rows[count].Cells["text440a"].Value = Grid[0];
                    GridView440.Rows[count].Cells["text440n"].Value = Grid[1];
                    GridView440.Rows[count].Cells["text440p"].Value = Grid[2];
                    GridView440.Rows[count].Cells["text440vNum"].Value = Grid[3];
                    GridView440.Rows[count].Cells["text440vTxt"].Value = Grid[4];
                    GridView440.Rows[count].Cells["text440x"].Value = Grid[5];
                    break;
                }
            }

            //GridView440.Rows.Add(Grid);
        }

        /// <summary>
        /// 490Grid에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag"></param>
        private void Grid490(string SplitTag)
        {
            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string v = GetMiddelString(SplitTag, "▼v", "▼");

            object[] Grid = { a, v };
            for (int count = 0; count < GridView490.Rows.Count; count++)
            {
                if (GridView490.Rows[count].Cells["text490a"].Value == null &&
                    GridView490.Rows[count].Cells["text490v"].Value == null)
                {
                    GridView490.Rows[count].Cells["text490a"].Value = Grid[0];
                    GridView490.Rows[count].Cells["text490v"].Value = Grid[1];
                    break;
                }
            }

            //GridView490.Rows.Add(Grid);
        }

        /// <summary>
        /// 505Grid에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag"></param>
        private void Grid505(string SplitTag)
        {
            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string n = GetMiddelString(SplitTag, "▼n", "▼");
            string t = GetMiddelString(SplitTag, "▼t", "▼");
            string d = GetMiddelString(SplitTag, "▼d", "▼");
            string e = GetMiddelString(SplitTag, "▼e", "▼");

            // object[] Grid = { n, t, d, e };
            // for (int count = 0; count < GridView505.Rows.Count; count++)
            // {
            //     if (GridView505.Rows[count].Cells["text505n"].Value == null &&
            //         GridView505.Rows[count].Cells["text505t"].Value == null &&
            //         GridView505.Rows[count].Cells["text505d"].Value == null &&
            //         GridView505.Rows[count].Cells["text505e"].Value == null)
            //     {
            // 
            //         GridView505.Rows[count].Cells["text505n"].Value = Grid[0];
            //         GridView505.Rows[count].Cells["text505t"].Value = Grid[1];
            //         GridView505.Rows[count].Cells["text505d"].Value = Grid[2];
            //         GridView505.Rows[count].Cells["text505e"].Value = Grid[3];
            //         break;
            //     }
            // }
            //GridView505.Rows.Add(Grid);

            if (a != "")
                text505a.Text = a;
            else
                InputGrid505(SplitTag);
        }


        private void InputGrid505(string Tag505)
        {
            string[] Split505 = Tag505.Split('▼');

            string[] grid = { "", "", "", "" };     // n t d e
            string[] clear = { "", "", "", "" };
            char[] Symbol = { 'n', 't', 'd', 'e' };
            int row = 0;
            int Before = -1;
            int After = -1;

            foreach (string Tag in Split505)
            {
                if (Tag == "") continue;
                Before = After;
                for (int a = 0; a < Symbol.Length; a++)
                {
                    if (Tag[0] == Symbol[a]) {
                        After = a;
                        break;
                    }
                }

                if (Before >= After)
                    row++;

                GridView505.Rows[row].Cells[After].Value = Tag.TrimStart(Symbol);
            }

        }
        #endregion

        #region Text_Input

        /// <summary>
        /// 041Text에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag">마크데이터</param>
        private void Text041(string SplitTag)
        {
            TextBox[] Text = {
                text041a, text041k, text041h, text041b
            };

            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string k = GetMiddelString(SplitTag, "▼k", "▼");
            string h = GetMiddelString(SplitTag, "▼h", "▼");
            string b = GetMiddelString(SplitTag, "▼b", "▼");

            string[] Marc = { a, k, h, b };

            InputMoreTextBox(Text, Marc);
        }

        /// <summary>
        /// 100Text에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag">마크데이터</param>
        private void Text100(string SplitTag, string TagNum)
        {
            TextBox Text = basicHeadBox;

            RadioButton[] rbtn = { rbtn_100, rbtn_110, rbtn_111 };

            if (TagNum == "100")
                rbtn[0].Checked = true;
            if (TagNum == "110")
                rbtn[1].Checked = true;
            if (TagNum == "111")
                rbtn[2].Checked = true;

            string a = GetMiddelString(SplitTag, "▼a", "▼");

            InputOneTextBox(Text, a);
        }

        /// <summary>
        /// 056Text에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag"></param>
        private void Text056(string SplitTag)
        {
            TextBox[] Text = {
                textKDC4, textKDC5, textKDC6
            };

            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string num = GetMiddelString(SplitTag, "▼2", "▼");

            int TextCount;
            switch (num)
            {
                case "4":
                    TextCount = 0;
                    break;
                case "5":
                    TextCount = 1;
                    break;
                case "6":
                    TextCount = 2;
                    break;
                default:
                    TextCount = 1;
                    break;
            }

            Text[TextCount].Text = a;
        }

        /// <summary>
        /// 082Text에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag"></param>
        private void Text082(string SplitTag)
        {
            TextBox[] Text = {
                textDDC21, textDDC22, textDDC23
            };

            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string num = GetMiddelString(SplitTag, "▼2", "▼");

            int TextCount;
            switch (num)
            {
                case "21":
                    TextCount = 0;
                    break;
                case "22":
                    TextCount = 1;
                    break;
                case "23":
                    TextCount = 2;
                    break;
                default:
                    TextCount = 1;
                    break;
            }

            Text[TextCount].Text = a;
        }

        /// <summary>
        /// 245Text에 적용하는 함수
        /// </summary>
        /// <param name="SplitTag"></param>
        private void Text245(string SplitTag)
        {
            TextBox[] Text = {
                text245a, text245b, text245x, text245n, text245p,
                text245d, text245e
            };

            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string b = GetMiddelString(SplitTag, "▼b", "▼");
            string x = GetMiddelString(SplitTag, "▼x", "▼");
            string n = GetMiddelString(SplitTag, "▼n", "▼");
            string p = GetMiddelString(SplitTag, "▼p", "▼");
            string d = GetMiddelString(SplitTag, "▼d", "▼");
            string e = GetMiddelString(SplitTag, "▼e", "▼");

            string[] Marc = {
                a, b, x, n, p,
                d, e
            };

            InputMoreTextBox(Text, Marc);
        }

        private void Text260(string SplitTag)
        {
            TextBox[] Text = {
                text260a, text260b, text260c, text260g
            };

            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string b = GetMiddelString(SplitTag, "▼b", "▼");
            string c = GetMiddelString(SplitTag, "▼c", "▼");
            string g = GetMiddelString(SplitTag, "▼g", "▼");

            string[] Marc = { a, b, c, g };

            InputMoreTextBox(Text, Marc);
        }

        private void Text300(string SplitTag)
        {
            TextBox[] Text = {
                text300a, text300b, text300c1, text300c2, text300e
            };

            string a = GetMiddelString(SplitTag, "▼a", "▼", "300").Replace(" p.", "").Replace("p.", "");
            string b = GetMiddelString(SplitTag, "▼b", "▼", "300");
            string c = GetMiddelString(SplitTag, "▼c", "▼");
            string e = GetMiddelString(SplitTag, "▼e", "▼");

            string[] size = { "", "" };
            string[] size_tmp = c.ToUpper().Split('X');

            size[0] = Regex.Replace(size_tmp[0], @"[^0-9]", "");
            if (size_tmp.Length > 1)
                size[1] = Regex.Replace(size_tmp[1], @"[^0-9]", "");

            string[] Marc = { a, b, size[0], size[1], e };

            InputMoreTextBox(Text, Marc);
        }

        private void Text507(string SplitTag)
        {
            TextBox[] Text = {
                text507a, text507t
            };

            string a = GetMiddelString(SplitTag, "▼a", "▼");
            string t = GetMiddelString(SplitTag, "▼t", "▼");

            string[] Marc = { a, t };

            InputMoreTextBox(Text, Marc);
        }

        private void Text710And910(string SplitTag, string TagNum)
        {
            TextBox[] box = { text710a, text910a };

            string a = GetMiddelString(SplitTag, "▼a", "▲", TagNum);

            a = a.Replace(".▼", "▼");
            a = a.Replace("▼b", "@");

            // int count = a.IndexOf("▼");
            // if (count > -1)
            // {
            //     a = a.Remove(count, 2);
            //     if (a[count - 1] != '.')
            //         a = a.Insert(count, ".");
            // }

            if (TagNum == "710")
                InputOneTextBox(box[0], a);

            if (TagNum == "910")
                InputOneTextBox(box[1], a);
        }

        /// <summary>
        /// 여러 텍스트박스에 값을 입력.
        /// </summary>
        /// <param name="Box">텍스트박스 배열</param>
        /// <param name="Marc">텍스트박스에 들어갈 값 배열</param>
        private void InputMoreTextBox(TextBox[] box, string[] Marc)
        {
            int count = 0;

            foreach (string marc in Marc)
            {
                if (box[count].Text == "")
                    box[count].Text = marc;

                else
                    box[count].Text += "▽" + marc;

                count++;
            }
        }

        /// <summary>
        /// 단일 텍스트박스에 값을 입력
        /// </summary>
        /// <param name="box">입력할 텍스트박스</param>
        /// <param name="marc">텍스트박스에 들어갈 값</param>
        private void InputOneTextBox(TextBox box, string marc)
        {
            if (box.Text == "")
            {
                box.Text = marc;
            }
            else
            {
                box.Text += "▽" + marc;
            }
        }
        #endregion


        /// <summary>
        /// 문자와 문자사이의 값 가져오기
        /// </summary>
        /// <param name="str">대상 문자열</param>
        /// <param name="begin">시작 문자열</param>
        /// <param name="end">마지막 문자열</param>
        /// <param name="TagNum">불러올 태그 번호</param>
        /// <returns>문자 사이값</returns>
        public string GetMiddelString(string str, string begin, string end, string TagNum = "")
        {
            string result = "";

            if (string.IsNullOrEmpty(str) || str == "")
                return result;

            int count = 0;
            bool loop = false;

            for (int a = count; a < str.Length; a++)
            {
                count = str.IndexOf(begin);
                if (count > -1)
                {
                    str = str.Substring(count + begin.Length);
                    if (loop)
                        // 여러 태그들 구분을 지어줌.
                        result += "▽";

                    if (str.IndexOf(end) > -1)
                        result += str.Substring(0, str.IndexOf(end));
                    else
                        result += str;

                    result = TrimEndGubun(result, TagNum);
                }
                else
                    break;

                loop = true;
            }

            return result;
        }

        string TrimEndGubun(string str, string TagNum)
        {
            
            if (TagNum == "300")
            {
                str = str.Trim();
                if (str.Contains("ill."))
                    return str;
                if (str.Contains("p."))
                    return str;
            }

            if (TagNum == "710" || TagNum == "910")
                return str;

            char[] gu = { '.', ',', ':', ';', '/' };

            foreach (char gubun in gu)
            {
                if (str.Length < 1) continue;
                if (str[str.Length - 1] == gubun)
                {
                    str = str.Remove(str.Length - 1);
                    str = str.Trim();
                }
            }

            return str;
        }
        #endregion

        #region 메모장 채우기 SUB

        /// <summary>
        /// 메모장 채우기
        /// </summary>
        /// <param name="SplitTag">메모장에 있는 값<br>[0]:태그번호, [1]:지시기호, [2]:마크내용</br></param>
        private void InputMemo(string[] SplitTag)
        {
            string[] Tag =
            {
                "020", "041", "056", "082", "100",
                "110", "111", "245", "246", "250",
                "260", "300", "440", "490", "500",
                "504", "505", "507", "520", "521",
                "525", "536", "546", "586", "650",
                "653", "700", "710", "900", "910",
                "940"
            };
            
            List<string> RemainTag = new List<string>();    // 메모장으로 출력되는 최종 리스트

            List<string> BlankTag = MakeMarcLine();         // 칸채우기의 값들이 여기로 저장이 되어 들어옴
            List<string> TextTag = SplitTag.ToList();       // 칸채우기에 없는 값들이 여기로 저장되어있음.
            List<string> AllTag = new List<string>();       // 칸채우기와 나머지 값들을 짱뽕시킨 짬통 리스트
            AllTag.AddRange(BlankTag);
            AllTag.AddRange(TextTag);

            AllTag.Sort();

            foreach (string Content in AllTag)
            {
                if (Content == "")
                    continue;

                if (Content.Length < 3)
                    continue;

                string tagNum = Content.Substring(0, 3);
                bool isCopy = false;

                foreach (string Num in Tag)
                {
                    if (tagNum == Num) {
                        isCopy = true;
                        break;
                    }
                }

                if (isCopy)
                {
                    string AddText = "";

                    foreach (string Blank in BlankTag)
                    {
                        if (Blank.Length < 4)
                            continue;
                        
                        string StrNum = Blank.Substring(0, 3);
                        int Num = Convert.ToInt32(StrNum);
                        int tagNumInt = Convert.ToInt32(tagNum);

                        if (Num == tagNumInt)
                        {
                            AddText = Blank.Replace("\n", "");
                            AddText = AddText.Replace("▲", "▲\n");
                            break;
                        }
                    }
                    if (AddText == "")
                        continue;

                    if (AddText.Substring(AddText.Length - 1, 1) == "\n")
                        AddText = AddText.Substring(0, AddText.Length - 1);

                    RemainTag.Add(AddText);
                }
                else
                {
                    Content.TrimEnd('\n');
                    RemainTag.Add(Content + "▲");
                }
            }

            RemainTag = RemainTag.Distinct().ToList();


            richTextBox1.Text = string.Join("\n", RemainTag) + "\n";
        }

        /// <summary>
        /// 칸채우기에 채워진 값들을 메모장 형식으로 변환하여 반환
        /// </summary>
        /// <returns></returns>
        private List<string> MakeMarcLine()
        {
            List<string> LineMarc = new List<string>();

            string[] Tag = {
                "020", "041", "056", "082", "100",
                "110", "111", "245", "246", "250",
                "260", "300", "440", "490", "500",
                "504", "505", "507", "520", "521",
                "525", "536", "546", "586", "650",
                "653", "700", "710", "900", "910",
                "940"
            };

            string[] GridTag = {
                "020", "246", "440", "490", "505"
            };

            string[] TextTag = {
                "041", "056", "082", "100", "110",
                "111", "245", "250", "260", "300",
                "500", "505", "504", "507", "520",
                "521", "525", "536", "546", "586",
                "650", "653", "700", "710", "900",
                "910", "940"
            };

            foreach (string TagNum in Tag)
            {
                bool isGrid = false;
                bool isText = false;

                foreach (string Grid in GridTag)
                {
                    if (TagNum == Grid)
                    {
                        isGrid = true;
                        break;
                    }
                }
                foreach (string Text in TextTag)
                {
                    if (TagNum == Text)
                    {
                        isText = true;
                        break;
                    }
                }

                if (isGrid)
                    LineMarc.Add(GridMarc(TagNum));

                if (isText)
                    LineMarc.Add(TextMarc(TagNum));
            }

            return LineMarc;
        }

        #region GridInsert부분
        private string GridMarc(string Tag)
        {
            DataGridView[] dgvNum = { GridView020, GridView246, GridView440, GridView490, GridView505 };
            string result = "";

            switch (Tag)
            {
                case "020": result = Grid020Insert(dgvNum[0]); break;
                case "246": result = Grid246Insert(dgvNum[1]); break;
                case "440": result = Grid440Insert(dgvNum[2]); break;
                case "490": result = Grid490Insert(dgvNum[3]); break;
                case "505": result = Grid505Insert(dgvNum[4]); break;
                default:
                    break;
            }

            return result;
        }

        private string Grid020Insert(DataGridView dgv)
        {
            string Insert020Sub(string Check)
            {
                string res = "  ";
                if (Check == "True")
                    res = "1 ";
                return res;
            }
            int count = dgv.Rows.Count - 1;
            string result = "";

            for (int a = 0; a < count; a++)
            {
                if (dgv.Rows[a].Cells["Text020a"].Value == null)
                    break;

                string CheckSet;
                if (dgv.Rows[a].Cells["CheckSet"].Value == null)
                    CheckSet = "";
                else
                    CheckSet = dgv.Rows[a].Cells["CheckSet"].Value.ToString();

                string a020;
                if (dgv.Rows[a].Cells["Text020a"].Value == null)
                    a020 = "";
                else
                    a020 = dgv.Rows[a].Cells["Text020a"].Value.ToString();

                string g020;
                if (dgv.Rows[a].Cells["Text020g"].Value == null)
                    g020 = "";
                else
                    g020 = dgv.Rows[a].Cells["Text020g"].Value.ToString();

                string c020;
                if (dgv.Rows[a].Cells["Text020c"].Value == null)
                    c020 = "";
                else
                    c020 = @"\" + dgv.Rows[a].Cells["Text020c"].Value.ToString();

                result += string.Format("020\t{0}\t▼a{1}", Insert020Sub(CheckSet), a020);

                if (g020 != "")
                    result += "▼g" + g020;

                if (c020.Length > 2)
                    result += "▼c" + c020;

                result += "▲\n";
            }

            return result;
        }

        private string Grid246Insert(DataGridView dgv)
        {
            int count = dgv.Rows.Count - 1;
            string result = "";

            for (int a = 0; a < count; a++)
            {
                if (dgv.Rows[a].Cells[0].Value == null)
                    break;

                string Jisi;
                if (dgv.Rows[a].Cells["Text246Jisi"].Value == null)
                    Jisi = "";
                else
                    Jisi = dgv.Rows[a].Cells["Text246Jisi"].Value.ToString();

                string a246;
                if (dgv.Rows[a].Cells["Text246a"].Value == null)
                    a246 = "";
                else
                    a246 = dgv.Rows[a].Cells["Text246a"].Value.ToString();

                string b246;
                if (dgv.Rows[a].Cells["Text246b"].Value == null)
                    b246 = "";
                else
                    b246 = dgv.Rows[a].Cells["Text246b"].Value.ToString();

                string i246;
                if (dgv.Rows[a].Cells["Text246i"].Value == null)
                    i246 = "";
                else
                    i246 = dgv.Rows[a].Cells["Text246i"].Value.ToString();

                string n246;
                if (dgv.Rows[a].Cells["Text246n"].Value == null)
                    n246 = "";
                else
                    n246 = dgv.Rows[a].Cells["Text246n"].Value.ToString();

                string p246;
                if (dgv.Rows[a].Cells["Text246p"].Value == null)
                    p246 = "";
                else
                    p246 = dgv.Rows[a].Cells["Text246p"].Value.ToString();


                result += string.Format("246\t{0}\t", Jisi);

                if (i246 != "") result += "▼i" + i246;
                if (a246 != "") result += "▼a" + a246;
                if (b246 != "") result += "▼b" + b246;
                if (n246 != "") result += "▼n" + n246;
                if (p246 != "") result += "▼p" + p246;

                result += "▲\n";
            }
            return result;
        }

        private string Grid440Insert(DataGridView dgv)
        {
            int count = dgv.Rows.Count - 1;
            string result = "";

            for (int a = 0; a < count; a++)
            {
                if (dgv.Rows[a].Cells[0].Value == null)
                    break;
                
                for (int b = 0; b < dgv.ColumnCount; b++)
                {
                    if (dgv.Rows[a].Cells[b].Value == null)
                        dgv.Rows[a].Cells[b].Value = "";
                }

                string a440;
                if (dgv.Rows[a].Cells["text440a"].Value == null)
                    a440 = "";
                else
                    a440 = dgv.Rows[a].Cells["text440a"].Value.ToString();

                string n440;
                if (dgv.Rows[a].Cells["text440n"].Value == null)
                    n440 = "";
                else
                    n440 = dgv.Rows[a].Cells["text440n"].Value.ToString();

                string p440;
                if (dgv.Rows[a].Cells["text440p"].Value == null)
                    p440 = "";
                else
                    p440 = dgv.Rows[a].Cells["text440p"].Value.ToString();

                string v440Num;
                if (dgv.Rows[a].Cells["text440vNum"].Value == null)
                    v440Num = "";
                else
                    v440Num = dgv.Rows[a].Cells["text440vNum"].Value.ToString();

                string v440Txt;
                if (dgv.Rows[a].Cells["text440vTxt"].Value == null)
                    v440Txt = "";
                else
                    v440Txt = dgv.Rows[a].Cells["text440vTxt"].Value.ToString();

                string x440;
                if (dgv.Rows[a].Cells["text440x"].Value == null)
                    x440 = "";
                else
                    x440 = dgv.Rows[a].Cells["text440x"].Value.ToString();


                result += string.Format("440\t  \t▼a{0}", a440);

                if (n440 != "") result += "▼n" + n440;
                if (p440 != "") result += "▼p" + p440;
                if (v440Num != "") result += "▼v" + v440Num + v440Txt;
                if (x440 != "") result += "▼x" + x440;

                result += "▲\n";
            }

            return result;
        }

        private string Grid490Insert(DataGridView dgv)
        {
            int count = dgv.Rows.Count - 1;
            string result = "";

            for (int a = 0; a < count; a++)
            {
                if (dgv.Rows[a].Cells[0].Value == null)
                    break;

                string a490;
                if (dgv.Rows[a].Cells["text490a"].Value == null)
                    a490 = "";
                else
                    a490 = dgv.Rows[a].Cells["text490a"].Value.ToString();

                string v490;
                if (dgv.Rows[a].Cells["text490v"].Value == null)
                    v490 = "";
                else
                    v490 = dgv.Rows[a].Cells["text490v"].Value.ToString();

                result += string.Format("490\t  \t▼a{0}", a490);

                if (v490 != "") result += "▼v" + v490;

                result += "▲\n";
            }

            return result;
        }

        private string Grid505Insert(DataGridView dgv)
        {
            int count = dgv.Rows.Count - 1;
            string result = "";

            for (int a = 0; a < count; a++)
            {
                if (dgv.Rows[a].Cells[1].Value == null)
                    break;

                string n505;
                if (dgv.Rows[a].Cells["text505n"].Value == null)
                    n505 = "";
                else
                    n505 = dgv.Rows[a].Cells["text505n"].Value.ToString();

                string t505;
                if (dgv.Rows[a].Cells["text505t"].Value == null)
                    t505 = "";
                else
                    t505 = dgv.Rows[a].Cells["text505t"].Value.ToString();

                string d505;
                if (dgv.Rows[a].Cells["text505d"].Value == null)
                    d505 = "";
                else
                    d505 = dgv.Rows[a].Cells["text505d"].Value.ToString();

                string e505;
                if (dgv.Rows[a].Cells["text505e"].Value == null)
                    e505 = "";
                else
                    e505 = dgv.Rows[a].Cells["text505e"].Value.ToString();

                if (n505 != "") result += "▼n" + n505;
                if (t505 != "") result += "▼t" + t505;
                if (d505 != "") result += "▼d" + d505;
                if (e505 != "") result += "▼e" + e505;
            }

            if (result != "")
                result = string.Format("505\t 0\t") + result + "▲\n";

            return result;
        }

        #endregion

        #region TextInsert부분

        private string TextMarc(string tag)
        {
            string result = "";
            //"▼" "▲";

            switch (tag)
            {
                case "041": result = Text041Insert(); break;
                case "056": result = Text056Insert(); break;
                case "082": result = Text082Insert(); break;
                case "100": result = Text100Insert(tag); break;
                case "110": result = Text100Insert(tag); break;
                case "111": result = Text100Insert(tag); break;
                case "245": result = Text245Insert(); break;
                case "250": result = Text250Insert(); break;
                case "260": result = Text260Insert(); break;
                case "300": result = Text300Insert(); break;
                case "500": result = Text500Insert(); break;
                case "504": result = Text504Insert(); break;
                case "505": result = Text505Insert(); break;
                case "507": result = Text507Insert(); break;
                case "520": result = Text520Insert(); break;
                case "521": result = Text521Insert(); break;
                case "525": result = Text525Insert(); break;
                case "536": result = Text536Insert(); break;
                case "546": result = Text546Insert(); break;
                case "586": result = Text586Insert(); break;
                case "650": result = Text650Insert(); break;
                case "653": result = Text653Insert(); break;
                case "700": result = Text700Insert(); break;
                case "710": result = Text710Insert(); break;
                case "900": result = Text900Insert(); break;
                case "910": result = Text910Insert(); break;
                case "940": result = Text940Insert(); break;
            }

            return result;
        }

        private string Text041Insert()
        {
            string[] boxText = { text041a.Text, text041b.Text, text041h.Text, text041k.Text };

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = "041\t  \t";

            if (boxText[0] != "") result += "▼a" + boxText[0].Replace("▽", "▼a");
            if (boxText[1] != "") result += "▼b" + boxText[1];
            if (boxText[2] != "") result += "▼h" + boxText[2].Replace("▽", "▼h");
            if (boxText[3] != "") result += "▼k" + boxText[3];

            result += "▲";

            return result;
        }

        private string Text056Insert()
        {
            string[] boxText = { textKDC4.Text, textKDC5.Text, textKDC6.Text };

            bool isText = isEmpty(boxText);
            string result = "";
            if (!isText)
                return "";

            if (boxText[0] != "")
                result += string.Format("056\t  \t▼a{0}▼2{1}▲", boxText[0], "4");
            if (boxText[1] != "")
                result += string.Format("056\t  \t▼a{0}▼2{1}▲", boxText[1], "5");
            if (boxText[2] != "")
                result += string.Format("056\t  \t▼a{0}▼2{1}▲", boxText[2], "6");

            return result;
        }

        private string Text082Insert()
        {
            string[] boxText = { textDDC21.Text, textDDC22.Text, textDDC23.Text };

            bool isText = isEmpty(boxText);
            string result = "";
            if (!isText)
                return "";

            if (boxText[0] != "")
                result += string.Format("082\t  \t▼a{0}▼2{1}▲", boxText[0], "21");
            if (boxText[1] != "")
                result += string.Format("082\t  \t▼a{0}▼2{1}▲", boxText[1], "22");
            if (boxText[2] != "")
                result += string.Format("082\t  \t▼a{0}▼2{1}▲", boxText[2], "23");

            return result;
        }

        private string Text100Insert(string TagNum)
        {
            string boxText = basicHeadBox.Text;

            bool isText;
            if (boxText == "")
                isText = false;
            else
                isText = true;

            if (!isText)
                return "";

            string result = "";

            string[] splitText = boxText.Split('▽');

            if (TagNum == "100" && rbtn_100.Checked)
                result = string.Format("{0}\t  \t▼a{1}", TagNum, splitText[0]);
            if (TagNum == "110" && rbtn_110.Checked)
                result = string.Format("{0}\t  \t▼a{1}", TagNum, splitText[0]);
            if (TagNum == "111" && rbtn_111.Checked)
                result = string.Format("{0}\t  \t▼a{1}", TagNum, splitText[0]);

            result += "▲";

            return result;
        }

        private string Text245Insert()
        {
            string[] boxText = {
                text245a.Text, text245n.Text, text245p.Text, text245b.Text, text245x.Text , text245d.Text, text245e.Text };

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = "245\t  \t";

            if (boxText[0] != "") result += "▼a" + boxText[0];
            if (boxText[1] != "") result += "▼n" + boxText[1];
            if (boxText[3] != "") {
                string[] splitText245b = boxText[3].Split('▽');
                foreach (string text in splitText245b)
                    result += "▼b" + text;

            }
            if (boxText[2] != "") result += "▼p" + boxText[2];
            if (boxText[4] != "") result += "▼x" + boxText[4];
            if (boxText[5] != "") result += "▼d" + boxText[5];
            if (boxText[6] != "") result += "▼e" + boxText[6].Replace("▽", "▼e");

            result += "▲";

            return result;
        }

        private string Text250Insert()
        {
            string boxText = text250a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("250\t  \t▼a{0}", boxText);

            result += "▲";

            return result;
        }

        private string Text260Insert()
        {
            string[] boxText = {
                text260a.Text, text260b.Text, text260c.Text, text260g.Text };

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = "260\t  \t";

            if (boxText[0] != "") result += "▼a" + boxText[0];
            if (boxText[1] != "") result += "▼b" + boxText[1].Replace("▽", "▼b"); ;
            if (boxText[2] != "") result += "▼c" + boxText[2];
            if (boxText[3] != "") result += "▼g" + boxText[3];

            result += "▲";

            return result;
        }

        private string Text300Insert()
        {
            string[] boxText = {
                text300a.Text, text300b.Text, text300c1.Text, text300c2.Text, text300e.Text };

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = "300\t  \t";

            if (boxText[0] != "") result += "▼a" + Tag300a_Sub(boxText[0]);
            if (boxText[1] != "") result += "▼b" + boxText[1];
            if (boxText[2] != "") {
                result += "▼c" + boxText[2];
                if (boxText[3] == "") result += "cm";
            }
            if (boxText[3] != "") result += " x " + boxText[3] + "cm";
            if (boxText[4] != "") result += "▼e" + boxText[4];

            result += "▲";

            return result;
        }
        #region Tag300a_Sub

        string Tag300a_Sub(string Text)
        {
            string result = Text;

            if (result.IndexOf(", ") > -1)
                return result + " p.";
            if (result.IndexOf("p.p.") > -1 || result.IndexOf("p. p.") > -1)
                return result.Replace("p.p.", "p.").Replace("p. p.", "p.");

            if (Regex.IsMatch(result, @"^[0-9]+$"))
                return result + " p.";

            if (!result.StartsWith("[") && !result.EndsWith("]"))
                return result;
            else if (Regex.IsMatch(GetMiddelString(result, "[", "]"), @"^[0-9]+$"))
                return result + " p.";

            return result;
        }
        #endregion

        private string Text500Insert()
        {
            string boxText = text500a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string[] splitText = boxText.Split('▽');

            string result = "";

            foreach (string text in splitText)
            {
                result += string.Format("500\t  \t▼a{0}▲\n", text);
            }

            return result;
        }

        private string Text504Insert()
        {
            string boxText = text504a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("504\t  \t▼a{0}", boxText);

            result += "▲";

            return result;
        }

        private string Text505Insert()
        {
            string boxText = text505a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("505\t  \t▼a{0}", boxText);

            result += "▲";

            return result;
        }

        private string Text507Insert()
        {
            string[] boxText = {
                text507a.Text, text507t.Text };

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = "507\t  \t";

            if (boxText[0] != "") result += "▼a" + boxText[0];
            if (boxText[1] != "") result += "▼t" + boxText[1];

            result += "▲";

            return result;
        }

        private string Text520Insert()
        {
            string boxText = text520a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("520\t  \t▼a{0}", boxText);

            result += "▲";

            return result;
        }

        private string Text521Insert()
        {
            string boxText = text521a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("521\t  \t▼a{0}", boxText);

            result += "▲";

            return result;
        }

        private string Text525Insert()
        {
            string boxText = text525a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("525\t  \t▼a{0}", boxText);

            result += "▲";

            return result;
        }

        private string Text536Insert()
        {
            string boxText = text536a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("536\t  \t▼a{0}", boxText);

            result += "▲";

            return result;
        }

        private string Text546Insert()
        {
            string boxText = text546a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("546\t  \t▼a{0}", boxText);

            result += "▲";

            return result;
        }

        private string Text586Insert()
        {
            string boxText = text586a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string[] splitText = boxText.Split('▽');

            string result = "";

            foreach (string text in splitText)
            {
                result += string.Format("586\t  \t▼a{0}▲\n", text);
            }

            return result;
        }

        private string Text650Insert()
        {
            string boxText = text650a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string[] splitText = boxText.Split('▽');

            string result = "";

            foreach (string text in splitText)
            {
                result += string.Format("650\t  \t▼a{0}▲\n", text);
            }

            return result;
        }

        private string Text653Insert()
        {
            string boxText = text653a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("653\t  \t▼a{0}", boxText.Replace("▽", "▼a"));

            result += "▲";

            return result;
        }

        private string Text700Insert()
        {
            string boxText = text700a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string[] splitText = boxText.Split('▽');

            string result = "";

            foreach (string text in splitText)
            {
                result += string.Format("700\t  \t▼a{0}▲\n", text);
            }

            return result;
        }

        private string Text710Insert()
        {
            string boxText = text710a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string[] splitText = boxText.Split('▽');

            string result = "";

            foreach (string text in splitText)
            {
                if (text.IndexOf("@") > 0)
                    result += string.Format("710\t  \t▼a{0}▲\n", text.Replace("@", "▼b"));
                else
                    result += string.Format("710\t  \t▼a{0}▲\n", text);

            }

            return result;
        }

        private string Text900Insert()
        {
            string boxText = text900a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string[] splitText = boxText.Split('▽');

            string result = "";

            foreach (string text in splitText)
            {
                result += string.Format("900\t  \t▼a{0}▲\n", text);
            }

            return result;
        }

        private string Text910Insert()
        {
            string boxText = text910a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string[] splitText = boxText.Split('▽');

            string result = "";

            foreach (string text in splitText)
            {
                if (text.IndexOf("@") > 0)
                    result += string.Format("910\t  \t▼a{0}▲\n", text.Replace("@", "▼b"));
                else
                    result += string.Format("910\t  \t▼a{0}▲\n", text);
            }

            return result;
        }

        private string Text940Insert()
        {
            string boxText = text940a.Text;

            bool isText = isEmpty(boxText);

            if (!isText)
                return "";

            string result = string.Format("940\t  \t▼a{0}\n", boxText);

            result += "▲";

            return result;
        }



        private bool isEmpty(string boxText)
        {
            if (boxText == "")
                return false;
            return true;
        }

        private bool isEmpty(string[] boxText)
        {
            bool[] isText = new bool[boxText.Length];
            int count = 0;
            int chkCount = 0;
            foreach (string Check in boxText)
            {
                if (Check == "")
                    isText[count] = false;
                else
                {
                    isText[count] = true;
                    chkCount++;
                }
                count++;
            }

            if (chkCount == 0)
                return false;

            return true;
        }
        #endregion

        #endregion

        private void Btn_interlock_Click(object sender, EventArgs e)
        {
            // 언어 36
            string[] combo6 = {
                "한국어", "영어", "일본어", "중국어", "독일어",
                "프랑스어", "러시아어", "스페인어", "이탈리아어", "네덜란드어",
                "핀란드어", "스웨덴어", "포르투갈어", "노르웨이어", "그리스어",
                "체코어", "폴란드어", "다국어", "말레이시아어", "몽골어",
                "버마어", "베트남어", "슬로베니아어", "아랍어", "아프리카어",
                "에스토니아어", "우즈베키스탄어", "우크라이나어", "마라티어", "캄보디아어",
                "태국어", "터키어", "투르크메니스탄어", "티베르어", "타갈로그어",
                "헝가리어" };

            string[] combo6_res = {
                "kor", "eng", "jpn", "chi", "ger",
                "fre", "rus", "spa", "ita", "dut",
                "fin", "swe", "por", "nor", "grc",
                "cze", "pol", "mul", "may", "mon",
                "bur", "vie", "slv", "ara", "afr",
                "est", "uzb", "ukr", "mar", "cam",
                "tha", "tur", "tuk", "tib", "tag",
                "hun" };
            string result = "";
            string a041 = text041a.Text;
            string k041 = text041k.Text;
            string h041 = text041h.Text;
            string b041 = text041b.Text;

            int count = 0;
            foreach (string code in combo6_res)
            {
                a041 = a041.Replace(code, combo6[count]);
                k041 = k041.Replace(code, combo6[count]);
                h041 = h041.Replace(code, combo6[count]);
                b041 = b041.Replace(code, combo6[count]);
                count++;
            }

            a041 = a041.Replace("▽", ", ");

            // k로 번역된 h 원작을 a로 중역
            if (a041 != "" && k041 != "" && h041 != "") {
                result = string.Format("{0}로 번역된 {1} 원작을 {2}로 중역", k041, h041, a041);
            }
            // h 원작을 a로 번역
            else if (a041 != "" && k041 == "" && h041 != "") {
                result = string.Format("{0} 원작을 {1}로 번역", h041, a041);
            }
            // 본문은 a1, a2 혼합수록됨.
            else if (a041.IndexOf(",") > 0) {
                result = string.Format("본문은 {0} 혼합수록 됨.", a041);
            }

            text546a.Text = result;
        }

        private void invertCheck_CheckedChanged(object sender, EventArgs e)
        {
            string tmpStr = text245d.Text;
            if (tmpStr[tmpStr.Length - 1] != ',') { tmpStr = basic_Replace(tmpStr); }
            else { tmpStr = tmpStr.Remove(tmpStr.Length - 1); }
            basicHeadBox.Text = tmpStr;

            string[] invert = basicHeadBox.Text.Split(' ');

            for (int a = 0; a < invert.Length; a++)
            {
                if (invert[a][invert[a].Length - 1] == ',')
                {
                    invert[a] = invert[a].Substring(0, invert[a].Length - 1);
                }
            }
            try
            {
                if (invertCheck.Checked)
                {
                    for (int a = 0; a < invert.Length; a++)
                    {
                        if (a == 0)
                        {
                            if (invert[a][invert[a].Length - 1] != ',') { invert[a] += ","; }
                            basicHeadBox.Text = invert[a] + " ";
                            if (invert[a][invert[a].Length - 1] == ',') { invert[a] = invert[a].Substring(0, invert.Length - 1); }
                        }
                        else
                        {
                            if (invert[a][invert[a].Length - 1] != ',') { invert[a] += ","; }
                            basicHeadBox.Text += invert[a] + " ";
                            if (invert[a][invert[a].Length - 1] == ',') { invert[a] = invert[a].Substring(0, invert.Length - 1); }
                        }
                    }
                }
                else if (!invertCheck.Checked)
                {
                    for (int a = invert.Length - 1; a >= 0; a--)
                    {
                        if (a == invert.Length - 1)
                        {
                            if (invert[a][invert[a].Length - 1] != ',') { invert[a] += ","; }
                            basicHeadBox.Text = invert[a] + " ";
                            if (invert[a][invert[a].Length - 1] == ',') { invert[a] = invert[a].Substring(0, invert.Length - 1); }
                        }
                        else
                        {
                            if (invert[a][invert[a].Length - 1] != ',') { invert[a] += ","; }
                            basicHeadBox.Text += invert[a] + " ";
                            if (invert[a][invert[a].Length - 1] == ',') { invert[a] = invert[a].Substring(0, invert.Length - 1); }
                        }
                    }
                }
                if (basicHeadBox.Text[basicHeadBox.Text.Length - 1] == ' ')
                {
                    basicHeadBox.Text = basicHeadBox.Text.Substring(0, basicHeadBox.Text.Length - 1);
                }
                if (basicHeadBox.Text[basicHeadBox.Text.Length - 1] == ',')
                {
                    basicHeadBox.Text = basicHeadBox.Text.Substring(0, basicHeadBox.Text.Length - 1);
                }
            }
            catch
            {
                MessageBox.Show("데이터가 올바르지않습니다.\n245d를 확인해주세요.");
            }
        }
        #region 기본표목 생성 서브 함수

        /// <summary>
        /// 245d에서 " 역할어,"를 잘라내는 함수
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public string basic_Replace(string strValue)
        {
            basicHeadBox.Text = "";
            string result = strValue + ",";
            string[] Role = { "글", "그림", "지음", "글·그림", "편", "엮음", "저", "씀" };
            string[] gl = { "글.그림", "글그림", "그림글", "그림.글" };

            for (int a = 0; a < gl.Length; a++) { result = result.Replace(gl[a], "글·그림"); }
            for (int a = 0; a < Role.Length; a++) { result = result.Replace(" " + Role[a] + ",", ""); }

            return result;
        }
        #endregion

        private void GridView_KeyDown(object sender, KeyEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Excel_to_DataGridView(sender, e);

            if (e.KeyCode == Keys.Delete)
                sg.DataGrid_to_Delete(sender, e);
        }

        private void btn_FilterReturn_Click(object sender, EventArgs e)
        {
            rb_Filter.Checked = false;
            rb_Sort.Checked = false;

            comboBox8.SelectedIndex = 0;
            comboBox9.SelectedIndex = 0;

            List_Book.Sort(list_idx, System.ComponentModel.ListSortDirection.Ascending);
        }

        private void List_Book_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }
    }
}