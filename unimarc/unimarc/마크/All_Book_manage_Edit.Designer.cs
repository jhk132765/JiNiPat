﻿
namespace UniMarc.마크
{
    partial class All_Book_manage_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb_set_name_old = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_set_isbn_old = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_set_count_old = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_set_price_old = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tb_set_price_new = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_set_count_new = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_set_isbn_new = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_set_name_new = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "세트 명";
            // 
            // tb_set_name_old
            // 
            this.tb_set_name_old.Enabled = false;
            this.tb_set_name_old.Location = new System.Drawing.Point(82, 42);
            this.tb_set_name_old.Name = "tb_set_name_old";
            this.tb_set_name_old.Size = new System.Drawing.Size(100, 21);
            this.tb_set_name_old.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.tb_set_price_old);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tb_set_count_old);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tb_set_isbn_old);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tb_set_name_old);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(207, 163);
            this.panel1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "세트 ISBN";
            // 
            // tb_set_isbn_old
            // 
            this.tb_set_isbn_old.Enabled = false;
            this.tb_set_isbn_old.Location = new System.Drawing.Point(82, 69);
            this.tb_set_isbn_old.Name = "tb_set_isbn_old";
            this.tb_set_isbn_old.Size = new System.Drawing.Size(100, 21);
            this.tb_set_isbn_old.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "세트 수량";
            // 
            // tb_set_count_old
            // 
            this.tb_set_count_old.Enabled = false;
            this.tb_set_count_old.Location = new System.Drawing.Point(82, 96);
            this.tb_set_count_old.Name = "tb_set_count_old";
            this.tb_set_count_old.Size = new System.Drawing.Size(100, 21);
            this.tb_set_count_old.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "세트 정가";
            // 
            // tb_set_price_old
            // 
            this.tb_set_price_old.Enabled = false;
            this.tb_set_price_old.Location = new System.Drawing.Point(82, 123);
            this.tb_set_price_old.Name = "tb_set_price_old";
            this.tb_set_price_old.Size = new System.Drawing.Size(100, 21);
            this.tb_set_price_old.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.tb_set_price_new);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.tb_set_count_new);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.tb_set_isbn_new);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.tb_set_name_new);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(296, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(207, 163);
            this.panel2.TabIndex = 2;
            // 
            // tb_set_price_new
            // 
            this.tb_set_price_new.Location = new System.Drawing.Point(82, 123);
            this.tb_set_price_new.Name = "tb_set_price_new";
            this.tb_set_price_new.Size = new System.Drawing.Size(100, 21);
            this.tb_set_price_new.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "세트 정가";
            // 
            // tb_set_count_new
            // 
            this.tb_set_count_new.Enabled = false;
            this.tb_set_count_new.Location = new System.Drawing.Point(82, 96);
            this.tb_set_count_new.Name = "tb_set_count_new";
            this.tb_set_count_new.Size = new System.Drawing.Size(100, 21);
            this.tb_set_count_new.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "세트 수량";
            // 
            // tb_set_isbn_new
            // 
            this.tb_set_isbn_new.Location = new System.Drawing.Point(82, 69);
            this.tb_set_isbn_new.Name = "tb_set_isbn_new";
            this.tb_set_isbn_new.Size = new System.Drawing.Size(100, 21);
            this.tb_set_isbn_new.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "세트 ISBN";
            // 
            // tb_set_name_new
            // 
            this.tb_set_name_new.Location = new System.Drawing.Point(82, 42);
            this.tb_set_name_new.Name = "tb_set_name_new";
            this.tb_set_name_new.Size = new System.Drawing.Size(100, 21);
            this.tb_set_name_new.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "세트 명";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(236, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 29);
            this.label9.TabIndex = 0;
            this.label9.Text = "=>";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(144, 181);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 3;
            this.btn_Save.Text = "저   장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(296, 181);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 3;
            this.btn_Close.Text = "닫   기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(80, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 16);
            this.label10.TabIndex = 2;
            this.label10.Text = "기존";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(80, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "변경";
            // 
            // All_Book_manage_Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 210);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label9);
            this.Name = "All_Book_manage_Edit";
            this.Text = "전집 목록 편집";
            this.Load += new System.EventHandler(this.All_Book_manage_Edit_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_set_name_old;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tb_set_price_old;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_set_count_old;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_set_isbn_old;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tb_set_price_new;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_set_count_new;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_set_isbn_new;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_set_name_new;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}