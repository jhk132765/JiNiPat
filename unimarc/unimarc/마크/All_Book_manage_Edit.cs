﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;

namespace UniMarc.마크
{
    public partial class All_Book_manage_Edit : Form
    {
        All_Book_manage manage;
        Helper_DB db = new Helper_DB();
        string compidx;
        public All_Book_manage_Edit(All_Book_manage _manage)
        {
            InitializeComponent();
            manage = _manage;
            compidx = manage.compidx;
        }

        private void All_Book_manage_Edit_Load(object sender, EventArgs e)
        {
            db.DBcon();
        }

        /// <summary>
        /// 편집창 기본 세팅
        /// </summary>
        /// <param name="old">세트명 / 세트ISBN / 세트 수량 / 세트 정가</param>
        public void set_old(string[] old)
        {
            TextBox[] tb_old = { tb_set_name_old, tb_set_isbn_old, tb_set_count_old, tb_set_price_old };
            TextBox[] tb_new = { tb_set_name_new, tb_set_isbn_new, tb_set_count_new, tb_set_price_new };
            for (int a = 0; a < tb_old.Length; a++)
            {
                tb_old[a].Text = old[a];
                tb_new[a].Text = old[a];
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            string table = "Set_Book";
            string[] sear_tbl = { "compidx", "set_name", "set_isbn", "set_count", "set_price" };
            string[] sear_col = { compidx, 
                                  tb_set_name_old.Text,
                                  tb_set_isbn_old.Text,
                                  tb_set_count_old.Text,
                                  tb_set_price_old.Text };

            string[] edit_tbl = { "set_name", "set_isbn", "set_count", "set_price" };
            string[] edit_col = { tb_set_name_new.Text,
                                  tb_set_isbn_new.Text,
                                  tb_set_count_new.Text,
                                  tb_set_price_new.Text };

            string U_cmd = db.More_Update(table, edit_tbl, edit_col, sear_tbl, sear_col);
            db.DB_Send_CMD_reVoid(U_cmd);
            MessageBox.Show("변경되었습니다!");
            manage.btn_Search_Click(null, null);
            this.Close();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
