﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;

namespace UniMarc.마크
{
    public partial class All_Book_manage_Add : Form
    {
        Helper_DB db = new Helper_DB();
        All_Book_manage manage;
        public All_Book_manage_Add(All_Book_manage _manage)
        {
            InitializeComponent();
            manage = _manage;
        }

        private void All_Book_manage_Add_Load(object sender, EventArgs e)
        {
            db.DBcon();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            bool grid_chk = true;
            /// book_name, series_title, sub_title, series_num, author, book_comp, price, isbn
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (dataGridView1.Rows[a].Cells["book_name"].Value == null || 
                    dataGridView1.Rows[a].Cells["author"].Value == null || 
                    dataGridView1.Rows[a].Cells["book_comp"].Value == null || 
                    dataGridView1.Rows[a].Cells["price"].Value == null) {

                    grid_chk = false;
                    break;
                }
                else if (dataGridView1.Rows[a].Cells["book_name"].Value.ToString() == "" ||
                    dataGridView1.Rows[a].Cells["author"].Value.ToString() == "" ||
                    dataGridView1.Rows[a].Cells["book_comp"].Value.ToString() == "" ||
                    dataGridView1.Rows[a].Cells["price"].Value.ToString() == "") {
                    grid_chk = false;
                    break;
                }
                else if (tb_setName.Text == "") {
                    grid_chk = false;
                    break;
                }
                if (dataGridView1.Rows[a].Cells["ISBN"].Value == null)
                {
                    dataGridView1.Rows[a].Cells["ISBN"].Value = "";
                }
            }
            if (!grid_chk) {
                MessageBox.Show("필수 입력사항이 비어있습니다!\n맨 앞에 \"*\"이 붙은 곳을 확인해주세요.");
                return;
            }

            // DB 저장
            for(int a= 0; a < dataGridView1.Rows.Count; a++)
            {
                string[] insert_tbl = { "compidx", 
                                        "set_name", "set_count", "set_isbn", "set_price", "set_pubyear",
                                        "book_name", "series_title", "sub_title", "series_num", "author", "book_comp", "isbn", "price", 
                                        "charge" };
                string[] insert_col = { manage.compidx,
                                        tb_setName.Text, tb_setCount.Text, tb_setISBN.Text, tb_setPrice.Text, tb_setYear.Text,
                                        dataGridView1.Rows[a].Cells["book_name"].Value.ToString(),
                                        dataGridView1.Rows[a].Cells["series_title"].Value.ToString(),
                                        dataGridView1.Rows[a].Cells["sub_title"].Value.ToString(),
                                        dataGridView1.Rows[a].Cells["series_num"].Value.ToString(),
                                        dataGridView1.Rows[a].Cells["author"].Value.ToString(),
                                        dataGridView1.Rows[a].Cells["book_comp"].Value.ToString(),
                                        dataGridView1.Rows[a].Cells["ISBN"].Value.ToString(),
                                        dataGridView1.Rows[a].Cells["price"].Value.ToString(), manage.charge };

                string Incmd = db.DB_INSERT("Set_Book", insert_tbl, insert_col);
                db.DB_Send_CMD_reVoid(Incmd);
            }

            MessageBox.Show("저장완료");

            // 부모폼 조회버튼 클릭
            manage.btn_Search_Click(null, null);
            this.Close();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            if ((e.Shift && e.KeyCode == Keys.Insert) || (e.Control && e.KeyCode == Keys.V))
            {
                sg.Excel_to_DataGridView(sender, e);
                tb_setCount.Text = dataGridView1.Rows.Count.ToString();
            }
        }
    }
}
