﻿using Microsoft.VisualBasic;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace UniMarc.마크
{
    public partial class CD_LP_Sub : Form
    {
        CD_LP cp;
        Helper_DB db = new Helper_DB();
        bool isKyoBo = false;
        bool isCD = false;
        int Count = 0;
        public CD_LP_Sub(CD_LP _cp)
        {
            InitializeComponent();
            cp = _cp;
        }

        private void CD_LP_Sub_Load(object sender, EventArgs e)
        {
            db.DBcon();
        }

        public void Btn_KyoBo_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("http://music.kyobobook.co.kr/");
            isKyoBo = true;
        }

        public void Btn_Aladin_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("https://www.aladin.co.kr/home/wmusicmain.aspx");
            isKyoBo = false;
        }

        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }

        private void Btn_Marc_Click(object sender, EventArgs e)
        {
            string result = "";
            if (isKyoBo)
            {
                if (KyoBo_trDVD_flCD())
                    result = MakeMarcKyoBo_DVD(KyoboInfor_DVD());

                else
                    result = MakeMarcKyoBo_Music(KyoBoInfor_Music(), KyoBoGetTrack_Music());
            }
            else
            {
                if (Aladin_trDVD_flCD())
                    result = MakeMarcAladin_DVD(AladinInfor_DVD());

                else
                    result = MakeMarcAladin_Music(AladinInfor_Music());
            }
            cp.richTextBox1.Text = result;
        }

        #region Marc_Sub

        #region KyoBo

        bool KyoBo_trDVD_flCD()
        {
            bool result = false;
            foreach (HtmlElement ul in webBrowser1.Document.GetElementsByTagName("ul"))
            {
                if (ul.GetAttribute("className").IndexOf("main") > -1)
                {
                    if (ul.InnerText.IndexOf("DVD") > -1)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        #region Music
        /// <summary>
        /// 앨범의 기본 정보를 가져옴.
        /// </summary>
        /// <returns>[0]Code [1]Name [2]Author [3]Comp [4]Price [5]PubDate</returns>
        string[] KyoBoInfor_Music()
        {
            string tmp = "";
            string Code = "";
            string Name = "";
            string Artist = "";
            string Comp = "";
            string Price = "";
            string PubDate = "";

            foreach (HtmlElement div in webBrowser1.Document.GetElementsByTagName("div"))
            {
                if (div.GetAttribute("className").IndexOf("music-basic-info") > -1)
                {
                    // 앨범명
                    foreach (HtmlElement p in div.GetElementsByTagName("p"))
                    {
                        if (p.GetAttribute("className").IndexOf("subject") > -1)
                        {
                            Name = p.InnerText;
                            break;
                        }
                    }
                    // 저자, 제작사, 출시일
                    foreach (HtmlElement ul in div.GetElementsByTagName("ul"))
                    {
                        if (ul.GetAttribute("className").IndexOf("newdf") > -1)
                        {
                            foreach (HtmlElement span in ul.GetElementsByTagName("span"))
                            {
                                tmp += span.InnerText + "|";
                            }
                            foreach (HtmlElement li in ul.GetElementsByTagName("li"))
                            {
                                if (li.GetAttribute("className").IndexOf("date") > -1)
                                {
                                    PubDate = li.InnerText;
                                }
                            }
                        }

                        // 정가 상품코드
                        foreach (HtmlElement dd in div.GetElementsByTagName("dd"))
                        {
                            if (dd.GetAttribute("className").IndexOf("sale-price") > -1)
                            {
                                Price = dd.InnerText;
                            }
                            if (dd.GetAttribute("className").IndexOf("product-code") > -1)
                            {
                                string tmpCode = dd.InnerText;
                                Code = tmpCode.Substring(0, tmpCode.IndexOf(" "));
                            }
                        }
                    }   // ul foreach
                }   // if music-basic-info
            }   // div foreach

            string[] ArrayTmp = tmp.Split('|');
            Artist = ArrayTmp[0];
            Comp = ArrayTmp[1];
            Name = Name.Replace(Artist + " - ", "");
            Name = Name.Substring(0, Name.IndexOf(" ["));

            PubDate = Regex.Replace(PubDate, @"[^0-9]", "").Substring(0, 4);
            Price = Regex.Replace(Price, @"[^0-9]", "");

            string[] result = { Code, Name, Artist, Comp, Price, PubDate };
            return result;
        }

        string[] KyoBoGetTrack_Music()
        {
            // CD,LP구분 디스크갯수
            foreach (HtmlElement dl in webBrowser1.Document.GetElementsByTagName("dl"))
            {
                if (dl.GetAttribute("className").IndexOf("etc_seperate") > -1)
                {
                    string tmp = dl.InnerText;
                    isCD = tmp.Contains("CD");
                    Count = Convert.ToInt32(Regex.Replace(tmp, @"[^0-9]", ""));
                }
            }

            string tmp_Track = "";
            // 트랙리스트
            foreach (HtmlElement div in webBrowser1.Document.GetElementsByTagName("div"))
            {
                if (div.GetAttribute("className").IndexOf("track_listbox") > -1)
                {
                    foreach (HtmlElement tr in div.GetElementsByTagName("tbody"))
                    {
                        tmp_Track += tr.InnerText;
                    }
                }
            }

            List<string> Track = new List<string>(tmp_Track.Split('\n'));
            Track = Track.Distinct().ToList();

            return Track.ToArray();
        }

        /// <summary>
        /// 종합하여 마크를 만드는 함수
        /// </summary>
        /// <param name="Data">[0]Code [1]Name [2]Author [3]Comp [4]Price [5]PubDate</param>
        /// <returns></returns>
        string MakeMarcKyoBo_Music(string[] Data, string[] Track)
        {
            string Marc = "020\t  \t▼a" + Data[0] + ":▼c\\" + Data[4] + "▲\n";

            Marc += "056\t  \t▼a▼25▲\n";
            Marc += "100\t1 \t▼a" + Data[2] + "▲\n";
            Marc += "245\t10\t▼a" + Data[1] + "▼h[녹음자료]/▼d" + Data[2] + "▲\n";
            Marc += "260\t  \t▼a서울:▼b" + Data[3] + " [제작],▼c" + Data[5] + "▲\n";
            Marc += "300\t  \t▼a음반 " + Count + "매";
            if (isCD) Marc += "▼b디지털, 스테레오▼c12 cm▲\n500\t  \t▼a컴팩트 디스크▲\n";
            else Marc += "▼c30 cm▲\n";
            Marc += "505\t00\t";
            foreach (string Value in Track)
            {
                Marc += "▼n" + Value.Substring(0, 3) + "▼t" + Value.Substring(3, Value.Length - 4).TrimEnd();
            }
            Marc += "▲\n";
            Marc += "653\t  \t▼a" + Data[1].Replace(" ", "▼a") + "▲\n";
            foreach (string Value in Track)
            {
                Marc += "740\t 2\t▼a" + Value.Substring(3, Value.Length - 4).TrimEnd() + "▲\n";
            }
            Marc += "950\t0 \t▼b\\" + Data[4] + "▲\n";

            return Marc;
        }
        #endregion

        #region DVD

        /// <summary>
        /// DVD 기본 데이터 추출
        /// </summary>
        /// <returns>[0]title, [1]artist, [2]comp, [3]code, [4]price, [5]PubDate, [6]synopsis, [7]actor, [8]data</returns>
        string[] KyoboInfor_DVD()
        {
            string title = "";
            string artist = "";
            string comp = "";
            string code = "";
            string price = "";
            string PubDate = "";
            string synopsis = "";
            string actor = "";

            // 타이틀 가져오기
            foreach (HtmlElement p in webBrowser1.Document.GetElementsByTagName("p"))
            {
                if (p.GetAttribute("className").IndexOf("subject") > -1)
                {
                    title = p.InnerText;
                    break;
                }
            }

            // 제작사, 출시일 가져오기
            string tmpCategory = "";
            string tmp = "";
            foreach (HtmlElement ul in webBrowser1.Document.GetElementsByTagName("ul"))
            {
                if (ul.GetAttribute("className").IndexOf("newdf") > -1)
                {
                    foreach (HtmlElement em in ul.GetElementsByTagName("em"))
                    {
                        tmpCategory += em.InnerText + "|";
                    }
                    foreach (HtmlElement span in ul.GetElementsByTagName("span"))
                    {
                        tmp += span.InnerText + "|";
                    }
                    foreach (HtmlElement li in ul.GetElementsByTagName("li"))
                    {
                        if (li.GetAttribute("className").IndexOf("date") > -1)
                        {
                            PubDate = Regex.Replace(li.InnerText, @"[^0-9]", "").Substring(0, 4);
                        }
                    }
                }
            }
            string[] cateArray = tmpCategory.Split('|');
            string[] tmpArray = tmp.Split('|');
            int a = 0;
            foreach (string t in cateArray)
            {
                if (t == "") continue;

                if (t.IndexOf("제작사") > -1)
                    comp = tmpArray[a];

                a++;
            }

            // 가격, 상품 코드 가져오기
            foreach (HtmlElement dd in webBrowser1.Document.GetElementsByTagName("dd"))
            {
                if (dd.GetAttribute("className").IndexOf("sale-price") > -1)
                    price = Regex.Replace(dd.InnerText, @"[^0-9]", "");     // 가격

                if (dd.GetAttribute("className").IndexOf("product-code") > -1)
                    code = dd.InnerText.Split(' ')[0];                      // 코드
            }

            // 출연진 가져오기
            foreach (HtmlElement p in webBrowser1.Document.GetElementsByTagName("p"))
            {
                if (p.GetAttribute("className").IndexOf("actor") > -1)
                {
                    actor = p.InnerText;
                }
            }

            // 시놉시스 & 기타 정보 가져오기
            string tmp_SYNOP = "";
            string data = "";
            foreach (HtmlElement div in webBrowser1.Document.GetElementsByTagName("div"))
            {
                if (div.GetAttribute("className").IndexOf("product_add") > -1)
                {
                    data = div.InnerText.Replace(actor, "").Replace("\n", " | ");
                }
                if (div.GetAttribute("className").IndexOf("album_information_content_real") > -1)
                {
                    tmp_SYNOP = div.InnerText;
                }
            }

            bool isSYNOP = false;
            foreach (string t in tmp_SYNOP.Split('\n'))
            {
                if (isSYNOP)
                {
                    synopsis = t;
                    break;
                }
                if (t.IndexOf("SYNOPSIS") > -1)
                    isSYNOP = true;
            }
            string[] result = { title, artist, comp, code, price, PubDate, synopsis, actor, data };
            return result;
        }

        /// <summary>
        /// 종합하여 마크를 만드는 함수
        /// </summary>
        /// <param name="Data">[0]title, [1]artist, [2]comp, [3]code, [4]price, [5]PubDate, [6]synopsis, [7]actor, [8]data</param>
        /// <returns></returns>
        string MakeMarcKyoBo_DVD(string[] Data)
        {
            String_Text st = new String_Text();

            string title = Data[0].Substring(0, Data[0].IndexOf("[")).Trim();
            string OriTitle = st.GetMiddelString(Data[0], "[", "]");
            string 언어 = MiddleData(Data[8], "언어");
            string 자막 = MiddleData(Data[8], "자막");
            string 화면 = MiddleData(Data[8], "화면");
            string 음향 = MiddleData(Data[8], "음향");
            string 등급 = MiddleData(Data[8], "등급");
            string 상영시간 = MiddleData(Data[8], "상영시간");
            string 디스크수 = MiddleData(Data[8], "디스크수");

            string Marc = "020\t  \t▼a" + Data[3] + ":▼c\\" + Data[4] + "▲\n";
            Marc += KyoBoMake049(언어, 자막);
            Marc += "056\t  \t▼a▼25▲\n";
            Marc += "100\t1 \t▼a" + Data[1] + "▲\n";    // 도치 필요
            Marc += "245\t10\t▼a" + title + "▼h[비디오 녹화자료]/▼d" + Data[1] + "▲\n";
            Marc += "260\t  \t▼a서울:▼b" + Data[2] + " [제작],▼c" + Data[5] + "▲\n";
            Marc += String.Format("300\t  \t▼aDVD {0}매({1})▼b유성, 천연색▼c12cm▲\n", 디스크수, 상영시간);

            if (언어 != "") Marc += String.Format("500\t  \t▼a언어 : {0}▲\n", 언어);
            if (자막 != "") Marc += String.Format("500\t  \t▼a자막 : {0}▲\n", 자막);
            if (화면 != "") Marc += String.Format("500\t  \t▼a화면 : {0}▲\n", 화면);
            if (음향 != "") Marc += String.Format("500\t  \t▼a음향 : {0}▲\n", 음향);
            Marc += "500\t  \t▼a지역코드, 3 NTSC▲\n";
            if(OriTitle != null || OriTitle != "")
                Marc += "507\t10\t▼a" + OriTitle + "▲\n";
            Marc += String.Format("508\t  \t▼제작진 : 감독 {0}▲\n", Data[1]);

            foreach (string Value in Data[7].Split(','))
            {
                if (Value == "")
                    continue;
                Marc += "507\t8 \t▼a" + Value.Trim() + "▲\n";
            }
            Marc += "520\t  \t▼a" + Data[6] + "▲\n";
            Marc += "521\t  \t▼a" + 등급 + "▲\n";
            Marc += "538\t  \t▼aDVD 전용 플레이어▲\n";
            foreach (string Value in Data[7].Split(','))
            {
                if (Value == "")
                    continue;
                Marc += "700\t1 \t▼a" + Value.Trim() + "▲\n";       // 도치 필요
            }
            Marc += "950\t0 \t▼b\\" + Data[4] + "▲\n";

            return Marc;
        }

        string KyoBoMake049(string Lang, string Text)
        {
            string Marc = "";   // "049\t0 \t";
            //▼a" + Data[3] + ":▼c\\" + Data[4] + "▲\n"
            string[] combo6 = {
                "한국어", "영어", "일본어", "중국어", "독일어",
                "프랑스어", "러시아어", "스페인어", "이탈리아어", "네덜란드어",
                "핀란드어", "스웨덴어", "포르투갈어", "노르웨이어", "그리스어",
                "체코어", "폴란드어", "다국어", "말레이시아어", "몽골어",
                "버마어", "베트남어", "슬로베니아어", "아랍어", "아프리카어",
                "에스토니아어", "우즈베키스탄어", "우크라이나어", "마라티어", "캄보디아어",
                "태국어", "터키어", "투르크메니스탄어", "티베르어", "타갈로그어",
                "헝가리어" };

            string[] combo6_res = {
                "kor", "eng", "jpn", "chi", "ger",
                "fre", "rus", "spa", "ita", "dut",
                "fin", "swe", "por", "nor", "grc",
                "cze", "pol", "mul", "may", "mon",
                "bur", "vie", "slv", "ara", "afr",
                "est", "uzb", "ukr", "mar", "cam",
                "tha", "tur", "tuk", "tib", "tag",
                "hun" };

            foreach (string s in Lang.Split(','))
            {
                int count = 0;
                foreach (string s2 in combo6)
                {
                    if (s.Trim() == s2)
                    {
                        Marc += "▼a" + combo6_res[count];
                    }
                    count++;
                }
            }
            foreach (string s in Text.Split(','))
            {
                int count = 0;
                foreach (string s2 in combo6)
                {
                    if (s.Trim() == s2)
                    {
                        Marc += "▼b" + combo6_res[count];
                    }
                    count++;
                }
            }

            if (Marc == "")
                return "";
            return "049\t0 \t" + Marc + "▲\n";
        }

        string MiddleData(string Data, string Search)
        {
            Search += " : ";
            Data = Data.Replace(" │ ", "│");
            if (Data.IndexOf(Search) > -1)
            {
                string tmp = Data.Substring(Data.IndexOf(Search) + Search.Length);

                if (tmp.IndexOf("│") > -1)
                    return tmp.Substring(0, tmp.IndexOf("│"));
                else
                    return tmp.Replace("│", "");
            }
            return "";
        }

        #endregion

        #endregion

        #region Aladin

        bool Aladin_trDVD_flCD()
        {
            bool result = false;
            foreach (HtmlElement ul in webBrowser1.Document.GetElementsByTagName("ul"))
            {
                if (ul.Id != null && ul.Id.IndexOf("ulCategory") > -1)
                    if (ul.InnerText.IndexOf("DVD") > -1)
                        result = true;
            }
            return result;
        }

        #region Music

        string[] AladinInfor_Music()
        {
            string title = "";
            string tmp = "";
            string artist = "";
            string subArtist = "";
            string comp = "";
            string price = "";
            string PubYear = "";

            string tmpMsg = "";

            foreach (HtmlElement a in webBrowser1.Document.GetElementsByTagName("a"))
            {
                if (a.GetAttribute("className").IndexOf("Ere_bo_title") > -1)
                    title = a.InnerText;

                if (a.GetAttribute("className").IndexOf("Ere_sub2_title") > -1)
                {
                    if (a.GetAttribute("href").IndexOf("Publisher") > -1)
                    {
                        comp = a.InnerText;
                        continue;
                    }
                    tmp += a.InnerText + "|";
                }
            }
            string tmp2 = "";
            foreach (HtmlElement li in webBrowser1.Document.GetElementsByTagName("li"))
            {
                if (li.GetAttribute("className").IndexOf("Ere_sub2_title") > -1)
                {
                    tmp2 += li.InnerText;
                }
            }
            foreach (HtmlElement div in webBrowser1.Document.GetElementsByTagName("div"))
            {
                if (div.GetAttribute("className").IndexOf("Ritem") > -1)
                {
                    price = Regex.Replace(div.InnerText, @"[^0-9]", "");
                    break;
                }
            }

            string[] arytmp = tmp.Split('|');
            foreach (string t in arytmp)
            {
                if (t == "")
                    continue;

                tmp2 = tmp2.Replace(t, "");
            }
            artist = arytmp[0];
            subArtist = string.Join("|", arytmp).Replace(artist, "");
            PubYear = Regex.Replace(tmp2, @"[^0-9]", "").Substring(0, 4);
            //PubYear = tmp2.Substring(0, 4);

            isCD = AladinIsCD_Music();

            string[] result = { title, artist, subArtist, comp, price, PubYear };
            return result;
        }

        bool AladinIsCD_Music()
        {
            foreach (HtmlElement ul in webBrowser1.Document.GetElementsByTagName("ul"))
            {
                if (ul.Id != null && ul.Id.IndexOf("ulCategory") > -1)
                {
                    if (ul.InnerText.Contains("LP"))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 종합하여 마크를 만드는 함수
        /// </summary>
        /// <param name="Data">[0]title, [1]artist, [2]subArtist, [3]comp, [4]price, [5]PubYear</param>
        /// <returns></returns>
        string MakeMarcAladin_Music(string[] Data)
        {
            string Marc =
                    "020\t  \t▼a:▼c▲\n" +
                    "041\t0 \t▼a▼b▼▲\n" + 
                    "056\t  \t▼a▼25▲\n" +
                    "090\\t  \\t▼a▼b▲\\n";
            Marc += "100\t1 \t▼a" + Data[1] + "▲\n";
            Marc += "245\t10\t▼a" + Data[0] + "▼h[녹음자료]/▼d" + Data[1] + MakeSubArtist(Data[2]) + "▲\n";
            Marc += "260\t  \t▼a서울:▼b" + Data[3] + "▼c" + Data[5] + "▲\n";
            Marc += "300\t  \t▼a음반 1매";
            if (isCD) Marc += "▼b디지털, 스테레오▼c12 cm▲\n500\t  \t▼a컴팩트 디스크▲\n";
            else Marc += "▼c30 cm▲\n";
            Marc += "508\t  \t▼a제작진 : " + Data[1] + "▲\n";
            Marc += "521\t  \t▼a전체이용가▲\n";
            Marc += "653\t  \t▼a" + Data[0].Replace(" ", "▼a") + "▲\n" +
                    "700\t1 \t▼a▲\n" +
                    "740\t 2\t▼a▲\n";
            Marc += "950\t0 \t▼b\\" + Data[4] + "▲\n";

            return Marc;
        }

        /// <summary>
        /// 245e에 들어갈 자리를 만듦.
        /// </summary>
        /// <param name="SubArtist">메인 아티스트를 제외한 서브 아티스트들</param>
        /// <returns></returns>
        string MakeSubArtist(string SubArtist, char VarChar = '|')
        {
            if (SubArtist == "")
                return "";
            string[] ary = SubArtist.Split(VarChar);

            string result = "";
            foreach (string t in ary)
            {
                if (t == "")
                    continue;
                result += "▼e" + t.Trim();
            }
            return result;
        }
        #endregion

        #region DVD

        string[] AladinInfor_DVD()
        {
            // 작품명, 제작사
            string title = "";
            string comp = "";
            foreach (HtmlElement a in webBrowser1.Document.GetElementsByTagName("a"))
            {
                if (a.GetAttribute("className").IndexOf("Ere_bo_title") > -1)
                    title = a.InnerText;

                if (a.GetAttribute("href").IndexOf("Publisher") > -1)
                    comp = a.InnerText;
            }

            // 감독명, 출연진, 제작년도, 작품원제
            string tmp = "";
            foreach (HtmlElement li in webBrowser1.Document.GetElementsByTagName("li"))
            {
                if (li.GetAttribute("className").IndexOf("Ere_sub2_title") > -1)
                {
                    tmp = li.InnerText.Replace(comp, "");
                }
            }
            int start = 0, end = tmp.IndexOf("(감독)");
            string Artist = Substring_Sub(tmp, start, end);

            start = end + "(감독)".Length + 1;
            end = tmp.IndexOf("(출연)") - start;
            string Actor = Substring_Sub(tmp, start, end);

            start = tmp.IndexOf("(출연)") + comp.Length - 1;
            end = 4;
            string Years = Substring_Sub(tmp, start, end);

            start = tmp.IndexOf("원제 : ") + "원제 : ".Length;
            end = tmp.Length - start;
            string oriTitle = Substring_Sub(tmp, start, end);

            // DVD 갯수
            string count = "";
            foreach (HtmlElement li in webBrowser1.Document.GetElementsByTagName("li"))
            {
                if (li.InnerText!=null && li.InnerText.IndexOf("Disc : ") > -1)
                    count = Regex.Replace(li.InnerText, @"\D", "");
            }

            // 정가
            string price = "";
            foreach (HtmlElement div in webBrowser1.Document.GetElementsByTagName("div"))
            {
                if (div.GetAttribute("className").IndexOf("Ritem") > -1)
                {
                    price = Regex.Replace(div.InnerText, @"\D", "");
                    break;
                }
            }

            /// [0]title, [1]artist, [2]subArtist, [3]comp, [4]price, [5]PubYear, [6]count, [7]oriTitle
            string[] Result = { title, Artist, Actor, comp, price, Years, count, oriTitle };
            return Result;
        }

        /// <summary>
        /// 종합하여 마크를 만드는 함수
        /// </summary>
        /// <param name="Data">[0]title, [1]artist, [2]subArtist, [3]comp, [4]price, [5]PubYear, [6]count, [7]oriTitle</param>
        /// <returns></returns>
        string MakeMarcAladin_DVD(string[] Data)
        {
            string Marc =
                    "020\t  \t▼a:▼c▲\n" +
                    "041\t0 \t▼a▼b▼▲\n" +
                    "056\t  \t▼a▼25▲\n" +
                    "090\\t  \\t▼a▼b▲\\n";
            Marc += "100\t1 \t▼a" + Data[1] + "▲\n";
            Marc += "245\t10\t▼a" + Data[0] + "▼h[비디오 녹음자료]/▼d" + Data[1] + MakeSubArtist(Data[2], ',') + "▲\n";
            Marc += "260\t  \t▼a서울:▼b" + Data[3] + "제작▼c" + Data[5] + "▲\n";
            Marc += "300\t  \t▼aDVD" + Data[6] + " 매▼b유성, 천연색▼c12cm▲\n";
            if (Data[7]!= "")
                Marc += "507\t10\t▼t" + Data[7] + "▲\n";
            Marc += "508\t  \t▼a제작진 : " + Data[1] + "▲\n";
            Marc += "511\t8 \t▼a" + Data[2] + "▲\n";
            Marc += "521\t  \t▼a전체이용가▲\n";
            Marc += "538\t  \t▼aDVD 전용 플레이어▲\n";
            Marc += "653\t  \t▼a" + Data[0].Replace(" ", "▼a") + "▲\n";
            foreach (string s in Data[2].Split(','))
            {
                Marc += "700\t1 \t▼a" + s.Trim() + "▲\n";
            }
            Marc += "950\t0 \t▼b\\" + Data[4] + "▲\n";

            return Marc;
        }


        string Substring_Sub(string Value, int start, int end)
        {
            if (start < 0 || end < 1)
                return "";

            return Value.Substring(start, end);
        }
        #endregion
        #endregion

        #endregion

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
