﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using AuthorSymbol;

namespace WindowsFormsApp1.Mac
{
    public partial class Symbol_Add : Form
    {
        Main main;
        Symbol sb = new Symbol();
        public Symbol_Add(Main _main)
        {
            InitializeComponent();
            main = _main;
        }

        private void Symbol_Add_Load(object sender, EventArgs e)
        {
            string[] com_list = sb.authorType;
            RadioButton[] rb = { rb_author1, rb_book1, rb_moum1 };

            for (int a = 0; a < rb.Length; a++)
            {
                rb[a].Checked = true;
            }
            cb_symbol.Items.AddRange(com_list);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string res = tb_author.Text;
            string book = tb_book.Text;

            if (tb_author.Text == "")
            {
                MessageBox.Show("대표 저자를 입력해주세요!");
                return;
            }
            if (cb_symbol.Text == "커터 샌본 저자기호표")
            {
                if (!CheckEng(res))
                {
                    MessageBox.Show("영어만 입력해주세요!");
                    return;
                }
            }
            else if (cb_symbol.Text == "동서양 저자기호법 (국중)") { }
            else
            {
                if (!CheckKor(res))
                {
                    MessageBox.Show("한글만 입력해주세요!");
                    return;
                }
            }
            bool AuthorLookUp = rb_author1.Checked;
            bool BookLookUp = rb_book1.Checked;

            bool[] isType = { AuthorLookUp, BookLookUp };
            string result = "";

            if (cb_symbol.SelectedIndex >= 0)
                result = sb.SymbolAuthor(res, book, cb_symbol.SelectedItem.ToString(), isType);
         
            tb_result.Text = result;
        }

        private void btn_GridTrans_Click(object sender, EventArgs e)
        {
            int count = dataGridView1.Rows.Count;
            for (int a = 0; a < count; a++)
            {
                if (dataGridView1.Rows[a].Cells["Author"].Value == null)
                    continue;

                string author = dataGridView1.Rows[a].Cells["Author"].Value.ToString();
                string BookName = dataGridView1.Rows[a].Cells["book_name"].Value.ToString();

                tb_author.Text = author;
                tb_book.Text = BookName;

                button2_Click(sender, e);

                string result = tb_result.Text;

                if (result.IndexOf("NH") > -1)
                    dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Yellow;
                else
                    dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.White;

                dataGridView1.Rows[a].Cells["AuthorSymbol"].Value = result;
            }
            tb_author.Text = "";
            tb_book.Text = "";
            tb_result.Text = "";
        }

        #region Btn2_Sub
        private bool CheckEng(string Value)
        {
            bool IsCheck = true;

            Regex engRegex = new Regex(@"[a-zA-Z]");
            Boolean isMatch = engRegex.IsMatch(Value);

            if (!isMatch)
                IsCheck = false;

            return IsCheck;
        }
        private bool CheckKor(string Value)
        {
            bool IsCheck = true;

            Regex korRegex = new Regex(@"[ㄱ-ㅎ가-힣]");
            Boolean isMatch = korRegex.IsMatch(Value);

            if (!isMatch)
                IsCheck = false;

            return IsCheck;
        }
        #endregion

        private void RadioBtn_Click(object sender, EventArgs e)
        {
            ((RadioButton)sender).Checked = true;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cb_symbol_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cb_symbol.Text == "장일세 저자기호표" || cb_symbol.Text == "동서양 저자기호법 (국중)")
            {
                rb_moum1.Enabled = false;
                rb_moum2.Checked = true;
            }
            else if (cb_symbol.Text == "커터 샌본 저자기호표")
            {
                rb_author1.Checked = true;
                rb_author2.Enabled = false;
                rb_book1.Checked = true;
                rb_book2.Enabled = false;
                rb_moum1.Enabled = false;
                rb_moum2.Checked = true;
            }
            else if (cb_symbol.Text == "엘러드 저자기호법")
            {
                rb_author1.Enabled = false;
                rb_author2.Enabled = false;
                rb_moum1.Enabled = false;
                rb_moum2.Enabled = false;
                rb_moum2.Checked = true;
            }
            else
            {
                rb_author1.Enabled = true;
                rb_author2.Enabled = true;
                rb_book1.Enabled = true;
                rb_book2.Enabled = true;
                rb_moum1.Enabled = true;
                rb_moum2.Enabled = true;
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Excel_to_DataGridView(sender, e);
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }
    }
}
