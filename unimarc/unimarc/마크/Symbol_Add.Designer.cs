﻿namespace WindowsFormsApp1.Mac
{
    partial class Symbol_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_symbol = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tb_result = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.rb_moum1 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.rb_moum2 = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rb_book1 = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.rb_book2 = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rb_author1 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.rb_author2 = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tb_book = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tb_author = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_OneTrans = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_GridTrans = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "저자기호표";
            // 
            // cb_symbol
            // 
            this.cb_symbol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_symbol.FormattingEnabled = true;
            this.cb_symbol.Location = new System.Drawing.Point(191, 8);
            this.cb_symbol.Name = "cb_symbol";
            this.cb_symbol.Size = new System.Drawing.Size(366, 20);
            this.cb_symbol.TabIndex = 0;
            this.cb_symbol.SelectedIndexChanged += new System.EventHandler(this.cb_symbol_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(431, 41);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 261);
            this.panel1.TabIndex = 7;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.tb_result);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 222);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(576, 37);
            this.panel8.TabIndex = 6;
            // 
            // tb_result
            // 
            this.tb_result.Location = new System.Drawing.Point(191, 7);
            this.tb_result.Name = "tb_result";
            this.tb_result.Size = new System.Drawing.Size(366, 21);
            this.tb_result.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "저자기호변환";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.rb_moum1);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.rb_moum2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 185);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(576, 37);
            this.panel7.TabIndex = 5;
            // 
            // rb_moum1
            // 
            this.rb_moum1.AutoSize = true;
            this.rb_moum1.Location = new System.Drawing.Point(195, 9);
            this.rb_moum1.Name = "rb_moum1";
            this.rb_moum1.Size = new System.Drawing.Size(129, 16);
            this.rb_moum1.TabIndex = 0;
            this.rb_moum1.TabStop = true;
            this.rb_moum1.Text = "모음기호보류(원칙)";
            this.rb_moum1.UseVisualStyleBackColor = true;
            this.rb_moum1.Click += new System.EventHandler(this.RadioBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "자음표2자리일때";
            // 
            // rb_moum2
            // 
            this.rb_moum2.AutoSize = true;
            this.rb_moum2.Location = new System.Drawing.Point(428, 9);
            this.rb_moum2.Name = "rb_moum2";
            this.rb_moum2.Size = new System.Drawing.Size(95, 16);
            this.rb_moum2.TabIndex = 1;
            this.rb_moum2.TabStop = true;
            this.rb_moum2.Text = "모음기호표시";
            this.rb_moum2.UseVisualStyleBackColor = true;
            this.rb_moum2.Click += new System.EventHandler(this.RadioBtn_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.rb_book1);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.rb_book2);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 148);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(576, 37);
            this.panel6.TabIndex = 4;
            // 
            // rb_book1
            // 
            this.rb_book1.AutoSize = true;
            this.rb_book1.Location = new System.Drawing.Point(195, 10);
            this.rb_book1.Name = "rb_book1";
            this.rb_book1.Size = new System.Drawing.Size(95, 16);
            this.rb_book1.TabIndex = 0;
            this.rb_book1.TabStop = true;
            this.rb_book1.Text = "서명음절표시";
            this.rb_book1.UseVisualStyleBackColor = true;
            this.rb_book1.Click += new System.EventHandler(this.RadioBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "서명초성/음절적용";
            // 
            // rb_book2
            // 
            this.rb_book2.AutoSize = true;
            this.rb_book2.Location = new System.Drawing.Point(428, 4);
            this.rb_book2.Name = "rb_book2";
            this.rb_book2.Size = new System.Drawing.Size(129, 28);
            this.rb_book2.TabIndex = 1;
            this.rb_book2.TabStop = true;
            this.rb_book2.Text = "서명초성표시\r\n(한글이 아닐경우X)";
            this.rb_book2.UseVisualStyleBackColor = true;
            this.rb_book2.Click += new System.EventHandler(this.RadioBtn_Click);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.rb_author1);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.rb_author2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 111);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(576, 37);
            this.panel5.TabIndex = 3;
            // 
            // rb_author1
            // 
            this.rb_author1.AutoSize = true;
            this.rb_author1.Location = new System.Drawing.Point(195, 9);
            this.rb_author1.Name = "rb_author1";
            this.rb_author1.Size = new System.Drawing.Size(95, 16);
            this.rb_author1.TabIndex = 0;
            this.rb_author1.TabStop = true;
            this.rb_author1.Text = "저자음절표시";
            this.rb_author1.UseVisualStyleBackColor = true;
            this.rb_author1.Click += new System.EventHandler(this.RadioBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "저자초성/음절적용";
            // 
            // rb_author2
            // 
            this.rb_author2.AutoSize = true;
            this.rb_author2.Location = new System.Drawing.Point(428, 9);
            this.rb_author2.Name = "rb_author2";
            this.rb_author2.Size = new System.Drawing.Size(95, 16);
            this.rb_author2.TabIndex = 1;
            this.rb_author2.TabStop = true;
            this.rb_author2.Text = "저자초성표시";
            this.rb_author2.UseVisualStyleBackColor = true;
            this.rb_author2.Click += new System.EventHandler(this.RadioBtn_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.tb_book);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 74);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(576, 37);
            this.panel3.TabIndex = 2;
            // 
            // tb_book
            // 
            this.tb_book.Location = new System.Drawing.Point(191, 7);
            this.tb_book.Name = "tb_book";
            this.tb_book.Size = new System.Drawing.Size(366, 21);
            this.tb_book.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "서명";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.tb_author);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 37);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(576, 37);
            this.panel4.TabIndex = 1;
            // 
            // tb_author
            // 
            this.tb_author.Location = new System.Drawing.Point(191, 7);
            this.tb_author.Name = "tb_author";
            this.tb_author.Size = new System.Drawing.Size(366, 21);
            this.tb_author.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "대표저자 *";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cb_symbol);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(576, 37);
            this.panel2.TabIndex = 0;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(934, 12);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 2;
            this.btn_close.Text = "닫   기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_OneTrans
            // 
            this.btn_OneTrans.Location = new System.Drawing.Point(853, 12);
            this.btn_OneTrans.Name = "btn_OneTrans";
            this.btn_OneTrans.Size = new System.Drawing.Size(75, 23);
            this.btn_OneTrans.TabIndex = 1;
            this.btn_OneTrans.Text = "개별 변환";
            this.btn_OneTrans.UseVisualStyleBackColor = true;
            this.btn_OneTrans.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Author,
            this.book_name,
            this.AuthorSymbol});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(413, 664);
            this.dataGridView1.TabIndex = 12;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // Author
            // 
            this.Author.HeaderText = "저자";
            this.Author.Name = "Author";
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 150;
            // 
            // AuthorSymbol
            // 
            this.AuthorSymbol.HeaderText = "저자기호";
            this.AuthorSymbol.Name = "AuthorSymbol";
            // 
            // btn_GridTrans
            // 
            this.btn_GridTrans.Location = new System.Drawing.Point(772, 12);
            this.btn_GridTrans.Name = "btn_GridTrans";
            this.btn_GridTrans.Size = new System.Drawing.Size(75, 23);
            this.btn_GridTrans.TabIndex = 0;
            this.btn_GridTrans.Text = "표 변환";
            this.btn_GridTrans.UseVisualStyleBackColor = true;
            this.btn_GridTrans.Click += new System.EventHandler(this.btn_GridTrans_Click);
            // 
            // Symbol_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 688);
            this.Controls.Add(this.btn_GridTrans);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_OneTrans);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Symbol_Add";
            this.Text = "저자기호 생성";
            this.Load += new System.EventHandler(this.Symbol_Add_Load);
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_symbol;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_book;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox tb_author;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rb_author1;
        private System.Windows.Forms.RadioButton rb_author2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton rb_moum1;
        private System.Windows.Forms.RadioButton rb_moum2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rb_book1;
        private System.Windows.Forms.RadioButton rb_book2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_result;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_OneTrans;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorSymbol;
        private System.Windows.Forms.Button btn_GridTrans;
    }
}