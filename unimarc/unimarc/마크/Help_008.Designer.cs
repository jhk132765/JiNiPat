﻿namespace UniMarc.마크
{
    partial class Help_008
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_Text = new System.Windows.Forms.Label();
            this.lbl_008 = new System.Windows.Forms.Label();
            this.PanelMusic = new System.Windows.Forms.Panel();
            this.tb_33 = new System.Windows.Forms.TextBox();
            this.tb_22 = new System.Windows.Forms.TextBox();
            this.tb_21 = new System.Windows.Forms.TextBox();
            this.tb_19 = new System.Windows.Forms.TextBox();
            this.tb_36 = new System.Windows.Forms.TextBox();
            this.tb_31 = new System.Windows.Forms.TextBox();
            this.tb_16 = new System.Windows.Forms.TextBox();
            this.tb_DontTouch01 = new System.Windows.Forms.TextBox();
            this.tb_DontTouch07 = new System.Windows.Forms.TextBox();
            this.tb_29 = new System.Windows.Forms.TextBox();
            this.tb_39 = new System.Windows.Forms.TextBox();
            this.PubYear1 = new System.Windows.Forms.TextBox();
            this.PubYear2 = new System.Windows.Forms.TextBox();
            this.tb_34 = new System.Windows.Forms.TextBox();
            this.tb_27 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_VisibleTarget = new System.Windows.Forms.Label();
            this.tb_25 = new System.Windows.Forms.TextBox();
            this.tb_24 = new System.Windows.Forms.TextBox();
            this.tb_23 = new System.Windows.Forms.TextBox();
            this.lbl_ChangeTarget = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Close = new System.Windows.Forms.Button();
            this.Btn_Apply = new System.Windows.Forms.Button();
            this.PanelDVD = new System.Windows.Forms.Panel();
            this.dvd_33 = new System.Windows.Forms.TextBox();
            this.dvd_36 = new System.Windows.Forms.TextBox();
            this.dvd_35 = new System.Windows.Forms.TextBox();
            this.dvd_19 = new System.Windows.Forms.TextBox();
            this.dvd_16 = new System.Windows.Forms.TextBox();
            this.dvd_01 = new System.Windows.Forms.TextBox();
            this.dvd_07 = new System.Windows.Forms.TextBox();
            this.dvd_29 = new System.Windows.Forms.TextBox();
            this.dvd_39 = new System.Windows.Forms.TextBox();
            this.dvd_08 = new System.Windows.Forms.TextBox();
            this.dvd_12 = new System.Windows.Forms.TextBox();
            this.dvd_27 = new System.Windows.Forms.TextBox();
            this.dvd_34 = new System.Windows.Forms.TextBox();
            this.dvd_30 = new System.Windows.Forms.TextBox();
            this.dvd_23 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.PanelMusic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.PanelDVD.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_Text);
            this.panel1.Controls.Add(this.lbl_008);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(412, 26);
            this.panel1.TabIndex = 1;
            // 
            // lbl_Text
            // 
            this.lbl_Text.AutoSize = true;
            this.lbl_Text.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Text.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_Text.Location = new System.Drawing.Point(40, 5);
            this.lbl_Text.Name = "lbl_Text";
            this.lbl_Text.Size = new System.Drawing.Size(250, 13);
            this.lbl_Text.TabIndex = 0;
            this.lbl_Text.Text = "220811s        ulk    d         avlkor  ";
            // 
            // lbl_008
            // 
            this.lbl_008.AutoSize = true;
            this.lbl_008.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_008.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_008.Location = new System.Drawing.Point(5, 5);
            this.lbl_008.Name = "lbl_008";
            this.lbl_008.Size = new System.Drawing.Size(31, 13);
            this.lbl_008.TabIndex = 0;
            this.lbl_008.Text = "008";
            // 
            // PanelMusic
            // 
            this.PanelMusic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelMusic.Controls.Add(this.tb_33);
            this.PanelMusic.Controls.Add(this.tb_22);
            this.PanelMusic.Controls.Add(this.tb_21);
            this.PanelMusic.Controls.Add(this.tb_19);
            this.PanelMusic.Controls.Add(this.tb_36);
            this.PanelMusic.Controls.Add(this.tb_31);
            this.PanelMusic.Controls.Add(this.tb_16);
            this.PanelMusic.Controls.Add(this.tb_DontTouch01);
            this.PanelMusic.Controls.Add(this.tb_DontTouch07);
            this.PanelMusic.Controls.Add(this.tb_29);
            this.PanelMusic.Controls.Add(this.tb_39);
            this.PanelMusic.Controls.Add(this.PubYear1);
            this.PanelMusic.Controls.Add(this.PubYear2);
            this.PanelMusic.Controls.Add(this.tb_34);
            this.PanelMusic.Controls.Add(this.tb_27);
            this.PanelMusic.Controls.Add(this.label3);
            this.PanelMusic.Controls.Add(this.lbl_VisibleTarget);
            this.PanelMusic.Controls.Add(this.tb_25);
            this.PanelMusic.Controls.Add(this.tb_24);
            this.PanelMusic.Controls.Add(this.tb_23);
            this.PanelMusic.Controls.Add(this.lbl_ChangeTarget);
            this.PanelMusic.Controls.Add(this.label13);
            this.PanelMusic.Controls.Add(this.label2);
            this.PanelMusic.Controls.Add(this.label12);
            this.PanelMusic.Controls.Add(this.label19);
            this.PanelMusic.Controls.Add(this.label1);
            this.PanelMusic.Controls.Add(this.label11);
            this.PanelMusic.Controls.Add(this.label10);
            this.PanelMusic.Controls.Add(this.label20);
            this.PanelMusic.Controls.Add(this.label9);
            this.PanelMusic.Controls.Add(this.label8);
            this.PanelMusic.Controls.Add(this.label7);
            this.PanelMusic.Controls.Add(this.label14);
            this.PanelMusic.Controls.Add(this.label6);
            this.PanelMusic.Controls.Add(this.label4);
            this.PanelMusic.Controls.Add(this.label5);
            this.PanelMusic.Location = new System.Drawing.Point(12, 44);
            this.PanelMusic.Name = "PanelMusic";
            this.PanelMusic.Size = new System.Drawing.Size(412, 255);
            this.PanelMusic.TabIndex = 2;
            // 
            // tb_33
            // 
            this.tb_33.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_33.Location = new System.Drawing.Point(338, 142);
            this.tb_33.Name = "tb_33";
            this.tb_33.ReadOnly = true;
            this.tb_33.Size = new System.Drawing.Size(55, 21);
            this.tb_33.TabIndex = 1;
            this.tb_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_33.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_22
            // 
            this.tb_22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_22.Location = new System.Drawing.Point(127, 196);
            this.tb_22.Name = "tb_22";
            this.tb_22.ReadOnly = true;
            this.tb_22.Size = new System.Drawing.Size(55, 21);
            this.tb_22.TabIndex = 1;
            this.tb_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_22.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_21
            // 
            this.tb_21.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_21.Location = new System.Drawing.Point(127, 169);
            this.tb_21.Name = "tb_21";
            this.tb_21.ReadOnly = true;
            this.tb_21.Size = new System.Drawing.Size(55, 21);
            this.tb_21.TabIndex = 1;
            this.tb_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_21.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_19
            // 
            this.tb_19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_19.Location = new System.Drawing.Point(127, 142);
            this.tb_19.Name = "tb_19";
            this.tb_19.ReadOnly = true;
            this.tb_19.Size = new System.Drawing.Size(55, 21);
            this.tb_19.TabIndex = 1;
            this.tb_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_19.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_36
            // 
            this.tb_36.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_36.Location = new System.Drawing.Point(338, 196);
            this.tb_36.Name = "tb_36";
            this.tb_36.ReadOnly = true;
            this.tb_36.Size = new System.Drawing.Size(55, 21);
            this.tb_36.TabIndex = 1;
            this.tb_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_36.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_31
            // 
            this.tb_31.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_31.Location = new System.Drawing.Point(338, 115);
            this.tb_31.Name = "tb_31";
            this.tb_31.ReadOnly = true;
            this.tb_31.Size = new System.Drawing.Size(55, 21);
            this.tb_31.TabIndex = 1;
            this.tb_31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_31.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_16
            // 
            this.tb_16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_16.Location = new System.Drawing.Point(127, 115);
            this.tb_16.Name = "tb_16";
            this.tb_16.ReadOnly = true;
            this.tb_16.Size = new System.Drawing.Size(55, 21);
            this.tb_16.TabIndex = 1;
            this.tb_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_16.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_DontTouch01
            // 
            this.tb_DontTouch01.BackColor = System.Drawing.Color.LightGray;
            this.tb_DontTouch01.Location = new System.Drawing.Point(127, 7);
            this.tb_DontTouch01.Name = "tb_DontTouch01";
            this.tb_DontTouch01.ReadOnly = true;
            this.tb_DontTouch01.Size = new System.Drawing.Size(55, 21);
            this.tb_DontTouch01.TabIndex = 1;
            this.tb_DontTouch01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_DontTouch07
            // 
            this.tb_DontTouch07.BackColor = System.Drawing.Color.LightGray;
            this.tb_DontTouch07.Location = new System.Drawing.Point(127, 34);
            this.tb_DontTouch07.Name = "tb_DontTouch07";
            this.tb_DontTouch07.ReadOnly = true;
            this.tb_DontTouch07.Size = new System.Drawing.Size(55, 21);
            this.tb_DontTouch07.TabIndex = 1;
            this.tb_DontTouch07.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_29
            // 
            this.tb_29.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_29.Location = new System.Drawing.Point(338, 88);
            this.tb_29.Name = "tb_29";
            this.tb_29.ReadOnly = true;
            this.tb_29.Size = new System.Drawing.Size(55, 21);
            this.tb_29.TabIndex = 1;
            this.tb_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_29.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_39
            // 
            this.tb_39.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_39.Location = new System.Drawing.Point(338, 223);
            this.tb_39.Name = "tb_39";
            this.tb_39.ReadOnly = true;
            this.tb_39.Size = new System.Drawing.Size(55, 21);
            this.tb_39.TabIndex = 1;
            this.tb_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_39.Click += new System.EventHandler(this.textBox_Click);
            // 
            // PubYear1
            // 
            this.PubYear1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.PubYear1.Location = new System.Drawing.Point(127, 61);
            this.PubYear1.MaxLength = 4;
            this.PubYear1.Name = "PubYear1";
            this.PubYear1.Size = new System.Drawing.Size(55, 21);
            this.PubYear1.TabIndex = 1;
            this.PubYear1.Text = "    ";
            this.PubYear1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PubYear1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PubYear_KeyPress);
            // 
            // PubYear2
            // 
            this.PubYear2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.PubYear2.Location = new System.Drawing.Point(127, 88);
            this.PubYear2.MaxLength = 4;
            this.PubYear2.Name = "PubYear2";
            this.PubYear2.Size = new System.Drawing.Size(55, 21);
            this.PubYear2.TabIndex = 1;
            this.PubYear2.Text = "    ";
            this.PubYear2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PubYear2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PubYear_KeyPress);
            // 
            // tb_34
            // 
            this.tb_34.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_34.Location = new System.Drawing.Point(338, 169);
            this.tb_34.Name = "tb_34";
            this.tb_34.ReadOnly = true;
            this.tb_34.Size = new System.Drawing.Size(55, 21);
            this.tb_34.TabIndex = 1;
            this.tb_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_34.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_27
            // 
            this.tb_27.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_27.Location = new System.Drawing.Point(338, 61);
            this.tb_27.Name = "tb_27";
            this.tb_27.ReadOnly = true;
            this.tb_27.Size = new System.Drawing.Size(55, 21);
            this.tb_27.TabIndex = 1;
            this.tb_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_27.Click += new System.EventHandler(this.textBox_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "악보파트보 (22)";
            // 
            // lbl_VisibleTarget
            // 
            this.lbl_VisibleTarget.AutoSize = true;
            this.lbl_VisibleTarget.Location = new System.Drawing.Point(6, 173);
            this.lbl_VisibleTarget.Name = "lbl_VisibleTarget";
            this.lbl_VisibleTarget.Size = new System.Drawing.Size(79, 12);
            this.lbl_VisibleTarget.TabIndex = 0;
            this.lbl_VisibleTarget.Text = "악보형식 (21)";
            // 
            // tb_25
            // 
            this.tb_25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_25.Location = new System.Drawing.Point(338, 34);
            this.tb_25.Name = "tb_25";
            this.tb_25.ReadOnly = true;
            this.tb_25.Size = new System.Drawing.Size(55, 21);
            this.tb_25.TabIndex = 1;
            this.tb_25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_25.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_24
            // 
            this.tb_24.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_24.Location = new System.Drawing.Point(338, 7);
            this.tb_24.Name = "tb_24";
            this.tb_24.ReadOnly = true;
            this.tb_24.Size = new System.Drawing.Size(55, 21);
            this.tb_24.TabIndex = 1;
            this.tb_24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_24.Click += new System.EventHandler(this.textBox_Click);
            // 
            // tb_23
            // 
            this.tb_23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_23.Location = new System.Drawing.Point(127, 223);
            this.tb_23.Name = "tb_23";
            this.tb_23.ReadOnly = true;
            this.tb_23.Size = new System.Drawing.Size(55, 21);
            this.tb_23.TabIndex = 1;
            this.tb_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_23.Click += new System.EventHandler(this.textBox_Click);
            // 
            // lbl_ChangeTarget
            // 
            this.lbl_ChangeTarget.AutoSize = true;
            this.lbl_ChangeTarget.Location = new System.Drawing.Point(6, 146);
            this.lbl_ChangeTarget.Name = "lbl_ChangeTarget";
            this.lbl_ChangeTarget.Size = new System.Drawing.Size(100, 12);
            this.lbl_ChangeTarget.TabIndex = 0;
            this.lbl_ChangeTarget.Text = "작곡형식 (19~20)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(212, 200);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "언어부호 (36~38)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "발행국명 (16~18)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(212, 227);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "기관부호 (39~40)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "발행년 [1] (08~11)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "발행년 [2] (12~15)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 11);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "입력일자 (01~06)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(212, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "조 옮김 및 편곡 (34)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "발행년유형 (07)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(212, 146);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "목록전거 (33)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(212, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "녹음자료내용 (31~32)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(212, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "수정레코드 (29)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(212, 38);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "딸림자료 (25~26)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(212, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "대학부호 (27~28)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(212, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "개별자료형태 (24)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "이용자수준 (23)";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.value,
            this.content});
            this.dataGridView1.Location = new System.Drawing.Point(430, 44);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(331, 255);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // value
            // 
            this.value.HeaderText = "값";
            this.value.Name = "value";
            this.value.ReadOnly = true;
            // 
            // content
            // 
            this.content.HeaderText = "내용";
            this.content.Name = "content";
            this.content.ReadOnly = true;
            this.content.Width = 170;
            // 
            // Btn_Close
            // 
            this.Btn_Close.Location = new System.Drawing.Point(686, 11);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(75, 28);
            this.Btn_Close.TabIndex = 4;
            this.Btn_Close.Text = "닫   기";
            this.Btn_Close.UseVisualStyleBackColor = true;
            this.Btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // Btn_Apply
            // 
            this.Btn_Apply.Location = new System.Drawing.Point(605, 11);
            this.Btn_Apply.Name = "Btn_Apply";
            this.Btn_Apply.Size = new System.Drawing.Size(75, 28);
            this.Btn_Apply.TabIndex = 5;
            this.Btn_Apply.Text = "적   용";
            this.Btn_Apply.UseVisualStyleBackColor = true;
            this.Btn_Apply.Click += new System.EventHandler(this.Btn_Apply_Click);
            // 
            // PanelDVD
            // 
            this.PanelDVD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelDVD.Controls.Add(this.dvd_33);
            this.PanelDVD.Controls.Add(this.dvd_36);
            this.PanelDVD.Controls.Add(this.dvd_35);
            this.PanelDVD.Controls.Add(this.dvd_19);
            this.PanelDVD.Controls.Add(this.dvd_16);
            this.PanelDVD.Controls.Add(this.dvd_01);
            this.PanelDVD.Controls.Add(this.dvd_07);
            this.PanelDVD.Controls.Add(this.dvd_29);
            this.PanelDVD.Controls.Add(this.dvd_39);
            this.PanelDVD.Controls.Add(this.dvd_08);
            this.PanelDVD.Controls.Add(this.dvd_12);
            this.PanelDVD.Controls.Add(this.dvd_27);
            this.PanelDVD.Controls.Add(this.dvd_34);
            this.PanelDVD.Controls.Add(this.dvd_30);
            this.PanelDVD.Controls.Add(this.dvd_23);
            this.PanelDVD.Controls.Add(this.label17);
            this.PanelDVD.Controls.Add(this.label18);
            this.PanelDVD.Controls.Add(this.label21);
            this.PanelDVD.Controls.Add(this.label22);
            this.PanelDVD.Controls.Add(this.label23);
            this.PanelDVD.Controls.Add(this.label24);
            this.PanelDVD.Controls.Add(this.label25);
            this.PanelDVD.Controls.Add(this.label27);
            this.PanelDVD.Controls.Add(this.label28);
            this.PanelDVD.Controls.Add(this.label29);
            this.PanelDVD.Controls.Add(this.label30);
            this.PanelDVD.Controls.Add(this.label31);
            this.PanelDVD.Controls.Add(this.label32);
            this.PanelDVD.Controls.Add(this.label33);
            this.PanelDVD.Controls.Add(this.label34);
            this.PanelDVD.Location = new System.Drawing.Point(12, 44);
            this.PanelDVD.Name = "PanelDVD";
            this.PanelDVD.Size = new System.Drawing.Size(412, 226);
            this.PanelDVD.TabIndex = 2;
            // 
            // dvd_33
            // 
            this.dvd_33.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_33.Location = new System.Drawing.Point(338, 61);
            this.dvd_33.Name = "dvd_33";
            this.dvd_33.ReadOnly = true;
            this.dvd_33.Size = new System.Drawing.Size(55, 21);
            this.dvd_33.TabIndex = 1;
            this.dvd_33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_33.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_36
            // 
            this.dvd_36.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_36.Location = new System.Drawing.Point(338, 142);
            this.dvd_36.Name = "dvd_36";
            this.dvd_36.ReadOnly = true;
            this.dvd_36.Size = new System.Drawing.Size(55, 21);
            this.dvd_36.TabIndex = 1;
            this.dvd_36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_36.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_35
            // 
            this.dvd_35.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_35.Location = new System.Drawing.Point(338, 115);
            this.dvd_35.Name = "dvd_35";
            this.dvd_35.ReadOnly = true;
            this.dvd_35.Size = new System.Drawing.Size(55, 21);
            this.dvd_35.TabIndex = 1;
            this.dvd_35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_35.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_19
            // 
            this.dvd_19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_19.Location = new System.Drawing.Point(127, 142);
            this.dvd_19.MaxLength = 3;
            this.dvd_19.Name = "dvd_19";
            this.dvd_19.ReadOnly = true;
            this.dvd_19.Size = new System.Drawing.Size(55, 21);
            this.dvd_19.TabIndex = 1;
            this.dvd_19.Text = "   ";
            this.dvd_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_19.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_16
            // 
            this.dvd_16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_16.Location = new System.Drawing.Point(127, 115);
            this.dvd_16.Name = "dvd_16";
            this.dvd_16.ReadOnly = true;
            this.dvd_16.Size = new System.Drawing.Size(55, 21);
            this.dvd_16.TabIndex = 1;
            this.dvd_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_16.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_01
            // 
            this.dvd_01.BackColor = System.Drawing.Color.LightGray;
            this.dvd_01.Location = new System.Drawing.Point(127, 7);
            this.dvd_01.Name = "dvd_01";
            this.dvd_01.ReadOnly = true;
            this.dvd_01.Size = new System.Drawing.Size(55, 21);
            this.dvd_01.TabIndex = 1;
            this.dvd_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dvd_07
            // 
            this.dvd_07.BackColor = System.Drawing.Color.LightGray;
            this.dvd_07.Location = new System.Drawing.Point(127, 34);
            this.dvd_07.Name = "dvd_07";
            this.dvd_07.ReadOnly = true;
            this.dvd_07.Size = new System.Drawing.Size(55, 21);
            this.dvd_07.TabIndex = 1;
            this.dvd_07.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dvd_29
            // 
            this.dvd_29.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_29.Location = new System.Drawing.Point(338, 7);
            this.dvd_29.Name = "dvd_29";
            this.dvd_29.ReadOnly = true;
            this.dvd_29.Size = new System.Drawing.Size(55, 21);
            this.dvd_29.TabIndex = 1;
            this.dvd_29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_29.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_39
            // 
            this.dvd_39.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_39.Location = new System.Drawing.Point(338, 169);
            this.dvd_39.Name = "dvd_39";
            this.dvd_39.ReadOnly = true;
            this.dvd_39.Size = new System.Drawing.Size(55, 21);
            this.dvd_39.TabIndex = 1;
            this.dvd_39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_39.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_08
            // 
            this.dvd_08.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_08.Location = new System.Drawing.Point(127, 61);
            this.dvd_08.MaxLength = 4;
            this.dvd_08.Name = "dvd_08";
            this.dvd_08.Size = new System.Drawing.Size(55, 21);
            this.dvd_08.TabIndex = 1;
            this.dvd_08.Text = "    ";
            this.dvd_08.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_08.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PubYear_KeyPress);
            // 
            // dvd_12
            // 
            this.dvd_12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_12.Location = new System.Drawing.Point(127, 88);
            this.dvd_12.MaxLength = 4;
            this.dvd_12.Name = "dvd_12";
            this.dvd_12.Size = new System.Drawing.Size(55, 21);
            this.dvd_12.TabIndex = 1;
            this.dvd_12.Text = "    ";
            this.dvd_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PubYear_KeyPress);
            // 
            // dvd_27
            // 
            this.dvd_27.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_27.Location = new System.Drawing.Point(127, 196);
            this.dvd_27.Name = "dvd_27";
            this.dvd_27.ReadOnly = true;
            this.dvd_27.Size = new System.Drawing.Size(55, 21);
            this.dvd_27.TabIndex = 1;
            this.dvd_27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_27.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_34
            // 
            this.dvd_34.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_34.Location = new System.Drawing.Point(338, 88);
            this.dvd_34.Name = "dvd_34";
            this.dvd_34.ReadOnly = true;
            this.dvd_34.Size = new System.Drawing.Size(55, 21);
            this.dvd_34.TabIndex = 1;
            this.dvd_34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_34.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_30
            // 
            this.dvd_30.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_30.Location = new System.Drawing.Point(338, 34);
            this.dvd_30.Name = "dvd_30";
            this.dvd_30.ReadOnly = true;
            this.dvd_30.Size = new System.Drawing.Size(55, 21);
            this.dvd_30.TabIndex = 1;
            this.dvd_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_30.Click += new System.EventHandler(this.textBox_Click);
            // 
            // dvd_23
            // 
            this.dvd_23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dvd_23.Location = new System.Drawing.Point(127, 169);
            this.dvd_23.Name = "dvd_23";
            this.dvd_23.ReadOnly = true;
            this.dvd_23.Size = new System.Drawing.Size(55, 21);
            this.dvd_23.TabIndex = 1;
            this.dvd_23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dvd_23.Click += new System.EventHandler(this.textBox_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 146);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "상영시간 (19~21)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(212, 146);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "언어부호 (36~38)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 119);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 12);
            this.label21.TabIndex = 0;
            this.label21.Text = "발행국명 (16~18)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(212, 173);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 12);
            this.label22.TabIndex = 0;
            this.label22.Text = "기관부호 (39~40)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 65);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(110, 12);
            this.label23.TabIndex = 0;
            this.label23.Text = "발행년 [1] (08~11)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 92);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(110, 12);
            this.label24.TabIndex = 0;
            this.label24.Text = "발행년 [2] (12~15)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(100, 12);
            this.label25.TabIndex = 0;
            this.label25.Text = "입력일자 (01~06)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 38);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(91, 12);
            this.label27.TabIndex = 0;
            this.label27.Text = "발행년유형 (07)";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(212, 65);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(79, 12);
            this.label28.TabIndex = 0;
            this.label28.Text = "목록전거 (33)";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(212, 119);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(79, 12);
            this.label29.TabIndex = 0;
            this.label29.Text = "기술수준 (35)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(212, 11);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(91, 12);
            this.label30.TabIndex = 0;
            this.label30.Text = "수정레코드 (29)";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(212, 92);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(115, 12);
            this.label31.TabIndex = 0;
            this.label31.Text = "시청각자료유형 (34)";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 200);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 12);
            this.label32.TabIndex = 0;
            this.label32.Text = "대학부호 (27~28)";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(212, 38);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(103, 12);
            this.label33.TabIndex = 0;
            this.label33.Text = "개별자료형태 (30)";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 173);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(91, 12);
            this.label34.TabIndex = 0;
            this.label34.Text = "이용자수준 (23)";
            // 
            // Help_008
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 311);
            this.Controls.Add(this.Btn_Close);
            this.Controls.Add(this.Btn_Apply);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.PanelDVD);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PanelMusic);
            this.Name = "Help_008";
            this.Text = "Help_008";
            this.Load += new System.EventHandler(this.Help_008_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PanelMusic.ResumeLayout(false);
            this.PanelMusic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.PanelDVD.ResumeLayout(false);
            this.PanelDVD.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_Text;
        private System.Windows.Forms.Label lbl_008;
        private System.Windows.Forms.Panel PanelMusic;
        private System.Windows.Forms.TextBox tb_19;
        private System.Windows.Forms.TextBox tb_16;
        private System.Windows.Forms.TextBox tb_DontTouch07;
        private System.Windows.Forms.TextBox PubYear2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lbl_ChangeTarget;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_21;
        private System.Windows.Forms.Label lbl_VisibleTarget;
        private System.Windows.Forms.TextBox tb_33;
        private System.Windows.Forms.TextBox tb_36;
        private System.Windows.Forms.TextBox tb_31;
        private System.Windows.Forms.TextBox tb_29;
        private System.Windows.Forms.TextBox tb_39;
        private System.Windows.Forms.TextBox tb_34;
        private System.Windows.Forms.TextBox tb_27;
        private System.Windows.Forms.TextBox tb_23;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn value;
        private System.Windows.Forms.DataGridViewTextBoxColumn content;
        private System.Windows.Forms.Button Btn_Close;
        private System.Windows.Forms.Button Btn_Apply;
        private System.Windows.Forms.TextBox tb_22;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_24;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_25;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_DontTouch01;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel PanelDVD;
        private System.Windows.Forms.TextBox dvd_33;
        private System.Windows.Forms.TextBox dvd_36;
        private System.Windows.Forms.TextBox dvd_35;
        private System.Windows.Forms.TextBox dvd_16;
        private System.Windows.Forms.TextBox dvd_01;
        private System.Windows.Forms.TextBox dvd_07;
        private System.Windows.Forms.TextBox dvd_29;
        private System.Windows.Forms.TextBox dvd_39;
        private System.Windows.Forms.TextBox dvd_12;
        private System.Windows.Forms.TextBox dvd_27;
        private System.Windows.Forms.TextBox dvd_34;
        private System.Windows.Forms.TextBox dvd_30;
        private System.Windows.Forms.TextBox dvd_23;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox dvd_19;
        private System.Windows.Forms.TextBox PubYear1;
        private System.Windows.Forms.TextBox dvd_08;
    }
}