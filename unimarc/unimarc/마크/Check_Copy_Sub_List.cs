﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;

namespace UniMarc.마크
{
    public partial class Check_Copy_Sub_List : Form
    {
        Helper_DB db = new Helper_DB();
        Check_copy cc;
        public Check_Copy_Sub_List(Check_copy _cc)
        {
            InitializeComponent();
            cc = _cc;
        }

        private void Check_Copy_Sub_List_Load(object sender, EventArgs e)
        {
            Init();
        }

        void Init()
        {
            db.DBcon();
            cb_Stat.Items.Add("진행");
            cb_Stat.Items.Add("완료");
            cb_Stat.SelectedItem = "진행";

            int basic = 2020;
            int Iyears = DateTime.Now.Year;

            while (basic <= Iyears)
            {
                cb_Years.Items.Add(basic);
                basic++;
            }
            cb_Years.SelectedItem = Iyears.ToString();
        }

        private void cb_Stat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_Stat.SelectedItem.ToString() == "완료")
                cb_Years.Visible = true;
            else
                cb_Years.Visible = false;

            TakeData();
        }

        void TakeData()
        {
            dataGridView1.Rows.Clear();

            string Area = "`work_list`, `date`";
            string compidx = Properties.Settings.Default.compidx;
            string stat = cb_Stat.Text;
            string cmd = "";

            if (cb_Years.Visible)
                cmd = String.Format("SELECT {0} FROM `Specs_List` WHERE `compidx` = '{1}' AND `state` = '{2}';",
                    Area, compidx, stat);
            else
                cmd = String.Format("SELECT {0} FROM `Specs_List` WHERE `compidx` = '{1}' AND `state` = '{2}' AND `date` LIKE '{3}%';",
                    Area, compidx, stat, cb_Years.Text);

            MakeGrid(db.self_Made_Cmd(cmd));
        }

        void MakeGrid(string Qurey)
        {
            string[] ary = Qurey.Split('|');

            string[] grid = { "", "" };
            

            for (int a = 0; a < ary.Length; a++)
            {
                if (a % 2 == 0) { grid[0] = ary[a]; }
                if (a % 2 == 1) { grid[1] = ary[a];dataGridView1.Rows.Add(grid); }
            }

        }

        private void btn_Select_Click(object sender, EventArgs e)
        {
            int row = dataGridView1.CurrentRow.Index;
            list_Select(row);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            list_Select(row);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter) {
                int row = dataGridView1.CurrentRow.Index;
                list_Select(row);
            }
        }

        void list_Select(int row)
        {
            string compidx = Properties.Settings.Default.compidx;
            string list_name = dataGridView1.Rows[row].Cells["list_name"].Value.ToString();
            string list_date = dataGridView1.Rows[row].Cells["list_date"].Value.ToString();

            string area = "`book_name`, `book_comp`";
            string[] search_col = { "compidx", "work_list", "date" };
            string[] search_data = { compidx, list_name, list_date };

            string cmd = db.More_DB_Search("Specs_Marc", search_col, search_data, area);
            Result(db.DB_Send_CMD_Search(cmd));
            cc.btn_SearchList.Text = list_name;
            cc.SearchCount.Value = cc.dataGridView1.Rows.Count - 1;
            this.Close();
        }

        void Result(string query)
        {
            cc.dataGridView1.Rows.Clear();

            string[] ary = query.Split('|');

            string[] grid = { "", "", "" };
            for(int a= 0; a < ary.Length; a++)
            {
                if (a % 2 == 0) { grid[0] = ary[a]; }
                if (a % 2 == 1) { grid[1] = ary[a]; cc.dataGridView1.Rows.Add(grid); }
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
