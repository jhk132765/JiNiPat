﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace UniMarc.마크
{
    public partial class CD_LP_SelectList : Form
    {
        Helper_DB db = new Helper_DB();
        CD_LP cp;
        CD_LP_List cpl;

        public CD_LP_SelectList(CD_LP _cp)
        {
            InitializeComponent();
            cp = _cp;
        }
        public CD_LP_SelectList(CD_LP_List _cp)
        {
            InitializeComponent();
            Btn_Delete.Visible = false;
            Btn_Insert.Visible = false;
            Btn_Close.Visible = false;
            cpl = _cp;
        }

        private void CD_LP_SelectList_Load(object sender, EventArgs e)
        {
            db.DBcon();

            string[] Filter = { "목록명", "등록자" };
            cb_Filter.Items.AddRange(Filter);
            cb_Filter.SelectedIndex = 0;
        }

        public void LoadList(string compidx)
        {
            string Area = "`idx`, `listname`, `date`, `user`";
            string Table = "DVD_List";
            string cmd = string.Format("SELECT {0} FROM {1} WHERE {2} = {3} ORDER BY `date` DESC;", Area, Table, "compidx", compidx);
            string res = db.DB_Send_CMD_Search(cmd);
            string[] ary_res = res.Split('|');

            string[] grid = { "", "", "", "" };
            for (int a = 0; a < ary_res.Length; a++)
            {
                if (a % 4 == 0) grid[0] = ary_res[a];
                if (a % 4 == 1) grid[1] = ary_res[a];
                if (a % 4 == 2) grid[2] = ary_res[a];
                if (a % 4 == 3) { grid[3] = ary_res[a]; dataGridView1.Rows.Add(grid); }
            }
        }

        public void LoadList(string compidx, int Filter, string Target)
        {
            string Area = "`idx`, `listname`, `date`, `user`";
            string Table = "DVD_List";
            string FilterTable;

            switch (Filter)
            {
                case 0:
                    FilterTable = "listname";
                    break;
                case 1:
                    FilterTable = "user";
                    break;
                default:
                    FilterTable = "listname";
                    break;
            }

            string cmd = string.Format("SELECT {0} FROM {1} WHERE {2} = {3} AND `{4}` = \"{5}\" ORDER BY `date` DESC;",
                Area, Table, "compidx", compidx, FilterTable, Target);
            string res = db.DB_Send_CMD_Search(cmd);
            string[] ary_res = res.Split('|');

            string[] grid = { "", "", "" };
            for (int a = 0; a < ary_res.Length; a++)
            {
                if (a % 3 == 0) grid[0] = ary_res[a];
                if (a % 3 == 1) grid[1] = ary_res[a];
                if (a % 3 == 2) { grid[2] = ary_res[a]; dataGridView1.Rows.Add(grid); }
            }
        }

        private void Btn_Insert_Click(object sender, EventArgs e)
        {
            CD_LP_AddList cD_LP_AddList = new CD_LP_AddList();
            cD_LP_AddList.Show();
        }

        private void Btn_Delete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Index < 0) return;
            if (MessageBox.Show("정말 삭제하시겠습니까?", "삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string idx = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells["idx"].Value.ToString();
                string compidx = Properties.Settings.Default.compidx;

                string cmd = db.DB_Delete("DVD_List", "compidx", compidx, "idx", idx);
                db.DB_Send_CMD_reVoid(cmd);

                MessageBox.Show("삭제되었습니다.");
            }
        }

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tb_Search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string compidx = Properties.Settings.Default.compidx;
                int ComboIndex = cb_Filter.SelectedIndex;
                string Target = tb_Search.Text;
                LoadList(compidx, ComboIndex, Target);
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            if (row < 0) return;

            string ListName = dataGridView1.Rows[row].Cells["ListTitle"].Value.ToString();
            string Date = dataGridView1.Rows[row].Cells["Date"].Value.ToString();

            if (cp != null)
                cp.MakeList(ListName, Date);
            else if (cpl != null)
                cpl.MakeList(ListName, Date);
            this.Close();
        }
    }
}
