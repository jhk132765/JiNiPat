﻿namespace WindowsFormsApp1.Mac
{
    partial class Mac_Stat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ISBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tag008 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_Lookup = new System.Windows.Forms.Button();
            this.btn_Stats = new System.Windows.Forms.Button();
            this.End_Date = new System.Windows.Forms.DateTimePicker();
            this.Start_Date = new System.Windows.Forms.DateTimePicker();
            this.tb_Search = new System.Windows.Forms.TextBox();
            this.cb_Charge = new System.Windows.Forms.ComboBox();
            this.cb_Sort = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_Search = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.ISBN,
            this.book_name,
            this.author,
            this.book_comp,
            this.tag008,
            this.charge,
            this.date});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1100, 461);
            this.dataGridView1.TabIndex = 8;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.Visible = false;
            // 
            // ISBN
            // 
            this.ISBN.HeaderText = "ISBN";
            this.ISBN.Name = "ISBN";
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 300;
            // 
            // author
            // 
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            // 
            // tag008
            // 
            this.tag008.HeaderText = "008_TAG";
            this.tag008.Name = "tag008";
            this.tag008.Width = 220;
            // 
            // charge
            // 
            this.charge.HeaderText = "입력자";
            this.charge.Name = "charge";
            this.charge.Width = 70;
            // 
            // date
            // 
            this.date.HeaderText = "입력시간";
            this.date.Name = "date";
            this.date.Width = 150;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btn_Close);
            this.panel2.Controls.Add(this.btn_Lookup);
            this.panel2.Controls.Add(this.btn_Stats);
            this.panel2.Controls.Add(this.End_Date);
            this.panel2.Controls.Add(this.Start_Date);
            this.panel2.Controls.Add(this.tb_Search);
            this.panel2.Controls.Add(this.cb_Charge);
            this.panel2.Controls.Add(this.cb_Sort);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.cb_Search);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1100, 34);
            this.panel2.TabIndex = 7;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(1018, 5);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 222;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_Lookup
            // 
            this.btn_Lookup.Location = new System.Drawing.Point(862, 5);
            this.btn_Lookup.Name = "btn_Lookup";
            this.btn_Lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_Lookup.TabIndex = 222;
            this.btn_Lookup.Text = "조    회";
            this.btn_Lookup.UseVisualStyleBackColor = true;
            this.btn_Lookup.Click += new System.EventHandler(this.btn_Lookup_Click);
            // 
            // btn_Stats
            // 
            this.btn_Stats.Location = new System.Drawing.Point(940, 5);
            this.btn_Stats.Name = "btn_Stats";
            this.btn_Stats.Size = new System.Drawing.Size(75, 23);
            this.btn_Stats.TabIndex = 222;
            this.btn_Stats.Text = "통    계";
            this.btn_Stats.UseVisualStyleBackColor = true;
            this.btn_Stats.Click += new System.EventHandler(this.btn_Stats_Click);
            // 
            // End_Date
            // 
            this.End_Date.CustomFormat = "yyyy-MM-dd";
            this.End_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.End_Date.Location = new System.Drawing.Point(625, 6);
            this.End_Date.Name = "End_Date";
            this.End_Date.ShowCheckBox = true;
            this.End_Date.Size = new System.Drawing.Size(99, 21);
            this.End_Date.TabIndex = 221;
            // 
            // Start_Date
            // 
            this.Start_Date.CustomFormat = "yyyy-MM-dd";
            this.Start_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Start_Date.Location = new System.Drawing.Point(507, 6);
            this.Start_Date.Name = "Start_Date";
            this.Start_Date.ShowCheckBox = true;
            this.Start_Date.Size = new System.Drawing.Size(99, 21);
            this.Start_Date.TabIndex = 221;
            // 
            // tb_Search
            // 
            this.tb_Search.Location = new System.Drawing.Point(125, 6);
            this.tb_Search.Name = "tb_Search";
            this.tb_Search.Size = new System.Drawing.Size(184, 21);
            this.tb_Search.TabIndex = 2;
            this.tb_Search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_Search_KeyDown);
            // 
            // cb_Charge
            // 
            this.cb_Charge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Charge.FormattingEnabled = true;
            this.cb_Charge.Location = new System.Drawing.Point(781, 6);
            this.cb_Charge.Name = "cb_Charge";
            this.cb_Charge.Size = new System.Drawing.Size(72, 20);
            this.cb_Charge.TabIndex = 1;
            // 
            // cb_Sort
            // 
            this.cb_Sort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Sort.FormattingEnabled = true;
            this.cb_Sort.Location = new System.Drawing.Point(355, 6);
            this.cb_Sort.Name = "cb_Sort";
            this.cb_Sort.Size = new System.Drawing.Size(82, 20);
            this.cb_Sort.TabIndex = 1;
            this.cb_Sort.SelectedIndexChanged += new System.EventHandler(this.cb_Sort_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(608, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "~";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(738, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "입력자";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(449, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "저장일자";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(324, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "정렬";
            // 
            // cb_Search
            // 
            this.cb_Search.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Search.FormattingEnabled = true;
            this.cb_Search.Location = new System.Drawing.Point(38, 6);
            this.cb_Search.Name = "cb_Search";
            this.cb_Search.Size = new System.Drawing.Size(81, 20);
            this.cb_Search.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "검색";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1100, 461);
            this.panel1.TabIndex = 9;
            // 
            // Mac_Stat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 495);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "Mac_Stat";
            this.Text = "마크 통계";
            this.Load += new System.EventHandler(this.Mac_Stat_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISBN;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn tag008;
        private System.Windows.Forms.DataGridViewTextBoxColumn charge;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_Stats;
        private System.Windows.Forms.DateTimePicker End_Date;
        private System.Windows.Forms.DateTimePicker Start_Date;
        private System.Windows.Forms.TextBox tb_Search;
        private System.Windows.Forms.ComboBox cb_Charge;
        private System.Windows.Forms.ComboBox cb_Sort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_Search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Lookup;
        private System.Windows.Forms.Panel panel1;
    }
}