﻿namespace ExcelTest
{
    partial class Marc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Marc));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.etc1 = new System.Windows.Forms.RichTextBox();
            this.etc2 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label98 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label99 = new System.Windows.Forms.Label();
            this.text008col = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label101 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label102 = new System.Windows.Forms.Label();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.text008gov = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.col008res = new System.Windows.Forms.Label();
            this.gov008res = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.Btn_Memo = new System.Windows.Forms.Button();
            this.List_Book = new System.Windows.Forms.DataGridView();
            this.list_idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ISBN13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.url = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marc_idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.db_marc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCheck = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaveDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Save = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.text008 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Reflesh008 = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.input_date = new System.Windows.Forms.DateTimePicker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_preview = new System.Windows.Forms.Button();
            this.cb_grade = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rb_Sort = new System.Windows.Forms.RadioButton();
            this.rb_Filter = new System.Windows.Forms.RadioButton();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_mk_marcList = new System.Windows.Forms.Button();
            this.btn_CopySelect = new System.Windows.Forms.Button();
            this.btn_FilterReturn = new System.Windows.Forms.Button();
            this.btn_Search = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.btn_FillBlank = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Btn_interlock = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.GridView020 = new System.Windows.Forms.DataGridView();
            this.CheckSet = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Text020a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Text020g = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Text020c = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.text505a = new System.Windows.Forms.TextBox();
            this.GridView505 = new System.Windows.Forms.DataGridView();
            this.text505n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text505t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text505d = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text505e = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label42 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.GridView246 = new System.Windows.Forms.DataGridView();
            this.Text246Jisi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Text246i = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Text246a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Text246b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Text246n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Text246p = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label39 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.text900a = new System.Windows.Forms.TextBox();
            this.text910a = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.GridView440 = new System.Windows.Forms.DataGridView();
            this.text440a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440p = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440vNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440vTxt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text440x = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.text700a = new System.Windows.Forms.TextBox();
            this.text710a = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.GridView490 = new System.Windows.Forms.DataGridView();
            this.text490a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.text490v = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.text300a = new System.Windows.Forms.TextBox();
            this.text300c1 = new System.Windows.Forms.TextBox();
            this.text300c2 = new System.Windows.Forms.TextBox();
            this.text300b = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.text300e = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.text546a = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.text260a = new System.Windows.Forms.TextBox();
            this.text260b = new System.Windows.Forms.TextBox();
            this.text260c = new System.Windows.Forms.TextBox();
            this.text260g = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.text504a = new System.Windows.Forms.TextBox();
            this.text536a = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.basicHeadBox = new System.Windows.Forms.TextBox();
            this.invertCheck = new System.Windows.Forms.CheckBox();
            this.rbtn_100 = new System.Windows.Forms.RadioButton();
            this.rbtn_111 = new System.Windows.Forms.RadioButton();
            this.rbtn_110 = new System.Windows.Forms.RadioButton();
            this.text650a = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.text041a = new System.Windows.Forms.TextBox();
            this.text041b = new System.Windows.Forms.TextBox();
            this.text041h = new System.Windows.Forms.TextBox();
            this.text041k = new System.Windows.Forms.TextBox();
            this.text586a = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textKDC4 = new System.Windows.Forms.TextBox();
            this.textKDC5 = new System.Windows.Forms.TextBox();
            this.textKDC6 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textDDC21 = new System.Windows.Forms.TextBox();
            this.textDDC22 = new System.Windows.Forms.TextBox();
            this.textDDC23 = new System.Windows.Forms.TextBox();
            this.text520a = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.text245a = new System.Windows.Forms.TextBox();
            this.text245b = new System.Windows.Forms.TextBox();
            this.text245x = new System.Windows.Forms.TextBox();
            this.text245n = new System.Windows.Forms.TextBox();
            this.text245d = new System.Windows.Forms.TextBox();
            this.text245e = new System.Windows.Forms.TextBox();
            this.text245p = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.text521a = new System.Windows.Forms.TextBox();
            this.text525a = new System.Windows.Forms.TextBox();
            this.text940a = new System.Windows.Forms.TextBox();
            this.text653a = new System.Windows.Forms.TextBox();
            this.text507t = new System.Windows.Forms.TextBox();
            this.text500a = new System.Windows.Forms.TextBox();
            this.text507a = new System.Windows.Forms.TextBox();
            this.text250a = new System.Windows.Forms.TextBox();
            this.lbl_SaveData = new System.Windows.Forms.Label();
            this.lbl_ISBN = new System.Windows.Forms.Label();
            this.lbl_BookList = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.List_Book)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView020)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView505)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView246)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView440)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView490)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.AcceptsTab = true;
            this.richTextBox1.BackColor = System.Drawing.Color.LightGray;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("굴림체", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.richTextBox1.Location = new System.Drawing.Point(3, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(932, 810);
            this.richTextBox1.TabIndex = 32;
            this.richTextBox1.Text = "";
            this.richTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyDown);
            // 
            // etc1
            // 
            this.etc1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.etc1.Font = new System.Drawing.Font("굴림체", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.etc1.Location = new System.Drawing.Point(1512, 348);
            this.etc1.Name = "etc1";
            this.etc1.Size = new System.Drawing.Size(287, 187);
            this.etc1.TabIndex = 32;
            this.etc1.Text = "Remark1";
            this.etc1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.etc_KeyDown);
            // 
            // etc2
            // 
            this.etc2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.etc2.Font = new System.Drawing.Font("굴림체", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.etc2.Location = new System.Drawing.Point(1512, 541);
            this.etc2.Name = "etc2";
            this.etc2.Size = new System.Drawing.Size(287, 184);
            this.etc2.TabIndex = 32;
            this.etc2.Text = "Remark2";
            this.etc2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.etc_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(933, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "입력일자";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1102, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 206;
            this.label2.Text = "이용자";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(1143, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(79, 20);
            this.comboBox1.TabIndex = 207;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1233, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 206;
            this.label3.Text = "자료형식";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(1287, 7);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(79, 20);
            this.comboBox2.TabIndex = 207;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(1374, 11);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(53, 12);
            this.label98.TabIndex = 14;
            this.label98.Text = "내용형식";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(1428, 7);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(118, 20);
            this.comboBox3.TabIndex = 207;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(1675, 11);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(29, 12);
            this.label99.TabIndex = 206;
            this.label99.Text = "대학";
            // 
            // text008col
            // 
            this.text008col.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text008col.Location = new System.Drawing.Point(1704, 7);
            this.text008col.Name = "text008col";
            this.text008col.Size = new System.Drawing.Size(41, 21);
            this.text008col.TabIndex = 204;
            this.text008col.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text008col_KeyDown);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(1233, 33);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(53, 12);
            this.label100.TabIndex = 206;
            this.label100.Text = "문학형식";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(1287, 29);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(79, 20);
            this.comboBox4.TabIndex = 207;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(1398, 33);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(29, 12);
            this.label101.TabIndex = 206;
            this.label101.Text = "전기";
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(1428, 29);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(97, 20);
            this.comboBox5.TabIndex = 207;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(1538, 33);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(29, 12);
            this.label102.TabIndex = 206;
            this.label102.Text = "언어";
            // 
            // comboBox6
            // 
            this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(1568, 29);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(97, 20);
            this.comboBox6.TabIndex = 207;
            this.comboBox6.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // text008gov
            // 
            this.text008gov.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text008gov.Location = new System.Drawing.Point(1704, 29);
            this.text008gov.Name = "text008gov";
            this.text008gov.Size = new System.Drawing.Size(41, 21);
            this.text008gov.TabIndex = 204;
            this.text008gov.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text008col_KeyDown);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(1675, 33);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(29, 12);
            this.label103.TabIndex = 206;
            this.label103.Text = "기관";
            // 
            // col008res
            // 
            this.col008res.AutoSize = true;
            this.col008res.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.col008res.ForeColor = System.Drawing.Color.Blue;
            this.col008res.Location = new System.Drawing.Point(1747, 11);
            this.col008res.Name = "col008res";
            this.col008res.Size = new System.Drawing.Size(13, 12);
            this.col008res.TabIndex = 206;
            this.col008res.Text = "  ";
            this.col008res.TextChanged += new System.EventHandler(this.col008res_TextChanged);
            // 
            // gov008res
            // 
            this.gov008res.AutoSize = true;
            this.gov008res.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.gov008res.ForeColor = System.Drawing.Color.Blue;
            this.gov008res.Location = new System.Drawing.Point(1747, 33);
            this.gov008res.Name = "gov008res";
            this.gov008res.Size = new System.Drawing.Size(13, 12);
            this.gov008res.TabIndex = 206;
            this.gov008res.Text = "  ";
            this.gov008res.TextChanged += new System.EventHandler(this.col008res_TextChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(931, 31);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(112, 16);
            this.checkBox1.TabIndex = 213;
            this.checkBox1.Text = "회의간행물 여부";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(1059, 31);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(112, 16);
            this.checkBox2.TabIndex = 213;
            this.checkBox2.Text = "기념논문집 여부";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // comboBox7
            // 
            this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(1547, 7);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(118, 20);
            this.comboBox7.TabIndex = 207;
            this.comboBox7.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // Btn_Memo
            // 
            this.Btn_Memo.Location = new System.Drawing.Point(1512, 169);
            this.Btn_Memo.Name = "Btn_Memo";
            this.Btn_Memo.Size = new System.Drawing.Size(77, 23);
            this.Btn_Memo.TabIndex = 215;
            this.Btn_Memo.Text = "메모장";
            this.Btn_Memo.UseVisualStyleBackColor = true;
            this.Btn_Memo.Click += new System.EventHandler(this.Btn_Memo_Click);
            // 
            // List_Book
            // 
            this.List_Book.AllowDrop = true;
            this.List_Book.AllowUserToAddRows = false;
            this.List_Book.AllowUserToDeleteRows = false;
            this.List_Book.AllowUserToResizeColumns = false;
            this.List_Book.BackgroundColor = System.Drawing.Color.Gray;
            this.List_Book.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.List_Book.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.List_Book.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.list_idx,
            this.ISBN13,
            this.num,
            this.book_name,
            this.author,
            this.book_comp,
            this.count,
            this.pay,
            this.url,
            this.marc_idx,
            this.db_marc,
            this.grade,
            this.colCheck,
            this.user,
            this.SaveDate});
            this.List_Book.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.List_Book.Location = new System.Drawing.Point(12, 74);
            this.List_Book.MultiSelect = false;
            this.List_Book.Name = "List_Book";
            this.List_Book.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.List_Book.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.List_Book.RowTemplate.Height = 23;
            this.List_Book.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.List_Book.Size = new System.Drawing.Size(543, 822);
            this.List_Book.TabIndex = 217;
            this.List_Book.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.List_Book_CellClick);
            this.List_Book.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.List_Book_RowPostPaint);
            this.List_Book.DragDrop += new System.Windows.Forms.DragEventHandler(this.List_Book_DragDrop);
            this.List_Book.DragOver += new System.Windows.Forms.DragEventHandler(this.List_Book_DragOver);
            this.List_Book.KeyDown += new System.Windows.Forms.KeyEventHandler(this.List_Book_KeyDown);
            this.List_Book.MouseDown += new System.Windows.Forms.MouseEventHandler(this.List_Book_MouseDown);
            this.List_Book.MouseMove += new System.Windows.Forms.MouseEventHandler(this.List_Book_MouseMove);
            // 
            // list_idx
            // 
            this.list_idx.HeaderText = "list_idx";
            this.list_idx.Name = "list_idx";
            this.list_idx.ReadOnly = true;
            this.list_idx.Visible = false;
            this.list_idx.Width = 30;
            // 
            // ISBN13
            // 
            this.ISBN13.FillWeight = 136.2398F;
            this.ISBN13.HeaderText = "ISBN13";
            this.ISBN13.Name = "ISBN13";
            this.ISBN13.ReadOnly = true;
            this.ISBN13.Visible = false;
            // 
            // num
            // 
            this.num.HeaderText = "연번";
            this.num.Name = "num";
            this.num.ReadOnly = true;
            this.num.Width = 50;
            // 
            // book_name
            // 
            this.book_name.FillWeight = 135.5107F;
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.ReadOnly = true;
            this.book_name.Width = 150;
            // 
            // author
            // 
            this.author.FillWeight = 67.49011F;
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            this.author.ReadOnly = true;
            this.author.Width = 80;
            // 
            // book_comp
            // 
            this.book_comp.FillWeight = 76.49199F;
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            this.book_comp.ReadOnly = true;
            this.book_comp.Width = 80;
            // 
            // count
            // 
            this.count.HeaderText = "C";
            this.count.Name = "count";
            this.count.ReadOnly = true;
            this.count.Width = 30;
            // 
            // pay
            // 
            this.pay.FillWeight = 84.26746F;
            this.pay.HeaderText = "정가";
            this.pay.Name = "pay";
            this.pay.ReadOnly = true;
            this.pay.Width = 62;
            // 
            // url
            // 
            this.url.HeaderText = "url";
            this.url.Name = "url";
            this.url.ReadOnly = true;
            this.url.Visible = false;
            // 
            // marc_idx
            // 
            this.marc_idx.HeaderText = "marc_idx";
            this.marc_idx.Name = "marc_idx";
            this.marc_idx.ReadOnly = true;
            this.marc_idx.Visible = false;
            this.marc_idx.Width = 30;
            // 
            // db_marc
            // 
            this.db_marc.HeaderText = "marc";
            this.db_marc.Name = "db_marc";
            this.db_marc.ReadOnly = true;
            this.db_marc.Visible = false;
            // 
            // grade
            // 
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.grade.DefaultCellStyle = dataGridViewCellStyle2;
            this.grade.HeaderText = "등급";
            this.grade.Name = "grade";
            this.grade.ReadOnly = true;
            this.grade.Visible = false;
            this.grade.Width = 50;
            // 
            // colCheck
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colCheck.DefaultCellStyle = dataGridViewCellStyle3;
            this.colCheck.HeaderText = "V";
            this.colCheck.Name = "colCheck";
            this.colCheck.ReadOnly = true;
            this.colCheck.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colCheck.Width = 30;
            // 
            // user
            // 
            this.user.HeaderText = "작업자";
            this.user.Name = "user";
            this.user.ReadOnly = true;
            this.user.Visible = false;
            // 
            // SaveDate
            // 
            this.SaveDate.HeaderText = "저장시각";
            this.SaveDate.Name = "SaveDate";
            this.SaveDate.ReadOnly = true;
            this.SaveDate.Visible = false;
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(1512, 197);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(77, 23);
            this.btn_Save.TabIndex = 215;
            this.btn_Save.Text = "저   장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 12);
            this.label4.TabIndex = 206;
            this.label4.Text = "008 Tag";
            // 
            // text008
            // 
            this.text008.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text008.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text008.Location = new System.Drawing.Point(60, 3);
            this.text008.MaxLength = 40;
            this.text008.Name = "text008";
            this.text008.Size = new System.Drawing.Size(272, 21);
            this.text008.TabIndex = 204;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btn_Reflesh008);
            this.panel3.Controls.Add(this.text008);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(561, 13);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(364, 30);
            this.panel3.TabIndex = 219;
            // 
            // btn_Reflesh008
            // 
            this.btn_Reflesh008.BackColor = System.Drawing.SystemColors.WindowText;
            this.btn_Reflesh008.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Reflesh008.BackgroundImage")));
            this.btn_Reflesh008.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Reflesh008.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_Reflesh008.Location = new System.Drawing.Point(334, 0);
            this.btn_Reflesh008.Name = "btn_Reflesh008";
            this.btn_Reflesh008.Size = new System.Drawing.Size(28, 28);
            this.btn_Reflesh008.TabIndex = 207;
            this.btn_Reflesh008.UseVisualStyleBackColor = false;
            this.btn_Reflesh008.Click += new System.EventHandler(this.btn_Reflesh008_Click);
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(1512, 255);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(77, 23);
            this.btn_close.TabIndex = 215;
            this.btn_close.Text = "닫   기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // input_date
            // 
            this.input_date.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.input_date.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.input_date.Checked = false;
            this.input_date.CustomFormat = "yyyy-MM-dd";
            this.input_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.input_date.Location = new System.Drawing.Point(992, 7);
            this.input_date.Name = "input_date";
            this.input_date.ShowCheckBox = true;
            this.input_date.Size = new System.Drawing.Size(101, 21);
            this.input_date.TabIndex = 220;
            this.input_date.ValueChanged += new System.EventHandler(this.input_date_ValueChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(1591, 53);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(208, 289);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 221;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // btn_preview
            // 
            this.btn_preview.Location = new System.Drawing.Point(1512, 226);
            this.btn_preview.Name = "btn_preview";
            this.btn_preview.Size = new System.Drawing.Size(77, 23);
            this.btn_preview.TabIndex = 215;
            this.btn_preview.Text = "미리보기";
            this.btn_preview.UseVisualStyleBackColor = true;
            this.btn_preview.Click += new System.EventHandler(this.Btn_preview_Click);
            // 
            // cb_grade
            // 
            this.cb_grade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_grade.FormattingEnabled = true;
            this.cb_grade.Items.AddRange(new object[] {
            "A (F9)",
            "B (F10)",
            "C (F11)",
            "D (F12)"});
            this.cb_grade.Location = new System.Drawing.Point(1513, 93);
            this.cb_grade.Name = "cb_grade";
            this.cb_grade.Size = new System.Drawing.Size(75, 20);
            this.cb_grade.TabIndex = 222;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(1519, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 12);
            this.label6.TabIndex = 223;
            this.label6.Text = "마크 등급";
            // 
            // rb_Sort
            // 
            this.rb_Sort.AutoSize = true;
            this.rb_Sort.Location = new System.Drawing.Point(76, 8);
            this.rb_Sort.Name = "rb_Sort";
            this.rb_Sort.Size = new System.Drawing.Size(47, 16);
            this.rb_Sort.TabIndex = 224;
            this.rb_Sort.TabStop = true;
            this.rb_Sort.Text = "정렬";
            this.rb_Sort.UseVisualStyleBackColor = true;
            this.rb_Sort.CheckedChanged += new System.EventHandler(this.Radio_Sort_CheckedChanged);
            // 
            // rb_Filter
            // 
            this.rb_Filter.AutoSize = true;
            this.rb_Filter.Location = new System.Drawing.Point(123, 8);
            this.rb_Filter.Name = "rb_Filter";
            this.rb_Filter.Size = new System.Drawing.Size(47, 16);
            this.rb_Filter.TabIndex = 224;
            this.rb_Filter.TabStop = true;
            this.rb_Filter.Text = "필터";
            this.rb_Filter.UseVisualStyleBackColor = true;
            this.rb_Filter.CheckedChanged += new System.EventHandler(this.Radio_Sort_CheckedChanged);
            // 
            // comboBox8
            // 
            this.comboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(175, 6);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(79, 20);
            this.comboBox8.TabIndex = 225;
            this.comboBox8.SelectedIndexChanged += new System.EventHandler(this.comboBox8_SelectedIndexChanged);
            // 
            // comboBox9
            // 
            this.comboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(256, 6);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(52, 20);
            this.comboBox9.TabIndex = 225;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_mk_marcList);
            this.panel1.Controls.Add(this.btn_CopySelect);
            this.panel1.Controls.Add(this.btn_FilterReturn);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.rb_Sort);
            this.panel1.Controls.Add(this.comboBox9);
            this.panel1.Controls.Add(this.rb_Filter);
            this.panel1.Controls.Add(this.comboBox8);
            this.panel1.Location = new System.Drawing.Point(12, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 35);
            this.panel1.TabIndex = 226;
            // 
            // btn_mk_marcList
            // 
            this.btn_mk_marcList.Location = new System.Drawing.Point(433, 5);
            this.btn_mk_marcList.Name = "btn_mk_marcList";
            this.btn_mk_marcList.Size = new System.Drawing.Size(85, 23);
            this.btn_mk_marcList.TabIndex = 227;
            this.btn_mk_marcList.Text = "마크목록생성";
            this.btn_mk_marcList.UseVisualStyleBackColor = true;
            this.btn_mk_marcList.Click += new System.EventHandler(this.btn_mk_marcList_Click);
            // 
            // btn_CopySelect
            // 
            this.btn_CopySelect.BackColor = System.Drawing.Color.Khaki;
            this.btn_CopySelect.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CopySelect.Location = new System.Drawing.Point(387, 5);
            this.btn_CopySelect.Name = "btn_CopySelect";
            this.btn_CopySelect.Size = new System.Drawing.Size(30, 23);
            this.btn_CopySelect.TabIndex = 319;
            this.btn_CopySelect.Text = "0";
            this.btn_CopySelect.UseVisualStyleBackColor = false;
            this.btn_CopySelect.Click += new System.EventHandler(this.btn_CopySelect_Click);
            // 
            // btn_FilterReturn
            // 
            this.btn_FilterReturn.Location = new System.Drawing.Point(8, 5);
            this.btn_FilterReturn.Name = "btn_FilterReturn";
            this.btn_FilterReturn.Size = new System.Drawing.Size(63, 23);
            this.btn_FilterReturn.TabIndex = 229;
            this.btn_FilterReturn.Text = "원래대로";
            this.btn_FilterReturn.UseVisualStyleBackColor = true;
            this.btn_FilterReturn.Click += new System.EventHandler(this.btn_FilterReturn_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(314, 5);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(57, 23);
            this.btn_Search.TabIndex = 227;
            this.btn_Search.Text = "검   색";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.BackColor = System.Drawing.Color.White;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(514, 79);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 227;
            this.checkBox3.UseVisualStyleBackColor = false;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // btn_FillBlank
            // 
            this.btn_FillBlank.Location = new System.Drawing.Point(1512, 119);
            this.btn_FillBlank.Name = "btn_FillBlank";
            this.btn_FillBlank.Size = new System.Drawing.Size(77, 44);
            this.btn_FillBlank.TabIndex = 228;
            this.btn_FillBlank.Text = "미소장 마크 코리스\r\n칸채우기";
            this.btn_FillBlank.UseVisualStyleBackColor = true;
            this.btn_FillBlank.Click += new System.EventHandler(this.btn_FillBlank_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(562, 57);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(946, 842);
            this.tabControl1.TabIndex = 311;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage1.Controls.Add(this.richTextBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(938, 816);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "마크 편집";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(938, 816);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "마크 칸채우기";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.Btn_interlock);
            this.panel4.Controls.Add(this.groupBox5);
            this.panel4.Controls.Add(this.groupBox10);
            this.panel4.Controls.Add(this.label45);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.label47);
            this.panel4.Controls.Add(this.label46);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.GridView246);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.groupBox9);
            this.panel4.Controls.Add(this.GridView440);
            this.panel4.Controls.Add(this.groupBox8);
            this.panel4.Controls.Add(this.GridView490);
            this.panel4.Controls.Add(this.groupBox7);
            this.panel4.Controls.Add(this.text546a);
            this.panel4.Controls.Add(this.groupBox6);
            this.panel4.Controls.Add(this.text504a);
            this.panel4.Controls.Add(this.text536a);
            this.panel4.Controls.Add(this.groupBox4);
            this.panel4.Controls.Add(this.text650a);
            this.panel4.Controls.Add(this.groupBox3);
            this.panel4.Controls.Add(this.text586a);
            this.panel4.Controls.Add(this.groupBox2);
            this.panel4.Controls.Add(this.text520a);
            this.panel4.Controls.Add(this.groupBox1);
            this.panel4.Controls.Add(this.text521a);
            this.panel4.Controls.Add(this.text525a);
            this.panel4.Controls.Add(this.text940a);
            this.panel4.Controls.Add(this.text653a);
            this.panel4.Controls.Add(this.text507t);
            this.panel4.Controls.Add(this.text500a);
            this.panel4.Controls.Add(this.text507a);
            this.panel4.Controls.Add(this.text250a);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(932, 810);
            this.panel4.TabIndex = 316;
            // 
            // Btn_interlock
            // 
            this.Btn_interlock.Location = new System.Drawing.Point(790, 45);
            this.Btn_interlock.Name = "Btn_interlock";
            this.Btn_interlock.Size = new System.Drawing.Size(137, 55);
            this.Btn_interlock.TabIndex = 309;
            this.Btn_interlock.Text = "태그연동\r\n(041-546)";
            this.Btn_interlock.UseVisualStyleBackColor = true;
            this.Btn_interlock.Click += new System.EventHandler(this.Btn_interlock_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox5.Controls.Add(this.GridView020);
            this.groupBox5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(414, 100);
            this.groupBox5.TabIndex = 304;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "020";
            // 
            // GridView020
            // 
            this.GridView020.AllowDrop = true;
            this.GridView020.AllowUserToAddRows = false;
            this.GridView020.AllowUserToResizeRows = false;
            this.GridView020.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView020.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckSet,
            this.Text020a,
            this.Text020g,
            this.Text020c});
            this.GridView020.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridView020.Location = new System.Drawing.Point(3, 17);
            this.GridView020.Name = "GridView020";
            this.GridView020.RowHeadersVisible = false;
            this.GridView020.RowHeadersWidth = 30;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GridView020.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.GridView020.RowTemplate.Height = 23;
            this.GridView020.Size = new System.Drawing.Size(408, 80);
            this.GridView020.TabIndex = 244;
            this.GridView020.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridView_KeyDown);
            // 
            // CheckSet
            // 
            this.CheckSet.HeaderText = "세트";
            this.CheckSet.Name = "CheckSet";
            this.CheckSet.Width = 40;
            // 
            // Text020a
            // 
            this.Text020a.HeaderText = "ISBN(a)";
            this.Text020a.Name = "Text020a";
            this.Text020a.Width = 140;
            // 
            // Text020g
            // 
            this.Text020g.HeaderText = "부가기호(g)";
            this.Text020g.Name = "Text020g";
            // 
            // Text020c
            // 
            this.Text020c.HeaderText = "가격(c)";
            this.Text020c.Name = "Text020c";
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox10.Controls.Add(this.text505a);
            this.groupBox10.Controls.Add(this.GridView505);
            this.groupBox10.Controls.Add(this.label42);
            this.groupBox10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox10.Location = new System.Drawing.Point(3, 624);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(414, 129);
            this.groupBox10.TabIndex = 308;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "505";
            // 
            // text505a
            // 
            this.text505a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text505a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text505a.Location = new System.Drawing.Point(18, 20);
            this.text505a.Multiline = true;
            this.text505a.Name = "text505a";
            this.text505a.Size = new System.Drawing.Size(389, 33);
            this.text505a.TabIndex = 270;
            // 
            // GridView505
            // 
            this.GridView505.AllowDrop = true;
            this.GridView505.AllowUserToAddRows = false;
            this.GridView505.AllowUserToResizeRows = false;
            this.GridView505.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView505.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.text505n,
            this.text505t,
            this.text505d,
            this.text505e});
            this.GridView505.Location = new System.Drawing.Point(6, 54);
            this.GridView505.Name = "GridView505";
            this.GridView505.RowHeadersVisible = false;
            this.GridView505.RowHeadersWidth = 30;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GridView505.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.GridView505.RowTemplate.Height = 23;
            this.GridView505.Size = new System.Drawing.Size(401, 71);
            this.GridView505.TabIndex = 246;
            this.GridView505.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridView_KeyDown);
            // 
            // text505n
            // 
            this.text505n.HeaderText = "505n";
            this.text505n.Name = "text505n";
            this.text505n.Width = 50;
            // 
            // text505t
            // 
            this.text505t.HeaderText = "505t";
            this.text505t.Name = "text505t";
            this.text505t.Width = 130;
            // 
            // text505d
            // 
            this.text505d.HeaderText = "505d";
            this.text505d.Name = "text505d";
            // 
            // text505e
            // 
            this.text505e.HeaderText = "505e";
            this.text505e.Name = "text505e";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label42.Location = new System.Drawing.Point(4, 23);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(13, 12);
            this.label42.TabIndex = 255;
            this.label42.Text = "a";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.Location = new System.Drawing.Point(483, 789);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(57, 12);
            this.label45.TabIndex = 255;
            this.label45.Text = "수상주기";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label41.Location = new System.Drawing.Point(444, 574);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(30, 12);
            this.label41.TabIndex = 255;
            this.label41.Text = "507t";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.Location = new System.Drawing.Point(444, 745);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(96, 12);
            this.label47.TabIndex = 255;
            this.label47.Text = "이용자대상주기";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label46.Location = new System.Drawing.Point(5, 453);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(57, 12);
            this.label46.TabIndex = 255;
            this.label46.Text = "부록주기";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.Location = new System.Drawing.Point(457, 767);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(83, 12);
            this.label44.TabIndex = 255;
            this.label44.Text = "기금정보주기";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.Location = new System.Drawing.Point(442, 452);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(44, 12);
            this.label37.TabIndex = 255;
            this.label37.Text = "주제명";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label40.Location = new System.Drawing.Point(444, 552);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(34, 12);
            this.label40.TabIndex = 255;
            this.label40.Text = "507a";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(444, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 24);
            this.label5.TabIndex = 255;
            this.label5.Text = "언어주기\r\n546a";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.Location = new System.Drawing.Point(442, 430);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(44, 12);
            this.label36.TabIndex = 255;
            this.label36.Text = "색인어";
            // 
            // GridView246
            // 
            this.GridView246.AllowDrop = true;
            this.GridView246.AllowUserToAddRows = false;
            this.GridView246.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridView246.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.GridView246.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Text246Jisi,
            this.Text246i,
            this.Text246a,
            this.Text246b,
            this.Text246n,
            this.Text246p});
            this.GridView246.Location = new System.Drawing.Point(434, 597);
            this.GridView246.Name = "GridView246";
            this.GridView246.RowHeadersVisible = false;
            this.GridView246.RowHeadersWidth = 30;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GridView246.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.GridView246.RowTemplate.Height = 23;
            this.GridView246.Size = new System.Drawing.Size(493, 138);
            this.GridView246.TabIndex = 250;
            this.GridView246.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridView_KeyDown);
            // 
            // Text246Jisi
            // 
            this.Text246Jisi.HeaderText = "지시";
            this.Text246Jisi.Name = "Text246Jisi";
            this.Text246Jisi.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Text246Jisi.Width = 40;
            // 
            // Text246i
            // 
            this.Text246i.HeaderText = "246i";
            this.Text246i.Name = "Text246i";
            this.Text246i.Width = 80;
            // 
            // Text246a
            // 
            this.Text246a.HeaderText = "246a";
            this.Text246a.Name = "Text246a";
            // 
            // Text246b
            // 
            this.Text246b.HeaderText = "246b";
            this.Text246b.Name = "Text246b";
            // 
            // Text246n
            // 
            this.Text246n.HeaderText = "246n";
            this.Text246n.Name = "Text246n";
            this.Text246n.Width = 50;
            // 
            // Text246p
            // 
            this.Text246p.HeaderText = "246p";
            this.Text246p.Name = "Text246p";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label39.Location = new System.Drawing.Point(7, 595);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(31, 24);
            this.label39.TabIndex = 255;
            this.label39.Text = "서지\r\n주기";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(33, 306);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 12);
            this.label23.TabIndex = 255;
            this.label23.Text = "판차";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.Location = new System.Drawing.Point(7, 761);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(31, 12);
            this.label43.TabIndex = 255;
            this.label43.Text = "요약";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.Location = new System.Drawing.Point(7, 552);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(31, 24);
            this.label38.TabIndex = 255;
            this.label38.Text = "일반\r\n주기";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(7, 284);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 12);
            this.label22.TabIndex = 255;
            this.label22.Text = "로컬서명";
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox9.Controls.Add(this.text900a);
            this.groupBox9.Controls.Add(this.text910a);
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Controls.Add(this.label34);
            this.groupBox9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox9.Location = new System.Drawing.Point(436, 353);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(491, 67);
            this.groupBox9.TabIndex = 307;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "로컬표목 (9XX) [이명, 원저자명]";
            // 
            // text900a
            // 
            this.text900a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text900a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text900a.Location = new System.Drawing.Point(44, 19);
            this.text900a.Name = "text900a";
            this.text900a.Size = new System.Drawing.Size(435, 21);
            this.text900a.TabIndex = 292;
            this.text900a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // text910a
            // 
            this.text910a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text910a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text910a.Location = new System.Drawing.Point(44, 41);
            this.text910a.Name = "text910a";
            this.text910a.Size = new System.Drawing.Size(435, 21);
            this.text910a.TabIndex = 299;
            this.text910a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.Location = new System.Drawing.Point(8, 44);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(34, 12);
            this.label35.TabIndex = 255;
            this.label35.Text = "910a";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.Location = new System.Drawing.Point(8, 22);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(34, 12);
            this.label34.TabIndex = 255;
            this.label34.Text = "900a";
            // 
            // GridView440
            // 
            this.GridView440.AllowDrop = true;
            this.GridView440.AllowUserToAddRows = false;
            this.GridView440.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridView440.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.GridView440.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView440.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.text440a,
            this.text440n,
            this.text440p,
            this.text440vNum,
            this.text440vTxt,
            this.text440x});
            this.GridView440.Location = new System.Drawing.Point(3, 475);
            this.GridView440.Name = "GridView440";
            this.GridView440.RowHeadersVisible = false;
            this.GridView440.RowHeadersWidth = 30;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GridView440.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.GridView440.RowTemplate.Height = 23;
            this.GridView440.Size = new System.Drawing.Size(597, 71);
            this.GridView440.TabIndex = 245;
            this.GridView440.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridView_KeyDown);
            // 
            // text440a
            // 
            this.text440a.HeaderText = "440a";
            this.text440a.Name = "text440a";
            // 
            // text440n
            // 
            this.text440n.HeaderText = "440n";
            this.text440n.Name = "text440n";
            this.text440n.Width = 50;
            // 
            // text440p
            // 
            this.text440p.HeaderText = "440p";
            this.text440p.Name = "text440p";
            // 
            // text440vNum
            // 
            this.text440vNum.HeaderText = "440v [숫자]";
            this.text440vNum.Name = "text440vNum";
            this.text440vNum.Width = 110;
            // 
            // text440vTxt
            // 
            this.text440vTxt.HeaderText = "440v [문자]";
            this.text440vTxt.Name = "text440vTxt";
            this.text440vTxt.Width = 110;
            // 
            // text440x
            // 
            this.text440x.HeaderText = "440x";
            this.text440x.Name = "text440x";
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox8.Controls.Add(this.text700a);
            this.groupBox8.Controls.Add(this.text710a);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox8.Location = new System.Drawing.Point(436, 280);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(491, 67);
            this.groupBox8.TabIndex = 307;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "부출표목 (7XX) [한글]";
            // 
            // text700a
            // 
            this.text700a.BackColor = System.Drawing.SystemColors.Info;
            this.text700a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text700a.Location = new System.Drawing.Point(44, 20);
            this.text700a.Name = "text700a";
            this.text700a.Size = new System.Drawing.Size(435, 21);
            this.text700a.TabIndex = 287;
            this.text700a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // text710a
            // 
            this.text710a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text710a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text710a.Location = new System.Drawing.Point(44, 42);
            this.text710a.Name = "text710a";
            this.text710a.Size = new System.Drawing.Size(435, 21);
            this.text710a.TabIndex = 298;
            this.text710a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(8, 46);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 12);
            this.label21.TabIndex = 255;
            this.label21.Text = "710a";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(8, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 12);
            this.label20.TabIndex = 255;
            this.label20.Text = "700a";
            // 
            // GridView490
            // 
            this.GridView490.AllowDrop = true;
            this.GridView490.AllowUserToAddRows = false;
            this.GridView490.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridView490.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.GridView490.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView490.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.text490a,
            this.text490v});
            this.GridView490.Location = new System.Drawing.Point(606, 475);
            this.GridView490.Name = "GridView490";
            this.GridView490.RowHeadersVisible = false;
            this.GridView490.RowHeadersWidth = 30;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GridView490.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.GridView490.RowTemplate.Height = 23;
            this.GridView490.Size = new System.Drawing.Size(321, 71);
            this.GridView490.TabIndex = 247;
            this.GridView490.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridView_KeyDown);
            // 
            // text490a
            // 
            this.text490a.HeaderText = "490a";
            this.text490a.Name = "text490a";
            this.text490a.Width = 150;
            // 
            // text490v
            // 
            this.text490v.HeaderText = "490v";
            this.text490v.Name = "text490v";
            this.text490v.Width = 150;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox7.Controls.Add(this.text300a);
            this.groupBox7.Controls.Add(this.text300c1);
            this.groupBox7.Controls.Add(this.text300c2);
            this.groupBox7.Controls.Add(this.text300b);
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this.text300e);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox7.Location = new System.Drawing.Point(215, 330);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(202, 113);
            this.groupBox7.TabIndex = 306;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "300";
            // 
            // text300a
            // 
            this.text300a.BackColor = System.Drawing.SystemColors.Info;
            this.text300a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text300a.Location = new System.Drawing.Point(17, 20);
            this.text300a.Name = "text300a";
            this.text300a.Size = new System.Drawing.Size(178, 21);
            this.text300a.TabIndex = 276;
            // 
            // text300c1
            // 
            this.text300c1.BackColor = System.Drawing.SystemColors.Info;
            this.text300c1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text300c1.Location = new System.Drawing.Point(44, 64);
            this.text300c1.Name = "text300c1";
            this.text300c1.Size = new System.Drawing.Size(59, 21);
            this.text300c1.TabIndex = 275;
            // 
            // text300c2
            // 
            this.text300c2.BackColor = System.Drawing.SystemColors.Info;
            this.text300c2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text300c2.Location = new System.Drawing.Point(136, 64);
            this.text300c2.Name = "text300c2";
            this.text300c2.Size = new System.Drawing.Size(59, 21);
            this.text300c2.TabIndex = 274;
            // 
            // text300b
            // 
            this.text300b.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text300b.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text300b.Location = new System.Drawing.Point(17, 42);
            this.text300b.Name = "text300b";
            this.text300b.Size = new System.Drawing.Size(178, 21);
            this.text300b.TabIndex = 272;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label31.Location = new System.Drawing.Point(2, 24);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(13, 12);
            this.label31.TabIndex = 255;
            this.label31.Text = "a";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(2, 46);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 12);
            this.label30.TabIndex = 255;
            this.label30.Text = "b";
            // 
            // text300e
            // 
            this.text300e.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text300e.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text300e.Location = new System.Drawing.Point(17, 86);
            this.text300e.Name = "text300e";
            this.text300e.Size = new System.Drawing.Size(178, 21);
            this.text300e.TabIndex = 271;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.Location = new System.Drawing.Point(107, 68);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 12);
            this.label33.TabIndex = 255;
            this.label33.Text = "가로";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.Location = new System.Drawing.Point(15, 68);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(29, 12);
            this.label32.TabIndex = 255;
            this.label32.Text = "세로";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.Location = new System.Drawing.Point(2, 68);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 12);
            this.label29.TabIndex = 255;
            this.label29.Text = "c";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(2, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 12);
            this.label28.TabIndex = 255;
            this.label28.Text = "e";
            // 
            // text546a
            // 
            this.text546a.AcceptsReturn = true;
            this.text546a.AcceptsTab = true;
            this.text546a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text546a.Location = new System.Drawing.Point(502, 47);
            this.text546a.Multiline = true;
            this.text546a.Name = "text546a";
            this.text546a.Size = new System.Drawing.Size(282, 53);
            this.text546a.TabIndex = 255;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox6.Controls.Add(this.text260a);
            this.groupBox6.Controls.Add(this.text260b);
            this.groupBox6.Controls.Add(this.text260c);
            this.groupBox6.Controls.Add(this.text260g);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox6.Location = new System.Drawing.Point(3, 330);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(202, 113);
            this.groupBox6.TabIndex = 306;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "260";
            // 
            // text260a
            // 
            this.text260a.BackColor = System.Drawing.SystemColors.Info;
            this.text260a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text260a.Location = new System.Drawing.Point(18, 20);
            this.text260a.Name = "text260a";
            this.text260a.Size = new System.Drawing.Size(178, 21);
            this.text260a.TabIndex = 258;
            // 
            // text260b
            // 
            this.text260b.BackColor = System.Drawing.SystemColors.Info;
            this.text260b.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text260b.Location = new System.Drawing.Point(18, 42);
            this.text260b.Name = "text260b";
            this.text260b.Size = new System.Drawing.Size(178, 21);
            this.text260b.TabIndex = 268;
            this.text260b.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // text260c
            // 
            this.text260c.BackColor = System.Drawing.SystemColors.Info;
            this.text260c.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text260c.Location = new System.Drawing.Point(18, 64);
            this.text260c.Name = "text260c";
            this.text260c.Size = new System.Drawing.Size(178, 21);
            this.text260c.TabIndex = 264;
            // 
            // text260g
            // 
            this.text260g.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text260g.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text260g.Location = new System.Drawing.Point(18, 86);
            this.text260g.Name = "text260g";
            this.text260g.Size = new System.Drawing.Size(178, 21);
            this.text260g.TabIndex = 263;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(4, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 12);
            this.label24.TabIndex = 255;
            this.label24.Text = "a";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(4, 45);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 12);
            this.label25.TabIndex = 255;
            this.label25.Text = "b";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(4, 66);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(13, 12);
            this.label26.TabIndex = 255;
            this.label26.Text = "c";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(4, 88);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 12);
            this.label27.TabIndex = 255;
            this.label27.Text = "g";
            // 
            // text504a
            // 
            this.text504a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text504a.Location = new System.Drawing.Point(38, 597);
            this.text504a.Name = "text504a";
            this.text504a.Size = new System.Drawing.Size(372, 21);
            this.text504a.TabIndex = 273;
            // 
            // text536a
            // 
            this.text536a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text536a.Location = new System.Drawing.Point(540, 763);
            this.text536a.Name = "text536a";
            this.text536a.Size = new System.Drawing.Size(387, 21);
            this.text536a.TabIndex = 269;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.radioButton4);
            this.groupBox4.Controls.Add(this.basicHeadBox);
            this.groupBox4.Controls.Add(this.invertCheck);
            this.groupBox4.Controls.Add(this.rbtn_100);
            this.groupBox4.Controls.Add(this.rbtn_111);
            this.groupBox4.Controls.Add(this.rbtn_110);
            this.groupBox4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox4.Location = new System.Drawing.Point(436, 109);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(491, 76);
            this.groupBox4.TabIndex = 303;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "기본표목 (1XX)";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radioButton4.Location = new System.Drawing.Point(349, 21);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(75, 16);
            this.radioButton4.TabIndex = 207;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "작자미상";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // basicHeadBox
            // 
            this.basicHeadBox.BackColor = System.Drawing.SystemColors.Info;
            this.basicHeadBox.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.basicHeadBox.Location = new System.Drawing.Point(22, 44);
            this.basicHeadBox.Name = "basicHeadBox";
            this.basicHeadBox.Size = new System.Drawing.Size(285, 21);
            this.basicHeadBox.TabIndex = 211;
            // 
            // invertCheck
            // 
            this.invertCheck.AutoSize = true;
            this.invertCheck.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.invertCheck.Location = new System.Drawing.Point(317, 46);
            this.invertCheck.Name = "invertCheck";
            this.invertCheck.Size = new System.Drawing.Size(48, 16);
            this.invertCheck.TabIndex = 213;
            this.invertCheck.Text = "생성";
            this.invertCheck.UseVisualStyleBackColor = true;
            this.invertCheck.CheckedChanged += new System.EventHandler(this.invertCheck_CheckedChanged);
            // 
            // rbtn_100
            // 
            this.rbtn_100.AutoSize = true;
            this.rbtn_100.Checked = true;
            this.rbtn_100.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtn_100.Location = new System.Drawing.Point(16, 21);
            this.rbtn_100.Name = "rbtn_100";
            this.rbtn_100.Size = new System.Drawing.Size(100, 16);
            this.rbtn_100.TabIndex = 207;
            this.rbtn_100.TabStop = true;
            this.rbtn_100.Text = "개인명 (100)";
            this.rbtn_100.UseVisualStyleBackColor = true;
            // 
            // rbtn_111
            // 
            this.rbtn_111.AutoSize = true;
            this.rbtn_111.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtn_111.Location = new System.Drawing.Point(238, 21);
            this.rbtn_111.Name = "rbtn_111";
            this.rbtn_111.Size = new System.Drawing.Size(100, 16);
            this.rbtn_111.TabIndex = 207;
            this.rbtn_111.TabStop = true;
            this.rbtn_111.Text = "회의명 (111)";
            this.rbtn_111.UseVisualStyleBackColor = true;
            // 
            // rbtn_110
            // 
            this.rbtn_110.AutoSize = true;
            this.rbtn_110.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtn_110.Location = new System.Drawing.Point(127, 21);
            this.rbtn_110.Name = "rbtn_110";
            this.rbtn_110.Size = new System.Drawing.Size(100, 16);
            this.rbtn_110.TabIndex = 207;
            this.rbtn_110.TabStop = true;
            this.rbtn_110.Text = "단체명 (110)";
            this.rbtn_110.UseVisualStyleBackColor = true;
            // 
            // text650a
            // 
            this.text650a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text650a.Location = new System.Drawing.Point(486, 448);
            this.text650a.Name = "text650a";
            this.text650a.Size = new System.Drawing.Size(429, 21);
            this.text650a.TabIndex = 277;
            this.text650a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.text041a);
            this.groupBox3.Controls.Add(this.text041b);
            this.groupBox3.Controls.Add(this.text041h);
            this.groupBox3.Controls.Add(this.text041k);
            this.groupBox3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox3.Location = new System.Drawing.Point(436, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(491, 40);
            this.groupBox3.TabIndex = 302;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "041";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(370, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 12);
            this.label8.TabIndex = 255;
            this.label8.Text = "b";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(249, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 12);
            this.label9.TabIndex = 255;
            this.label9.Text = "h";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(129, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 12);
            this.label10.TabIndex = 255;
            this.label10.Text = "k";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(8, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 12);
            this.label11.TabIndex = 255;
            this.label11.Text = "a";
            // 
            // text041a
            // 
            this.text041a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text041a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text041a.Location = new System.Drawing.Point(22, 14);
            this.text041a.Name = "text041a";
            this.text041a.Size = new System.Drawing.Size(94, 21);
            this.text041a.TabIndex = 251;
            // 
            // text041b
            // 
            this.text041b.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text041b.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text041b.Location = new System.Drawing.Point(385, 14);
            this.text041b.Name = "text041b";
            this.text041b.Size = new System.Drawing.Size(94, 21);
            this.text041b.TabIndex = 252;
            // 
            // text041h
            // 
            this.text041h.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text041h.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text041h.Location = new System.Drawing.Point(264, 14);
            this.text041h.Name = "text041h";
            this.text041h.Size = new System.Drawing.Size(94, 21);
            this.text041h.TabIndex = 253;
            this.text041h.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // text041k
            // 
            this.text041k.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text041k.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text041k.Location = new System.Drawing.Point(143, 14);
            this.text041k.Name = "text041k";
            this.text041k.Size = new System.Drawing.Size(94, 21);
            this.text041k.TabIndex = 254;
            // 
            // text586a
            // 
            this.text586a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text586a.Location = new System.Drawing.Point(540, 785);
            this.text586a.Name = "text586a";
            this.text586a.Size = new System.Drawing.Size(387, 21);
            this.text586a.TabIndex = 267;
            this.text586a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.textKDC4);
            this.groupBox2.Controls.Add(this.textKDC5);
            this.groupBox2.Controls.Add(this.textKDC6);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.textDDC21);
            this.groupBox2.Controls.Add(this.textDDC22);
            this.groupBox2.Controls.Add(this.textDDC23);
            this.groupBox2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox2.Location = new System.Drawing.Point(3, 109);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(414, 76);
            this.groupBox2.TabIndex = 301;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "분류기호";
            // 
            // textKDC4
            // 
            this.textKDC4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textKDC4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textKDC4.Location = new System.Drawing.Point(62, 20);
            this.textKDC4.Name = "textKDC4";
            this.textKDC4.Size = new System.Drawing.Size(93, 21);
            this.textKDC4.TabIndex = 256;
            // 
            // textKDC5
            // 
            this.textKDC5.BackColor = System.Drawing.SystemColors.Info;
            this.textKDC5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textKDC5.Location = new System.Drawing.Point(188, 20);
            this.textKDC5.Name = "textKDC5";
            this.textKDC5.Size = new System.Drawing.Size(93, 21);
            this.textKDC5.TabIndex = 257;
            // 
            // textKDC6
            // 
            this.textKDC6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textKDC6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textKDC6.Location = new System.Drawing.Point(314, 20);
            this.textKDC6.Name = "textKDC6";
            this.textKDC6.Size = new System.Drawing.Size(93, 21);
            this.textKDC6.TabIndex = 259;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(295, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 12);
            this.label12.TabIndex = 255;
            this.label12.Text = "23";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(169, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 12);
            this.label13.TabIndex = 255;
            this.label13.Text = "22";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(301, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(12, 12);
            this.label14.TabIndex = 255;
            this.label14.Text = "6";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(43, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 12);
            this.label15.TabIndex = 255;
            this.label15.Text = "21";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(175, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(12, 12);
            this.label16.TabIndex = 255;
            this.label16.Text = "5";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(4, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 12);
            this.label17.TabIndex = 255;
            this.label17.Text = "DDC";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(4, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 12);
            this.label18.TabIndex = 255;
            this.label18.Text = "KDC";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(49, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(12, 12);
            this.label19.TabIndex = 255;
            this.label19.Text = "4";
            // 
            // textDDC21
            // 
            this.textDDC21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textDDC21.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textDDC21.Location = new System.Drawing.Point(62, 44);
            this.textDDC21.Name = "textDDC21";
            this.textDDC21.Size = new System.Drawing.Size(93, 21);
            this.textDDC21.TabIndex = 262;
            // 
            // textDDC22
            // 
            this.textDDC22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textDDC22.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textDDC22.Location = new System.Drawing.Point(188, 44);
            this.textDDC22.Name = "textDDC22";
            this.textDDC22.Size = new System.Drawing.Size(93, 21);
            this.textDDC22.TabIndex = 261;
            // 
            // textDDC23
            // 
            this.textDDC23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textDDC23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textDDC23.Location = new System.Drawing.Point(314, 44);
            this.textDDC23.Name = "textDDC23";
            this.textDDC23.Size = new System.Drawing.Size(93, 21);
            this.textDDC23.TabIndex = 260;
            // 
            // text520a
            // 
            this.text520a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text520a.Location = new System.Drawing.Point(38, 758);
            this.text520a.Multiline = true;
            this.text520a.Name = "text520a";
            this.text520a.Size = new System.Drawing.Size(372, 48);
            this.text520a.TabIndex = 266;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.text245a);
            this.groupBox1.Controls.Add(this.text245b);
            this.groupBox1.Controls.Add(this.text245x);
            this.groupBox1.Controls.Add(this.text245n);
            this.groupBox1.Controls.Add(this.text245d);
            this.groupBox1.Controls.Add(this.text245e);
            this.groupBox1.Controls.Add(this.text245p);
            this.groupBox1.Controls.Add(this.label48);
            this.groupBox1.Controls.Add(this.label49);
            this.groupBox1.Controls.Add(this.label50);
            this.groupBox1.Controls.Add(this.label51);
            this.groupBox1.Controls.Add(this.label52);
            this.groupBox1.Controls.Add(this.label53);
            this.groupBox1.Controls.Add(this.label54);
            this.groupBox1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox1.Location = new System.Drawing.Point(3, 191);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(924, 83);
            this.groupBox1.TabIndex = 300;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "245";
            // 
            // text245a
            // 
            this.text245a.BackColor = System.Drawing.SystemColors.Info;
            this.text245a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text245a.Location = new System.Drawing.Point(18, 13);
            this.text245a.Name = "text245a";
            this.text245a.Size = new System.Drawing.Size(389, 21);
            this.text245a.TabIndex = 285;
            // 
            // text245b
            // 
            this.text245b.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text245b.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text245b.Location = new System.Drawing.Point(18, 35);
            this.text245b.Name = "text245b";
            this.text245b.Size = new System.Drawing.Size(389, 21);
            this.text245b.TabIndex = 291;
            this.text245b.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // text245x
            // 
            this.text245x.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text245x.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text245x.Location = new System.Drawing.Point(18, 58);
            this.text245x.Name = "text245x";
            this.text245x.Size = new System.Drawing.Size(389, 21);
            this.text245x.TabIndex = 290;
            // 
            // text245n
            // 
            this.text245n.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text245n.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text245n.Location = new System.Drawing.Point(455, 13);
            this.text245n.Name = "text245n";
            this.text245n.Size = new System.Drawing.Size(107, 21);
            this.text245n.TabIndex = 289;
            // 
            // text245d
            // 
            this.text245d.BackColor = System.Drawing.SystemColors.Info;
            this.text245d.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text245d.Location = new System.Drawing.Point(455, 35);
            this.text245d.Name = "text245d";
            this.text245d.Size = new System.Drawing.Size(457, 21);
            this.text245d.TabIndex = 288;
            // 
            // text245e
            // 
            this.text245e.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text245e.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text245e.Location = new System.Drawing.Point(455, 58);
            this.text245e.Name = "text245e";
            this.text245e.Size = new System.Drawing.Size(457, 21);
            this.text245e.TabIndex = 283;
            this.text245e.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // text245p
            // 
            this.text245p.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text245p.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.text245p.Location = new System.Drawing.Point(582, 13);
            this.text245p.Name = "text245p";
            this.text245p.Size = new System.Drawing.Size(330, 21);
            this.text245p.TabIndex = 282;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label48.Location = new System.Drawing.Point(4, 62);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 12);
            this.label48.TabIndex = 255;
            this.label48.Text = "x";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label49.Location = new System.Drawing.Point(441, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(13, 12);
            this.label49.TabIndex = 255;
            this.label49.Text = "n";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label50.Location = new System.Drawing.Point(568, 16);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(13, 12);
            this.label50.TabIndex = 255;
            this.label50.Text = "p";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.Location = new System.Drawing.Point(441, 60);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(13, 12);
            this.label51.TabIndex = 255;
            this.label51.Text = "e";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label52.Location = new System.Drawing.Point(441, 38);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(13, 12);
            this.label52.TabIndex = 255;
            this.label52.Text = "d";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label53.Location = new System.Drawing.Point(4, 39);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(13, 12);
            this.label53.TabIndex = 255;
            this.label53.Text = "b";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label54.Location = new System.Drawing.Point(4, 17);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(13, 12);
            this.label54.TabIndex = 255;
            this.label54.Text = "a";
            // 
            // text521a
            // 
            this.text521a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text521a.Location = new System.Drawing.Point(540, 741);
            this.text521a.Name = "text521a";
            this.text521a.Size = new System.Drawing.Size(387, 21);
            this.text521a.TabIndex = 265;
            // 
            // text525a
            // 
            this.text525a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text525a.Location = new System.Drawing.Point(64, 449);
            this.text525a.Name = "text525a";
            this.text525a.Size = new System.Drawing.Size(346, 21);
            this.text525a.TabIndex = 286;
            // 
            // text940a
            // 
            this.text940a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text940a.Location = new System.Drawing.Point(64, 280);
            this.text940a.Name = "text940a";
            this.text940a.Size = new System.Drawing.Size(346, 21);
            this.text940a.TabIndex = 279;
            // 
            // text653a
            // 
            this.text653a.BackColor = System.Drawing.SystemColors.Info;
            this.text653a.Location = new System.Drawing.Point(486, 426);
            this.text653a.Name = "text653a";
            this.text653a.Size = new System.Drawing.Size(429, 21);
            this.text653a.TabIndex = 284;
            this.text653a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // text507t
            // 
            this.text507t.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text507t.Location = new System.Drawing.Point(480, 570);
            this.text507t.Name = "text507t";
            this.text507t.Size = new System.Drawing.Size(435, 21);
            this.text507t.TabIndex = 280;
            // 
            // text500a
            // 
            this.text500a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text500a.Location = new System.Drawing.Point(38, 548);
            this.text500a.Multiline = true;
            this.text500a.Name = "text500a";
            this.text500a.Size = new System.Drawing.Size(372, 48);
            this.text500a.TabIndex = 278;
            this.text500a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FillTextBox_KeyDown);
            // 
            // text507a
            // 
            this.text507a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text507a.Location = new System.Drawing.Point(480, 548);
            this.text507a.Name = "text507a";
            this.text507a.Size = new System.Drawing.Size(435, 21);
            this.text507a.TabIndex = 280;
            // 
            // text250a
            // 
            this.text250a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.text250a.Location = new System.Drawing.Point(64, 302);
            this.text250a.Name = "text250a";
            this.text250a.Size = new System.Drawing.Size(346, 21);
            this.text250a.TabIndex = 281;
            // 
            // lbl_SaveData
            // 
            this.lbl_SaveData.AutoSize = true;
            this.lbl_SaveData.Font = new System.Drawing.Font("굴림체", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_SaveData.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.lbl_SaveData.Location = new System.Drawing.Point(890, 53);
            this.lbl_SaveData.Name = "lbl_SaveData";
            this.lbl_SaveData.Size = new System.Drawing.Size(64, 19);
            this.lbl_SaveData.TabIndex = 318;
            this.lbl_SaveData.Text = "[] []";
            // 
            // lbl_ISBN
            // 
            this.lbl_ISBN.AutoSize = true;
            this.lbl_ISBN.Font = new System.Drawing.Font("굴림체", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbl_ISBN.Location = new System.Drawing.Point(722, 53);
            this.lbl_ISBN.Name = "lbl_ISBN";
            this.lbl_ISBN.Size = new System.Drawing.Size(31, 19);
            this.lbl_ISBN.TabIndex = 33;
            this.lbl_ISBN.Text = "[]";
            // 
            // lbl_BookList
            // 
            this.lbl_BookList.AutoSize = true;
            this.lbl_BookList.BackColor = System.Drawing.Color.LightGray;
            this.lbl_BookList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_BookList.Font = new System.Drawing.Font("굴림체", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbl_BookList.Location = new System.Drawing.Point(12, 12);
            this.lbl_BookList.Name = "lbl_BookList";
            this.lbl_BookList.Size = new System.Drawing.Size(66, 21);
            this.lbl_BookList.TabIndex = 33;
            this.lbl_BookList.Text = "     ";
            // 
            // Marc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1808, 908);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.List_Book);
            this.Controls.Add(this.lbl_BookList);
            this.Controls.Add(this.lbl_ISBN);
            this.Controls.Add(this.lbl_SaveData);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btn_FillBlank);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cb_grade);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.input_date);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.btn_preview);
            this.Controls.Add(this.Btn_Memo);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.comboBox6);
            this.Controls.Add(this.comboBox5);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.comboBox7);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.col008res);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.gov008res);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.text008col);
            this.Controls.Add(this.text008gov);
            this.Controls.Add(this.etc2);
            this.Controls.Add(this.etc1);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label1);
            this.Name = "Marc";
            this.Text = "마크 작성";
            this.Load += new System.EventHandler(this.Marc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.List_Book)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridView020)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView505)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView246)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView440)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView490)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.RichTextBox richTextBox1;
        public System.Windows.Forms.RichTextBox etc1;
        public System.Windows.Forms.RichTextBox etc2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label103;
        public System.Windows.Forms.TextBox text008col;
        public System.Windows.Forms.TextBox text008gov;
        public System.Windows.Forms.Label col008res;
        public System.Windows.Forms.Label gov008res;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.Button Btn_Memo;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.DataGridViewTextBoxColumn 도서명;
        private System.Windows.Forms.DataGridViewTextBoxColumn 저자;
        private System.Windows.Forms.DataGridViewTextBoxColumn 출판사;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn 마크;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox text008;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.DateTimePicker input_date;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_preview;
        private System.Windows.Forms.ComboBox cb_grade;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rb_Sort;
        private System.Windows.Forms.RadioButton rb_Filter;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Button btn_mk_marcList;
        public System.Windows.Forms.DataGridView List_Book;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Button btn_Reflesh008;
        private System.Windows.Forms.Button btn_FillBlank;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView GridView020;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox text505a;
        private System.Windows.Forms.DataGridView GridView505;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DataGridView GridView246;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text246Jisi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text246i;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text246a;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text246b;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text246n;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text246p;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox text900a;
        private System.Windows.Forms.TextBox text910a;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DataGridView GridView440;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440a;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440n;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440p;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440vNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440vTxt;
        private System.Windows.Forms.DataGridViewTextBoxColumn text440x;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox text700a;
        private System.Windows.Forms.TextBox text710a;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView GridView490;
        private System.Windows.Forms.DataGridViewTextBoxColumn text490a;
        private System.Windows.Forms.DataGridViewTextBoxColumn text490v;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox text300a;
        private System.Windows.Forms.TextBox text300c1;
        private System.Windows.Forms.TextBox text300c2;
        private System.Windows.Forms.TextBox text300b;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox text300e;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox text546a;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox text260a;
        private System.Windows.Forms.TextBox text260b;
        private System.Windows.Forms.TextBox text260c;
        private System.Windows.Forms.TextBox text260g;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox text504a;
        private System.Windows.Forms.TextBox text536a;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.TextBox basicHeadBox;
        private System.Windows.Forms.CheckBox invertCheck;
        private System.Windows.Forms.RadioButton rbtn_100;
        private System.Windows.Forms.RadioButton rbtn_111;
        private System.Windows.Forms.RadioButton rbtn_110;
        private System.Windows.Forms.TextBox text650a;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox text041a;
        private System.Windows.Forms.TextBox text041b;
        private System.Windows.Forms.TextBox text041h;
        private System.Windows.Forms.TextBox text041k;
        private System.Windows.Forms.TextBox text586a;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textKDC4;
        private System.Windows.Forms.TextBox textKDC5;
        private System.Windows.Forms.TextBox textKDC6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textDDC21;
        private System.Windows.Forms.TextBox textDDC22;
        private System.Windows.Forms.TextBox textDDC23;
        private System.Windows.Forms.TextBox text520a;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox text245a;
        private System.Windows.Forms.TextBox text245b;
        private System.Windows.Forms.TextBox text245x;
        private System.Windows.Forms.TextBox text245n;
        private System.Windows.Forms.TextBox text245d;
        private System.Windows.Forms.TextBox text245e;
        private System.Windows.Forms.TextBox text245p;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox text521a;
        private System.Windows.Forms.TextBox text525a;
        private System.Windows.Forms.TextBox text940a;
        private System.Windows.Forms.TextBox text653a;
        private System.Windows.Forms.TextBox text507t;
        private System.Windows.Forms.TextBox text500a;
        private System.Windows.Forms.TextBox text507a;
        private System.Windows.Forms.TextBox text250a;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckSet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text020a;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text020g;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text020c;
        private System.Windows.Forms.Label lbl_SaveData;
        private System.Windows.Forms.Button Btn_interlock;
        private System.Windows.Forms.Label lbl_ISBN;
        private System.Windows.Forms.Button btn_FilterReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISBN13;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn url;
        private System.Windows.Forms.DataGridViewTextBoxColumn marc_idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn db_marc;
        private System.Windows.Forms.DataGridViewTextBoxColumn grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn user;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaveDate;
        private System.Windows.Forms.Button btn_CopySelect;
        public System.Windows.Forms.Label lbl_BookList;
        private System.Windows.Forms.DataGridViewTextBoxColumn text505n;
        private System.Windows.Forms.DataGridViewTextBoxColumn text505t;
        private System.Windows.Forms.DataGridViewTextBoxColumn text505d;
        private System.Windows.Forms.DataGridViewTextBoxColumn text505e;
    }
}