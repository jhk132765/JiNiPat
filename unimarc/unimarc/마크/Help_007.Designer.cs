﻿namespace UniMarc.마크
{
    partial class Help_007
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_Text = new System.Windows.Forms.Label();
            this.lbl_007 = new System.Windows.Forms.Label();
            this.Panel음반 = new System.Windows.Forms.Panel();
            this.tb_14 = new System.Windows.Forms.TextBox();
            this.tb_11 = new System.Windows.Forms.TextBox();
            this.tb_13 = new System.Windows.Forms.TextBox();
            this.tb_10 = new System.Windows.Forms.TextBox();
            this.tb_06 = new System.Windows.Forms.TextBox();
            this.tb_12 = new System.Windows.Forms.TextBox();
            this.tb_09 = new System.Windows.Forms.TextBox();
            this.tb_05 = new System.Windows.Forms.TextBox();
            this.tb_08 = new System.Windows.Forms.TextBox();
            this.tb_04 = new System.Windows.Forms.TextBox();
            this.tb_07 = new System.Windows.Forms.TextBox();
            this.tb_02 = new System.Windows.Forms.TextBox();
            this.tb_01 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Btn_Apply = new System.Windows.Forms.Button();
            this.Btn_Close = new System.Windows.Forms.Button();
            this.PanelDVD = new System.Windows.Forms.Panel();
            this.tb_DVD06 = new System.Windows.Forms.TextBox();
            this.tb_DVD09 = new System.Windows.Forms.TextBox();
            this.tb_DVD05 = new System.Windows.Forms.TextBox();
            this.tb_DVD08 = new System.Windows.Forms.TextBox();
            this.tb_DVD04 = new System.Windows.Forms.TextBox();
            this.tb_DVD07 = new System.Windows.Forms.TextBox();
            this.tb_DVD02 = new System.Windows.Forms.TextBox();
            this.tb_DVD01 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.Panel음반.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.PanelDVD.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_Text);
            this.panel1.Controls.Add(this.lbl_007);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 26);
            this.panel1.TabIndex = 0;
            // 
            // lbl_Text
            // 
            this.lbl_Text.AutoSize = true;
            this.lbl_Text.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Text.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_Text.Location = new System.Drawing.Point(40, 5);
            this.lbl_Text.Name = "lbl_Text";
            this.lbl_Text.Size = new System.Drawing.Size(130, 13);
            this.lbl_Text.TabIndex = 0;
            this.lbl_Text.Text = "sd fsngunmmned";
            // 
            // lbl_007
            // 
            this.lbl_007.AutoSize = true;
            this.lbl_007.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_007.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_007.Location = new System.Drawing.Point(5, 5);
            this.lbl_007.Name = "lbl_007";
            this.lbl_007.Size = new System.Drawing.Size(31, 13);
            this.lbl_007.TabIndex = 0;
            this.lbl_007.Text = "007";
            // 
            // Panel음반
            // 
            this.Panel음반.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel음반.Controls.Add(this.tb_14);
            this.Panel음반.Controls.Add(this.tb_11);
            this.Panel음반.Controls.Add(this.tb_13);
            this.Panel음반.Controls.Add(this.tb_10);
            this.Panel음반.Controls.Add(this.tb_06);
            this.Panel음반.Controls.Add(this.tb_12);
            this.Panel음반.Controls.Add(this.tb_09);
            this.Panel음반.Controls.Add(this.tb_05);
            this.Panel음반.Controls.Add(this.tb_08);
            this.Panel음반.Controls.Add(this.tb_04);
            this.Panel음반.Controls.Add(this.tb_07);
            this.Panel음반.Controls.Add(this.tb_02);
            this.Panel음반.Controls.Add(this.tb_01);
            this.Panel음반.Controls.Add(this.label13);
            this.Panel음반.Controls.Add(this.label12);
            this.Panel음반.Controls.Add(this.label11);
            this.Panel음반.Controls.Add(this.label10);
            this.Panel음반.Controls.Add(this.label9);
            this.Panel음반.Controls.Add(this.label8);
            this.Panel음반.Controls.Add(this.label7);
            this.Panel음반.Controls.Add(this.label6);
            this.Panel음반.Controls.Add(this.label5);
            this.Panel음반.Controls.Add(this.label4);
            this.Panel음반.Controls.Add(this.label3);
            this.Panel음반.Controls.Add(this.label2);
            this.Panel음반.Controls.Add(this.label1);
            this.Panel음반.Location = new System.Drawing.Point(12, 45);
            this.Panel음반.Name = "Panel음반";
            this.Panel음반.Size = new System.Drawing.Size(245, 363);
            this.Panel음반.TabIndex = 0;
            // 
            // tb_14
            // 
            this.tb_14.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_14.Location = new System.Drawing.Point(178, 331);
            this.tb_14.Name = "tb_14";
            this.tb_14.ReadOnly = true;
            this.tb_14.Size = new System.Drawing.Size(55, 21);
            this.tb_14.TabIndex = 1;
            this.tb_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_14.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_11
            // 
            this.tb_11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_11.Location = new System.Drawing.Point(178, 250);
            this.tb_11.Name = "tb_11";
            this.tb_11.ReadOnly = true;
            this.tb_11.Size = new System.Drawing.Size(55, 21);
            this.tb_11.TabIndex = 1;
            this.tb_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_11.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_13
            // 
            this.tb_13.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_13.Location = new System.Drawing.Point(178, 304);
            this.tb_13.Name = "tb_13";
            this.tb_13.ReadOnly = true;
            this.tb_13.Size = new System.Drawing.Size(55, 21);
            this.tb_13.TabIndex = 1;
            this.tb_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_13.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_10
            // 
            this.tb_10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_10.Location = new System.Drawing.Point(178, 223);
            this.tb_10.Name = "tb_10";
            this.tb_10.ReadOnly = true;
            this.tb_10.Size = new System.Drawing.Size(55, 21);
            this.tb_10.TabIndex = 1;
            this.tb_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_10.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_06
            // 
            this.tb_06.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_06.Location = new System.Drawing.Point(178, 115);
            this.tb_06.Name = "tb_06";
            this.tb_06.ReadOnly = true;
            this.tb_06.Size = new System.Drawing.Size(55, 21);
            this.tb_06.TabIndex = 1;
            this.tb_06.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_06.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_12
            // 
            this.tb_12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_12.Location = new System.Drawing.Point(178, 277);
            this.tb_12.Name = "tb_12";
            this.tb_12.ReadOnly = true;
            this.tb_12.Size = new System.Drawing.Size(55, 21);
            this.tb_12.TabIndex = 1;
            this.tb_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_12.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_09
            // 
            this.tb_09.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_09.Location = new System.Drawing.Point(178, 196);
            this.tb_09.Name = "tb_09";
            this.tb_09.ReadOnly = true;
            this.tb_09.Size = new System.Drawing.Size(55, 21);
            this.tb_09.TabIndex = 1;
            this.tb_09.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_09.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_05
            // 
            this.tb_05.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_05.Location = new System.Drawing.Point(178, 88);
            this.tb_05.Name = "tb_05";
            this.tb_05.ReadOnly = true;
            this.tb_05.Size = new System.Drawing.Size(55, 21);
            this.tb_05.TabIndex = 1;
            this.tb_05.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_05.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_08
            // 
            this.tb_08.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_08.Location = new System.Drawing.Point(178, 169);
            this.tb_08.Name = "tb_08";
            this.tb_08.ReadOnly = true;
            this.tb_08.Size = new System.Drawing.Size(55, 21);
            this.tb_08.TabIndex = 1;
            this.tb_08.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_08.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_04
            // 
            this.tb_04.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_04.Location = new System.Drawing.Point(178, 61);
            this.tb_04.Name = "tb_04";
            this.tb_04.ReadOnly = true;
            this.tb_04.Size = new System.Drawing.Size(55, 21);
            this.tb_04.TabIndex = 1;
            this.tb_04.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_04.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_07
            // 
            this.tb_07.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_07.Location = new System.Drawing.Point(178, 142);
            this.tb_07.Name = "tb_07";
            this.tb_07.ReadOnly = true;
            this.tb_07.Size = new System.Drawing.Size(55, 21);
            this.tb_07.TabIndex = 1;
            this.tb_07.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_07.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_02
            // 
            this.tb_02.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_02.Location = new System.Drawing.Point(178, 34);
            this.tb_02.Name = "tb_02";
            this.tb_02.ReadOnly = true;
            this.tb_02.Size = new System.Drawing.Size(55, 21);
            this.tb_02.TabIndex = 1;
            this.tb_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_02.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_01
            // 
            this.tb_01.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_01.Location = new System.Drawing.Point(178, 7);
            this.tb_01.Name = "tb_01";
            this.tb_01.ReadOnly = true;
            this.tb_01.Size = new System.Drawing.Size(55, 21);
            this.tb_01.TabIndex = 1;
            this.tb_01.Text = "s";
            this.tb_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 334);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "녹음/저장기법(14)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 308);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(123, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "특수재생장치특성(13)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 281);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "음구의 깎임 종류(12)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 254);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "재질(11)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 227);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(167, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "음반, 실린더, 테이프 종류(10)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "테이프 트랙수(09)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 173);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "테이프 폭(08)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "크기(07)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "음구의 폭/높이(06)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "녹음재생형태(05)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "속도(04)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "특정자료증별(02)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "자료범주표시(01)";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Location = new System.Drawing.Point(263, 45);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(331, 363);
            this.panel3.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.value,
            this.content});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(331, 363);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // value
            // 
            this.value.HeaderText = "값";
            this.value.Name = "value";
            this.value.ReadOnly = true;
            // 
            // content
            // 
            this.content.HeaderText = "내용";
            this.content.Name = "content";
            this.content.ReadOnly = true;
            this.content.Width = 170;
            // 
            // Btn_Apply
            // 
            this.Btn_Apply.Location = new System.Drawing.Point(438, 11);
            this.Btn_Apply.Name = "Btn_Apply";
            this.Btn_Apply.Size = new System.Drawing.Size(75, 28);
            this.Btn_Apply.TabIndex = 1;
            this.Btn_Apply.Text = "적   용";
            this.Btn_Apply.UseVisualStyleBackColor = true;
            this.Btn_Apply.Click += new System.EventHandler(this.Btn_Apply_Click);
            // 
            // Btn_Close
            // 
            this.Btn_Close.Location = new System.Drawing.Point(519, 11);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(75, 28);
            this.Btn_Close.TabIndex = 1;
            this.Btn_Close.Text = "닫   기";
            this.Btn_Close.UseVisualStyleBackColor = true;
            this.Btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // PanelDVD
            // 
            this.PanelDVD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelDVD.Controls.Add(this.tb_DVD06);
            this.PanelDVD.Controls.Add(this.tb_DVD09);
            this.PanelDVD.Controls.Add(this.tb_DVD05);
            this.PanelDVD.Controls.Add(this.tb_DVD08);
            this.PanelDVD.Controls.Add(this.tb_DVD04);
            this.PanelDVD.Controls.Add(this.tb_DVD07);
            this.PanelDVD.Controls.Add(this.tb_DVD02);
            this.PanelDVD.Controls.Add(this.tb_DVD01);
            this.PanelDVD.Controls.Add(this.label19);
            this.PanelDVD.Controls.Add(this.label20);
            this.PanelDVD.Controls.Add(this.label21);
            this.PanelDVD.Controls.Add(this.label22);
            this.PanelDVD.Controls.Add(this.label23);
            this.PanelDVD.Controls.Add(this.label24);
            this.PanelDVD.Controls.Add(this.label25);
            this.PanelDVD.Controls.Add(this.label26);
            this.PanelDVD.Location = new System.Drawing.Point(12, 45);
            this.PanelDVD.Name = "PanelDVD";
            this.PanelDVD.Size = new System.Drawing.Size(245, 225);
            this.PanelDVD.TabIndex = 0;
            // 
            // tb_DVD06
            // 
            this.tb_DVD06.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_DVD06.Location = new System.Drawing.Point(178, 115);
            this.tb_DVD06.Name = "tb_DVD06";
            this.tb_DVD06.ReadOnly = true;
            this.tb_DVD06.Size = new System.Drawing.Size(55, 21);
            this.tb_DVD06.TabIndex = 1;
            this.tb_DVD06.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_DVD06.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_DVD09
            // 
            this.tb_DVD09.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_DVD09.Location = new System.Drawing.Point(178, 196);
            this.tb_DVD09.Name = "tb_DVD09";
            this.tb_DVD09.ReadOnly = true;
            this.tb_DVD09.Size = new System.Drawing.Size(55, 21);
            this.tb_DVD09.TabIndex = 1;
            this.tb_DVD09.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_DVD09.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_DVD05
            // 
            this.tb_DVD05.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_DVD05.Location = new System.Drawing.Point(178, 88);
            this.tb_DVD05.Name = "tb_DVD05";
            this.tb_DVD05.ReadOnly = true;
            this.tb_DVD05.Size = new System.Drawing.Size(55, 21);
            this.tb_DVD05.TabIndex = 1;
            this.tb_DVD05.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_DVD05.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_DVD08
            // 
            this.tb_DVD08.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_DVD08.Location = new System.Drawing.Point(178, 169);
            this.tb_DVD08.Name = "tb_DVD08";
            this.tb_DVD08.ReadOnly = true;
            this.tb_DVD08.Size = new System.Drawing.Size(55, 21);
            this.tb_DVD08.TabIndex = 1;
            this.tb_DVD08.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_DVD08.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_DVD04
            // 
            this.tb_DVD04.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_DVD04.Location = new System.Drawing.Point(178, 61);
            this.tb_DVD04.Name = "tb_DVD04";
            this.tb_DVD04.ReadOnly = true;
            this.tb_DVD04.Size = new System.Drawing.Size(55, 21);
            this.tb_DVD04.TabIndex = 1;
            this.tb_DVD04.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_DVD04.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_DVD07
            // 
            this.tb_DVD07.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_DVD07.Location = new System.Drawing.Point(178, 142);
            this.tb_DVD07.Name = "tb_DVD07";
            this.tb_DVD07.ReadOnly = true;
            this.tb_DVD07.Size = new System.Drawing.Size(55, 21);
            this.tb_DVD07.TabIndex = 1;
            this.tb_DVD07.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_DVD07.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_DVD02
            // 
            this.tb_DVD02.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_DVD02.Location = new System.Drawing.Point(178, 34);
            this.tb_DVD02.Name = "tb_DVD02";
            this.tb_DVD02.ReadOnly = true;
            this.tb_DVD02.Size = new System.Drawing.Size(55, 21);
            this.tb_DVD02.TabIndex = 1;
            this.tb_DVD02.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_DVD02.Click += new System.EventHandler(this.TextBox_Click);
            // 
            // tb_DVD01
            // 
            this.tb_DVD01.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_DVD01.Location = new System.Drawing.Point(178, 7);
            this.tb_DVD01.Name = "tb_DVD01";
            this.tb_DVD01.ReadOnly = true;
            this.tb_DVD01.Size = new System.Drawing.Size(55, 21);
            this.tb_DVD01.TabIndex = 1;
            this.tb_DVD01.Text = "s";
            this.tb_DVD01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 200);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(115, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "음향의 재생상태(09)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 173);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "필름의 폭(08)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 146);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(103, 12);
            this.label21.TabIndex = 0;
            this.label21.Text = "음향수록 매체(07)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 119);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 12);
            this.label22.TabIndex = 0;
            this.label22.Text = "음향 유무(06)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 92);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(115, 12);
            this.label23.TabIndex = 0;
            this.label23.Text = "비디오 녹화형식(05)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 65);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(51, 12);
            this.label24.TabIndex = 0;
            this.label24.Text = "색채(04)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 38);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(99, 12);
            this.label25.TabIndex = 0;
            this.label25.Text = "특정자료증별(02)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 11);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(99, 12);
            this.label26.TabIndex = 0;
            this.label26.Text = "자료범주표시(01)";
            // 
            // Help_007
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 420);
            this.Controls.Add(this.Btn_Close);
            this.Controls.Add(this.Btn_Apply);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.PanelDVD);
            this.Controls.Add(this.Panel음반);
            this.Controls.Add(this.panel1);
            this.Name = "Help_007";
            this.Text = "Help_007";
            this.Load += new System.EventHandler(this.Help_007_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Panel음반.ResumeLayout(false);
            this.Panel음반.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.PanelDVD.ResumeLayout(false);
            this.PanelDVD.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_007;
        private System.Windows.Forms.Panel Panel음반;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button Btn_Apply;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_14;
        private System.Windows.Forms.TextBox tb_11;
        private System.Windows.Forms.TextBox tb_13;
        private System.Windows.Forms.TextBox tb_10;
        private System.Windows.Forms.TextBox tb_06;
        private System.Windows.Forms.TextBox tb_12;
        private System.Windows.Forms.TextBox tb_09;
        private System.Windows.Forms.TextBox tb_05;
        private System.Windows.Forms.TextBox tb_08;
        private System.Windows.Forms.TextBox tb_04;
        private System.Windows.Forms.TextBox tb_07;
        private System.Windows.Forms.TextBox tb_02;
        private System.Windows.Forms.TextBox tb_01;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn value;
        private System.Windows.Forms.DataGridViewTextBoxColumn content;
        private System.Windows.Forms.Label lbl_Text;
        private System.Windows.Forms.Button Btn_Close;
        private System.Windows.Forms.Panel PanelDVD;
        private System.Windows.Forms.TextBox tb_DVD06;
        private System.Windows.Forms.TextBox tb_DVD09;
        private System.Windows.Forms.TextBox tb_DVD05;
        private System.Windows.Forms.TextBox tb_DVD08;
        private System.Windows.Forms.TextBox tb_DVD04;
        private System.Windows.Forms.TextBox tb_DVD07;
        private System.Windows.Forms.TextBox tb_DVD02;
        private System.Windows.Forms.TextBox tb_DVD01;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
    }
}