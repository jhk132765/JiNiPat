﻿
namespace UniMarc.마크
{
    partial class Marc_Plan_Sub_MarcEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.cb_grade = new System.Windows.Forms.ComboBox();
            this.lbl_SaveData = new System.Windows.Forms.Label();
            this.lbl_ClassSymbol = new System.Windows.Forms.Label();
            this.lbl_AuthorSymbol = new System.Windows.Forms.Label();
            this.btn_Close = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.input_date = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.text008gov = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.text008col = new System.Windows.Forms.TextBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label103 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.gov008res = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label99 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.col008res = new System.Windows.Forms.Label();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btn_Save = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_EditNum = new System.Windows.Forms.Button();
            this.tb_num = new System.Windows.Forms.TextBox();
            this.tb_Mcode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cb_authorType = new System.Windows.Forms.ComboBox();
            this.cb_divType = new System.Windows.Forms.ComboBox();
            this.cb_divNum = new System.Windows.Forms.ComboBox();
            this.cb_FirstBook = new System.Windows.Forms.ComboBox();
            this.cb_FirstAuthor = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_Front = new System.Windows.Forms.Button();
            this.btn_Back = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.text008 = new System.Windows.Forms.TextBox();
            this.text007 = new System.Windows.Forms.TextBox();
            this.text001 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.etcBox1 = new System.Windows.Forms.RichTextBox();
            this.etcBox2 = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.cb_grade);
            this.panel1.Controls.Add(this.lbl_SaveData);
            this.panel1.Controls.Add(this.lbl_ClassSymbol);
            this.panel1.Controls.Add(this.lbl_AuthorSymbol);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btn_Front);
            this.panel1.Controls.Add(this.btn_Back);
            this.panel1.Location = new System.Drawing.Point(13, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1087, 212);
            this.panel1.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(851, 116);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 12);
            this.label12.TabIndex = 321;
            this.label12.Text = "마크 등급";
            // 
            // cb_grade
            // 
            this.cb_grade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_grade.FormattingEnabled = true;
            this.cb_grade.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.cb_grade.Location = new System.Drawing.Point(845, 130);
            this.cb_grade.Name = "cb_grade";
            this.cb_grade.Size = new System.Drawing.Size(75, 20);
            this.cb_grade.TabIndex = 320;
            // 
            // lbl_SaveData
            // 
            this.lbl_SaveData.AutoSize = true;
            this.lbl_SaveData.Font = new System.Drawing.Font("굴림체", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_SaveData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_SaveData.Location = new System.Drawing.Point(188, 156);
            this.lbl_SaveData.Name = "lbl_SaveData";
            this.lbl_SaveData.Size = new System.Drawing.Size(64, 19);
            this.lbl_SaveData.TabIndex = 319;
            this.lbl_SaveData.Text = "[] []";
            // 
            // lbl_ClassSymbol
            // 
            this.lbl_ClassSymbol.AutoSize = true;
            this.lbl_ClassSymbol.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_ClassSymbol.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_ClassSymbol.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_ClassSymbol.Location = new System.Drawing.Point(354, 191);
            this.lbl_ClassSymbol.Name = "lbl_ClassSymbol";
            this.lbl_ClassSymbol.Size = new System.Drawing.Size(12, 13);
            this.lbl_ClassSymbol.TabIndex = 5;
            this.lbl_ClassSymbol.Text = " ";
            // 
            // lbl_AuthorSymbol
            // 
            this.lbl_AuthorSymbol.AutoSize = true;
            this.lbl_AuthorSymbol.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_AuthorSymbol.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_AuthorSymbol.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_AuthorSymbol.Location = new System.Drawing.Point(188, 191);
            this.lbl_AuthorSymbol.Name = "lbl_AuthorSymbol";
            this.lbl_AuthorSymbol.Size = new System.Drawing.Size(12, 13);
            this.lbl_AuthorSymbol.TabIndex = 5;
            this.lbl_AuthorSymbol.Text = " ";
            // 
            // btn_Close
            // 
            this.btn_Close.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Close.Location = new System.Drawing.Point(966, 60);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(115, 51);
            this.btn_Close.TabIndex = 4;
            this.btn_Close.Text = "닫    기\r\n\r\n(ESC)";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.input_date);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label98);
            this.panel5.Controls.Add(this.checkBox2);
            this.panel5.Controls.Add(this.text008gov);
            this.panel5.Controls.Add(this.checkBox1);
            this.panel5.Controls.Add(this.text008col);
            this.panel5.Controls.Add(this.comboBox6);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.comboBox5);
            this.panel5.Controls.Add(this.label103);
            this.panel5.Controls.Add(this.comboBox4);
            this.panel5.Controls.Add(this.gov008res);
            this.panel5.Controls.Add(this.comboBox2);
            this.panel5.Controls.Add(this.label99);
            this.panel5.Controls.Add(this.label102);
            this.panel5.Controls.Add(this.col008res);
            this.panel5.Controls.Add(this.comboBox7);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.comboBox3);
            this.panel5.Controls.Add(this.label100);
            this.panel5.Controls.Add(this.label101);
            this.panel5.Controls.Add(this.comboBox1);
            this.panel5.Location = new System.Drawing.Point(179, 34);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(660, 84);
            this.panel5.TabIndex = 3;
            // 
            // input_date
            // 
            this.input_date.CustomFormat = "yyyy-MM-dd";
            this.input_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.input_date.Location = new System.Drawing.Point(61, 5);
            this.input_date.Name = "input_date";
            this.input_date.Size = new System.Drawing.Size(91, 21);
            this.input_date.TabIndex = 247;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 224;
            this.label7.Text = "입력일자";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(2, 36);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(53, 12);
            this.label98.TabIndex = 225;
            this.label98.Text = "내용형식";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(524, 34);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(112, 16);
            this.checkBox2.TabIndex = 245;
            this.checkBox2.Text = "기념논문집 여부";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // text008gov
            // 
            this.text008gov.Location = new System.Drawing.Point(461, 58);
            this.text008gov.Name = "text008gov";
            this.text008gov.Size = new System.Drawing.Size(41, 21);
            this.text008gov.TabIndex = 226;
            this.text008gov.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text008col_KeyDown);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(413, 34);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(112, 16);
            this.checkBox1.TabIndex = 244;
            this.checkBox1.Text = "회의간행물 여부";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // text008col
            // 
            this.text008col.Location = new System.Drawing.Point(329, 32);
            this.text008col.Name = "text008col";
            this.text008col.Size = new System.Drawing.Size(41, 21);
            this.text008col.TabIndex = 227;
            this.text008col.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text008col_KeyDown);
            // 
            // comboBox6
            // 
            this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(321, 58);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(101, 20);
            this.comboBox6.TabIndex = 242;
            this.comboBox6.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 228;
            this.label5.Text = "이용자";
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(175, 58);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(101, 20);
            this.comboBox5.TabIndex = 241;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(432, 62);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(29, 12);
            this.label103.TabIndex = 229;
            this.label103.Text = "기관";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(57, 58);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(79, 20);
            this.comboBox4.TabIndex = 240;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // gov008res
            // 
            this.gov008res.AutoSize = true;
            this.gov008res.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gov008res.ForeColor = System.Drawing.Color.Blue;
            this.gov008res.Location = new System.Drawing.Point(504, 62);
            this.gov008res.Name = "gov008res";
            this.gov008res.Size = new System.Drawing.Size(13, 12);
            this.gov008res.TabIndex = 230;
            this.gov008res.Text = "  ";
            this.gov008res.TextChanged += new System.EventHandler(this.col008res_TextChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(355, 5);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(79, 20);
            this.comboBox2.TabIndex = 239;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(300, 36);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(29, 12);
            this.label99.TabIndex = 231;
            this.label99.Text = "대학";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(291, 62);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(29, 12);
            this.label102.TabIndex = 235;
            this.label102.Text = "언어";
            // 
            // col008res
            // 
            this.col008res.AutoSize = true;
            this.col008res.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.col008res.ForeColor = System.Drawing.Color.Blue;
            this.col008res.Location = new System.Drawing.Point(372, 36);
            this.col008res.Name = "col008res";
            this.col008res.Size = new System.Drawing.Size(13, 12);
            this.col008res.TabIndex = 232;
            this.col008res.Text = "  ";
            this.col008res.TextChanged += new System.EventHandler(this.col008res_TextChanged);
            // 
            // comboBox7
            // 
            this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(176, 32);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(118, 20);
            this.comboBox7.TabIndex = 243;
            this.comboBox7.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(304, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 233;
            this.label3.Text = "자료형식";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(57, 32);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(118, 20);
            this.comboBox3.TabIndex = 238;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(2, 62);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(53, 12);
            this.label100.TabIndex = 234;
            this.label100.Text = "문학형식";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(145, 62);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(29, 12);
            this.label101.TabIndex = 236;
            this.label101.Text = "전기";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(212, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(79, 20);
            this.comboBox1.TabIndex = 237;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // btn_Save
            // 
            this.btn_Save.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Save.Location = new System.Drawing.Point(845, 60);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(115, 51);
            this.btn_Save.TabIndex = 4;
            this.btn_Save.Text = "저    장\r\n\r\n(F9)";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btn_EditNum);
            this.panel6.Controls.Add(this.tb_num);
            this.panel6.Controls.Add(this.tb_Mcode);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Location = new System.Drawing.Point(179, 120);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(660, 30);
            this.panel6.TabIndex = 3;
            // 
            // btn_EditNum
            // 
            this.btn_EditNum.Location = new System.Drawing.Point(318, 5);
            this.btn_EditNum.Name = "btn_EditNum";
            this.btn_EditNum.Size = new System.Drawing.Size(37, 19);
            this.btn_EditNum.TabIndex = 3;
            this.btn_EditNum.Text = "수정";
            this.btn_EditNum.UseVisualStyleBackColor = true;
            this.btn_EditNum.Click += new System.EventHandler(this.btn_EditNum_Click);
            // 
            // tb_num
            // 
            this.tb_num.Location = new System.Drawing.Point(214, 4);
            this.tb_num.Name = "tb_num";
            this.tb_num.Size = new System.Drawing.Size(100, 21);
            this.tb_num.TabIndex = 2;
            this.tb_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_Mcode
            // 
            this.tb_Mcode.Location = new System.Drawing.Point(57, 4);
            this.tb_Mcode.Name = "tb_Mcode";
            this.tb_Mcode.Size = new System.Drawing.Size(100, 21);
            this.tb_Mcode.TabIndex = 2;
            this.tb_Mcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(183, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 1;
            this.label6.Text = "연번";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "MCODE";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cb_authorType);
            this.panel3.Controls.Add(this.cb_divType);
            this.panel3.Controls.Add(this.cb_divNum);
            this.panel3.Controls.Add(this.cb_FirstBook);
            this.panel3.Controls.Add(this.cb_FirstAuthor);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(179, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(660, 30);
            this.panel3.TabIndex = 3;
            // 
            // cb_authorType
            // 
            this.cb_authorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_authorType.FormattingEnabled = true;
            this.cb_authorType.Location = new System.Drawing.Point(125, 4);
            this.cb_authorType.Name = "cb_authorType";
            this.cb_authorType.Size = new System.Drawing.Size(171, 20);
            this.cb_authorType.TabIndex = 1;
            // 
            // cb_divType
            // 
            this.cb_divType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_divType.FormattingEnabled = true;
            this.cb_divType.Location = new System.Drawing.Point(414, 4);
            this.cb_divType.Name = "cb_divType";
            this.cb_divType.Size = new System.Drawing.Size(88, 20);
            this.cb_divType.TabIndex = 1;
            this.cb_divType.SelectedIndexChanged += new System.EventHandler(this.cb_divType_SelectedIndexChanged);
            // 
            // cb_divNum
            // 
            this.cb_divNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_divNum.FormattingEnabled = true;
            this.cb_divNum.Location = new System.Drawing.Point(508, 4);
            this.cb_divNum.Name = "cb_divNum";
            this.cb_divNum.Size = new System.Drawing.Size(61, 20);
            this.cb_divNum.TabIndex = 1;
            // 
            // cb_FirstBook
            // 
            this.cb_FirstBook.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_FirstBook.FormattingEnabled = true;
            this.cb_FirstBook.Location = new System.Drawing.Point(302, 4);
            this.cb_FirstBook.Name = "cb_FirstBook";
            this.cb_FirstBook.Size = new System.Drawing.Size(61, 20);
            this.cb_FirstBook.TabIndex = 1;
            // 
            // cb_FirstAuthor
            // 
            this.cb_FirstAuthor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_FirstAuthor.FormattingEnabled = true;
            this.cb_FirstAuthor.Location = new System.Drawing.Point(57, 4);
            this.cb_FirstAuthor.Name = "cb_FirstAuthor";
            this.cb_FirstAuthor.Size = new System.Drawing.Size(61, 20);
            this.cb_FirstAuthor.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(383, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "구분";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "저자기호";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(175, 206);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btn_Front
            // 
            this.btn_Front.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Front.Location = new System.Drawing.Point(966, 3);
            this.btn_Front.Name = "btn_Front";
            this.btn_Front.Size = new System.Drawing.Size(115, 51);
            this.btn_Front.TabIndex = 4;
            this.btn_Front.Text = "다    음 (F12)";
            this.btn_Front.UseVisualStyleBackColor = true;
            this.btn_Front.Click += new System.EventHandler(this.btn_Front_Click);
            // 
            // btn_Back
            // 
            this.btn_Back.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Back.Location = new System.Drawing.Point(845, 3);
            this.btn_Back.Name = "btn_Back";
            this.btn_Back.Size = new System.Drawing.Size(115, 51);
            this.btn_Back.TabIndex = 4;
            this.btn_Back.Text = "이    전 (F11)";
            this.btn_Back.UseVisualStyleBackColor = true;
            this.btn_Back.Click += new System.EventHandler(this.btn_Back_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.AcceptsTab = true;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("굴림체", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(838, 456);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "ㅇㅁㄴ먄륨\'ㄴ륨\nㅔㄴ륨레ㅏㅠㅁㄴ\n;라ㅠㅁㄴ;라뮨라;뮨리ㅏㅓ뮨라ㅣ뮨라ㅣㅁ뉾ㄴㄻㄴㄹ\nㅁㄴ ㄹ\nㅁ\nㄴㄻㄴㄻㄴㄻㄴㄻㄴㄻㄴㄻㄴㄻ\nㄴㄻㄴㄻㄴㅅㅎ 무에ㅐㅎ ㅜ" +
    "네후 ㅈㄷ\\ㅈㄷ\nㅅㅎㅈㅇㅎㄴㅇㅎ모로\n";
            this.richTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyDown);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Location = new System.Drawing.Point(13, 266);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(840, 458);
            this.panel2.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.text008);
            this.panel4.Controls.Add(this.text007);
            this.panel4.Controls.Add(this.text001);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(13, 230);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1087, 30);
            this.panel4.TabIndex = 246;
            // 
            // text008
            // 
            this.text008.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text008.Location = new System.Drawing.Point(419, 4);
            this.text008.MaxLength = 40;
            this.text008.Name = "text008";
            this.text008.Size = new System.Drawing.Size(331, 21);
            this.text008.TabIndex = 204;
            // 
            // text007
            // 
            this.text007.Location = new System.Drawing.Point(224, 4);
            this.text007.Name = "text007";
            this.text007.Size = new System.Drawing.Size(144, 21);
            this.text007.TabIndex = 2;
            this.text007.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // text001
            // 
            this.text001.Location = new System.Drawing.Point(36, 4);
            this.text001.Name = "text001";
            this.text001.Size = new System.Drawing.Size(142, 21);
            this.text001.TabIndex = 2;
            this.text001.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(8, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 12);
            this.label10.TabIndex = 206;
            this.label10.Text = "001";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(196, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 12);
            this.label9.TabIndex = 206;
            this.label9.Text = "007";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(919, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(162, 12);
            this.label11.TabIndex = 206;
            this.label11.Text = "단축키 F7 : 090 태그 생성";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(391, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 12);
            this.label4.TabIndex = 206;
            this.label4.Text = "008";
            // 
            // etcBox1
            // 
            this.etcBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.etcBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.etcBox1.Location = new System.Drawing.Point(4, 4);
            this.etcBox1.Name = "etcBox1";
            this.etcBox1.Size = new System.Drawing.Size(233, 220);
            this.etcBox1.TabIndex = 0;
            this.etcBox1.Text = "";
            // 
            // etcBox2
            // 
            this.etcBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.etcBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.etcBox2.Location = new System.Drawing.Point(4, 231);
            this.etcBox2.Name = "etcBox2";
            this.etcBox2.Size = new System.Drawing.Size(233, 223);
            this.etcBox2.TabIndex = 0;
            this.etcBox2.Text = "";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.etcBox2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.etcBox1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(859, 266);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 229F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(241, 458);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Marc_Plan_Sub_MarcEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 736);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Marc_Plan_Sub_MarcEdit";
            this.Text = "마크편집";
            this.Load += new System.EventHandler(this.Marc_Plan_Sub_MarcEdit_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cb_authorType;
        private System.Windows.Forms.ComboBox cb_divType;
        private System.Windows.Forms.ComboBox cb_divNum;
        private System.Windows.Forms.ComboBox cb_FirstBook;
        private System.Windows.Forms.ComboBox cb_FirstAuthor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker input_date;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox text008;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label col008res;
        private System.Windows.Forms.Label label99;
        public System.Windows.Forms.Label gov008res;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox text008col;
        public System.Windows.Forms.TextBox text008gov;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Front;
        private System.Windows.Forms.Button btn_Back;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_num;
        private System.Windows.Forms.TextBox tb_Mcode;
        private System.Windows.Forms.TextBox text001;
        private System.Windows.Forms.TextBox text007;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbl_ClassSymbol;
        private System.Windows.Forms.Label lbl_AuthorSymbol;
        public System.Windows.Forms.Button btn_EditNum;
        private System.Windows.Forms.Label lbl_SaveData;
        private System.Windows.Forms.RichTextBox etcBox2;
        private System.Windows.Forms.RichTextBox etcBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cb_grade;
    }
}