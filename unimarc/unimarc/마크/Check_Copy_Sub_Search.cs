﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;

namespace UniMarc.마크
{
    public partial class Check_Copy_Sub_Search : Form
    {
        Check_copy cc;
        public Check_Copy_Sub_Search(Check_copy _cc)
        {
            InitializeComponent();
            cc = _cc;
        }

        public void Init(string[] ary, string SearchText)
        {
            this.Text = string.Format("검색어 : [{0}]", SearchText);

            // Province, Area, lib_name, Code
            string[] grid = { "", "", "", "", "" };
            for (int a = 0; a < ary.Length; a++)
            {
                if (a % 5 == 0) grid[0] = ary[a];
                if (a % 5 == 1) grid[1] = ary[a];
                if (a % 5 == 2) grid[2] = string.Format("{0}_{1}_{2}", grid[0], grid[1], ary[a]);
                if (a % 5 == 3) grid[3] = ary[a];
                if (a % 5 == 4) {
                    grid[4] = ary[a];
                    dataGridView1.Rows.Add(grid);
                }
            }
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            SetCode(row);
            this.Close();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dataGridView1.Rows.Count <= 0) return;
                int row = dataGridView1.CurrentRow.Index;
                SetCode(row);
                this.Close();
            }
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        void SetCode(int row)
        {
            string Province = dataGridView1.Rows[row].Cells["Province"].Value.ToString();
            string Area = dataGridView1.Rows[row].Cells["Area"].Value.ToString();
            string Code = dataGridView1.Rows[row].Cells["Code"].Value.ToString();
            string URL = dataGridView1.Rows[row].Cells["URL"].Value.ToString();
            string lib_name = dataGridView1.Rows[row].Cells["lib_name"].Value.ToString();

            string lib_Category = string.Format("{0}_{1}", Province, Area);

            cc.lib_Category = lib_Category;
            cc.Code = Code;
            cc.URL = URL;
            cc.tb_SearchTarget.Text = lib_name;
            
            if (lib_name.Contains("Kolasys.net"))
            {
                Check_Copy_Login ccl = new Check_Copy_Login(this);
                ccl.Show();
            }
        }

        public void SetLogin(string id, string pw)
        {
            cc.lbl_ID.Text = id;
            cc.lbl_PW.Text = pw;
        }
    }
}
