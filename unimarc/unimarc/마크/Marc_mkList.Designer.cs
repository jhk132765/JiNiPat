﻿
namespace UniMarc.마크
{
    partial class Marc_mkList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.rb_autoNum = new System.Windows.Forms.RadioButton();
            this.rb_basicNum = new System.Windows.Forms.RadioButton();
            this.cb_listType = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chk_countSplit = new System.Windows.Forms.CheckBox();
            this.btn_mkList = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cb_authorType = new System.Windows.Forms.ComboBox();
            this.cb_divType = new System.Windows.Forms.ComboBox();
            this.cb_divNum = new System.Windows.Forms.ComboBox();
            this.cb_FirstBook = new System.Windows.Forms.ComboBox();
            this.cb_FirstAuthor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tb_listName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtb_etc = new System.Windows.Forms.RichTextBox();
            this.tb_outnum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rb_autoNum);
            this.panel1.Controls.Add(this.rb_basicNum);
            this.panel1.Controls.Add(this.cb_listType);
            this.panel1.Location = new System.Drawing.Point(12, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 30);
            this.panel1.TabIndex = 0;
            // 
            // rb_autoNum
            // 
            this.rb_autoNum.AutoSize = true;
            this.rb_autoNum.Location = new System.Drawing.Point(88, 6);
            this.rb_autoNum.Name = "rb_autoNum";
            this.rb_autoNum.Size = new System.Drawing.Size(71, 16);
            this.rb_autoNum.TabIndex = 0;
            this.rb_autoNum.TabStop = true;
            this.rb_autoNum.Text = "자동연번";
            this.rb_autoNum.UseVisualStyleBackColor = true;
            // 
            // rb_basicNum
            // 
            this.rb_basicNum.AutoSize = true;
            this.rb_basicNum.Location = new System.Drawing.Point(11, 6);
            this.rb_basicNum.Name = "rb_basicNum";
            this.rb_basicNum.Size = new System.Drawing.Size(71, 16);
            this.rb_basicNum.TabIndex = 0;
            this.rb_basicNum.TabStop = true;
            this.rb_basicNum.Text = "기존연번";
            this.rb_basicNum.UseVisualStyleBackColor = true;
            // 
            // cb_listType
            // 
            this.cb_listType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_listType.FormattingEnabled = true;
            this.cb_listType.Location = new System.Drawing.Point(165, 4);
            this.cb_listType.Name = "cb_listType";
            this.cb_listType.Size = new System.Drawing.Size(61, 20);
            this.cb_listType.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.chk_countSplit);
            this.panel2.Location = new System.Drawing.Point(252, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(76, 30);
            this.panel2.TabIndex = 0;
            // 
            // chk_countSplit
            // 
            this.chk_countSplit.AutoSize = true;
            this.chk_countSplit.Location = new System.Drawing.Point(5, 7);
            this.chk_countSplit.Name = "chk_countSplit";
            this.chk_countSplit.Size = new System.Drawing.Size(72, 16);
            this.chk_countSplit.TabIndex = 0;
            this.chk_countSplit.Text = "수량분리";
            this.chk_countSplit.UseVisualStyleBackColor = true;
            // 
            // btn_mkList
            // 
            this.btn_mkList.Location = new System.Drawing.Point(425, 10);
            this.btn_mkList.Name = "btn_mkList";
            this.btn_mkList.Size = new System.Drawing.Size(75, 23);
            this.btn_mkList.TabIndex = 1;
            this.btn_mkList.Text = "목록생성";
            this.btn_mkList.UseVisualStyleBackColor = true;
            this.btn_mkList.Click += new System.EventHandler(this.btn_mkList_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(506, 10);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 1;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cb_authorType);
            this.panel3.Controls.Add(this.cb_divType);
            this.panel3.Controls.Add(this.cb_divNum);
            this.panel3.Controls.Add(this.cb_FirstBook);
            this.panel3.Controls.Add(this.cb_FirstAuthor);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(12, 42);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(569, 30);
            this.panel3.TabIndex = 2;
            // 
            // cb_authorType
            // 
            this.cb_authorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_authorType.FormattingEnabled = true;
            this.cb_authorType.Location = new System.Drawing.Point(136, 4);
            this.cb_authorType.Name = "cb_authorType";
            this.cb_authorType.Size = new System.Drawing.Size(171, 20);
            this.cb_authorType.TabIndex = 1;
            // 
            // cb_divType
            // 
            this.cb_divType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_divType.FormattingEnabled = true;
            this.cb_divType.Location = new System.Drawing.Point(409, 4);
            this.cb_divType.Name = "cb_divType";
            this.cb_divType.Size = new System.Drawing.Size(88, 20);
            this.cb_divType.TabIndex = 1;
            this.cb_divType.SelectedIndexChanged += new System.EventHandler(this.cb_divType_SelectedIndexChanged);
            // 
            // cb_divNum
            // 
            this.cb_divNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_divNum.FormattingEnabled = true;
            this.cb_divNum.Location = new System.Drawing.Point(503, 4);
            this.cb_divNum.Name = "cb_divNum";
            this.cb_divNum.Size = new System.Drawing.Size(61, 20);
            this.cb_divNum.TabIndex = 1;
            // 
            // cb_FirstBook
            // 
            this.cb_FirstBook.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_FirstBook.FormattingEnabled = true;
            this.cb_FirstBook.Location = new System.Drawing.Point(313, 4);
            this.cb_FirstBook.Name = "cb_FirstBook";
            this.cb_FirstBook.Size = new System.Drawing.Size(61, 20);
            this.cb_FirstBook.TabIndex = 1;
            // 
            // cb_FirstAuthor
            // 
            this.cb_FirstAuthor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_FirstAuthor.FormattingEnabled = true;
            this.cb_FirstAuthor.Location = new System.Drawing.Point(68, 4);
            this.cb_FirstAuthor.Name = "cb_FirstAuthor";
            this.cb_FirstAuthor.Size = new System.Drawing.Size(61, 20);
            this.cb_FirstAuthor.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "저자기호";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.tb_listName);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(12, 78);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(569, 30);
            this.panel4.TabIndex = 2;
            // 
            // tb_listName
            // 
            this.tb_listName.Location = new System.Drawing.Point(68, 4);
            this.tb_listName.Name = "tb_listName";
            this.tb_listName.Size = new System.Drawing.Size(496, 21);
            this.tb_listName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "목 록 명";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(124, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(345, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "저장된 마크만 반출됩니다. 저자기호를 확인해주세요.";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 293);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(569, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rtb_etc);
            this.groupBox1.Location = new System.Drawing.Point(15, 134);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(566, 153);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "비 고 란";
            // 
            // rtb_etc
            // 
            this.rtb_etc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtb_etc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtb_etc.Location = new System.Drawing.Point(3, 17);
            this.rtb_etc.Name = "rtb_etc";
            this.rtb_etc.Size = new System.Drawing.Size(560, 133);
            this.rtb_etc.TabIndex = 0;
            this.rtb_etc.Text = "";
            // 
            // tb_outnum
            // 
            this.tb_outnum.Location = new System.Drawing.Point(340, 11);
            this.tb_outnum.Name = "tb_outnum";
            this.tb_outnum.Size = new System.Drawing.Size(31, 21);
            this.tb_outnum.TabIndex = 5;
            this.tb_outnum.Text = "0";
            this.tb_outnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(377, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "차";
            // 
            // Marc_mkList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 328);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_outnum);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_mkList);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Marc_mkList";
            this.Text = "마크목록생성";
            this.Load += new System.EventHandler(this.Marc_mkList_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cb_listType;
        private System.Windows.Forms.RadioButton rb_autoNum;
        private System.Windows.Forms.RadioButton rb_basicNum;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chk_countSplit;
        private System.Windows.Forms.Button btn_mkList;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cb_authorType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_FirstBook;
        private System.Windows.Forms.ComboBox cb_FirstAuthor;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_listName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ComboBox cb_divType;
        private System.Windows.Forms.ComboBox cb_divNum;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtb_etc;
        private System.Windows.Forms.TextBox tb_outnum;
        private System.Windows.Forms.Label label4;
    }
}