﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using Excel = Microsoft.Office.Interop.Excel;

namespace UniMarc.마크
{
    public partial class Marc_Plan_GearExcel : Form
    {
        Helper_DB db= new Helper_DB();

        public string[,] Content;
        public string ListName;

        public Marc_Plan_GearExcel()
        {
            InitializeComponent();
        }

        private void Marc_Plan_GearExcel_Load(object sender, EventArgs e)
        {
            db.DBcon();
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            string SearchText = tb_SearchBox.Text;

            string Area = "`idx`, `c_sangho`, `c_man`, `c_mantel`";
            string Table = "Client";
            string CMD = string.Format("SELECT {0} FROM {1} WHERE `c_sangho` LIKE \"%{2}%\";", Area, Table, SearchText);
            string[] ARY = db.DB_Send_CMD_Search(CMD).Split('|');

            string[] Grid = { "", "", "", "" };
            for (int a = 0; a < ARY.Length; a++)
            {
                if (a % 4 == 0) Grid[0] = ARY[a];
                if (a % 4 == 1) Grid[1] = ARY[a];
                if (a % 4 == 2) Grid[2] = ARY[a];
                if (a % 4 == 3) {
                    Grid[3] = ARY[a];
                    dataGridView1.Rows.Add(Grid);
                }
            }
        }

        private void tb_SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_Search_Click(null, null);

        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_MakeExcel_Click(object sender, EventArgs e)
        {
            string[] Title = {
                "연번", "등록번호", "도서명", "저자", "출판사", "비고" };

            string etc = richTextBox1.Text;

            Mk_Excel(Title, Content, etc);
        }

        #region MakeExcel SUB

        /// <summary>
        /// 엑셀 내보내기
        /// </summary>
        /// <param name="data">사용할 데이터</param>
        public void Mk_Excel(string[] title, string[,] data, string etc)
        {
            try
            {
                Excel.Application app = new Excel.Application();
                app.Visible = true;             // true 일때 엑셀이 작업되는 내용이 보임
                app.Interactive = false;        // false 일때 유저의 조작에 방해받지않음.

                Excel._Workbook wb = (Excel._Workbook)(app.Workbooks.Add(Missing.Value));   // 워크북 생성
                Excel._Worksheet ws = (Excel._Worksheet)app.ActiveSheet;    // 시트 가져옴
                Excel.PageSetup ps = ws.PageSetup;

                Excel.Range rng = null;     // 셀 처리 변수

                rng = ws.Range["A1", Excel_Sub(title) + "1"];
                rng.MergeCells = true;
                rng.Value2 = ListName;
                rng.Font.Size = 20;
                rng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                rng = ws.Range["A2", Excel_Sub(title) + "2"];
                rng.MergeCells = true;
                rng.Value2 = etc;
                rng.RowHeight = 24;
                rng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                rng = ws.Range["A3", Excel_Sub(title) + "3"];
                rng.Font.Color = Color.Blue;
                rng.HorizontalAlignment = 3;
                rng.Value2 = title;

                int length = data.GetLength(0) + 1;
                rng = ws.Range["A4", Excel_Sub(title) + length];
                rng.Value2 = data;
                ws.Columns.AutoFit();

                app.Interactive = true;
                app.Quit();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        #region MK_Excel_Sub
        private string Excel_Sub(string[] data)
        {
            string[] length = {
                "1", "2", "3", "4", "5",
                "6", "7", "8", "9", "10",
                "11", "12", "13", "14", "15",
                "16", "17", "18", "19", "20",
                "21", "22", "23", "24", "25", "26"
            };
            string[] Alpha = {
                "A", "B", "C", "D", "E",
                "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O",
                "P", "Q", "R", "S", "T",
                "U", "V", "W", "X", "Y", "Z"
            };

            string count = data.Length.ToString();
            string res = string.Empty;

            for (int a = 0; a < length.Length; a++)
            {
                if (length[a] == count)
                {
                    res = Alpha[a];
                }
            }
            return res;
        }
        #endregion
        #endregion

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            string idx = dataGridView1.Rows[e.RowIndex].Cells["idx"].Value.ToString();

            string cmd = string.Format("SELECT `c_etc` FROM `Client` WHERE `idx` = {0};", idx);
            richTextBox1.Text = db.DB_Send_CMD_Search(cmd).Replace("|", "");
        }
    }
}
