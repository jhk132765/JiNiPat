﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace UniMarc.마크
{
    internal class Macro_Gudu
    {
        String_Text st = new String_Text();
        Helper_DB db;

        public Macro_Gudu(Helper_DB db)
        {
            this.db = db;
        }

        bool isAuthorTag = false;
        string Author_Idx_6768 = "";
        public string FileType = "ANSI";
        

        public string MacroMarc(string ViewMarc, List<string> idx, List<string> TagArray, List<string> RunCode)
        {
            return RunningMacro(ViewMarc, idx.ToArray(), TagArray.ToArray(), RunCode.ToArray());
        }

        public string MacroMarc(string ViewMarc, string[] idx, string[] TagArray, string[] RunCode)
        {
            return RunningMacro(ViewMarc, idx, TagArray, RunCode);
        }

        private string RunningMacro(string ViewMarc, string[] idx, string[] TagArray, string[] RunCode)
        {
            List<string> SplitMarc = new List<string>(ViewMarc.Split('\n'));
            
            for (int a = 0; a < SplitMarc.Count; a++)
            {
                if (SplitMarc[a].Length < 2) continue;
                string ContentTag = SplitMarc[a].Substring(0, 3);

                string MacroData = Macro_Index(idx, TagArray, RunCode, ContentTag, SplitMarc[a]);
                SplitMarc[a] = MacroData;
            }

            List<string> result = new List<string>();
            foreach (string Macro in SplitMarc)
            {
                if (Macro == "")
                    continue;
                result.Add(Macro);
            }
            ViewMarc = String.Join("\n", result);

            for (int a = 0; a < idx.Length; a++)
            {
                ViewMarc = ChangeTagByIndex(idx[a], TagArray[a], RunCode[a], ViewMarc);
            }

            // 반출용 마크로 변환
            return st.made_Ori_marc(ViewMarc);
        }

        private string ChangeTagByIndex(string idx, string TagNum, string RunCode, string ViewMarc)
        {
            #region 방식이 달라 특별관리

            // 440n p x -> 490v [p-n-v 순서]로 변환 / 440n p x -> 830 n p v로 변환
            if (idx == "56" || idx == "57")
            {
                ViewMarc = Sub_440npv(ViewMarc, idx);
                return ViewMarc;
            }

            #endregion

            if (RunCode.IndexOf("/") < 0) return ViewMarc;

            bool isDelete = RunCode.Contains("del");

            string[] CodeSplit = RunCode.Split('/');

            string TargetTagNum = CodeSplit[0].Substring(0, 3);
            string TargetTag = CodeSplit[0].Substring(3, 1);
            string MoveTagNum = CodeSplit[1].Substring(0, 3);
            string MoveTag = CodeSplit[1].Substring(3, 1);
            
            ViewMarc = TagToOtherTag(TargetTagNum, TargetTag, MoveTagNum, MoveTag, ViewMarc, isDelete);

            #region 코드 간소화 작업진행중으로 주석처리 (나중에 삭제)

            /*
            // 525a를 500a로 부출 후 삭제
            if (idx == "49")
                ViewMarc = TagToOtherTag("525", "a", "500", "a", ViewMarc, true);
            // 440a를 830a로 복사
            if (idx == "50")
                ViewMarc = TagToOtherTag("440", "a", "830", "a", ViewMarc);
            // 440x를 490a로 복사
            if (idx == "51")
                ViewMarc = TagToOtherTag("440", "a", "490", "a", ViewMarc);
            // 049v를 090c로 복사
            if (idx == "52")
                ViewMarc = TagToOtherTag("049", "v", "090", "c", ViewMarc);
            // 020c를 950b로 복사
            if (idx == "53")
                ViewMarc = TagToOtherTag("020", "c", "950", "b", ViewMarc);
            // 245b -> 740a로 복사
            if (idx == "58")
                ViewMarc = TagToOtherTag("245", "b", "740", "a", ViewMarc);
            // 245p -> 740a로 복사
            if (idx == "62")
                ViewMarc = TagToOtherTag("245", "p", "740", "a", ViewMarc);
            // 900a -> 700a로 복사
            if (idx == "65")
                ViewMarc = TagToOtherTag("900", "a", "700", "a", ViewMarc, true);
            // 910a -> 710a로 복사
            if (idx == "66")
                ViewMarc = TagToOtherTag("910", "a", "710", "a", ViewMarc, true);
            */
            #endregion

            return ViewMarc;
        }

        #region ChangeTagByIndex_SUB

        private string Sub_440npv(string ViewMarc, string idx)
        {
            string oriMarc = st.made_Ori_marc(ViewMarc);
            string[] TakeMarc = { "440a", "440n", "440p", "440v" };
            string[] TakeTag = st.Take_Tag(oriMarc, TakeMarc);

            if (TakeTag[0] == "")
                return ViewMarc;

            if (idx == "56")        // 440 a n p v -> 490a 490v [p-n-v 순서]로 변환
            {
                // 구분 ,
                string Tag = "490\t  \t";
                string Content = "";

                if (TakeTag[0] != "")
                    Tag += "▼a" + TakeTag[0];
                Tag += "▼v";
                if (TakeTag[2] != "")
                    Content += TakeTag[2] + "-";
                if (TakeTag[1] != "")
                    Content += TakeTag[1] + "-";
                if (TakeTag[3] != "")
                    Content += TakeTag[3];

                Content = Content.TrimEnd('-');

                Tag += Content + "▲";
                ViewMarc = st.AddTagInMarc(Tag, ViewMarc);
            }

            if (idx == "57")        // 440 a n p v -> 830 a n p v로 변환
            {
                string Tag = string.Format("830\t  \t");
                if (TakeTag[0] != "")
                    Tag += "▼a" + TakeTag[0];
                if (TakeTag[1] != "")
                    Tag += "▼n" + TakeTag[1];
                if (TakeTag[2] != "")
                    Tag += "▼p" + TakeTag[2];
                if (TakeTag[3] != "")
                    Tag += "▼v" + TakeTag[3];
                Tag += "▲";
                ViewMarc = st.AddTagInMarc(Tag, ViewMarc);
            }
            return ViewMarc;
        }
        #endregion

        private string Macro_Index(string[] index, string[] TagArray, string[] RunCode, string ContentTag, string Content)
        {
            string[] SplitContent = Content.Split('\t');
            string Jisi = SplitContent[1];
            string Target = SplitContent[2];

            int cout = 0;
            foreach (string idx in index)
            {
                Jisi = ChangeJisi(idx, RunCode[cout], TagArray[cout], ContentTag, Jisi, Target);

                if (RunCode[cout] == "del") {
                    if (TagArray[cout] == ContentTag) {
                        Target = "";
                        break;
                    }
                }
                if (ContentTag == "020" && Jisi == "1 " && idx == "75") {
                    Target = "";
                }

                switch (ContentTag)
                {
                    case "020": Target = Index_020(idx, Target.Split('▼')); break;
                    case "100": Target = Index_100(idx, Target.Split('▼')); isAuthorTag = true; break;
                    case "110": Target = Index_110(idx, Target.Split('▼')); isAuthorTag = true; break;
                    case "111": Target = Index_111(idx, Target.Split('▼')); break;
                    case "245": Target = Index_245(idx, Target.Split('▼')); break;
                    case "246": Target = Index_246(idx, Target.Split('▼')); break;
                    case "260": Target = Index_260(idx, Target.Split('▼')); break;
                    case "300": Target = Index_300(idx, Target.Split('▼')); break;
                    case "440": Target = Index_440(idx, Target.Split('▼')); break;
                    case "700": Target = Index_700(idx, Target.Split('▼')); break;
                    case "710": Target = Index_710(idx, Target.Split('▼')); break;
                    case "830": Target = Index_830(idx, Target.Split('▼')); break;
                    case "950": Target = Index_950(idx, Target.Split('▼')); break;
                }
                cout++;
            }
            if (Target == "" || !Target.Contains("▲"))
                return "";

            return string.Format("{0}\t{1}\t{2}", ContentTag, Jisi, Target);
        }


        string ChangeJisi(string idx, string RunCode, string TargetTagNum, string RoofTagNum, string Jisi, string Content)
        {
            if (!RunCode.Contains("jisi")) return Jisi;

            if (idx == "48" && RoofTagNum == "245") return JiSi245(Content);

            if (idx == "70" && RoofTagNum == "440") return JiSi440(Content);

            if (TargetTagNum == RoofTagNum) return RunCode.Substring(0, 2);

            #region 코드 간소화 작업진행중으로 주석처리 (나중에 삭제)
            /*
            if (idx == "34" && RoofTagNum == "049") return "0 ";
            if (idx == "35" && RoofTagNum == "100") return "1 ";
            if (idx == "36" && RoofTagNum == "110") return "  ";
            if (idx == "37" && RoofTagNum == "490") return "10";
            if (idx == "38" && RoofTagNum == "500") return "  ";
            if (idx == "39" && RoofTagNum == "505") return "00";
            if (idx == "40" && RoofTagNum == "700") return "1 ";
            if (idx == "41" && RoofTagNum == "710") return "  ";
            if (idx == "42" && RoofTagNum == "740") return "02";
            if (idx == "43" && RoofTagNum == "830") return " 0";
            if (idx == "44" && RoofTagNum == "900") return "10";
            if (idx == "45" && RoofTagNum == "910") return " 0";
            if (idx == "46" && RoofTagNum == "940") return "0 ";
            if (idx == "47" && RoofTagNum == "950") return "0 ";
            */
            #endregion

            return Jisi;
        }

        #region JISI_SUB

        /// <summary>
        /// 245가 괄호 시작일 경우 [20], 괄호 시작이 아니며 100이나 110태그 사용시 [10], 해당 없을시 [00]
        /// </summary>
        /// <param name="Content"></param>
        /// <returns></returns>
        string JiSi245(string Content)
        {
            if (Content.StartsWith("▼a"))
            {
                Content = Content.Replace("▼a", "");
                if (Content.StartsWith("("))
                    return "20";
                else if (isAuthorTag)
                    return "10";
            }
            return "00";
        }

        /// <summary>
        /// 440가 괄호 시작일 경우 [10], 괄호 시작이 아니면 [00]
        /// </summary>
        /// <param name="Content"></param>
        /// <returns></returns>
        string JiSi440(string Content)
        {
            if (Content.StartsWith("▼a"))
            {
                Content = Content.Replace("▼a", "");
                if (Content.StartsWith("("))
                    return "10";
            }
            return "00";
        }

        #endregion

        #region IDX TAG NUM

        /// <summary>
        /// 020 매크로
        /// </summary>
        /// <param name="SplitContent">020 태그 내용</param>
        /// <returns>020태그 매크로 적용된 내용</returns>
        private string Index_020(string idx, string[] SplitContent)
        {

            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (SplitContent[a].StartsWith("c"))
                {
                    if (idx == "63")    // $c 원표시 제거
                        SplitContent[a] = SplitContent[a].Replace("\\", "");
                    if (idx == "71")    // $b 원표시 추가
                        SplitContent[a] = SplitContent[a].Insert(1, "\\");
                }

                if (a <= 1) continue;

                // $c 앞에 $g가 있을 경우 $c 앞에 ":"
                if (SplitContent[a].StartsWith("c") && idx == "0")
                    if (SplitContent[a - 1].StartsWith("g"))
                        if (!SplitContent[a - 1].EndsWith(":"))
                            SplitContent[a - 1] += ":";
            }

            return string.Join("▼", SplitContent);
        }

        /// <summary>
        /// 100 매크로
        /// </summary>
        /// <param name="Content">태그 내용</param>
        /// <returns>태그 매크로 적용된 내용</returns>
        private string Index_100(string idx, string[] SplitContent)
        {
            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (SplitContent[a].StartsWith("a") && idx == "67")
                    Author_Idx_6768 = SplitContent[a].Substring(1).Replace("▲", "");
                
                if (a <= 1) continue;

            }
            return string.Join("▼", SplitContent);
        }

        /// <summary>
        /// 110 매크로
        /// </summary>
        /// <param name="Content">110태그 내용</param>
        /// <returns>110태그 매크로 적용된 내용</returns>
        private string Index_110(string idx, string[] SplitContent)
        {
            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (SplitContent[a].StartsWith("a") && idx == "68")
                    Author_Idx_6768 = SplitContent[a].Substring(1).Replace("▲", "");

                if (a <= 1) continue;

                if (idx == "1")
                {
                    // $b 뒤에 $b가 있는 경우 $b앞에 "."
                    if (SplitContent[a].StartsWith("b"))
                        if (SplitContent[a - 1].StartsWith("b"))
                            if (!SplitContent[a - 1].EndsWith("."))
                                SplitContent[a - 1] += ".";
                }
            }
            return string.Join("▼", SplitContent);
        }

        /// <summary>
        /// 111 매크로
        /// </summary>
        /// <param name="Content">태그 내용</param>
        /// <returns>태그 매크로 적용된 내용</returns>
        private string Index_111(string idx, string[] SplitContent)
        {
            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (a <= 1) continue;
            }
            return string.Join("▼", SplitContent);
        }

        /// <summary>
        /// 245 매크로 적용
        /// </summary>
        /// <param name="Content">245태그 내용</param>
        /// <returns>245태그 매크로 적용된 내용</returns>
        private string Index_245(string idx, string[] SplitContent)
        {

            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (a <= 1) continue;

                // 두번째 $a 앞에 ":"
                if (SplitContent[a - 1].StartsWith("a") && idx == "2")
                    if (SplitContent[a].StartsWith("a"))
                        if (!SplitContent[a - 1].EndsWith(":"))
                            SplitContent[a - 1] += ":";
                
                // $b 앞에 ":"적용
                if (SplitContent[a].StartsWith("b") && idx == "3")
                    if (!SplitContent[a - 1].EndsWith(":"))
                        SplitContent[a - 1] += ":";

                // $n 앞에 "," 적용
                if (SplitContent[a].StartsWith("n") && idx == "4")
                    if (!SplitContent[a - 1].EndsWith(","))
                        SplitContent[a - 1] += ",";

                // $d 앞에 "/" 적용
                if (SplitContent[a].StartsWith("d") && idx == "6")
                    if (!SplitContent[a - 1].EndsWith("/"))
                        SplitContent[a - 1] += "/";

                // $d앞에 ,, 를 ,로 변경
                if (SplitContent[a].StartsWith("d") && idx == "7")
                    SplitContent[a] = SplitContent[a].Replace(",,", ",");


                // $p 앞에 $n이 나온 경우 "," 적용, $p앞에 $n이 없는 경우 "." 적용
                if (SplitContent[a].StartsWith("p") && idx == "8")
                {
                    if (SplitContent[a - 1].StartsWith("n"))
                    {
                        if (!SplitContent[a - 1].EndsWith(","))
                            SplitContent[a - 1] += ",";
                    }
                    else
                    {
                        if (!SplitContent[a - 1].EndsWith("."))
                            SplitContent[a - 1] += ".";
                    }
                }

                // $x 앞에 "=" 적용
                if (SplitContent[a].StartsWith("x") && idx == "5")
                    if (!SplitContent[a - 1].EndsWith("="))
                        SplitContent[a - 1] += "=";

                // $e 앞에 "," 적용
                if (SplitContent[a].StartsWith("e") && idx == "9")
                    if (!SplitContent[a - 1].EndsWith(","))
                        SplitContent[a - 1] += ",";
            }
            return string.Join("▼", SplitContent);
        }

        /// <summary>
        /// 246 매크로 적용
        /// </summary>
        /// <param name="Content">245태그 내용</param>
        /// <returns>245태그 매크로 적용된 내용</returns>
        private string Index_246(string idx, string[] SplitContent)
        {
            char[] Gudo = { ';', ':', '.', ',', '/' };

            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (a <= 1) continue;

                // $a 앞에 $i가 나온경우 $a 앞에 ":"적용
                if (SplitContent[a].StartsWith("a") && idx == "10")
                    if (SplitContent[a - 1].StartsWith("i"))
                        if (!SplitContent[a - 1].EndsWith(":"))
                            SplitContent[a - 1] += ":";

                // $b 앞에는 ":"적용
                if (SplitContent[a].StartsWith("b") && idx == "11")
                    if (!SplitContent[a - 1].EndsWith(":"))
                        SplitContent[a - 1] += ":";

                // $n 앞에는 "." 적용
                if (SplitContent[a].StartsWith("n") && idx == "12")
                    if (!SplitContent[a - 1].EndsWith("."))
                        SplitContent[a - 1] += ".";

                // $p 앞에 $n이 나온 경우 "," 적용, $p앞에 $n이 없는 경우 "."적용
                if (SplitContent[a].StartsWith("p") && idx == "13")
                {
                    if (SplitContent[a - 1].StartsWith("n"))
                    {
                        if (!SplitContent[a - 1].EndsWith(","))
                            SplitContent[a - 1] += ",";
                    }
                    else
                    {
                        if (!SplitContent[a - 1].EndsWith("."))
                            SplitContent[a - 1] += ".";
                    }
                }

                // $g 앞에는 ""
                if (SplitContent[a].StartsWith("g") && idx == "14")
                {
                    SplitContent[a - 1].TrimEnd(Gudo);
                    SplitContent[a - 1].TrimEnd(' ');
                }
            }

            return string.Join("▼", SplitContent);
        }

        private string Index_260(string idx, string[] SplitContent)
        {
            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (a <= 1) continue;

                // 두번째 $a 앞에 ";"
                if (SplitContent[a].StartsWith("a") && idx == "15")
                    if (!SplitContent[a - 1].EndsWith(";"))
                        SplitContent[a - 1] += ";";

                // $b 앞에는 ":" 적용
                if (SplitContent[a].StartsWith("b") && idx == "16")
                    if (!SplitContent[a - 1].EndsWith(":"))
                        SplitContent[a - 1] += ":";

                // $c 앞에는 "," 적용
                if (SplitContent[a].StartsWith("c") && idx == "17")
                    if (!SplitContent[a - 1].EndsWith(","))
                        SplitContent[a - 1] += ",";
            }

            return string.Join("▼", SplitContent);
        }

        private string Index_300(string idx, string[] SplitContent)
        {
            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (a <= 1) continue;

                // $b 앞에는 ":" 적용
                if (SplitContent[a].StartsWith("b") && idx == "18")
                    if (!SplitContent[a - 1].EndsWith(":"))
                        SplitContent[a - 1] += ":";

                if (SplitContent[a].StartsWith("c"))
                {
                    // $c 앞에는 ";" 적용
                    if( idx == "19")
                        if (!SplitContent[a - 1].EndsWith(";"))
                            SplitContent[a - 1] += ";";

                    // $c X포함 뒷부분 삭제
                    if (idx == "73") {
                        SplitContent[a] = SplitContent[a].ToLower();
                        string[] tmp = SplitContent[a].Split('x');
                        SplitContent[a] = tmp[0] + "cm▲";
                    }
                }

                // $e 앞에는 "+" 적용
                if (SplitContent[a].StartsWith("e") && idx == "20")
                    if (!SplitContent[a - 1].EndsWith("+"))
                        SplitContent[a - 1] += "+";
            }

            return string.Join("▼", SplitContent);
        }

        private string Index_440(string idx, string[] SplitContent)
        {

            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (a <= 1) continue;

                // $n 앞에는 "." 적용
                if (SplitContent[a].StartsWith("n") && idx == "21")
                    if (!SplitContent[a - 1].EndsWith("."))
                        SplitContent[a - 1] += ".";

                // $p 앞에 $n이 나온 경우 "," 적용, $p앞에 $n이 없는 경우 "."적용
                if (SplitContent[a].StartsWith("p") && idx == "22")
                {
                    if (SplitContent[a - 1].StartsWith("n")) {
                        if (SplitContent[a - 1].EndsWith(","))
                            SplitContent[a - 1] += ",";
                    }
                    else {
                        if (SplitContent[a - 1].EndsWith("."))
                            SplitContent[a - 1] += ".";
                    }
                }

                // $v 앞에는 ";" 적용
                if (SplitContent[a].StartsWith("v") && idx == "23")
                    if (!SplitContent[a - 1].EndsWith(";"))
                        SplitContent[a - 1] += ";";

                // $x 앞에는 "=" 적용
                if (SplitContent[a].StartsWith("x") && idx == "24")
                    if (!SplitContent[a - 1].EndsWith("="))
                        SplitContent[a - 1] += "=";
            }

            return string.Join("▼", SplitContent);
        }

        private string Index_700(string idx, string[] SplitContent)
        {
            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (idx == "67" && Author_Idx_6768 != "" && SplitContent[a].IndexOf(Author_Idx_6768) > -1)
                    return "";

                if (a <= 1) continue;
            }

            return string.Join("▼", SplitContent);
        }

        private string Index_710(string idx, string[] SplitContent)
        {
            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (idx == "68" && Author_Idx_6768 != "" && SplitContent[a].IndexOf(Author_Idx_6768) > -1)
                    return "";

                if (a <= 1) continue;
            }

            return string.Join("▼", SplitContent);
        }

        private string Index_830(string idx, string[] SplitContent)
        {
            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (a <= 1) continue;

                // $n 앞에는 "." 적용
                if (SplitContent[a].StartsWith("n") && idx == "25")
                    if (!SplitContent[a - 1].EndsWith("."))
                        SplitContent[a - 1] += ".";

                // $p 앞에 $n이 나온 경우 "," 적용, $p앞에 $n이 없는 경우 "."적용
                if (SplitContent[a].StartsWith("p") && idx == "26")
                {
                    if (SplitContent[a - 1].StartsWith("n"))
                    {
                        if (SplitContent[a - 1].EndsWith(","))
                            SplitContent[a - 1] += ",";
                    }
                    else
                    {
                        if (SplitContent[a - 1].EndsWith("."))
                            SplitContent[a - 1] += ".";
                    }
                }

                // $v 앞에는 ";" 적용
                if (SplitContent[a].StartsWith("v") && idx == "27")
                    if (!SplitContent[a - 1].EndsWith(";"))
                        SplitContent[a - 1] += ";";
            }

            return string.Join("▼", SplitContent);
        }

        private string Index_950(string idx, string[] SplitContent)
        {

            for (int a = 0; a < SplitContent.Length; a++)
            {
                if (SplitContent[a].StartsWith("b"))
                {
                
                    if (idx == "63")    // $b 원표시 제거
                        SplitContent[a] = SplitContent[a].Replace("\\", "");
                    if (idx == "71")    // $b 원표시 추가
                        SplitContent[a] = SplitContent[a].Insert(1, "\\");
                }

                if (a <= 1) continue;
            }
            return string.Join("▼", SplitContent);
        }
        #endregion

        /// <summary>
        /// 타겟 태그를 가져와서 적용할 태그에 새로 만들고 필요에 따라 기존 식별기호 데이터를 삭제함
        /// </summary>
        /// <param name="TargetTagNum">가져올 태그넘버</param>
        /// <param name="TargetTagAccount">가져올 태그의 식별기호</param>
        /// <param name="ApplyTagNum">적용할 태그넘버</param>
        /// <param name="ApplyTagAccount">적용할 태그의 식별기호</param>
        /// <param name="ViewMarc">대상 마크(뷰형태)</param>
        /// <param name="jisi">적용할 지시기호</param>
        /// <param name="isDelete">가져올 태그의 식별기호 데이터 삭제여부</param>"
        string TagToOtherTag(string TargetTagNum, string TargetTagAccount, string ApplyTagNum, string ApplyTagAccount, string ViewMarc, bool isDelete = false, string jisi = "  ")
        {
            List<string> SplitMarc = new List<string>(ViewMarc.Split('\n'));
            
            string Target = "";

            // 가져올 태그 정보 추출
            foreach (string TagMarc in SplitMarc)
            {
                if (TagMarc.Length < 2)
                    continue;

                if (TagMarc.StartsWith(TargetTagNum))
                {
                    Target = st.GetMiddelString(TagMarc, "▼" + TargetTagAccount, "▼").Replace("▲", "");
                    break;
                }
            }

            // 추출된 값이 없을 경우, 처음 들어온 그대로 반환
            if (Target == "")
                return ViewMarc;

            // 추출한 태그의 정보를 적용할 태그에 복사
            SplitMarc = MoveTag(ApplyTagNum, jisi, ApplyTagAccount, Target, SplitMarc);

            if (isDelete)
            {
                for (int a = 0; a < SplitMarc.Count; a++)
                {
                    if (SplitMarc[a].Length < 2)
                        continue;

                    if (SplitMarc[a].StartsWith(TargetTagNum) && SplitMarc[a].Contains(Target))
                    {
                        SplitMarc.RemoveAt(a);
                        
                        // string RemoveTarget = string.Format("▼{0}{1}", TargetTagAccount, Target);
                        // SplitMarc[a] = SplitMarc[a].Replace(RemoveTarget, "");
                        break;
                    }
                }
            }

            return string.Join("\n", SplitMarc);
        }

        /// <summary>가져온 태그의 정보를 적용할 태그에 복사함.</summary>
        /// <param name="TagNum">적용할 태그 번호</param>
        /// <param name="Jisi">적용할 지시기호</param>
        /// <param name="TagAccount">적용할 식별기호</param>
        /// <param name="TagContent">적용할 내용</param>
        /// <param name="Marc">적용할 List형 마크</param>
        /// <returns>적용된 List형 마크</returns>
        List<string> MoveTag(string TagNum, string Jisi, string TagAccount, string TagContent, List<string> Marc)
        {
            int AddIndex = -1;

            for (int a = 0; a < Marc.Count; a++)
            {
                if (Marc[a].Length < 2)
                    continue;

                int targetNum = Convert.ToInt32(Marc[a].Substring(0, 3));
                int tagNum = Convert.ToInt32(TagNum);

                if (targetNum > tagNum)
                {
                    AddIndex = a;
                    break;
                }
            }
            string Item = string.Format("{0}\t{1}\t▼{2}{3}▲", TagNum, Jisi, TagAccount, TagContent);
            Marc.Insert(AddIndex, Item);

            return Marc;
        }
    }
}
