﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace UniMarc.마크
{
    public partial class CD_LP_AddList : Form
    {
        Skill_Grid sg = new Skill_Grid();
        Helper_DB db = new Helper_DB();

        public CD_LP_AddList()
        {
            InitializeComponent();
        }

        private void CD_LP_AddList_Load(object sender, EventArgs e)
        {
            db.DBcon();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            sg.Print_Grid_Num(sender, e);
        }

        private void btn_DelRow_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell.RowIndex > 0) return;
            int row = dataGridView1.CurrentCell.RowIndex;

            dataGridView1.Rows.RemoveAt(row);
        }

        private void btn_Empty_Click(object sender, EventArgs e)
        {
            tb_ExpectList.Text = "";
            dataGridView1.Rows.Clear();
        }

        private void btn_AddList_Click(object sender, EventArgs e)
        {
            string listName = tb_ExpectList.Text;
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string compidx = Properties.Settings.Default.compidx;
            string user = Properties.Settings.Default.User;

            if (listName == "") {
                MessageBox.Show("목록명이 비어있습니다!");
                return;
            }

            string[] list_col = { "compidx", "listname", "date", "user" };
            string[] list_data = { compidx, listName, date, user };

            string listCMD = db.DB_INSERT("DVD_List", list_col, list_data);

            string[] Product_col = { "listname", "date", "compidx", "user", "code", "title", "artist", "comp", "price", "num" };
            string[] Product_data = { listName, date, compidx, user, "", "", "", "", "", "" };
            string ProductCMD = string.Format("INSERT INTO {0} {1} value", "DVD_List_Product", Product_col);
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (dataGridView1.Rows[a].Cells["Code"].Value == null)   dataGridView1.Rows[a].Cells["Code"].Value = "";
                if (dataGridView1.Rows[a].Cells["title"].Value == null)  dataGridView1.Rows[a].Cells["title"].Value = "";
                if (dataGridView1.Rows[a].Cells["artist"].Value == null) dataGridView1.Rows[a].Cells["artist"].Value = "";
                if (dataGridView1.Rows[a].Cells["Comp"].Value == null)   dataGridView1.Rows[a].Cells["Comp"].Value = "";
                if (dataGridView1.Rows[a].Cells["Price"].Value == null)  dataGridView1.Rows[a].Cells["Price"].Value = "";

                Product_data[4] = dataGridView1.Rows[a].Cells["Code"].Value.ToString();
                Product_data[5] = dataGridView1.Rows[a].Cells["title"].Value.ToString();
                Product_data[6] = dataGridView1.Rows[a].Cells["artist"].Value.ToString();
                Product_data[7] = dataGridView1.Rows[a].Cells["Comp"].Value.ToString();
                Product_data[8] = dataGridView1.Rows[a].Cells["Price"].Value.ToString();
                Product_data[9] = a.ToString();

                ProductCMD += db.DB_INSERT_SUB("value", Product_data) + ",";
            }
            ProductCMD = ProductCMD.TrimEnd(',');
            ProductCMD += ";";

            db.DB_Send_CMD_reVoid(listCMD);
            db.DB_Send_CMD_reVoid(ProductCMD);

            MessageBox.Show("저장되었습니다.");
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            sg.Excel_to_DataGridView(sender, e);
        }
    }
}
