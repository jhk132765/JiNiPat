﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace UniMarc.마크
{
    public partial class ShowDeleteMarc : Form
    {
        Helper_DB db = new Helper_DB();

        public ShowDeleteMarc()
        {
            InitializeComponent();
        }

        private void ShowDeleteMarc_Load(object sender, EventArgs e)
        {
            db.DBcon();

            string Area =
                "`idx`, `grade`, `user_Del`, `date`, `ISBN`, " +
                "`서명`, `저자`, `출판사`, `가격`, `marc`";
            string compidx = Properties.Settings.Default.compidx;
            string cmd = string.Format("SELECT {0} FROM {1} WHERE `compidx` = {2};", Area, "Marc_Del", compidx);
            string res = db.self_Made_Cmd(cmd);
            InputGrid(res);
        }

        void InputGrid(string Query)
        {
            string[] QueryArray = Query.Split('|');
            string[] Grid = {
                "", "", "", "", "",
                "", "", "", "", "" };

            int count = Grid.Length;
            for (int a = 0; a < QueryArray.Length; a++)
            {
                if (a % count == 0) Grid[0] = QueryArray[a];
                if (a % count == 1) Grid[1] = QueryArray[a];
                if (a % count == 2) Grid[2] = QueryArray[a];
                if (a % count == 3) Grid[3] = QueryArray[a];
                if (a % count == 4) Grid[4] = QueryArray[a];
                if (a % count == 5) Grid[5] = QueryArray[a];
                if (a % count == 6) Grid[6] = QueryArray[a];
                if (a % count == 7) Grid[7] = QueryArray[a];
                if (a % count == 8) Grid[8] = QueryArray[a];
                if (a % count == 9) {
                    Grid[9] = QueryArray[a];
                    dataGridView1.Rows.Add(Grid);
                }
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }
    }
}
