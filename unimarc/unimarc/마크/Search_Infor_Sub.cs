﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UniMarc.마크;
using WindowsFormsApp1.Mac;

namespace WindowsFormsApp1.마크
{
    public partial class Search_Infor_Sub : Form
    {
        Search_Infor si;
        public string isbn = "";
        public string marc = "";
        public Search_Infor_Sub(Search_Infor _si)
        {
            InitializeComponent();
            si = _si;
        }
        private void Search_Infor_Sub_Load(object sender, EventArgs e)
        {
            try
            {
                string pic_url = "http://image.kyobobook.co.kr/images/book/xlarge/" + isbn.Substring(isbn.Length - 3, 3) + "/x" + isbn + ".jpg";
                pictureBox1.ImageLocation = pic_url;

                richTextBox1.Text = marc.Replace("", "\n");
            }
            catch { }
        }
        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            Zoom_Picture zp = new Zoom_Picture();
            zp.url = pictureBox1.ImageLocation;
            zp.Show();
        }
    }
}
