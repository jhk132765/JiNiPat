﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;
using System.IO;

namespace UniMarc.마크
{
    public partial class Mac_List_Add : Form
    {
        Helper_DB db = new Helper_DB();
        Skill_Grid sg = new Skill_Grid();
        Mac_List ml;

        public Mac_List_Add(Mac_List _ml)
        {
            InitializeComponent();
            ml = _ml;
        }

        private void Mac_List_Add_Load(object sender, EventArgs e)
        {
            db.DBcon();

            string compidx = Properties.Settings.Default.compidx;
            string MyName = Properties.Settings.Default.User;

            #region 콤보박스 채우기

            // 담당자
            string CompQuery = string.Format("SELECT `comp_name` FROM `Comp` WHERE `idx` = {0}", compidx);
            string cmd = string.Format("SELECT `name` FROM `User_Data` WHERE `affil` = ({0});", CompQuery);
            string[] cmdResult = db.DB_Send_CMD_Search(cmd).Split('|');

            foreach (string UserName in cmdResult)
            {
                if (UserName != "")
                    cb_User.Items.Add(UserName);
            }

            cb_User.SelectedItem = MyName;

            // 쉼표구분
            string[] Gubun = { "tap", ",", "|" };
            cb_Gubun.Items.AddRange(Gubun);
            cb_Gubun.SelectedIndex = 0;
            #endregion
        }

        private void Delivery_TextChanged(object sender, EventArgs e)
        {
            string divComp = tb_divComp.Text;
            string divName = tb_divName.Text;

            string ListName = string.Format("[{0}]{1}", divComp, divName);
            tb_ExpectList.Text = ListName;
        }

        private void btn_Empty_Click(object sender, EventArgs e)
        {
            tb_divComp.Text = "";
            tb_divName.Text = "";
            dataGridView1.Rows.Clear();
        }

        private void btn_AddList_Click(object sender, EventArgs e)
        {
            string compidx = Properties.Settings.Default.compidx;
            string Today = DateTime.Now.ToString("yyyy-MM-dd");
            string listName = tb_ExpectList.Text;
            string charge = cb_User.Text;

            if (!CopyCheck(compidx, listName, Today)) {
                MessageBox.Show("목록이 중복되었습니다! 다시 확인해주세요.", "Error");
                return;
            }
            if (tb_ExpectList.Text == "") {
                MessageBox.Show("목록명이 비어있습니다! 다시 확인해주세요.", "Error");
                return;
            }

            string InBook_Area = "`compidx`, `list_name`, `date`, `header`, `num`, " +
                                 "`book_name`, `author`, `book_comp`, `pay`, `count`, " +
                                 "`total`, `isbn_marc`, `l_idx`";
            List<string> InBook_List = new List<string>();

            int TotalCount = 0;
            string InList_Idx = "㏓InList_Idx♠";

            string[] InList_Tbl = { "comp_num", "date", "list_name", "m_charge", "state", "vol", "chk_marc" };
            string[] InList_Col = { compidx, Today, listName, charge, "진행", TotalCount.ToString(), "1" };

            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                GridNotNull(a);

                if (dataGridView1.Rows[a].Cells["BookName"].Value.ToString() == "")
                    break;

                string header   = dataGridView1.Rows[a].Cells["header"].Value.ToString();
                string num      = dataGridView1.Rows[a].Cells["num"].Value.ToString();
                string bookname = dataGridView1.Rows[a].Cells["BookName"].Value.ToString();
                string author   = dataGridView1.Rows[a].Cells["Author"].Value.ToString();
                string bookcomp = dataGridView1.Rows[a].Cells["BookComp"].Value.ToString();
                string price    = dataGridView1.Rows[a].Cells["Price"].Value.ToString().Replace(",", "");
                string count    = dataGridView1.Rows[a].Cells["Count"].Value.ToString().Replace(",", "");
                string total    = dataGridView1.Rows[a].Cells["Total"].Value.ToString().Replace(",", "");
                string isbn     = dataGridView1.Rows[a].Cells["ISBN"].Value.ToString();

                string tmp = string.Format(
                    "(\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\", " +
                    "\"{5}\", \"{6}\", \"{7}\", \"{8}\", \"{9}\", " +
                    "\"{10}\", \"{11}\", \"{12}\")",
                    compidx, listName, Today, header, num,
                    bookname, author, bookcomp, price, count,
                    total, isbn, InList_Idx);

                TotalCount += Convert.ToInt32(count);

                InBook_List.Add(tmp);
            }

            InList_Col[5] = TotalCount.ToString();
            string InList_Cmd = db.DB_INSERT("Obj_List", InList_Tbl, InList_Col);

            db.DB_Send_CMD_reVoid(InList_Cmd);

            string listIdx = db.DB_Send_CMD_Search(db.More_DB_Search("Obj_List", InList_Tbl, InList_Col, "`idx`")).Replace("|", "");
            string InBook_Col = string.Join(", ", InBook_List).Replace(InList_Idx, listIdx);
            string InBook_Cmd = string.Format("INSERT INTO `Obj_List_Book` ({0}) VALUES {1};", InBook_Area, InBook_Col);

            db.DB_Send_CMD_reVoid(InBook_Cmd);

            MessageBox.Show("저장되었습니다!");
            ml.btn_Lookup_Click(null, null);
        }

        #region AddList_Sub

        /// <summary>
        /// INSERT할 목록 중복체크
        /// </summary>
        /// <param name="compidx"></param>
        /// <param name="listName"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        bool CopyCheck(string compidx, string listName, string date)
        {
            string cmd = string.Format(
                "SELECT `list_name` " +
                "FROM `Obj_List` " +
                "WHERE `comp_num` = \"{0}\" " +
                "AND `list_name` = \"{1}\" " +
                "AND `date` = \"{2}\";", compidx, listName, date);
            string cmdResult = db.DB_Send_CMD_Search(cmd);

            if (cmdResult.Length > 2)
                return false;

            return true;
        }

        /// <summary>
        /// Grid에서 Null인 부분을 ""로 바꿔줌
        /// </summary>
        /// <param name="row">rowIndex</param>
        void GridNotNull(int row)
        {
            for (int a = 0; a < dataGridView1.Columns.Count; a++)
            {
                if (a == 5 || a == 7) {
                    if (dataGridView1.Rows[row].Cells[a].Value == null)
                        dataGridView1.Rows[row].Cells[a].Value = "0";
                }

                else if (a == 6) {
                    if (dataGridView1.Rows[row].Cells[a].Value == null)
                        dataGridView1.Rows[row].Cells[a].Value = "1";
                }

                else if (dataGridView1.Rows[row].Cells[a].Value == null)
                    dataGridView1.Rows[row].Cells[a].Value = "";
            }
        }

        #endregion

        private void btn_Upload_Click(object sender, EventArgs e)
        {
            string FilePath = "";
            openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                FilePath = openFileDialog1.FileName;

            string[] textValue = File.ReadAllLines(FilePath);

            char Gubun = cb_Gubun.Text[0];
            if (cb_Gubun.SelectedIndex == 0)
            {
                Gubun = '\t';
            }
            
            if (textValue.Length < 0)
                return;
            try
            {
                foreach (string Value in textValue)
                {
                    string[] grid = Value.Split(Gubun);
                    dataGridView1.Rows.Add(grid);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("오류가 발생했습니다.\n" + ex.Message, "Error");
            }
        }

        private void btn_DelRow_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("삭제하시겠습니까?", "삭제", MessageBoxButtons.YesNo);

            if (result == DialogResult.No) return;

            int row = dataGridView1.CurrentCell.RowIndex;

            if (row < 0) return;

            dataGridView1.Rows.Remove(dataGridView1.Rows[row]);
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            sg.Print_Grid_Num(sender, e);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            sg.Excel_to_DataGridView(sender, e);
        }
    }
}
