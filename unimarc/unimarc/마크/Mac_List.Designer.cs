﻿namespace WindowsFormsApp1.Mac
{
    partial class Mac_List
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_Search = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.start_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.end_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.list_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.work_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unstock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.check = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cb_state = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Lookup = new System.Windows.Forms.Button();
            this.btn_Excel = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btn_Merge = new System.Windows.Forms.Button();
            this.btn_Progress = new System.Windows.Forms.Button();
            this.btn_Completion = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.btn_AddList = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "목록 검색";
            // 
            // tb_Search
            // 
            this.tb_Search.Location = new System.Drawing.Point(73, 9);
            this.tb_Search.Name = "tb_Search";
            this.tb_Search.Size = new System.Drawing.Size(241, 21);
            this.tb_Search.TabIndex = 6;
            this.tb_Search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_Search_KeyDown);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.start_date,
            this.end_date,
            this.list_name,
            this.work_name,
            this.count,
            this.stock,
            this.unstock,
            this.state,
            this.etc,
            this.charge,
            this.check});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 40;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1124, 623);
            this.dataGridView1.TabIndex = 48;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.Visible = false;
            // 
            // start_date
            // 
            this.start_date.HeaderText = "목록일자";
            this.start_date.Name = "start_date";
            // 
            // end_date
            // 
            this.end_date.HeaderText = "완료일자";
            this.end_date.Name = "end_date";
            // 
            // list_name
            // 
            this.list_name.HeaderText = "목록명";
            this.list_name.Name = "list_name";
            this.list_name.Width = 200;
            // 
            // work_name
            // 
            this.work_name.HeaderText = "작업명";
            this.work_name.Name = "work_name";
            // 
            // count
            // 
            this.count.HeaderText = "수량";
            this.count.Name = "count";
            this.count.Width = 80;
            // 
            // stock
            // 
            this.stock.HeaderText = "입고";
            this.stock.Name = "stock";
            this.stock.Visible = false;
            this.stock.Width = 70;
            // 
            // unstock
            // 
            this.unstock.HeaderText = "미입고";
            this.unstock.Name = "unstock";
            this.unstock.Visible = false;
            this.unstock.Width = 70;
            // 
            // state
            // 
            this.state.HeaderText = "상태";
            this.state.Name = "state";
            this.state.Width = 70;
            // 
            // etc
            // 
            this.etc.HeaderText = "비고";
            this.etc.Name = "etc";
            this.etc.Width = 280;
            // 
            // charge
            // 
            this.charge.HeaderText = "담당자";
            this.charge.Name = "charge";
            this.charge.Width = 70;
            // 
            // check
            // 
            this.check.HeaderText = "V";
            this.check.Name = "check";
            this.check.Width = 35;
            // 
            // cb_state
            // 
            this.cb_state.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_state.FormattingEnabled = true;
            this.cb_state.Location = new System.Drawing.Point(365, 9);
            this.cb_state.Name = "cb_state";
            this.cb_state.Size = new System.Drawing.Size(74, 20);
            this.cb_state.TabIndex = 49;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(334, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "상태";
            // 
            // btn_Lookup
            // 
            this.btn_Lookup.Location = new System.Drawing.Point(489, 3);
            this.btn_Lookup.Name = "btn_Lookup";
            this.btn_Lookup.Size = new System.Drawing.Size(61, 32);
            this.btn_Lookup.TabIndex = 50;
            this.btn_Lookup.Text = "조   회";
            this.btn_Lookup.UseVisualStyleBackColor = true;
            this.btn_Lookup.Click += new System.EventHandler(this.btn_Lookup_Click);
            // 
            // btn_Excel
            // 
            this.btn_Excel.Location = new System.Drawing.Point(958, 3);
            this.btn_Excel.Name = "btn_Excel";
            this.btn_Excel.Size = new System.Drawing.Size(61, 32);
            this.btn_Excel.TabIndex = 50;
            this.btn_Excel.Text = "엑셀반출";
            this.btn_Excel.UseVisualStyleBackColor = true;
            this.btn_Excel.Click += new System.EventHandler(this.btn_Excel_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(1025, 3);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(61, 32);
            this.btn_Close.TabIndex = 50;
            this.btn_Close.Text = "닫   기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(891, 3);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(61, 32);
            this.btn_Save.TabIndex = 50;
            this.btn_Save.Text = "체크사항\r\n저장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Merge
            // 
            this.btn_Merge.Location = new System.Drawing.Point(623, 3);
            this.btn_Merge.Name = "btn_Merge";
            this.btn_Merge.Size = new System.Drawing.Size(61, 32);
            this.btn_Merge.TabIndex = 51;
            this.btn_Merge.Text = "목록병합";
            this.btn_Merge.UseVisualStyleBackColor = true;
            this.btn_Merge.Click += new System.EventHandler(this.btn_Merge_Click);
            // 
            // btn_Progress
            // 
            this.btn_Progress.Location = new System.Drawing.Point(690, 3);
            this.btn_Progress.Name = "btn_Progress";
            this.btn_Progress.Size = new System.Drawing.Size(61, 32);
            this.btn_Progress.TabIndex = 51;
            this.btn_Progress.Text = "진행처리";
            this.btn_Progress.UseVisualStyleBackColor = true;
            this.btn_Progress.Click += new System.EventHandler(this.btn_Progress_Click);
            // 
            // btn_Completion
            // 
            this.btn_Completion.Location = new System.Drawing.Point(757, 3);
            this.btn_Completion.Name = "btn_Completion";
            this.btn_Completion.Size = new System.Drawing.Size(61, 32);
            this.btn_Completion.TabIndex = 51;
            this.btn_Completion.Text = "완료처리";
            this.btn_Completion.UseVisualStyleBackColor = true;
            this.btn_Completion.Click += new System.EventHandler(this.btn_Completion_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(824, 3);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(61, 32);
            this.btn_Delete.TabIndex = 51;
            this.btn_Delete.Text = "목록삭제";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_AddList
            // 
            this.btn_AddList.Location = new System.Drawing.Point(556, 3);
            this.btn_AddList.Name = "btn_AddList";
            this.btn_AddList.Size = new System.Drawing.Size(61, 32);
            this.btn_AddList.TabIndex = 50;
            this.btn_AddList.Text = "목록생성";
            this.btn_AddList.UseVisualStyleBackColor = true;
            this.btn_AddList.Click += new System.EventHandler(this.btn_AddList_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_Delete);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btn_Completion);
            this.panel1.Controls.Add(this.tb_Search);
            this.panel1.Controls.Add(this.btn_Progress);
            this.panel1.Controls.Add(this.cb_state);
            this.panel1.Controls.Add(this.btn_Merge);
            this.panel1.Controls.Add(this.btn_Lookup);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.btn_AddList);
            this.panel1.Controls.Add(this.btn_Excel);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1124, 38);
            this.panel1.TabIndex = 52;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1124, 623);
            this.panel2.TabIndex = 53;
            // 
            // Mac_List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1124, 661);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Mac_List";
            this.ShowIcon = false;
            this.Text = "마크목록";
            this.Load += new System.EventHandler(this.Mac_List_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_Search;
        private System.Windows.Forms.ComboBox cb_state;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Excel;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_Save;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btn_Merge;
        private System.Windows.Forms.Button btn_Progress;
        private System.Windows.Forms.Button btn_Completion;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.Button btn_AddList;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn start_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn end_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn work_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn stock;
        private System.Windows.Forms.DataGridViewTextBoxColumn unstock;
        private System.Windows.Forms.DataGridViewTextBoxColumn state;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
        private System.Windows.Forms.DataGridViewTextBoxColumn charge;
        private System.Windows.Forms.DataGridViewTextBoxColumn check;
        public System.Windows.Forms.Button btn_Lookup;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}