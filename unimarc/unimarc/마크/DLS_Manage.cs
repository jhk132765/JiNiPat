﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniMarc.마크
{
    public partial class DLS_Manage : Form
    {
        public DLS_Manage()
        {
            InitializeComponent();
        }
        public WebBrowser Browser
        {
            get { return webBrowser1; }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this.Text = webBrowser1.Url.ToString();

            if (webBrowser1.Url.ToString() == "about:blank")
                this.Close();
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            string isbn = "";

            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
            {
                if (he.Id == "isbn_issn")
                    isbn = he.GetAttribute("value");
            }
            if (isbn == "") return;

            string isbn3 = isbn.Substring(10);
            string url = string.Format("http://image.kyobobook.co.kr/images/book/xlarge/{0}/x{1}.jpg", isbn3, isbn);
            textBox1.Text = url;

            ((TextBox)sender).SelectAll();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
