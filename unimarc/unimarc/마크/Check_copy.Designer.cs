﻿namespace WindowsFormsApp1.Mac
{
    partial class Check_copy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rb_isNumber = new System.Windows.Forms.RadioButton();
            this.rb_isHave = new System.Windows.Forms.RadioButton();
            this.btn_SearchList = new System.Windows.Forms.Button();
            this.btn_Start = new System.Windows.Forms.Button();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.SearchCount = new System.Windows.Forms.NumericUpDown();
            this.btn_Close = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_SearchTarget = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_SiteDenote = new System.Windows.Forms.Button();
            this.lbl_PW = new System.Windows.Forms.Label();
            this.lbl_ID = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_ApplyFilter = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.chk_RemoveBrit = new System.Windows.Forms.CheckBox();
            this.btn_ResultEmpty = new System.Windows.Forms.Button();
            this.btn_GridReset = new System.Windows.Forms.Button();
            this.btn_OpenMemo = new System.Windows.Forms.Button();
            this.chk_spChar = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchCount)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rb_isNumber);
            this.panel1.Controls.Add(this.rb_isHave);
            this.panel1.Controls.Add(this.btn_SearchList);
            this.panel1.Controls.Add(this.btn_Start);
            this.panel1.Controls.Add(this.btn_Stop);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(629, 34);
            this.panel1.TabIndex = 0;
            // 
            // rb_isNumber
            // 
            this.rb_isNumber.AutoSize = true;
            this.rb_isNumber.Location = new System.Drawing.Point(394, 8);
            this.rb_isNumber.Name = "rb_isNumber";
            this.rb_isNumber.Size = new System.Drawing.Size(71, 16);
            this.rb_isNumber.TabIndex = 5;
            this.rb_isNumber.TabStop = true;
            this.rb_isNumber.Text = "숫자표출";
            this.rb_isNumber.UseVisualStyleBackColor = true;
            // 
            // rb_isHave
            // 
            this.rb_isHave.AutoSize = true;
            this.rb_isHave.Checked = true;
            this.rb_isHave.Location = new System.Drawing.Point(318, 8);
            this.rb_isHave.Name = "rb_isHave";
            this.rb_isHave.Size = new System.Drawing.Size(71, 16);
            this.rb_isHave.TabIndex = 5;
            this.rb_isHave.TabStop = true;
            this.rb_isHave.Text = "소장표출";
            this.rb_isHave.UseVisualStyleBackColor = true;
            // 
            // btn_SearchList
            // 
            this.btn_SearchList.Location = new System.Drawing.Point(4, 4);
            this.btn_SearchList.Name = "btn_SearchList";
            this.btn_SearchList.Size = new System.Drawing.Size(305, 24);
            this.btn_SearchList.TabIndex = 3;
            this.btn_SearchList.Text = "목록 검색";
            this.btn_SearchList.UseVisualStyleBackColor = true;
            this.btn_SearchList.Click += new System.EventHandler(this.btn_SearchList_Click);
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(470, 4);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(75, 24);
            this.btn_Start.TabIndex = 2;
            this.btn_Start.Text = "검색시작";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // btn_Stop
            // 
            this.btn_Stop.Location = new System.Drawing.Point(548, 4);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(75, 24);
            this.btn_Stop.TabIndex = 2;
            this.btn_Stop.Text = "검색중지";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // SearchCount
            // 
            this.SearchCount.Location = new System.Drawing.Point(491, 6);
            this.SearchCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.SearchCount.Name = "SearchCount";
            this.SearchCount.Size = new System.Drawing.Size(54, 21);
            this.SearchCount.TabIndex = 2;
            this.SearchCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(548, 4);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 24);
            this.btn_Close.TabIndex = 2;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(444, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "검색 수";
            // 
            // tb_SearchTarget
            // 
            this.tb_SearchTarget.Location = new System.Drawing.Point(66, 6);
            this.tb_SearchTarget.Name = "tb_SearchTarget";
            this.tb_SearchTarget.Size = new System.Drawing.Size(243, 21);
            this.tb_SearchTarget.TabIndex = 1;
            this.tb_SearchTarget.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_SearchTarget_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "검색대상";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btn_SiteDenote);
            this.panel2.Controls.Add(this.lbl_PW);
            this.panel2.Controls.Add(this.lbl_ID);
            this.panel2.Controls.Add(this.SearchCount);
            this.panel2.Controls.Add(this.tb_SearchTarget);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btn_Close);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(629, 34);
            this.panel2.TabIndex = 0;
            // 
            // btn_SiteDenote
            // 
            this.btn_SiteDenote.Location = new System.Drawing.Point(317, 5);
            this.btn_SiteDenote.Name = "btn_SiteDenote";
            this.btn_SiteDenote.Size = new System.Drawing.Size(77, 23);
            this.btn_SiteDenote.TabIndex = 4;
            this.btn_SiteDenote.Text = "사이트 표출";
            this.btn_SiteDenote.UseVisualStyleBackColor = true;
            this.btn_SiteDenote.Click += new System.EventHandler(this.btn_SiteDenote_Click);
            // 
            // lbl_PW
            // 
            this.lbl_PW.AutoSize = true;
            this.lbl_PW.Location = new System.Drawing.Point(379, 10);
            this.lbl_PW.Name = "lbl_PW";
            this.lbl_PW.Size = new System.Drawing.Size(9, 12);
            this.lbl_PW.TabIndex = 3;
            this.lbl_PW.Text = " ";
            // 
            // lbl_ID
            // 
            this.lbl_ID.AutoSize = true;
            this.lbl_ID.Location = new System.Drawing.Point(316, 10);
            this.lbl_ID.Name = "lbl_ID";
            this.lbl_ID.Size = new System.Drawing.Size(9, 12);
            this.lbl_ID.TabIndex = 3;
            this.lbl_ID.Text = " ";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.book_name,
            this.book_comp,
            this.Count});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(629, 636);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명(총서명)";
            this.book_name.Name = "book_name";
            this.book_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.book_name.Width = 350;
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            this.book_comp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.book_comp.Width = 150;
            // 
            // Count
            // 
            this.Count.HeaderText = "검색 수";
            this.Count.Name = "Count";
            this.Count.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Count.Width = 70;
            // 
            // btn_ApplyFilter
            // 
            this.btn_ApplyFilter.Location = new System.Drawing.Point(317, 4);
            this.btn_ApplyFilter.Name = "btn_ApplyFilter";
            this.btn_ApplyFilter.Size = new System.Drawing.Size(75, 24);
            this.btn_ApplyFilter.TabIndex = 2;
            this.btn_ApplyFilter.Text = "필터적용";
            this.btn_ApplyFilter.UseVisualStyleBackColor = true;
            this.btn_ApplyFilter.Click += new System.EventHandler(this.btn_ApplyFilter_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(629, 738);
            this.panel3.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dataGridView1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 102);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(629, 636);
            this.panel6.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.chk_RemoveBrit);
            this.panel4.Controls.Add(this.btn_ResultEmpty);
            this.panel4.Controls.Add(this.btn_GridReset);
            this.panel4.Controls.Add(this.btn_OpenMemo);
            this.panel4.Controls.Add(this.btn_ApplyFilter);
            this.panel4.Controls.Add(this.chk_spChar);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 68);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(629, 34);
            this.panel4.TabIndex = 2;
            // 
            // chk_RemoveBrit
            // 
            this.chk_RemoveBrit.AutoSize = true;
            this.chk_RemoveBrit.Location = new System.Drawing.Point(113, 8);
            this.chk_RemoveBrit.Name = "chk_RemoveBrit";
            this.chk_RemoveBrit.Size = new System.Drawing.Size(128, 16);
            this.chk_RemoveBrit.TabIndex = 0;
            this.chk_RemoveBrit.Text = "괄호안의 문자 제거";
            this.chk_RemoveBrit.UseVisualStyleBackColor = true;
            // 
            // btn_ResultEmpty
            // 
            this.btn_ResultEmpty.Location = new System.Drawing.Point(471, 4);
            this.btn_ResultEmpty.Name = "btn_ResultEmpty";
            this.btn_ResultEmpty.Size = new System.Drawing.Size(75, 24);
            this.btn_ResultEmpty.TabIndex = 2;
            this.btn_ResultEmpty.Text = "결과 비움";
            this.btn_ResultEmpty.UseVisualStyleBackColor = true;
            this.btn_ResultEmpty.Click += new System.EventHandler(this.btn_ResultEmpty_Click);
            // 
            // btn_GridReset
            // 
            this.btn_GridReset.Location = new System.Drawing.Point(548, 4);
            this.btn_GridReset.Name = "btn_GridReset";
            this.btn_GridReset.Size = new System.Drawing.Size(75, 24);
            this.btn_GridReset.TabIndex = 2;
            this.btn_GridReset.Text = "표 비우기";
            this.btn_GridReset.UseVisualStyleBackColor = true;
            this.btn_GridReset.Click += new System.EventHandler(this.btn_GridReset_Click);
            // 
            // btn_OpenMemo
            // 
            this.btn_OpenMemo.Location = new System.Drawing.Point(394, 4);
            this.btn_OpenMemo.Name = "btn_OpenMemo";
            this.btn_OpenMemo.Size = new System.Drawing.Size(75, 24);
            this.btn_OpenMemo.TabIndex = 2;
            this.btn_OpenMemo.Text = ".txt 열기";
            this.btn_OpenMemo.UseVisualStyleBackColor = true;
            this.btn_OpenMemo.Click += new System.EventHandler(this.btn_OpenMemo_Click);
            // 
            // chk_spChar
            // 
            this.chk_spChar.AutoSize = true;
            this.chk_spChar.Location = new System.Drawing.Point(7, 8);
            this.chk_spChar.Name = "chk_spChar";
            this.chk_spChar.Size = new System.Drawing.Size(100, 16);
            this.chk_spChar.TabIndex = 0;
            this.chk_spChar.Text = "특수문자 제거";
            this.chk_spChar.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.webBrowser1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(629, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(662, 738);
            this.panel5.TabIndex = 4;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(662, 738);
            this.webBrowser1.TabIndex = 2;
            // 
            // Check_copy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1291, 738);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Name = "Check_copy";
            this.Text = "복본조사";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Check_copy_FormClosing);
            this.Load += new System.EventHandler(this.Check_copy_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchCount)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.Button btn_Start;
        public System.Windows.Forms.TextBox tb_SearchTarget;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.Button btn_SearchList;
        private System.Windows.Forms.Button btn_ApplyFilter;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.CheckBox chk_spChar;
        private System.Windows.Forms.CheckBox chk_RemoveBrit;
        public System.Windows.Forms.NumericUpDown SearchCount;
        private System.Windows.Forms.RadioButton rb_isNumber;
        private System.Windows.Forms.RadioButton rb_isHave;
        private System.Windows.Forms.Button btn_GridReset;
        private System.Windows.Forms.Button btn_OpenMemo;
        private System.Windows.Forms.Button btn_ResultEmpty;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn Count;
        public System.Windows.Forms.Label lbl_PW;
        public System.Windows.Forms.Label lbl_ID;
        private System.Windows.Forms.Button btn_SiteDenote;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}