﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using WindowsFormsApp1;

namespace AuthorSymbol
{
    /// <summary>
    /// 저자기호 클래스 이재철 저자 표 포함
    /// </summary>
    public class Symbol
    {
        /// <summary>
        /// 저자기호 첫음, 초성
        /// </summary>
        public string[] authorFirst = { "첫음", "초성" };

        /// <summary>
        /// 저자기호표
        /// </summary>
        public string[] authorType =
        {
 /* 00 */   "이재철 1표", 
 /* 01 */   "이재철 2표",
 /* 02 */   "이재철 3표(실용형 가표)", 
 /* 03 */   "이재철 4표(실용형 까표)",
 /* 04 */   "이재철 5표(완전형 가표)", 
 /* 05 */   "이재철 6표(완전형 가표)",
 /* 06 */   "이재철 7표(실용형 하표)", 
 /* 07 */   "이재철 7표(동서저자기호표)",
 /* 08 */   "이재철 8표(완전형 하표)", 
 /* 09 */   "이재철 8표(동서저자기호표)",
 /* 10 */   "장일세 저자기호표", 
 /* 11 */   "커터 샌본 저자기호표",
 /* 12 */   "엘러드 저자기호법",
 /* 13 */   "동서양 저자기호법 (국중)"
        };

        /// <summary>
        /// 저자기호 도서명사용
        /// </summary>
        public string[] authorBook = { "첫음", "초성", "중성" };
        
        /// <summary>
        /// 이재철 1표
        /// </summary>
        /// <param name="author">대표저자</param>
        /// <param name="book">도서명</param>
        /// <param name="rb">사용된 라디오박스 (3개)</param>
        /// <returns>완성된 저자기호</returns>
        private string Symbol_1(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ', 'ㄲ', 'ㅋ',
                'ㄴ', 'ㄹ',
                'ㄷ', 'ㄸ',
                'ㅁ',
                'ㅂ', 'ㅃ', 'ㅍ',
                'ㅅ', 'ㅆ',
                'ㅇ',
                'ㅈ', 'ㅉ', 'ㅊ',
                'ㅎ'
            };
            string[] Jaum_code = {
                "1", "1", "1",
                "2", "2",
                "3", "3",
                "4",
                "5", "5", "5",
                "6", "6",
                "7",
                "8", "8", "8",
                "9"
            };

            char[] Moum = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ',
                'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ',
                'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moum_code = {
                "1",
                "2", "2", "2",
                "3", "3",
                "4", "4",
                "5", "5", "5", "5",
                "6",
                "7", "7", "7", "7", "7",
                "8", "8",
                "9"
            };
            #endregion

            string res = string.Empty;
            HANGUL_INFO info1 = HangulJaso.DevideJaso(author);
            if (info1.isHangul == "NH") { return info1.isHangul; }
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info1.chars[0] == Jaum[a])
                {
                    res = Jaum_code[a];
                    break;
                }
            }
            for (int a = 0; a < Moum.Length; a++)
            {
                if (info1.chars[1] == Moum[a])
                {
                    res += Moum_code[a];
                    break;
                }
            }
            return res;
        }

        /// <summary>
        /// 이재철 2표
        /// </summary>
        /// <param name="author">대표저자</param>
        /// <param name="book">도서명</param>
        /// <param name="rb">사용된 라디오박스 (3개)</param>
        /// <returns>완성된 저자기호</returns>
        private string Symbol_2(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ', 'ㄲ',
                'ㄴ',
                'ㄷ', 'ㄸ',
                'ㄹ',
                'ㅁ',
                'ㅂ', 'ㅃ',
                'ㅅ', 'ㅆ',
                'ㅇ',
                'ㅈ', 'ㅉ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ'
            };
            string[] Jaum_code =
            {
                "1", "1",
                "21",
                "22", "22",
                "23",
                "3",
                "4", "4",
                "5", "5",
                "6",
                "7", "7",
                "81",
                "82",
                "83",
                "84",
                "9"
            };

            char[] Moum =
            {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ',
                'ㅕ', 'ㅖ',
                'ㅗ',
                'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moum_code = {
                "1",
                "2", "2", "2",
                "3", "3",
                "4", "4",
                "5",
                "6", "6", "6", "6",
                "7", "7", "7", "7", "7",
                "8", "8",
                "9"
            };
            #endregion

            string res = string.Empty;

            HANGUL_INFO info1 = HangulJaso.DevideJaso(author);
            if (info1.isHangul == "NH") { return info1.isHangul; }
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info1.chars[0] == Jaum[a])
                {
                    res = Jaum_code[a];
                    break;
                }
            }
            for (int a = 0; a < Moum.Length; a++)
            {
                if (info1.chars[1] == Moum[a])
                {
                    res += Moum_code[a];
                    break;
                }
            }
            return res;
        }

        /// <summary>
        /// 이재철 3표
        /// </summary>
        /// <param name="author">대표저자</param>
        /// <param name="book">도서명</param>
        /// <param name="rb">사용된 라디오박스 (3개)</param>
        /// <returns>완성된 저자기호</returns>
        private string Symbol_3(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ', 'ㄲ',
                'ㄴ',
                'ㄷ', 'ㄸ',
                'ㄹ',
                'ㅁ',
                'ㅂ', 'ㅃ',
                'ㅅ', 'ㅆ',
                'ㅇ',
                'ㅈ', 'ㅉ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ'
            };
            string[] Jaum_code = {
                "1", "1",
                "19",
                "2", "2",
                "29",
                "3",
                "4", "4",
                "5", "5",
                "6",
                "7", "7",
                "8",
                "87",
                "88",
                "89",
                "9"
            };

            char[] Moum = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ',
                'ㅕ', 'ㅖ',
                'ㅗ',
                'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moum_code = {
                "1",
                "2", "2", "2",
                "3", "3",
                "4", "4",
                "5",
                "6", "6", "6", "6",
                "7", "7", "7", "7", "7",
                "8", "8",
                "9"
            };

            char[] Moumㄱㄷ = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ',
                'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㄱㄷ_code = {
                "1",
                "2", "2", "2",
                "3", "3",
                "4", "4",
                "5", "5", "5", "5", "5",
                "6", "6", "6", "6", "6",
                "7", "7",
                "8"
            };

            char[] Moumㅊ = {
                'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㅊ_code = {
                "1", "1", "1", "1",
                "2", "2", "2", "2",
                "3", "3", "3", "3", "3",
                "4", "4", "4", "4", "4",
                "5", "5",
                "6"
            };
            #endregion

            string res = string.Empty;
            bool[] isㄱㄷㅊ = { false, false };

            HANGUL_INFO info2 = HangulJaso.DevideJaso(author);
            if (info2.isHangul == "NH") return info2.isHangul;
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info2.chars[0] == Jaum[a])
                {
                    res += Jaum_code[a];
                    if (res == "1" || res == "2") isㄱㄷㅊ[0] = true;
                    else if (res == "8") isㄱㄷㅊ[1] = true;
                    break;
                }
            }

            if (isㄱㄷㅊ[0])
            {
                for (int a = 0; a < Moumㄱㄷ.Length; a++)
                {
                    if (info2.chars[1] == Moumㄱㄷ[a])
                    {
                        res += Moumㄱㄷ_code[a];
                        break;
                    }
                }
            }
            else if (isㄱㄷㅊ[1])
            {
                for (int a = 0; a < Moumㅊ.Length; a++)
                {
                    if (info2.chars[1] == Moumㅊ[a])
                    {
                        res += Moumㅊ_code[a];
                        break;
                    }
                }
            }
            else
            {
                for (int a = 0; a < Moum.Length; a++)
                {
                    if (info2.chars[1] == Moum[a])
                    {
                        res += Moum_code[a];
                        break;
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// 이재철 4표
        /// </summary>
        /// <param name="author">대표저자</param>
        /// <param name="book">도서명</param>
        /// <param name="rb">사용된 라디오박스 (3개)</param>
        /// <returns>완성된 저자기호</returns>
        private string Symbol_4(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ',
                'ㄲ',
                'ㄴ',
                'ㄷ',
                'ㄸ',
                'ㄹ',
                'ㅁ',
                'ㅂ',
                'ㅃ',
                'ㅅ',
                'ㅆ',
                'ㅇ',
                'ㅈ',
                'ㅉ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ'
            };
            string[] Jaum_code = {
                "1",
                "18",
                "19",
                "2",
                "28",
                "29",
                "3",
                "4",
                "49",
                "5",
                "59",
                "6",
                "7",
                "79",
                "8",
                "87",
                "88",
                "89",
                "9"
            };

            char[] Moum = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ',
                'ㅕ', 'ㅖ',
                'ㅗ',
                'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moum_code = {
                "1",
                "2", "2", "2",
                "3", "3",
                "4", "4",
                "5",
                "6", "6", "6", "6",
                "7", "7", "7", "7", "7",
                "8", "8",
                "9"
            };

            char[] Moumㄱㄷ = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㄱㄷ_code = {
                "1",
                "2", "2", "2",
                "3", "3", "3", "3",
                "4", "4", "4", "4", "4",
                "5", "5", "5", "5", "5",
                "6", "6",
                "7"
            };

            char[] Moumㅊ = {
                'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㅊ_code = {
                "1", "1", "1", "1",
                "2", "2", "2", "2",
                "3", "3", "3", "3", "3",
                "4", "4", "4", "4", "4",
                "5", "5",
                "6"
            };
            #endregion

            string res = string.Empty;
            
            bool[] isㄱㄷㅊ = { false, false };

            HANGUL_INFO info2 = HangulJaso.DevideJaso(author);
            if (info2.isHangul == "NH") return info2.isHangul;
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info2.chars[0] == Jaum[a])
                {
                    res += Jaum_code[a];
                    if (res == "1" || res == "2") isㄱㄷㅊ[0] = true;
                    else if (res == "8") isㄱㄷㅊ[1] = true;
                    break;
                }
            }

            if (isㄱㄷㅊ[0])
            {
                for (int a = 0; a < Moumㄱㄷ.Length; a++)
                {
                    if (info2.chars[1] == Moumㄱㄷ[a])
                    {
                        res += Moumㄱㄷ_code[a];
                        break;
                    }
                }
            }
            else if (isㄱㄷㅊ[1])
            {
                for (int a = 0; a < Moumㅊ.Length; a++)
                {
                    if (info2.chars[1] == Moumㅊ[a])
                    {
                        res += Moumㅊ_code[a];
                        break;
                    }
                }
            }
            else
            {
                for (int a = 0; a < Moum.Length; a++)
                {
                    if (info2.chars[1] == Moum[a])
                    {
                        res += Moum_code[a];
                        break;
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// 이재철 5표
        /// </summary>
        /// <param name="author">대표저자</param>
        /// <param name="book">도서명</param>
        /// <param name="rb">사용된 라디오박스 (3개)</param>
        /// <returns>완성된 저자기호</returns>
        private string Symbol_5(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ', 'ㄲ',
                'ㄴ',
                'ㄷ', 'ㄸ',
                'ㄹ',
                'ㅁ',
                'ㅂ', 'ㅃ',
                'ㅅ', 'ㅆ',
                'ㅇ',
                'ㅈ', 'ㅉ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ' };
            string[] Jaum_code = {
                "1", "1",
                "19",
                "2", "2",
                "29",
                "3",
                "4", "4",
                "5", "5",
                "6",
                "7", "7",
                "8",
                "87",
                "88",
                "89",
                "9" };

            char[] Moum = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ' };
            string[] Moum_code = {
                "2",
                "3", "3", "3",
                "4", "4", "4", "4",
                "5", "5", "5", "5", "5",
                "6", "6", "6", "6", "6",
                "7", "7",
                "8" };

            char[] Moumㅊ = {
                'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅣ' };
            string[] Moumㅊ_code = {
                "2", "2", "2", "2",
                "3", "3", "3", "3",
                "4", "4", "4", "4", "4",
                "5", "5", "5", "5", "5",
                "6" };
            #endregion

            string res = string.Empty;

            bool isㅊ = false;

            HANGUL_INFO info1 = HangulJaso.DevideJaso(author);
            if (info1.isHangul == "NH") { return info1.isHangul; }
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info1.chars[0] == Jaum[a])
                {
                    res += Jaum_code[a];
                    if (res == "8") isㅊ = true;
                    break;
                }
            }

            if (isㅊ)
            {
                for (int a = 0; a < Moumㅊ.Length; a++)
                {
                    if (info1.chars[1] == Moumㅊ[a])
                    {
                        res += Moumㅊ_code[a];
                        break;
                    }
                }
            }
            else
            {
                for (int a = 0; a < Moum.Length; a++)
                {
                    if (info1.chars[1] == Moum[a])
                    {
                        res += Moum_code[a];
                        break;
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// 이재철 6표
        /// </summary>
        /// <param name="author">대표저자</param>
        /// <param name="book">도서명</param>
        /// <param name="rb">사용된 라디오박스 (3개)</param>
        /// <returns>완성된 저자기호</returns>
        private string Symbol_6(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ',
                'ㄲ',
                'ㄴ',
                'ㄷ',
                'ㄸ',
                'ㄹ',
                'ㅁ',
                'ㅂ',
                'ㅃ',
                'ㅅ',
                'ㅆ',
                'ㅇ',
                'ㅈ',
                'ㅉ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ'
            };
            string[] Jaum_code = {
                "1",
                "18",
                "19",
                "2",
                "28",
                "29",
                "3",
                "4",
                "49",
                "5",
                "59",
                "6",
                "7",
                "79",
                "8",
                "87",
                "88",
                "89",
                "9"
            };

            char[] Moum = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moum_code = {
                "2",
                "3", "3", "3",
                "4", "4", "4", "4",
                "5", "5", "5", "5", "5",
                "6", "6", "6", "6", "6",
                "7", "7",
                "8"
            };

            char[] Moumㄱㄷ = {
                'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㄱㄷ_code = {
                "2", "2", "2", "2",
                "3", "3", "3", "3",
                "4", "4", "4", "4", "4",
                "5", "5", "5", "5", "5",
                "6", "6",
                "7"
            };

            char[] Moumㅊ = {
                'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ', 'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㅊ_code = {
                "2", "2", "2", "2",
                "3", "3", "3", "3",
                "4", "4", "4", "4", "4",
                "5", "5", "5", "5", "5", "5", "5",
                "6"
            };
            #endregion

            string res = string.Empty;
            
            bool[] isㄱㄷㅊ = { false, false };

            HANGUL_INFO info2 = HangulJaso.DevideJaso(author);
            if (info2.isHangul == "NH") return info2.isHangul;
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info2.chars[0] == Jaum[a])
                {
                    res += Jaum_code[a];
                    if (res == "1" || res == "2") isㄱㄷㅊ[0] = true;
                    else if (res == "8") isㄱㄷㅊ[1] = true;
                    break;
                }
            }

            if (isㄱㄷㅊ[0])
            {
                for (int a = 0; a < Moumㄱㄷ.Length; a++)
                {
                    if (info2.chars[1] == Moumㄱㄷ[a])
                    {
                        res += Moumㄱㄷ_code[a];
                        break;
                    }
                }
            }
            else if (isㄱㄷㅊ[1])
            {
                for (int a = 0; a < Moumㅊ.Length; a++)
                {
                    if (info2.chars[1] == Moumㅊ[a])
                    {
                        res += Moumㅊ_code[a];
                        break;
                    }
                }
            }
            else
            {
                for (int a = 0; a < Moum.Length; a++)
                {
                    if (info2.chars[1] == Moum[a])
                    {
                        res += Moum_code[a];
                        break;
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// 이재철 7표
        /// </summary>
        /// <param name="author">대표저자</param>
        /// <param name="book">도서명</param>
        /// <param name="rb">사용된 라디오박스 (3개)</param>
        /// <returns>완성된 저자기호</returns>
        private string Symbol_7(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ',
                'ㄴ',
                'ㄷ',
                'ㄹ',
                'ㅁ',
                'ㅂ',
                'ㅅ',
                'ㅈ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ',
                'ㄲ',
                'ㄸ',
                'ㅃ',
                'ㅆ',
                'ㅉ',
                'ㅇ'
            };
            string[] Jaum_code = {
                "1",
                "19",
                "2",
                "29",
                "3",
                "4",
                "5",
                "6",
                "66",
                "67",
                "68",
                "69",
                "7",
                "8",
                "86",
                "87",
                "88",
                "89",
                "9"
            };

            char[] Moum = {
                'ㅏ', 'ㅑ',
                'ㅓ',
                'ㅕ',
                'ㅗ', 'ㅛ',
                'ㅜ',
                'ㅠ', 'ㅡ',
                'ㅣ',
                'ㅐ', 'ㅒ', 'ㅔ', 'ㅖ',
                'ㅚ', 'ㅟ', 'ㅢ', 'ㅘ', 'ㅝ', 'ㅙ', 'ㅞ'
            };
            string[] Moum_code = {
                "1", "1",
                "2",
                "3",
                "4", "4",
                "5",
                "6", "6",
                "7",
                "8", "8", "8", "8",
                "9", "9", "9", "9", "9", "9", "9"
            };

            char[] Moumㄱㄷ = {
                'ㅏ', 'ㅑ',
                'ㅓ',
                'ㅕ',
                'ㅗ', 'ㅛ',
                'ㅜ',
                'ㅠ', 'ㅡ',
                'ㅣ',
                'ㅐ', 'ㅒ', 'ㅔ', 'ㅖ', 'ㅚ', 'ㅟ', 'ㅢ', 'ㅙ', 'ㅞ'
            };
            string[] Moumㄱㄷ_code = {
                "1", "1",
                "2",
                "3",
                "4", "4",
                "5",
                "6", "6",
                "7",
                "8", "8", "8", "8", "8", "8", "8", "8", "8"
            };

            char[] Moumㅈㄲ = {
                'ㅏ', 'ㅑ',
                'ㅓ', 'ㅕ',
                'ㅗ', 'ㅛ',
                'ㅜ', 'ㅠ', 'ㅡ',
                'ㅣ', 'ㅐ', 'ㅒ', 'ㅔ', 'ㅖ', 'ㅚ', 'ㅟ', 'ㅢ', 'ㅘ', 'ㅙ', 'ㅝ', 'ㅞ'
            };
            string[] Moumㅈㄲ_code = {
                "1", "1",
                "2", "2",
                "3", "3",
                "4", "4", "4",
                "5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5"
            };
            #endregion

            string res = string.Empty;
            
            bool[] isㄱㄷㅈㄲ = { false, false };

            HANGUL_INFO info2 = HangulJaso.DevideJaso(author);
            if (info2.isHangul == "NH") return info2.isHangul;
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info2.chars[0] == Jaum[a])
                {
                    res += Jaum_code[a];
                    if (res == "1" || res == "2") isㄱㄷㅈㄲ[0] = true;
                    else if (res == "6" || res == "8") isㄱㄷㅈㄲ[1] = true;
                    break;
                }
            }

            if (isㄱㄷㅈㄲ[0])
            {
                for (int a = 0; a < Moumㄱㄷ.Length; a++)
                {
                    if (info2.chars[1] == Moumㄱㄷ[a])
                    {
                        res += Moumㄱㄷ_code[a];
                        break;
                    }
                }
            }
            else if (isㄱㄷㅈㄲ[1])
            {
                for (int a = 0; a < Moumㅈㄲ.Length; a++)
                {
                    if (info2.chars[1] == Moumㅈㄲ[a])
                    {
                        res += Moumㅈㄲ_code[a];
                        break;
                    }
                }
            }
            else
            {
                for (int a = 0; a < Moum.Length; a++)
                {
                    if (info2.chars[1] == Moum[a])
                    {
                        res += Moum_code[a];
                        break;
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// 이재철 8표
        /// </summary>
        /// <param name="author">대표저자</param>
        /// <param name="book">도서명</param>
        /// <param name="rb">사용된 라디오박스 (3개)</param>
        /// <returns>완성된 저자기호</returns>
        private string Symbol_8(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ',
                'ㄴ',
                'ㄷ',
                'ㄹ',
                'ㅁ',
                'ㅂ',
                'ㅅ',
                'ㅈ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ',
                'ㄲ',
                'ㄸ',
                'ㅃ',
                'ㅆ',
                'ㅉ',
                'ㅇ'
            };
            string[] Jaum_code = {
                "1",
                "19",
                "2",
                "29",
                "3",
                "4",
                "5",
                "6",
                "66",
                "67",
                "68",
                "69",
                "7",
                "8",
                "86",
                "87",
                "88",
                "89",
                "9"
            };

            char[] Moum = {
                'ㅏ', 'ㅑ',
                'ㅓ',
                'ㅕ',
                'ㅗ', 'ㅛ',
                'ㅜ', 'ㅠ', 'ㅡ',
                'ㅣ',
                'ㅐ', 'ㅒ', 'ㅔ', 'ㅖ', 'ㅚ', 'ㅟ', 'ㅢ', 'ㅙ', 'ㅞ'
            };
            string[] Moum_code = {
                "2", "2",
                "3",
                "4",
                "5", "5",
                "6", "6", "6",
                "7",
                "8", "8", "8", "8", "8", "8", "8", "8", "8"
            };

            char[] Moumㅈㄲ = {
                'ㅏ', 'ㅑ', 'ㅓ', 'ㅕ',
                'ㅗ', 'ㅛ',
                'ㅜ', 'ㅠ', 'ㅡ',
                'ㅣ', 'ㅐ', 'ㅒ', 'ㅔ', 'ㅖ', 'ㅚ', 'ㅟ', 'ㅢ', 'ㅙ', 'ㅞ'
            };
            string[] Moumㅈㄲ_code = {
                "2", "2", "2", "2",
                "3", "3",
                "4", "4", "4",
                "5", "5", "5", "5", "5", "5", "5", "5", "5", "5"
            };
            #endregion

            string res = string.Empty;
            
            bool isㅈㄲ = false;
            
            HANGUL_INFO info2 = HangulJaso.DevideJaso(author);
            if (info2.isHangul == "NH") return info2.isHangul;
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info2.chars[0] == Jaum[a])
                {
                    res += Jaum_code[a];
                    if (res == "6" || res == "8") isㅈㄲ = true;
                    break;
                }
            }

            if (isㅈㄲ)
            {
                for (int a = 0; a < Moumㅈㄲ.Length; a++)
                {
                    if (info2.chars[1] == Moumㅈㄲ[a])
                    {
                        res += Moumㅈㄲ_code[a];
                        break;
                    }
                }
            }
            else
            {
                for (int a = 0; a < Moum.Length; a++)
                {
                    if (info2.chars[1] == Moum[a])
                    {
                        res += Moum_code[a];
                        break;
                    }
                }
            }
            return res;
        }
        
        private string Symbol_7_동서(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {'ㄱ',
                'ㄴ',
                'ㄷ',
                'ㄹ',
                'ㅁ',
                'ㅂ',
                'ㅅ',
                'ㅇ',
                'ㅈ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ',
                'ㄲ',
                'ㄸ',
                'ㅃ',
                'ㅆ',
                'ㅉ'
            };
            string[] Jaum_code = {
                "1",
                "19",
                "2",
                "29",
                "3",
                "4",
                "5",
                "6",
                "7",
                "76",
                "77",
                "78",
                "79",
                "8",
                "9",
                "96",
                "97",
                "98",
                "99"
            };

            char[] Moum = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ',
                'ㅕ', 'ㅖ',
                'ㅗ',
                'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'

            };
            string[] Moum_code = {
                "1",
                "2", "2", "2",
                "3", "3",
                "4", "4",
                "5",
                "6", "6", "6", "6",
                "7", "7", "7", "7", "7",
                "8", "8",
                "9"
            };

            char[] Moumㄱㄷ = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ',
                'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㄱㄷ_code = {
                "1",
                "2", "2", "2",
                "3", "3",
                "4", "4",
                "5", "5", "5", "5", "5",
                "6", "6", "6", "6", "6",
                "7", "7",
                "8"
            };

            char[] Moumㅈㄲ = {
                'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ', 'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㅈㄲ_code = {
                "1", "1", "1", "1",
                "2", "2", "2", "2", 
                "3", "3", "3", "3", "3",
                "4", "4", "4", "4", "4", "4", "4", 
                "5"
            };
            #endregion

            string res = string.Empty;

            bool[] isㄱㄷㅈㄲ = { false, false };

            HANGUL_INFO info2 = HangulJaso.DevideJaso(author);
            if (info2.isHangul == "NH") return info2.isHangul;
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info2.chars[0] == Jaum[a])
                {
                    res += Jaum_code[a];
                    if (res == "1" || res == "2") isㄱㄷㅈㄲ[0] = true;
                    else if (res == "6" || res == "8") isㄱㄷㅈㄲ[1] = true;
                    break;
                }
            }

            if (isㄱㄷㅈㄲ[0])
            {
                for (int a = 0; a < Moumㄱㄷ.Length; a++)
                {
                    if (info2.chars[1] == Moumㄱㄷ[a])
                    {
                        res += Moumㄱㄷ_code[a];
                        break;
                    }
                }
            }
            else if (isㄱㄷㅈㄲ[1])
            {
                for (int a = 0; a < Moumㅈㄲ.Length; a++)
                {
                    if (info2.chars[1] == Moumㅈㄲ[a])
                    {
                        res += Moumㅈㄲ_code[a];
                        break;
                    }
                }
            }
            else
            {
                for (int a = 0; a < Moum.Length; a++)
                {
                    if (info2.chars[1] == Moum[a])
                    {
                        res += Moum_code[a];
                        break;
                    }
                }
            }
            return res;
        }

        private string Symbol_8_동서(char author)
        {
            #region 자음 모음 표
            char[] Jaum = {
                'ㄱ',
                'ㄴ',
                'ㄷ',
                'ㄹ',
                'ㅁ',
                'ㅂ',
                'ㅅ',
                'ㅇ',
                'ㅈ',
                'ㅊ',
                'ㅋ',
                'ㅌ',
                'ㅍ',
                'ㅎ',
                'ㄲ',
                'ㄸ',
                'ㅃ',
                'ㅆ',
                'ㅉ'
            };
            string[] Jaum_code = {
                "1",
                "19",
                "2",
                "29",
                "3",
                "4",
                "5",
                "6",
                "7",
                "76",
                "77",
                "78",
                "79",
                "8",
                "9",
                "96",
                "97",
                "98",
                "99"
            };

            char[] Moum = {
                'ㅏ',
                'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ',
                'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ',
                'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moum_code = {
                "2",
                "3", "3", "3", 
                "4", "4", "4", "4", 
                "5", "5", "5", "5", "5", 
                "6", "6", "6", "6", "6", 
                "7", "7", 
                "8"
            };

            char[] Moumㅈㄲ = {
                'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ',
                'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ',
                'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ', 'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ', 'ㅡ', 'ㅢ',
                'ㅣ'
            };
            string[] Moumㅈㄲ_code = {
                "2", "2", "2", "2",
                "3", "3", "3", "3", 
                "4", "4", "4", "4", "4", "4", "4", "4", "4", "4", "4", "4", 
                "5"
            };
            #endregion

            string res = string.Empty;

            bool isㅈㄲ = false;

            HANGUL_INFO info2 = HangulJaso.DevideJaso(author);
            if (info2.isHangul == "NH") return info2.isHangul;
            for (int a = 0; a < Jaum.Length; a++)
            {
                if (info2.chars[0] == Jaum[a])
                {
                    res += Jaum_code[a];
                    if (res == "6" || res == "8") isㅈㄲ = true;
                    break;
                }
            }

            if (isㅈㄲ)
            {
                for (int a = 0; a < Moumㅈㄲ.Length; a++)
                {
                    if (info2.chars[1] == Moumㅈㄲ[a])
                    {
                        res += Moumㅈㄲ_code[a];
                        break;
                    }
                }
            }
            else
            {
                for (int a = 0; a < Moum.Length; a++)
                {
                    if (info2.chars[1] == Moum[a])
                    {
                        res += Moum_code[a];
                        break;
                    }
                }
            }
            return res;
        }

        private string Symbol_Jang(string author)
        {
            author = author.ToUpper();
            Helper_DB db = new Helper_DB();
            db.DBcon();
            string code = db.DB_Search_Author("Author_Jang_Cutter", "Code", "Author", author);
            
            return code;
        }

        private string Cutter(string author)
        {
            author = author.ToUpper();
            Helper_DB db = new Helper_DB();
            db.DBcon();
            string code = db.DB_Search_Author("Author_Jang_Cutter", "Code", "Target", author);

            return code;
        }

        private string Elord(string author)
        {
            author = Regex.Replace(author, @"[^a-zA-Z0-9가-힣_]", "", RegexOptions.Singleline);
            string result = author;

            if (author.Length > 3) {
                result = author.Substring(0, 3);
            }
            else if (author.Length <= 2) {
                result += ",";
            }

            return result;
        }
        
        private string NLK(string author)
        {
            HANGUL_INFO info = HangulJaso.DevideJaso(author[0]);
            string HNH = info.isHangul;

            bool isHangul;
            if (HNH == "NH") {
                author = author.ToUpper();
                isHangul = false;
            }
            else
                isHangul = true;
            Helper_DB db = new Helper_DB();
            db.DBcon();

            string Code;
            if (isHangul)
                Code = db.DB_Search_Author("Author_NLK", "Code", "Author", author);

            else
                Code = db.DB_Search_Author("Author_NLK", "Code", "Target", author);

            return Code;
        }
        /// <summary>
        /// 저자입력시 저자기호를 리턴하는 함수
        /// </summary>
        /// <param name="author">저자</param>
        /// <param name="book">도서명</param>
        /// <param name="Type">저자기호 타입</param>
        /// <param name="rb">저자첫음 / 서명첫음</param>
        /// <returns></returns>
        public string SymbolAuthor(string author, string book, string type, bool[] isType)
        {
            string resAuthor = Author_Fillter(author[0], isType[0]);
            string resBook = Book_Fillter(book, isType[1]);

            char aut;
            if (author.Length <= 1)
                aut = author[0];
            else
                aut = author[1];

            #region 콤보박스 선택
            string authorType = string.Empty;
            if (type == this.authorType[0])
                authorType = Symbol_1(aut);

            if (type == this.authorType[1])
                authorType = Symbol_2(aut);

            if (type == this.authorType[2])
                authorType = Symbol_3(aut);

            if (type == this.authorType[3])
                authorType = Symbol_4(aut);

            if (type == this.authorType[4])
                authorType = Symbol_5(aut);

            if (type == this.authorType[5])
                authorType = Symbol_6(aut);

            if (type == this.authorType[6])
                authorType = Symbol_7(aut);

            if (type == this.authorType[7])
                authorType = Symbol_7_동서(aut);

            if (type == this.authorType[8])
                authorType = Symbol_8(aut);

            if (type == this.authorType[9])
                authorType = Symbol_8_동서(aut);

            if (type == this.authorType[10])
                authorType = Symbol_Jang(author);

            if (type == this.authorType[11])
                authorType = Cutter(author);

            if (type == this.authorType[12])
                authorType = Elord(author);

            if (type == this.authorType[13])
                authorType = NLK(author);
            #endregion
            authorType = authorType.Replace("|", "");

            if (resAuthor == "")
                return "";

            string res = resAuthor + authorType + resBook;
            return res;
        }

        /// <summary>
        /// 저자 음절/초성 표시
        /// </summary>
        /// <param name="Author"></param>
        /// <param name="rb"></param>
        /// <returns></returns>
        private string Author_Fillter(char Author, bool rb)
        {
            if (rb)
                return Author.ToString();

            HANGUL_INFO info = HangulJaso.DevideJaso(Author);
            if (info.isHangul == "NH")
                return "";

            return info.chars[0].ToString();
        }

        /// <summary>
        /// 도서명 음절/초성 표시
        /// </summary>
        /// <param name="book"></param>
        /// <param name="rb"></param>
        /// <returns></returns>
        private string Book_Fillter(string book, bool rb)
        {
            if (book == "")
                return "";

            HANGUL_INFO info = HangulJaso.DevideJaso(book[0]);
            if (info.isHangul == "NH")
                return "";

            if (rb)
                return book[0].ToString();

            return info.chars[0].ToString();
        }

        private string Moum_Fillter(string Value, bool rb)
        {
            string result = Value;
            if (rb)
            {
                if (Value.Length == 3)
                {
                    result = Value.Substring(0, 2);
                }
            }
            return result;
        }
    }
    /// <summary>
    /// HangulJaso에서 한글자소에 대한 정보를 담음
    /// </summary>
    public struct HANGUL_INFO
    {
        /// <summary>
        /// 한글 여부(H, NH)
        /// </summary>
        public string isHangul;

        /// <summary>
        /// 분석 한글
        /// </summary>
        public char oriChar;

        /// <summary>
        /// 분리된 한글 (강 -> ㄱ ㅏ ㅇ)
        /// </summary>
        public char[] chars;
    }

    /// <summary>
    /// 한글 분석 클래스
    /// </summary>
    public sealed class HangulJaso
    {
        /// <summary>
        /// 초성 리스트
        /// </summary>
        public static readonly string HTable_ChoSung = "ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ";

        /// <summary>
        /// 중성 리스트
        /// </summary>
        public static readonly string HTable_JungSung = "ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ";

        /// <summary>
        /// 종성 리스트
        /// </summary>
        public static readonly string HTable_JongSung = "ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ";

        private static readonly ushort m_UniCodeHangulBase = 0xAC00;

        private static readonly ushort m_UniCodeHangulLast = 0xD79F;

        /// <summary>
        /// 생성자
        /// </summary>
        public HangulJaso() { }

        /// <summary>
        /// 초성, 중성, 종성으로 이루어진 한글을 한글자의 한글로 만듬
        /// </summary>
        /// <param name="choSung">초성</param>
        /// <param name="jungSung">중성</param>
        /// <param name="jongSung">종성</param>
        /// <returns>합쳐진 글자</returns>
        /// <remarks>
        /// <para>초성, 중성, 종성으로 이루어진 한글을 한글자의 한글로 만든다.</para><example>
        /// <code>
        /// string choSung = "ㄱ", jungSung = "ㅏ", jongSung = "ㅇ";
        /// char hangul = MergeJaso(choSung, jungSung, jongSung);
        /// /결과 -> 강</code></example>
        /// </remarks>
        public static char MergeJaso(string choSung, string jungSung, string jongSung)
        {
            int ChoSungPos, JungSungPos, JongSungPos;
            int nUniCode;

            ChoSungPos = HTable_ChoSung.IndexOf(choSung);       // 초성 위치
            JungSungPos = HTable_JungSung.IndexOf(jungSung);    // 중성 위치
            JongSungPos = HTable_JongSung.IndexOf(jongSung);    // 종성 위치

            // 앞서 만들어 낸 계산식
            nUniCode = m_UniCodeHangulBase + (ChoSungPos * 21 + JungSungPos) * 28 + JongSungPos;

            // 코드값을 문자로 변환
            char temp = Convert.ToChar(nUniCode);

            return temp;
        }

        /// <summary>
        /// 한글자의 한글을 초성, 중성, 종성으로 나눈다.
        /// </summary>
        /// <param name="hanChar">한글</param>
        /// <returns>분리된 한글에 대한 정보</returns>
        /// <seealso cref="HANGUL_INFO"/>
        /// <remarks><para>한글자의 한글을 초성, 중성, 종성으로 나눈다.</para>
        /// <example><code>
        /// HANGUL_INFO hinfo = DevideJaso('강');
        /// // hinfo.isHangul -> "H"(한글)
        /// // hinfo.oriChar -> 강
        /// // hinfo.chars[0] -> ㄱ, hinfo.chars[1] -> ㅏ, hinfo.chars[2] -> ㅇ</code></example></remarks>
        public static HANGUL_INFO DevideJaso(char hanChar)
        {
            int ChoSung, JungSung, JongSung;    // 초성, 중성, 종성의 인덱스
            ushort temp = 0x0000;               // 임시로 코드값을 담을 변수

            HANGUL_INFO hi = new HANGUL_INFO();

            // Char을 16비트 부호없는 정수형 형태로 변환 - Unicode
            temp = Convert.ToUInt16(hanChar);

            // 캐릭터가 한글이 아닐 경우 처리
            if ((temp < m_UniCodeHangulBase) || temp > m_UniCodeHangulLast)
            {
                hi.isHangul = "NH";
                hi.oriChar = hanChar;
                hi.chars = null;
            }
            else
            {
                // nUniCode에 한글코드에 대한 유니코드 위치를 담고 이를 이용해 인덱스 계산
                int nUniCode = temp - m_UniCodeHangulBase;

                ChoSung = nUniCode / (21 * 28);
                nUniCode = nUniCode % (21 * 28);

                JungSung = nUniCode / 28;
                nUniCode = nUniCode % 28;

                JongSung = nUniCode;

                hi.isHangul = "H";
                hi.oriChar = hanChar;
                hi.chars = new char[] { HTable_ChoSung[ChoSung], HTable_JungSung[JungSung], HTable_JongSung[JongSung] };
            }
            return hi;
        }
    }
}
