﻿
namespace UniMarc.마크
{
    partial class Marc_Plan_Sub_SelectList_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Edit_Save = new System.Windows.Forms.Button();
            this.btn_Edit_Close = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tb_listName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_authorType = new System.Windows.Forms.ComboBox();
            this.cb_divType = new System.Windows.Forms.ComboBox();
            this.cb_divNum = new System.Windows.Forms.ComboBox();
            this.cb_FirstBook = new System.Windows.Forms.ComboBox();
            this.cb_FirstAuthor = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Info;
            this.panel2.Controls.Add(this.btn_Edit_Save);
            this.panel2.Controls.Add(this.btn_Edit_Close);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(567, 123);
            this.panel2.TabIndex = 4;
            // 
            // btn_Edit_Save
            // 
            this.btn_Edit_Save.Location = new System.Drawing.Point(399, 90);
            this.btn_Edit_Save.Name = "btn_Edit_Save";
            this.btn_Edit_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Edit_Save.TabIndex = 4;
            this.btn_Edit_Save.Text = "저   장";
            this.btn_Edit_Save.UseVisualStyleBackColor = true;
            this.btn_Edit_Save.Click += new System.EventHandler(this.btn_Edit_Save_Click);
            // 
            // btn_Edit_Close
            // 
            this.btn_Edit_Close.Location = new System.Drawing.Point(480, 90);
            this.btn_Edit_Close.Name = "btn_Edit_Close";
            this.btn_Edit_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Edit_Close.TabIndex = 4;
            this.btn_Edit_Close.Text = "닫   기";
            this.btn_Edit_Close.UseVisualStyleBackColor = true;
            this.btn_Edit_Close.Click += new System.EventHandler(this.btn_Edit_Close_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Info;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.tb_listName);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.cb_authorType);
            this.panel4.Controls.Add(this.cb_divType);
            this.panel4.Controls.Add(this.cb_divNum);
            this.panel4.Controls.Add(this.cb_FirstBook);
            this.panel4.Controls.Add(this.cb_FirstAuthor);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Location = new System.Drawing.Point(12, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(543, 72);
            this.panel4.TabIndex = 3;
            // 
            // tb_listName
            // 
            this.tb_listName.Location = new System.Drawing.Point(52, 8);
            this.tb_listName.Name = "tb_listName";
            this.tb_listName.Size = new System.Drawing.Size(481, 21);
            this.tb_listName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "목록명";
            // 
            // cb_authorType
            // 
            this.cb_authorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_authorType.FormattingEnabled = true;
            this.cb_authorType.Location = new System.Drawing.Point(132, 37);
            this.cb_authorType.Name = "cb_authorType";
            this.cb_authorType.Size = new System.Drawing.Size(171, 20);
            this.cb_authorType.TabIndex = 1;
            // 
            // cb_divType
            // 
            this.cb_divType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_divType.FormattingEnabled = true;
            this.cb_divType.Location = new System.Drawing.Point(378, 37);
            this.cb_divType.Name = "cb_divType";
            this.cb_divType.Size = new System.Drawing.Size(88, 20);
            this.cb_divType.TabIndex = 1;
            this.cb_divType.SelectedIndexChanged += new System.EventHandler(this.cb_divType_SelectedIndexChanged);
            // 
            // cb_divNum
            // 
            this.cb_divNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_divNum.FormattingEnabled = true;
            this.cb_divNum.Location = new System.Drawing.Point(472, 37);
            this.cb_divNum.Name = "cb_divNum";
            this.cb_divNum.Size = new System.Drawing.Size(61, 20);
            this.cb_divNum.TabIndex = 1;
            // 
            // cb_FirstBook
            // 
            this.cb_FirstBook.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_FirstBook.FormattingEnabled = true;
            this.cb_FirstBook.Location = new System.Drawing.Point(309, 37);
            this.cb_FirstBook.Name = "cb_FirstBook";
            this.cb_FirstBook.Size = new System.Drawing.Size(61, 20);
            this.cb_FirstBook.TabIndex = 1;
            // 
            // cb_FirstAuthor
            // 
            this.cb_FirstAuthor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_FirstAuthor.FormattingEnabled = true;
            this.cb_FirstAuthor.Location = new System.Drawing.Point(64, 37);
            this.cb_FirstAuthor.Name = "cb_FirstAuthor";
            this.cb_FirstAuthor.Size = new System.Drawing.Size(61, 20);
            this.cb_FirstAuthor.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "저자기호";
            // 
            // Marc_Plan_Sub_SelectList_Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 123);
            this.Controls.Add(this.panel2);
            this.Name = "Marc_Plan_Sub_SelectList_Edit";
            this.Text = "마크정리_목록수정";
            this.Load += new System.EventHandler(this.Marc_Plan_Sub_SelectList_Edit_Load);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Edit_Save;
        private System.Windows.Forms.Button btn_Edit_Close;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tb_listName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_authorType;
        private System.Windows.Forms.ComboBox cb_divType;
        private System.Windows.Forms.ComboBox cb_divNum;
        private System.Windows.Forms.ComboBox cb_FirstBook;
        private System.Windows.Forms.ComboBox cb_FirstAuthor;
        private System.Windows.Forms.Label label5;
    }
}