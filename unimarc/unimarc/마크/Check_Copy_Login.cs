﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniMarc.마크
{
    public partial class Check_Copy_Login : Form
    {
        Check_Copy_Sub_Search ccss;
        public Check_Copy_Login(Check_Copy_Sub_Search _ccss)
        {
            InitializeComponent();
            ccss = _ccss;
        }

        private void Check_Copy_Login_Load(object sender, EventArgs e)
        {

        }

        private void btn_Apply_Click(object sender, EventArgs e)
        {
            string id = tb_ID.Text;
            string pw = tb_PW.Text;

            if (id == "" || pw == "")
            {
                MessageBox.Show("입력된 값이 없습니다.");
                return;
            }

            ccss.SetLogin(id, pw);

            this.Close();
        }
    }
}
