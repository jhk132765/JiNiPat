﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniMarc.마크
{
    public partial class Help_007 : Form
    {
        CD_LP cp;
        TextBox ClickBox = null;
        public bool isMusic { get; set; }
        public string text007 { get; set; }

        public Help_007(CD_LP cp)
        {
            InitializeComponent();
            this.cp = cp;
        }

        private void Help_007_Load(object sender, EventArgs e)
        {
            lbl_Text.Text = text007;

            TextBox[] AllBox;

            // 작성하려는 마크 타입에 따라 폼 구성이 달라짐.
            if (isMusic) // 음반
            {
                this.Text = "녹음자료(MU) - 007";
                Panel음반.Visible = true;
                PanelDVD.Visible = false;

                AllBox = new TextBox[] {
                    tb_04, tb_05, tb_06, tb_07, tb_08,
                    tb_09, tb_10, tb_11, tb_12, tb_13,
                    tb_14
                };
                tb_02.Text = lbl_Text.Text[1].ToString();

            }
            else // DVD
            {
                this.Text = "시청각자료(VM) - 007";
                Panel음반.Visible = false;
                PanelDVD.Visible = true;
                panel3.Height = PanelDVD.Height;
                this.Height = 326;

                AllBox = new TextBox[] {
                    tb_DVD04, tb_DVD05, tb_DVD06, tb_DVD07, tb_DVD08, tb_DVD09
                };
                tb_DVD02.Text = lbl_Text.Text[1].ToString();

            }
            int count = 3;
            foreach (TextBox t in AllBox)
            {
                try
                {
                    t.Text = lbl_Text.Text[count].ToString();
                }
                catch
                {
                    t.Text = "";
                }
                count++;
            }
        }

        private void TextBox_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            TextBox textBox = (TextBox)sender;
            ClickBox = textBox;
            TextBox[] AllBox = {
                tb_02, tb_04, tb_05, tb_06, tb_07,
                tb_07, tb_08, tb_09, tb_10, tb_11,
                tb_12, tb_13, tb_14
            };
            TextBox[] DVDBox =  {
                tb_DVD02, tb_DVD04, tb_DVD05, tb_DVD06, tb_DVD07, tb_DVD08, tb_DVD09
            };

            TextBox[] Target;
            if (isMusic)
                Target = AllBox;
            else
                Target = DVDBox;
            int idx = -1;
            int count = 0;
            foreach (TextBox t in Target)
            {
                t.BackColor = Color.WhiteSmoke;

                if (t == textBox)
                    idx = count;

                count++;
            }
            textBox.BackColor = Color.Bisque;

            if (isMusic)
                MakeGridMusic(idx);
            else
                MakeGridDVD(idx);
        }

        void MakeGridMusic(int BoxIndex)
        {
            string[,] Grid;
            switch (BoxIndex)
            {
                case 0:     // tb_02
                    Grid = new string[,] { 
                        { "d", "음반" },
                        { "e", "실린더" },
                        { "g", "음향 카트리지" },
                        { "i", "음향-트랙 필름" },
                        { "q", "롤" },
                        { "s", "음향 카세트" },
                        { "t", "음향-테이프 릴" },
                        { "u", "자료를 특정화하지 않음" },
                        { "w", "전선녹음" },
                        { "z", "기타" }
                    };
                    break;
                case 1:     // tb_04
                    Grid = new string[,] {
                        { "a", "16 rpm" },
                        { "b", "33 1/3 rpm" },
                        { "c", "45 rpm" },
                        { "d", "78 rpm" },
                        { "e", "8 rpm" },
                        { "f", "1.4m. per sec." },
                        { "h", "120 rpm" },
                        { "i", "160 rpm" },
                        { "k", "15/16 ips" },
                        { "l", "1 7/8 ips" },
                        { "m", "3 3/4 ips" },
                        { "o", "7 1/2 ips" },
                        { "p", "15 ips" },
                        { "r", "30 ips" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 2:     // tb_05
                    Grid = new string[,] {
                        { "m", "단음(Monaural)" },
                        { "q", "4채널음" },
                        { "s", "스테레오" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 3:     // tb_06
                    Grid = new string[,] {
                        { "m", "미세형" },
                        { "n", "적용불가" },
                        { "s", "표준형" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 4:     // tb_07
                    Grid = new string[,] {
                        { "a", "3 in." },
                        { "b", "5 in." },
                        { "c", "7 in." },
                        { "d", "10 in." },
                        { "e", "12 in." },
                        { "f", "16 in." },
                        { "g", "4 3/4 in." },
                        { "j", "3 7/8 x 2 1/2 in." },
                        { "n", "적용불가" },
                        { "o", "5 1/4 x 3 7/8 in." },
                        { "s", "2 3/4 x 4 in." },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 5:     // tb_08
                    Grid = new string[,] {
                        { "l", "1/8 in." },
                        { "m", "1/4 in." },
                        { "n", "적용불가" },
                        { "o", "1/2 in." },
                        { "p", "1 in." },
                        { "u", "미상" }
                    };
                    break;
                case 6:     // tb_09
                    Grid = new string[,] {
                        { "a", "1 트랙 (Full)" },
                        { "b", "2 트랙 (Half)" },
                        { "c", "4 트랙 (표준 카세트)" },
                        { "d", "8 트랙 (상용 카트리지)" },
                        { "e", "12 트랙" },
                        { "f", "16 트랙" },
                        { "n", "적용불가" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 7:     // tb_10
                    Grid = new string[,] {
                        { "a", "마스터 테이프" },
                        { "b", "테이프 복제 마스터" },
                        { "d", "음반 마스터 (음각)" },
                        { "i", "현장 녹음" },
                        { "m", "상업용 제작" },
                        { "n", "적용불가" },
                        { "r", "Mother (양각)" },
                        { "s", "Stamper (음각)" },
                        { "t", "테스트 제작" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 8:     // tb_11
                    Grid = new string[,] {
                        { "a", "칠 (라카)" },
                        { "l", "금속" },
                        { "m", "금속 및 플라스틱" },
                        { "n", "적용불가" },
                        { "p", "플라스틱" },
                        { "s", "도료 (Shellac)" },
                        { "w", "왁스 (Wax)" },
                        { "u", "미상" }
                    };
                    break;
                case 9:     // tb_12
                    Grid = new string[,] {
                        { "h", "삼각골형 깎임" },
                        { "l", "측면 또는 측면수직형 깎임" },
                        { "n", "적용불가" },
                        { "u", "미상" }
                    };
                    break;
                case 10:    // tb_13
                    Grid = new string[,] {
                        { "a", "미국방송가협회 (NAB) 표준" },
                        { "b", "CCIR 표준" },
                        { "c", "돌비 B (표준돌비)" },
                        { "d", "dbx (잡음감소)" },
                        { "e", "디지털녹음 (컴팩트디스크)" },
                        { "f", "돌비 A" },
                        { "g", "돌비 C" },
                        { "h", "CX" },
                        { "n", "적용불가" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 11:    // tb_14
                    Grid = new string[,] {
                        { "a", "직접 녹음 (흡음판 사용)" },
                        { "b", "직접 녹음 (흡음판 미사용)" },
                        { "d", "디지털 녹음" },
                        { "e", "아날로그 전기적 녹음" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                default:
                    return;
            }
            for (int a = 0; a < Grid.GetLength(0); a++)
            {
                string[] Input = { Grid[a, 0], Grid[a, 1] };
                dataGridView1.Rows.Add(Input);
            }
        }

        void MakeGridDVD(int BoxIndex)
        {
            string[,] Grid;
            switch (BoxIndex)
            {
                case 0:     // tb_DVD02
                    Grid = new string[,] {
                        { "c", "비디오 카트리지" },
                        { "d", "비디오 디스크" },
                        { "f", "비디오 카세트" },
                        { "r", "비디오 릴" },
                        { "u", "자료를 특정화하지 않음" },
                        { "z", "기타" }
                    };
                    break;
                case 1:     // tb_DVD04
                    Grid = new string[,] {
                        { "a", "단색" },
                        { "b", "흑색" },
                        { "c", "천연색" },
                        { "m", "혼합" },
                        { "n", "적용불가" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 2:     // tb_DVD05
                    Grid = new string[,] {
                        { "a", "베타 (1/2 in, 비디오카세트)" },
                        { "b", "VHS (1/2 in, 비디오카세트)" },
                        { "c", "U-matic (3/4 in, 비디오카세트)" },
                        { "d", "EIAJ (일본규격 1/2 in, 릴)" },
                        { "e", "Type C (1 in, 릴)" },
                        { "f", "Quadruplex (2 in, 릴)" },
                        { "g", "레이저 광 비디오디스크" },
                        { "h", "CED 비디오디스크" },
                        { "i", "베타캠 (1/2 in, 비디오카세트)" },
                        { "j", "베타캠 SP (1/2 in, 비디오카세트)" },
                        { "k", "슈퍼 VHS (1/2 in, 비디오카세트)" },
                        { "m", "M-Ⅱ (1/2 in. 비디오카세트)" },
                        { "o", "D-2(3/4 in. 비디오카세트)" },
                        { "p", "8 mm" },
                        { "q", "Hi-8 mm" },
                        { "s", "블루레이 디스크" },
                        { "u", "미상" },
                        { "v", "DVD" },
                        { "z", "기타" }
                    };
                    break;
                case 3:     // tb_DVD06
                    Grid = new string[,] {
                        { " ", "무성" },
                        { "a", "매체에 음향이 수록되어 있음" },
                        { "b", "매체와 별도로 음향이 수록되어 있음" },
                        { "u", "미상" }
                    };
                    break;
                case 4:     // tb_DVD07
                    Grid = new string[,] {
                        { " ", "무성" },
                        { "a", "영화필름상의 광학녹음대" },
                        { "b", "영화필름상의 자기녹음대" },
                        { "c", "카트리지상의 자기음향녹음대" },
                        { "d", "음반" },
                        { "e", "릴상의 자기음향테이프" },
                        { "f", "카세트상의 자기음향테이프" },
                        { "g", "영화필름상의 광학 및 자기녹음대" },
                        { "h", "비디오테이프" },
                        { "i", "비디오디스크" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                case 5:     // tb_DVD08
                    Grid = new string[,] {
                        { "a", "8 mm" },
                        { "m", "1/4 in." },
                        { "o", "1/2 in." },
                        { "p", "1 in." },
                        { "q", "2 in." },
                        { "r", "3/4 in." },
                        { "u", "미상" },
                        { "z", "기타 (비디오디스크에 해당)" }
                    };
                    break;
                case 6:     // tb_DVD09
                    Grid = new string[,] {
                        { "k", "혼합" },
                        { "m", "단음 (모노럴)" },
                        { "n", "적용불가 (무성)" },
                        { "q", "4채널, 다채널, 서라운드" },
                        { "s", "스테레오" },
                        { "u", "미상" },
                        { "z", "기타" }
                    };
                    break;
                default:
                    return;
            }
            for (int a = 0; a < Grid.GetLength(0); a++)
            {
                string[] Input = { Grid[a, 0], Grid[a, 1] };
                dataGridView1.Rows.Add(Input);
            }
        }

        private void Btn_Apply_Click(object sender, EventArgs e)
        {
            cp.tb_T007.Text = lbl_Text.Text;
        }

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ClickBox == null || e.RowIndex < 0)
                return;

            ClickBox.Text = dataGridView1.Rows[e.RowIndex].Cells["value"].Value.ToString();

            string[] Make007;
            if (isMusic)
                Make007 = new string[] {
                    tb_01.Text, tb_02.Text, " ", tb_04.Text, tb_05.Text,
                    tb_06.Text, tb_07.Text, tb_08.Text, tb_09.Text, tb_10.Text,
                    tb_11.Text, tb_12.Text, tb_13.Text, tb_14.Text
                };
            else
                Make007 = new string[] {
                    tb_DVD01.Text, tb_DVD02.Text, " ", tb_DVD04.Text, tb_DVD05.Text, tb_DVD06.Text, tb_DVD07.Text, tb_DVD08.Text, tb_DVD09.Text
                };

            lbl_Text.Text = string.Join("", Make007);
        }
    }
}
