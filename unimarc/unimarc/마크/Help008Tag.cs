﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelTest
{
    class Help008Tag
    {
        /// <summary>
        /// 이용자 9개
        /// </summary>
        public string[] combo1 = { 
            "일반", "유아", "아동", "초등", "중학생",
            "고등학생", "성인용 19금", "특수계층", "미상"
        };

        /// <summary>
        /// 자료형식 3개
        /// </summary>
        public string[] combo2 = { 
            "해당무", "큰 글자", "점자"
        };

        /// <summary>
        /// 내용형식 28개
        /// </summary>
        public string[] combo3 = { 
            "해당무", "만화/그래픽노블", "사전", "백과사전", "평론",
            "통계자료", "명감", "연감", "족보", "조약", "협정문",
            "학위논문", "법률논문", "법령집", "판례집 및 판례요약집", "판결보도 및 판결 평석",
            "별쇄본", "역서(전체)", "음반목록", "영화작품목록", "초록",
            "서지", "목록", "편람", "색인", "조사보고",
            "기술보고서", "프로그램화된 텍스트", "표준/표준해설자료"
        };

        /// <summary>
        /// 문학형식 16개
        /// </summary>
        public string[] combo4 = {
            "해당무", "소설", "추리소설", "단편소설", "시",
            "수필", "희곡/시나리오", "문집", "기행문/일기문/수기", "평론",
            "논픽션", "연설문", "논설문", "향가/시조", "풍자문학",
            "서간문학"
        };

        /// <summary>
        /// 전기 5개
        /// </summary>
        public string[] combo5 = {
            "해당무", "자서전", "개인전기서", "전기물의 합저서", "전기적 정보가 포함된 자료"
        };

        /// <summary>
        /// 언어 36개
        /// </summary>
        public string[] combo6 = {
            "한국어", "영어", "일본어", "중국어", "독일어",
            "프랑스어", "러시아어", "스페인어", "이탈리아어", "네덜란드어",
            "핀란드어", "스웨덴어", "포르투갈어", "노르웨이어", "그리스어",
            "체코어", "폴란드어", "다국어", "말레이시아어", "몽골어",
            "미얀마(버마어)", "베트남어", "슬로베니아어", "아랍어", "아프리카어",
            "에스토니아어", "우즈베키스탄어", "우크라이나어", "인도(마라티어)", "캄보디아어",
            "태국어", "터키어", "투르크메니스탄어", "티베르어", "필리핀(타갈로그어)",
            "헝가리어"
        };

        public string Combo_Change(string Text, string Name, int idx)
        {
            char[] textArray = Text.ToCharArray();

            // comboBox1 = 이용대상자수준
            string[] combo1_res = {
                " ", "a", "j", "b", "c",
                "d", "e", "f", "z"
            };

            // comboBox2 = 개별자료형태
            string[] combo2_res = {
                " ", "d", "f"
            };

            // comboBox3 = 내용형식1
            // comboBox7 = 내용형식2
            string[] combo3_res = {
                " ", "6", "d", "e", "o",
                "s", "r", "y", "j", "z",
                "m", "g", "l", "v", "w",
                "2", "5", "k", "q", "a",
                "b", "c", "f", "i", "n",
                "t", "p", "u"
            };

            // comboBox4 = 문학형식
            string[] combo4_res = {
                " ", "f", "k", "j", "p",
                "e", "d", "v", "m", "u",
                "l", "s", "t", "w", "h",
                "i"
            };

            // comboBox5 = 전기
            string[] combo5_res = {
                " ", "a", "b", "c", "d"
            };

            // comboBox6 = 언어
            string[] combo6_res = {
                "kor", "eng", "jpn", "chi", "ger",
                "fre", "rus", "spa", "ita", "dut",
                "fin", "swe", "por", "nor", "grc",
                "cze", "pol", "mul", "may", "mon",
                "bur", "vie", "slv", "ara", "afr",
                "est", "uzb", "ukr", "mar", "cam",
                "tha", "tur", "tuk", "tib", "tag",
                "hun"
            };

            char[] inputArray;

            switch (Name)
            {
                case "comboBox1":       // 이용대상자수준
                    inputArray = combo1_res[idx].ToCharArray();
                    textArray[22] = inputArray[0];
                    break;
                case "comboBox2":       // 개별자료형태
                    inputArray = combo2_res[idx].ToCharArray();
                    textArray[23] = inputArray[0];
                    break;
                case "comboBox3":       // 내용형식1
                    inputArray = combo3_res[idx].ToCharArray();
                    textArray[24] = inputArray[0];
                    break;
                case "comboBox7":       // 내용형식2
                    inputArray = combo3_res[idx].ToCharArray();
                    textArray[25] = inputArray[0];
                    break;
                case "comboBox4":       // 문학형식
                    inputArray = combo4_res[idx].ToCharArray();
                    textArray[33] = inputArray[0];
                    break;
                case "comboBox5":       // 전기
                    inputArray = combo5_res[idx].ToCharArray();
                    textArray[34] = inputArray[0];
                    break;
                case "comboBox6":       // 언어
                    inputArray = combo6_res[idx].ToCharArray();
                    textArray[35] = inputArray[0];
                    textArray[36] = inputArray[1];
                    textArray[37] = inputArray[2];
                    break;
            }
            Text = new string(textArray);

            return Text;
        }
        /// <summary>
        /// 008 이용대상자수준 (22)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int User_008(string value)
        {
            string[] combo1_res = { " ", "a", "j", "b", "c",
                                    "d", "e", "f", "z" };
            int result;
            for(result = 0; result < combo1_res.Length; result++)
            {
                if (value == combo1_res[result]) { break; }
            }
            return result;
        }
        /// <summary>
        /// 008 개별자료형태 (23)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int DataType_008(string value)
        {
            string[] combo2_res = { " ", "d", "f" };
            int result;
            for (result = 0; result < combo2_res.Length; result++)
            {
                if (value == combo2_res[result]) { break; }
            }
            return result;
        }
        /// <summary>
        /// 008 내용형식 (24 / 25) 두번 불러야함.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int Format_008(string value)
        {
            string[] combo3_res = { " ", "6", "d", "e", "o",
                                    "s", "r", "y", "j", "z",
                                    "m", "g", "l", "v", "w",
                                    "2", "5", "k", "q", "a",
                                    "b", "c", "f", "i", "n",
                                    "t", "p", "u" };
            int result;
            for (result = 0; result < combo3_res.Length; result++)
            {
                if (value == combo3_res[result]) { break; }
            }
            return result;
        }
        /// <summary>
        /// 008 문학형식 (33)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int Literary_008(string value)
        {
            string[] combo4_res = { " ", "f", "k", "j", "p",
                                    "e", "d", "v", "m", "u",
                                    "l", "s", "t", "w", "h",
                                    "i" };
            int result;
            for (result = 0; result < combo4_res.Length; result++)
            {
                if (value == combo4_res[result]) { break; }
            }
            return result;
        }
        /// <summary>
        /// 008 전기 (34)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int Biography_008(string value)
        {
            string[] combo5_res = { " ", "a", "b", "c", "d" };
            int result;
            for (result = 0; result < combo5_res.Length; result++)
            {
                if (value == combo5_res[result]) { break; }
            }
            return result;
        }
        /// <summary>
        /// 008 언어 (35-37)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int Language_008(string value)
        {
            string[] combo6_res = { "kor", "eng", "jpn", "chi", "ger",
                                    "fre", "rus", "spa", "ita", "dut",
                                    "fin", "swe", "por", "nor", "grc",
                                    "cze", "pol", "mul", "may", "mon",
                                    "bur", "vie", "slv", "ara", "afr",
                                    "est", "uzb", "ukr", "mar", "cam",
                                    "tha", "tur", "tuk", "tib", "tag",
                                    "hun" };
            int result;
            for (result = 0; result < combo6_res.Length; result++)
            {
                if (value == combo6_res[result]) { break; }
            }
            return result;
        }
        public bool CheckBox_008(string value)
        {
            bool result = false;
            if (value == "1") { result = true; }
            return result;
        }
        /// <summary>
        /// 나라 코드 입력
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public string Country_008(string strValue)
        {
            string ulk = "서울", bnk = "부산", tgk = "대구", ick = "인천", kjk = "광주", tjk = "대전", usk = "울산", sjk = "세종";
            string[] ggk = { "경기도", "경기", "수원", "성남", "의정부", "안양", "부천", "광명", "평택", "송탄", "동두천", "안산", "고양", "과천", "구리", "미금", "남양주", "오산", "시흥", "군포", "의왕", "하남", "용인", "파주", "이천", "안성", "김포", "양주", "여주", "화성", "연천", "포천", "가평", "양평", "강화", "옹진" };
            string[] gak = { "강원도", "강원", "춘천", "원주", "강릉", "동해", "태백", "속초", "삼척", "양양", "철원", "화천", "양구", "인제", "홍천", "횡성", "평창", "영월", "정선" };
            string[] hbk = { "충청북도", "충북", "충주", "제천", "단양", "음성", "진천", "괴산", "증평", "청주", "청원", "보은", "영동", "옥천" };
            string[] hck = { "충청남도", "충남", "천안", "아산", "예산", "홍성", "청양", "당진", "서산", "태안", "보령", "서천" };
            string[] gbk = { "경상북도", "경북", "울진", "봉화", "영주", "영양", "영덕", "안동", "문경", "예천", "의성", "청송", "울릉", "독도", "김천", "상주", "구미", "성주", "칠곡", "군위", "고령", "청도", "경산", "포항", "경주", "영천" };
            string[] gnk = { "경상남도", "경남", "거창", "함양", "합천", "창녕", "의령", "산청", "하동", "진주", "사천", "함안", "고성", "남해", "통영", "창원", "마산", "진해", "거제", "김해", "양산", "밀양" };
            string[] jbk = { "전라북도", "전북", "전주", "무주", "완주", "진안", "익산", "장수", "군산", "김제", "부안", "고창", "정읍", "임실", "순창", "남원" };
            string[] jnk = { "전라남도", "전남", "담양", "장성", "영광", "함평", "나주", "순천", "광양", "구례", "곡성", "화순", "여수", "보성", "고흥", "목포", "무안", "신안", "영암", "장흥", "강진", "해남", "진도", "완도" };
            string[] jjk = { "제주도", "제주", "서귀포" };
            string[] us = { "new york", "boston", "los angeles", "san francisco", "washington", "san diego" };
            string[] uk = { "london", "oxford", "manchester", "chesterfield", "lincoln", "hong kong", "glasgow", "edinburgh", "leeds", "liverpool", "sheffield", "birmingham" };
            string result = "";

            strValue = strValue.ToLower();
            if (strValue.Contains(ulk)) { result = "ulk"; } // 서울
            if (strValue.Contains(bnk)) { result = "bnk"; } // 부산
            if (strValue.Contains(tgk)) { result = "tgk"; } // 대구
            if (strValue.Contains(ick)) { result = "ick"; } // 인천
            if (strValue.Contains(kjk)) { result = "kjk"; } // 광주
            if (strValue.Contains(tjk)) { result = "tjk"; } // 대전
            if (strValue.Contains(usk)) { result = "usk"; } // 울산
            if (strValue.Contains(sjk)) { result = "sjk"; } // 세종
            for (int a = 0; a < ggk.Length; a++) { if (strValue.Contains(ggk[a])) { result = "ggk"; } }  // 경기
            for (int a = 0; a < gak.Length; a++) { if (strValue.Contains(gak[a])) { result = "gak"; } }  // 강원
            for (int a = 0; a < hbk.Length; a++) { if (strValue.Contains(hbk[a])) { result = "hbk"; } }  // 충북
            for (int a = 0; a < hck.Length; a++) { if (strValue.Contains(hck[a])) { result = "hck"; } }  // 충남
            for (int a = 0; a < gbk.Length; a++) { if (strValue.Contains(gbk[a])) { result = "gbk"; } }  // 경북
            for (int a = 0; a < gnk.Length; a++) { if (strValue.Contains(gnk[a])) { result = "gnk"; } }  // 경남
            for (int a = 0; a < jbk.Length; a++) { if (strValue.Contains(jbk[a])) { result = "jbk"; } }  // 전북
            for (int a = 0; a < jnk.Length; a++) { if (strValue.Contains(jnk[a])) { result = "jnk"; } }  // 전남
            for (int a = 0; a < jjk.Length; a++) { if (strValue.Contains(jjk[a])) { result = "jjk"; } }  // 제주
            for (int a = 0; a < us.Length; a++) { if (strValue.Contains(us[a])) { result = "us"; } }  // 미국
            for (int a = 0; a < uk.Length; a++) { if (strValue.Contains(uk[a])) { result = "uk"; } }  // 영국

            return result;
        }

        public string Picture_008(string strValue)
        {
            string result = "";
            string[] dpfk = { "삽화", "지도", "초상", "표", "설계",
                              "도판", "악보", "영인물", "문장", "계보",
                              "서식", "양식", "견본", "녹음", "사진",
                              "채색장식", 
                              "ill", "map", "photo" };

            string[] tlqkf = { "a", "b", "c", "d", "e",
                               "f", "g", "h", "i", "j",
                               "k", "k", "l", "m", "o",
                               "p",
                               "a", "b", "o" };
             
            // "col.ill.", "col. ill.", "ill."
            strValue = strValue.ToLower();
            string[] str1 = strValue.Split(',');
            for(int a = 0; a < str1.Length; a++)
            {
                for (int b = 0; b < dpfk.Length; b++)
                {
                    if(str1[a].Contains(dpfk[b])) { result += tlqkf[b]; }
                }
            }
            if (result.Length > 4)
            {
                result = result.Substring(0, 4);
            }
            else
            {
                for(int a = 0; a < 4; a++)
                {
                    if (result.Length < 4)
                        result += " ";
                    else
                        break;
                }
            }

            return result;
        }
    }
}