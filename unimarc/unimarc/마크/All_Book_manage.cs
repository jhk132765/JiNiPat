﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UniMarc.마크;

namespace WindowsFormsApp1.Mac
{
    public partial class All_Book_manage : Form
    {
        public string compidx;
        public string charge;
        Helper_DB db = new Helper_DB();
        Main main;
        public All_Book_manage(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
            charge = main.User;
        }

        private void All_Book_manage_Load(object sender, EventArgs e)
        {
            db.DBcon();
        }

        public void btn_Search_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string text = textBox1.Text;
            // 세트명 세트ISBN 세트수량 세트정가 저자 출판사 추가자
            string Area_db = "`set_name`, `set_isbn`, `set_count`, `set_price`, `author`, `book_comp`, `charge`";
            string cmd = db.DB_Contains("Set_Book", compidx, "set_name", text, Area_db);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] db_data = db_res.Split('|');
            input_Grid(db_data);
        }
        #region Search_Sub
        private void input_Grid(string[] db_data)
        {
            string[] grid = { "", "", "", "", "", 
                              "", "", "" };

            for (int a = 0; a < db_data.Length; a++)
            {
                switch (a % 7)
                {
                    case 0:
                        grid[0] = db_data[a];
                        break;
                    case 1:
                        grid[1] = db_data[a];
                        break;
                    case 2:
                        grid[2] = db_data[a];
                        break;
                    case 3:
                        grid[3] = db_data[a];
                        break;
                    case 4:
                        grid[4] = db_data[a];
                        break;
                    case 5:
                        grid[5] = db_data[a];
                        break;
                    case 6:
                        grid[6] = db_data[a];
                        if (input_Grid_Sub(grid[0])) {
                            dataGridView1.Rows.Add(grid);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        private bool input_Grid_Sub(string value)
        {
            if (dataGridView1.Rows.Count < 1)
            {
                return true;
            }
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (dataGridView1.Rows[a].Cells["set_name"].Value.ToString() == value) {
                    return false;
                }
            }
            return true;
        }
        #endregion

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_Search_Click(null, null);
            }
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            // 새로운 폼 작업이 필요함.
            All_Book_manage_Add add = new All_Book_manage_Add(this);
            add.Show();
        }

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            int V_idx = Edit_Delete_Sub();
            if (V_idx == -1)
            {
                return;
            }

            string[] edit_data = {
                dataGridView1.Rows[V_idx].Cells["set_name"].Value.ToString(),
                dataGridView1.Rows[V_idx].Cells["set_isbn"].Value.ToString(),
                dataGridView1.Rows[V_idx].Cells["set_count"].Value.ToString(),
                dataGridView1.Rows[V_idx].Cells["set_price"].Value.ToString()
            };
            All_Book_manage_Edit edit = new All_Book_manage_Edit(this);
            edit.Show();
            edit.set_old(edit_data);
        }

        /// <summary>
        /// row에 체크가 된 idx값을 가져오기 위한 함수. (2개 이상 체크시 리턴됨)
        /// </summary>
        /// <returns>체크된 row의 idx값</returns>
        private int Edit_Delete_Sub()
        {
            // 목록에 값이 없을 경우 리턴
            if (dataGridView1.Rows.Count < 1)
            {
                return -1;
            }
            int V_idx = -1;

            // 체크표시된 인덱스 값 확인 및 추출 (2개이상일 경우 오류표출하고 리턴)
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (V_idx != -1 && dataGridView1.Rows[a].Cells["chk_V"].Value.ToString() == "V")
                {
                    MessageBox.Show("체크사항이 1개인지 확인해주세요.");
                    return -1;
                }
                if (dataGridView1.Rows[a].Cells["chk_V"].Value.ToString() == "V")
                {
                    V_idx = a;
                }
            }
            return V_idx;
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            int V_idx = Edit_Delete_Sub();
            if (V_idx == -1)
            {
                return;
            }

            if (MessageBox.Show("삭제하시겠습니까?", "삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string[] delete_area = { "set_name", "set_isbn", "set_count", "set_price" };
                string[] delete_data = {
                    dataGridView1.Rows[V_idx].Cells["set_name"].Value.ToString(),
                    dataGridView1.Rows[V_idx].Cells["set_isbn"].Value.ToString(),
                    dataGridView1.Rows[V_idx].Cells["set_count"].Value.ToString(),
                    dataGridView1.Rows[V_idx].Cells["set_price"].Value.ToString() };

                string cmd = db.DB_Delete_No_Limit("Set_Book", "compidx", compidx, delete_area, delete_data);
                db.DB_Send_CMD_reVoid(cmd);
            }

            btn_Search_Click(null, null);
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int col = e.ColumnIndex;
            int row = e.RowIndex;

            if (dataGridView1.Columns["chk_V"].Index == col)
            {
                if (dataGridView1.Rows[row].Cells["chk_V"].Value.ToString() == "" ||
                    dataGridView1.Rows[row].Cells["chk_V"].Value == null)
                {
                    dataGridView1.Rows[row].Cells["chk_V"].Value = "V";
                }
                else if (dataGridView1.Rows[row].Cells["chk_V"].Value.ToString() == "V")
                {
                    dataGridView1.Rows[row].Cells["chk_V"].Value = "";
                }
            }
            else
            {
                All_Book_Detail detail = new All_Book_Detail(this);
                detail.row = row;
                detail.col = col;
                detail.data[0] = dataGridView1.Rows[row].Cells["set_name"].Value.ToString();
                detail.data[1] = dataGridView1.Rows[row].Cells["set_isbn"].Value.ToString();
                detail.data[2] = dataGridView1.Rows[row].Cells["set_count"].Value.ToString();
                detail.data[3] = dataGridView1.Rows[row].Cells["set_price"].Value.ToString();
                detail.Show();
            }
        }
    }
}
