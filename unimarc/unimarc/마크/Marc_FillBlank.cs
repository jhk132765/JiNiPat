﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace UniMarc
{
    public partial class Marc_FillBlank : Form
    {
        public string ISBN;
        bool isAll;
        bool isBreak;
        ExcelTest.Marc marc;

        public Marc_FillBlank(ExcelTest.Marc _marc)
        {
            InitializeComponent();
            marc = _marc;
        }

        private void Marc_FillBlank_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate(MakeURL(ISBN));
        }

        /// <summary>
        /// 입력된 ISBN으로 코리스URL을 생성함.
        /// </summary>
        /// <param name="ISBN"></param>
        /// <returns></returns>
        string MakeURL(string ISBN)
        {
            return string.Format(
                "https://nl.go.kr/kolisnet/search/searchResultAllList.do?" +
                "tab=ALL&" +
                "historyYn=Y&" +
                "keywordType1=total&" +
                "keyword1={0}&" +
                "bookFilter=BKGM&" +
                "bookFilter=YON&" +
                "bookFilter=BKDM&" +
                "bookFilter=NK&" +
                "bookFilter=NP&" +
                "bookFilter=OT", ISBN);
        }

        /// <summary>
        /// 옆 그리드에 채울 정보를 입력
        /// </summary>
        /// <param name="GridData">[0]idx [1]ISBN [2]도서명 [3]저자 [4]출판사 [5]가격</param>
        public void InitFillBlank(string[] GridData)
        {
            dataGridView1.Rows.Add(GridData);
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            tb_URL.Text = webBrowser1.Url.AbsoluteUri;
            
            if (isAll)
            {
                if (tb_URL.Text.Contains("searchResultAllList"))
                    InitCrowling();

                if (tb_URL.Text.Contains("searchResultEditonList"))
                    SearchResultEditonList();

                if (tb_URL.Text.Contains("searchResultDetail"))
                    SearchResultDetail();

                if (tb_URL.Text.Contains("searchResultMarc"))
                    SearchResultMarc();
            }
        }
        #region Completed SUB

        /// <summary>
        /// 코리스에서 ISBN검색이 된 목록 중 맨 첫번째만 클릭해주는 함수.
        /// </summary>
        private void InitCrowling()
        {
            string resultCount = "";

            HtmlElementCollection span = webBrowser1.Document.GetElementsByTagName("span");
            foreach (HtmlElement Search in span)
            {
                if (Search.GetAttribute("className") == "result_count")
                {
                    resultCount = Search.InnerText;
                    break;
                }
            }

            if (resultCount.Contains("0"))
            {
                isBreak = true;
                return;
            }

            HtmlElementCollection div = webBrowser1.Document.GetElementsByTagName("div");
            foreach (HtmlElement SearchDiv in div)
            {
                if (SearchDiv.GetAttribute("className") == "resultBookInfo")
                {
                    SearchDiv.Children[0].Children[0].InvokeMember("click");
                    break;
                }
            }
        }

        /// <summary>
        /// 도서의 상세 정보를 보여주는 페이지로 들어가는 함수
        /// </summary>
        private void SearchResultEditonList()
        {
            HtmlElementCollection span = webBrowser1.Document.GetElementsByTagName("span");
            foreach (HtmlElement Search in span)
            {
                if (Search.GetAttribute("className") == "bookName")
                {
                    Search.Children[0].InvokeMember("click");
                    break;
                }
            }
        }

        /// <summary>
        /// 마크 보기 버튼을 클릭
        /// </summary>
        private void SearchResultDetail()
        {
            HtmlElementCollection a = webBrowser1.Document.GetElementsByTagName("a");
            foreach (HtmlElement Search in a)
            {
                if (Search.Id == "marcViewBtn")
                {
                    Search.InvokeMember("click");
                    break;
                }
            }
        }

        /// <summary>
        /// 해당하는 마크 따오기
        /// </summary>
        private void SearchResultMarc()
        {
            string Text = "";

            HtmlElementCollection TableName = webBrowser1.Document.GetElementsByTagName("table");
            foreach (HtmlElement SearchTable in TableName)
            {
                if (SearchTable.GetAttribute("className") == "tbl")
                {
                    HtmlElementCollection tdName = SearchTable.GetElementsByTagName("td");
                    foreach (HtmlElement SearchTd in tdName)
                    {
                        string Td = SearchTd.InnerText;

                        if (Td is null)
                            Td = "";

                        if (Td.Contains("▲"))
                            Text += SearchTd.InnerText + "\n";
                        else
                            Text += SearchTd.InnerText + "\t";
                    }
                }
            }

            if(!isAll)
                richTextBox1.Text = SplitText(Text);
            else
            {
                String_Text st = new String_Text();
                for (int a = 0; a < dataGridView1.Rows.Count; a++)
                {
                    string isbn = dataGridView1.Rows[a].Cells["ISBN13"].Value.ToString();

                    if (tb_URL.Text.Contains(isbn))
                    {
                        dataGridView1.Rows[a].Cells["BookMarc"].Value = SplitText(Text);
                        isBreak = true;
                    }
                }
            }
        }

        private string SplitText(string Text)
        {
            string result = "";

            string[] Line = Text.Split('\n');
            for (int a = 0; a < Line.Length - 1; a++)
            {
                string[] Tag = Line[a].Split('\t');
                int Length = Tag.Length;
                if (Tag[0].StartsWith("00")) continue;
                result += Tag[Length - 3] + "\t" + Tag[Length - 2] + "\t" + Tag[Length - 1] + "\n";
            }

            return result;
        }
        #endregion

        private void btn_Back_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void btn_Forward_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tb_URL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string url = tb_URL.Text;
                webBrowser1.Navigate(url);
            }
        }

        private void btn_Move_Click(object sender, EventArgs e)
        {
            isAll = false;
            marc.richTextBox1.Text = "";

            if (!tb_URL.Text.Contains("searchResultMarc")) {
                MessageBox.Show("코리스 마크가 있는곳으로 이동해주세요!");
                return;
            }
            SearchResultMarc();

            string Text = richTextBox1.Text;
            marc.richTextBox1.Text = MakeMarc(Text);
        }

        private void btn_AllMove_Click(object sender, EventArgs e)
        {
            String_Text st = new String_Text();

            progressBar1.Maximum = dataGridView1.Rows.Count;
            isAll = true;
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                for (int b = 0; b < dataGridView1.RowCount; b++)
                {
                    dataGridView1.Rows[b].DefaultCellStyle.BackColor = Color.White;
                }
                isBreak = false;
                dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Yellow;
                int idx = Convert.ToInt32(dataGridView1.Rows[a].Cells["List_idx"].Value.ToString());
                string isbn = dataGridView1.Rows[a].Cells["ISBN13"].Value.ToString();

                if (isbn == "") {
                    dataGridView1.Rows[a].DefaultCellStyle.ForeColor = Color.Red;
                    progressBar1.Value += 1;
                    continue;
                }

                string URL = MakeURL(isbn);
                webBrowser1.Navigate(URL);

                // 검색이 다 될때까지 기다림
                while (!isBreak)
                {
                    Delay(300);
                }

                string marc = dataGridView1.Rows[a].Cells["BookMarc"].Value.ToString();
                if (marc == "")
                    dataGridView1.Rows[a].DefaultCellStyle.ForeColor = Color.Red;
                else
                {
                    dataGridView1.Rows[a].DefaultCellStyle.ForeColor = Color.Blue;
                    this.marc.List_Book.Rows[idx].Cells["db_marc"].Value = st.made_Ori_marc(MakeMarc(marc));
                }

                progressBar1.Value += 1;
            }

            MessageBox.Show("완료되었습니다!");
        }

        string MakeMarc(string text)
        {
            string[] SplitLine = text.Split('\n');

            string result = "";
            foreach (string line in SplitLine)
            {
                if (line == "") continue;

                string[] SplitTag = line.Split('\t');
                SplitTag[2] = SplitTag[2].Replace("↔", "");
                SplitTag[1] = SplitTag[1].PadRight(2, ' ');
                result += string.Format("{0}\t{1}\t{2}\n", SplitTag[0], SplitTag[1], SplitTag[2]);
            }

            return result;
        }
        

        /// <summary>
        /// 지연시키는 함수
        /// </summary>
        /// <param name="ms">1000 = 1초</param>
        void Delay(int ms)
        {
            DateTime dateTimeNow = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, ms);
            DateTime dateTimeAdd = dateTimeNow.Add(duration);

            while (dateTimeAdd >= dateTimeNow)
            {
                Application.DoEvents();
                dateTimeNow = DateTime.Now;
            }
            return;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            int row = e.RowIndex;

            richTextBox1.Text = dataGridView1.Rows[row].Cells["BookMarc"].Value.ToString();
        }
    }
}
