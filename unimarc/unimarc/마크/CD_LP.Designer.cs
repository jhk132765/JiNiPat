﻿
namespace UniMarc.마크
{
    partial class CD_LP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Btn_New = new System.Windows.Forms.Button();
            this.cb_Type = new System.Windows.Forms.ComboBox();
            this.btn_Close = new System.Windows.Forms.Button();
            this.Btn_SaveMarc = new System.Windows.Forms.Button();
            this.Btn_Connect = new System.Windows.Forms.Button();
            this.cb_SiteCon = new System.Windows.Forms.ComboBox();
            this.tb_Num = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_T008 = new System.Windows.Forms.TextBox();
            this.tb_T007 = new System.Windows.Forms.TextBox();
            this.tb_T005 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Btn_Help008 = new System.Windows.Forms.Button();
            this.Btn_Help007 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_AddRow = new System.Windows.Forms.Button();
            this.btn_mk_marcList = new System.Windows.Forms.Button();
            this.btn_SaveList = new System.Windows.Forms.Button();
            this.Btn_Undo = new System.Windows.Forms.Button();
            this.Btn_SelectRemove = new System.Windows.Forms.Button();
            this.lbl_ListTitle = new System.Windows.Forms.Label();
            this.lbl_date = new System.Windows.Forms.Label();
            this.Btn_SelectList = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_CopySelect = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.etc1 = new System.Windows.Forms.RichTextBox();
            this.etc2 = new System.Windows.Forms.RichTextBox();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.richTextBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 68);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(734, 666);
            this.panel3.TabIndex = 6;
            // 
            // richTextBox1
            // 
            this.richTextBox1.AcceptsTab = true;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("굴림체", 11.25F);
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(732, 664);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            this.richTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyDown);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.Btn_New);
            this.panel2.Controls.Add(this.cb_Type);
            this.panel2.Controls.Add(this.btn_Close);
            this.panel2.Controls.Add(this.Btn_SaveMarc);
            this.panel2.Controls.Add(this.Btn_Connect);
            this.panel2.Controls.Add(this.cb_SiteCon);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(959, 33);
            this.panel2.TabIndex = 5;
            // 
            // Btn_New
            // 
            this.Btn_New.Location = new System.Drawing.Point(617, 4);
            this.Btn_New.Name = "Btn_New";
            this.Btn_New.Size = new System.Drawing.Size(97, 23);
            this.Btn_New.TabIndex = 4;
            this.Btn_New.Text = "신규 등록(F8)";
            this.Btn_New.UseVisualStyleBackColor = true;
            this.Btn_New.Click += new System.EventHandler(this.Btn_New_Click);
            // 
            // cb_Type
            // 
            this.cb_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Type.FormattingEnabled = true;
            this.cb_Type.Location = new System.Drawing.Point(545, 5);
            this.cb_Type.Name = "cb_Type";
            this.cb_Type.Size = new System.Drawing.Size(69, 20);
            this.cb_Type.TabIndex = 3;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(817, 4);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 2;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // Btn_SaveMarc
            // 
            this.Btn_SaveMarc.Location = new System.Drawing.Point(717, 4);
            this.Btn_SaveMarc.Name = "Btn_SaveMarc";
            this.Btn_SaveMarc.Size = new System.Drawing.Size(97, 23);
            this.Btn_SaveMarc.TabIndex = 2;
            this.Btn_SaveMarc.Text = "마크 저장(F9)";
            this.Btn_SaveMarc.UseVisualStyleBackColor = true;
            this.Btn_SaveMarc.Click += new System.EventHandler(this.Btn_SaveMarc_Click);
            // 
            // Btn_Connect
            // 
            this.Btn_Connect.Location = new System.Drawing.Point(140, 4);
            this.Btn_Connect.Name = "Btn_Connect";
            this.Btn_Connect.Size = new System.Drawing.Size(75, 23);
            this.Btn_Connect.TabIndex = 1;
            this.Btn_Connect.Text = "접   속";
            this.Btn_Connect.UseVisualStyleBackColor = true;
            this.Btn_Connect.Click += new System.EventHandler(this.Btn_Connect_Click);
            // 
            // cb_SiteCon
            // 
            this.cb_SiteCon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_SiteCon.FormattingEnabled = true;
            this.cb_SiteCon.Location = new System.Drawing.Point(12, 5);
            this.cb_SiteCon.Name = "cb_SiteCon";
            this.cb_SiteCon.Size = new System.Drawing.Size(121, 20);
            this.cb_SiteCon.TabIndex = 0;
            // 
            // tb_Num
            // 
            this.tb_Num.Location = new System.Drawing.Point(46, 6);
            this.tb_Num.Name = "tb_Num";
            this.tb_Num.Size = new System.Drawing.Size(87, 21);
            this.tb_Num.TabIndex = 1;
            this.tb_Num.Text = "000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "연번";
            // 
            // tb_T008
            // 
            this.tb_T008.Location = new System.Drawing.Point(705, 6);
            this.tb_T008.Name = "tb_T008";
            this.tb_T008.Size = new System.Drawing.Size(189, 21);
            this.tb_T008.TabIndex = 1;
            // 
            // tb_T007
            // 
            this.tb_T007.Location = new System.Drawing.Point(437, 6);
            this.tb_T007.Name = "tb_T007";
            this.tb_T007.Size = new System.Drawing.Size(177, 21);
            this.tb_T007.TabIndex = 1;
            // 
            // tb_T005
            // 
            this.tb_T005.Location = new System.Drawing.Point(196, 6);
            this.tb_T005.Name = "tb_T005";
            this.tb_T005.Size = new System.Drawing.Size(146, 21);
            this.tb_T005.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(163, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "T005";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Btn_Help008);
            this.panel1.Controls.Add(this.Btn_Help007);
            this.panel1.Controls.Add(this.tb_Num);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tb_T008);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tb_T007);
            this.panel1.Controls.Add(this.tb_T005);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(959, 35);
            this.panel1.TabIndex = 4;
            // 
            // Btn_Help008
            // 
            this.Btn_Help008.Location = new System.Drawing.Point(656, 5);
            this.Btn_Help008.Name = "Btn_Help008";
            this.Btn_Help008.Size = new System.Drawing.Size(47, 23);
            this.Btn_Help008.TabIndex = 2;
            this.Btn_Help008.Text = "T008";
            this.Btn_Help008.UseVisualStyleBackColor = true;
            this.Btn_Help008.Click += new System.EventHandler(this.Btn_Help008_Click);
            // 
            // Btn_Help007
            // 
            this.Btn_Help007.Location = new System.Drawing.Point(388, 5);
            this.Btn_Help007.Name = "Btn_Help007";
            this.Btn_Help007.Size = new System.Drawing.Size(47, 23);
            this.Btn_Help007.TabIndex = 2;
            this.Btn_Help007.Text = "T007";
            this.Btn_Help007.UseVisualStyleBackColor = true;
            this.Btn_Help007.Click += new System.EventHandler(this.Btn_Help007_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(332, 734);
            this.panel4.TabIndex = 7;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.dataGridView1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 89);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(332, 645);
            this.panel7.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.num,
            this.code,
            this.title,
            this.author,
            this.comp,
            this.price,
            this.m_idx,
            this.marc});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(332, 645);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.Visible = false;
            // 
            // num
            // 
            this.num.HeaderText = "연번";
            this.num.Name = "num";
            this.num.Width = 60;
            // 
            // code
            // 
            this.code.HeaderText = "상품코드";
            this.code.Name = "code";
            // 
            // title
            // 
            this.title.HeaderText = "상품명";
            this.title.Name = "title";
            // 
            // author
            // 
            this.author.HeaderText = "제작자";
            this.author.Name = "author";
            // 
            // comp
            // 
            this.comp.HeaderText = "제작사";
            this.comp.Name = "comp";
            // 
            // price
            // 
            this.price.HeaderText = "정가";
            this.price.Name = "price";
            // 
            // m_idx
            // 
            this.m_idx.HeaderText = "마크인덱스";
            this.m_idx.Name = "m_idx";
            this.m_idx.Visible = false;
            // 
            // marc
            // 
            this.marc.HeaderText = "마크";
            this.marc.Name = "marc";
            this.marc.Visible = false;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btn_CopySelect);
            this.panel6.Controls.Add(this.btn_AddRow);
            this.panel6.Controls.Add(this.btn_mk_marcList);
            this.panel6.Controls.Add(this.btn_SaveList);
            this.panel6.Controls.Add(this.Btn_Undo);
            this.panel6.Controls.Add(this.Btn_SelectRemove);
            this.panel6.Controls.Add(this.lbl_ListTitle);
            this.panel6.Controls.Add(this.lbl_date);
            this.panel6.Controls.Add(this.Btn_SelectList);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(332, 89);
            this.panel6.TabIndex = 0;
            // 
            // btn_AddRow
            // 
            this.btn_AddRow.Location = new System.Drawing.Point(167, 33);
            this.btn_AddRow.Name = "btn_AddRow";
            this.btn_AddRow.Size = new System.Drawing.Size(75, 23);
            this.btn_AddRow.TabIndex = 8;
            this.btn_AddRow.Text = "행 추가";
            this.btn_AddRow.UseVisualStyleBackColor = true;
            this.btn_AddRow.Click += new System.EventHandler(this.btn_AddRow_Click);
            // 
            // btn_mk_marcList
            // 
            this.btn_mk_marcList.Location = new System.Drawing.Point(243, 61);
            this.btn_mk_marcList.Name = "btn_mk_marcList";
            this.btn_mk_marcList.Size = new System.Drawing.Size(85, 23);
            this.btn_mk_marcList.TabIndex = 8;
            this.btn_mk_marcList.Text = "장비목록생성";
            this.btn_mk_marcList.UseVisualStyleBackColor = true;
            this.btn_mk_marcList.Click += new System.EventHandler(this.btn_mk_marcList_Click);
            // 
            // btn_SaveList
            // 
            this.btn_SaveList.Location = new System.Drawing.Point(248, 33);
            this.btn_SaveList.Name = "btn_SaveList";
            this.btn_SaveList.Size = new System.Drawing.Size(75, 23);
            this.btn_SaveList.TabIndex = 8;
            this.btn_SaveList.Text = "목록 저장";
            this.btn_SaveList.UseVisualStyleBackColor = true;
            this.btn_SaveList.Click += new System.EventHandler(this.btn_SaveList_Click);
            // 
            // Btn_Undo
            // 
            this.Btn_Undo.Location = new System.Drawing.Point(86, 33);
            this.Btn_Undo.Name = "Btn_Undo";
            this.Btn_Undo.Size = new System.Drawing.Size(75, 23);
            this.Btn_Undo.TabIndex = 4;
            this.Btn_Undo.Text = "되돌리기";
            this.Btn_Undo.UseVisualStyleBackColor = true;
            this.Btn_Undo.Click += new System.EventHandler(this.Btn_Undo_Click);
            // 
            // Btn_SelectRemove
            // 
            this.Btn_SelectRemove.Location = new System.Drawing.Point(5, 33);
            this.Btn_SelectRemove.Name = "Btn_SelectRemove";
            this.Btn_SelectRemove.Size = new System.Drawing.Size(75, 23);
            this.Btn_SelectRemove.TabIndex = 3;
            this.Btn_SelectRemove.Text = "선택 삭제";
            this.Btn_SelectRemove.UseVisualStyleBackColor = true;
            this.Btn_SelectRemove.Click += new System.EventHandler(this.Btn_SelectRemove_Click);
            // 
            // lbl_ListTitle
            // 
            this.lbl_ListTitle.AutoSize = true;
            this.lbl_ListTitle.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_ListTitle.Location = new System.Drawing.Point(87, 9);
            this.lbl_ListTitle.Name = "lbl_ListTitle";
            this.lbl_ListTitle.Size = new System.Drawing.Size(0, 13);
            this.lbl_ListTitle.TabIndex = 1;
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Location = new System.Drawing.Point(87, 15);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(111, 12);
            this.lbl_date.TabIndex = 2;
            this.lbl_date.Text = "목록일자(가려놓음)";
            this.lbl_date.Visible = false;
            // 
            // Btn_SelectList
            // 
            this.Btn_SelectList.Location = new System.Drawing.Point(6, 4);
            this.Btn_SelectList.Name = "Btn_SelectList";
            this.Btn_SelectList.Size = new System.Drawing.Size(75, 23);
            this.Btn_SelectList.TabIndex = 0;
            this.Btn_SelectList.Text = "목록 선택";
            this.Btn_SelectList.UseVisualStyleBackColor = true;
            this.Btn_SelectList.Click += new System.EventHandler(this.Btn_SelectList_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel3);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(332, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(959, 734);
            this.panel5.TabIndex = 8;
            // 
            // btn_CopySelect
            // 
            this.btn_CopySelect.BackColor = System.Drawing.Color.Khaki;
            this.btn_CopySelect.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CopySelect.Location = new System.Drawing.Point(207, 61);
            this.btn_CopySelect.Name = "btn_CopySelect";
            this.btn_CopySelect.Size = new System.Drawing.Size(30, 23);
            this.btn_CopySelect.TabIndex = 320;
            this.btn_CopySelect.Text = "0";
            this.btn_CopySelect.UseVisualStyleBackColor = false;
            this.btn_CopySelect.Click += new System.EventHandler(this.btn_CopySelect_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.tableLayoutPanel1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(734, 68);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(225, 666);
            this.panel8.TabIndex = 9;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.etc2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.etc1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(225, 666);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // etc1
            // 
            this.etc1.BackColor = System.Drawing.SystemColors.Window;
            this.etc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.etc1.Font = new System.Drawing.Font("굴림체", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.etc1.Location = new System.Drawing.Point(3, 3);
            this.etc1.Name = "etc1";
            this.etc1.Size = new System.Drawing.Size(219, 327);
            this.etc1.TabIndex = 33;
            this.etc1.Text = "Remark1";
            // 
            // etc2
            // 
            this.etc2.BackColor = System.Drawing.SystemColors.Window;
            this.etc2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.etc2.Font = new System.Drawing.Font("굴림체", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.etc2.Location = new System.Drawing.Point(3, 336);
            this.etc2.Name = "etc2";
            this.etc2.Size = new System.Drawing.Size(219, 327);
            this.etc2.TabIndex = 34;
            this.etc2.Text = "Remark2";
            // 
            // CD_LP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1291, 734);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Name = "CD_LP";
            this.Text = "CD_LP";
            this.Load += new System.EventHandler(this.CD_LP_Load);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tb_Num;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_T005;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Btn_Connect;
        private System.Windows.Forms.ComboBox cb_SiteCon;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button Btn_SelectList;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button Btn_SaveMarc;
        public System.Windows.Forms.Label lbl_ListTitle;
        private System.Windows.Forms.ComboBox cb_Type;
        public System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.Button Btn_Help008;
        private System.Windows.Forms.Button Btn_Help007;
        private System.Windows.Forms.Button Btn_New;
        public System.Windows.Forms.TextBox tb_T007;
        public System.Windows.Forms.TextBox tb_T008;
        private System.Windows.Forms.Button Btn_SelectRemove;
        private System.Windows.Forms.Button Btn_Undo;
        private System.Windows.Forms.Button btn_AddRow;
        private System.Windows.Forms.Button btn_SaveList;
        private System.Windows.Forms.Button btn_mk_marcList;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn code;
        private System.Windows.Forms.DataGridViewTextBoxColumn title;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn marc;
        private System.Windows.Forms.Button btn_CopySelect;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.RichTextBox etc1;
        public System.Windows.Forms.RichTextBox etc2;
    }
}