﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Mac;

namespace WindowsFormsApp1.마크
{
    public partial class Check_ISBN_Sub : Form
    {
        Check_ISBN ci;
        public int row;
        public string Call_API = string.Empty;
        int rowidx;
        public Check_ISBN_Sub(Check_ISBN _ci)
        {
            InitializeComponent();
            ci = _ci;
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Check_ISBN_Sub_Load(object sender, EventArgs e)
        {
            Sort_data();
            same_chk();     // 비슷한거 색깔표시

            dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[0];
            this.ActiveControl = dataGridView1;
            rowidx = 0;
        }
        private void Sort_data()
        {
            tb_book_name.Text = ci.dataGridView1.Rows[row].Cells["book_name"].Value.ToString();
            tb_author.Text    = ci.dataGridView1.Rows[row].Cells["author"].Value.ToString();
            tb_book_comp.Text = ci.dataGridView1.Rows[row].Cells["book_comp"].Value.ToString();

            if (ci.dataGridView1.Rows[row].Cells["unit"].Value != null)
                tb_price.Text = ci.dataGridView1.Rows[row].Cells["unit"].Value.ToString();

            else tb_price.Text = "";

            string data = ci.dataGridView1.Rows[row].Cells["api_data"].Value.ToString();

            // 도서명 / 저자 / 출판사 / isbn / 출간일 / 카테고리 / 품절여부
            string[] tmp = data.Split('|');
            string[] grid = { "", "", "", "", "", "", "", "", "" };

            int idx = 9;
            for(int a= 0; a < tmp.Length; a++)
            {
                if (a % idx == 0) grid[0] = tmp[a];
                if (a % idx == 1) grid[1] = tmp[a];
                if (a % idx == 2) grid[2] = tmp[a];
                if (a % idx == 3) grid[3] = tmp[a];
                if (a % idx == 4) grid[4] = tmp[a];
                if (a % idx == 5) grid[5] = change_Date_type(tmp[a]);
                if (a % idx == 6) grid[6] = tmp[a];
                if (a % idx == 7) grid[7] = tmp[a];
                if (a % idx == 8) { grid[8] = tmp[a]; dataGridView1.Rows.Add(grid); }
            }
        }
        private string change_Date_type(string date)
        {
            if (Call_API == "알라딘")
            {
                try
                {
                    return String.Format("{0:yyyy/MM/dd}",
                    DateTime.Parse(date.Remove(date.IndexOf(" G"))));
                }
                catch { return date; }
            }
            else if (Call_API == "네이버")
            {
                if (date.Length < 5) 
                    return date;

                return DateTime.ParseExact(date, "yyyyMMdd", null).ToString("yyyy-MM-dd");
            }
            else
                return date;
        }
        private void same_chk()
        {
            // 도서명, 저자, 출판사, ISBN, 정가
            string[] ori_data = { tb_book_name.Text, 
                                  tb_author.Text, 
                                  tb_book_comp.Text, 
                                  tb_isbn.Text, 
                                  tb_price.Text.Replace(",", "") };

            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                int chk_idx = 0;

                if (dataGridView1.Rows[a].Cells["book_name"].Value.ToString() == ori_data[0])
                {
                    chk_idx++;
                }
                if (dataGridView1.Rows[a].Cells["author"].Value.ToString().Contains(ori_data[1]) == true)
                {
                    chk_idx++;
                }
                if (dataGridView1.Rows[a].Cells["book_comp"].Value.ToString() == ori_data[2])
                {
                    chk_idx++;
                }
                if (chk_idx >= 2)
                {
                    int pay = Convert.ToInt32(dataGridView1.Rows[a].Cells["price"].Value.ToString());
                    int price = 0;
                    if (ori_data[4] != "")
                        price = Convert.ToInt32(ori_data[4]);

                    if (price == pay)
                        dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Yellow;

                    else if (price - 500 < pay && pay < price + 500)
                        dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Orange;

                    else
                        dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowidx = e.RowIndex;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e != null) { rowidx = e.RowIndex; }
            string book_name = dataGridView1.Rows[rowidx].Cells["book_name"].Value.ToString();
            string author = dataGridView1.Rows[rowidx].Cells["author"].Value.ToString();
            string book_comp = dataGridView1.Rows[rowidx].Cells["book_comp"].Value.ToString();
            string isbn = dataGridView1.Rows[rowidx].Cells["isbn"].Value.ToString();
            string price = dataGridView1.Rows[rowidx].Cells["price"].Value.ToString();
            string Date = dataGridView1.Rows[rowidx].Cells["pubDate"].Value.ToString();
            string category = dataGridView1.Rows[rowidx].Cells["category"].Value.ToString();
            string sold = dataGridView1.Rows[rowidx].Cells["sold_out"].Value.ToString();
            string image = dataGridView1.Rows[rowidx].Cells["Column1"].Value.ToString();

            ci.dataGridView1.Rows[row].Cells["search_book_name"].Value = book_name;
            ci.dataGridView1.Rows[row].Cells["search_author"].Value = author;
            ci.dataGridView1.Rows[row].Cells["search_book_comp"].Value = book_comp;
            ci.dataGridView1.Rows[row].Cells["isbn"].Value = isbn;
            ci.dataGridView1.Rows[row].Cells["price"].Value = price;
            ci.dataGridView1.Rows[row].Cells["pubDate"].Value = Date;
            ci.dataGridView1.Rows[row].Cells["category"].Value = ci.Aladin_CategorySort(category);
            ci.dataGridView1.Rows[row].Cells["sold_out"].Value = sold;
            ci.dataGridView1.Rows[row].Cells["image"].Value = image;
            
            ci.dataGridView1.Rows[row].DefaultCellStyle.BackColor = Color.Yellow;

            this.Close();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { dataGridView1_CellDoubleClick(null, null); }
            if (e.KeyCode == Keys.Up)
            {
                rowidx--;
                if (rowidx < 0)
                    rowidx = 0;
            }
            if (e.KeyCode == Keys.Down)
            {
                rowidx++;
                if (rowidx > dataGridView1.Rows.Count - 1)
                    rowidx = dataGridView1.Rows.Count - 1;
            }
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }
    }
}
