﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;

namespace UniMarc.마크
{
    public partial class Marc_Plan_Sub_SelectList : Form
    {
        Helper_DB db = new Helper_DB();
        string compidx;
        Marc_Plan mp;
        public Marc_Plan_Sub_SelectList(Marc_Plan _mp)
        {
            InitializeComponent();
            mp = _mp;
            compidx = Properties.Settings.Default.compidx;
        }

        private void Marc_Plan_Sub_SelectList_Load(object sender, EventArgs e)
        {
            db.DBcon();

            string[] gu = { "진행", "완료" };
            cb_gu.Items.AddRange(gu);
            cb_gu.SelectedIndex = 0;

            dataGridView1.Columns["list_name"].ReadOnly = true;
            dataGridView1.Columns["date"].ReadOnly = true;
            dataGridView1.Columns["user"].ReadOnly = true;

            // btn_Search_Click(null, e);
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }

        private void tb_Search_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.Khaki;
        }

        private void tb_Search_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
        }

        public void btn_Search_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            string gu = cb_gu.Text;         // 검색 필터
            string text = tb_Search.Text;   // 검색어 DB LIKE 검색
            string Area = "`idx`, `work_list`, `date`, `user`";

            string cmd = string.Format("SELECT {0} FROM `Specs_List` WHERE `compidx` = {1} AND `state` = \"{2}\" AND `work_list` LIKE \"%{3}%\"",
                Area, compidx, gu, text);

            string res = db.DB_Send_CMD_Search(cmd);
            string[] ary = res.Split('|');
            Input_Grid(ary);
        }
        #region Search_Sub
        private void Input_Grid(string[] ary)
        {
            string[] grid = { "", "", "", "", "F" };
            for(int a = 0; a < ary.Length; a++)
            {
                if (a % 4 == 0) grid[0] = ary[a];
                if (a % 4 == 1) grid[1] = ary[a];
                if (a % 4 == 2) grid[2] = ary[a];
                if (a % 4 == 3) {
                    grid[3] = ary[a];
                    dataGridView1.Rows.Add(grid);
                }
            }
        }
        #endregion

        private void Change_State(object sender, EventArgs e)     // 완료, 진행처리
        {
            string name = ((Button)sender).Text;
            int[] CheckRow = CheckIndex();

            if (CheckRow.Length != 1) {
                MessageBox.Show("처리할 목록 [1]개 선택하여 주세요.");
                return;
            }

            int row = CheckRow[0];

            string msg = string.Format("【{0}】을(를) {1} 하시겠습니까?", dataGridView1.Rows[row].Cells["list_name"].Value.ToString(), name);
            if (MessageBox.Show(msg, name, MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                return;
            
            string[] search_col = { "idx", "work_list", "date" };
            string[] update_col = { "state" };
            string[] search_data = {
                dataGridView1.Rows[row].Cells["idx"].Value.ToString(),
                dataGridView1.Rows[row].Cells["list_name"].Value.ToString(),
                dataGridView1.Rows[row].Cells["date"].Value.ToString() };
            string[] update_data = { "" };

            if (name == "완료처리")
                update_data[0] = "완료";
            else
                update_data[0] = "진행";

            string cmd = db.More_Update("Specs_List", update_col, update_data, search_col, search_data);
            db.DB_Send_CMD_reVoid(cmd);

            btn_Search_Click(null, e);
        }

        private void btn_Edit_Click(object sender, EventArgs e)         // 편집 (V 확인필요)
        {
            int[] CheckRow = CheckIndex();

            if (CheckRow.Length != 1) {
                MessageBox.Show("수정할 목록 [1]개 선택하여 주세요.");
                return;
            }

            int row = CheckRow[0];

            Marc_Plan_Sub_SelectList_Edit edit = new Marc_Plan_Sub_SelectList_Edit(this);
            edit.TopMost = true;
            edit.Show();
            edit.BaseSetting(dataGridView1.Rows[row].Cells["idx"].Value.ToString());
            edit.TextBoxSetting(dataGridView1.Rows[row].Cells["list_name"].Value.ToString());
        }

        private void btn_Morge_Click(object sender, EventArgs e)       // 병합 (V 확인필요)
        {
            int[] CheckRow = CheckIndex();

            if (CheckRow.Length < 2) {
                MessageBox.Show("병합할 목록 [2]개이상 선택하여 주세요.");
                return;
            }

            List<string> ListIndex = new List<string>();
            List<string> ListName = new List<string>();
            List<string> ListDate = new List<string>();
            List<string> ListUser = new List<string>();

            foreach (int row in CheckRow)
            {
                ListIndex.Add(dataGridView1.Rows[row].Cells["idx"].Value.ToString());
                ListName.Add(dataGridView1.Rows[row].Cells["list_name"].Value.ToString());
                ListDate.Add(dataGridView1.Rows[row].Cells["date"].Value.ToString());
                ListUser.Add(dataGridView1.Rows[row].Cells["user"].Value.ToString());
            }

            Marc_Plan_Sub_SelectList_Morge morge = new Marc_Plan_Sub_SelectList_Morge(this);
            morge.Init(ListIndex, ListName, ListDate, ListUser);
            morge.TopMost = true;
            morge.Show(this);
        }

        private void btn_Delete_Click(object sender, EventArgs e)       // 삭제 (V 확인필요)
        {
            int[] CheckRow = CheckIndex();

            if (CheckRow.Length != 1) {
                MessageBox.Show("삭제할 목록 [1]개 선택하여 주세요.");
                return;
            }

            int row = CheckRow[0];
            string msg = string.Format("【{0}】을(를) 삭제하시겠습니까?", dataGridView1.Rows[row].Cells["list_name"].Value.ToString());

            if (MessageBox.Show(msg, "삭제하시겠습니까?", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            string idx = dataGridView1.Rows[row].Cells["idx"].Value.ToString();
            string[] search_col = { "work_list", "date" };
            string[] search_data = { dataGridView1.Rows[row].Cells["list_name"].Value.ToString(),
                                     dataGridView1.Rows[row].Cells["date"].Value.ToString() };

            // 목록 제거
            string cmd = db.DB_Delete_More_term("Specs_List", "idx", idx, search_col, search_data);
            db.DB_Send_CMD_reVoid(cmd);

            // 목록 내 마크 제거
            cmd = db.DB_Delete_No_Limit("Specs_Marc", "compidx", compidx, search_col, search_data);
            db.DB_Send_CMD_reVoid(cmd);

            MessageBox.Show("삭제되었습니다.");
            btn_Search_Click(null, null);
        }

        #region ButtonSub
        /// <summary>
        /// colCheck에 체크가 된 RowIndex값을 가져옴
        /// </summary>
        /// <returns></returns>
        private int[] CheckIndex()
        {
            List<int> CheckRow = new List<int>();
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (dataGridView1.Rows[a].Cells["colCheck"].Value == null)
                    continue;

                if (dataGridView1.Rows[a].Cells["colCheck"].Value.ToString().IndexOf("T") > -1)
                    CheckRow.Add(a);
            }

            return CheckRow.ToArray();
        }
        #endregion

        private void btn_OpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFileDialog = new OpenFileDialog();
            if (OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                mp.dataGridView1.Rows.Clear();
                string filePath = OpenFileDialog.FileName;
                try
                {
                    System.IO.StreamReader r = new System.IO.StreamReader(filePath, Encoding.Default);
                    InputGridByFileData(r.ReadToEnd());
                    r.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        #region OpenFileSub

        void InputGridByFileData(string text)
        {
            String_Text st = new String_Text();
            
            string[] grid = text.Split('');
            for (int a = 0; a < grid.Length - 1; a++)
            {
                string[] Search = {
                // 등록번호, 분류기호, 저자기호, 볼륨v, 복본c, 별치f
                    "049l", "090a", "090b", "049v", "049c", "049f",
                // ISBN, 도서명, 총서명1, 총서번호1, 총서명2, 총서번호2, 출판사, 정가, 저자
                    "020a", "245a", "440a", "440v", "490a", "490v", "260b", "950b", "245d" };
                string[] Search_Res = st.Take_Tag(grid[a], Search);
                

                string[] Author_Search = { "100a", "110a", "111a" };
                string[] Author_Res = st.Take_Tag(grid[a], Author_Search);
                string author_Fin = Search_Res[14];
                foreach (string author in Author_Res)
                {
                    if (author != "") {
                        author_Fin = author;
                        break;
                    }
                }

                string[] AddGrid = {
                // idx, 연번, 등록번호, 분류, 저자기호
                    "", "", Search_Res[0], Search_Res[1], Search_Res[2],
                // 볼륨v, 복본c, 별치f, 구분, isbn
                    Search_Res[3], Search_Res[4], Search_Res[5], "", Search_Res[6],
                // 도서명, 총서명1, 총서번호1, 총서명1, 총서번호2
                    Search_Res[7], Search_Res[8], Search_Res[9], Search_Res[10], Search_Res[11],
                // 저자, 출판사, 정가, midx, 마크
                    author_Fin, Search_Res[12], Search_Res[13], "", grid[a],
                // 검색태그
                    "", "T" };
                mp.dataGridView1.Rows.Add(AddGrid);
                this.Close();
            }
        }
        #endregion

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cb_gu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_gu.SelectedIndex == 0) {
                btn_Progress.Enabled = false;
                btn_Complite.Enabled = true;
                btn_Search_Click(null, e);
            }
            else {
                btn_Complite.Enabled = false;
                btn_Progress.Enabled = true;
                btn_Search_Click(null, e);
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int col = e.ColumnIndex;
            int row = e.RowIndex;

            if (dataGridView1.Columns[col].Name == "colCheck")
                return;

            string idx = dataGridView1.Rows[row].Cells["idx"].Value.ToString();
            string list_name = dataGridView1.Rows[row].Cells["list_name"].Value.ToString();
            string date = dataGridView1.Rows[row].Cells["date"].Value.ToString();

            mp.mk_Grid(list_name, date);
            mp.mk_Panel(idx, list_name, date);

            this.Close();
        }

        private void tb_Search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_Search_Click(null, null);
        }
    }
}
