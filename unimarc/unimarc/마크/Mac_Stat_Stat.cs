﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;

namespace UniMarc.마크
{
    public partial class Mac_Stat_Stat : Form
    {
        Mac_Stat stat;
        Helper_DB db = new Helper_DB();
        public string compidx;
        public Mac_Stat_Stat(Mac_Stat _stat)
        {
            InitializeComponent();
            stat = _stat;
        }

        private void Mac_Stat_Stat_Load(object sender, EventArgs e)
        {
            db.DBcon();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Label[] label = {
                lbl_0, lbl_1, lbl_2, lbl_3, lbl_4,
                lbl_5, lbl_6, lbl_7, lbl_8, lbl_9, lbl_10
            };
            for (int a = 0; a < label.Length; a++)
            {
                string count = a.ToString();
                if (count == "10")
                    count = "";
                string m_cmd = string.Format("SELECT COUNT(*) from `Marc` WHERE LEFT(`division`,1) = '{0}' AND `compidx` = {1};",
                                             count, compidx);
                string cmd = db.self_Made_Cmd(m_cmd);
                cmd = cmd.Replace("|", "");
                label[a].Text = cmd;
                lbl_total.Text = tab1_total(cmd);
            }
        }
        #region button1_Click_Sub
        string tab1_total(string text)
        {
            int lbl = Convert.ToInt32(lbl_total.Text);
            text = Regex.Replace(text, @"[^0-9_]", "", RegexOptions.Singleline);
            int target = Convert.ToInt32(text);
            int res = lbl + target;
            return res.ToString();
        }
        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            string date = dateTimePicker1.Value.ToString("yyyy-MM-dd");

            string kor = "MID(`008tag`, 36, 3) = 'kor'";
            string nkor = "MID(`008tag`, 36, 3) != 'kor'";

            // 국내도서 누적소장
            string cmd = string.Format(
                "SELECT COUNT(*) from `Marc` WHERE `compidx` = {0} AND '{1} 00:00:00' > `date` AND {2};", compidx, date, kor);
            lbl_kor_s.Text = db.self_Made_Cmd(cmd).Replace("|", "");

            // 외국도서 누적소장
            cmd = string.Format(
                "SELECT COUNT(*) from `Marc` WHERE `compidx` = {0} AND '{1} 00:00:00' > `date` AND {2};", compidx, date, nkor);
            lbl_log_s.Text = db.self_Made_Cmd(cmd).Replace("|", "");

            // 국내도서 소장추가
            cmd = string.Format(
                "SELECT COUNT(*) from `Marc` WHERE `compidx` = {0} AND '{1} 23:59:59' > `date` AND `date` > '{1} 00:00:00' AND {2};", 
                compidx, date, kor);
            lbl_kor_a.Text = db.self_Made_Cmd(cmd).Replace("|", "");

            // 국내도서 소장추가
            cmd = string.Format(
                "SELECT COUNT(*) from `Marc` WHERE `compidx` = {0} AND '{0} 23:59:59' > `date` AND `date` > '{1} 00:00:00' AND {2};", 
                compidx, date, nkor);
            lbl_log_a.Text = db.self_Made_Cmd(cmd).Replace("|", "");

            Label[] k_lbl = { lbl_kor_s, lbl_kor_a, lbl_kor_t };    // 국내서 라벨
            tab2_total1(k_lbl);

            Label[] l_lbl = { lbl_log_s, lbl_log_a, lbl_log_t };    // 서양서 라벨
            tab2_total1(l_lbl);

            Label[] s_lbl = { lbl_kor_s, lbl_log_s, lbl_s_total };  // 소장 라벨
            tab2_total1(s_lbl);

            Label[] a_lbl = { lbl_kor_a, lbl_log_a, lbl_a_total };  // 추가 라벨
            tab2_total1(a_lbl);

        }
        #region button2_Click_Sub
        void tab2_total1(Label[] lbl)
        {
            string str_stock = Regex.Replace(lbl[0].Text, @"[^0-9_]", "", RegexOptions.Singleline);
            int stock = Convert.ToInt32(str_stock);

            string str_add = Regex.Replace(lbl[1].Text, @"[^0-9_]", "", RegexOptions.Singleline);
            int add = Convert.ToInt32(str_add);

            int res = stock + add;

            lbl[2].Text = res.ToString();
        }
        #endregion

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
