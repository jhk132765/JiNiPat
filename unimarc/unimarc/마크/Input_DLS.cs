﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UniMarc.마크;

namespace WindowsFormsApp1.DLS
{
    public partial class Input_DLS : Form
    {
        Main main;
        public Input_DLS(Main _main)
        {
            InitializeComponent();
            main = _main;
        }

        #region SHDocVw
        SHDocVw.WebBrowser nativeBrowser;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            nativeBrowser = (SHDocVw.WebBrowser)webBrowser1.ActiveXInstance;
            nativeBrowser.NewWindow2 += nativeBrowser_NewWindow2;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            nativeBrowser.NewWindow2 -= nativeBrowser_NewWindow2;
            base.OnFormClosing(e);
        }

        void nativeBrowser_NewWindow2(ref object ppDisp, ref bool Cancel)
        {
            var popup = new DLS_Manage();
            popup.Show(this);
            ppDisp = popup.Browser.ActiveXInstance;
        }
        #endregion

        private void Input_DLS_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate("https://reading.jnei.go.kr/r/newReading/member/loginForm.jsp");
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            tb_URL.Text = webBrowser1.Url.AbsoluteUri;
            this.Text = webBrowser1.DocumentText;

            if (tb_URL.Text.Contains("loginForm"))
            {
                Load_Search();
            }
        }

        private void Load_Search()
        {
            string ID = "wolpyeong";
            string PW = "wp3906705*";
            Text = ID;

            Delay(1000);

            webBrowser1.Document.GetElementById("s_id").SetAttribute("value", ID);
            webBrowser1.Document.GetElementById("s_pwd").SetAttribute("value", PW);

            HtmlElementCollection button = webBrowser1.Document.GetElementsByTagName("button");
            foreach (HtmlElement SearchButton in button)
            {
                if (SearchButton.Id == "s_login")
                    SearchButton.InvokeMember("click");
            }

            Delay(4000);
            webBrowser1.Navigate("https://reading.jnei.go.kr/r/dls_new/loan/loan.jsp");
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void btn_Forward_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void btn_Go_Click(object sender, EventArgs e)
        {
            string url = tb_URL.Text;
            webBrowser1.Navigate(url);
        }

        private void tb_URL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_Go_Click(null, null);
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 지연시키는 함수
        /// </summary>
        /// <param name="ms">1000 = 1초</param>
        void Delay(int ms)
        {
            DateTime dateTimeNow = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, ms);
            DateTime dateTimeAdd = dateTimeNow.Add(duration);
            while (dateTimeAdd >= dateTimeNow)
            {
                Application.DoEvents();
                dateTimeNow = DateTime.Now;
            }
            return;
        }

        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {

        }
    }
}
