﻿namespace UniMarc.마크
{
    partial class CD_LP_List
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Btn_SelectList = new System.Windows.Forms.Button();
            this.Btn_CheckFalse = new System.Windows.Forms.Button();
            this.Btn_ViewMarc = new System.Windows.Forms.Button();
            this.Btn_Close = new System.Windows.Forms.Button();
            this.Btn_CheckTrue = new System.Windows.Forms.Button();
            this.Btn_Excel = new System.Windows.Forms.Button();
            this.Btn_OutPut = new System.Windows.Forms.Button();
            this.lbl_date = new System.Windows.Forms.Label();
            this.lbl_ListTitle = new System.Windows.Forms.Label();
            this.cb_EncodingType = new System.Windows.Forms.ComboBox();
            this.Btn_SelectRemove = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Marc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Artist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Copy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClassNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 713);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1290, 1);
            this.panel2.TabIndex = 1;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1290, 1);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Btn_SelectList
            // 
            this.Btn_SelectList.Location = new System.Drawing.Point(7, 5);
            this.Btn_SelectList.Name = "Btn_SelectList";
            this.Btn_SelectList.Size = new System.Drawing.Size(75, 23);
            this.Btn_SelectList.TabIndex = 0;
            this.Btn_SelectList.Text = "목록 선택";
            this.Btn_SelectList.UseVisualStyleBackColor = true;
            this.Btn_SelectList.Click += new System.EventHandler(this.Btn_SelectList_Click);
            // 
            // Btn_CheckFalse
            // 
            this.Btn_CheckFalse.Location = new System.Drawing.Point(419, 5);
            this.Btn_CheckFalse.Name = "Btn_CheckFalse";
            this.Btn_CheckFalse.Size = new System.Drawing.Size(75, 23);
            this.Btn_CheckFalse.TabIndex = 0;
            this.Btn_CheckFalse.Text = "선택 해제";
            this.Btn_CheckFalse.UseVisualStyleBackColor = true;
            this.Btn_CheckFalse.Click += new System.EventHandler(this.Btn_SelectGrid);
            // 
            // Btn_ViewMarc
            // 
            this.Btn_ViewMarc.Location = new System.Drawing.Point(707, 5);
            this.Btn_ViewMarc.Name = "Btn_ViewMarc";
            this.Btn_ViewMarc.Size = new System.Drawing.Size(75, 23);
            this.Btn_ViewMarc.TabIndex = 0;
            this.Btn_ViewMarc.Text = "마크보이기";
            this.Btn_ViewMarc.UseVisualStyleBackColor = true;
            this.Btn_ViewMarc.Click += new System.EventHandler(this.Btn_ViewMarc_Click);
            // 
            // Btn_Close
            // 
            this.Btn_Close.Location = new System.Drawing.Point(948, 5);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(75, 23);
            this.Btn_Close.TabIndex = 0;
            this.Btn_Close.Text = "닫    기";
            this.Btn_Close.UseVisualStyleBackColor = true;
            this.Btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // Btn_CheckTrue
            // 
            this.Btn_CheckTrue.Location = new System.Drawing.Point(341, 5);
            this.Btn_CheckTrue.Name = "Btn_CheckTrue";
            this.Btn_CheckTrue.Size = new System.Drawing.Size(75, 23);
            this.Btn_CheckTrue.TabIndex = 0;
            this.Btn_CheckTrue.Text = "전체 선택";
            this.Btn_CheckTrue.UseVisualStyleBackColor = true;
            this.Btn_CheckTrue.Click += new System.EventHandler(this.Btn_SelectGrid);
            // 
            // Btn_Excel
            // 
            this.Btn_Excel.Location = new System.Drawing.Point(626, 5);
            this.Btn_Excel.Name = "Btn_Excel";
            this.Btn_Excel.Size = new System.Drawing.Size(75, 23);
            this.Btn_Excel.TabIndex = 0;
            this.Btn_Excel.Text = "엑    셀";
            this.Btn_Excel.UseVisualStyleBackColor = true;
            this.Btn_Excel.Click += new System.EventHandler(this.Btn_Excel_Click);
            // 
            // Btn_OutPut
            // 
            this.Btn_OutPut.Location = new System.Drawing.Point(864, 5);
            this.Btn_OutPut.Name = "Btn_OutPut";
            this.Btn_OutPut.Size = new System.Drawing.Size(75, 23);
            this.Btn_OutPut.TabIndex = 0;
            this.Btn_OutPut.Text = "반    출";
            this.Btn_OutPut.UseVisualStyleBackColor = true;
            this.Btn_OutPut.Click += new System.EventHandler(this.Btn_OutPut_Click);
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Location = new System.Drawing.Point(88, 16);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(111, 12);
            this.lbl_date.TabIndex = 3;
            this.lbl_date.Text = "목록일자(가려놓음)";
            this.lbl_date.Visible = false;
            // 
            // lbl_ListTitle
            // 
            this.lbl_ListTitle.AutoSize = true;
            this.lbl_ListTitle.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_ListTitle.Location = new System.Drawing.Point(88, 10);
            this.lbl_ListTitle.Name = "lbl_ListTitle";
            this.lbl_ListTitle.Size = new System.Drawing.Size(0, 13);
            this.lbl_ListTitle.TabIndex = 2;
            // 
            // cb_EncodingType
            // 
            this.cb_EncodingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_EncodingType.FormattingEnabled = true;
            this.cb_EncodingType.Items.AddRange(new object[] {
            "ANSI",
            "UTF-8",
            "UniCode"});
            this.cb_EncodingType.Location = new System.Drawing.Point(788, 6);
            this.cb_EncodingType.Name = "cb_EncodingType";
            this.cb_EncodingType.Size = new System.Drawing.Size(75, 20);
            this.cb_EncodingType.TabIndex = 4;
            // 
            // Btn_SelectRemove
            // 
            this.Btn_SelectRemove.Location = new System.Drawing.Point(497, 5);
            this.Btn_SelectRemove.Name = "Btn_SelectRemove";
            this.Btn_SelectRemove.Size = new System.Drawing.Size(75, 23);
            this.Btn_SelectRemove.TabIndex = 5;
            this.Btn_SelectRemove.Text = "선택 삭제";
            this.Btn_SelectRemove.UseVisualStyleBackColor = true;
            this.Btn_SelectRemove.Click += new System.EventHandler(this.Btn_SelectRemove_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Btn_SelectRemove);
            this.panel1.Controls.Add(this.cb_EncodingType);
            this.panel1.Controls.Add(this.lbl_ListTitle);
            this.panel1.Controls.Add(this.lbl_date);
            this.panel1.Controls.Add(this.Btn_OutPut);
            this.panel1.Controls.Add(this.Btn_Excel);
            this.panel1.Controls.Add(this.Btn_CheckTrue);
            this.panel1.Controls.Add(this.Btn_Close);
            this.panel1.Controls.Add(this.Btn_ViewMarc);
            this.panel1.Controls.Add(this.Btn_CheckFalse);
            this.panel1.Controls.Add(this.Btn_SelectList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1290, 35);
            this.panel1.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 35);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1290, 678);
            this.panel3.TabIndex = 2;
            // 
            // Marc
            // 
            this.Marc.HeaderText = "마크";
            this.Marc.Name = "Marc";
            this.Marc.Visible = false;
            // 
            // Type
            // 
            this.Type.HeaderText = "유형";
            this.Type.Name = "Type";
            // 
            // Price
            // 
            this.Price.HeaderText = "정가";
            this.Price.Name = "Price";
            // 
            // Comp
            // 
            this.Comp.HeaderText = "제작";
            this.Comp.Name = "Comp";
            // 
            // Artist
            // 
            this.Artist.HeaderText = "감독/뮤지션";
            this.Artist.Name = "Artist";
            // 
            // Title
            // 
            this.Title.HeaderText = "작품명";
            this.Title.Name = "Title";
            this.Title.Width = 500;
            // 
            // Fix
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Fix.DefaultCellStyle = dataGridViewCellStyle7;
            this.Fix.HeaderText = "별치";
            this.Fix.Name = "Fix";
            this.Fix.Width = 60;
            // 
            // Copy
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Copy.DefaultCellStyle = dataGridViewCellStyle6;
            this.Copy.HeaderText = "복본";
            this.Copy.Name = "Copy";
            this.Copy.Width = 50;
            // 
            // Vol
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Vol.DefaultCellStyle = dataGridViewCellStyle5;
            this.Vol.HeaderText = "볼륨";
            this.Vol.Name = "Vol";
            this.Vol.Width = 60;
            // 
            // AuthorSymbol
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.AuthorSymbol.DefaultCellStyle = dataGridViewCellStyle4;
            this.AuthorSymbol.HeaderText = "저자기호";
            this.AuthorSymbol.Name = "AuthorSymbol";
            this.AuthorSymbol.Width = 80;
            // 
            // ClassNum
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClassNum.DefaultCellStyle = dataGridViewCellStyle3;
            this.ClassNum.HeaderText = "분류기호";
            this.ClassNum.Name = "ClassNum";
            this.ClassNum.Width = 80;
            // 
            // RegNum
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.RegNum.DefaultCellStyle = dataGridViewCellStyle2;
            this.RegNum.HeaderText = "등록번호";
            this.RegNum.Name = "RegNum";
            this.RegNum.Width = 120;
            // 
            // Num
            // 
            this.Num.HeaderText = "연번";
            this.Num.Name = "Num";
            this.Num.Width = 50;
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.Visible = false;
            // 
            // Check
            // 
            this.Check.FalseValue = "F";
            this.Check.HeaderText = "선택";
            this.Check.Name = "Check";
            this.Check.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Check.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Check.TrueValue = "T";
            this.Check.Width = 50;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Check,
            this.idx,
            this.Num,
            this.RegNum,
            this.ClassNum,
            this.AuthorSymbol,
            this.Vol,
            this.Copy,
            this.Fix,
            this.Title,
            this.Artist,
            this.Comp,
            this.Price,
            this.Type,
            this.Marc});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1290, 678);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // CD_LP_List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 714);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "CD_LP_List";
            this.Text = "CD_LP_List";
            this.Load += new System.EventHandler(this.CD_LP_List_Load);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button Btn_SelectList;
        private System.Windows.Forms.Button Btn_CheckFalse;
        private System.Windows.Forms.Button Btn_ViewMarc;
        private System.Windows.Forms.Button Btn_Close;
        private System.Windows.Forms.Button Btn_CheckTrue;
        private System.Windows.Forms.Button Btn_Excel;
        private System.Windows.Forms.Button Btn_OutPut;
        public System.Windows.Forms.Label lbl_date;
        public System.Windows.Forms.Label lbl_ListTitle;
        private System.Windows.Forms.ComboBox cb_EncodingType;
        private System.Windows.Forms.Button Btn_SelectRemove;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Check;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClassNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorSymbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Copy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fix;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Artist;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Marc;
    }
}