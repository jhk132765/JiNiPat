﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniMarc.마크
{
    public partial class Zoom_Picture : Form
    {
        public string url = "";
        public string ISBN = "";
        public Zoom_Picture()
        {
            InitializeComponent();
        }

        private void Zoom_Picture_Load(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = url;
            pictureBox1.MouseWheel += new MouseEventHandler(MouseWheelEvent);

            string PreViewURL =
                string.Format("http://preview.kyobobook.co.kr/preview.jsp?siteGb=INK&ejkGb=KOR&barcode={0}&loginYn=N&orderClick=JAW",
                ISBN);

            webBrowser1.Navigate(PreViewURL);
        }

        private void MouseWheelEvent(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
                pictureBox1.Size = new Size((int)(pictureBox1.Size.Width * 1.2), (int)(pictureBox1.Size.Height * 1.2));
            else if (e.Delta < 0)
                pictureBox1.Size = new Size((int)(pictureBox1.Size.Width * 0.8), (int)(pictureBox1.Size.Height * 0.8));

        }

        private void Zoom_Picture_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { this.Close(); }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Preview_Click(object sender, EventArgs e)
        {
            // 1300, 750
            this.Size = new Size(1300, 750);
            panel2.Visible = false;
        }

        private void btn_Photo_Click(object sender, EventArgs e)
        {
            this.Size = new Size(560, 750);
            panel2.Visible = true;
        }
    }
}