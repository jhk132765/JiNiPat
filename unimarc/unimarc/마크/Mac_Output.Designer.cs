﻿namespace WindowsFormsApp1.Mac
{
    partial class Mac_Output
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.isbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.series = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Marc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cb_EncodingType = new System.Windows.Forms.ComboBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.cb_list = new System.Windows.Forms.ComboBox();
            this.btn_file_save = new System.Windows.Forms.Button();
            this.cb_years = new System.Windows.Forms.ComboBox();
            this.cb_state = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.isbn,
            this.book_name,
            this.series,
            this.author,
            this.book_comp,
            this.price,
            this.Marc});
            this.dataGridView1.Location = new System.Drawing.Point(12, 58);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1273, 600);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // isbn
            // 
            this.isbn.HeaderText = "isbn";
            this.isbn.Name = "isbn";
            this.isbn.Width = 110;
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 300;
            // 
            // series
            // 
            this.series.HeaderText = "총서명";
            this.series.Name = "series";
            this.series.Width = 200;
            // 
            // author
            // 
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            this.book_comp.Width = 150;
            // 
            // price
            // 
            this.price.HeaderText = "정가";
            this.price.Name = "price";
            this.price.Width = 70;
            // 
            // Marc
            // 
            this.Marc.HeaderText = "마크";
            this.Marc.Name = "Marc";
            this.Marc.Width = 300;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cb_EncodingType);
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.cb_list);
            this.panel1.Controls.Add(this.btn_file_save);
            this.panel1.Controls.Add(this.cb_years);
            this.panel1.Controls.Add(this.cb_state);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1273, 40);
            this.panel1.TabIndex = 10;
            // 
            // cb_EncodingType
            // 
            this.cb_EncodingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_EncodingType.FormattingEnabled = true;
            this.cb_EncodingType.Location = new System.Drawing.Point(737, 9);
            this.cb_EncodingType.Name = "cb_EncodingType";
            this.cb_EncodingType.Size = new System.Drawing.Size(81, 20);
            this.cb_EncodingType.TabIndex = 8;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(919, 8);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 7;
            this.btn_close.Text = "닫   기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // cb_list
            // 
            this.cb_list.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_list.FormattingEnabled = true;
            this.cb_list.Location = new System.Drawing.Point(308, 9);
            this.cb_list.Name = "cb_list";
            this.cb_list.Size = new System.Drawing.Size(375, 20);
            this.cb_list.TabIndex = 5;
            this.cb_list.SelectedIndexChanged += new System.EventHandler(this.cb_list_SelectedIndexChanged);
            // 
            // btn_file_save
            // 
            this.btn_file_save.Location = new System.Drawing.Point(824, 8);
            this.btn_file_save.Name = "btn_file_save";
            this.btn_file_save.Size = new System.Drawing.Size(89, 23);
            this.btn_file_save.TabIndex = 4;
            this.btn_file_save.Text = "파일로 저장";
            this.btn_file_save.UseVisualStyleBackColor = true;
            this.btn_file_save.Click += new System.EventHandler(this.btn_file_save_Click);
            // 
            // cb_years
            // 
            this.cb_years.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_years.Enabled = false;
            this.cb_years.FormattingEnabled = true;
            this.cb_years.Location = new System.Drawing.Point(126, 9);
            this.cb_years.Name = "cb_years";
            this.cb_years.Size = new System.Drawing.Size(90, 20);
            this.cb_years.TabIndex = 2;
            // 
            // cb_state
            // 
            this.cb_state.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_state.FormattingEnabled = true;
            this.cb_state.Location = new System.Drawing.Point(45, 9);
            this.cb_state.Name = "cb_state";
            this.cb_state.Size = new System.Drawing.Size(75, 20);
            this.cb_state.TabIndex = 2;
            this.cb_state.SelectedIndexChanged += new System.EventHandler(this.cb_state_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "Marc목록";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "구분";
            // 
            // Mac_Output
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 671);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Mac_Output";
            this.Text = "마크 반출";
            this.Load += new System.EventHandler(this.Mac_Output_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn series;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Marc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_file_save;
        private System.Windows.Forms.ComboBox cb_state;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_years;
        private System.Windows.Forms.ComboBox cb_list;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.ComboBox cb_EncodingType;
    }
}