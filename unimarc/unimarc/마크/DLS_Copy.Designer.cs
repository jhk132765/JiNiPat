﻿namespace WindowsFormsApp1.Mac
{
    partial class DLS_Copy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DLS_Copy));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ISBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Check = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.chk_RemoveBrit = new System.Windows.Forms.CheckBox();
            this.chk_spChar = new System.Windows.Forms.CheckBox();
            this.btn_ApplyFilter = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Reflesh008 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rBtn_ISBN = new System.Windows.Forms.RadioButton();
            this.btn_Search = new System.Windows.Forms.Button();
            this.rBtn_BookName = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Connect = new System.Windows.Forms.Button();
            this.lbl_PW = new System.Windows.Forms.Label();
            this.lbl_Area = new System.Windows.Forms.Label();
            this.lbl_ID = new System.Windows.Forms.Label();
            this.lbl_Client = new System.Windows.Forms.Label();
            this.tb_SearchClient = new System.Windows.Forms.TextBox();
            this.btn_Close = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_Back = new System.Windows.Forms.Button();
            this.btn_Forward = new System.Windows.Forms.Button();
            this.tb_URL = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "납품처명";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(364, 734);
            this.panel1.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.dataGridView1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 103);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(362, 629);
            this.panel8.TabIndex = 5;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Book_name,
            this.ISBN,
            this.Check});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 31;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(362, 629);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // Book_name
            // 
            this.Book_name.HeaderText = "도서명";
            this.Book_name.Name = "Book_name";
            this.Book_name.Width = 140;
            // 
            // ISBN
            // 
            this.ISBN.HeaderText = "ISBN";
            this.ISBN.Name = "ISBN";
            // 
            // Check
            // 
            this.Check.HeaderText = "Y/N";
            this.Check.Name = "Check";
            this.Check.Width = 70;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.chk_RemoveBrit);
            this.panel5.Controls.Add(this.chk_spChar);
            this.panel5.Controls.Add(this.btn_ApplyFilter);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 68);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(362, 35);
            this.panel5.TabIndex = 4;
            // 
            // chk_RemoveBrit
            // 
            this.chk_RemoveBrit.AutoSize = true;
            this.chk_RemoveBrit.Location = new System.Drawing.Point(122, 9);
            this.chk_RemoveBrit.Name = "chk_RemoveBrit";
            this.chk_RemoveBrit.Size = new System.Drawing.Size(128, 16);
            this.chk_RemoveBrit.TabIndex = 3;
            this.chk_RemoveBrit.Text = "괄호안의 문자 제거";
            this.chk_RemoveBrit.UseVisualStyleBackColor = true;
            // 
            // chk_spChar
            // 
            this.chk_spChar.AutoSize = true;
            this.chk_spChar.Location = new System.Drawing.Point(10, 9);
            this.chk_spChar.Name = "chk_spChar";
            this.chk_spChar.Size = new System.Drawing.Size(100, 16);
            this.chk_spChar.TabIndex = 4;
            this.chk_spChar.Text = "특수문자 제거";
            this.chk_spChar.UseVisualStyleBackColor = true;
            // 
            // btn_ApplyFilter
            // 
            this.btn_ApplyFilter.Location = new System.Drawing.Point(278, 5);
            this.btn_ApplyFilter.Name = "btn_ApplyFilter";
            this.btn_ApplyFilter.Size = new System.Drawing.Size(75, 24);
            this.btn_ApplyFilter.TabIndex = 5;
            this.btn_ApplyFilter.Text = "필터적용";
            this.btn_ApplyFilter.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btn_Reflesh008);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.rBtn_ISBN);
            this.panel3.Controls.Add(this.btn_Search);
            this.panel3.Controls.Add(this.rBtn_BookName);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 33);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(362, 35);
            this.panel3.TabIndex = 4;
            // 
            // btn_Reflesh008
            // 
            this.btn_Reflesh008.BackColor = System.Drawing.SystemColors.WindowText;
            this.btn_Reflesh008.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Reflesh008.BackgroundImage")));
            this.btn_Reflesh008.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Reflesh008.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_Reflesh008.Location = new System.Drawing.Point(327, 0);
            this.btn_Reflesh008.Name = "btn_Reflesh008";
            this.btn_Reflesh008.Size = new System.Drawing.Size(33, 33);
            this.btn_Reflesh008.TabIndex = 208;
            this.btn_Reflesh008.UseVisualStyleBackColor = false;
            this.btn_Reflesh008.Click += new System.EventHandler(this.btn_Reflesh008_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "검색조건";
            // 
            // rBtn_ISBN
            // 
            this.rBtn_ISBN.AutoSize = true;
            this.rBtn_ISBN.Checked = true;
            this.rBtn_ISBN.Location = new System.Drawing.Point(184, 9);
            this.rBtn_ISBN.Name = "rBtn_ISBN";
            this.rBtn_ISBN.Size = new System.Drawing.Size(51, 16);
            this.rBtn_ISBN.TabIndex = 4;
            this.rBtn_ISBN.TabStop = true;
            this.rBtn_ISBN.Text = "ISBN";
            this.rBtn_ISBN.UseVisualStyleBackColor = true;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(241, 6);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(75, 23);
            this.btn_Search.TabIndex = 2;
            this.btn_Search.Text = "검    색";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // rBtn_BookName
            // 
            this.rBtn_BookName.AutoSize = true;
            this.rBtn_BookName.Location = new System.Drawing.Point(97, 9);
            this.rBtn_BookName.Name = "rBtn_BookName";
            this.rBtn_BookName.Size = new System.Drawing.Size(59, 16);
            this.rBtn_BookName.TabIndex = 4;
            this.rBtn_BookName.Text = "도서명";
            this.rBtn_BookName.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btn_Connect);
            this.panel2.Controls.Add(this.lbl_PW);
            this.panel2.Controls.Add(this.lbl_Area);
            this.panel2.Controls.Add(this.lbl_ID);
            this.panel2.Controls.Add(this.lbl_Client);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.tb_SearchClient);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(362, 33);
            this.panel2.TabIndex = 3;
            // 
            // btn_Connect
            // 
            this.btn_Connect.Location = new System.Drawing.Point(278, 5);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Size = new System.Drawing.Size(75, 23);
            this.btn_Connect.TabIndex = 5;
            this.btn_Connect.Text = "접    속";
            this.btn_Connect.UseVisualStyleBackColor = true;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            // 
            // lbl_PW
            // 
            this.lbl_PW.AutoSize = true;
            this.lbl_PW.Location = new System.Drawing.Point(297, 37);
            this.lbl_PW.Name = "lbl_PW";
            this.lbl_PW.Size = new System.Drawing.Size(23, 12);
            this.lbl_PW.TabIndex = 3;
            this.lbl_PW.Text = "PW";
            // 
            // lbl_Area
            // 
            this.lbl_Area.AutoSize = true;
            this.lbl_Area.Location = new System.Drawing.Point(97, 37);
            this.lbl_Area.Name = "lbl_Area";
            this.lbl_Area.Size = new System.Drawing.Size(31, 12);
            this.lbl_Area.TabIndex = 3;
            this.lbl_Area.Text = "Area";
            // 
            // lbl_ID
            // 
            this.lbl_ID.AutoSize = true;
            this.lbl_ID.Location = new System.Drawing.Point(185, 37);
            this.lbl_ID.Name = "lbl_ID";
            this.lbl_ID.Size = new System.Drawing.Size(16, 12);
            this.lbl_ID.TabIndex = 3;
            this.lbl_ID.Text = "ID";
            // 
            // lbl_Client
            // 
            this.lbl_Client.AutoSize = true;
            this.lbl_Client.Location = new System.Drawing.Point(7, 37);
            this.lbl_Client.Name = "lbl_Client";
            this.lbl_Client.Size = new System.Drawing.Size(37, 12);
            this.lbl_Client.TabIndex = 3;
            this.lbl_Client.Text = "Client";
            // 
            // tb_SearchClient
            // 
            this.tb_SearchClient.Location = new System.Drawing.Point(65, 6);
            this.tb_SearchClient.Name = "tb_SearchClient";
            this.tb_SearchClient.Size = new System.Drawing.Size(198, 21);
            this.tb_SearchClient.TabIndex = 1;
            this.tb_SearchClient.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_SearchClient_KeyDown);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(820, 5);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 2;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(364, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(964, 734);
            this.panel4.TabIndex = 2;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.webBrowser1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 35);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(962, 697);
            this.panel7.TabIndex = 7;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(962, 697);
            this.webBrowser1.TabIndex = 5;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btn_Back);
            this.panel6.Controls.Add(this.btn_Forward);
            this.panel6.Controls.Add(this.tb_URL);
            this.panel6.Controls.Add(this.btn_Close);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(962, 35);
            this.panel6.TabIndex = 6;
            // 
            // btn_Back
            // 
            this.btn_Back.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Back.Location = new System.Drawing.Point(10, 5);
            this.btn_Back.Name = "btn_Back";
            this.btn_Back.Size = new System.Drawing.Size(43, 23);
            this.btn_Back.TabIndex = 1;
            this.btn_Back.Text = "<<";
            this.btn_Back.UseVisualStyleBackColor = true;
            this.btn_Back.Click += new System.EventHandler(this.btn_Back_Click);
            // 
            // btn_Forward
            // 
            this.btn_Forward.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Forward.Location = new System.Drawing.Point(59, 5);
            this.btn_Forward.Name = "btn_Forward";
            this.btn_Forward.Size = new System.Drawing.Size(43, 23);
            this.btn_Forward.TabIndex = 1;
            this.btn_Forward.Text = ">>";
            this.btn_Forward.UseVisualStyleBackColor = true;
            this.btn_Forward.Click += new System.EventHandler(this.btn_Forward_Click);
            // 
            // tb_URL
            // 
            this.tb_URL.Location = new System.Drawing.Point(108, 6);
            this.tb_URL.Name = "tb_URL";
            this.tb_URL.Size = new System.Drawing.Size(706, 21);
            this.tb_URL.TabIndex = 0;
            this.tb_URL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_URL_KeyDown);
            // 
            // DLS_Copy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1328, 734);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Name = "DLS_Copy";
            this.Text = "DLS 복본조사";
            this.Load += new System.EventHandler(this.DLS_Copy_Load);
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Close;
        public System.Windows.Forms.TextBox tb_SearchClient;
        public System.Windows.Forms.Label lbl_PW;
        public System.Windows.Forms.Label lbl_ID;
        public System.Windows.Forms.Label lbl_Client;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton rBtn_ISBN;
        private System.Windows.Forms.RadioButton rBtn_BookName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.DataGridViewTextBoxColumn Book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISBN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Check;
        public System.Windows.Forms.Label lbl_Area;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox tb_URL;
        private System.Windows.Forms.Button btn_Back;
        private System.Windows.Forms.Button btn_Forward;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btn_Reflesh008;
        private System.Windows.Forms.Button btn_Connect;
        private System.Windows.Forms.CheckBox chk_RemoveBrit;
        private System.Windows.Forms.Button btn_ApplyFilter;
        private System.Windows.Forms.CheckBox chk_spChar;
        private System.Windows.Forms.Panel panel8;
    }
}