﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelTest;

namespace UniMarc.마크
{
    public partial class Marc_Preview : Form
    {
        public string isbn;
        AddMarc am;
        Marc mac;

        public Marc_Preview()
        {
            InitializeComponent();
        }

        private void Mac_Preview_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate("https://www.aladin.co.kr/");
            webBrowser2.Navigate("https://www.nl.go.kr/");
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            string Search = isbn;

            Aladin(Search);
            NationalLibrary(Search);
            Kolis(Search);
        }

        /// <summary>
        /// 알라딘 접속
        /// </summary>
        private void Aladin(string Search)
        {
            Delay(3000);

            HtmlElementCollection search = webBrowser1.Document.GetElementsByTagName("input");
            foreach (HtmlElement SearchBox in search)
            {
                if (SearchBox.Id == "SearchWord")
                    SearchBox.InvokeMember("click");
            }

            webBrowser1.Document.GetElementById("SearchWord").SetAttribute("value", Search);

            Delay(500);

            HtmlElementCollection button = webBrowser1.Document.GetElementsByTagName("input");
            foreach (HtmlElement SearchButton in button)
            {
                if (SearchButton.GetAttribute("className") == "searchBtn")
                    SearchButton.InvokeMember("click");
            }
        }

        /// <summary>
        /// 국립중앙도서관
        /// </summary>
        private void NationalLibrary(string Search)
        {
            Delay(3000);

            webBrowser2.Document.GetElementById("main_input-text").SetAttribute("value", Search);

            Delay(500);
            
            HtmlElementCollection button = webBrowser2.Document.GetElementsByTagName("button");
            foreach (HtmlElement SearchButton in button)
            {
                if (SearchButton.GetAttribute("className") == "btn-search")
                    SearchButton.InvokeMember("click");
            }
        }

        /// <summary>
        /// 국중 코리스넷
        /// </summary>
        /// <param name="Search"></param>
        private void Kolis(string Search)
        {
            string url = string.Format("https://www.nl.go.kr/kolisnet/search/searchResultAllList.do?tab=ALL" +
                "&historyYn=Y" +
                "&keywordType1=total" +
                "&keyword1={0}" +
                "&bookFilter=BKGM" +
                "&bookFilter=YON" +
                "&bookFilter=BKDM" +
                "&bookFilter=NK" +
                "&bookFilter=NP" +
                "&bookFilter=OT", Search);
            webBrowser3.Navigate(url);
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (mac != null)
                mac.richTextBox1.Text = richTextBox1.Text;
            else if (am != null)
                am.richTextBox1.Text = richTextBox1.Text;
        }

        /// <summary>
        /// 지연시키는 함수
        /// </summary>
        /// <param name="ms">1000 = 1초</param>
        void Delay(int ms)
        {
            DateTime dateTimeNow = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, ms);
            DateTime dateTimeAdd = dateTimeNow.Add(duration);
            while (dateTimeAdd >= dateTimeNow)
            {
                Application.DoEvents();
                dateTimeNow = DateTime.Now;
            }
            return;
        }
    }
}
