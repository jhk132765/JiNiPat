﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using UniMarc.마크;

namespace WindowsFormsApp1.Mac
{
    public partial class Set_Macro : Form
    {
        Main main;
        Marc_Plan mp;
        Helper_DB db = new Helper_DB();
        DataGridView TargetGrid;
        string compidx = UniMarc.Properties.Settings.Default.compidx;
        public string[] ViewMarcArray { get; set; }
        public string[] RunCode;

        public Set_Macro(Main _main)
        {
            InitializeComponent();
            main = _main;
        }

        public Set_Macro(Marc_Plan _mp, DataGridView mpGrid)
        {
            InitializeComponent();
            mp = _mp;
            TargetGrid = mpGrid;
        }

        public Set_Macro()
        {
            InitializeComponent();
        }

        private void Set_Macro_Load(object sender, EventArgs e)
        {
            db.DBcon();

            string cmd = string.Format("SELECT `listname` FROM `Comp_SaveMacro` WHERE `compidx` = \"{0}\";", compidx);
            string res = db.DB_Send_CMD_Search(cmd);
            foreach (string l in res.Split('|'))
            {
                if (l == "")
                    continue;
                cb_SearchList.Items.Add(l);
            }

            cmd = String.Format("SELECT `idx`, `Tag`, `Macro`, `RunCode` FROM `SetMacro`;");
            db.DB_Send_CMD_Search_ApplyGrid(cmd, MacroGrid);
            MacroGrid.Sort(TagNum, ListSortDirection.Ascending);
            RunCode = new string[MacroGrid.RowCount];
            for (int a = 0; a < MacroGrid.RowCount; a++)
            {
                RunCode[a] = MacroGrid.Rows[a].Cells["Check"].Value.ToString();
            }

            MakeGridCheckReload();
        }

        /// <summary>
        /// 표의 체크박스 초기화
        /// </summary>
        void MakeGridCheckReload()
        {
            for (int a = 0; a < MacroGrid.Rows.Count; a++)
            {
                MacroGrid.Rows[a].Cells["Check"].Value = "F";
            }
        }

        private void cb_SearchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string listname = cb_SearchList.SelectedItem.ToString();
            string cmd = string.Format("SELECT `Macroidx` FROM `Comp_SaveMacro` WHERE `compidx` = \"{0}\" AND `listname` = \"{1}\";", compidx, listname);
            string res = db.DB_Send_CMD_Search(cmd).Replace("|", "");

            MakeGridCheckReload();

            foreach (string l in res.Split('^'))
            {
                for (int a = 0; a < MacroGrid.Rows.Count; a++)
                {
                    if (MacroGrid.Rows[a].Cells["idx"].Value.ToString() == l)
                    {
                        MacroGrid.Rows[a].Cells["Check"].Value = "T";
                    }
                }
            }
        }

        private void btn_ApplyMacro_Click(object sender, EventArgs e)
        {
            Macro_Gudu macro = new Macro_Gudu(db);
            List<string> idxArray = new List<string>();
            List<string> TagArray = new List<string>();
            List<string> RunCode = new List<string>();

            for (int a = 0; a < MacroGrid.Rows.Count; a++)
            {
                if (MacroGrid.Rows[a].Cells["Check"].Value.ToString().Contains("T"))
                {
                    idxArray.Add(MacroGrid.Rows[a].Cells["idx"].Value.ToString());
                    TagArray.Add(MacroGrid.Rows[a].Cells["TagNum"].Value.ToString());
                    RunCode.Add(this.RunCode[a]);
                }
            }

            int count = 0;
            for (int a = 0; a < TargetGrid.Rows.Count; a++)
            {
                if (TargetGrid.Rows[a].Cells["marc"].Value.ToString() == "" &&
                    TargetGrid.Rows[a].Cells["marc"].Value == null)
                    continue;

                if (TargetGrid.Rows[a].Cells["colCheck"].Value.ToString() != "T")
                    continue;

                TargetGrid.Rows[a].Cells["marc"].Value = macro.MacroMarc(ViewMarcArray[count], idxArray, TagArray, RunCode);
                count++;
            }
            MessageBox.Show("적용되었습니다!");
        }

        private void btn_AddList_Click(object sender, EventArgs e)
        {
            Skill_Search_Text sst = new Skill_Search_Text();

            string value = "";
            if (sst.InputBox("생성할 목록명을 입력해주세요.", "목록 생성", ref value) == DialogResult.OK)
            {
                string user = UniMarc.Properties.Settings.Default.User;
                string compidx = UniMarc.Properties.Settings.Default.compidx;
                string MacroIndex = "";
                for (int a = 0; a < MacroGrid.Rows.Count; a++)
                {
                    if (MacroGrid.Rows[a].Cells["Check"].Value.ToString() == "T")
                        MacroIndex += MacroGrid.Rows[a].Cells["idx"].Value.ToString() + "^";
                }
                MacroIndex.TrimEnd('^');

                string Table = "Comp_SaveMacro";
                if (db.DB_Send_CMD_Search(string.Format("SELECT * FROM {0} WHERE `compidx` = \"{1}\" AND `{2}` = \"{3}\"", Table, compidx, "listname", value)).Length > 2)
                {
                    MessageBox.Show("중복된 목록명이 있습니다!");
                    return;
                }
                string[] Insert_Tbl = { "compidx", "listname", "user", "Macroidx" };
                string[] Insert_Col = { compidx, value, user, MacroIndex };

                string cmd = db.DB_INSERT(Table, Insert_Tbl, Insert_Col);
                db.DB_Send_CMD_reVoid(cmd);
                cb_SearchList.Items.Add(value);
            }
        }

        private void btn_ListSave_Click(object sender, EventArgs e)
        {
            string MacroIndex = "";
            string compidx = UniMarc.Properties.Settings.Default.compidx;
            string listname = cb_SearchList.Text;
            for (int a = 0; a < MacroGrid.Rows.Count; a++)
            {
                if (MacroGrid.Rows[a].Cells["Check"].Value.ToString() == "T")
                    MacroIndex += MacroGrid.Rows[a].Cells["idx"].Value.ToString() + "^";
            }
            MacroIndex.TrimEnd('^');

            string Table = "Comp_SaveMacro";
            string[] Search_T = { "compidx", "listname" };
            string[] Search_C = {  compidx ,  listname  };

            if (db.DB_Send_CMD_Search(db.More_DB_Search(Table, Search_T, Search_C)).Length < 2)
            {
                MessageBox.Show("선택된 목록이 없습니다!");
                return;
            }

            string[] Update_T = { "Macroidx" };
            string[] Update_C = { MacroIndex };

            db.DB_Send_CMD_reVoid(db.More_Update(Table, Update_T, Update_C, Search_T, Search_C));
            MessageBox.Show("저장되었습니다!");
        }

        private void btn_ListRemove_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes != MessageBox.Show("현재 목록을 삭제하시겠습니까?", "목록 삭제", MessageBoxButtons.YesNo))
                return;

            string compidx = UniMarc.Properties.Settings.Default.compidx;
            string listname = cb_SearchList.Text;

            string Table = "Comp_SaveMacro";
            string[] Search_T = { "compidx", "listname" };
            string[] Search_C = { compidx, listname };

            if (db.DB_Send_CMD_Search(db.More_DB_Search(Table, Search_T, Search_C)).Length < 2)
            {
                MessageBox.Show("선택된 목록이 없습니다!");
                return;
            }

            db.DB_Send_CMD_reVoid(db.DB_Delete_More_term(Table, "compidx", compidx, Search_T, Search_C));
            MessageBox.Show("삭제되었습니다!");
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MacroGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            if (e.ColumnIndex == 3)
                if (MacroGrid.Rows[e.RowIndex].Cells["Check"].Value.ToString().Contains("T"))
                {
                    MacroGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "F";
                    MacroGrid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                }
                else
                {
                    MacroGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "T";
                    MacroGrid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Yellow;
                }
        }
    }
}
