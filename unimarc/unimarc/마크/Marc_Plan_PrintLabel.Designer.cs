﻿
namespace UniMarc.마크
{
    partial class Marc_Plan_PrintLabel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_Search = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_Empty = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.cb_TextFont = new System.Windows.Forms.ComboBox();
            this.tb_TextSize = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.chk_C = new System.Windows.Forms.CheckBox();
            this.chk_V = new System.Windows.Forms.CheckBox();
            this.tb_Left = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_Top = new System.Windows.Forms.TextBox();
            this.tb_Width = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_SeroGap = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_Height = new System.Windows.Forms.TextBox();
            this.tb_Sero = new System.Windows.Forms.TextBox();
            this.tb_GaroGap = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_Garo = new System.Windows.Forms.TextBox();
            this.tb_ListName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Apply = new System.Windows.Forms.Button();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tb_Search);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.btn_Delete);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.btn_Empty);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(746, 33);
            this.panel1.TabIndex = 0;
            // 
            // tb_Search
            // 
            this.tb_Search.Location = new System.Drawing.Point(52, 5);
            this.tb_Search.Name = "tb_Search";
            this.tb_Search.Size = new System.Drawing.Size(387, 21);
            this.tb_Search.TabIndex = 4;
            this.tb_Search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_Search_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "검   색";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(599, 4);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(66, 23);
            this.btn_Save.TabIndex = 1;
            this.btn_Save.Text = "저    장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(525, 4);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(66, 23);
            this.btn_Delete.TabIndex = 1;
            this.btn_Delete.Text = "삭    제";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(673, 4);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(66, 23);
            this.btn_Close.TabIndex = 1;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_Empty
            // 
            this.btn_Empty.Location = new System.Drawing.Point(451, 4);
            this.btn_Empty.Name = "btn_Empty";
            this.btn_Empty.Size = new System.Drawing.Size(66, 23);
            this.btn_Empty.TabIndex = 1;
            this.btn_Empty.Text = "비 우 기";
            this.btn_Empty.UseVisualStyleBackColor = true;
            this.btn_Empty.Click += new System.EventHandler(this.btn_Empty_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.name,
            this.user});
            this.dataGridView1.Location = new System.Drawing.Point(12, 50);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(440, 196);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.cb_TextFont);
            this.panel2.Controls.Add(this.tb_TextSize);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.chk_C);
            this.panel2.Controls.Add(this.chk_V);
            this.panel2.Controls.Add(this.tb_Left);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.tb_Top);
            this.panel2.Controls.Add(this.tb_Width);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.tb_SeroGap);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.tb_Height);
            this.panel2.Controls.Add(this.tb_Sero);
            this.panel2.Controls.Add(this.tb_GaroGap);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.tb_Garo);
            this.panel2.Controls.Add(this.tb_ListName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(458, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(300, 196);
            this.panel2.TabIndex = 4;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(10, 147);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 12);
            this.label18.TabIndex = 106;
            this.label18.Text = "글자체";
            // 
            // cb_TextFont
            // 
            this.cb_TextFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_TextFont.FormattingEnabled = true;
            this.cb_TextFont.Location = new System.Drawing.Point(56, 143);
            this.cb_TextFont.Name = "cb_TextFont";
            this.cb_TextFont.Size = new System.Drawing.Size(72, 20);
            this.cb_TextFont.TabIndex = 102;
            // 
            // tb_TextSize
            // 
            this.tb_TextSize.Location = new System.Drawing.Point(229, 143);
            this.tb_TextSize.Name = "tb_TextSize";
            this.tb_TextSize.Size = new System.Drawing.Size(30, 21);
            this.tb_TextSize.TabIndex = 103;
            this.tb_TextSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(170, 147);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 12);
            this.label20.TabIndex = 101;
            this.label20.Text = "글자크기";
            // 
            // chk_C
            // 
            this.chk_C.AutoSize = true;
            this.chk_C.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_C.Location = new System.Drawing.Point(28, 169);
            this.chk_C.Name = "chk_C";
            this.chk_C.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chk_C.Size = new System.Drawing.Size(71, 16);
            this.chk_C.TabIndex = 105;
            this.chk_C.Text = "복본(C.)";
            this.chk_C.UseVisualStyleBackColor = true;
            // 
            // chk_V
            // 
            this.chk_V.AutoSize = true;
            this.chk_V.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_V.Location = new System.Drawing.Point(172, 170);
            this.chk_V.Name = "chk_V";
            this.chk_V.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chk_V.Size = new System.Drawing.Size(70, 16);
            this.chk_V.TabIndex = 104;
            this.chk_V.Text = "볼륨(V.)";
            this.chk_V.UseVisualStyleBackColor = true;
            // 
            // tb_Left
            // 
            this.tb_Left.Location = new System.Drawing.Point(43, 35);
            this.tb_Left.Name = "tb_Left";
            this.tb_Left.Size = new System.Drawing.Size(56, 21);
            this.tb_Left.TabIndex = 85;
            this.tb_Left.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(10, 39);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 12);
            this.label23.TabIndex = 77;
            this.label23.Text = "좌측";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(101, 39);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 12);
            this.label22.TabIndex = 81;
            this.label22.Text = "mm";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(261, 39);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 12);
            this.label21.TabIndex = 92;
            this.label21.Text = "mm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(170, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 12);
            this.label10.TabIndex = 93;
            this.label10.Text = "위쪽";
            // 
            // tb_Top
            // 
            this.tb_Top.Location = new System.Drawing.Point(203, 35);
            this.tb_Top.Name = "tb_Top";
            this.tb_Top.Size = new System.Drawing.Size(56, 21);
            this.tb_Top.TabIndex = 94;
            this.tb_Top.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_Width
            // 
            this.tb_Width.Location = new System.Drawing.Point(43, 62);
            this.tb_Width.Name = "tb_Width";
            this.tb_Width.Size = new System.Drawing.Size(56, 21);
            this.tb_Width.TabIndex = 95;
            this.tb_Width.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(101, 66);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 12);
            this.label19.TabIndex = 91;
            this.label19.Text = "mm";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(261, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 12);
            this.label11.TabIndex = 87;
            this.label11.Text = "mm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(10, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 12);
            this.label9.TabIndex = 78;
            this.label9.Text = "너비";
            // 
            // tb_SeroGap
            // 
            this.tb_SeroGap.Location = new System.Drawing.Point(229, 89);
            this.tb_SeroGap.Name = "tb_SeroGap";
            this.tb_SeroGap.Size = new System.Drawing.Size(30, 21);
            this.tb_SeroGap.TabIndex = 98;
            this.tb_SeroGap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(170, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 12);
            this.label8.TabIndex = 79;
            this.label8.Text = "높이";
            // 
            // tb_Height
            // 
            this.tb_Height.Location = new System.Drawing.Point(203, 62);
            this.tb_Height.Name = "tb_Height";
            this.tb_Height.Size = new System.Drawing.Size(56, 21);
            this.tb_Height.TabIndex = 96;
            this.tb_Height.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_Sero
            // 
            this.tb_Sero.Location = new System.Drawing.Point(229, 116);
            this.tb_Sero.Name = "tb_Sero";
            this.tb_Sero.Size = new System.Drawing.Size(30, 21);
            this.tb_Sero.TabIndex = 100;
            this.tb_Sero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_GaroGap
            // 
            this.tb_GaroGap.Location = new System.Drawing.Point(69, 89);
            this.tb_GaroGap.Name = "tb_GaroGap";
            this.tb_GaroGap.Size = new System.Drawing.Size(30, 21);
            this.tb_GaroGap.TabIndex = 97;
            this.tb_GaroGap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(101, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 12);
            this.label12.TabIndex = 90;
            this.label12.Text = "mm";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(170, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 12);
            this.label15.TabIndex = 84;
            this.label15.Text = "세로간격";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(101, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 89;
            this.label16.Text = "개";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(170, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 12);
            this.label6.TabIndex = 83;
            this.label6.Text = "세로갯수";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(261, 120);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 88;
            this.label17.Text = "개";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(261, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 12);
            this.label13.TabIndex = 86;
            this.label13.Text = "mm";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(10, 93);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 12);
            this.label14.TabIndex = 82;
            this.label14.Text = "가로간격";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(10, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 12);
            this.label5.TabIndex = 80;
            this.label5.Text = "가로갯수";
            // 
            // tb_Garo
            // 
            this.tb_Garo.Location = new System.Drawing.Point(69, 116);
            this.tb_Garo.Name = "tb_Garo";
            this.tb_Garo.Size = new System.Drawing.Size(30, 21);
            this.tb_Garo.TabIndex = 99;
            this.tb_Garo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_ListName
            // 
            this.tb_ListName.Location = new System.Drawing.Point(56, 8);
            this.tb_ListName.Name = "tb_ListName";
            this.tb_ListName.Size = new System.Drawing.Size(229, 21);
            this.tb_ListName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(10, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "목록명";
            // 
            // btn_Apply
            // 
            this.btn_Apply.Location = new System.Drawing.Point(683, 252);
            this.btn_Apply.Name = "btn_Apply";
            this.btn_Apply.Size = new System.Drawing.Size(75, 23);
            this.btn_Apply.TabIndex = 1;
            this.btn_Apply.Text = "적    용";
            this.btn_Apply.UseVisualStyleBackColor = true;
            this.btn_Apply.Click += new System.EventHandler(this.btn_Apply_Click);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.ReadOnly = true;
            this.idx.Visible = false;
            this.idx.Width = 50;
            // 
            // name
            // 
            this.name.HeaderText = "목록";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 300;
            // 
            // user
            // 
            this.user.HeaderText = "저장자";
            this.user.Name = "user";
            this.user.ReadOnly = true;
            this.user.Width = 80;
            // 
            // Marc_Plan_PrintLabel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 285);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_Apply);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Marc_Plan_PrintLabel";
            this.Load += new System.EventHandler(this.Marc_Plan_PrintLabel_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox tb_Search;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tb_ListName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_Left;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_Top;
        private System.Windows.Forms.TextBox tb_Width;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_SeroGap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_Height;
        private System.Windows.Forms.TextBox tb_Sero;
        private System.Windows.Forms.TextBox tb_GaroGap;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_Garo;
        private System.Windows.Forms.ComboBox cb_TextFont;
        private System.Windows.Forms.TextBox tb_TextSize;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chk_C;
        private System.Windows.Forms.CheckBox chk_V;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btn_Apply;
        private System.Windows.Forms.Button btn_Empty;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn user;
    }
}