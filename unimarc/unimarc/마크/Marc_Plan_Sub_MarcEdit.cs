﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;
using ExcelTest;

namespace UniMarc.마크
{
    public partial class Marc_Plan_Sub_MarcEdit : Form
    {
        string idx;
        public int row;
        public string UserName = "";
        Marc_Plan mp;
        Helper_DB db = new Helper_DB();
        String_Text st = new String_Text();
        Search_Infor si;
        Help008Tag tag008 = new Help008Tag();
        public Marc_Plan_Sub_MarcEdit(Marc_Plan _mp)
        {
            InitializeComponent();
            mp = _mp;
            db.DBcon();
        }
        public Marc_Plan_Sub_MarcEdit(Search_Infor _si)
        {
            InitializeComponent();
            si = _si;
            db.DBcon();
        }

        /// <summary>
        /// 시작전 세팅작업. 필요한 데이터들을 각각의 박스들에 집어넣음.
        /// </summary>
        /// <param name="Marc">0:Marc 1:MarcIdx 2:연번 3:idx 4:ISBN</param>
        public void Init(string[] Marc)
        {
            // 이용자 9
            comboBox1.Items.AddRange(tag008.combo1);

            // 자료형식 3
            comboBox2.Items.AddRange(tag008.combo2);

            // 내용형식 28
            comboBox3.Items.AddRange(tag008.combo3);
            comboBox7.Items.AddRange(tag008.combo3);

            // 문학형식 16
            comboBox4.Items.AddRange(tag008.combo4);

            // 전기 5
            comboBox5.Items.AddRange(tag008.combo5);

            // 언어 36
            comboBox6.Items.AddRange(tag008.combo6);

            richTextBox1.Text = split_Marc(Marc[0]);
            Create_008();
            st.Color_change("▼", richTextBox1);
            st.Color_change("▲", richTextBox1);

            tb_Mcode.Text = Marc[1];
            tb_num.Text = Marc[2];
            idx = Marc[3];

            richTextBox1.Select(0, richTextBox1.Text.Length);
            richTextBox1.SelectionBackColor = Color.Transparent;

            input_picture(Marc[4]);

            SetHistory();
        }

        /// <summary>
        /// 작성자, 작성시각 표출해주는 함수.
        /// (22.02.07 추가) 비고1, 비고2 추가로 표출
        /// </summary>
        public void SetHistory()
        {
            if (mp != null)
            {
                string cmd = string.Format("SELECT `user`, `editDate`, `etc1`, `etc2` FROM `Specs_Marc` WHERE `idx` = \"{0}\"", idx);
                string res = db.DB_Send_CMD_Search(cmd);
                string[] aryResultData = res.Split('|');

                if (aryResultData.Length > 1) {
                    lbl_SaveData.Text = string.Format("[{0}] [{1}]", aryResultData[0], aryResultData[1]);
                    etcBox1.Text = aryResultData[2];
                    etcBox2.Text = aryResultData[3];
                }
            }
            else if (si != null)
            {
                string cmd = string.Format("SELECT `user`, `date`, `비고1`, `비고2` FROM `Marc` WHERE `idx` = \"{0}\"", tb_Mcode.Text);
                string res = db.DB_Send_CMD_Search(cmd);
                string[] aryResultData = res.Split('|');

                if (aryResultData.Length > 1) {
                    lbl_SaveData.Text = string.Format("[{0}] [{1}]", aryResultData[0], aryResultData[1]);
                    etcBox1.Text = aryResultData[2];
                    etcBox2.Text = aryResultData[3];
                }
            }

        }

        /// <summary>
        /// 시작전 저자기호 세팅작업. 필요한 데이터들을 각각의 박스들에 집어넣음.
        /// </summary>
        /// <param name="Symbol_Type">저자기호 배열
        /// 0:FirstAuthor 1:authorType 2:FirstBook 3:divType 4:divNum </param>
        public void SetSymbolType(string[] Symbol_Type)
        {
            #region 저자기호
            AuthorSymbol.Symbol sb = new AuthorSymbol.Symbol();

            cb_FirstAuthor.Items.AddRange(sb.authorBook);
            cb_authorType.Items.AddRange(sb.authorType);
            cb_FirstBook.Items.AddRange(sb.authorFirst);

            cb_FirstAuthor.SelectedItem = Symbol_Type[0];
            cb_authorType.SelectedItem = Symbol_Type[1];
            cb_FirstBook.SelectedItem = Symbol_Type[2];
            #endregion

            #region 분류기호
            string[] divType = { "KDC", "DDC" };
            cb_divType.Items.AddRange(divType);

            string[] divNum = { "4", "5", "6" };
            cb_divNum.Items.AddRange(divNum);

            cb_divType.SelectedItem = Symbol_Type[3];
            cb_divNum.SelectedItem = Symbol_Type[4];
            #endregion
        }

        #region Init_Sub
        void input_picture(string isbn)
        {
            try
            {
                string isbn3 = isbn.Substring(isbn.Length - 3, 3);
                pictureBox1.ImageLocation = "http://image.kyobobook.co.kr/images/book/xlarge/" + isbn3 + "/x" + isbn + ".jpg";
            }
            catch { }
        }

        /// <summary>
        /// 008 각각의 박스에 대입하는 함수
        /// </summary>
        /// <returns></returns>
        public void Create_008()
        {
            string data = text008.Text;

            if (data == "" || data == null) { return; }

            string[] Tag008 = {
                "", "", "", "", "",
                "", "", "", "", "",
                "", "", "", "", "",
                "", "", "", "", ""
            };
            /* 입력일자 발행년유형 발행년1 발행년2 발행국 
             * 삽화표시 이용대상자수준 개별자료형태 내용형식1 내용형식2 
             * 한국대학부호 수정레코드 회의간행물 기념논문집 색인 
             * 목록전거 문학형식 전기 언어 한국정부기관부호 */

            // 사전에 선언된 string배열에 맞는 데이터를 배정.
            int tmp_years = Convert.ToInt32(data.Substring(0, 2));
            int now_years = Convert.ToInt32(DateTime.Now.ToString("yy"));
            string century = "20";

            if (tmp_years > now_years)
                century = "19";

            Tag008[0] = century
                      + data.Substring(0, 6);           // 입력일자 (00-05)
            Tag008[1] = data.Substring(6, 1);           // 발행년유형 (6)
            Tag008[2] = data.Substring(7, 4);           // 발행년1 (07-10)
            Tag008[3] = data.Substring(11, 4);          // 발행년2 (11-14)
            Tag008[4] = data.Substring(15, 3);          // 발행국 (15-17)

            Tag008[5] = data.Substring(18, 4);          // 삽화표시 (18-21)
            Tag008[6] = data.Substring(22, 1);          // 이용대상자수준 (22) v
            Tag008[7] = data.Substring(23, 1);          // 개별자료형태 (23) v
            Tag008[8] = data.Substring(24, 1);          // 내용형식1 (24) v
            Tag008[9] = data.Substring(25, 1);          // 내용형식2 (25) v

            Tag008[10] = data.Substring(26, 2);         // 한국대학부호 (26-27)
            Tag008[11] = data.Substring(28, 1);         // 수정레코드 (28)
            Tag008[12] = data.Substring(29, 1);         // 회의간행물 (29) c
            Tag008[13] = data.Substring(30, 1);         // 기념논문집 (30) c
            Tag008[14] = data.Substring(31, 1);         // 색인 (31)

            Tag008[15] = data.Substring(32, 1);         // 목록전거 (32)
            Tag008[16] = data.Substring(33, 1);         // 문학형식 (33) v
            Tag008[17] = data.Substring(34, 1);         // 전기 (34) v
            Tag008[18] = data.Substring(35, 3);         // 언어 (35-37) v
            Tag008[19] = data.Substring(38, 2);         // 한국정부기관부호 (38-39)

            // 배열에 들어간 데이터로 콤보박스를 꾸미는 작업.
            int year = Convert.ToInt32(Tag008[0].Substring(0, 4));
            int month = Convert.ToInt32(Tag008[0].Substring(4, 2));
            int day = Convert.ToInt32(Tag008[0].Substring(6, 2));
            input_date.Value = new DateTime(year, month, day);      // 입력일자

            comboBox1.SelectedIndex = tag008.User_008(Tag008[6]);   // 이용대상자수준
            comboBox2.SelectedIndex = tag008.DataType_008(Tag008[7]);   // 개별자료형태
            comboBox3.SelectedIndex = tag008.Format_008(Tag008[8]);   // 내용형식1
            comboBox7.SelectedIndex = tag008.Format_008(Tag008[9]);   // 내용형식2
            comboBox4.SelectedIndex = tag008.Literary_008(Tag008[16]);   // 문학형식
            comboBox5.SelectedIndex = tag008.Biography_008(Tag008[17]);   // 전기
            comboBox6.SelectedIndex = tag008.Language_008(Tag008[18]);   // 언어


            checkBox1.Checked = tag008.CheckBox_008(Tag008[12]);    // 회의간행물
            checkBox2.Checked = tag008.CheckBox_008(Tag008[13]);    // 기념논문집

            col008res.Text = Tag008[10];
            gov008res.Text = Tag008[19];

            return;
        }

        /// <summary>
        /// 마크데이터가 있는지 확인하고 메모장으로 출력
        /// </summary>
        /// <param name="row">마크 데이터</param>
        /// <returns></returns>
        string split_Marc(string Marc_data)
        {
            if (Marc_data.Length < 3) return "";

            string result = string.Empty;

            List<string> TagNum = new List<string>();   // 태그번호
            List<string> field = new List<string>();    // 가변길이필드 저장

            // 특수기호 육안으로 확인하기 쉽게 변환
            Marc_data = Marc_data.Replace("", "▼");
            Marc_data = Marc_data.Replace("", "▲");
            Marc_data = Marc_data.Replace("￦", "\\");
            // string leader = Marc_data.Substring(0, 24);

            int startidx = 0;
            string[] data = Marc_data.Substring(24).Split('▲'); // 리더부를 제외한 디렉터리, 가변길이필드 저장

            // List에 필요한 데이터 집어넣는 작업.
            for (int a = 1; a < data.Length - 1; a++)
            {
                TagNum.Add(data[0].Substring(startidx, 3));
                startidx += 12;
                field.Add(data[a] + "▲");
            }

            // List에 들어간 데이터를 메모장에 출력시키는 작업.
            for (int a = 0; a < TagNum.Count; a++)
            {
                string res = TagNum[a];
                if (TagNum[a] == "008")
                {
                    text008.Text = field[a].Replace("▲", "");
                    continue;
                }
                else { }
                if (field[a].IndexOf("▼") == -1)
                {
                    res += "\t  \t" + field[a];
                }
                else
                {
                    string temp = field[a].Insert(2, "\t");
                    res += "\t" + temp;
                }
                result += res + "\n";
            }
            return result;
        }
        #endregion

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {

            string name = ((CheckBox)sender).Name;
            if (((CheckBox)sender).Checked == true)
            {
                switch (name)
                {
                    case "checkBox1":
                        ((CheckBox)sender).Text = "회의간행물o";
                        break;
                    case "checkBox2":
                        ((CheckBox)sender).Text = "기념논문집o";
                        break;
                }
            }
            else if (((CheckBox)sender).Checked == false)
            {
                switch (name)
                {
                    case "checkBox1":
                        ((CheckBox)sender).Text = "회의간행물x";
                        break;
                    case "checkBox2":
                        ((CheckBox)sender).Text = "기념논문집x";
                        break;
                }
            }
            switch (name)
            {
                case "checkBox1":
                    Publication(checkBox1.Checked, 29);
                    break;
                case "checkBox2":
                    Publication(checkBox2.Checked, 30);
                    break;
            }
        }
        #region checkBox_Checked_Sub
        void Publication(bool check, int idx)
        {
            if (text008.Text == "") { return; }
            char[] ArrayChar = text008.Text.ToCharArray();
            if (!check) { ArrayChar[idx] = '0'; }
            else if (check) { ArrayChar[idx] = '1'; }
            text008.Text = new string(ArrayChar);
        }
        #endregion

        private void col008res_TextChanged(object sender, EventArgs e)
        {
            if (text008.Text.Length < 3) { return; }
            string text = text008.Text;
            string iText = ((Label)sender).Text;
            if (iText.Length < 1) { return; }
            char[] textArray = text.ToCharArray();
            char[] inputArray = iText.ToCharArray();

            switch (((Label)sender).Name)
            {
                case "col008res":
                    textArray[26] = inputArray[0];
                    textArray[27] = inputArray[1];
                    break;
                case "gov008res":
                    textArray[38] = inputArray[0];
                    textArray[39] = inputArray[1];
                    break;
            }
            text = new string(textArray);
            text008.Text = text;
        }

        private void text008col_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string tbName = ((TextBox)sender).Name;
                string call = ((TextBox)sender).Text;
                Helper008 helper = new Helper008(this);
                helper.Who_Call(tbName, call);
                helper.Show();
            }
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (text008.Text.Length < 3) { return; }

            string text = text008.Text;
            string comboName = ((ComboBox)sender).Name;
            int comboIdx = ((ComboBox)sender).SelectedIndex;

            text008.Text = tag008.Combo_Change(text, comboName, comboIdx);
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            string oriMarc = st.made_Ori_marc(richTextBox1);

            if (mp != null)
                Save_mp(oriMarc);

            if (si != null)
                Save_si(oriMarc);
        }
        #region Save_Sub

        private void Save_mp(string oriMarc)
        {
            // 등록번호 분류기호 저자기호 볼륨 복본
            // 별치     총서명  총서번호  저자 출판사
            // 정가    ISBN
            string[] Search_Tag = {
                "049l", "090a", "090b", "049v", "049c",
                "049f", "440a", "440v", "245d", "260b",
                "950b", "020a"
            };
            string[] SearchBookTag = st.Take_Tag(oriMarc, Search_Tag);

            string etc1 = etcBox1.Text;
            string etc2 = etcBox2.Text;

            // 도서명 (본서명 = 대등서명 : 부서명)
            string[] BookTag = { "245a", "245x", "245b" };
            string[] BookNameTag = st.Take_Tag(oriMarc, BookTag);
            string BookName = mk_BookName(BookNameTag);

            string Table = "Specs_Marc";

            string[] Search_Col = { "idx" };
            string[] Search_data = { idx };

            string[] Update_Col = {
                "marc", "book_name", "etc1", "etc2",
                "r_num", "class_symbol", "author_symbol", "prefix", "s_book_name1",
                "s_book_num1", "author", "book_comp", "price", "ISBN"
            };
            string[] Update_data = {
                oriMarc, BookName, etc1, etc2,
                SearchBookTag[0], SearchBookTag[1], SearchBookTag[2], SearchBookTag[5], SearchBookTag[6],
                SearchBookTag[7], SearchBookTag[8], SearchBookTag[9], SearchBookTag[10], SearchBookTag[11]
            };

            string cmd = db.More_Update(Table, Update_Col, Update_data, Search_Col, Search_data);

            db.DB_Send_CMD_reVoid(cmd);
            InputBookData(SearchBookTag);

            MessageBox.Show("저장되었습니다!");

            mp.dataGridView1.Rows[row].Cells["book_name"].Value = BookName;
            mp.dataGridView1.Rows[row].Cells["marc"].Value = oriMarc;
        }

        private void Save_si(string oriMarc)
        {
            string today = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string user = UserName;
            string MarcIndex = tb_Mcode.Text;

            string etc1 = etcBox1.Text;
            string etc2 = etcBox2.Text;
            string grade = cb_grade.SelectedIndex.ToString();

            string Table = "Marc";

            string[] Search_Col = { "idx" };
            string[] Search_data = { MarcIndex };

            string[] Update_Col = {
                "marc", "marc_chk", "marc_chk2", "date", "user",
                "비고1", "비고2", "grade"
            };
            string[] Update_data = {
                oriMarc, "1", "0", today, user,
                etc1, etc2, grade
            };

            string res = Sub_marc_chk(MarcIndex);
            switch (res)
            {
                case "0":
                    Update_Col[0] = "marc1";
                    Update_Col[1] = "marc_chk1";
                    Update_Col[2] = "marc_chk";
                    break;
                case "1":
                    Update_Col[0] = "marc2";
                    Update_Col[1] = "marc_chk2";
                    Update_Col[2] = "marc_chk1";
                    break;
                case "2":    
                    Update_Col[0] = "marc";
                    Update_Col[1] = "marc_chk";
                    Update_Col[2] = "marc_chk2";
                    break;
                default:
                    break;
            }

            string Ucmd = db.More_Update(Table, Update_Col, Update_data, Search_Col, Search_data);
            db.DB_Send_CMD_reVoid(Ucmd);

            MessageBox.Show("저장되었습니다!");

            si.dataGridView1.Rows[row].Cells["Marc"].Value = oriMarc;
            si.dataGridView1.Rows[row].Cells["User"].Value = user;
            si.dataGridView1.Rows[row].Cells["date"].Value = today;
        }

        private string Sub_marc_chk(string idx)
        {
            string Area = "`marc_chk`, `marc_chk1`, `marc_chk2`";
            string cmd = db.DB_Select_Search(Area, "Marc", "idx", idx);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] chk_ary = db_res.Split('|');

            for (int a = 0; a < chk_ary.Length; a++)
            {
                if (chk_ary[a] == "1")
                {
                    return a.ToString();
                }
            }
            return "0";
        }

        /// <summary>
        /// 추출해낸 태그값을 Grid에 적용
        /// </summary>
        /// <param name="SearchTag">
        /// 등록번호 분류기호 저자기호 볼륨 복본<br/>
        /// 별치     총서명  총서번호  저자 출판사<br/>
        /// 정가     ISBN
        /// </param>
        private void InputBookData(string[] SearchTag)
        {
            mp.dataGridView1.Rows[row].Cells["reg_num"     ].Value = SearchTag[0];
            mp.dataGridView1.Rows[row].Cells["class_code"  ].Value = SearchTag[1];
            mp.dataGridView1.Rows[row].Cells["author_code" ].Value = SearchTag[2];
            mp.dataGridView1.Rows[row].Cells["volume"      ].Value = SearchTag[3];
            mp.dataGridView1.Rows[row].Cells["copy"        ].Value = SearchTag[4];
            mp.dataGridView1.Rows[row].Cells["prefix"      ].Value = SearchTag[5];
            mp.dataGridView1.Rows[row].Cells["s_book_name1"].Value = SearchTag[6];
            mp.dataGridView1.Rows[row].Cells["s_book_num1" ].Value = SearchTag[7];
            mp.dataGridView1.Rows[row].Cells["author"      ].Value = SearchTag[8];
            mp.dataGridView1.Rows[row].Cells["book_comp"   ].Value = SearchTag[9];
            mp.dataGridView1.Rows[row].Cells["price"       ].Value = SearchTag[10];
            mp.dataGridView1.Rows[row].Cells["ISBN"        ].Value = SearchTag[11];
        }

        /// <summary>
        /// 도서명 이어붙히기
        /// <para>
        /// 본서명 = 대등서명 : 부서명 . 권차 , 권차명<br/>
        /// 245a = 245x : 245b . 245n , 245p
        /// </para>
        /// </summary>
        /// <param name="book_name">
        /// 0: 245a  1: 245x  2: 245b  3: 245n  4: 245p<br/>
        /// 0: 도서명  1: 대동서명  2: 부서명  3: 권차  4: 권차명<br/>
        /// </param>
        /// <returns>이어붙혀진 도서명</returns>
        private string mk_BookName(string[] book_name)
        {
            string a245 = book_name[0];     // 도서명
            string x245 = book_name[1];     // 대동서명
            string b245 = book_name[2];     // 부서명

            string result = a245;

            if (x245 != "")
                result += " = " + x245;

            if (b245 != "")
                result += " : " + b245;

            return result;
        }
        #endregion

        private void btn_Back_Click(object sender, EventArgs e)
        {
            if (row <= 0)
                return;
            else
                row--;

            Btn_FrontAndBack_Sub();
        }

        private void btn_Front_Click(object sender, EventArgs e)
        {
            int mprow = 0;
            if (mp != null)
                mprow = mp.dataGridView1.Rows.Count;
            else if(si!=null)
                mprow = si.dataGridView1.Rows.Count;

            if (row < mprow - 1)
                row++;
            else
                return;

            Btn_FrontAndBack_Sub();
        }

        #region btn_FrontAndBack_Sub

        private void Btn_FrontAndBack_Sub()
        {
            if (mp != null)
                FrontBack_mp();

            else if (si != null)
                FrontBack_si();
        }

        private void FrontBack_mp()
        {
            string[] marc = {
                mp.dataGridView1.Rows[row].Cells["marc"].Value.ToString(),
                mp.dataGridView1.Rows[row].Cells["midx"].Value.ToString(),
                mp.dataGridView1.Rows[row].Cells["num"].Value.ToString(),
                mp.dataGridView1.Rows[row].Cells["idx"].Value.ToString(),
                mp.dataGridView1.Rows[row].Cells["ISBN"].Value.ToString()
            };
            string[] Symbol_Type = {
                cb_FirstAuthor.Text, cb_authorType.Text, cb_FirstBook.Text, cb_divType.Text, cb_divNum.Text
            };

            Init(marc);
            SetSymbolType(Symbol_Type);
        }

        private void FrontBack_si()
        {
            string[] Marc = {
                si.dataGridView1.Rows[row].Cells["Marc"].Value.ToString(),
                si.dataGridView1.Rows[row].Cells["idx"].Value.ToString(),
                "",
                "",
                si.dataGridView1.Rows[row].Cells["ISBN"].Value.ToString()
            };

            Init(Marc);
        }
        #endregion

        private void Marc_Plan_Sub_MarcEdit_Load(object sender, EventArgs e)
        {
            this.ActiveControl = richTextBox1;
        }

        private void btn_EditNum_Click(object sender, EventArgs e)
        {
            string Table = "Specs_Marc";
            string[] Search_col = { "idx" };
            string[] Search_Data = { idx };
            string[] Update_col = { "midx", "num" };
            string[] Update_Data = { tb_Mcode.Text, tb_num.Text };
            string cmd = db.More_Update(Table, Update_col, Update_Data, Search_col, Search_Data);
            db.DB_Send_CMD_reVoid(cmd);

            MessageBox.Show("적용되었습니다!");
        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F9)
                btn_Save_Click(null, null);

            if (e.KeyCode == Keys.F11)
                btn_Back_Click(null, null);

            if (e.KeyCode == Keys.F12)
                btn_Front_Click(null, null);

            if (e.KeyCode == Keys.Escape)
                btn_Close_Click(null, null);

            if (e.KeyCode == Keys.F7) {
                string Tag090 = Sample_090();
                Tag_Create("090", Tag090);
            }
        }
        #region KeyDown_Sub
        /// <summary>
        /// 저자기호와 분류기호를 반환하는 코드<br/>
        /// 현재 저자기호만 가능
        /// </summary>
        /// <returns></returns>
        private string Sample_090()
        {
            string Marc = richTextBox1.Text;

            bool[] isType = { false, false };
            if (cb_FirstAuthor.SelectedIndex == 0)
                isType[0] = true;
            if (cb_FirstBook.SelectedIndex == 0)
                isType[1] = true;

            string authorType = cb_authorType.SelectedItem.ToString();

            string ClassSymbol = string.Empty;
            string Author = string.Empty;
            string BookName = string.Empty;

            #region 100 작가(Author) / 245 도서명(BookName) 태그 가져오는 코드 

            string AuthorTagNum = string.Empty;
            string[] splitMarc = Marc.Split('\n');

            // 100, 110, 111 태그의 저자명 구해오는 반복문
            for (int a = 0; a < splitMarc.Length - 1; a++)
            {
                if (splitMarc[a].Substring(0, 3) == "100")
                {
                    AuthorTagNum = "100";
                    break;
                }
                if (splitMarc[a].Substring(0, 3) == "110")
                {
                    AuthorTagNum = "110";
                    break;
                }
                if (splitMarc[a].Substring(0, 3) == "111")
                {
                    AuthorTagNum = "111";
                    break;
                }
            }

            for (int a = 0; a < splitMarc.Length - 1; a++)
            {
                if (splitMarc[a].Substring(0, 3) == "056")
                {
                    int start = splitMarc[a].IndexOf("▼a") + 2;
                    int end = splitMarc[a].IndexOf("▼", start);
                    if (end < 0)
                        ClassSymbol = splitMarc[a].Substring(start).Replace("▲", "");
                    else
                        ClassSymbol = splitMarc[a].Substring(start, end - start);
                }
                if (splitMarc[a].Substring(0, 3) == AuthorTagNum)
                {
                    int start = splitMarc[a].IndexOf("▼a") + 2;
                    int end = splitMarc[a].IndexOf("▼", start);
                    if (end < 0)
                        Author = splitMarc[a].Substring(start).Replace("▲", "");
                    else
                        Author = splitMarc[a].Substring(start, end - start);
                }
                if (splitMarc[a].Substring(0, 3) == "245")
                {
                    int start = splitMarc[a].IndexOf("▼a") + 2;
                    int end = splitMarc[a].IndexOf("▼", start);
                    if (end < 0)
                        BookName = splitMarc[a].Substring(start).Replace("▲", "");
                    else
                        BookName = splitMarc[a].Substring(start, end - start);
                }
            }
            BookName = st.RemoveWordInBracket(BookName);
            #endregion

            AuthorSymbol.Symbol sb = new AuthorSymbol.Symbol();

            string AuthorSymbol = sb.SymbolAuthor(Author, BookName, authorType, isType);
            lbl_AuthorSymbol.Text = AuthorSymbol;
            lbl_ClassSymbol.Text = ClassSymbol;

            ClassSymbol = "▼a" + ClassSymbol;
            AuthorSymbol = "▼b" + AuthorSymbol;

            string result = string.Format("\t{0}\t{1}{2}▲", "  ", ClassSymbol, AuthorSymbol);

            return result;
        }

        /// <summary>
        /// 마크에 090 태그가 삽입되어야함.
        /// <para>
        /// 090a 분류기호<br/>
        /// 090b 도서기호
        /// </para>
        /// 지시기호 따로 정의되지 않음. (빈칸으로 정의)<br/>
        /// ex. 090  ▼a813.6▼b홍214ㄱ▲
        /// <param name="tar">태그 명 (숫자 3자리만!)</param>
        /// </summary>
        private void Tag_Create(string tar,string Content)
        {
            int Itar = Convert.ToInt32(tar);
            string marc = richTextBox1.Text;
            List<string> target = marc.Split('\n').ToList();

            bool chk = true;
            int count = -1;

            for (int a = 0; a < target.Count - 1; a++)
            {
                string[] tmp = target[a].Split('\t');
                string tag = tmp[0];
                int tag_num = Convert.ToInt32(tag);
                if (tag_num == Itar) {
                    chk = false;
                }
                if (tag_num > Itar && chk)
                {
                    count = a;
                    chk = false;
                }
            }
            if (count > 0)
                target.Insert(count, string.Format("{0}{1}", tar, Content)); //text008.Text));
            richTextBox1.Text = string.Join("\n", target.ToArray());
            return;
        }
        #endregion

        private void cb_divType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cb_divNum.Items.Clear();

            string value = cb_divType.SelectedItem.ToString();
            if (value == "KDC")
            {
                string[] data = { "4", "5", "6" };
                cb_divNum.Items.AddRange(data);
            }
            else
            {
                string[] data = { "21", "22", "23" };
                cb_divNum.Items.AddRange(data);
            }
        }
    }
}
