﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniMarc.마크
{
    public partial class AddMarc_FillBlank : Form
    {
        AddMarc am;
        public AddMarc_FillBlank(AddMarc _am)
        {
            InitializeComponent();
            am = _am;
        }

        private void AddMarc_FillBlank_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate("https://nl.go.kr/kolisnet/search/searchResultAllList.do?");
        }

        #region 웹브라우저 관련 버튼들
        /// <summary>
        /// 뒤로 가기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btn_Back_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }
        /// <summary>
        /// 앞으로 가기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btn_Forward_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }
        /// <summary>
        /// 새로고침
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }
        #endregion

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            tb_URL.Text = webBrowser1.Url.AbsoluteUri;
        }

        private void tb_URL_TextChanged(object sender, EventArgs e)
        {
            if (tb_URL.Text.Contains("searchResultMarc"))
            {
                Btn_Apply.Enabled = true;
                Btn_Apply.ForeColor = Color.Blue;
            }
            else
            {
                Btn_Apply.Enabled = false;
            }
        }

        private void Btn_Apply_Click(object sender, EventArgs e)
        {
            string Text = "";

            HtmlElementCollection TableName = webBrowser1.Document.GetElementsByTagName("table");
            foreach (HtmlElement SearchTable in TableName)
            {
                if (SearchTable.GetAttribute("className") == "tbl")
                {
                    HtmlElementCollection tdName = SearchTable.GetElementsByTagName("td");
                    foreach (HtmlElement SearchTd in tdName)
                    {
                        string Td = SearchTd.InnerText;

                        if (Td is null)
                            Td = "";

                        if (Td.Contains("▲"))
                            Text += SearchTd.InnerText + "\n";
                        else
                            Text += SearchTd.InnerText + "\t";
                    }
                }
            }
            string tmp = "";
            foreach (string t in Text.Replace("↔", "").Split('\n'))
            {
                if (t.StartsWith("00") || t == "")
                    continue;

                string[] ary = t.Split('\t');
                if (ary[1] == "") ary[1] = "  ";
                if (ary[1].Length != 2) ary[1].PadRight(2);
                tmp += String.Join("\t", ary) + "\n";
            }
            am.richTextBox1.Text = tmp;
        }

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
