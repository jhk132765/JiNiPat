﻿namespace WindowsFormsApp1.Mac
{
    partial class Marc_Plan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Marc_Plan));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_GearExcel = new System.Windows.Forms.Button();
            this.btn_ApplyMacro = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Excel = new System.Windows.Forms.Button();
            this.cb_EncodingType = new System.Windows.Forms.ComboBox();
            this.btn_Output = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_Select_List = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cb_authorType = new System.Windows.Forms.ComboBox();
            this.cb_divType = new System.Windows.Forms.ComboBox();
            this.cb_divNum = new System.Windows.Forms.ComboBox();
            this.cb_FirstBook = new System.Windows.Forms.ComboBox();
            this.cb_FirstAuthor = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_ChangeTag = new System.Windows.Forms.Button();
            this.tb_SearchTag = new System.Windows.Forms.TextBox();
            this.tb_ISBN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reg_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.class_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volume = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.copy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prefix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ISBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s_book_name1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s_book_num1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s_book_name2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s_book_num2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.midx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.search_tag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.WorkCopy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkFix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_ClassSymbol = new System.Windows.Forms.Button();
            this.btn_InputColorFix = new System.Windows.Forms.Button();
            this.btn_InputAutoCopy = new System.Windows.Forms.Button();
            this.chkBox_AllowDrop = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.chk_Alignment = new System.Windows.Forms.CheckBox();
            this.tb_Left = new System.Windows.Forms.TextBox();
            this.btn_Bring = new System.Windows.Forms.Button();
            this.btn_PrintView = new System.Windows.Forms.Button();
            this.chk_GuideLine = new System.Windows.Forms.CheckBox();
            this.cb_TextFont = new System.Windows.Forms.ComboBox();
            this.chk_Num = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tb_NumSize = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tb_TextSize = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.chk_C = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chk_V = new System.Windows.Forms.CheckBox();
            this.tb_Top = new System.Windows.Forms.TextBox();
            this.tb_Width = new System.Windows.Forms.TextBox();
            this.tb_StartPosition = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_SeroGap = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_Height = new System.Windows.Forms.TextBox();
            this.tb_Sero = new System.Windows.Forms.TextBox();
            this.tb_GaroGap = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_Garo = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.tb_SearchChangeColor = new System.Windows.Forms.TextBox();
            this.btn_ChangeColor = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_ClassSymbol);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.btn_InputColorFix);
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.btn_InputAutoCopy);
            this.panel1.Controls.Add(this.btn_Select_List);
            this.panel1.Controls.Add(this.chkBox_AllowDrop);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1661, 35);
            this.panel1.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btn_GearExcel);
            this.panel4.Controls.Add(this.btn_ApplyMacro);
            this.panel4.Controls.Add(this.btn_Save);
            this.panel4.Controls.Add(this.btn_Excel);
            this.panel4.Controls.Add(this.cb_EncodingType);
            this.panel4.Controls.Add(this.btn_Output);
            this.panel4.Location = new System.Drawing.Point(402, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(537, 29);
            this.panel4.TabIndex = 9;
            // 
            // btn_GearExcel
            // 
            this.btn_GearExcel.Location = new System.Drawing.Point(455, 2);
            this.btn_GearExcel.Name = "btn_GearExcel";
            this.btn_GearExcel.Size = new System.Drawing.Size(77, 23);
            this.btn_GearExcel.TabIndex = 12;
            this.btn_GearExcel.Text = "장비용 엑셀";
            this.btn_GearExcel.UseVisualStyleBackColor = true;
            this.btn_GearExcel.Click += new System.EventHandler(this.btn_GearExcel_Click);
            // 
            // btn_ApplyMacro
            // 
            this.btn_ApplyMacro.Location = new System.Drawing.Point(95, 2);
            this.btn_ApplyMacro.Name = "btn_ApplyMacro";
            this.btn_ApplyMacro.Size = new System.Drawing.Size(86, 23);
            this.btn_ApplyMacro.TabIndex = 11;
            this.btn_ApplyMacro.Text = "매크로 적용";
            this.btn_ApplyMacro.UseVisualStyleBackColor = true;
            this.btn_ApplyMacro.Click += new System.EventHandler(this.btn_ApplyMacro_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(3, 2);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(86, 23);
            this.btn_Save.TabIndex = 10;
            this.btn_Save.Text = "전체 저장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Excel
            // 
            this.btn_Excel.Location = new System.Drawing.Point(377, 2);
            this.btn_Excel.Name = "btn_Excel";
            this.btn_Excel.Size = new System.Drawing.Size(75, 23);
            this.btn_Excel.TabIndex = 8;
            this.btn_Excel.Text = "엑셀 반출";
            this.btn_Excel.UseVisualStyleBackColor = true;
            this.btn_Excel.Click += new System.EventHandler(this.btn_Excel_Click);
            // 
            // cb_EncodingType
            // 
            this.cb_EncodingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_EncodingType.FormattingEnabled = true;
            this.cb_EncodingType.Location = new System.Drawing.Point(212, 3);
            this.cb_EncodingType.Name = "cb_EncodingType";
            this.cb_EncodingType.Size = new System.Drawing.Size(81, 20);
            this.cb_EncodingType.TabIndex = 9;
            // 
            // btn_Output
            // 
            this.btn_Output.Location = new System.Drawing.Point(299, 2);
            this.btn_Output.Name = "btn_Output";
            this.btn_Output.Size = new System.Drawing.Size(75, 23);
            this.btn_Output.TabIndex = 9;
            this.btn_Output.Text = "마크 반출";
            this.btn_Output.UseVisualStyleBackColor = true;
            this.btn_Output.Click += new System.EventHandler(this.btn_Output_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(1384, 6);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(86, 23);
            this.btn_Close.TabIndex = 5;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_Select_List
            // 
            this.btn_Select_List.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Select_List.Location = new System.Drawing.Point(9, 3);
            this.btn_Select_List.Name = "btn_Select_List";
            this.btn_Select_List.Size = new System.Drawing.Size(365, 29);
            this.btn_Select_List.TabIndex = 0;
            this.btn_Select_List.Text = "목록선택";
            this.btn_Select_List.UseVisualStyleBackColor = true;
            this.btn_Select_List.Click += new System.EventHandler(this.btn_Select_List_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cb_authorType);
            this.panel3.Controls.Add(this.cb_divType);
            this.panel3.Controls.Add(this.cb_divNum);
            this.panel3.Controls.Add(this.cb_FirstBook);
            this.panel3.Controls.Add(this.cb_FirstAuthor);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(10, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(566, 29);
            this.panel3.TabIndex = 7;
            // 
            // cb_authorType
            // 
            this.cb_authorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_authorType.Enabled = false;
            this.cb_authorType.FormattingEnabled = true;
            this.cb_authorType.Location = new System.Drawing.Point(125, 3);
            this.cb_authorType.Name = "cb_authorType";
            this.cb_authorType.Size = new System.Drawing.Size(171, 20);
            this.cb_authorType.TabIndex = 1;
            // 
            // cb_divType
            // 
            this.cb_divType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_divType.Enabled = false;
            this.cb_divType.FormattingEnabled = true;
            this.cb_divType.Location = new System.Drawing.Point(402, 3);
            this.cb_divType.Name = "cb_divType";
            this.cb_divType.Size = new System.Drawing.Size(88, 20);
            this.cb_divType.TabIndex = 1;
            this.cb_divType.SelectedIndexChanged += new System.EventHandler(this.cb_divType_SelectedIndexChanged);
            // 
            // cb_divNum
            // 
            this.cb_divNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_divNum.Enabled = false;
            this.cb_divNum.FormattingEnabled = true;
            this.cb_divNum.Location = new System.Drawing.Point(496, 3);
            this.cb_divNum.Name = "cb_divNum";
            this.cb_divNum.Size = new System.Drawing.Size(61, 20);
            this.cb_divNum.TabIndex = 1;
            // 
            // cb_FirstBook
            // 
            this.cb_FirstBook.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_FirstBook.Enabled = false;
            this.cb_FirstBook.FormattingEnabled = true;
            this.cb_FirstBook.Location = new System.Drawing.Point(302, 3);
            this.cb_FirstBook.Name = "cb_FirstBook";
            this.cb_FirstBook.Size = new System.Drawing.Size(61, 20);
            this.cb_FirstBook.TabIndex = 1;
            // 
            // cb_FirstAuthor
            // 
            this.cb_FirstAuthor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_FirstAuthor.Enabled = false;
            this.cb_FirstAuthor.FormattingEnabled = true;
            this.cb_FirstAuthor.Location = new System.Drawing.Point(57, 3);
            this.cb_FirstAuthor.Name = "cb_FirstAuthor";
            this.cb_FirstAuthor.Size = new System.Drawing.Size(61, 20);
            this.cb_FirstAuthor.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(371, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "구분";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "저자기호";
            // 
            // btn_ChangeTag
            // 
            this.btn_ChangeTag.Location = new System.Drawing.Point(322, 2);
            this.btn_ChangeTag.Name = "btn_ChangeTag";
            this.btn_ChangeTag.Size = new System.Drawing.Size(86, 23);
            this.btn_ChangeTag.TabIndex = 5;
            this.btn_ChangeTag.Text = "검색태그변경";
            this.btn_ChangeTag.UseVisualStyleBackColor = true;
            this.btn_ChangeTag.Click += new System.EventHandler(this.btn_ChangeTag_Click);
            // 
            // tb_SearchTag
            // 
            this.tb_SearchTag.Location = new System.Drawing.Point(211, 3);
            this.tb_SearchTag.Name = "tb_SearchTag";
            this.tb_SearchTag.Size = new System.Drawing.Size(100, 21);
            this.tb_SearchTag.TabIndex = 4;
            this.tb_SearchTag.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_SearchTag_KeyDown);
            // 
            // tb_ISBN
            // 
            this.tb_ISBN.Enabled = false;
            this.tb_ISBN.Location = new System.Drawing.Point(39, 3);
            this.tb_ISBN.Name = "tb_ISBN";
            this.tb_ISBN.Size = new System.Drawing.Size(100, 21);
            this.tb_ISBN.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(152, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "태그 검색";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "ISBN";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridView1.ColumnHeadersHeight = 25;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.num,
            this.reg_num,
            this.class_code,
            this.author_code,
            this.volume,
            this.copy,
            this.prefix,
            this.gu,
            this.ISBN,
            this.book_name,
            this.s_book_name1,
            this.s_book_num1,
            this.s_book_name2,
            this.s_book_num2,
            this.author,
            this.book_comp,
            this.price,
            this.midx,
            this.marc,
            this.search_tag,
            this.colCheck,
            this.WorkCopy,
            this.WorkFix});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1661, 549);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.dataGridView1.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dataGridView1_SortCompare);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.ReadOnly = true;
            this.idx.Visible = false;
            // 
            // num
            // 
            this.num.FillWeight = 64.46414F;
            this.num.HeaderText = "연번";
            this.num.Name = "num";
            this.num.ReadOnly = true;
            this.num.Width = 50;
            // 
            // reg_num
            // 
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.reg_num.DefaultCellStyle = dataGridViewCellStyle30;
            this.reg_num.FillWeight = 130.9363F;
            this.reg_num.HeaderText = "등록번호";
            this.reg_num.Name = "reg_num";
            this.reg_num.Width = 102;
            // 
            // class_code
            // 
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.class_code.DefaultCellStyle = dataGridViewCellStyle31;
            this.class_code.FillWeight = 76.41504F;
            this.class_code.HeaderText = "분류";
            this.class_code.Name = "class_code";
            this.class_code.Width = 59;
            // 
            // author_code
            // 
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.author_code.DefaultCellStyle = dataGridViewCellStyle32;
            this.author_code.FillWeight = 77.02635F;
            this.author_code.HeaderText = "저자기호";
            this.author_code.Name = "author_code";
            this.author_code.Width = 60;
            // 
            // volume
            // 
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.volume.DefaultCellStyle = dataGridViewCellStyle33;
            this.volume.FillWeight = 38.80909F;
            this.volume.HeaderText = "V";
            this.volume.Name = "volume";
            this.volume.ToolTipText = "049v";
            this.volume.Width = 30;
            // 
            // copy
            // 
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.copy.DefaultCellStyle = dataGridViewCellStyle34;
            this.copy.FillWeight = 40.14827F;
            this.copy.HeaderText = "C";
            this.copy.Name = "copy";
            this.copy.ToolTipText = "049c";
            this.copy.Width = 31;
            // 
            // prefix
            // 
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.prefix.DefaultCellStyle = dataGridViewCellStyle35;
            this.prefix.FillWeight = 41.51828F;
            this.prefix.HeaderText = "F";
            this.prefix.Name = "prefix";
            this.prefix.ToolTipText = "049f";
            this.prefix.Width = 32;
            // 
            // gu
            // 
            this.gu.FillWeight = 71.64279F;
            this.gu.HeaderText = "구분";
            this.gu.Name = "gu";
            this.gu.ReadOnly = true;
            this.gu.Width = 56;
            // 
            // ISBN
            // 
            this.ISBN.HeaderText = "ISBN";
            this.ISBN.Name = "ISBN";
            this.ISBN.Visible = false;
            // 
            // book_name
            // 
            this.book_name.FillWeight = 291.9296F;
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.ReadOnly = true;
            this.book_name.Width = 226;
            // 
            // s_book_name1
            // 
            this.s_book_name1.FillWeight = 129.9903F;
            this.s_book_name1.HeaderText = "총서명";
            this.s_book_name1.Name = "s_book_name1";
            this.s_book_name1.ReadOnly = true;
            this.s_book_name1.Width = 101;
            // 
            // s_book_num1
            // 
            this.s_book_num1.FillWeight = 76.24091F;
            this.s_book_num1.HeaderText = "총서번호";
            this.s_book_num1.Name = "s_book_num1";
            this.s_book_num1.ReadOnly = true;
            this.s_book_num1.Width = 59;
            // 
            // s_book_name2
            // 
            this.s_book_name2.HeaderText = "총서명";
            this.s_book_name2.Name = "s_book_name2";
            // 
            // s_book_num2
            // 
            this.s_book_num2.HeaderText = "총서번호";
            this.s_book_num2.Name = "s_book_num2";
            this.s_book_num2.Width = 59;
            // 
            // author
            // 
            this.author.FillWeight = 128.5217F;
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            this.author.ReadOnly = true;
            // 
            // book_comp
            // 
            this.book_comp.FillWeight = 125.7765F;
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            this.book_comp.ReadOnly = true;
            this.book_comp.Width = 97;
            // 
            // price
            // 
            this.price.FillWeight = 86.15041F;
            this.price.HeaderText = "정가";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            this.price.Width = 67;
            // 
            // midx
            // 
            this.midx.HeaderText = "midx";
            this.midx.Name = "midx";
            this.midx.Visible = false;
            // 
            // marc
            // 
            this.marc.HeaderText = "마크";
            this.marc.Name = "marc";
            // 
            // search_tag
            // 
            this.search_tag.FillWeight = 185.6383F;
            this.search_tag.HeaderText = "검색태그";
            this.search_tag.Name = "search_tag";
            this.search_tag.Width = 144;
            // 
            // colCheck
            // 
            this.colCheck.FalseValue = "F";
            this.colCheck.FillWeight = 34.79187F;
            this.colCheck.HeaderText = "□";
            this.colCheck.IndeterminateValue = "F";
            this.colCheck.Name = "colCheck";
            this.colCheck.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colCheck.TrueValue = "T";
            this.colCheck.Width = 27;
            // 
            // WorkCopy
            // 
            this.WorkCopy.HeaderText = "복본 작업용 열";
            this.WorkCopy.Name = "WorkCopy";
            // 
            // WorkFix
            // 
            this.WorkFix.HeaderText = "별치 작업용 열";
            this.WorkFix.Name = "WorkFix";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(1520, 6);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.TabStop = false;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btn_ChangeColor);
            this.panel2.Controls.Add(this.tb_SearchChangeColor);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.tb_ISBN);
            this.panel2.Controls.Add(this.btn_ChangeTag);
            this.panel2.Controls.Add(this.tb_SearchTag);
            this.panel2.Location = new System.Drawing.Point(596, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(761, 29);
            this.panel2.TabIndex = 8;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel3);
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 35);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1661, 35);
            this.panel5.TabIndex = 9;
            // 
            // btn_ClassSymbol
            // 
            this.btn_ClassSymbol.Location = new System.Drawing.Point(1289, 6);
            this.btn_ClassSymbol.Name = "btn_ClassSymbol";
            this.btn_ClassSymbol.Size = new System.Drawing.Size(89, 23);
            this.btn_ClassSymbol.TabIndex = 13;
            this.btn_ClassSymbol.Text = "등록번호 확인";
            this.btn_ClassSymbol.UseVisualStyleBackColor = true;
            this.btn_ClassSymbol.Click += new System.EventHandler(this.btn_ClassSymbol_Click);
            // 
            // btn_InputColorFix
            // 
            this.btn_InputColorFix.Location = new System.Drawing.Point(1194, 6);
            this.btn_InputColorFix.Name = "btn_InputColorFix";
            this.btn_InputColorFix.Size = new System.Drawing.Size(89, 23);
            this.btn_InputColorFix.TabIndex = 12;
            this.btn_InputColorFix.Text = "별치 색부여";
            this.btn_InputColorFix.UseVisualStyleBackColor = true;
            this.btn_InputColorFix.Click += new System.EventHandler(this.btn_InputColorFix_Click);
            // 
            // btn_InputAutoCopy
            // 
            this.btn_InputAutoCopy.Location = new System.Drawing.Point(1099, 6);
            this.btn_InputAutoCopy.Name = "btn_InputAutoCopy";
            this.btn_InputAutoCopy.Size = new System.Drawing.Size(89, 23);
            this.btn_InputAutoCopy.TabIndex = 11;
            this.btn_InputAutoCopy.Text = "복본 자동부여";
            this.btn_InputAutoCopy.UseVisualStyleBackColor = true;
            this.btn_InputAutoCopy.Click += new System.EventHandler(this.btn_InputAutoCopy_Click);
            // 
            // chkBox_AllowDrop
            // 
            this.chkBox_AllowDrop.AutoSize = true;
            this.chkBox_AllowDrop.Location = new System.Drawing.Point(947, 10);
            this.chkBox_AllowDrop.Name = "chkBox_AllowDrop";
            this.chkBox_AllowDrop.Size = new System.Drawing.Size(147, 16);
            this.chkBox_AllowDrop.TabIndex = 10;
            this.chkBox_AllowDrop.Text = "표 드래그 앤 드롭 OFF";
            this.chkBox_AllowDrop.UseVisualStyleBackColor = true;
            this.chkBox_AllowDrop.CheckedChanged += new System.EventHandler(this.chkBox_AllowDrop_CheckedChanged);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel6.Controls.Add(this.chk_Alignment);
            this.panel6.Controls.Add(this.tb_Left);
            this.panel6.Controls.Add(this.btn_Bring);
            this.panel6.Controls.Add(this.btn_PrintView);
            this.panel6.Controls.Add(this.chk_GuideLine);
            this.panel6.Controls.Add(this.cb_TextFont);
            this.panel6.Controls.Add(this.chk_Num);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.tb_NumSize);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.tb_TextSize);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.chk_C);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.chk_V);
            this.panel6.Controls.Add(this.tb_Top);
            this.panel6.Controls.Add(this.tb_Width);
            this.panel6.Controls.Add(this.tb_StartPosition);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.tb_SeroGap);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.tb_Height);
            this.panel6.Controls.Add(this.tb_Sero);
            this.panel6.Controls.Add(this.tb_GaroGap);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.tb_Garo);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 619);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1661, 59);
            this.panel6.TabIndex = 10;
            // 
            // chk_Alignment
            // 
            this.chk_Alignment.AutoSize = true;
            this.chk_Alignment.Location = new System.Drawing.Point(890, 7);
            this.chk_Alignment.Name = "chk_Alignment";
            this.chk_Alignment.Size = new System.Drawing.Size(88, 16);
            this.chk_Alignment.TabIndex = 85;
            this.chk_Alignment.Text = "가운데 정렬";
            this.chk_Alignment.UseVisualStyleBackColor = true;
            // 
            // tb_Left
            // 
            this.tb_Left.Location = new System.Drawing.Point(41, 6);
            this.tb_Left.Name = "tb_Left";
            this.tb_Left.Size = new System.Drawing.Size(56, 21);
            this.tb_Left.TabIndex = 58;
            this.tb_Left.Text = "20";
            this.tb_Left.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_Bring
            // 
            this.btn_Bring.Location = new System.Drawing.Point(1089, 5);
            this.btn_Bring.Name = "btn_Bring";
            this.btn_Bring.Size = new System.Drawing.Size(75, 49);
            this.btn_Bring.TabIndex = 80;
            this.btn_Bring.Text = "불러오기";
            this.btn_Bring.UseVisualStyleBackColor = true;
            this.btn_Bring.Click += new System.EventHandler(this.btn_Bring_Click);
            // 
            // btn_PrintView
            // 
            this.btn_PrintView.Location = new System.Drawing.Point(1008, 5);
            this.btn_PrintView.Name = "btn_PrintView";
            this.btn_PrintView.Size = new System.Drawing.Size(75, 49);
            this.btn_PrintView.TabIndex = 80;
            this.btn_PrintView.Text = "미리보기";
            this.btn_PrintView.UseVisualStyleBackColor = true;
            this.btn_PrintView.Click += new System.EventHandler(this.btn_PrintView_Click);
            // 
            // chk_GuideLine
            // 
            this.chk_GuideLine.AutoSize = true;
            this.chk_GuideLine.Location = new System.Drawing.Point(890, 37);
            this.chk_GuideLine.Name = "chk_GuideLine";
            this.chk_GuideLine.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chk_GuideLine.Size = new System.Drawing.Size(112, 16);
            this.chk_GuideLine.TabIndex = 84;
            this.chk_GuideLine.Text = "가이드라인 제거";
            this.chk_GuideLine.UseVisualStyleBackColor = true;
            // 
            // cb_TextFont
            // 
            this.cb_TextFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_TextFont.FormattingEnabled = true;
            this.cb_TextFont.Location = new System.Drawing.Point(712, 6);
            this.cb_TextFont.Name = "cb_TextFont";
            this.cb_TextFont.Size = new System.Drawing.Size(168, 20);
            this.cb_TextFont.TabIndex = 78;
            // 
            // chk_Num
            // 
            this.chk_Num.AutoSize = true;
            this.chk_Num.Location = new System.Drawing.Point(890, 22);
            this.chk_Num.Name = "chk_Num";
            this.chk_Num.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chk_Num.Size = new System.Drawing.Size(112, 16);
            this.chk_Num.TabIndex = 83;
            this.chk_Num.Text = "등록번호 안으로";
            this.chk_Num.UseVisualStyleBackColor = true;
            this.chk_Num.CheckedChanged += new System.EventHandler(this.chk_Num_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(8, 10);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 12);
            this.label23.TabIndex = 50;
            this.label23.Text = "좌측";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(99, 10);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 12);
            this.label22.TabIndex = 54;
            this.label22.Text = "mm";
            // 
            // tb_NumSize
            // 
            this.tb_NumSize.Enabled = false;
            this.tb_NumSize.Location = new System.Drawing.Point(850, 33);
            this.tb_NumSize.Name = "tb_NumSize";
            this.tb_NumSize.Size = new System.Drawing.Size(30, 21);
            this.tb_NumSize.TabIndex = 79;
            this.tb_NumSize.Text = "10";
            this.tb_NumSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(767, 37);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 12);
            this.label24.TabIndex = 60;
            this.label24.Text = "등록번호크기";
            // 
            // tb_TextSize
            // 
            this.tb_TextSize.Location = new System.Drawing.Point(725, 33);
            this.tb_TextSize.Name = "tb_TextSize";
            this.tb_TextSize.Size = new System.Drawing.Size(30, 21);
            this.tb_TextSize.TabIndex = 79;
            this.tb_TextSize.Text = "10";
            this.tb_TextSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(666, 37);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 12);
            this.label20.TabIndex = 60;
            this.label20.Text = "글자크기";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(233, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 12);
            this.label21.TabIndex = 68;
            this.label21.Text = "mm";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(666, 10);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 12);
            this.label18.TabIndex = 62;
            this.label18.Text = "글자체";
            // 
            // chk_C
            // 
            this.chk_C.AutoSize = true;
            this.chk_C.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_C.Location = new System.Drawing.Point(609, 35);
            this.chk_C.Name = "chk_C";
            this.chk_C.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chk_C.Size = new System.Drawing.Size(37, 16);
            this.chk_C.TabIndex = 82;
            this.chk_C.Text = "C.";
            this.chk_C.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(142, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 12);
            this.label10.TabIndex = 69;
            this.label10.Text = "위쪽";
            // 
            // chk_V
            // 
            this.chk_V.AutoSize = true;
            this.chk_V.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_V.Location = new System.Drawing.Point(549, 35);
            this.chk_V.Name = "chk_V";
            this.chk_V.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chk_V.Size = new System.Drawing.Size(36, 16);
            this.chk_V.TabIndex = 81;
            this.chk_V.Text = "V.";
            this.chk_V.UseVisualStyleBackColor = true;
            // 
            // tb_Top
            // 
            this.tb_Top.Location = new System.Drawing.Point(175, 6);
            this.tb_Top.Name = "tb_Top";
            this.tb_Top.Size = new System.Drawing.Size(56, 21);
            this.tb_Top.TabIndex = 70;
            this.tb_Top.Text = "22";
            this.tb_Top.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_Width
            // 
            this.tb_Width.Location = new System.Drawing.Point(41, 33);
            this.tb_Width.Name = "tb_Width";
            this.tb_Width.Size = new System.Drawing.Size(56, 21);
            this.tb_Width.TabIndex = 71;
            this.tb_Width.Text = "30";
            this.tb_Width.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_StartPosition
            // 
            this.tb_StartPosition.Location = new System.Drawing.Point(598, 6);
            this.tb_StartPosition.Name = "tb_StartPosition";
            this.tb_StartPosition.Size = new System.Drawing.Size(59, 21);
            this.tb_StartPosition.TabIndex = 77;
            this.tb_StartPosition.Text = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(99, 37);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 12);
            this.label19.TabIndex = 67;
            this.label19.Text = "mm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(539, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 12);
            this.label7.TabIndex = 61;
            this.label7.Text = "시작위치";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(233, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 12);
            this.label11.TabIndex = 63;
            this.label11.Text = "mm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(8, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 12);
            this.label9.TabIndex = 51;
            this.label9.Text = "너비";
            // 
            // tb_SeroGap
            // 
            this.tb_SeroGap.Location = new System.Drawing.Point(465, 6);
            this.tb_SeroGap.Name = "tb_SeroGap";
            this.tb_SeroGap.Size = new System.Drawing.Size(30, 21);
            this.tb_SeroGap.TabIndex = 74;
            this.tb_SeroGap.Text = "13.5";
            this.tb_SeroGap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(142, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 12);
            this.label8.TabIndex = 52;
            this.label8.Text = "높이";
            // 
            // tb_Height
            // 
            this.tb_Height.Location = new System.Drawing.Point(175, 33);
            this.tb_Height.Name = "tb_Height";
            this.tb_Height.Size = new System.Drawing.Size(56, 21);
            this.tb_Height.TabIndex = 72;
            this.tb_Height.Text = "31";
            this.tb_Height.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_Sero
            // 
            this.tb_Sero.Location = new System.Drawing.Point(465, 33);
            this.tb_Sero.Name = "tb_Sero";
            this.tb_Sero.Size = new System.Drawing.Size(30, 21);
            this.tb_Sero.TabIndex = 76;
            this.tb_Sero.Text = "6";
            this.tb_Sero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_GaroGap
            // 
            this.tb_GaroGap.Location = new System.Drawing.Point(336, 6);
            this.tb_GaroGap.Name = "tb_GaroGap";
            this.tb_GaroGap.Size = new System.Drawing.Size(30, 21);
            this.tb_GaroGap.TabIndex = 73;
            this.tb_GaroGap.Text = "5";
            this.tb_GaroGap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(368, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 12);
            this.label12.TabIndex = 66;
            this.label12.Text = "mm";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(406, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 12);
            this.label15.TabIndex = 57;
            this.label15.Text = "세로간격";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(368, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 65;
            this.label16.Text = "개";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(406, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 12);
            this.label6.TabIndex = 56;
            this.label6.Text = "세로갯수";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(497, 37);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 64;
            this.label17.Text = "개";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(497, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 12);
            this.label13.TabIndex = 59;
            this.label13.Text = "mm";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(277, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 12);
            this.label14.TabIndex = 55;
            this.label14.Text = "가로간격";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(277, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 12);
            this.label5.TabIndex = 53;
            this.label5.Text = "가로갯수";
            // 
            // tb_Garo
            // 
            this.tb_Garo.Location = new System.Drawing.Point(336, 33);
            this.tb_Garo.Name = "tb_Garo";
            this.tb_Garo.Size = new System.Drawing.Size(30, 21);
            this.tb_Garo.TabIndex = 75;
            this.tb_Garo.Text = "5";
            this.tb_Garo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.checkBox1);
            this.panel7.Controls.Add(this.dataGridView1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 70);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1661, 549);
            this.panel7.TabIndex = 11;
            // 
            // printDocument1
            // 
            this.printDocument1.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.printDocument1_BeginPrint);
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // tb_SearchChangeColor
            // 
            this.tb_SearchChangeColor.Location = new System.Drawing.Point(420, 3);
            this.tb_SearchChangeColor.Name = "tb_SearchChangeColor";
            this.tb_SearchChangeColor.Size = new System.Drawing.Size(147, 21);
            this.tb_SearchChangeColor.TabIndex = 6;
            this.tb_SearchChangeColor.Text = "여러 개 입력시 , 로 구분";
            this.tb_SearchChangeColor.Click += new System.EventHandler(this.tb_SearchChangeColor_Click);
            // 
            // btn_ChangeColor
            // 
            this.btn_ChangeColor.Location = new System.Drawing.Point(573, 2);
            this.btn_ChangeColor.Name = "btn_ChangeColor";
            this.btn_ChangeColor.Size = new System.Drawing.Size(93, 23);
            this.btn_ChangeColor.TabIndex = 7;
            this.btn_ChangeColor.Text = "검색값 색 변경";
            this.btn_ChangeColor.UseVisualStyleBackColor = true;
            this.btn_ChangeColor.Click += new System.EventHandler(this.btn_ChangeColor_Click);
            // 
            // Marc_Plan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1661, 678);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Name = "Marc_Plan";
            this.Text = "마크 정리";
            this.Load += new System.EventHandler(this.Marc_Plan_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Select_List;
        private System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.TextBox tb_SearchTag;
        private System.Windows.Forms.TextBox tb_ISBN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_ChangeTag;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cb_authorType;
        private System.Windows.Forms.ComboBox cb_divType;
        private System.Windows.Forms.ComboBox cb_divNum;
        private System.Windows.Forms.ComboBox cb_FirstBook;
        private System.Windows.Forms.ComboBox cb_FirstAuthor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Excel;
        private System.Windows.Forms.Button btn_Output;
        private System.Windows.Forms.ComboBox cb_EncodingType;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btn_PrintView;
        private System.Windows.Forms.CheckBox chk_GuideLine;
        private System.Windows.Forms.ComboBox cb_TextFont;
        private System.Windows.Forms.CheckBox chk_Num;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chk_C;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chk_V;
        private System.Windows.Forms.TextBox tb_StartPosition;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label5;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Button btn_Bring;
        private System.Windows.Forms.TextBox tb_Left;
        private System.Windows.Forms.TextBox tb_TextSize;
        private System.Windows.Forms.TextBox tb_Top;
        private System.Windows.Forms.TextBox tb_Width;
        private System.Windows.Forms.TextBox tb_SeroGap;
        private System.Windows.Forms.TextBox tb_Height;
        private System.Windows.Forms.TextBox tb_Sero;
        private System.Windows.Forms.TextBox tb_GaroGap;
        private System.Windows.Forms.TextBox tb_Garo;
        private System.Windows.Forms.CheckBox chkBox_AllowDrop;
        private System.Windows.Forms.CheckBox chk_Alignment;
        private System.Windows.Forms.TextBox tb_NumSize;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btn_ApplyMacro;
        private System.Windows.Forms.Button btn_InputAutoCopy;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn reg_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn class_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn author_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn volume;
        private System.Windows.Forms.DataGridViewTextBoxColumn copy;
        private System.Windows.Forms.DataGridViewTextBoxColumn prefix;
        private System.Windows.Forms.DataGridViewTextBoxColumn gu;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISBN;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn s_book_name1;
        private System.Windows.Forms.DataGridViewTextBoxColumn s_book_num1;
        private System.Windows.Forms.DataGridViewTextBoxColumn s_book_name2;
        private System.Windows.Forms.DataGridViewTextBoxColumn s_book_num2;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn midx;
        private System.Windows.Forms.DataGridViewTextBoxColumn marc;
        private System.Windows.Forms.DataGridViewTextBoxColumn search_tag;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkCopy;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkFix;
        private System.Windows.Forms.Button btn_InputColorFix;
        private System.Windows.Forms.Button btn_ClassSymbol;
        private System.Windows.Forms.Button btn_GearExcel;
        private System.Windows.Forms.Button btn_ChangeColor;
        private System.Windows.Forms.TextBox tb_SearchChangeColor;
    }
}