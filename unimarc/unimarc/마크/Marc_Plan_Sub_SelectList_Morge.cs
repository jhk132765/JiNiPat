﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace UniMarc.마크
{
    public partial class Marc_Plan_Sub_SelectList_Morge : Form
    {
        Helper_DB db = new Helper_DB();
        Marc_Plan_Sub_SelectList sub;
        public Marc_Plan_Sub_SelectList_Morge(Marc_Plan_Sub_SelectList _sub)
        {
            InitializeComponent();
            sub = _sub;
        }

        public void Init(List<string> ListIdx, List<string> ListName, List<string>ListDate, List<string> ListUser)
        {
            string[] idx = ListIdx.ToArray();
            string[] Name = ListName.ToArray();
            string[] Date = ListDate.ToArray();
            string[] User = ListUser.ToArray();

            Init(idx, Name, Date, User);
        }

        private void Init(string[] Aryidx, string[] AryName, string[] AryDate, string[] AryUser)
        {
            db.DBcon();

            int idxLength = Aryidx.Length;
            int NameLength = AryName.Length;
            int DateLength = AryDate.Length;
            int UserLength = AryUser.Length;

            // 받은 배열의 길이가 맞지않을경우 리턴
            if (NameLength != DateLength || NameLength != UserLength || DateLength != UserLength)
                return;

            for (int a = 0; a < NameLength; a++)
            {
                string[] Grid = { Aryidx[a], AryName[a], AryDate[a], AryUser[a] };
                dataGridView1.Rows.Add(Grid);
            }
        }

        private void btn_Morge_Click(object sender, EventArgs e)
        {
            if (tb_ListName.Text == "") {
                MessageBox.Show("저장될 목록명이 비어있습니다.", "Error");
                return;
            }
            string MorgeUser = Properties.Settings.Default.User;
            string Compidx = Properties.Settings.Default.compidx;
            string NowDate = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");

            string[] Search_Col = { "idx" };
            string[] Search_Data = { dataGridView1.Rows[0].Cells["idx"].Value.ToString() };
            string[] Update_Col = { "work_list", "date", "user" };
            string[] Update_Data = { tb_ListName.Text, NowDate, MorgeUser };

            string U_cmd = db.More_Update("Specs_List", Update_Col, Update_Data, Search_Col, Search_Data);
            db.DB_Send_CMD_reVoid(U_cmd);

            // 마크 수정
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                string[] Col = {
                    "work_list", "date"
                };
                string[] Data = {
                    dataGridView1.Rows[a].Cells["ListName"].Value.ToString(),
                    dataGridView1.Rows[a].Cells["ListDate"].Value.ToString()
                };

                string cmd = db.More_Update("Specs_Marc", Update_Col, Update_Data, Col, Data);
                db.DB_Send_CMD_reVoid(cmd);

                if (a != 0) {
                    string listIdx = dataGridView1.Rows[a].Cells["idx"].Value.ToString();
                    string listDate = dataGridView1.Rows[a].Cells["ListDate"].Value.ToString();
                    string D_cmd = db.DB_Delete("Specs_List", "idx", listIdx, "date", listDate);
                    db.DB_Send_CMD_reVoid(D_cmd);
                }
            }

            MessageBox.Show("병합되었습니다.");
            sub.btn_Search_Click(null, null);
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }
    }
}
