﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace UniMarc.마크
{
    public partial class Marc_Plan_Sub_SelectList_Edit : Form
    {
        string IDX;

        Helper_DB db = new Helper_DB();
        Marc_Plan_Sub_SelectList sub;
        public Marc_Plan_Sub_SelectList_Edit(Marc_Plan_Sub_SelectList _sub)
        {
            InitializeComponent();
            sub = _sub;
        }

        public void BaseSetting(string idx)
        {
            db.DBcon();

            string Area = "`first_Author`, `symbol_Author`, `book_Author`, `divType`, `divNum`";
            string cmd = db.DB_Select_Search(Area, "Specs_List", "idx", idx);
            string res = db.DB_Send_CMD_Search(cmd);
            string[] ary = res.Split('|');

            IDX = idx;

            // `first_Author`, `symbol_Author`, `book_Author`, `divType`, `divNum`
            cb_FirstAuthor.SelectedItem = ary[0];
            cb_authorType.SelectedItem = ary[1];
            cb_FirstBook.SelectedItem = ary[2];
            cb_divType.SelectedItem = ary[3];
            cb_divNum.SelectedItem = ary[4];
        }

        public void TextBoxSetting(string Text)
        {
            tb_listName.Text = Text;
        }

        private void Marc_Plan_Sub_SelectList_Edit_Load(object sender, EventArgs e)
        {
            #region panel2 set 저자기호 // 분류기호

            // 저자기호
            string[] First = { "첫음", "초성" };
            cb_FirstAuthor.Items.AddRange(First);
            cb_FirstAuthor.SelectedIndex = 0;

            string[] authorType = { 
     /* 00 */   "이재철 1표", 
     /* 01 */   "이재철 2표",
     /* 02 */   "이재철 3표(실용형 가표)", 
     /* 03 */   "이재철 4표(실용형 까표)",
     /* 04 */   "이재철 5표(완전형 가표)", 
     /* 05 */   "이재철 6표(완전형 가표)",
     /* 06 */   "이재철 7표(실용형 하표)", 
     /* 07 */   "이재철 7표(동서저자기호표)",
     /* 08 */   "이재철 8표(완전형 하표)", 
     /* 09 */   "이재철 8표(동서저자기호표)",
     /* 10 */   "장일세 저자기호표", 
     /* 11 */   "커터 샌본 저자기호표",
     /* 12 */   "엘러드 저자기호법",
     /* 13 */   "동서양 저자기호법 (국중)"
            };
            cb_authorType.Items.AddRange(authorType);
            cb_authorType.SelectedIndex = 0;

            string[] Book = { "첫음", "초성", "중성" };
            cb_FirstBook.Items.AddRange(Book);
            cb_FirstBook.SelectedIndex = 0;

            // 분류기호
            string[] divType = { "KDC", "DDC" };
            cb_divType.Items.AddRange(divType);
            cb_divType.SelectedIndex = 0;
            #endregion
        }

        private void btn_Edit_Save_Click(object sender, EventArgs e)
        {
            string[] update_col = { "work_list", "first_Author", "symbol_Author", "book_Author", "divType", "divNum" };
            string[] update_data = { tb_listName.Text, cb_FirstAuthor.Text, cb_authorType.Text, cb_FirstBook.Text, cb_divType.Text, cb_divNum.Text };
            string[] search_col = { "idx" };
            string[] search_data = { IDX };
            string cmd = db.More_Update("Specs_List", update_col, update_data, search_col, search_data);
            db.DB_Send_CMD_reVoid(cmd);
            MessageBox.Show("저장완료되었습니다.");
            sub.btn_Search_Click(null, null);
            this.Close();
        }

        private void cb_divType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cb_divNum.Items.Clear();
            if (((ComboBox)sender).SelectedIndex == 0)
            {
                string[] divNum = { "4", "5", "6" };
                cb_divNum.Items.AddRange(divNum);
            }
            else
            {
                string[] divNum = { "21", "22", "23" };
                cb_divNum.Items.AddRange(divNum);
            }
        }

        private void btn_Edit_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
