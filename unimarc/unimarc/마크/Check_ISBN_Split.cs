﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
using WindowsFormsApp1.Mac;

namespace UniMarc.마크
{
    public partial class Check_ISBN_Split : Form
    {
        /// <summary>
        /// 번호 / 도서명 / 저자 / 출판사 / 정가 / 수량
        /// </summary>
        public string[] book_data = { "", "", "", "", "", "" };
        public string compidx;
        public int row_idx;
        Check_ISBN isbn;
        Helper_DB db = new Helper_DB();
        public Check_ISBN_Split()
        {
            InitializeComponent();
        }
        public Check_ISBN_Split(Check_ISBN _isbn)
        {
            InitializeComponent();
            isbn = _isbn;
        }

        private void Check_ISBN_Split_Load(object sender, EventArgs e)
        {
            db.DBcon();
            TextBox[] tb = { tb_num, tb_book_name, tb_author, tb_book_comp, tb_price, tb_count };
            for (int a = 0; a < tb.Length; a++)
            {
                tb[a].Text = book_data[a];
            }

            tb_c_book_name.Text = tb_book_name.Text;
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid sg = new Skill_Grid();
            sg.Print_Grid_Num(sender, e);
        }

        private void btn_Split_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            int start = Convert.ToInt32(num_Start.Value);
            string[] grid = { book_data[0], tb_c_book_name.Text, book_data[2], book_data[3], book_data[4], book_data[5], "" };

            for (int a = 0; a < num_Line.Value; a++)
            {
                grid[1] = string.Format("{0} {1}", tb_c_book_name.Text, start);
                grid[5] = num_Count.Value.ToString();
                grid[6] = Total(grid[4], grid[5]);

                start += 1;
                dataGridView1.Rows.Add(grid);
            }
        }
        #region btn_Split_Sub
        private string Total(string str_count, string str_price)
        {
            int count = Convert.ToInt32(Regex.Replace(str_count, @"[^0-9]", ""));
            int price = Convert.ToInt32(Regex.Replace(str_price, @"[^0-9]", ""));

            int result = count * price;
            return result.ToString();
        }
        #endregion

        private void btn_Save_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(num_Line.Value) - 1;
            for (int a = count; a >= 0; a--)
            {
                string[] grid_data = {
                    dataGridView1.Rows[a].Cells["num"].Value.ToString(),        // 번호
                    dataGridView1.Rows[a].Cells["book_name"].Value.ToString(),  // 도서명
                    dataGridView1.Rows[a].Cells["author"].Value.ToString(),     // 저자
                    dataGridView1.Rows[a].Cells["book_comp"].Value.ToString(),  // 출판사
                    dataGridView1.Rows[a].Cells["count"].Value.ToString(),      // 수량
                    dataGridView1.Rows[a].Cells["price"].Value.ToString(),      // 가격
                    dataGridView1.Rows[a].Cells["total"].Value.ToString()       // 합계
                };
                string[] grid = {
                    grid_data[0], "", grid_data[1], "", grid_data[2],
                    "", grid_data[3], "", grid_data[4], grid_data[5],
                    grid_data[6], "", "", "", "",
                    "", "", "", "", ""
                };

                Set_Insert(grid_data);

                isbn.dataGridView1.Rows.Insert(row_idx + 1, grid);
            }
            btn_Close_Click(null, null);
        }
        #region Save_Sub
        /// <summary>
        /// 세트도서에 해당하는 DB를 먼저 Select한 후
        /// 그 정보를 set_book_name만 수정하여 Insert한다
        /// </summary>
        private void Set_Insert(string[] grid_data)
        {
            string list_name = isbn.list_name;
            string Area = "`compidx`, `list_name`, `date`, `header`, `num`, `book_name`";
            string[] Search_col = {
                "list_name", "compidx", "book_name", "author", "book_comp", "pay", "count"
            };
            string[] Search_data = { 
                list_name, compidx, book_data[1], book_data[2], book_data[3], book_data[4], book_data[5] 
            };

            string cmd = db.More_DB_Search("Obj_List_Book", Search_col, Search_data, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] tmp_db = db_res.Split('|');

            List<string> Search_List = new List<string>();
            for (int a = 0; a < tmp_db.Length - 1; a++) {
                Search_List.Add(tmp_db[a]);
            }
            for (int a = 1; a < grid_data.Length; a++) {
                Search_List.Add(grid_data[a]);
            }
            Search_List.Add("세트분할");

            // 회사인덱스, 목록명, 목록일자, 헤더, 넘버, 도서명, 세트도서명, 저자, 출판사, 수량, 단가, 합계, 비고
            tmp_db = Search_List.ToArray();
            string[] Insert_col = {
                "compidx", "list_name", "date", "header", "num", 
                "book_name", "set_book_name", "author", "book_comp", "count", 
                "pay", "total", "etc"
            };
            // 정가 != 단가 일경우 INSERT 오류 발생.
            string Incmd = db.DB_INSERT("Obj_List_Book", Insert_col, tmp_db);
            db.DB_Send_CMD_reVoid(Incmd);
        }
        #endregion

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            tb_c_book_name.Text = tb_book_name.Text;
            num_Start.Value = 1;
            num_Line.Value = 1;
            num_Count.Value = 1;
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
