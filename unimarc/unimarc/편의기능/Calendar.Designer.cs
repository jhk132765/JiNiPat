﻿namespace WindowsFormsApp1.Convenience
{
    partial class Calendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Year = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Btn_After = new System.Windows.Forms.Button();
            this.Btn_Before = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.lbl_1_4 = new System.Windows.Forms.Label();
            this.lbl_Day_1_4 = new System.Windows.Forms.Label();
            this.rb_1_4 = new System.Windows.Forms.RichTextBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.lbl_1_2 = new System.Windows.Forms.Label();
            this.lbl_Day_1_2 = new System.Windows.Forms.Label();
            this.rb_1_2 = new System.Windows.Forms.RichTextBox();
            this.panel35 = new System.Windows.Forms.Panel();
            this.lbl_4_6 = new System.Windows.Forms.Label();
            this.lbl_Day_4_6 = new System.Windows.Forms.Label();
            this.rb_4_6 = new System.Windows.Forms.RichTextBox();
            this.panel34 = new System.Windows.Forms.Panel();
            this.lbl_4_5 = new System.Windows.Forms.Label();
            this.lbl_Day_4_5 = new System.Windows.Forms.Label();
            this.rb_4_5 = new System.Windows.Forms.RichTextBox();
            this.panel33 = new System.Windows.Forms.Panel();
            this.lbl_4_4 = new System.Windows.Forms.Label();
            this.lbl_Day_4_4 = new System.Windows.Forms.Label();
            this.rb_4_4 = new System.Windows.Forms.RichTextBox();
            this.panel32 = new System.Windows.Forms.Panel();
            this.lbl_4_3 = new System.Windows.Forms.Label();
            this.lbl_Day_4_3 = new System.Windows.Forms.Label();
            this.rb_4_3 = new System.Windows.Forms.RichTextBox();
            this.panel31 = new System.Windows.Forms.Panel();
            this.lbl_4_2 = new System.Windows.Forms.Label();
            this.lbl_Day_4_2 = new System.Windows.Forms.Label();
            this.rb_4_2 = new System.Windows.Forms.RichTextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lbl_2_6 = new System.Windows.Forms.Label();
            this.lbl_Day_2_6 = new System.Windows.Forms.Label();
            this.rb_2_6 = new System.Windows.Forms.RichTextBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.lbl_4_1 = new System.Windows.Forms.Label();
            this.lbl_Day_4_1 = new System.Windows.Forms.Label();
            this.rb_4_1 = new System.Windows.Forms.RichTextBox();
            this.panel29 = new System.Windows.Forms.Panel();
            this.lbl_3_1 = new System.Windows.Forms.Label();
            this.lbl_Day_3_1 = new System.Windows.Forms.Label();
            this.rb_3_1 = new System.Windows.Forms.RichTextBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.lbl_3_2 = new System.Windows.Forms.Label();
            this.lbl_Day_3_2 = new System.Windows.Forms.Label();
            this.rb_3_2 = new System.Windows.Forms.RichTextBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.lbl_3_3 = new System.Windows.Forms.Label();
            this.lbl_Day_3_3 = new System.Windows.Forms.Label();
            this.rb_3_3 = new System.Windows.Forms.RichTextBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.lbl_3_4 = new System.Windows.Forms.Label();
            this.lbl_Day_3_4 = new System.Windows.Forms.Label();
            this.rb_3_4 = new System.Windows.Forms.RichTextBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.lbl_2_4 = new System.Windows.Forms.Label();
            this.lbl_Day_2_4 = new System.Windows.Forms.Label();
            this.rb_2_4 = new System.Windows.Forms.RichTextBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.lbl_2_3 = new System.Windows.Forms.Label();
            this.lbl_Day_2_3 = new System.Windows.Forms.Label();
            this.rb_2_3 = new System.Windows.Forms.RichTextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.lbl_2_2 = new System.Windows.Forms.Label();
            this.lbl_Day_2_2 = new System.Windows.Forms.Label();
            this.rb_2_2 = new System.Windows.Forms.RichTextBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.lbl_2_1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbl_Day_2_1 = new System.Windows.Forms.Label();
            this.rb_2_1 = new System.Windows.Forms.RichTextBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.lbl_1_3 = new System.Windows.Forms.Label();
            this.lbl_Day_1_3 = new System.Windows.Forms.Label();
            this.rb_1_3 = new System.Windows.Forms.RichTextBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.lbl_1_1 = new System.Windows.Forms.Label();
            this.lbl_Day_1_1 = new System.Windows.Forms.Label();
            this.rb_1_1 = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_1_5 = new System.Windows.Forms.Label();
            this.lbl_Day_1_5 = new System.Windows.Forms.Label();
            this.rb_1_5 = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lbl_1_7 = new System.Windows.Forms.Label();
            this.lbl_Day_1_7 = new System.Windows.Forms.Label();
            this.rb_1_7 = new System.Windows.Forms.RichTextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lbl_1_6 = new System.Windows.Forms.Label();
            this.lbl_Day_1_6 = new System.Windows.Forms.Label();
            this.rb_1_6 = new System.Windows.Forms.RichTextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lbl_3_7 = new System.Windows.Forms.Label();
            this.lbl_Day_3_7 = new System.Windows.Forms.Label();
            this.rb_3_7 = new System.Windows.Forms.RichTextBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.lbl_3_6 = new System.Windows.Forms.Label();
            this.lbl_Day_3_6 = new System.Windows.Forms.Label();
            this.rb_3_6 = new System.Windows.Forms.RichTextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.lbl_2_5 = new System.Windows.Forms.Label();
            this.lbl_Day_2_5 = new System.Windows.Forms.Label();
            this.rb_2_5 = new System.Windows.Forms.RichTextBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.lbl_3_5 = new System.Windows.Forms.Label();
            this.lbl_Day_3_5 = new System.Windows.Forms.Label();
            this.rb_3_5 = new System.Windows.Forms.RichTextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.lbl_4_7 = new System.Windows.Forms.Label();
            this.lbl_Day_4_7 = new System.Windows.Forms.Label();
            this.rb_4_7 = new System.Windows.Forms.RichTextBox();
            this.panel36 = new System.Windows.Forms.Panel();
            this.lbl_5_1 = new System.Windows.Forms.Label();
            this.lbl_Day_5_1 = new System.Windows.Forms.Label();
            this.rb_5_1 = new System.Windows.Forms.RichTextBox();
            this.panel37 = new System.Windows.Forms.Panel();
            this.lbl_5_2 = new System.Windows.Forms.Label();
            this.lbl_Day_5_2 = new System.Windows.Forms.Label();
            this.rb_5_2 = new System.Windows.Forms.RichTextBox();
            this.panel38 = new System.Windows.Forms.Panel();
            this.lbl_5_3 = new System.Windows.Forms.Label();
            this.lbl_Day_5_3 = new System.Windows.Forms.Label();
            this.rb_5_3 = new System.Windows.Forms.RichTextBox();
            this.panel39 = new System.Windows.Forms.Panel();
            this.lbl_5_4 = new System.Windows.Forms.Label();
            this.lbl_Day_5_4 = new System.Windows.Forms.Label();
            this.rb_5_4 = new System.Windows.Forms.RichTextBox();
            this.panel40 = new System.Windows.Forms.Panel();
            this.lbl_5_5 = new System.Windows.Forms.Label();
            this.lbl_Day_5_5 = new System.Windows.Forms.Label();
            this.rb_5_5 = new System.Windows.Forms.RichTextBox();
            this.panel41 = new System.Windows.Forms.Panel();
            this.lbl_5_6 = new System.Windows.Forms.Label();
            this.lbl_Day_5_6 = new System.Windows.Forms.Label();
            this.rb_5_6 = new System.Windows.Forms.RichTextBox();
            this.panel42 = new System.Windows.Forms.Panel();
            this.lbl_5_7 = new System.Windows.Forms.Label();
            this.lbl_Day_5_7 = new System.Windows.Forms.Label();
            this.rb_5_7 = new System.Windows.Forms.RichTextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lbl_2_7 = new System.Windows.Forms.Label();
            this.lbl_Day_2_7 = new System.Windows.Forms.Label();
            this.rb_2_7 = new System.Windows.Forms.RichTextBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.lbl_Month = new System.Windows.Forms.Label();
            this.rb_all = new System.Windows.Forms.RadioButton();
            this.rb_dly = new System.Windows.Forms.RadioButton();
            this.rb_marc = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_Year
            // 
            this.lbl_Year.AutoSize = true;
            this.lbl_Year.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Year.Location = new System.Drawing.Point(15, 16);
            this.lbl_Year.Name = "lbl_Year";
            this.lbl_Year.Size = new System.Drawing.Size(72, 27);
            this.lbl_Year.TabIndex = 2;
            this.lbl_Year.Text = "Year";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(303, 15);
            this.dateTimePicker1.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(164, 21);
            this.dateTimePicker1.TabIndex = 8;
            this.dateTimePicker1.Value = new System.DateTime(2022, 8, 8, 0, 0, 0, 0);
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // Btn_After
            // 
            this.Btn_After.Location = new System.Drawing.Point(641, 7);
            this.Btn_After.Name = "Btn_After";
            this.Btn_After.Size = new System.Drawing.Size(92, 41);
            this.Btn_After.TabIndex = 7;
            this.Btn_After.Text = "한달 후";
            this.Btn_After.UseVisualStyleBackColor = true;
            this.Btn_After.Click += new System.EventHandler(this.Btn_After_Click);
            // 
            // Btn_Before
            // 
            this.Btn_Before.Location = new System.Drawing.Point(536, 7);
            this.Btn_Before.Name = "Btn_Before";
            this.Btn_Before.Size = new System.Drawing.Size(92, 41);
            this.Btn_Before.TabIndex = 6;
            this.Btn_Before.Text = "한달 전";
            this.Btn_Before.UseVisualStyleBackColor = true;
            this.Btn_Before.Click += new System.EventHandler(this.Btn_Before_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Controls.Add(this.panel20, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel19, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel35, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel34, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel33, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel32, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel31, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel12, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel30, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel29, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel28, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel27, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel26, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel25, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel24, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel23, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel21, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel22, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel18, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel9, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel10, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel13, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel14, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel15, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel16, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel17, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel36, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel37, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel38, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel39, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel40, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel41, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel42, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel11, 6, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 53);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1337, 799);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.lbl_1_4);
            this.panel20.Controls.Add(this.lbl_Day_1_4);
            this.panel20.Controls.Add(this.rb_1_4);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(551, 39);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(187, 143);
            this.panel20.TabIndex = 2;
            // 
            // lbl_1_4
            // 
            this.lbl_1_4.AutoSize = true;
            this.lbl_1_4.Location = new System.Drawing.Point(82, 1);
            this.lbl_1_4.Name = "lbl_1_4";
            this.lbl_1_4.Size = new System.Drawing.Size(38, 12);
            this.lbl_1_4.TabIndex = 1;
            this.lbl_1_4.Text = "label8";
            this.lbl_1_4.Visible = false;
            // 
            // lbl_Day_1_4
            // 
            this.lbl_Day_1_4.AutoSize = true;
            this.lbl_Day_1_4.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_1_4.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_1_4.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_1_4.Name = "lbl_Day_1_4";
            this.lbl_Day_1_4.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_1_4.TabIndex = 0;
            this.lbl_Day_1_4.Text = "0";
            // 
            // rb_1_4
            // 
            this.rb_1_4.BackColor = System.Drawing.SystemColors.Control;
            this.rb_1_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_1_4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_1_4.Location = new System.Drawing.Point(0, 15);
            this.rb_1_4.Name = "rb_1_4";
            this.rb_1_4.ReadOnly = true;
            this.rb_1_4.Size = new System.Drawing.Size(187, 128);
            this.rb_1_4.TabIndex = 0;
            this.rb_1_4.TabStop = false;
            this.rb_1_4.Text = "";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.lbl_1_2);
            this.panel19.Controls.Add(this.lbl_Day_1_2);
            this.panel19.Controls.Add(this.rb_1_2);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(159, 39);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(187, 143);
            this.panel19.TabIndex = 16;
            // 
            // lbl_1_2
            // 
            this.lbl_1_2.AutoSize = true;
            this.lbl_1_2.Location = new System.Drawing.Point(80, 1);
            this.lbl_1_2.Name = "lbl_1_2";
            this.lbl_1_2.Size = new System.Drawing.Size(38, 12);
            this.lbl_1_2.TabIndex = 1;
            this.lbl_1_2.Text = "label8";
            this.lbl_1_2.Visible = false;
            // 
            // lbl_Day_1_2
            // 
            this.lbl_Day_1_2.AutoSize = true;
            this.lbl_Day_1_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_1_2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_1_2.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_1_2.Name = "lbl_Day_1_2";
            this.lbl_Day_1_2.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_1_2.TabIndex = 0;
            this.lbl_Day_1_2.Text = "0";
            // 
            // rb_1_2
            // 
            this.rb_1_2.BackColor = System.Drawing.SystemColors.Control;
            this.rb_1_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_1_2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_1_2.Location = new System.Drawing.Point(0, 15);
            this.rb_1_2.Name = "rb_1_2";
            this.rb_1_2.ReadOnly = true;
            this.rb_1_2.Size = new System.Drawing.Size(187, 128);
            this.rb_1_2.TabIndex = 0;
            this.rb_1_2.TabStop = false;
            this.rb_1_2.Text = "";
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.lbl_4_6);
            this.panel35.Controls.Add(this.lbl_Day_4_6);
            this.panel35.Controls.Add(this.rb_4_6);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel35.Location = new System.Drawing.Point(943, 495);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(187, 143);
            this.panel35.TabIndex = 15;
            // 
            // lbl_4_6
            // 
            this.lbl_4_6.AutoSize = true;
            this.lbl_4_6.Location = new System.Drawing.Point(83, 1);
            this.lbl_4_6.Name = "lbl_4_6";
            this.lbl_4_6.Size = new System.Drawing.Size(38, 12);
            this.lbl_4_6.TabIndex = 1;
            this.lbl_4_6.Text = "label8";
            this.lbl_4_6.Visible = false;
            // 
            // lbl_Day_4_6
            // 
            this.lbl_Day_4_6.AutoSize = true;
            this.lbl_Day_4_6.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_4_6.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_4_6.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_4_6.Name = "lbl_Day_4_6";
            this.lbl_Day_4_6.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_4_6.TabIndex = 0;
            this.lbl_Day_4_6.Text = "0";
            // 
            // rb_4_6
            // 
            this.rb_4_6.BackColor = System.Drawing.SystemColors.Control;
            this.rb_4_6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_4_6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_4_6.Location = new System.Drawing.Point(0, 15);
            this.rb_4_6.Name = "rb_4_6";
            this.rb_4_6.ReadOnly = true;
            this.rb_4_6.Size = new System.Drawing.Size(187, 128);
            this.rb_4_6.TabIndex = 0;
            this.rb_4_6.TabStop = false;
            this.rb_4_6.Text = "";
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.lbl_4_5);
            this.panel34.Controls.Add(this.lbl_Day_4_5);
            this.panel34.Controls.Add(this.rb_4_5);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel34.Location = new System.Drawing.Point(747, 495);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(187, 143);
            this.panel34.TabIndex = 15;
            // 
            // lbl_4_5
            // 
            this.lbl_4_5.AutoSize = true;
            this.lbl_4_5.Location = new System.Drawing.Point(82, 1);
            this.lbl_4_5.Name = "lbl_4_5";
            this.lbl_4_5.Size = new System.Drawing.Size(38, 12);
            this.lbl_4_5.TabIndex = 1;
            this.lbl_4_5.Text = "label8";
            this.lbl_4_5.Visible = false;
            // 
            // lbl_Day_4_5
            // 
            this.lbl_Day_4_5.AutoSize = true;
            this.lbl_Day_4_5.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_4_5.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_4_5.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_4_5.Name = "lbl_Day_4_5";
            this.lbl_Day_4_5.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_4_5.TabIndex = 0;
            this.lbl_Day_4_5.Text = "0";
            // 
            // rb_4_5
            // 
            this.rb_4_5.BackColor = System.Drawing.SystemColors.Control;
            this.rb_4_5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_4_5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_4_5.Location = new System.Drawing.Point(0, 15);
            this.rb_4_5.Name = "rb_4_5";
            this.rb_4_5.ReadOnly = true;
            this.rb_4_5.Size = new System.Drawing.Size(187, 128);
            this.rb_4_5.TabIndex = 0;
            this.rb_4_5.TabStop = false;
            this.rb_4_5.Text = "";
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.lbl_4_4);
            this.panel33.Controls.Add(this.lbl_Day_4_4);
            this.panel33.Controls.Add(this.rb_4_4);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel33.Location = new System.Drawing.Point(551, 495);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(187, 143);
            this.panel33.TabIndex = 15;
            // 
            // lbl_4_4
            // 
            this.lbl_4_4.AutoSize = true;
            this.lbl_4_4.Location = new System.Drawing.Point(82, 1);
            this.lbl_4_4.Name = "lbl_4_4";
            this.lbl_4_4.Size = new System.Drawing.Size(38, 12);
            this.lbl_4_4.TabIndex = 1;
            this.lbl_4_4.Text = "label8";
            this.lbl_4_4.Visible = false;
            // 
            // lbl_Day_4_4
            // 
            this.lbl_Day_4_4.AutoSize = true;
            this.lbl_Day_4_4.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_4_4.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_4_4.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_4_4.Name = "lbl_Day_4_4";
            this.lbl_Day_4_4.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_4_4.TabIndex = 0;
            this.lbl_Day_4_4.Text = "0";
            // 
            // rb_4_4
            // 
            this.rb_4_4.BackColor = System.Drawing.SystemColors.Control;
            this.rb_4_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_4_4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_4_4.Location = new System.Drawing.Point(0, 15);
            this.rb_4_4.Name = "rb_4_4";
            this.rb_4_4.ReadOnly = true;
            this.rb_4_4.Size = new System.Drawing.Size(187, 128);
            this.rb_4_4.TabIndex = 0;
            this.rb_4_4.TabStop = false;
            this.rb_4_4.Text = "";
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.lbl_4_3);
            this.panel32.Controls.Add(this.lbl_Day_4_3);
            this.panel32.Controls.Add(this.rb_4_3);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(355, 495);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(187, 143);
            this.panel32.TabIndex = 15;
            // 
            // lbl_4_3
            // 
            this.lbl_4_3.AutoSize = true;
            this.lbl_4_3.Location = new System.Drawing.Point(81, 1);
            this.lbl_4_3.Name = "lbl_4_3";
            this.lbl_4_3.Size = new System.Drawing.Size(38, 12);
            this.lbl_4_3.TabIndex = 1;
            this.lbl_4_3.Text = "label8";
            this.lbl_4_3.Visible = false;
            // 
            // lbl_Day_4_3
            // 
            this.lbl_Day_4_3.AutoSize = true;
            this.lbl_Day_4_3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_4_3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_4_3.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_4_3.Name = "lbl_Day_4_3";
            this.lbl_Day_4_3.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_4_3.TabIndex = 0;
            this.lbl_Day_4_3.Text = "0";
            // 
            // rb_4_3
            // 
            this.rb_4_3.BackColor = System.Drawing.SystemColors.Control;
            this.rb_4_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_4_3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_4_3.Location = new System.Drawing.Point(0, 15);
            this.rb_4_3.Name = "rb_4_3";
            this.rb_4_3.ReadOnly = true;
            this.rb_4_3.Size = new System.Drawing.Size(187, 128);
            this.rb_4_3.TabIndex = 0;
            this.rb_4_3.TabStop = false;
            this.rb_4_3.Text = "";
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.lbl_4_2);
            this.panel31.Controls.Add(this.lbl_Day_4_2);
            this.panel31.Controls.Add(this.rb_4_2);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(159, 495);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(187, 143);
            this.panel31.TabIndex = 15;
            // 
            // lbl_4_2
            // 
            this.lbl_4_2.AutoSize = true;
            this.lbl_4_2.Location = new System.Drawing.Point(80, 1);
            this.lbl_4_2.Name = "lbl_4_2";
            this.lbl_4_2.Size = new System.Drawing.Size(38, 12);
            this.lbl_4_2.TabIndex = 1;
            this.lbl_4_2.Text = "label8";
            this.lbl_4_2.Visible = false;
            // 
            // lbl_Day_4_2
            // 
            this.lbl_Day_4_2.AutoSize = true;
            this.lbl_Day_4_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_4_2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_4_2.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_4_2.Name = "lbl_Day_4_2";
            this.lbl_Day_4_2.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_4_2.TabIndex = 0;
            this.lbl_Day_4_2.Text = "0";
            // 
            // rb_4_2
            // 
            this.rb_4_2.BackColor = System.Drawing.SystemColors.Control;
            this.rb_4_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_4_2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_4_2.Location = new System.Drawing.Point(0, 15);
            this.rb_4_2.Name = "rb_4_2";
            this.rb_4_2.ReadOnly = true;
            this.rb_4_2.Size = new System.Drawing.Size(187, 128);
            this.rb_4_2.TabIndex = 0;
            this.rb_4_2.TabStop = false;
            this.rb_4_2.Text = "";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.lbl_2_6);
            this.panel12.Controls.Add(this.lbl_Day_2_6);
            this.panel12.Controls.Add(this.rb_2_6);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(943, 191);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(187, 143);
            this.panel12.TabIndex = 9;
            // 
            // lbl_2_6
            // 
            this.lbl_2_6.AutoSize = true;
            this.lbl_2_6.Location = new System.Drawing.Point(83, 1);
            this.lbl_2_6.Name = "lbl_2_6";
            this.lbl_2_6.Size = new System.Drawing.Size(38, 12);
            this.lbl_2_6.TabIndex = 1;
            this.lbl_2_6.Text = "label8";
            this.lbl_2_6.Visible = false;
            // 
            // lbl_Day_2_6
            // 
            this.lbl_Day_2_6.AutoSize = true;
            this.lbl_Day_2_6.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_2_6.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_2_6.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_2_6.Name = "lbl_Day_2_6";
            this.lbl_Day_2_6.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_2_6.TabIndex = 0;
            this.lbl_Day_2_6.Text = "0";
            // 
            // rb_2_6
            // 
            this.rb_2_6.BackColor = System.Drawing.SystemColors.Control;
            this.rb_2_6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_2_6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_2_6.Location = new System.Drawing.Point(0, 15);
            this.rb_2_6.Name = "rb_2_6";
            this.rb_2_6.ReadOnly = true;
            this.rb_2_6.Size = new System.Drawing.Size(187, 128);
            this.rb_2_6.TabIndex = 0;
            this.rb_2_6.TabStop = false;
            this.rb_2_6.Text = "";
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.LightCoral;
            this.panel30.Controls.Add(this.lbl_4_1);
            this.panel30.Controls.Add(this.lbl_Day_4_1);
            this.panel30.Controls.Add(this.rb_4_1);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel30.Location = new System.Drawing.Point(6, 495);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(144, 143);
            this.panel30.TabIndex = 15;
            // 
            // lbl_4_1
            // 
            this.lbl_4_1.AutoSize = true;
            this.lbl_4_1.Location = new System.Drawing.Point(58, 1);
            this.lbl_4_1.Name = "lbl_4_1";
            this.lbl_4_1.Size = new System.Drawing.Size(38, 12);
            this.lbl_4_1.TabIndex = 1;
            this.lbl_4_1.Text = "label8";
            this.lbl_4_1.Visible = false;
            // 
            // lbl_Day_4_1
            // 
            this.lbl_Day_4_1.AutoSize = true;
            this.lbl_Day_4_1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_4_1.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_4_1.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_4_1.Name = "lbl_Day_4_1";
            this.lbl_Day_4_1.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_4_1.TabIndex = 0;
            this.lbl_Day_4_1.Text = "0";
            // 
            // rb_4_1
            // 
            this.rb_4_1.BackColor = System.Drawing.Color.LightCoral;
            this.rb_4_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_4_1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_4_1.Location = new System.Drawing.Point(0, 15);
            this.rb_4_1.Name = "rb_4_1";
            this.rb_4_1.ReadOnly = true;
            this.rb_4_1.Size = new System.Drawing.Size(144, 128);
            this.rb_4_1.TabIndex = 0;
            this.rb_4_1.TabStop = false;
            this.rb_4_1.Text = "";
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.LightCoral;
            this.panel29.Controls.Add(this.lbl_3_1);
            this.panel29.Controls.Add(this.lbl_Day_3_1);
            this.panel29.Controls.Add(this.rb_3_1);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(6, 343);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(144, 143);
            this.panel29.TabIndex = 15;
            // 
            // lbl_3_1
            // 
            this.lbl_3_1.AutoSize = true;
            this.lbl_3_1.Location = new System.Drawing.Point(58, 1);
            this.lbl_3_1.Name = "lbl_3_1";
            this.lbl_3_1.Size = new System.Drawing.Size(38, 12);
            this.lbl_3_1.TabIndex = 1;
            this.lbl_3_1.Text = "label8";
            this.lbl_3_1.Visible = false;
            // 
            // lbl_Day_3_1
            // 
            this.lbl_Day_3_1.AutoSize = true;
            this.lbl_Day_3_1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_3_1.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_3_1.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_3_1.Name = "lbl_Day_3_1";
            this.lbl_Day_3_1.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_3_1.TabIndex = 0;
            this.lbl_Day_3_1.Text = "0";
            // 
            // rb_3_1
            // 
            this.rb_3_1.BackColor = System.Drawing.Color.LightCoral;
            this.rb_3_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_3_1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_3_1.Location = new System.Drawing.Point(0, 15);
            this.rb_3_1.Name = "rb_3_1";
            this.rb_3_1.ReadOnly = true;
            this.rb_3_1.Size = new System.Drawing.Size(144, 128);
            this.rb_3_1.TabIndex = 0;
            this.rb_3_1.TabStop = false;
            this.rb_3_1.Text = "";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.lbl_3_2);
            this.panel28.Controls.Add(this.lbl_Day_3_2);
            this.panel28.Controls.Add(this.rb_3_2);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel28.Location = new System.Drawing.Point(159, 343);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(187, 143);
            this.panel28.TabIndex = 15;
            // 
            // lbl_3_2
            // 
            this.lbl_3_2.AutoSize = true;
            this.lbl_3_2.Location = new System.Drawing.Point(80, 1);
            this.lbl_3_2.Name = "lbl_3_2";
            this.lbl_3_2.Size = new System.Drawing.Size(38, 12);
            this.lbl_3_2.TabIndex = 1;
            this.lbl_3_2.Text = "label8";
            this.lbl_3_2.Visible = false;
            // 
            // lbl_Day_3_2
            // 
            this.lbl_Day_3_2.AutoSize = true;
            this.lbl_Day_3_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_3_2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_3_2.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_3_2.Name = "lbl_Day_3_2";
            this.lbl_Day_3_2.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_3_2.TabIndex = 0;
            this.lbl_Day_3_2.Text = "0";
            // 
            // rb_3_2
            // 
            this.rb_3_2.BackColor = System.Drawing.SystemColors.Control;
            this.rb_3_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_3_2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_3_2.Location = new System.Drawing.Point(0, 15);
            this.rb_3_2.Name = "rb_3_2";
            this.rb_3_2.ReadOnly = true;
            this.rb_3_2.Size = new System.Drawing.Size(187, 128);
            this.rb_3_2.TabIndex = 0;
            this.rb_3_2.TabStop = false;
            this.rb_3_2.Text = "";
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.lbl_3_3);
            this.panel27.Controls.Add(this.lbl_Day_3_3);
            this.panel27.Controls.Add(this.rb_3_3);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(355, 343);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(187, 143);
            this.panel27.TabIndex = 15;
            // 
            // lbl_3_3
            // 
            this.lbl_3_3.AutoSize = true;
            this.lbl_3_3.Location = new System.Drawing.Point(81, 1);
            this.lbl_3_3.Name = "lbl_3_3";
            this.lbl_3_3.Size = new System.Drawing.Size(38, 12);
            this.lbl_3_3.TabIndex = 1;
            this.lbl_3_3.Text = "label8";
            this.lbl_3_3.Visible = false;
            // 
            // lbl_Day_3_3
            // 
            this.lbl_Day_3_3.AutoSize = true;
            this.lbl_Day_3_3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_3_3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_3_3.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_3_3.Name = "lbl_Day_3_3";
            this.lbl_Day_3_3.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_3_3.TabIndex = 0;
            this.lbl_Day_3_3.Text = "0";
            // 
            // rb_3_3
            // 
            this.rb_3_3.BackColor = System.Drawing.SystemColors.Control;
            this.rb_3_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_3_3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_3_3.Location = new System.Drawing.Point(0, 15);
            this.rb_3_3.Name = "rb_3_3";
            this.rb_3_3.ReadOnly = true;
            this.rb_3_3.Size = new System.Drawing.Size(187, 128);
            this.rb_3_3.TabIndex = 0;
            this.rb_3_3.TabStop = false;
            this.rb_3_3.Text = "";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.lbl_3_4);
            this.panel26.Controls.Add(this.lbl_Day_3_4);
            this.panel26.Controls.Add(this.rb_3_4);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(551, 343);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(187, 143);
            this.panel26.TabIndex = 15;
            // 
            // lbl_3_4
            // 
            this.lbl_3_4.AutoSize = true;
            this.lbl_3_4.Location = new System.Drawing.Point(82, 1);
            this.lbl_3_4.Name = "lbl_3_4";
            this.lbl_3_4.Size = new System.Drawing.Size(38, 12);
            this.lbl_3_4.TabIndex = 1;
            this.lbl_3_4.Text = "label8";
            this.lbl_3_4.Visible = false;
            // 
            // lbl_Day_3_4
            // 
            this.lbl_Day_3_4.AutoSize = true;
            this.lbl_Day_3_4.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_3_4.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_3_4.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_3_4.Name = "lbl_Day_3_4";
            this.lbl_Day_3_4.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_3_4.TabIndex = 0;
            this.lbl_Day_3_4.Text = "0";
            // 
            // rb_3_4
            // 
            this.rb_3_4.BackColor = System.Drawing.SystemColors.Control;
            this.rb_3_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_3_4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_3_4.Location = new System.Drawing.Point(0, 15);
            this.rb_3_4.Name = "rb_3_4";
            this.rb_3_4.ReadOnly = true;
            this.rb_3_4.Size = new System.Drawing.Size(187, 128);
            this.rb_3_4.TabIndex = 0;
            this.rb_3_4.TabStop = false;
            this.rb_3_4.Text = "";
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.lbl_2_4);
            this.panel25.Controls.Add(this.lbl_Day_2_4);
            this.panel25.Controls.Add(this.rb_2_4);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(551, 191);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(187, 143);
            this.panel25.TabIndex = 15;
            // 
            // lbl_2_4
            // 
            this.lbl_2_4.AutoSize = true;
            this.lbl_2_4.Location = new System.Drawing.Point(82, 1);
            this.lbl_2_4.Name = "lbl_2_4";
            this.lbl_2_4.Size = new System.Drawing.Size(38, 12);
            this.lbl_2_4.TabIndex = 1;
            this.lbl_2_4.Text = "label8";
            this.lbl_2_4.Visible = false;
            // 
            // lbl_Day_2_4
            // 
            this.lbl_Day_2_4.AutoSize = true;
            this.lbl_Day_2_4.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_2_4.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_2_4.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_2_4.Name = "lbl_Day_2_4";
            this.lbl_Day_2_4.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_2_4.TabIndex = 0;
            this.lbl_Day_2_4.Text = "0";
            // 
            // rb_2_4
            // 
            this.rb_2_4.BackColor = System.Drawing.SystemColors.Control;
            this.rb_2_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_2_4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_2_4.Location = new System.Drawing.Point(0, 15);
            this.rb_2_4.Name = "rb_2_4";
            this.rb_2_4.ReadOnly = true;
            this.rb_2_4.Size = new System.Drawing.Size(187, 128);
            this.rb_2_4.TabIndex = 0;
            this.rb_2_4.TabStop = false;
            this.rb_2_4.Text = "";
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.lbl_2_3);
            this.panel24.Controls.Add(this.lbl_Day_2_3);
            this.panel24.Controls.Add(this.rb_2_3);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(355, 191);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(187, 143);
            this.panel24.TabIndex = 15;
            // 
            // lbl_2_3
            // 
            this.lbl_2_3.AutoSize = true;
            this.lbl_2_3.Location = new System.Drawing.Point(81, 1);
            this.lbl_2_3.Name = "lbl_2_3";
            this.lbl_2_3.Size = new System.Drawing.Size(38, 12);
            this.lbl_2_3.TabIndex = 1;
            this.lbl_2_3.Text = "label8";
            this.lbl_2_3.Visible = false;
            // 
            // lbl_Day_2_3
            // 
            this.lbl_Day_2_3.AutoSize = true;
            this.lbl_Day_2_3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_2_3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_2_3.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_2_3.Name = "lbl_Day_2_3";
            this.lbl_Day_2_3.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_2_3.TabIndex = 0;
            this.lbl_Day_2_3.Text = "0";
            // 
            // rb_2_3
            // 
            this.rb_2_3.BackColor = System.Drawing.SystemColors.Control;
            this.rb_2_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_2_3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_2_3.Location = new System.Drawing.Point(0, 15);
            this.rb_2_3.Name = "rb_2_3";
            this.rb_2_3.ReadOnly = true;
            this.rb_2_3.Size = new System.Drawing.Size(187, 128);
            this.rb_2_3.TabIndex = 0;
            this.rb_2_3.TabStop = false;
            this.rb_2_3.Text = "";
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.lbl_2_2);
            this.panel23.Controls.Add(this.lbl_Day_2_2);
            this.panel23.Controls.Add(this.rb_2_2);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(159, 191);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(187, 143);
            this.panel23.TabIndex = 15;
            // 
            // lbl_2_2
            // 
            this.lbl_2_2.AutoSize = true;
            this.lbl_2_2.Location = new System.Drawing.Point(79, 1);
            this.lbl_2_2.Name = "lbl_2_2";
            this.lbl_2_2.Size = new System.Drawing.Size(38, 12);
            this.lbl_2_2.TabIndex = 1;
            this.lbl_2_2.Text = "label8";
            this.lbl_2_2.Visible = false;
            // 
            // lbl_Day_2_2
            // 
            this.lbl_Day_2_2.AutoSize = true;
            this.lbl_Day_2_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_2_2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_2_2.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_2_2.Name = "lbl_Day_2_2";
            this.lbl_Day_2_2.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_2_2.TabIndex = 0;
            this.lbl_Day_2_2.Text = "0";
            // 
            // rb_2_2
            // 
            this.rb_2_2.BackColor = System.Drawing.SystemColors.Control;
            this.rb_2_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_2_2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_2_2.Location = new System.Drawing.Point(0, 15);
            this.rb_2_2.Name = "rb_2_2";
            this.rb_2_2.ReadOnly = true;
            this.rb_2_2.Size = new System.Drawing.Size(187, 128);
            this.rb_2_2.TabIndex = 0;
            this.rb_2_2.TabStop = false;
            this.rb_2_2.Text = "";
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.LightCoral;
            this.panel21.Controls.Add(this.lbl_2_1);
            this.panel21.Controls.Add(this.label14);
            this.panel21.Controls.Add(this.lbl_Day_2_1);
            this.panel21.Controls.Add(this.rb_2_1);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(6, 191);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(144, 143);
            this.panel21.TabIndex = 15;
            // 
            // lbl_2_1
            // 
            this.lbl_2_1.AutoSize = true;
            this.lbl_2_1.Location = new System.Drawing.Point(58, 1);
            this.lbl_2_1.Name = "lbl_2_1";
            this.lbl_2_1.Size = new System.Drawing.Size(38, 12);
            this.lbl_2_1.TabIndex = 1;
            this.lbl_2_1.Text = "label8";
            this.lbl_2_1.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1216, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 12);
            this.label14.TabIndex = 1;
            this.label14.Text = "label8";
            // 
            // lbl_Day_2_1
            // 
            this.lbl_Day_2_1.AutoSize = true;
            this.lbl_Day_2_1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_2_1.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_2_1.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_2_1.Name = "lbl_Day_2_1";
            this.lbl_Day_2_1.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_2_1.TabIndex = 0;
            this.lbl_Day_2_1.Text = "0";
            // 
            // rb_2_1
            // 
            this.rb_2_1.BackColor = System.Drawing.Color.LightCoral;
            this.rb_2_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_2_1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_2_1.Location = new System.Drawing.Point(0, 15);
            this.rb_2_1.Name = "rb_2_1";
            this.rb_2_1.ReadOnly = true;
            this.rb_2_1.Size = new System.Drawing.Size(144, 128);
            this.rb_2_1.TabIndex = 0;
            this.rb_2_1.TabStop = false;
            this.rb_2_1.Text = "";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.lbl_1_3);
            this.panel22.Controls.Add(this.lbl_Day_1_3);
            this.panel22.Controls.Add(this.rb_1_3);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel22.Location = new System.Drawing.Point(355, 39);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(187, 143);
            this.panel22.TabIndex = 15;
            // 
            // lbl_1_3
            // 
            this.lbl_1_3.AutoSize = true;
            this.lbl_1_3.Location = new System.Drawing.Point(81, 1);
            this.lbl_1_3.Name = "lbl_1_3";
            this.lbl_1_3.Size = new System.Drawing.Size(38, 12);
            this.lbl_1_3.TabIndex = 1;
            this.lbl_1_3.Text = "label8";
            this.lbl_1_3.Visible = false;
            // 
            // lbl_Day_1_3
            // 
            this.lbl_Day_1_3.AutoSize = true;
            this.lbl_Day_1_3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_1_3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_1_3.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_1_3.Name = "lbl_Day_1_3";
            this.lbl_Day_1_3.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_1_3.TabIndex = 0;
            this.lbl_Day_1_3.Text = "0";
            // 
            // rb_1_3
            // 
            this.rb_1_3.BackColor = System.Drawing.SystemColors.Control;
            this.rb_1_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_1_3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_1_3.Location = new System.Drawing.Point(0, 15);
            this.rb_1_3.Name = "rb_1_3";
            this.rb_1_3.ReadOnly = true;
            this.rb_1_3.Size = new System.Drawing.Size(187, 128);
            this.rb_1_3.TabIndex = 0;
            this.rb_1_3.TabStop = false;
            this.rb_1_3.Text = "";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.LightCoral;
            this.panel18.Controls.Add(this.lbl_1_1);
            this.panel18.Controls.Add(this.lbl_Day_1_1);
            this.panel18.Controls.Add(this.rb_1_1);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(6, 39);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(144, 143);
            this.panel18.TabIndex = 15;
            // 
            // lbl_1_1
            // 
            this.lbl_1_1.AutoSize = true;
            this.lbl_1_1.Location = new System.Drawing.Point(58, 1);
            this.lbl_1_1.Name = "lbl_1_1";
            this.lbl_1_1.Size = new System.Drawing.Size(38, 12);
            this.lbl_1_1.TabIndex = 1;
            this.lbl_1_1.Text = "label8";
            this.lbl_1_1.Visible = false;
            // 
            // lbl_Day_1_1
            // 
            this.lbl_Day_1_1.AutoSize = true;
            this.lbl_Day_1_1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_1_1.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_1_1.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_1_1.Name = "lbl_Day_1_1";
            this.lbl_Day_1_1.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_1_1.TabIndex = 0;
            this.lbl_Day_1_1.Text = "0";
            // 
            // rb_1_1
            // 
            this.rb_1_1.BackColor = System.Drawing.Color.LightCoral;
            this.rb_1_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_1_1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_1_1.Location = new System.Drawing.Point(0, 15);
            this.rb_1_1.Name = "rb_1_1";
            this.rb_1_1.ReadOnly = true;
            this.rb_1_1.Size = new System.Drawing.Size(144, 128);
            this.rb_1_1.TabIndex = 0;
            this.rb_1_1.TabStop = false;
            this.rb_1_1.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl_1_5);
            this.panel1.Controls.Add(this.lbl_Day_1_5);
            this.panel1.Controls.Add(this.rb_1_5);
            this.panel1.Location = new System.Drawing.Point(747, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(181, 143);
            this.panel1.TabIndex = 1;
            // 
            // lbl_1_5
            // 
            this.lbl_1_5.AutoSize = true;
            this.lbl_1_5.Location = new System.Drawing.Point(82, 1);
            this.lbl_1_5.Name = "lbl_1_5";
            this.lbl_1_5.Size = new System.Drawing.Size(38, 12);
            this.lbl_1_5.TabIndex = 1;
            this.lbl_1_5.Text = "label8";
            this.lbl_1_5.Visible = false;
            // 
            // lbl_Day_1_5
            // 
            this.lbl_Day_1_5.AutoSize = true;
            this.lbl_Day_1_5.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_1_5.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_1_5.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_1_5.Name = "lbl_Day_1_5";
            this.lbl_Day_1_5.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_1_5.TabIndex = 0;
            this.lbl_Day_1_5.Text = "0";
            // 
            // rb_1_5
            // 
            this.rb_1_5.BackColor = System.Drawing.SystemColors.Control;
            this.rb_1_5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_1_5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_1_5.Location = new System.Drawing.Point(0, 15);
            this.rb_1_5.Name = "rb_1_5";
            this.rb_1_5.ReadOnly = true;
            this.rb_1_5.Size = new System.Drawing.Size(181, 128);
            this.rb_1_5.TabIndex = 0;
            this.rb_1_5.TabStop = false;
            this.rb_1_5.Text = "";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightCoral;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(144, 24);
            this.panel2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(58, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "일";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(159, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(187, 24);
            this.panel3.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(80, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "월";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(355, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(187, 24);
            this.panel4.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(81, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "화";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel5.Controls.Add(this.label4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(551, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(187, 24);
            this.panel5.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(82, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "수";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel6.Controls.Add(this.label5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(747, 6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(187, 24);
            this.panel6.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(82, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "목";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel7.Controls.Add(this.label6);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(943, 6);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(187, 24);
            this.panel7.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(83, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "금";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel8.Controls.Add(this.label7);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(1139, 6);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(192, 24);
            this.panel8.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(82, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "토";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel9.Controls.Add(this.lbl_1_7);
            this.panel9.Controls.Add(this.lbl_Day_1_7);
            this.panel9.Controls.Add(this.rb_1_7);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(1139, 39);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(192, 143);
            this.panel9.TabIndex = 6;
            // 
            // lbl_1_7
            // 
            this.lbl_1_7.AutoSize = true;
            this.lbl_1_7.Location = new System.Drawing.Point(82, 1);
            this.lbl_1_7.Name = "lbl_1_7";
            this.lbl_1_7.Size = new System.Drawing.Size(38, 12);
            this.lbl_1_7.TabIndex = 1;
            this.lbl_1_7.Text = "label8";
            this.lbl_1_7.Visible = false;
            // 
            // lbl_Day_1_7
            // 
            this.lbl_Day_1_7.AutoSize = true;
            this.lbl_Day_1_7.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_1_7.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_1_7.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_1_7.Name = "lbl_Day_1_7";
            this.lbl_Day_1_7.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_1_7.TabIndex = 0;
            this.lbl_Day_1_7.Text = "0";
            // 
            // rb_1_7
            // 
            this.rb_1_7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rb_1_7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_1_7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_1_7.Location = new System.Drawing.Point(0, 15);
            this.rb_1_7.Name = "rb_1_7";
            this.rb_1_7.ReadOnly = true;
            this.rb_1_7.Size = new System.Drawing.Size(192, 128);
            this.rb_1_7.TabIndex = 0;
            this.rb_1_7.TabStop = false;
            this.rb_1_7.Text = "";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.lbl_1_6);
            this.panel10.Controls.Add(this.lbl_Day_1_6);
            this.panel10.Controls.Add(this.rb_1_6);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(943, 39);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(187, 143);
            this.panel10.TabIndex = 7;
            // 
            // lbl_1_6
            // 
            this.lbl_1_6.AutoSize = true;
            this.lbl_1_6.Location = new System.Drawing.Point(83, 1);
            this.lbl_1_6.Name = "lbl_1_6";
            this.lbl_1_6.Size = new System.Drawing.Size(38, 12);
            this.lbl_1_6.TabIndex = 1;
            this.lbl_1_6.Text = "label8";
            this.lbl_1_6.Visible = false;
            // 
            // lbl_Day_1_6
            // 
            this.lbl_Day_1_6.AutoSize = true;
            this.lbl_Day_1_6.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_1_6.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_1_6.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_1_6.Name = "lbl_Day_1_6";
            this.lbl_Day_1_6.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_1_6.TabIndex = 0;
            this.lbl_Day_1_6.Text = "0";
            // 
            // rb_1_6
            // 
            this.rb_1_6.BackColor = System.Drawing.SystemColors.Control;
            this.rb_1_6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_1_6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_1_6.Location = new System.Drawing.Point(0, 15);
            this.rb_1_6.Name = "rb_1_6";
            this.rb_1_6.ReadOnly = true;
            this.rb_1_6.Size = new System.Drawing.Size(187, 128);
            this.rb_1_6.TabIndex = 0;
            this.rb_1_6.TabStop = false;
            this.rb_1_6.Text = "";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel13.Controls.Add(this.lbl_3_7);
            this.panel13.Controls.Add(this.lbl_Day_3_7);
            this.panel13.Controls.Add(this.rb_3_7);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(1139, 343);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(192, 143);
            this.panel13.TabIndex = 10;
            // 
            // lbl_3_7
            // 
            this.lbl_3_7.AutoSize = true;
            this.lbl_3_7.Location = new System.Drawing.Point(82, 1);
            this.lbl_3_7.Name = "lbl_3_7";
            this.lbl_3_7.Size = new System.Drawing.Size(38, 12);
            this.lbl_3_7.TabIndex = 1;
            this.lbl_3_7.Text = "label8";
            this.lbl_3_7.Visible = false;
            // 
            // lbl_Day_3_7
            // 
            this.lbl_Day_3_7.AutoSize = true;
            this.lbl_Day_3_7.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_3_7.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_3_7.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_3_7.Name = "lbl_Day_3_7";
            this.lbl_Day_3_7.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_3_7.TabIndex = 0;
            this.lbl_Day_3_7.Text = "0";
            // 
            // rb_3_7
            // 
            this.rb_3_7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rb_3_7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_3_7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_3_7.Location = new System.Drawing.Point(0, 15);
            this.rb_3_7.Name = "rb_3_7";
            this.rb_3_7.ReadOnly = true;
            this.rb_3_7.Size = new System.Drawing.Size(192, 128);
            this.rb_3_7.TabIndex = 0;
            this.rb_3_7.TabStop = false;
            this.rb_3_7.Text = "";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.lbl_3_6);
            this.panel14.Controls.Add(this.lbl_Day_3_6);
            this.panel14.Controls.Add(this.rb_3_6);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(943, 343);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(187, 143);
            this.panel14.TabIndex = 11;
            // 
            // lbl_3_6
            // 
            this.lbl_3_6.AutoSize = true;
            this.lbl_3_6.Location = new System.Drawing.Point(83, 1);
            this.lbl_3_6.Name = "lbl_3_6";
            this.lbl_3_6.Size = new System.Drawing.Size(38, 12);
            this.lbl_3_6.TabIndex = 1;
            this.lbl_3_6.Text = "label8";
            this.lbl_3_6.Visible = false;
            // 
            // lbl_Day_3_6
            // 
            this.lbl_Day_3_6.AutoSize = true;
            this.lbl_Day_3_6.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_3_6.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_3_6.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_3_6.Name = "lbl_Day_3_6";
            this.lbl_Day_3_6.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_3_6.TabIndex = 0;
            this.lbl_Day_3_6.Text = "0";
            // 
            // rb_3_6
            // 
            this.rb_3_6.BackColor = System.Drawing.SystemColors.Control;
            this.rb_3_6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_3_6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_3_6.Location = new System.Drawing.Point(0, 15);
            this.rb_3_6.Name = "rb_3_6";
            this.rb_3_6.ReadOnly = true;
            this.rb_3_6.Size = new System.Drawing.Size(187, 128);
            this.rb_3_6.TabIndex = 0;
            this.rb_3_6.TabStop = false;
            this.rb_3_6.Text = "";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.lbl_2_5);
            this.panel15.Controls.Add(this.lbl_Day_2_5);
            this.panel15.Controls.Add(this.rb_2_5);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(747, 191);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(187, 143);
            this.panel15.TabIndex = 12;
            // 
            // lbl_2_5
            // 
            this.lbl_2_5.AutoSize = true;
            this.lbl_2_5.Location = new System.Drawing.Point(82, 1);
            this.lbl_2_5.Name = "lbl_2_5";
            this.lbl_2_5.Size = new System.Drawing.Size(38, 12);
            this.lbl_2_5.TabIndex = 1;
            this.lbl_2_5.Text = "label8";
            this.lbl_2_5.Visible = false;
            // 
            // lbl_Day_2_5
            // 
            this.lbl_Day_2_5.AutoSize = true;
            this.lbl_Day_2_5.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_2_5.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_2_5.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_2_5.Name = "lbl_Day_2_5";
            this.lbl_Day_2_5.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_2_5.TabIndex = 0;
            this.lbl_Day_2_5.Text = "0";
            // 
            // rb_2_5
            // 
            this.rb_2_5.BackColor = System.Drawing.SystemColors.Control;
            this.rb_2_5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_2_5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_2_5.Location = new System.Drawing.Point(0, 15);
            this.rb_2_5.Name = "rb_2_5";
            this.rb_2_5.ReadOnly = true;
            this.rb_2_5.Size = new System.Drawing.Size(187, 128);
            this.rb_2_5.TabIndex = 0;
            this.rb_2_5.TabStop = false;
            this.rb_2_5.Text = "";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.lbl_3_5);
            this.panel16.Controls.Add(this.lbl_Day_3_5);
            this.panel16.Controls.Add(this.rb_3_5);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(747, 343);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(187, 143);
            this.panel16.TabIndex = 13;
            // 
            // lbl_3_5
            // 
            this.lbl_3_5.AutoSize = true;
            this.lbl_3_5.Location = new System.Drawing.Point(82, 1);
            this.lbl_3_5.Name = "lbl_3_5";
            this.lbl_3_5.Size = new System.Drawing.Size(38, 12);
            this.lbl_3_5.TabIndex = 1;
            this.lbl_3_5.Text = "label8";
            this.lbl_3_5.Visible = false;
            // 
            // lbl_Day_3_5
            // 
            this.lbl_Day_3_5.AutoSize = true;
            this.lbl_Day_3_5.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_3_5.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_3_5.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_3_5.Name = "lbl_Day_3_5";
            this.lbl_Day_3_5.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_3_5.TabIndex = 0;
            this.lbl_Day_3_5.Text = "0";
            // 
            // rb_3_5
            // 
            this.rb_3_5.BackColor = System.Drawing.SystemColors.Control;
            this.rb_3_5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_3_5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_3_5.Location = new System.Drawing.Point(0, 15);
            this.rb_3_5.Name = "rb_3_5";
            this.rb_3_5.ReadOnly = true;
            this.rb_3_5.Size = new System.Drawing.Size(187, 128);
            this.rb_3_5.TabIndex = 0;
            this.rb_3_5.TabStop = false;
            this.rb_3_5.Text = "";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel17.Controls.Add(this.lbl_4_7);
            this.panel17.Controls.Add(this.lbl_Day_4_7);
            this.panel17.Controls.Add(this.rb_4_7);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(1139, 495);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(192, 143);
            this.panel17.TabIndex = 14;
            // 
            // lbl_4_7
            // 
            this.lbl_4_7.AutoSize = true;
            this.lbl_4_7.Location = new System.Drawing.Point(82, 1);
            this.lbl_4_7.Name = "lbl_4_7";
            this.lbl_4_7.Size = new System.Drawing.Size(38, 12);
            this.lbl_4_7.TabIndex = 1;
            this.lbl_4_7.Text = "label8";
            this.lbl_4_7.Visible = false;
            // 
            // lbl_Day_4_7
            // 
            this.lbl_Day_4_7.AutoSize = true;
            this.lbl_Day_4_7.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_4_7.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_4_7.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_4_7.Name = "lbl_Day_4_7";
            this.lbl_Day_4_7.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_4_7.TabIndex = 0;
            this.lbl_Day_4_7.Text = "0";
            // 
            // rb_4_7
            // 
            this.rb_4_7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rb_4_7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_4_7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_4_7.Location = new System.Drawing.Point(0, 15);
            this.rb_4_7.Name = "rb_4_7";
            this.rb_4_7.ReadOnly = true;
            this.rb_4_7.Size = new System.Drawing.Size(192, 128);
            this.rb_4_7.TabIndex = 0;
            this.rb_4_7.TabStop = false;
            this.rb_4_7.Text = "";
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.LightCoral;
            this.panel36.Controls.Add(this.lbl_5_1);
            this.panel36.Controls.Add(this.lbl_Day_5_1);
            this.panel36.Controls.Add(this.rb_5_1);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel36.Location = new System.Drawing.Point(6, 647);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(144, 146);
            this.panel36.TabIndex = 17;
            // 
            // lbl_5_1
            // 
            this.lbl_5_1.AutoSize = true;
            this.lbl_5_1.Location = new System.Drawing.Point(58, 1);
            this.lbl_5_1.Name = "lbl_5_1";
            this.lbl_5_1.Size = new System.Drawing.Size(38, 12);
            this.lbl_5_1.TabIndex = 1;
            this.lbl_5_1.Text = "label8";
            this.lbl_5_1.Visible = false;
            // 
            // lbl_Day_5_1
            // 
            this.lbl_Day_5_1.AutoSize = true;
            this.lbl_Day_5_1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_5_1.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_5_1.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_5_1.Name = "lbl_Day_5_1";
            this.lbl_Day_5_1.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_5_1.TabIndex = 0;
            this.lbl_Day_5_1.Text = "0";
            // 
            // rb_5_1
            // 
            this.rb_5_1.BackColor = System.Drawing.Color.LightCoral;
            this.rb_5_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_5_1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_5_1.Location = new System.Drawing.Point(0, 18);
            this.rb_5_1.Name = "rb_5_1";
            this.rb_5_1.ReadOnly = true;
            this.rb_5_1.Size = new System.Drawing.Size(144, 128);
            this.rb_5_1.TabIndex = 0;
            this.rb_5_1.TabStop = false;
            this.rb_5_1.Text = "";
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.lbl_5_2);
            this.panel37.Controls.Add(this.lbl_Day_5_2);
            this.panel37.Controls.Add(this.rb_5_2);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel37.Location = new System.Drawing.Point(159, 647);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(187, 146);
            this.panel37.TabIndex = 17;
            // 
            // lbl_5_2
            // 
            this.lbl_5_2.AutoSize = true;
            this.lbl_5_2.Location = new System.Drawing.Point(80, 1);
            this.lbl_5_2.Name = "lbl_5_2";
            this.lbl_5_2.Size = new System.Drawing.Size(38, 12);
            this.lbl_5_2.TabIndex = 1;
            this.lbl_5_2.Text = "label8";
            this.lbl_5_2.Visible = false;
            // 
            // lbl_Day_5_2
            // 
            this.lbl_Day_5_2.AutoSize = true;
            this.lbl_Day_5_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_5_2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_5_2.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_5_2.Name = "lbl_Day_5_2";
            this.lbl_Day_5_2.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_5_2.TabIndex = 0;
            this.lbl_Day_5_2.Text = "0";
            // 
            // rb_5_2
            // 
            this.rb_5_2.BackColor = System.Drawing.SystemColors.Control;
            this.rb_5_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_5_2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_5_2.Location = new System.Drawing.Point(0, 18);
            this.rb_5_2.Name = "rb_5_2";
            this.rb_5_2.ReadOnly = true;
            this.rb_5_2.Size = new System.Drawing.Size(187, 128);
            this.rb_5_2.TabIndex = 0;
            this.rb_5_2.TabStop = false;
            this.rb_5_2.Text = "";
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.lbl_5_3);
            this.panel38.Controls.Add(this.lbl_Day_5_3);
            this.panel38.Controls.Add(this.rb_5_3);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel38.Location = new System.Drawing.Point(355, 647);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(187, 146);
            this.panel38.TabIndex = 17;
            // 
            // lbl_5_3
            // 
            this.lbl_5_3.AutoSize = true;
            this.lbl_5_3.Location = new System.Drawing.Point(81, 1);
            this.lbl_5_3.Name = "lbl_5_3";
            this.lbl_5_3.Size = new System.Drawing.Size(38, 12);
            this.lbl_5_3.TabIndex = 1;
            this.lbl_5_3.Text = "label8";
            this.lbl_5_3.Visible = false;
            // 
            // lbl_Day_5_3
            // 
            this.lbl_Day_5_3.AutoSize = true;
            this.lbl_Day_5_3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_5_3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_5_3.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_5_3.Name = "lbl_Day_5_3";
            this.lbl_Day_5_3.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_5_3.TabIndex = 0;
            this.lbl_Day_5_3.Text = "0";
            // 
            // rb_5_3
            // 
            this.rb_5_3.BackColor = System.Drawing.SystemColors.Control;
            this.rb_5_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_5_3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_5_3.Location = new System.Drawing.Point(0, 18);
            this.rb_5_3.Name = "rb_5_3";
            this.rb_5_3.ReadOnly = true;
            this.rb_5_3.Size = new System.Drawing.Size(187, 128);
            this.rb_5_3.TabIndex = 0;
            this.rb_5_3.TabStop = false;
            this.rb_5_3.Text = "";
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.lbl_5_4);
            this.panel39.Controls.Add(this.lbl_Day_5_4);
            this.panel39.Controls.Add(this.rb_5_4);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel39.Location = new System.Drawing.Point(551, 647);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(187, 146);
            this.panel39.TabIndex = 17;
            // 
            // lbl_5_4
            // 
            this.lbl_5_4.AutoSize = true;
            this.lbl_5_4.Location = new System.Drawing.Point(82, 1);
            this.lbl_5_4.Name = "lbl_5_4";
            this.lbl_5_4.Size = new System.Drawing.Size(38, 12);
            this.lbl_5_4.TabIndex = 1;
            this.lbl_5_4.Text = "label8";
            this.lbl_5_4.Visible = false;
            // 
            // lbl_Day_5_4
            // 
            this.lbl_Day_5_4.AutoSize = true;
            this.lbl_Day_5_4.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_5_4.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_5_4.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_5_4.Name = "lbl_Day_5_4";
            this.lbl_Day_5_4.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_5_4.TabIndex = 0;
            this.lbl_Day_5_4.Text = "0";
            // 
            // rb_5_4
            // 
            this.rb_5_4.BackColor = System.Drawing.SystemColors.Control;
            this.rb_5_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_5_4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_5_4.Location = new System.Drawing.Point(0, 18);
            this.rb_5_4.Name = "rb_5_4";
            this.rb_5_4.ReadOnly = true;
            this.rb_5_4.Size = new System.Drawing.Size(187, 128);
            this.rb_5_4.TabIndex = 0;
            this.rb_5_4.TabStop = false;
            this.rb_5_4.Text = "";
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.lbl_5_5);
            this.panel40.Controls.Add(this.lbl_Day_5_5);
            this.panel40.Controls.Add(this.rb_5_5);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel40.Location = new System.Drawing.Point(747, 647);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(187, 146);
            this.panel40.TabIndex = 17;
            // 
            // lbl_5_5
            // 
            this.lbl_5_5.AutoSize = true;
            this.lbl_5_5.Location = new System.Drawing.Point(82, 1);
            this.lbl_5_5.Name = "lbl_5_5";
            this.lbl_5_5.Size = new System.Drawing.Size(38, 12);
            this.lbl_5_5.TabIndex = 1;
            this.lbl_5_5.Text = "label8";
            this.lbl_5_5.Visible = false;
            // 
            // lbl_Day_5_5
            // 
            this.lbl_Day_5_5.AutoSize = true;
            this.lbl_Day_5_5.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_5_5.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_5_5.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_5_5.Name = "lbl_Day_5_5";
            this.lbl_Day_5_5.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_5_5.TabIndex = 0;
            this.lbl_Day_5_5.Text = "0";
            // 
            // rb_5_5
            // 
            this.rb_5_5.BackColor = System.Drawing.SystemColors.Control;
            this.rb_5_5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_5_5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_5_5.Location = new System.Drawing.Point(0, 18);
            this.rb_5_5.Name = "rb_5_5";
            this.rb_5_5.ReadOnly = true;
            this.rb_5_5.Size = new System.Drawing.Size(187, 128);
            this.rb_5_5.TabIndex = 0;
            this.rb_5_5.TabStop = false;
            this.rb_5_5.Text = "";
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.lbl_5_6);
            this.panel41.Controls.Add(this.lbl_Day_5_6);
            this.panel41.Controls.Add(this.rb_5_6);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel41.Location = new System.Drawing.Point(943, 647);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(187, 146);
            this.panel41.TabIndex = 17;
            // 
            // lbl_5_6
            // 
            this.lbl_5_6.AutoSize = true;
            this.lbl_5_6.Location = new System.Drawing.Point(83, 1);
            this.lbl_5_6.Name = "lbl_5_6";
            this.lbl_5_6.Size = new System.Drawing.Size(38, 12);
            this.lbl_5_6.TabIndex = 1;
            this.lbl_5_6.Text = "label8";
            this.lbl_5_6.Visible = false;
            // 
            // lbl_Day_5_6
            // 
            this.lbl_Day_5_6.AutoSize = true;
            this.lbl_Day_5_6.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_5_6.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_5_6.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_5_6.Name = "lbl_Day_5_6";
            this.lbl_Day_5_6.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_5_6.TabIndex = 0;
            this.lbl_Day_5_6.Text = "0";
            // 
            // rb_5_6
            // 
            this.rb_5_6.BackColor = System.Drawing.SystemColors.Control;
            this.rb_5_6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_5_6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_5_6.Location = new System.Drawing.Point(0, 18);
            this.rb_5_6.Name = "rb_5_6";
            this.rb_5_6.ReadOnly = true;
            this.rb_5_6.Size = new System.Drawing.Size(187, 128);
            this.rb_5_6.TabIndex = 0;
            this.rb_5_6.TabStop = false;
            this.rb_5_6.Text = "";
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel42.Controls.Add(this.lbl_5_7);
            this.panel42.Controls.Add(this.lbl_Day_5_7);
            this.panel42.Controls.Add(this.rb_5_7);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel42.Location = new System.Drawing.Point(1139, 647);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(192, 146);
            this.panel42.TabIndex = 17;
            // 
            // lbl_5_7
            // 
            this.lbl_5_7.AutoSize = true;
            this.lbl_5_7.Location = new System.Drawing.Point(82, 1);
            this.lbl_5_7.Name = "lbl_5_7";
            this.lbl_5_7.Size = new System.Drawing.Size(38, 12);
            this.lbl_5_7.TabIndex = 1;
            this.lbl_5_7.Text = "label8";
            this.lbl_5_7.Visible = false;
            // 
            // lbl_Day_5_7
            // 
            this.lbl_Day_5_7.AutoSize = true;
            this.lbl_Day_5_7.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_5_7.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_5_7.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_5_7.Name = "lbl_Day_5_7";
            this.lbl_Day_5_7.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_5_7.TabIndex = 0;
            this.lbl_Day_5_7.Text = "0";
            // 
            // rb_5_7
            // 
            this.rb_5_7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rb_5_7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_5_7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_5_7.Location = new System.Drawing.Point(0, 18);
            this.rb_5_7.Name = "rb_5_7";
            this.rb_5_7.ReadOnly = true;
            this.rb_5_7.Size = new System.Drawing.Size(192, 128);
            this.rb_5_7.TabIndex = 0;
            this.rb_5_7.TabStop = false;
            this.rb_5_7.Text = "";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel11.Controls.Add(this.lbl_2_7);
            this.panel11.Controls.Add(this.lbl_Day_2_7);
            this.panel11.Controls.Add(this.rb_2_7);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(1139, 191);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(192, 143);
            this.panel11.TabIndex = 8;
            // 
            // lbl_2_7
            // 
            this.lbl_2_7.AutoSize = true;
            this.lbl_2_7.Location = new System.Drawing.Point(82, 1);
            this.lbl_2_7.Name = "lbl_2_7";
            this.lbl_2_7.Size = new System.Drawing.Size(38, 12);
            this.lbl_2_7.TabIndex = 1;
            this.lbl_2_7.Text = "label8";
            this.lbl_2_7.Visible = false;
            // 
            // lbl_Day_2_7
            // 
            this.lbl_Day_2_7.AutoSize = true;
            this.lbl_Day_2_7.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Day_2_7.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Day_2_7.Location = new System.Drawing.Point(0, 0);
            this.lbl_Day_2_7.Name = "lbl_Day_2_7";
            this.lbl_Day_2_7.Size = new System.Drawing.Size(16, 15);
            this.lbl_Day_2_7.TabIndex = 0;
            this.lbl_Day_2_7.Text = "0";
            // 
            // rb_2_7
            // 
            this.rb_2_7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rb_2_7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rb_2_7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rb_2_7.Location = new System.Drawing.Point(0, 15);
            this.rb_2_7.Name = "rb_2_7";
            this.rb_2_7.ReadOnly = true;
            this.rb_2_7.Size = new System.Drawing.Size(192, 128);
            this.rb_2_7.TabIndex = 0;
            this.rb_2_7.TabStop = false;
            this.rb_2_7.Text = "";
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(1236, 16);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 9;
            this.btn_close.Text = "닫    기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // lbl_Month
            // 
            this.lbl_Month.AutoSize = true;
            this.lbl_Month.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Month.Location = new System.Drawing.Point(93, 16);
            this.lbl_Month.Name = "lbl_Month";
            this.lbl_Month.Size = new System.Drawing.Size(94, 27);
            this.lbl_Month.TabIndex = 2;
            this.lbl_Month.Text = "Month";
            // 
            // rb_all
            // 
            this.rb_all.AutoSize = true;
            this.rb_all.Location = new System.Drawing.Point(784, 19);
            this.rb_all.Name = "rb_all";
            this.rb_all.Size = new System.Drawing.Size(47, 16);
            this.rb_all.TabIndex = 10;
            this.rb_all.TabStop = true;
            this.rb_all.Text = "전체";
            this.rb_all.UseVisualStyleBackColor = true;
            this.rb_all.Visible = false;
            this.rb_all.CheckedChanged += new System.EventHandler(this.rb_all_CheckedChanged);
            // 
            // rb_dly
            // 
            this.rb_dly.AutoSize = true;
            this.rb_dly.Location = new System.Drawing.Point(850, 19);
            this.rb_dly.Name = "rb_dly";
            this.rb_dly.Size = new System.Drawing.Size(59, 16);
            this.rb_dly.TabIndex = 10;
            this.rb_dly.TabStop = true;
            this.rb_dly.Text = "납품만";
            this.rb_dly.UseVisualStyleBackColor = true;
            this.rb_dly.Visible = false;
            this.rb_dly.CheckedChanged += new System.EventHandler(this.rb_all_CheckedChanged);
            // 
            // rb_marc
            // 
            this.rb_marc.AutoSize = true;
            this.rb_marc.Location = new System.Drawing.Point(928, 19);
            this.rb_marc.Name = "rb_marc";
            this.rb_marc.Size = new System.Drawing.Size(59, 16);
            this.rb_marc.TabIndex = 10;
            this.rb_marc.TabStop = true;
            this.rb_marc.Text = "마크만";
            this.rb_marc.UseVisualStyleBackColor = true;
            this.rb_marc.Visible = false;
            this.rb_marc.CheckedChanged += new System.EventHandler(this.rb_all_CheckedChanged);
            // 
            // Calendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1360, 863);
            this.Controls.Add(this.rb_marc);
            this.Controls.Add(this.rb_dly);
            this.Controls.Add(this.rb_all);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.Btn_After);
            this.Controls.Add(this.Btn_Before);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lbl_Month);
            this.Controls.Add(this.lbl_Year);
            this.Name = "Calendar";
            this.Text = "캘린더";
            this.Load += new System.EventHandler(this.Calendar_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_Year;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button Btn_After;
        private System.Windows.Forms.Button Btn_Before;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label lbl_Day_1_4;
        private System.Windows.Forms.RichTextBox rb_1_4;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label lbl_Day_1_2;
        private System.Windows.Forms.RichTextBox rb_1_2;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label lbl_Day_4_6;
        private System.Windows.Forms.RichTextBox rb_4_6;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label lbl_Day_4_5;
        private System.Windows.Forms.RichTextBox rb_4_5;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label lbl_Day_4_4;
        private System.Windows.Forms.RichTextBox rb_4_4;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label lbl_Day_4_3;
        private System.Windows.Forms.RichTextBox rb_4_3;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label lbl_Day_4_2;
        private System.Windows.Forms.RichTextBox rb_4_2;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label lbl_Day_4_1;
        private System.Windows.Forms.RichTextBox rb_4_1;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label lbl_Day_3_1;
        private System.Windows.Forms.RichTextBox rb_3_1;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label lbl_Day_3_2;
        private System.Windows.Forms.RichTextBox rb_3_2;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label lbl_Day_3_3;
        private System.Windows.Forms.RichTextBox rb_3_3;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label lbl_Day_3_4;
        private System.Windows.Forms.RichTextBox rb_3_4;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label lbl_Day_2_4;
        private System.Windows.Forms.RichTextBox rb_2_4;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label lbl_Day_2_3;
        private System.Windows.Forms.RichTextBox rb_2_3;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label lbl_Day_2_2;
        private System.Windows.Forms.RichTextBox rb_2_2;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label lbl_Day_2_1;
        private System.Windows.Forms.RichTextBox rb_2_1;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label lbl_Day_1_3;
        private System.Windows.Forms.RichTextBox rb_1_3;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label lbl_Day_1_1;
        private System.Windows.Forms.RichTextBox rb_1_1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_Day_1_5;
        private System.Windows.Forms.RichTextBox rb_1_5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lbl_Day_1_7;
        private System.Windows.Forms.RichTextBox rb_1_7;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lbl_Day_1_6;
        private System.Windows.Forms.RichTextBox rb_1_6;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lbl_Day_2_7;
        private System.Windows.Forms.RichTextBox rb_2_7;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lbl_Day_2_6;
        private System.Windows.Forms.RichTextBox rb_2_6;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label lbl_Day_3_7;
        private System.Windows.Forms.RichTextBox rb_3_7;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label lbl_Day_3_6;
        private System.Windows.Forms.RichTextBox rb_3_6;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label lbl_Day_2_5;
        private System.Windows.Forms.RichTextBox rb_2_5;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label lbl_Day_3_5;
        private System.Windows.Forms.RichTextBox rb_3_5;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label lbl_Day_4_7;
        private System.Windows.Forms.RichTextBox rb_4_7;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label lbl_Day_5_1;
        private System.Windows.Forms.RichTextBox rb_5_1;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Label lbl_Day_5_2;
        private System.Windows.Forms.RichTextBox rb_5_2;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label lbl_Day_5_3;
        private System.Windows.Forms.RichTextBox rb_5_3;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Label lbl_Day_5_4;
        private System.Windows.Forms.RichTextBox rb_5_4;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label lbl_Day_5_5;
        private System.Windows.Forms.RichTextBox rb_5_5;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Label lbl_Day_5_6;
        private System.Windows.Forms.RichTextBox rb_5_6;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label lbl_Day_5_7;
        private System.Windows.Forms.RichTextBox rb_5_7;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label lbl_Month;
        private System.Windows.Forms.Label lbl_1_4;
        private System.Windows.Forms.Label lbl_1_2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbl_1_3;
        private System.Windows.Forms.Label lbl_1_1;
        private System.Windows.Forms.Label lbl_1_5;
        private System.Windows.Forms.Label lbl_1_7;
        private System.Windows.Forms.Label lbl_1_6;
        private System.Windows.Forms.Label lbl_4_6;
        private System.Windows.Forms.Label lbl_4_5;
        private System.Windows.Forms.Label lbl_4_4;
        private System.Windows.Forms.Label lbl_4_3;
        private System.Windows.Forms.Label lbl_4_2;
        private System.Windows.Forms.Label lbl_2_6;
        private System.Windows.Forms.Label lbl_4_1;
        private System.Windows.Forms.Label lbl_3_1;
        private System.Windows.Forms.Label lbl_3_2;
        private System.Windows.Forms.Label lbl_3_3;
        private System.Windows.Forms.Label lbl_3_4;
        private System.Windows.Forms.Label lbl_2_4;
        private System.Windows.Forms.Label lbl_2_3;
        private System.Windows.Forms.Label lbl_2_2;
        private System.Windows.Forms.Label lbl_2_1;
        private System.Windows.Forms.Label lbl_3_7;
        private System.Windows.Forms.Label lbl_3_6;
        private System.Windows.Forms.Label lbl_2_5;
        private System.Windows.Forms.Label lbl_3_5;
        private System.Windows.Forms.Label lbl_4_7;
        private System.Windows.Forms.Label lbl_5_1;
        private System.Windows.Forms.Label lbl_5_2;
        private System.Windows.Forms.Label lbl_5_3;
        private System.Windows.Forms.Label lbl_5_4;
        private System.Windows.Forms.Label lbl_5_5;
        private System.Windows.Forms.Label lbl_5_6;
        private System.Windows.Forms.Label lbl_5_7;
        private System.Windows.Forms.Label lbl_2_7;
        private System.Windows.Forms.RadioButton rb_all;
        private System.Windows.Forms.RadioButton rb_dly;
        private System.Windows.Forms.RadioButton rb_marc;
    }
}