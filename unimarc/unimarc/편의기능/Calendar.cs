﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Convenience
{
    public partial class Calendar : Form
    {
        string compidx = "";
        Helper_DB db = new Helper_DB();
        Main main;

        public Calendar(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }

        private void Calendar_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now;

            db.DBcon();
            rb_all.Checked = true;

            Grep_Data();

            Day_Setting();
        }
        /// <summary>
        /// 오늘을 기준으로 달력을 출력하기 때문에 오늘의 요일을 구하여 해당 요일에 오늘 날짜를 입력함
        /// </summary>
        private void Day_Setting()
        {
            Reset_richBox();

            string day_now = dateTimePicker1.Value.ToString("dd");
            string week_now = dateTimePicker1.Value.DayOfWeek.ToString();
            switch (week_now)
            {
                case "Sunday":
                    lbl_Day_2_1.Text = day_now;
                    Cal_Setting(day_now, 7);
                    break;
                case "Monday":
                    lbl_Day_2_2.Text = day_now;
                    Cal_Setting(day_now, 8);
                    break;
                case "Tuesday":
                    lbl_Day_2_3.Text = day_now;
                    Cal_Setting(day_now, 9);
                    break;
                case "Wednesday":
                    lbl_Day_2_4.Text = day_now;
                    Cal_Setting(day_now, 10);
                    break;
                case "Thursday":
                    lbl_Day_2_5.Text = day_now;
                    Cal_Setting(day_now, 11);
                    break;
                case "Friday":
                    lbl_Day_2_6.Text = day_now;
                    Cal_Setting(day_now, 12);
                    break;
                case "Saturday":
                    lbl_Day_2_7.Text = day_now;
                    Cal_Setting(day_now, 13);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 달력 날짜 세팅
        /// </summary>
        /// <param name="day_now"></param>
        /// <param name="idx"></param>
        private void Cal_Setting(string day_now, int idx)
        {
            int day = Convert.ToInt32(day_now);
            int year = dateTimePicker1.Value.Year;
            int months = dateTimePicker1.Value.Month;
            lbl_Year.Text = year.ToString();
            lbl_Month.Text = months.ToString();
            if (lbl_Month.Text.Length < 2)
                lbl_Month.Text = "0" + lbl_Month.Text;

            Label[] month = {
                lbl_Day_1_1, lbl_Day_1_2, lbl_Day_1_3, lbl_Day_1_4, lbl_Day_1_5, lbl_Day_1_6, lbl_Day_1_7,
                lbl_Day_2_1, lbl_Day_2_2, lbl_Day_2_3, lbl_Day_2_4, lbl_Day_2_5, lbl_Day_2_6, lbl_Day_2_7,
                lbl_Day_3_1, lbl_Day_3_2, lbl_Day_3_3, lbl_Day_3_4, lbl_Day_3_5, lbl_Day_3_6, lbl_Day_3_7,
                lbl_Day_4_1, lbl_Day_4_2, lbl_Day_4_3, lbl_Day_4_4, lbl_Day_4_5, lbl_Day_4_6, lbl_Day_4_7,
                lbl_Day_5_1, lbl_Day_5_2, lbl_Day_5_3, lbl_Day_5_4, lbl_Day_5_5, lbl_Day_5_6, lbl_Day_5_7
            };
            Label[] lbl_tmp = {
                lbl_1_1, lbl_1_2, lbl_1_3, lbl_1_4, lbl_1_5, lbl_1_6, lbl_1_7,
                lbl_2_1, lbl_2_2, lbl_2_3, lbl_2_4, lbl_2_5, lbl_2_6, lbl_2_7,
                lbl_3_1, lbl_3_2, lbl_3_3, lbl_3_4, lbl_3_5, lbl_3_6, lbl_3_7,
                lbl_4_1, lbl_4_2, lbl_4_3, lbl_4_4, lbl_4_5, lbl_4_6, lbl_4_7,
                lbl_5_1, lbl_5_2, lbl_5_3, lbl_5_4, lbl_5_5, lbl_5_6, lbl_5_7
            };

            // 오늘을 포함한 이후의 달력을 만듬
            for (int a = idx; a < month.Length; a++)
            {
                if (day - 1 == DateTime.DaysInMonth(year, months)) { day = 1; }
                month[a].Text = day.ToString();
                if (month[a].Text.Length < 2) 
                    month[a].Text = "0" + month[a].Text;
                day++;
                lbl_tmp[a].Text = string.Format("{0}-{1}-{2}",year, months, month[a].Text);
            }

            // 오늘 이전의 달력을 만듬
            bool ago = false;
            day = Convert.ToInt32(day_now);
            for (int a = idx; a > -1; a--)
            {
                if (day < 1)
                {
                    months--;
                    if (months < 1) { year--; months = 12; }
                    day = DateTime.DaysInMonth(year, months);
                    ago = true;
                }
                if (ago) {
                    month[a].ForeColor = Color.Gray;
                }
                else { 
                    month[a].ForeColor = Color.Black; 
                }
                month[a].Text = day.ToString();
                if (month[a].Text.Length < 2) 
                    month[a].Text = "0" + month[a].Text;
                day--;
                lbl_tmp[a].Text = string.Format("{0}-{1}-{2}",year, months, month[a].Text);
            }

            // 토요일 일요일 색을 입힘
            for (int a = 0; a < month.Length; a++)
            {
                if (month[a].Name[10] == '1')
                    month[a].ForeColor = Color.Red;

                if (month[a].Name[10] == '7')
                    month[a].ForeColor = Color.Blue;
                input_List(a);
            }

        }
        private void Reset_richBox()
        {
            RichTextBox[] rbBox = {
                rb_1_1, rb_1_2, rb_1_3, rb_1_4, rb_1_5, rb_1_6, rb_1_7,
                rb_2_1, rb_2_2, rb_2_3, rb_2_4, rb_2_5, rb_2_6, rb_2_7,
                rb_3_1, rb_3_2, rb_3_3, rb_3_4, rb_3_5, rb_3_6, rb_3_7,
                rb_4_1, rb_4_2, rb_4_3, rb_4_4, rb_4_5, rb_4_6, rb_4_7,
                rb_5_1, rb_5_2, rb_5_3, rb_5_4, rb_5_5, rb_5_6, rb_5_7
            };
            for(int a = 0; a < rbBox.Length; a++)
            {
                rbBox[a].Text = "";
            }
        }
        List<string> date_res = new List<string>();
        List<string> list_name = new List<string>();
        List<string> chk_marc = new List<string>();
        private void Grep_Data()
        {
            string Area = "`date_res`, `list_name`, `chk_marc`";
            string cmd = db.DB_Select_Search(Area, "Obj_List", "comp_num", compidx);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] ary_tmp = db_res.Split('|');
            bool chk = false;

            for(int a = 0; a < ary_tmp.Length; a++)
            {
                if (a % 3 == 0) {
                    if (ary_tmp[a].Length > 4) {
                        chk = true;
                        date_res.Add(ary_tmp[a]);
                    }
                    else { chk = false; }
                }
                if (a % 3 == 1)
                {
                    if (chk) { list_name.Add(ary_tmp[a]); chk_marc.Add(ary_tmp[a + 1]); }
                    else { chk = true; }
                }
            }
        }
        private void input_List(int idx)
        {
            Label[] lbl_tmp = {
                lbl_1_1, lbl_1_2, lbl_1_3, lbl_1_4, lbl_1_5, lbl_1_6, lbl_1_7,
                lbl_2_1, lbl_2_2, lbl_2_3, lbl_2_4, lbl_2_5, lbl_2_6, lbl_2_7,
                lbl_3_1, lbl_3_2, lbl_3_3, lbl_3_4, lbl_3_5, lbl_3_6, lbl_3_7,
                lbl_4_1, lbl_4_2, lbl_4_3, lbl_4_4, lbl_4_5, lbl_4_6, lbl_4_7,
                lbl_5_1, lbl_5_2, lbl_5_3, lbl_5_4, lbl_5_5, lbl_5_6, lbl_5_7
            };
            RichTextBox[] rbBox = {
                rb_1_1, rb_1_2, rb_1_3, rb_1_4, rb_1_5, rb_1_6, rb_1_7,
                rb_2_1, rb_2_2, rb_2_3, rb_2_4, rb_2_5, rb_2_6, rb_2_7,
                rb_3_1, rb_3_2, rb_3_3, rb_3_4, rb_3_5, rb_3_6, rb_3_7,
                rb_4_1, rb_4_2, rb_4_3, rb_4_4, rb_4_5, rb_4_6, rb_4_7,
                rb_5_1, rb_5_2, rb_5_3, rb_5_4, rb_5_5, rb_5_6, rb_5_7
            };

            bool print = false;
            string mk_date = lbl_tmp[idx].Text;
            DateTime mkd = DateTime.Parse(mk_date);
            for(int a = 0; a < date_res.Count; a++)
            {
                DateTime res = DateTime.Parse(date_res[a]);
                if (mkd == res) {
                    if (rb_all.Checked) print = true;
                    else if (rb_dly.Checked) {
                        if (chk_marc[a] == "0" || chk_marc[a] == "2") {
                            print = true;
                        }
                    }
                    else if (rb_marc.Checked) {
                        if (chk_marc[a] == "1" || chk_marc[a] == "2") {
                            print = true;
                        }
                    }
                    if (print)
                        rbBox[idx].Text += list_name[a] + "\n";
                }
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Day_Setting();
        }

        private void Btn_Before_Click(object sender, EventArgs e)
        {
            int year, month, day;

            DateTime picker = dateTimePicker1.Value;
            year = picker.Year;
            month = picker.Month - 1;
            if (month < 1)
            {
                year--;
                month = 12;
            }
            day = picker.Day;

            string date = string.Format("{0}-{1}-{2}", year, month, day);
            dateTimePicker1.Value = DateTime.Parse(date);
        }

        private void Btn_After_Click(object sender, EventArgs e)
        {
            int year, month, day;

            DateTime picker = dateTimePicker1.Value;
            year = picker.Year;
            month = picker.Month + 1; 
            if (month > 12)
            {
                year++;
                month = 1;
            }
            day = picker.Day;

            string date = string.Format("{0}-{1}-{2}", year, month, day);
            dateTimePicker1.Value = DateTime.Parse(date);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rb_all_CheckedChanged(object sender, EventArgs e)
        {
            Day_Setting();
        }
    }
}
