﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UniMarc.Properties;

namespace WindowsFormsApp1.Convenience
{
    public partial class Send_Notice : Form
    {
        Main main;
        public Send_Notice(Main _main)
        {
            InitializeComponent();
            main = _main;
        }

        private void Send_Notice_Load(object sender, EventArgs e)
        {
            label1.Text = Settings.Default.User + " | " + Settings.Default.compidx;
        }
    }
}
