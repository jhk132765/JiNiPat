﻿namespace WindowsFormsApp1.Convenience
{
    partial class Quick_menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.cb_menu1 = new System.Windows.Forms.ComboBox();
            this.cb_setShort1 = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cb_setShort2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_menu2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cb_setShort3 = new System.Windows.Forms.ComboBox();
            this.cb_menu3 = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cb_setShort4 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_menu4 = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cb_setShort5 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cb_menu5 = new System.Windows.Forms.ComboBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cb_setShort6 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_menu6 = new System.Windows.Forms.ComboBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cb_setShort7 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cb_menu7 = new System.Windows.Forms.ComboBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.cb_setShort8 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cb_menu8 = new System.Windows.Forms.ComboBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.cb_setShort9 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cb_menu9 = new System.Windows.Forms.ComboBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.cb_setShort10 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cb_menu10 = new System.Windows.Forms.ComboBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.cb_setShort11 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cb_menu11 = new System.Windows.Forms.ComboBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.cb_setShort12 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cb_menu12 = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(23, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "퀵 메뉴 1";
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(612, 12);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(79, 27);
            this.btn_save.TabIndex = 0;
            this.btn_save.Text = "저    장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(701, 12);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(79, 27);
            this.btn_close.TabIndex = 13;
            this.btn_close.Text = "닫    기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // cb_menu1
            // 
            this.cb_menu1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu1.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu1.FormattingEnabled = true;
            this.cb_menu1.ItemHeight = 15;
            this.cb_menu1.Location = new System.Drawing.Point(136, 11);
            this.cb_menu1.Name = "cb_menu1";
            this.cb_menu1.Size = new System.Drawing.Size(121, 23);
            this.cb_menu1.TabIndex = 3;
            this.cb_menu1.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // cb_setShort1
            // 
            this.cb_setShort1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort1.Enabled = false;
            this.cb_setShort1.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort1.FormattingEnabled = true;
            this.cb_setShort1.ItemHeight = 15;
            this.cb_setShort1.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort1.Name = "cb_setShort1";
            this.cb_setShort1.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cb_setShort1);
            this.panel1.Controls.Add(this.cb_menu1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(15, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(765, 52);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cb_setShort2);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.cb_menu2);
            this.panel2.Location = new System.Drawing.Point(15, 102);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(765, 52);
            this.panel2.TabIndex = 2;
            // 
            // cb_setShort2
            // 
            this.cb_setShort2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort2.Enabled = false;
            this.cb_setShort2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort2.FormattingEnabled = true;
            this.cb_setShort2.ItemHeight = 15;
            this.cb_setShort2.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort2.Name = "cb_setShort2";
            this.cb_setShort2.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(23, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "퀵 메뉴 2";
            // 
            // cb_menu2
            // 
            this.cb_menu2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu2.FormattingEnabled = true;
            this.cb_menu2.ItemHeight = 15;
            this.cb_menu2.Location = new System.Drawing.Point(136, 11);
            this.cb_menu2.Name = "cb_menu2";
            this.cb_menu2.Size = new System.Drawing.Size(121, 23);
            this.cb_menu2.TabIndex = 3;
            this.cb_menu2.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(23, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "퀵 메뉴 3";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cb_setShort3);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.cb_menu3);
            this.panel3.Location = new System.Drawing.Point(15, 153);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(765, 52);
            this.panel3.TabIndex = 3;
            // 
            // cb_setShort3
            // 
            this.cb_setShort3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort3.Enabled = false;
            this.cb_setShort3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort3.FormattingEnabled = true;
            this.cb_setShort3.ItemHeight = 15;
            this.cb_setShort3.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort3.Name = "cb_setShort3";
            this.cb_setShort3.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort3.TabIndex = 3;
            // 
            // cb_menu3
            // 
            this.cb_menu3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu3.FormattingEnabled = true;
            this.cb_menu3.ItemHeight = 15;
            this.cb_menu3.Location = new System.Drawing.Point(136, 11);
            this.cb_menu3.Name = "cb_menu3";
            this.cb_menu3.Size = new System.Drawing.Size(121, 23);
            this.cb_menu3.TabIndex = 3;
            this.cb_menu3.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.cb_setShort4);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.cb_menu4);
            this.panel4.Location = new System.Drawing.Point(15, 204);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(765, 52);
            this.panel4.TabIndex = 4;
            // 
            // cb_setShort4
            // 
            this.cb_setShort4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort4.Enabled = false;
            this.cb_setShort4.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort4.FormattingEnabled = true;
            this.cb_setShort4.ItemHeight = 15;
            this.cb_setShort4.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort4.Name = "cb_setShort4";
            this.cb_setShort4.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort4.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(23, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "퀵 메뉴 4";
            // 
            // cb_menu4
            // 
            this.cb_menu4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu4.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu4.FormattingEnabled = true;
            this.cb_menu4.ItemHeight = 15;
            this.cb_menu4.Location = new System.Drawing.Point(136, 11);
            this.cb_menu4.Name = "cb_menu4";
            this.cb_menu4.Size = new System.Drawing.Size(121, 23);
            this.cb_menu4.TabIndex = 3;
            this.cb_menu4.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.cb_setShort5);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.cb_menu5);
            this.panel5.Location = new System.Drawing.Point(15, 255);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(765, 52);
            this.panel5.TabIndex = 5;
            // 
            // cb_setShort5
            // 
            this.cb_setShort5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort5.Enabled = false;
            this.cb_setShort5.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort5.FormattingEnabled = true;
            this.cb_setShort5.ItemHeight = 15;
            this.cb_setShort5.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort5.Name = "cb_setShort5";
            this.cb_setShort5.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort5.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(23, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "퀵 메뉴 5";
            // 
            // cb_menu5
            // 
            this.cb_menu5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu5.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu5.FormattingEnabled = true;
            this.cb_menu5.ItemHeight = 15;
            this.cb_menu5.Location = new System.Drawing.Point(136, 11);
            this.cb_menu5.Name = "cb_menu5";
            this.cb_menu5.Size = new System.Drawing.Size(121, 23);
            this.cb_menu5.TabIndex = 3;
            this.cb_menu5.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.cb_setShort6);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.cb_menu6);
            this.panel6.Location = new System.Drawing.Point(15, 306);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(765, 52);
            this.panel6.TabIndex = 6;
            // 
            // cb_setShort6
            // 
            this.cb_setShort6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort6.Enabled = false;
            this.cb_setShort6.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort6.FormattingEnabled = true;
            this.cb_setShort6.ItemHeight = 15;
            this.cb_setShort6.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort6.Name = "cb_setShort6";
            this.cb_setShort6.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort6.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(23, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "퀵 메뉴 6";
            // 
            // cb_menu6
            // 
            this.cb_menu6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu6.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu6.FormattingEnabled = true;
            this.cb_menu6.ItemHeight = 15;
            this.cb_menu6.Location = new System.Drawing.Point(136, 11);
            this.cb_menu6.Name = "cb_menu6";
            this.cb_menu6.Size = new System.Drawing.Size(121, 23);
            this.cb_menu6.TabIndex = 3;
            this.cb_menu6.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.cb_setShort7);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.cb_menu7);
            this.panel7.Location = new System.Drawing.Point(15, 357);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(765, 52);
            this.panel7.TabIndex = 7;
            // 
            // cb_setShort7
            // 
            this.cb_setShort7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort7.Enabled = false;
            this.cb_setShort7.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort7.FormattingEnabled = true;
            this.cb_setShort7.ItemHeight = 15;
            this.cb_setShort7.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort7.Name = "cb_setShort7";
            this.cb_setShort7.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort7.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(23, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "퀵 메뉴 7";
            // 
            // cb_menu7
            // 
            this.cb_menu7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu7.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu7.FormattingEnabled = true;
            this.cb_menu7.ItemHeight = 15;
            this.cb_menu7.Location = new System.Drawing.Point(136, 11);
            this.cb_menu7.Name = "cb_menu7";
            this.cb_menu7.Size = new System.Drawing.Size(121, 23);
            this.cb_menu7.TabIndex = 3;
            this.cb_menu7.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.cb_setShort8);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.cb_menu8);
            this.panel8.Location = new System.Drawing.Point(15, 408);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(765, 52);
            this.panel8.TabIndex = 8;
            // 
            // cb_setShort8
            // 
            this.cb_setShort8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort8.Enabled = false;
            this.cb_setShort8.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort8.FormattingEnabled = true;
            this.cb_setShort8.ItemHeight = 15;
            this.cb_setShort8.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort8.Name = "cb_setShort8";
            this.cb_setShort8.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort8.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(23, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "퀵 메뉴 8";
            // 
            // cb_menu8
            // 
            this.cb_menu8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu8.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu8.FormattingEnabled = true;
            this.cb_menu8.ItemHeight = 15;
            this.cb_menu8.Location = new System.Drawing.Point(136, 11);
            this.cb_menu8.Name = "cb_menu8";
            this.cb_menu8.Size = new System.Drawing.Size(121, 23);
            this.cb_menu8.TabIndex = 3;
            this.cb_menu8.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.cb_setShort9);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.cb_menu9);
            this.panel9.Location = new System.Drawing.Point(15, 459);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(765, 52);
            this.panel9.TabIndex = 9;
            // 
            // cb_setShort9
            // 
            this.cb_setShort9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort9.Enabled = false;
            this.cb_setShort9.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort9.FormattingEnabled = true;
            this.cb_setShort9.ItemHeight = 15;
            this.cb_setShort9.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort9.Name = "cb_setShort9";
            this.cb_setShort9.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort9.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(23, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "퀵 메뉴 9";
            // 
            // cb_menu9
            // 
            this.cb_menu9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu9.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu9.FormattingEnabled = true;
            this.cb_menu9.ItemHeight = 15;
            this.cb_menu9.Location = new System.Drawing.Point(136, 11);
            this.cb_menu9.Name = "cb_menu9";
            this.cb_menu9.Size = new System.Drawing.Size(121, 23);
            this.cb_menu9.TabIndex = 3;
            this.cb_menu9.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.cb_setShort10);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.cb_menu10);
            this.panel10.Location = new System.Drawing.Point(15, 510);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(765, 52);
            this.panel10.TabIndex = 10;
            // 
            // cb_setShort10
            // 
            this.cb_setShort10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort10.Enabled = false;
            this.cb_setShort10.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort10.FormattingEnabled = true;
            this.cb_setShort10.ItemHeight = 15;
            this.cb_setShort10.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort10.Name = "cb_setShort10";
            this.cb_setShort10.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort10.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(23, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "퀵 메뉴 10";
            // 
            // cb_menu10
            // 
            this.cb_menu10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu10.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu10.FormattingEnabled = true;
            this.cb_menu10.ItemHeight = 15;
            this.cb_menu10.Location = new System.Drawing.Point(136, 11);
            this.cb_menu10.Name = "cb_menu10";
            this.cb_menu10.Size = new System.Drawing.Size(121, 23);
            this.cb_menu10.TabIndex = 3;
            this.cb_menu10.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.cb_setShort11);
            this.panel11.Controls.Add(this.label11);
            this.panel11.Controls.Add(this.cb_menu11);
            this.panel11.Location = new System.Drawing.Point(15, 561);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(765, 52);
            this.panel11.TabIndex = 11;
            // 
            // cb_setShort11
            // 
            this.cb_setShort11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort11.Enabled = false;
            this.cb_setShort11.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort11.FormattingEnabled = true;
            this.cb_setShort11.ItemHeight = 15;
            this.cb_setShort11.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort11.Name = "cb_setShort11";
            this.cb_setShort11.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort11.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(23, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "퀵 메뉴 11";
            // 
            // cb_menu11
            // 
            this.cb_menu11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu11.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu11.FormattingEnabled = true;
            this.cb_menu11.ItemHeight = 15;
            this.cb_menu11.Location = new System.Drawing.Point(136, 11);
            this.cb_menu11.Name = "cb_menu11";
            this.cb_menu11.Size = new System.Drawing.Size(121, 23);
            this.cb_menu11.TabIndex = 3;
            this.cb_menu11.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.cb_setShort12);
            this.panel12.Controls.Add(this.label12);
            this.panel12.Controls.Add(this.cb_menu12);
            this.panel12.Location = new System.Drawing.Point(15, 612);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(765, 52);
            this.panel12.TabIndex = 12;
            // 
            // cb_setShort12
            // 
            this.cb_setShort12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_setShort12.Enabled = false;
            this.cb_setShort12.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_setShort12.FormattingEnabled = true;
            this.cb_setShort12.ItemHeight = 15;
            this.cb_setShort12.Location = new System.Drawing.Point(263, 11);
            this.cb_setShort12.Name = "cb_setShort12";
            this.cb_setShort12.Size = new System.Drawing.Size(483, 23);
            this.cb_setShort12.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(23, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "퀵 메뉴 12";
            // 
            // cb_menu12
            // 
            this.cb_menu12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_menu12.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cb_menu12.FormattingEnabled = true;
            this.cb_menu12.ItemHeight = 15;
            this.cb_menu12.Location = new System.Drawing.Point(136, 11);
            this.cb_menu12.Name = "cb_menu12";
            this.cb_menu12.Size = new System.Drawing.Size(121, 23);
            this.cb_menu12.TabIndex = 3;
            this.cb_menu12.SelectedIndexChanged += new System.EventHandler(this.cb_menus_SelectedIndexChanged);
            // 
            // Quick_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 680);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_save);
            this.Name = "Quick_menu";
            this.Text = "퀵 메뉴";
            this.Load += new System.EventHandler(this.Quick_menu_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.ComboBox cb_menu1;
        private System.Windows.Forms.ComboBox cb_setShort1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cb_setShort2;
        private System.Windows.Forms.ComboBox cb_menu2;
        private System.Windows.Forms.ComboBox cb_setShort3;
        private System.Windows.Forms.ComboBox cb_menu3;
        private System.Windows.Forms.ComboBox cb_setShort4;
        private System.Windows.Forms.ComboBox cb_menu4;
        private System.Windows.Forms.ComboBox cb_setShort5;
        private System.Windows.Forms.ComboBox cb_menu5;
        private System.Windows.Forms.ComboBox cb_setShort6;
        private System.Windows.Forms.ComboBox cb_menu6;
        private System.Windows.Forms.ComboBox cb_setShort7;
        private System.Windows.Forms.ComboBox cb_menu7;
        private System.Windows.Forms.ComboBox cb_setShort8;
        private System.Windows.Forms.ComboBox cb_menu8;
        private System.Windows.Forms.ComboBox cb_setShort9;
        private System.Windows.Forms.ComboBox cb_menu9;
        private System.Windows.Forms.ComboBox cb_setShort10;
        private System.Windows.Forms.ComboBox cb_menu10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.ComboBox cb_setShort11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cb_menu11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ComboBox cb_setShort12;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cb_menu12;
    }
}