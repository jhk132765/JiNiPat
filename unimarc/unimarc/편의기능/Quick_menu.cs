﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WindowsFormsApp1.Convenience
{
    public partial class Quick_menu : Form
    {
        Main main;
        Helper_DB db = new Helper_DB();
        string compidx;
        public Quick_menu(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        private void Quick_menu_Load(object sender, EventArgs e)
        {
            db.DBcon();
            Init();
        }

        private void Init()
        {
            #region 메뉴 콤보박스 밑작업
            string[] MenuList = { 
                //"적용안함", "홈", "납품관리", "회계", "마크"
                "적용안함", "홈", "마크"
            };

            ComboBox[] Menu = {
                cb_menu1, cb_menu2, cb_menu3, cb_menu4, cb_menu5,
                cb_menu6, cb_menu7, cb_menu8, cb_menu9, cb_menu10,
                cb_menu11, cb_menu12
            };

            ComboBox[] ShortCombo = {
                cb_setShort1,
                cb_setShort2,
                cb_setShort3,
                cb_setShort4,
                cb_setShort5,
                cb_setShort6,
                cb_setShort7,
                cb_setShort8,
                cb_setShort9,
                cb_setShort10,
                cb_setShort11,
                cb_setShort12
            };

            for (int a = 0; a < Menu.Length; a++)
            {
                Menu[a].Items.AddRange(MenuList);
            }


            /// 저장된 퀵메뉴 입력 ///
            
            string[] Home = {
                "사용자 관리", "납품 / 거래처 관리", "주문처 관리", "도서정보 관리"
            };
            string[] Div = {
                "물품등록", "목록조회", "목록집계", "주문입력", "입고작업",
                "재고입력 및 조회", "반품처리"
            };
            string[] Acc = {
                "송금내역조회", "송금등록", "매입 집계", "매입 장부", "매출 입력", "매출 입금",
                "매출 조회", "매출 집계", "파트타임 관리"
            };
            string[] Marc = {
                "마크 추가", "마크 목록", "소장자료검색", "마크 정리", "ISBN 조회", "반입",
                "반출", "복본 조사", "DLS 복본 조사", "마크 수집", "전집관리",
                "검수", "저자기호", "DLS 조회 입력", "서류작성",
                "마크통계", "장비관리"
            };

            string[] QuickButton = {
                main.ShortCut1.Text, main.ShortCut2.Text, main.ShortCut3.Text, main.ShortCut4.Text, main.ShortCut5.Text,
                main.ShortCut6.Text, main.ShortCut7.Text, main.ShortCut8.Text, main.ShortCut9.Text, main.ShortCut10.Text,
                main.ShortCut11.Text, main.ShortCut12.Text
            };

            int count = 0;
            foreach (string text in QuickButton)
            {
                if (isContain(text, Home)) {
                    Menu[count].SelectedIndex = 1;
                    ShortCombo[count].SelectedItem = text;
                }
                // else if (isContain(text, Div)) {
                //     Menu[count].SelectedItem = 2;
                //     ShortCombo[count].SelectedItem = text;
                // }
                // 
                // else if (isContain(text, Acc)) {
                //     Menu[count].SelectedItem = 3;
                //     ShortCombo[count].SelectedItem = text;
                // }
                // 
                // else if (isContain(text, Marc)) {
                //     Menu[count].SelectedItem = 4;
                //     ShortCombo[count].SelectedItem = text;
                // }

                // TODO: ERP적용시 아래 else if 지울것.
                else if (isContain(text, Marc)) {
                    Menu[count].SelectedIndex = 2;
                    ShortCombo[count].SelectedItem = text;
                }
                // 여기까지

                else {
                    Menu[count].SelectedIndex = 0;
                }
                count++;
            }

            #endregion
        }

        bool isContain(string text, string[] MenuData)
        {
            foreach (string menu in MenuData)
            {
                if (menu.Contains(text))
                    return true;
            }
            return false;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            string Table = "User_ShortCut";

            string User = main.User_Name;

            ComboBox[] DetailMenu = {
                cb_setShort1, cb_setShort2, cb_setShort3, cb_setShort4, cb_setShort5,
                cb_setShort6, cb_setShort7, cb_setShort8, cb_setShort9, cb_setShort10,
                cb_setShort11, cb_setShort12
            };

            List<string> Column_L = new List<string> {
                "`id`"
            };
            List<string> Values_L = new List<string>{
                "\"" + User + "\""
            };

            for (int a = 0; a < DetailMenu.Length; a++)
            {
                if (DetailMenu[a].Text != "") {
                    Column_L.Add(string.Format("`ShortCut{0}`", a + 1));
                    Values_L.Add(string.Format("\"{0}\"", DetailMenu[a].Text));
                }
                else
                {
                    Column_L.Add(string.Format("`ShortCut{0}`", a + 1));
                    Values_L.Add(string.Format("\"\""));
                }
            }

            string Column = string.Join(", ", Column_L);
            string Values = string.Join(", ", Values_L);

            string InCmd = string.Format("INSERT INTO `{0}` ({1}) VALUES ({2}) ", Table, Column, Values);

            string UpCmd = "ON DUPLICATE KEY UPDATE ";
            for (int a = 0; a < Column_L.Count; a++)
            {
                UpCmd += string.Format("{0} = {1}", Column_L[a], Values_L[a]);
                if (a % Column_L.Count == Column_L.Count - 1)
                    UpCmd += ";";
                else
                    UpCmd += ", ";
            }

            string cmd = InCmd + UpCmd;
            db.DB_Send_CMD_reVoid(cmd);

            MessageBox.Show("저장완료!");

            main.SetBtnName();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cb_menus_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = sender as ComboBox;

            string[] comboName = {
                "cb_menu1", "cb_menu2", "cb_menu3", "cb_menu4", "cb_menu5",
                "cb_menu6", "cb_menu7", "cb_menu8", "cb_menu9", "cb_menu10",
                "cb_menu11", "cb_menu12"
            };

            ComboBox[] DetailMenu = { 
                cb_setShort1, cb_setShort2, cb_setShort3, cb_setShort4, cb_setShort5,
                cb_setShort6, cb_setShort7, cb_setShort8, cb_setShort9, cb_setShort10,
                cb_setShort11, cb_setShort12
            };

            string[] Home = {
                "사용자 관리", "납품 / 거래처 관리", "주문처 관리", "도서정보 관리"
            };

            string[] Div = {
                "물품등록", "목록조회", "목록집계", "주문입력", "입고작업",
                "재고입력 및 조회", "반품처리"
            };

            string[] Acc = {
                "송금내역조회", "송금등록", "매입 집계", "매입 장부", "매출 입력", "매출 입금",
                "매출 조회", "매출 집계", "파트타임 관리"
            };
            string[] Marc = {
                "마크 추가", "마크 목록", "소장자료검색", "마크 정리", "복본 조사", "ISBN 조회",
                "DVD/CD/LP 목록", "DVD/CD/LP 편목", 
                "반입", "반출", 
                "전집관리", "저자기호", "DLS 복본 조사", "DLS 조회 입력",
                "마크통계"
            };
            /* string[] Marc = {
                "마크 목록", "소장자료검색", "마크 정리", "ISBN 조회", "반입",
                "반출", "복본 조사", "DLS 복본 조사", "마크 수집", "전집관리",
                "검수", "저자기호", "DLS 조회 입력", "서류작성",
                "마크통계", "장비관리"
            };*/

            int count = 0;
            foreach (string name in comboName)
            {
                if (name == combo.Name)
                    break;
                count++;
            }

            if (DetailMenu[count].Items.Count > 0)
                DetailMenu[count].Items.Clear();

            DetailMenu[count].Enabled = true;

            switch (combo.SelectedIndex)
            {
                case 0:     // 적용안함
                    DetailMenu[count].Enabled = false;
                    break;
                case 1:     // 홈
                    DetailMenu[count].Items.AddRange(Home);
                    break;
                //case 2:      납품관리
                //    DetailMenu[count].Items.AddRange(Div);
                //    break;
                //case 3:      회계
                //    DetailMenu[count].Items.AddRange(Acc);
                //    break;
                case 2:     // 4: 마크
                    DetailMenu[count].Items.AddRange(Marc);
                    break;
                default:
                    break;
            }
        }
    }
}
