﻿namespace WindowsFormsApp1
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.홈ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.사업체정보 = new System.Windows.Forms.ToolStripMenuItem();
            this.사용자관리 = new System.Windows.Forms.ToolStripMenuItem();
            this.납품거래처관리 = new System.Windows.Forms.ToolStripMenuItem();
            this.주문처관리 = new System.Windows.Forms.ToolStripMenuItem();
            this.비밀번호변경 = new System.Windows.Forms.ToolStripMenuItem();
            this.도서정보관리 = new System.Windows.Forms.ToolStripMenuItem();
            this.사용대장 = new System.Windows.Forms.ToolStripMenuItem();
            this.납품관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.목록등록 = new System.Windows.Forms.ToolStripMenuItem();
            this.목록조회 = new System.Windows.Forms.ToolStripMenuItem();
            this.목록집계 = new System.Windows.Forms.ToolStripMenuItem();
            this.주문입력 = new System.Windows.Forms.ToolStripMenuItem();
            this.입고작업 = new System.Windows.Forms.ToolStripMenuItem();
            this.재고입력및조회 = new System.Windows.Forms.ToolStripMenuItem();
            this.반품처리 = new System.Windows.Forms.ToolStripMenuItem();
            this.회계ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.송금내역조회 = new System.Windows.Forms.ToolStripMenuItem();
            this.송금등록 = new System.Windows.Forms.ToolStripMenuItem();
            this.매입 = new System.Windows.Forms.ToolStripMenuItem();
            this.매입집계 = new System.Windows.Forms.ToolStripMenuItem();
            this.매입장부 = new System.Windows.Forms.ToolStripMenuItem();
            this.매입미결제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.매출 = new System.Windows.Forms.ToolStripMenuItem();
            this.매출입력 = new System.Windows.Forms.ToolStripMenuItem();
            this.매출입금 = new System.Windows.Forms.ToolStripMenuItem();
            this.매출조회 = new System.Windows.Forms.ToolStripMenuItem();
            this.매출집계 = new System.Windows.Forms.ToolStripMenuItem();
            this.파트타임관리 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.마크설정 = new System.Windows.Forms.ToolStripMenuItem();
            this.단축키설정 = new System.Windows.Forms.ToolStripMenuItem();
            this.매크로문구 = new System.Windows.Forms.ToolStripMenuItem();
            this.불용어 = new System.Windows.Forms.ToolStripMenuItem();
            this.작업지시서 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크작업 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크작성 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크목록 = new System.Windows.Forms.ToolStripMenuItem();
            this.소장자료검색 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크정리 = new System.Windows.Forms.ToolStripMenuItem();
            this.복본조사1 = new System.Windows.Forms.ToolStripMenuItem();
            this.iSBN조회 = new System.Windows.Forms.ToolStripMenuItem();
            this.dVDCDLPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.목록 = new System.Windows.Forms.ToolStripMenuItem();
            this.편목 = new System.Windows.Forms.ToolStripMenuItem();
            this.반입및반출 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크반입 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크반출 = new System.Windows.Forms.ToolStripMenuItem();
            this.부가기능 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크수집 = new System.Windows.Forms.ToolStripMenuItem();
            this.전집관리 = new System.Windows.Forms.ToolStripMenuItem();
            this.검수 = new System.Windows.Forms.ToolStripMenuItem();
            this.저자기호 = new System.Windows.Forms.ToolStripMenuItem();
            this.DLS = new System.Windows.Forms.ToolStripMenuItem();
            this.DLS조회 = new System.Windows.Forms.ToolStripMenuItem();
            this.dLS복본조사 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크기타 = new System.Windows.Forms.ToolStripMenuItem();
            this.서류작성 = new System.Windows.Forms.ToolStripMenuItem();
            this.마크통계 = new System.Windows.Forms.ToolStripMenuItem();
            this.장비관리 = new System.Windows.Forms.ToolStripMenuItem();
            this.작업일지ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.편의기능ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.캘린더 = new System.Windows.Forms.ToolStripMenuItem();
            this.채팅 = new System.Windows.Forms.ToolStripMenuItem();
            this.퀵메뉴 = new System.Windows.Forms.ToolStripMenuItem();
            this.게시판 = new System.Windows.Forms.ToolStripMenuItem();
            this.공지발송 = new System.Windows.Forms.ToolStripMenuItem();
            this.판매 = new System.Windows.Forms.ToolStripMenuItem();
            this.판매1 = new System.Windows.Forms.ToolStripMenuItem();
            this.정산 = new System.Windows.Forms.ToolStripMenuItem();
            this.판매마감 = new System.Windows.Forms.ToolStripMenuItem();
            this.회원관리 = new System.Windows.Forms.ToolStripMenuItem();
            this.마스터ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.이용자관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.신규사업자등록ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.기존사업자관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.공지발송ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.매출내역ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.이용자거래처조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.마크설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.일괄처리관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ShortCut12 = new System.Windows.Forms.Button();
            this.ShortCut11 = new System.Windows.Forms.Button();
            this.ShortCut10 = new System.Windows.Forms.Button();
            this.ShortCut9 = new System.Windows.Forms.Button();
            this.ShortCut8 = new System.Windows.Forms.Button();
            this.ShortCut6 = new System.Windows.Forms.Button();
            this.ShortCut5 = new System.Windows.Forms.Button();
            this.ShortCut7 = new System.Windows.Forms.Button();
            this.ShortCut3 = new System.Windows.Forms.Button();
            this.ShortCut4 = new System.Windows.Forms.Button();
            this.ShortCut2 = new System.Windows.Forms.Button();
            this.ShortCut1 = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.VersionText = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.botUserLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.IPText = new System.Windows.Forms.ToolStripLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.홈ToolStripMenuItem,
            this.납품관리ToolStripMenuItem,
            this.회계ToolStripMenuItem,
            this.마크ToolStripMenuItem,
            this.작업일지ToolStripMenuItem,
            this.편의기능ToolStripMenuItem,
            this.마스터ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1259, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 홈ToolStripMenuItem
            // 
            this.홈ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.사업체정보,
            this.사용자관리,
            this.납품거래처관리,
            this.주문처관리,
            this.비밀번호변경,
            this.도서정보관리,
            this.사용대장});
            this.홈ToolStripMenuItem.Name = "홈ToolStripMenuItem";
            this.홈ToolStripMenuItem.Size = new System.Drawing.Size(31, 20);
            this.홈ToolStripMenuItem.Text = "홈";
            // 
            // 사업체정보
            // 
            this.사업체정보.Name = "사업체정보";
            this.사업체정보.Size = new System.Drawing.Size(175, 22);
            this.사업체정보.Text = "사업체 정보";
            this.사업체정보.Click += new System.EventHandler(this.사용자정보ToolStripMenuItem_Click);
            // 
            // 사용자관리
            // 
            this.사용자관리.Name = "사용자관리";
            this.사용자관리.Size = new System.Drawing.Size(175, 22);
            this.사용자관리.Text = "사용자 관리";
            this.사용자관리.Click += new System.EventHandler(this.사용자관리ToolStripMenuItem_Click);
            // 
            // 납품거래처관리
            // 
            this.납품거래처관리.Name = "납품거래처관리";
            this.납품거래처관리.Size = new System.Drawing.Size(175, 22);
            this.납품거래처관리.Text = "납품 / 거래처 관리";
            this.납품거래처관리.Click += new System.EventHandler(this.납품거래처관리ToolStripMenuItem_Click);
            // 
            // 주문처관리
            // 
            this.주문처관리.Name = "주문처관리";
            this.주문처관리.Size = new System.Drawing.Size(175, 22);
            this.주문처관리.Text = "주문처 관리";
            this.주문처관리.Click += new System.EventHandler(this.주문처관리ToolStripMenuItem_Click);
            // 
            // 비밀번호변경
            // 
            this.비밀번호변경.Enabled = false;
            this.비밀번호변경.Name = "비밀번호변경";
            this.비밀번호변경.Size = new System.Drawing.Size(175, 22);
            this.비밀번호변경.Text = "비밀번호 변경";
            this.비밀번호변경.Click += new System.EventHandler(this.비밀번호변경ToolStripMenuItem_Click);
            // 
            // 도서정보관리
            // 
            this.도서정보관리.Name = "도서정보관리";
            this.도서정보관리.Size = new System.Drawing.Size(175, 22);
            this.도서정보관리.Text = "도서정보 관리";
            this.도서정보관리.Click += new System.EventHandler(this.도서정보관리ToolStripMenuItem_Click);
            // 
            // 사용대장
            // 
            this.사용대장.Enabled = false;
            this.사용대장.Name = "사용대장";
            this.사용대장.Size = new System.Drawing.Size(175, 22);
            this.사용대장.Text = "사용대장";
            this.사용대장.Click += new System.EventHandler(this.사용대장ToolStripMenuItem_Click);
            // 
            // 납품관리ToolStripMenuItem
            // 
            this.납품관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.목록등록,
            this.목록조회,
            this.목록집계,
            this.주문입력,
            this.입고작업,
            this.재고입력및조회,
            this.반품처리});
            this.납품관리ToolStripMenuItem.Name = "납품관리ToolStripMenuItem";
            this.납품관리ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.납품관리ToolStripMenuItem.Text = "납품관리";
            // 
            // 목록등록
            // 
            this.목록등록.Name = "목록등록";
            this.목록등록.Size = new System.Drawing.Size(166, 22);
            this.목록등록.Text = "물품등록";
            this.목록등록.Click += new System.EventHandler(this.물품등록ToolStripMenuItem_Click);
            // 
            // 목록조회
            // 
            this.목록조회.Name = "목록조회";
            this.목록조회.Size = new System.Drawing.Size(166, 22);
            this.목록조회.Text = "목록조회";
            this.목록조회.Click += new System.EventHandler(this.목록등록편의ToolStripMenuItem_Click);
            // 
            // 목록집계
            // 
            this.목록집계.Name = "목록집계";
            this.목록집계.Size = new System.Drawing.Size(166, 22);
            this.목록집계.Text = "목록집계";
            this.목록집계.Click += new System.EventHandler(this.목록집계ToolStripMenuItem_Click);
            // 
            // 주문입력
            // 
            this.주문입력.Name = "주문입력";
            this.주문입력.Size = new System.Drawing.Size(166, 22);
            this.주문입력.Text = "주문입력";
            this.주문입력.Click += new System.EventHandler(this.주문입력ToolStripMenuItem_Click);
            // 
            // 입고작업
            // 
            this.입고작업.Name = "입고작업";
            this.입고작업.Size = new System.Drawing.Size(166, 22);
            this.입고작업.Text = "입고작업";
            this.입고작업.Click += new System.EventHandler(this.매입ToolStripMenuItem_Click);
            // 
            // 재고입력및조회
            // 
            this.재고입력및조회.Name = "재고입력및조회";
            this.재고입력및조회.Size = new System.Drawing.Size(166, 22);
            this.재고입력및조회.Text = "재고입력 및 조회";
            this.재고입력및조회.Click += new System.EventHandler(this.재고입력및조회ToolStripMenuItem_Click);
            // 
            // 반품처리
            // 
            this.반품처리.Name = "반품처리";
            this.반품처리.Size = new System.Drawing.Size(166, 22);
            this.반품처리.Text = "반품처리";
            this.반품처리.Click += new System.EventHandler(this.반품처리ToolStripMenuItem_Click);
            // 
            // 회계ToolStripMenuItem
            // 
            this.회계ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.송금내역조회,
            this.송금등록,
            this.매입,
            this.매출,
            this.파트타임관리});
            this.회계ToolStripMenuItem.Name = "회계ToolStripMenuItem";
            this.회계ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.회계ToolStripMenuItem.Text = "회계";
            // 
            // 송금내역조회
            // 
            this.송금내역조회.Name = "송금내역조회";
            this.송금내역조회.Size = new System.Drawing.Size(154, 22);
            this.송금내역조회.Text = "송금 내역 조회";
            this.송금내역조회.Click += new System.EventHandler(this.송금내역조회ToolStripMenuItem_Click);
            // 
            // 송금등록
            // 
            this.송금등록.Name = "송금등록";
            this.송금등록.Size = new System.Drawing.Size(154, 22);
            this.송금등록.Text = "송금 등록";
            this.송금등록.Click += new System.EventHandler(this.송금등록ToolStripMenuItem_Click);
            // 
            // 매입
            // 
            this.매입.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.매입집계,
            this.매입장부,
            this.매입미결제ToolStripMenuItem});
            this.매입.Name = "매입";
            this.매입.Size = new System.Drawing.Size(154, 22);
            this.매입.Text = "매입";
            // 
            // 매입집계
            // 
            this.매입집계.Name = "매입집계";
            this.매입집계.Size = new System.Drawing.Size(138, 22);
            this.매입집계.Text = "매입 집계";
            this.매입집계.Click += new System.EventHandler(this.매입집계ToolStripMenuItem_Click);
            // 
            // 매입장부
            // 
            this.매입장부.Name = "매입장부";
            this.매입장부.Size = new System.Drawing.Size(138, 22);
            this.매입장부.Text = "매입 장부";
            this.매입장부.Click += new System.EventHandler(this.매입장부ToolStripMenuItem1_Click);
            // 
            // 매입미결제ToolStripMenuItem
            // 
            this.매입미결제ToolStripMenuItem.Enabled = false;
            this.매입미결제ToolStripMenuItem.Name = "매입미결제ToolStripMenuItem";
            this.매입미결제ToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.매입미결제ToolStripMenuItem.Text = "매입 미결제";
            this.매입미결제ToolStripMenuItem.Click += new System.EventHandler(this.매입미결제ToolStripMenuItem_Click);
            // 
            // 매출
            // 
            this.매출.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.매출입력,
            this.매출입금,
            this.매출조회,
            this.매출집계});
            this.매출.Name = "매출";
            this.매출.Size = new System.Drawing.Size(154, 22);
            this.매출.Text = "매출";
            // 
            // 매출입력
            // 
            this.매출입력.Name = "매출입력";
            this.매출입력.Size = new System.Drawing.Size(126, 22);
            this.매출입력.Text = "매출 입력";
            this.매출입력.Click += new System.EventHandler(this.매출입력ToolStripMenuItem1_Click);
            // 
            // 매출입금
            // 
            this.매출입금.Name = "매출입금";
            this.매출입금.Size = new System.Drawing.Size(126, 22);
            this.매출입금.Text = "매출 입금";
            this.매출입금.Click += new System.EventHandler(this.매출입금toolStripMenuItem1_Click_1);
            // 
            // 매출조회
            // 
            this.매출조회.Name = "매출조회";
            this.매출조회.Size = new System.Drawing.Size(126, 22);
            this.매출조회.Text = "매출 조회";
            this.매출조회.Click += new System.EventHandler(this.매출장부ToolStripMenuItem_Click);
            // 
            // 매출집계
            // 
            this.매출집계.Name = "매출집계";
            this.매출집계.Size = new System.Drawing.Size(126, 22);
            this.매출집계.Text = "매출 집계";
            this.매출집계.Click += new System.EventHandler(this.매출입금ToolStripMenuItem1_Click);
            // 
            // 파트타임관리
            // 
            this.파트타임관리.Name = "파트타임관리";
            this.파트타임관리.Size = new System.Drawing.Size(154, 22);
            this.파트타임관리.Text = "파트타임 관리";
            this.파트타임관리.Click += new System.EventHandler(this.파트타임관리ToolStripMenuItem_Click);
            // 
            // 마크ToolStripMenuItem
            // 
            this.마크ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.마크설정,
            this.마크작업,
            this.dVDCDLPToolStripMenuItem,
            this.반입및반출,
            this.부가기능,
            this.DLS,
            this.마크기타});
            this.마크ToolStripMenuItem.Name = "마크ToolStripMenuItem";
            this.마크ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.마크ToolStripMenuItem.Text = "마크";
            // 
            // 마크설정
            // 
            this.마크설정.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.단축키설정,
            this.매크로문구,
            this.불용어,
            this.작업지시서});
            this.마크설정.Name = "마크설정";
            this.마크설정.Size = new System.Drawing.Size(180, 22);
            this.마크설정.Text = "설정";
            // 
            // 단축키설정
            // 
            this.단축키설정.Name = "단축키설정";
            this.단축키설정.Size = new System.Drawing.Size(138, 22);
            this.단축키설정.Text = "단축키";
            this.단축키설정.Visible = false;
            this.단축키설정.Click += new System.EventHandler(this.단축키설정ToolStripMenuItem_Click);
            // 
            // 매크로문구
            // 
            this.매크로문구.Name = "매크로문구";
            this.매크로문구.Size = new System.Drawing.Size(138, 22);
            this.매크로문구.Text = "매크로 문구";
            this.매크로문구.Click += new System.EventHandler(this.매크로문구설정ToolStripMenuItem_Click);
            // 
            // 불용어
            // 
            this.불용어.Name = "불용어";
            this.불용어.Size = new System.Drawing.Size(138, 22);
            this.불용어.Text = "불용어";
            this.불용어.Visible = false;
            this.불용어.Click += new System.EventHandler(this.불용어ToolStripMenuItem_Click);
            // 
            // 작업지시서
            // 
            this.작업지시서.Name = "작업지시서";
            this.작업지시서.Size = new System.Drawing.Size(138, 22);
            this.작업지시서.Text = "작업지시서";
            this.작업지시서.Visible = false;
            this.작업지시서.Click += new System.EventHandler(this.작업지시서ToolStripMenuItem_Click);
            // 
            // 마크작업
            // 
            this.마크작업.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.마크작성,
            this.마크목록,
            this.소장자료검색,
            this.마크정리,
            this.복본조사1,
            this.iSBN조회});
            this.마크작업.Name = "마크작업";
            this.마크작업.Size = new System.Drawing.Size(180, 22);
            this.마크작업.Text = "마크 작업";
            // 
            // 마크작성
            // 
            this.마크작성.Name = "마크작성";
            this.마크작성.Size = new System.Drawing.Size(180, 22);
            this.마크작성.Text = "마크 작성";
            this.마크작성.Click += new System.EventHandler(this.마크작성ToolStripMenuItem_Click);
            // 
            // 마크목록
            // 
            this.마크목록.Name = "마크목록";
            this.마크목록.Size = new System.Drawing.Size(180, 22);
            this.마크목록.Text = "마크 목록";
            this.마크목록.Click += new System.EventHandler(this.마크목록ToolStripMenuItem_Click);
            // 
            // 소장자료검색
            // 
            this.소장자료검색.Name = "소장자료검색";
            this.소장자료검색.Size = new System.Drawing.Size(180, 22);
            this.소장자료검색.Text = "소장자료검색";
            this.소장자료검색.Click += new System.EventHandler(this.소장자료검색ToolStripMenuItem_Click);
            // 
            // 마크정리
            // 
            this.마크정리.Name = "마크정리";
            this.마크정리.Size = new System.Drawing.Size(180, 22);
            this.마크정리.Text = "마크 정리";
            this.마크정리.Click += new System.EventHandler(this.마크정리ToolStripMenuItem_Click);
            // 
            // 복본조사1
            // 
            this.복본조사1.Name = "복본조사1";
            this.복본조사1.Size = new System.Drawing.Size(180, 22);
            this.복본조사1.Text = "복본조사";
            this.복본조사1.Click += new System.EventHandler(this.복본조사ToolStripMenuItem1_Click);
            // 
            // iSBN조회
            // 
            this.iSBN조회.Name = "iSBN조회";
            this.iSBN조회.Size = new System.Drawing.Size(180, 22);
            this.iSBN조회.Text = "ISBN 조회";
            this.iSBN조회.Click += new System.EventHandler(this.iSBN조회ToolStripMenuItem_Click);
            // 
            // dVDCDLPToolStripMenuItem
            // 
            this.dVDCDLPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.목록,
            this.편목});
            this.dVDCDLPToolStripMenuItem.Name = "dVDCDLPToolStripMenuItem";
            this.dVDCDLPToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.dVDCDLPToolStripMenuItem.Text = "DVD / CD / LP";
            // 
            // 목록
            // 
            this.목록.Enabled = false;
            this.목록.Name = "목록";
            this.목록.Size = new System.Drawing.Size(180, 22);
            this.목록.Text = "목록";
            this.목록.Click += new System.EventHandler(this.목록_Click);
            // 
            // 편목
            // 
            this.편목.Name = "편목";
            this.편목.Size = new System.Drawing.Size(180, 22);
            this.편목.Text = "편목";
            this.편목.Click += new System.EventHandler(this.편목ToolStripMenuItem_Click);
            // 
            // 반입및반출
            // 
            this.반입및반출.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.마크반입,
            this.마크반출});
            this.반입및반출.Name = "반입및반출";
            this.반입및반출.Size = new System.Drawing.Size(180, 22);
            this.반입및반출.Text = "반입 및 반출";
            // 
            // 마크반입
            // 
            this.마크반입.Name = "마크반입";
            this.마크반입.Size = new System.Drawing.Size(98, 22);
            this.마크반입.Text = "반입";
            this.마크반입.Click += new System.EventHandler(this.반입ToolStripMenuItem_Click);
            // 
            // 마크반출
            // 
            this.마크반출.Name = "마크반출";
            this.마크반출.Size = new System.Drawing.Size(98, 22);
            this.마크반출.Text = "반출";
            this.마크반출.Click += new System.EventHandler(this.반출ToolStripMenuItem_Click);
            // 
            // 부가기능
            // 
            this.부가기능.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.마크수집,
            this.전집관리,
            this.검수,
            this.저자기호});
            this.부가기능.Name = "부가기능";
            this.부가기능.Size = new System.Drawing.Size(180, 22);
            this.부가기능.Text = "부가기능";
            // 
            // 마크수집
            // 
            this.마크수집.Enabled = false;
            this.마크수집.Name = "마크수집";
            this.마크수집.Size = new System.Drawing.Size(122, 22);
            this.마크수집.Text = "마크수집";
            this.마크수집.Visible = false;
            this.마크수집.Click += new System.EventHandler(this.마크수집ToolStripMenuItem_Click);
            // 
            // 전집관리
            // 
            this.전집관리.Name = "전집관리";
            this.전집관리.Size = new System.Drawing.Size(122, 22);
            this.전집관리.Text = "전집관리";
            this.전집관리.Click += new System.EventHandler(this.전집관리ToolStripMenuItem1_Click);
            // 
            // 검수
            // 
            this.검수.Enabled = false;
            this.검수.Name = "검수";
            this.검수.Size = new System.Drawing.Size(122, 22);
            this.검수.Text = "검수";
            this.검수.Visible = false;
            this.검수.Click += new System.EventHandler(this.검수ToolStripMenuItem_Click);
            // 
            // 저자기호
            // 
            this.저자기호.Name = "저자기호";
            this.저자기호.Size = new System.Drawing.Size(122, 22);
            this.저자기호.Text = "저자기호";
            this.저자기호.Click += new System.EventHandler(this.저자기호ToolStripMenuItem_Click);
            // 
            // DLS
            // 
            this.DLS.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DLS조회,
            this.dLS복본조사});
            this.DLS.Name = "DLS";
            this.DLS.Size = new System.Drawing.Size(180, 22);
            this.DLS.Text = "DLS";
            // 
            // DLS조회
            // 
            this.DLS조회.Name = "DLS조회";
            this.DLS조회.Size = new System.Drawing.Size(154, 22);
            this.DLS조회.Text = "DLS_조회/입력";
            this.DLS조회.Click += new System.EventHandler(this.dLS조회ToolStripMenuItem_Click);
            // 
            // dLS복본조사
            // 
            this.dLS복본조사.Name = "dLS복본조사";
            this.dLS복본조사.Size = new System.Drawing.Size(154, 22);
            this.dLS복본조사.Text = "DLS 복본조사";
            this.dLS복본조사.Click += new System.EventHandler(this.dLS복본조사ToolStripMenuItem_Click);
            // 
            // 마크기타
            // 
            this.마크기타.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.서류작성,
            this.마크통계,
            this.장비관리});
            this.마크기타.Name = "마크기타";
            this.마크기타.Size = new System.Drawing.Size(180, 22);
            this.마크기타.Text = "기타";
            // 
            // 서류작성
            // 
            this.서류작성.Enabled = false;
            this.서류작성.Name = "서류작성";
            this.서류작성.Size = new System.Drawing.Size(122, 22);
            this.서류작성.Text = "서류작성";
            this.서류작성.Visible = false;
            this.서류작성.Click += new System.EventHandler(this.서류작성ToolStripMenuItem_Click);
            // 
            // 마크통계
            // 
            this.마크통계.Name = "마크통계";
            this.마크통계.Size = new System.Drawing.Size(122, 22);
            this.마크통계.Text = "마크통계";
            this.마크통계.Click += new System.EventHandler(this.마크통계ToolStripMenuItem_Click);
            // 
            // 장비관리
            // 
            this.장비관리.Enabled = false;
            this.장비관리.Name = "장비관리";
            this.장비관리.Size = new System.Drawing.Size(122, 22);
            this.장비관리.Text = "장비관리";
            this.장비관리.Visible = false;
            this.장비관리.Click += new System.EventHandler(this.장비관리ToolStripMenuItem1_Click);
            // 
            // 작업일지ToolStripMenuItem
            // 
            this.작업일지ToolStripMenuItem.Enabled = false;
            this.작업일지ToolStripMenuItem.Name = "작업일지ToolStripMenuItem";
            this.작업일지ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.작업일지ToolStripMenuItem.Text = "작업일지";
            this.작업일지ToolStripMenuItem.Click += new System.EventHandler(this.작업일지ToolStripMenuItem_Click);
            // 
            // 편의기능ToolStripMenuItem
            // 
            this.편의기능ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.캘린더,
            this.채팅,
            this.퀵메뉴,
            this.게시판,
            this.공지발송,
            this.판매});
            this.편의기능ToolStripMenuItem.Name = "편의기능ToolStripMenuItem";
            this.편의기능ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.편의기능ToolStripMenuItem.Text = "편의기능";
            // 
            // 캘린더
            // 
            this.캘린더.Name = "캘린더";
            this.캘린더.Size = new System.Drawing.Size(122, 22);
            this.캘린더.Text = "캘린더";
            this.캘린더.Click += new System.EventHandler(this.캘린더ToolStripMenuItem_Click);
            // 
            // 채팅
            // 
            this.채팅.Name = "채팅";
            this.채팅.Size = new System.Drawing.Size(122, 22);
            this.채팅.Text = "채팅";
            this.채팅.Visible = false;
            this.채팅.Click += new System.EventHandler(this.채팅ToolStripMenuItem_Click);
            // 
            // 퀵메뉴
            // 
            this.퀵메뉴.Name = "퀵메뉴";
            this.퀵메뉴.Size = new System.Drawing.Size(122, 22);
            this.퀵메뉴.Text = "퀵메뉴";
            this.퀵메뉴.Click += new System.EventHandler(this.퀵메뉴ToolStripMenuItem_Click);
            // 
            // 게시판
            // 
            this.게시판.Name = "게시판";
            this.게시판.Size = new System.Drawing.Size(122, 22);
            this.게시판.Text = "게시판";
            this.게시판.Visible = false;
            this.게시판.Click += new System.EventHandler(this.게시판ToolStripMenuItem_Click);
            // 
            // 공지발송
            // 
            this.공지발송.Name = "공지발송";
            this.공지발송.Size = new System.Drawing.Size(122, 22);
            this.공지발송.Text = "공지발송";
            this.공지발송.Visible = false;
            this.공지발송.Click += new System.EventHandler(this.공지발송ToolStripMenuItem_Click);
            // 
            // 판매
            // 
            this.판매.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.판매1,
            this.정산,
            this.판매마감,
            this.회원관리});
            this.판매.Name = "판매";
            this.판매.Size = new System.Drawing.Size(122, 22);
            this.판매.Text = "판매";
            this.판매.Visible = false;
            // 
            // 판매1
            // 
            this.판매1.Name = "판매1";
            this.판매1.Size = new System.Drawing.Size(122, 22);
            this.판매1.Text = "판매";
            this.판매1.Click += new System.EventHandler(this.판매ToolStripMenuItem1_Click);
            // 
            // 정산
            // 
            this.정산.Name = "정산";
            this.정산.Size = new System.Drawing.Size(122, 22);
            this.정산.Text = "정산";
            this.정산.Click += new System.EventHandler(this.정산ToolStripMenuItem_Click);
            // 
            // 판매마감
            // 
            this.판매마감.Name = "판매마감";
            this.판매마감.Size = new System.Drawing.Size(122, 22);
            this.판매마감.Text = "판매마감";
            this.판매마감.Click += new System.EventHandler(this.판매마감ToolStripMenuItem_Click);
            // 
            // 회원관리
            // 
            this.회원관리.Name = "회원관리";
            this.회원관리.Size = new System.Drawing.Size(122, 22);
            this.회원관리.Text = "회원관리";
            this.회원관리.Click += new System.EventHandler(this.회원관리ToolStripMenuItem_Click);
            // 
            // 마스터ToolStripMenuItem
            // 
            this.마스터ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.이용자관리ToolStripMenuItem,
            this.공지발송ToolStripMenuItem1,
            this.매출내역ToolStripMenuItem,
            this.이용자거래처조회ToolStripMenuItem,
            this.마크설정ToolStripMenuItem,
            this.일괄처리관리ToolStripMenuItem});
            this.마스터ToolStripMenuItem.Name = "마스터ToolStripMenuItem";
            this.마스터ToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.마스터ToolStripMenuItem.Text = "마스터";
            // 
            // 이용자관리ToolStripMenuItem
            // 
            this.이용자관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.신규사업자등록ToolStripMenuItem,
            this.기존사업자관리ToolStripMenuItem});
            this.이용자관리ToolStripMenuItem.Name = "이용자관리ToolStripMenuItem";
            this.이용자관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.이용자관리ToolStripMenuItem.Text = "이용자 관리";
            // 
            // 신규사업자등록ToolStripMenuItem
            // 
            this.신규사업자등록ToolStripMenuItem.Name = "신규사업자등록ToolStripMenuItem";
            this.신규사업자등록ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.신규사업자등록ToolStripMenuItem.Text = "신규 사업자 등록";
            this.신규사업자등록ToolStripMenuItem.Click += new System.EventHandler(this.신규사업자등록ToolStripMenuItem_Click);
            // 
            // 기존사업자관리ToolStripMenuItem
            // 
            this.기존사업자관리ToolStripMenuItem.Name = "기존사업자관리ToolStripMenuItem";
            this.기존사업자관리ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.기존사업자관리ToolStripMenuItem.Text = "기존 사업자 관리";
            this.기존사업자관리ToolStripMenuItem.Click += new System.EventHandler(this.기존사업자관리ToolStripMenuItem_Click);
            // 
            // 공지발송ToolStripMenuItem1
            // 
            this.공지발송ToolStripMenuItem1.Name = "공지발송ToolStripMenuItem1";
            this.공지발송ToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.공지발송ToolStripMenuItem1.Text = "공지 발송";
            this.공지발송ToolStripMenuItem1.Click += new System.EventHandler(this.공지발송ToolStripMenuItem1_Click);
            // 
            // 매출내역ToolStripMenuItem
            // 
            this.매출내역ToolStripMenuItem.Name = "매출내역ToolStripMenuItem";
            this.매출내역ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.매출내역ToolStripMenuItem.Text = "매출내역";
            this.매출내역ToolStripMenuItem.Click += new System.EventHandler(this.매출내역ToolStripMenuItem_Click);
            // 
            // 이용자거래처조회ToolStripMenuItem
            // 
            this.이용자거래처조회ToolStripMenuItem.Name = "이용자거래처조회ToolStripMenuItem";
            this.이용자거래처조회ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.이용자거래처조회ToolStripMenuItem.Text = "이용자 거래처 조회";
            this.이용자거래처조회ToolStripMenuItem.Click += new System.EventHandler(this.이용자거래처조회ToolStripMenuItem_Click);
            // 
            // 마크설정ToolStripMenuItem
            // 
            this.마크설정ToolStripMenuItem.Name = "마크설정ToolStripMenuItem";
            this.마크설정ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.마크설정ToolStripMenuItem.Text = "마크설정";
            this.마크설정ToolStripMenuItem.Click += new System.EventHandler(this.마크설정ToolStripMenuItem_Click);
            // 
            // 일괄처리관리ToolStripMenuItem
            // 
            this.일괄처리관리ToolStripMenuItem.Name = "일괄처리관리ToolStripMenuItem";
            this.일괄처리관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.일괄처리관리ToolStripMenuItem.Text = "일괄처리 관리";
            this.일괄처리관리ToolStripMenuItem.Click += new System.EventHandler(this.일괄처리관리ToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.ShortCut12);
            this.panel1.Controls.Add(this.ShortCut11);
            this.panel1.Controls.Add(this.ShortCut10);
            this.panel1.Controls.Add(this.ShortCut9);
            this.panel1.Controls.Add(this.ShortCut8);
            this.panel1.Controls.Add(this.ShortCut6);
            this.panel1.Controls.Add(this.ShortCut5);
            this.panel1.Controls.Add(this.ShortCut7);
            this.panel1.Controls.Add(this.ShortCut3);
            this.panel1.Controls.Add(this.ShortCut4);
            this.panel1.Controls.Add(this.ShortCut2);
            this.panel1.Controls.Add(this.ShortCut1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1259, 64);
            this.panel1.TabIndex = 2;
            // 
            // ShortCut12
            // 
            this.ShortCut12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ShortCut12.Enabled = false;
            this.ShortCut12.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut12.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut12.Location = new System.Drawing.Point(859, 1);
            this.ShortCut12.Name = "ShortCut12";
            this.ShortCut12.Size = new System.Drawing.Size(60, 60);
            this.ShortCut12.TabIndex = 2;
            this.ShortCut12.Text = "즐겨찾기12";
            this.ShortCut12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut12.UseVisualStyleBackColor = true;
            this.ShortCut12.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut11
            // 
            this.ShortCut11.Enabled = false;
            this.ShortCut11.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut11.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut11.Location = new System.Drawing.Point(782, 1);
            this.ShortCut11.Name = "ShortCut11";
            this.ShortCut11.Size = new System.Drawing.Size(60, 60);
            this.ShortCut11.TabIndex = 2;
            this.ShortCut11.Text = "즐겨찾기11";
            this.ShortCut11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut11.UseVisualStyleBackColor = true;
            this.ShortCut11.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut10
            // 
            this.ShortCut10.Enabled = false;
            this.ShortCut10.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut10.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut10.Location = new System.Drawing.Point(705, 1);
            this.ShortCut10.Name = "ShortCut10";
            this.ShortCut10.Size = new System.Drawing.Size(60, 60);
            this.ShortCut10.TabIndex = 2;
            this.ShortCut10.Text = "즐겨찾기10";
            this.ShortCut10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut10.UseVisualStyleBackColor = true;
            this.ShortCut10.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut9
            // 
            this.ShortCut9.Enabled = false;
            this.ShortCut9.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut9.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut9.Location = new System.Drawing.Point(628, 1);
            this.ShortCut9.Name = "ShortCut9";
            this.ShortCut9.Size = new System.Drawing.Size(60, 60);
            this.ShortCut9.TabIndex = 2;
            this.ShortCut9.Text = "즐겨찾기9";
            this.ShortCut9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut9.UseVisualStyleBackColor = true;
            this.ShortCut9.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut8
            // 
            this.ShortCut8.Enabled = false;
            this.ShortCut8.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut8.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut8.Location = new System.Drawing.Point(551, 1);
            this.ShortCut8.Name = "ShortCut8";
            this.ShortCut8.Size = new System.Drawing.Size(60, 60);
            this.ShortCut8.TabIndex = 1;
            this.ShortCut8.Text = "즐겨찾기8";
            this.ShortCut8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut8.UseVisualStyleBackColor = true;
            this.ShortCut8.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut6
            // 
            this.ShortCut6.Enabled = false;
            this.ShortCut6.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut6.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut6.Location = new System.Drawing.Point(397, 1);
            this.ShortCut6.Name = "ShortCut6";
            this.ShortCut6.Size = new System.Drawing.Size(60, 60);
            this.ShortCut6.TabIndex = 2;
            this.ShortCut6.Text = "즐겨찾기6";
            this.ShortCut6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut6.UseVisualStyleBackColor = true;
            this.ShortCut6.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut5
            // 
            this.ShortCut5.Enabled = false;
            this.ShortCut5.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut5.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut5.Location = new System.Drawing.Point(320, 1);
            this.ShortCut5.Name = "ShortCut5";
            this.ShortCut5.Size = new System.Drawing.Size(60, 60);
            this.ShortCut5.TabIndex = 1;
            this.ShortCut5.Text = "즐겨찾기5";
            this.ShortCut5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut5.UseVisualStyleBackColor = true;
            this.ShortCut5.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut7
            // 
            this.ShortCut7.Enabled = false;
            this.ShortCut7.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut7.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut7.Location = new System.Drawing.Point(474, 1);
            this.ShortCut7.Name = "ShortCut7";
            this.ShortCut7.Size = new System.Drawing.Size(60, 60);
            this.ShortCut7.TabIndex = 0;
            this.ShortCut7.Text = "즐겨찾기7";
            this.ShortCut7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut7.UseVisualStyleBackColor = true;
            this.ShortCut7.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut3
            // 
            this.ShortCut3.Enabled = false;
            this.ShortCut3.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut3.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut3.Location = new System.Drawing.Point(166, 1);
            this.ShortCut3.Name = "ShortCut3";
            this.ShortCut3.Size = new System.Drawing.Size(60, 60);
            this.ShortCut3.TabIndex = 2;
            this.ShortCut3.Text = "즐겨찾기3";
            this.ShortCut3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut3.UseVisualStyleBackColor = true;
            this.ShortCut3.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut4
            // 
            this.ShortCut4.Enabled = false;
            this.ShortCut4.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut4.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut4.Location = new System.Drawing.Point(243, 1);
            this.ShortCut4.Name = "ShortCut4";
            this.ShortCut4.Size = new System.Drawing.Size(60, 60);
            this.ShortCut4.TabIndex = 0;
            this.ShortCut4.Text = "즐겨찾기4";
            this.ShortCut4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut4.UseVisualStyleBackColor = true;
            this.ShortCut4.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut2
            // 
            this.ShortCut2.Enabled = false;
            this.ShortCut2.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut2.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut2.Location = new System.Drawing.Point(89, 1);
            this.ShortCut2.Name = "ShortCut2";
            this.ShortCut2.Size = new System.Drawing.Size(60, 60);
            this.ShortCut2.TabIndex = 1;
            this.ShortCut2.Text = "즐겨찾기2";
            this.ShortCut2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut2.UseVisualStyleBackColor = true;
            this.ShortCut2.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // ShortCut1
            // 
            this.ShortCut1.Enabled = false;
            this.ShortCut1.Font = new System.Drawing.Font("굴림", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ShortCut1.ForeColor = System.Drawing.Color.Transparent;
            this.ShortCut1.Location = new System.Drawing.Point(12, 1);
            this.ShortCut1.Name = "ShortCut1";
            this.ShortCut1.Size = new System.Drawing.Size(60, 60);
            this.ShortCut1.TabIndex = 0;
            this.ShortCut1.Text = "즐겨찾기1";
            this.ShortCut1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShortCut1.UseVisualStyleBackColor = true;
            this.ShortCut1.Click += new System.EventHandler(this.Btn_ShortCut_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.VersionText,
            this.toolStripSeparator1,
            this.botUserLabel,
            this.toolStripSeparator2,
            this.toolStripSeparator3,
            this.IPText});
            this.toolStrip1.Location = new System.Drawing.Point(0, 681);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1259, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(43, 22);
            this.toolStripLabel2.Text = "회사명";
            // 
            // VersionText
            // 
            this.VersionText.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.VersionText.Name = "VersionText";
            this.VersionText.Size = new System.Drawing.Size(116, 22);
            this.VersionText.Text = "UniMarc Ver 0.0000";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // botUserLabel
            // 
            this.botUserLabel.Name = "botUserLabel";
            this.botUserLabel.Size = new System.Drawing.Size(43, 22);
            this.botUserLabel.Text = "이용자";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // IPText
            // 
            this.IPText.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.IPText.Name = "IPText";
            this.IPText.Size = new System.Drawing.Size(154, 22);
            this.IPText.Text = "접속 아이피 : 0.000.00.000";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1259, 706);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "메인";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 홈ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 사용자관리;
        private System.Windows.Forms.ToolStripMenuItem 납품거래처관리;
        private System.Windows.Forms.ToolStripMenuItem 주문처관리;
        private System.Windows.Forms.ToolStripMenuItem 비밀번호변경;
        private System.Windows.Forms.ToolStripMenuItem 도서정보관리;
        private System.Windows.Forms.ToolStripMenuItem 사용대장;
        private System.Windows.Forms.ToolStripMenuItem 납품관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 목록등록;
        private System.Windows.Forms.ToolStripMenuItem 목록조회;
        private System.Windows.Forms.ToolStripMenuItem 목록집계;
        private System.Windows.Forms.ToolStripMenuItem 주문입력;
        private System.Windows.Forms.ToolStripMenuItem 입고작업;
        private System.Windows.Forms.ToolStripMenuItem 재고입력및조회;
        private System.Windows.Forms.ToolStripMenuItem 회계ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 송금등록;
        private System.Windows.Forms.ToolStripMenuItem 매입;
        private System.Windows.Forms.ToolStripMenuItem 매출;
        private System.Windows.Forms.ToolStripMenuItem 매입장부;
        private System.Windows.Forms.ToolStripMenuItem 매입집계;
        private System.Windows.Forms.ToolStripMenuItem 매입미결제ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 매출입력;
        private System.Windows.Forms.ToolStripMenuItem 매출집계;
        private System.Windows.Forms.ToolStripMenuItem 매출조회;
        private System.Windows.Forms.ToolStripMenuItem 마크ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 마크설정;
        private System.Windows.Forms.ToolStripMenuItem 단축키설정;
        private System.Windows.Forms.ToolStripMenuItem 매크로문구;
        private System.Windows.Forms.ToolStripMenuItem 불용어;
        private System.Windows.Forms.ToolStripMenuItem 작업일지ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 편의기능ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 캘린더;
        private System.Windows.Forms.ToolStripMenuItem 채팅;
        private System.Windows.Forms.ToolStripMenuItem 퀵메뉴;
        private System.Windows.Forms.ToolStripMenuItem 게시판;
        private System.Windows.Forms.ToolStripMenuItem 공지발송;
        private System.Windows.Forms.ToolStripMenuItem 판매;
        private System.Windows.Forms.ToolStripMenuItem 판매1;
        private System.Windows.Forms.ToolStripMenuItem 정산;
        private System.Windows.Forms.ToolStripMenuItem 판매마감;
        private System.Windows.Forms.ToolStripMenuItem 회원관리;
        private System.Windows.Forms.ToolStripMenuItem 마스터ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 이용자관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 공지발송ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 매출내역ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 이용자거래처조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 마크설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 일괄처리관리ToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem 송금내역조회;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripMenuItem 사업체정보;
        private System.Windows.Forms.ToolStripMenuItem 작업지시서;
        private System.Windows.Forms.ToolStripMenuItem 마크작업;
        private System.Windows.Forms.ToolStripMenuItem 마크목록;
        private System.Windows.Forms.ToolStripMenuItem 소장자료검색;
        private System.Windows.Forms.ToolStripMenuItem 마크정리;
        private System.Windows.Forms.ToolStripMenuItem 반입및반출;
        private System.Windows.Forms.ToolStripMenuItem 마크반입;
        private System.Windows.Forms.ToolStripMenuItem 마크반출;
        private System.Windows.Forms.ToolStripMenuItem 부가기능;
        private System.Windows.Forms.ToolStripMenuItem 마크수집;
        private System.Windows.Forms.ToolStripMenuItem 전집관리;
        private System.Windows.Forms.ToolStripMenuItem 검수;
        private System.Windows.Forms.ToolStripMenuItem 저자기호;
        private System.Windows.Forms.ToolStripMenuItem DLS;
        private System.Windows.Forms.ToolStripMenuItem DLS조회;
        private System.Windows.Forms.ToolStripMenuItem 마크기타;
        private System.Windows.Forms.ToolStripMenuItem 서류작성;
        private System.Windows.Forms.ToolStripMenuItem 마크통계;
        private System.Windows.Forms.ToolStripMenuItem 장비관리;
        private System.Windows.Forms.ToolStripMenuItem iSBN조회;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem 매출입금;
        private System.Windows.Forms.ToolStripMenuItem 반품처리;
        public System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripMenuItem 파트타임관리;
        public System.Windows.Forms.ToolStripLabel botUserLabel;
        private System.Windows.Forms.ToolStripMenuItem 복본조사1;
        private System.Windows.Forms.ToolStripMenuItem dLS복본조사;
        private System.Windows.Forms.ToolStripLabel VersionText;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.Button ShortCut3;
        public System.Windows.Forms.Button ShortCut2;
        public System.Windows.Forms.Button ShortCut1;
        public System.Windows.Forms.Button ShortCut10;
        public System.Windows.Forms.Button ShortCut9;
        public System.Windows.Forms.Button ShortCut8;
        public System.Windows.Forms.Button ShortCut6;
        public System.Windows.Forms.Button ShortCut5;
        public System.Windows.Forms.Button ShortCut7;
        public System.Windows.Forms.Button ShortCut4;
        public System.Windows.Forms.Button ShortCut12;
        public System.Windows.Forms.Button ShortCut11;
        private System.Windows.Forms.ToolStripMenuItem 신규사업자등록ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 기존사업자관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 마크작성;
        private System.Windows.Forms.ToolStripMenuItem dVDCDLPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 목록;
        private System.Windows.Forms.ToolStripMenuItem 편목;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        public System.Windows.Forms.ToolStripLabel IPText;
    }
}