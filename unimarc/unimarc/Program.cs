﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// 해당 애플리케이션의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DB_InitSetting();
            Application.Run(new Main());
        }
        static void DB_InitSetting()
        {
            UniMarc.Properties.Settings.Default.IP = ConvertIP(UniMarc.Properties.Settings.Default.IP);
            UniMarc.Properties.Settings.Default.Port = Convert2to10(UniMarc.Properties.Settings.Default.Port);
            UniMarc.Properties.Settings.Default.Uid = ConvertAscii(UniMarc.Properties.Settings.Default.Uid);
            UniMarc.Properties.Settings.Default.pwd = ConvertAscii(UniMarc.Properties.Settings.Default.pwd);
            UniMarc.Properties.Settings.Default.dbPort = Convert2to10(UniMarc.Properties.Settings.Default.dbPort);
            UniMarc.Properties.Settings.Default.dbUid = ConvertAscii(UniMarc.Properties.Settings.Default.dbUid);
            UniMarc.Properties.Settings.Default.dbPwd = ConvertAscii(UniMarc.Properties.Settings.Default.dbPwd);
        }

        public static string ConvertIP(string TargetIP)
        {
            string[] IpSplit = TargetIP.Split('.');
            int[] TenIP = new int[IpSplit.Length];

            for (int a = 0; a < IpSplit.Length; a++)
            {
                TenIP[a] = Convert.ToInt32(IpSplit[a], 2);
            }
            return String.Join(".", TenIP);
        }
        #region 2진수를 10진수로 바꾸는 함수 서브
        public static int Convert2to10(int Target)
        {
            return Convert.ToInt32(Target.ToString(), 2);
        }
        public static string Convert2to10(string Target)
        {
            return Convert.ToInt32(Target, 2).ToString();
        }
        #endregion

        public static string ConvertAscii(string Target)
        {
            string[] TargetSplit = Target.Split('.');

            string result = "";
            foreach (string s in TargetSplit)
            {
                result += Convert.ToChar(Convert.ToInt32(s));
            }

            return result;
        }

    }
}
