﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;

using WindowsFormsApp1.Account;
using WindowsFormsApp1.Convenience;
using WindowsFormsApp1.Delivery;
using WindowsFormsApp1.DLS;
using WindowsFormsApp1.Home;
using WindowsFormsApp1.Mac;
using WindowsFormsApp1.Work_log;
using WindowsFormsApp1.마크;
using WindowsFormsApp1.회계;
using WindowsFormsApp1.납품관리;
using UniMarc.회계;
using UniMarc.마크;
using UniMarc.Properties;

namespace WindowsFormsApp1
{
    public partial class Main : Form
    {
        Helper_DB _DB = new Helper_DB();
        IP ip = new IP();
        public string DB_User_Data;
        public string User;
        public string com_idx;
        
        public Main()
        {
            InitializeComponent();
        }

        public string User_Name { get; internal set; }

        private void Main_Load(object sender, EventArgs e)
        {
            this.Visible = false;   // 메인폼을 먼저 숨김

            login login = new login();

            VersionText.Text = string.Format("UniMarc Ver.{0}", ip.VersionInfo());

            if (DialogResult.OK == login.ShowDialog(this)) {
                this.Visible = true;
            }
            else {
                this.Close();
            }

            _DB.DBcon();
            string cmd = _DB.DB_Search("User_Data", "ID", User_Name);
            DB_User_Data = _DB.DB_Send_CMD_Search(cmd);
            try
            {
                string[] result = DB_User_Data.Split('|');

                if (result[3] == null) { }
                else
                {
                    toolStripLabel2.Text = result[4];
                    botUserLabel.Text = result[3];
                    User = result[3];
                }

                cmd = _DB.DB_Select_Search("`idx`", "Comp", "comp_name", result[4]);
                com_idx = _DB.DB_Send_CMD_Search(cmd).Replace("|", "");

                if (com_idx != "1")
                {
                    납품관리ToolStripMenuItem.Visible = false;
                    회계ToolStripMenuItem.Visible = false;
                }
                if (result[5] != "관리자") { 마스터ToolStripMenuItem.Visible = false; }

                Settings.Default.compidx = com_idx;
                Settings.Default.User = botUserLabel.Text;

                this.Text = "[ '" + com_idx + "' : " + result[4] + " - " + result[3] + "]";

                isAccess();
                SetBtnName();
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }


        #region 즐겨찾기 버튼 세팅
        public void SetBtnName()
        {
            string Area = 
                "`ShortCut1`, `ShortCut2`, `ShortCut3`, `ShortCut4`, `ShortCut5`, " +
                "`ShortCut6`, `ShortCut7`, `ShortCut8`, `ShortCut9`, `ShortCut10`, " +
                "`ShortCut11`, `ShortCut12`";

            string cmd = string.Format("SELECT {0} FROM `User_ShortCut` WHERE `id` = \"{1}\"", Area, User_Name);
            string res = _DB.DB_Send_CMD_Search(cmd);
            string[] ary_res = res.Split('|');

            Button[] btn = {
                ShortCut1, ShortCut2, ShortCut3, ShortCut4, ShortCut5,
                ShortCut6, ShortCut7, ShortCut8, ShortCut9, ShortCut10,
                ShortCut11, ShortCut12
            };
            for (int a = 0; a < btn.Length; a++)
            {
                if (ary_res[a] == "")
                    ary_res[a] = string.Format("미지정{0}", a + 1);

                btn[a].Text = ary_res[a];
                btn[a].BackgroundImage = SetPicture(ary_res[a]);
                btn[a].BackgroundImageLayout = ImageLayout.Stretch;

                if (!ary_res[a].Contains("미지정"))
                    btn[a].Enabled = true;
            }
        }

        private Image SetPicture(string Text)
        {
            Image[] source_Home = {
                Resources._1_2_사용자관리, Resources._1_3_납품처관리, Resources._1_4_주문처관리, Resources._1_5_도서정보관리

            };

            Image[] source_Marc = {
                Resources._3_1_1_마크작성, Resources._3_1_2_마크목록, Resources._3_1_3_소장자료검색, Resources._3_1_4_마크정리, Resources._3_1_5_복본조사, Resources._3_1_6_ISBN조회,
                Resources._3_2_1_목록, Resources._3_2_2_편목,
                Resources._3_2_1_반입, Resources._3_2_2_반출,
                Resources._3_3_1_전집관리, Resources._3_3_2_저자기호,
                Resources._3_4_1_DLS조회입력, Resources._3_4_2_DLS복본조사,
                Resources._3_5_1_마크통계
            };

            string[] Home = {
                "사용자 관리", "납품 / 거래처 관리", "주문처 관리", "도서정보 관리"
            };

            string[] Marc = {
                "마크 추가", "마크 목록", "소장자료검색", "마크 정리", "복본 조사", "ISBN 조회",
                "DVD/CD/LP 목록", "DVD/CD/LP 편목",
                "반입", "반출",
                "전집관리", "저자기호",
                "DLS 조회 입력", "DLS 복본 조사",
                "마크통계"
            };

            int count = 0;
            foreach (string BoxText in Home)
            {
                if (Text == BoxText)
                    return source_Home[count];

                count++;
            }

            count = 0;
            foreach (string BoxText in Marc)
            {
                if (Text == BoxText)
                    return source_Marc[count];

                count++;
            }
            return Resources._0_Empty;
        }

        private void Btn_ShortCut_Click(object sender, EventArgs e)
        {
            #region 변수선언
            Button btn = sender as Button;
            string btnText = btn.Text;

            string[] Div = {
                "물품등록", "목록조회", "목록집계", "주문입력", "입고작업",
                "재고입력 및 조회", "반품처리" };
            ToolStripMenuItem[] DivT = {
                목록등록, 목록조회, 목록집계, 주문입력, 입고작업,
                재고입력및조회, 반품처리
            };

            string[] Acc = {
                "송금내역조회", "송금등록", "매입 집계", "매입 장부", "매출 입력",
                "매출 입금", "매출 조회", "매출 집계", "파트타임 관리" };
            ToolStripMenuItem[] AccT = {
                송금내역조회, 송금등록, 매입집계, 매입장부, 매출입력, 
                매출입금, 매출조회, 매출집계, 파트타임관리
            };
            ToolStripMenuItem[] TempAcc = {
                송금내역조회, 송금등록, 매입, 매입, 매출,
                매출, 매출, 매출, 파트타임관리
            };

            string[] Marc = {
                "마크 추가", "마크 목록", "소장자료검색", "마크 정리", "ISBN 조회",
                "DVD/CD/LP 목록", "DVD/CD/LP 편목",
                "반입", "반출", "복본 조사", "DLS 복본 조사", "마크 수집", "전집관리",
                "검수", "저자기호", "DLS 조회 입력", "서류작성",
                "마크통계", "장비관리" };
            ToolStripMenuItem[] MarcT = {
                마크작성, 마크목록, 소장자료검색, 마크정리, iSBN조회, 목록, 편목, 마크반입,
                마크반출, 복본조사1, dLS복본조사, 마크수집, 전집관리,
                검수, 저자기호, DLS조회, 서류작성,
                마크통계, 장비관리
            };
            ToolStripMenuItem[] TempMarc = {
                마크작업, 마크작업, 마크작업, 마크작업, 마크작업, dVDCDLPToolStripMenuItem, dVDCDLPToolStripMenuItem, 반입및반출,
                반입및반출, 마크작업, DLS, 부가기능, 부가기능,
                부가기능, 부가기능, DLS, 마크기타,
                마크기타, 마크기타
            };


            string[] Home = {
                "사용자 관리", "납품 / 거래처 관리", "주문처 관리", "도서정보 관리" };
            ToolStripMenuItem[] HomeT = {
                사용자관리, 납품거래처관리, 주문처관리, 도서정보관리
            };

            string[][] MenuTotal = { Div, Acc, Marc, Home };
            ToolStripMenuItem[][] MenuTotalT = { DivT, AccT, MarcT, HomeT };
            ToolStripMenuItem[][] MenuCheckT = { DivT, TempAcc, TempMarc, HomeT };

            int[] count = { -1, -1 };
            #endregion

            for (int a = 0; a < MenuTotal.Length; a++)
            {
                bool IsText = false;
                for(int b = 0; b < MenuTotal[a].Length; b++)
                {
                    if (MenuTotal[a][b] == btnText) {
                        IsText = true;
                        count[1] = b;
                        break;
                    }
                }
                if (IsText) {
                    count[0] = a;
                    break;
                }
            }

            // 지정된게 없을경우 리턴
            foreach (int a in count)
                if (a == -1)
                    return;

            // 권한설정으로 인한 리턴
            if (!MenuCheckT[count[0]][count[1]].Enabled) {
                MessageBox.Show("권한이 설정되지 않았습니다!");
                return;
            }

            // 실행.
            MenuTotalT[count[0]][count[1]].PerformClick();
        }
        #endregion

        #region 권한별 잠금
        public void isAccess()
        {
            string Area =
                 "`Div_ListInput`, `Div_ListLookup`, `Div_ListTotal`, `Div_OrderInput`, `Div_Inven`,"
               + "`Div_Stock`, `Div_Return`,"

               + "`Acc_SendMoneyList`, `Acc_SendMoneyInput`, `Acc_Buy`, `Acc_Sales`, `Acc_PartTime`,"

               + "`Marc_Setup`, `Marc_Work`, `Marc_Input`, `Marc_Option`, `Marc_DLS`,"
               + " `Marc_ETC`,"

               + "`Manage_User`, `Manage_Client`, `Manage_Purchase`, `Manage_Book`";

            string cmd = string.Format("SELECT {0} FROM `User_Access` WHERE `id` = \"{1}\"", Area, User_Name);
            string res = _DB.DB_Send_CMD_Search(cmd);
            string[] ary_res = res.Split('|');

            ToolStripMenuItem[] menus =
            {
                목록등록, 목록조회, 목록집계, 주문입력, 입고작업,
                재고입력및조회, 반품처리,

                송금내역조회, 송금등록, 매입, 매출, 파트타임관리,

                마크설정, 마크작업, 반입및반출, 부가기능, DLS, 마크기타,

                사용자관리, 납품거래처관리, 주문처관리, 도서정보관리
            };

            for (int a = 0; a < ary_res.Length; a++)
            {
                if (ary_res[a] == "0")
                    menus[a].Enabled = false;
                else if (ary_res[a] == "1")
                    menus[a].Enabled = true;
            }

            ToolStripMenuItem[] Div = {
                목록등록, 목록조회, 목록집계, 주문입력, 입고작업,
                재고입력및조회, 반품처리
            };
            IsMenuAccess(Div, 납품관리ToolStripMenuItem);

            ToolStripMenuItem[] Acc = {
                송금내역조회, 송금등록, 매입, 매출, 파트타임관리
            };
            IsMenuAccess(Acc, 회계ToolStripMenuItem);

            ToolStripMenuItem[] Marc = {
                마크설정, 마크작업, 반입및반출, 부가기능, DLS, 마크기타
            };
            IsMenuAccess(Marc, 마크ToolStripMenuItem);

            ToolStripMenuItem[] Manage = {
                사용자관리, 납품거래처관리, 주문처관리, 도서정보관리
            };
            IsMenuAccess(Manage, 홈ToolStripMenuItem);
        }

        private void IsMenuAccess(ToolStripMenuItem[] Menu, ToolStripMenuItem Target)
        {
            bool IsEnabled = true;
            for (int a = 0; a < Menu.Length; a++)
            {
                if (Menu[a].Enabled)
                    IsEnabled = false;

                if (!IsEnabled)
                    break;
            }

            if (IsEnabled)
                Target.Enabled = false;

        }
        #endregion

        #region 홈 탭
        // 홈 탭 클릭이벤트
        private User_Infor User_Infor;
        private Home_User_manage home_User_Manage;
        private Transaction_manage Home_transaction_Manage;
        private Order_manage Home_order_Manage;
        private pw_change Home_pw_Change;
        private Book_infor_manage Home_book_Infor_Manage;
        private ledger_of_use Home_ledger_Of_Use;

        private void 사용자정보ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (User_Infor != null)
                User_Infor.Focus();

            else
            {
                User_Infor = new User_Infor(this);
                User_Infor.TopMost = true;
                User_Infor.FormClosed += (o, ea) => User_Infor = null;
                User_Infor.Show();
            }
            User_Infor.Show();
        }
        private void 사용자관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (home_User_Manage != null)
            {
                home_User_Manage.Focus();
            }
            else
            {
                home_User_Manage = new Home_User_manage(this);
                home_User_Manage.MdiParent = this;
                home_User_Manage.WindowState = FormWindowState.Maximized;
                home_User_Manage.FormClosed += (o, ea) => home_User_Manage = null;
                home_User_Manage.Show();
            }
        }
        private void 납품거래처관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Home_transaction_Manage != null)
            {
                Home_transaction_Manage.Focus();
            }
            else
            {
                Home_transaction_Manage = new Transaction_manage(this);
                Home_transaction_Manage.MdiParent = this;
                Home_transaction_Manage.WindowState = FormWindowState.Maximized;
                Home_transaction_Manage.FormClosed += (o, ea) => Home_transaction_Manage = null;
                Home_transaction_Manage.Show();
            }
        }
        private void 주문처관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Home_order_Manage != null)
            {
                Home_order_Manage.Focus();
            }
            else
            {
                Home_order_Manage = new Order_manage(this);
                Home_order_Manage.MdiParent = this;
                Home_order_Manage.WindowState = FormWindowState.Maximized;
                Home_order_Manage.FormClosed += (o, ea) => Home_order_Manage = null;
                Home_order_Manage.Show();
            }
        }

        private void 비밀번호변경ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Home_pw_Change != null)
            {
                Home_pw_Change.Focus();
            }
            else
            {
                Home_pw_Change = new pw_change(this);
                Home_pw_Change.MdiParent = this;
                Home_pw_Change.WindowState = FormWindowState.Maximized;
                Home_pw_Change.FormClosed += (o, ea) => Home_pw_Change = null;
                Home_pw_Change.Show();
            }
        }

        private void 도서정보관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Home_book_Infor_Manage != null)
            {
                Home_book_Infor_Manage.Focus();
            }
            else
            {
                Home_book_Infor_Manage = new Book_infor_manage();
                Home_book_Infor_Manage.FormClosed += (o, ea) => Home_book_Infor_Manage = null;
                Home_book_Infor_Manage.Show();
            }
            Home_book_Infor_Manage.Show();
        }

        private void 사용대장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Home_ledger_Of_Use != null)
            {
                Home_ledger_Of_Use.Focus();
            }
            else
            {
                Home_ledger_Of_Use = new ledger_of_use(this);
                Home_ledger_Of_Use.MdiParent = this;
                Home_ledger_Of_Use.WindowState = FormWindowState.Maximized;
                Home_ledger_Of_Use.FormClosed += (o, ea) => Home_ledger_Of_Use = null;
                Home_ledger_Of_Use.Show();
            }
        }
        #endregion

        #region 납품관리 탭
        // 납품관리 탭 클릭 이벤트
        Commodity_registration Del_commodity_Registration;
        List_Lookup del_list_lookup;
        List_aggregation del_list_Aggregation;
        Order_input del_order_Input;
        Purchase Del_purchase;
        Input_Lookup_Stock del_input_Lookup_Stock;
        Bring_Back del_bring_Back;

        private void 물품등록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Del_commodity_Registration != null)
            {
                Del_commodity_Registration.Focus();
            }
            else
            {
                Del_commodity_Registration = new Commodity_registration(this);
                Del_commodity_Registration.MdiParent = this;
                Del_commodity_Registration.WindowState = FormWindowState.Maximized;
                Del_commodity_Registration.FormClosed += (o, ea) => Del_commodity_Registration = null;
                Del_commodity_Registration.Show();
            }
        }

        private void 목록등록편의ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (del_list_lookup != null)
            {
                del_list_lookup.Focus();
            }
            else
            {
                del_list_lookup = new List_Lookup(this);
                del_list_lookup.MdiParent = this;
                del_list_lookup.WindowState = FormWindowState.Maximized;
                del_list_lookup.FormClosed += (o, ea) => del_list_lookup = null;
                del_list_lookup.Show();
            }
        }

        private void 목록집계ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (del_list_Aggregation != null)
            {
                del_list_Aggregation.Focus();
            }
            else
            {
                del_list_Aggregation = new List_aggregation(this);
                del_list_Aggregation.MdiParent = this;
                del_list_Aggregation.WindowState = FormWindowState.Maximized;
                del_list_Aggregation.FormClosed += (o, ea) => del_list_Aggregation = null;
                del_list_Aggregation.Show();
            }
        }

        private void 주문입력ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (del_order_Input != null)
            {
                del_order_Input.Focus();
            }
            else
            {
                del_order_Input = new Order_input(this);
                del_order_Input.MdiParent = this;
                del_order_Input.WindowState = FormWindowState.Maximized;
                del_order_Input.FormClosed += (o, ea) => del_order_Input = null;
                del_order_Input.Show();
            }
        }
        private void 매입ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Del_purchase != null)
            {
                Del_purchase.Focus();
            }
            else
            {
                Del_purchase = new Purchase(this);
                Del_purchase.MdiParent = this;
                Del_purchase.WindowState = FormWindowState.Maximized;
                Del_purchase.FormClosed += (o, ea) => Del_purchase = null;
                Del_purchase.Show();
            }
        }
        private void 재고입력및조회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (del_input_Lookup_Stock != null)
            {
                del_input_Lookup_Stock.Focus();
            }
            else
            {
                del_input_Lookup_Stock = new Input_Lookup_Stock(this);
                del_input_Lookup_Stock.MdiParent = this;
                del_input_Lookup_Stock.WindowState = FormWindowState.Maximized;
                del_input_Lookup_Stock.FormClosed += (o, ea) => del_input_Lookup_Stock = null;
                del_input_Lookup_Stock.Show();
            }
        }
        private void 반품처리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (del_bring_Back != null)
            {
                del_bring_Back.Focus();
            }
            else
            {
                del_bring_Back = new Bring_Back(this);
                del_bring_Back.MdiParent = this;
                del_bring_Back.WindowState = FormWindowState.Maximized;
                del_bring_Back.FormClosed += (o, ea) => del_bring_Back = null;
                del_bring_Back.Show();
            }
        }
        #endregion

        #region 회계 탭
        // 회계 탭 클릭 이벤트
        Remit_reg2 Acc_remit_Reg2;
        Purchase_Input Acc_purchase_Input;
        Remit_reg Acc_remit_Reg;
        Purchase_Book Acc_purchase_Book;
        Purchase_Aggregation Acc_purchase_Aggregation;
        Purchase_not_pay Acc_purchase_Not_Pay;
        Sales_Input Acc_sales_Input;
        Sales_In_Pay Acc_sales_In_Pay;
        Sales_Deposit Acc_sales_Deposit;
        Sales_Lookup Acc_sales_Book;
        Sales_Not_Pay Acc_sales_Not_Pay;
        Sales_Book Acc_sales_book;
        Bill_manage Acc_bill_Manage;
        Part_time Acc_part_time;


        private void 파트타임관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Acc_part_time != null)
            {
                Acc_part_time.Focus();
            }
            else
            {
                Acc_part_time = new Part_time(this);
                Acc_part_time.MdiParent = this;
                // Acc_part_time.WindowState = FormWindowState.Maximized;
                Acc_part_time.FormClosed += (o, ea) => Acc_part_time = null;
                Acc_part_time.Show();
            }
        }
        private void 송금등록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Acc_remit_Reg2 != null)
            {
                Acc_remit_Reg2.Focus();
            }
            else
            {
                Acc_remit_Reg2 = new Remit_reg2(this);
                // Acc_remit_Reg2.MdiParent = this;
                Acc_remit_Reg2.FormClosed += (o, ea) => Acc_remit_Reg2 = null;
                Acc_remit_Reg2.Show();
            }
        }
        private void 송금내역조회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Acc_remit_Reg != null)
            {
                Acc_remit_Reg.Focus();
            }
            else
            {
                Acc_remit_Reg = new Remit_reg(this);
                Acc_remit_Reg.MdiParent = this;
                Acc_remit_Reg.WindowState = FormWindowState.Maximized;
                Acc_remit_Reg.FormClosed += (o, ea) => Acc_remit_Reg = null;
                Acc_remit_Reg.Show();
            }
        }

        private void 매입장부ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Acc_purchase_Book != null)
            {
                Acc_purchase_Book.Focus();
            }
            else
            {
                Acc_purchase_Book = new Purchase_Book(this);
                Acc_purchase_Book.MdiParent = this;
                Acc_purchase_Book.WindowState = FormWindowState.Maximized;
                Acc_purchase_Book.FormClosed += (o, ea) => Acc_purchase_Book = null;
                Acc_purchase_Book.Show();
            }
        }

        private void 매입집계ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Acc_purchase_Aggregation != null)
            {
                Acc_purchase_Aggregation.Focus();
            }
            else
            {
                Acc_purchase_Aggregation = new Purchase_Aggregation(this);
                Acc_purchase_Aggregation.MdiParent = this;
                Acc_purchase_Aggregation.WindowState = FormWindowState.Maximized;
                Acc_purchase_Aggregation.FormClosed += (o, ea) => Acc_purchase_Aggregation = null;
                Acc_purchase_Aggregation.Show();
            }
        }
        
        private void 매입미결제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Acc_purchase_Not_Pay != null)
            {
                Acc_purchase_Not_Pay.Focus();
            }
            else
            {
                Acc_purchase_Not_Pay = new Purchase_not_pay(this);
                Acc_purchase_Not_Pay.MdiParent = this;
                Acc_purchase_Not_Pay.WindowState = FormWindowState.Maximized;
                Acc_purchase_Not_Pay.FormClosed += (o, ea) => Acc_purchase_Not_Pay = null;
                Acc_purchase_Not_Pay.Show();
            }
        }
        
        private void 매출입력ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Acc_sales_Input != null)
            {
                Acc_sales_Input.Focus();
            }
            else
            {
                Acc_sales_Input = new Sales_Input(this);
                Acc_sales_Input.MdiParent = this;
                Acc_sales_Input.WindowState = FormWindowState.Maximized;
                Acc_sales_Input.FormClosed += (o, ea) => Acc_sales_Input = null;
                Acc_sales_Input.Show();
            }
        }
        private void 매출입금toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            if (Acc_sales_In_Pay != null)
            {
                Acc_sales_In_Pay.Focus();
            }
            else
            {
                Acc_sales_In_Pay = new Sales_In_Pay(this);
                Acc_sales_In_Pay.FormClosed += (o, ea) => Acc_sales_In_Pay = null;
                Acc_sales_In_Pay.Show();
            }
        }
        private void 매출입금ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Acc_sales_Deposit != null)
            {
                Acc_sales_Deposit.Focus();
            }
            else
            {
                Acc_sales_Deposit = new Sales_Deposit(this);
                Acc_sales_Deposit.MdiParent = this;
                Acc_sales_Deposit.WindowState = FormWindowState.Maximized;
                Acc_sales_Deposit.FormClosed += (o, ea) => Acc_sales_Deposit = null;
                Acc_sales_Deposit.Show();
            }
        }

        private void 매출장부ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Acc_sales_Book != null)
            {
                Acc_sales_Book.Focus();
            }
            else
            {
                Acc_sales_Book = new Sales_Lookup(this);
                Acc_sales_Book.MdiParent = this;
                Acc_sales_Book.WindowState = FormWindowState.Maximized;
                Acc_sales_Book.FormClosed += (o, ea) => Acc_sales_Book = null;
                Acc_sales_Book.Show();
            }
        }

        private void 매출미수금ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Acc_sales_book != null)
            {
                Acc_sales_book.Focus();
            }
            else
            {
                Acc_sales_book = new Sales_Book(this);
                Acc_sales_book.MdiParent = this;
                Acc_sales_book.WindowState = FormWindowState.Maximized;
                Acc_sales_book.FormClosed += (o, ea) => Acc_sales_book = null;
                Acc_sales_book.Show();
            }
        }

        private void 매출입금ToolStripMenuItem_Click(object sender, EventArgs e)        // 계산서 관리
        {
            if (Acc_bill_Manage != null)
            {
                Acc_bill_Manage.Focus();
            }
            else
            {
                Acc_bill_Manage = new Bill_manage(this);
                Acc_bill_Manage.MdiParent = this;
                Acc_bill_Manage.WindowState = FormWindowState.Maximized;
                Acc_bill_Manage.FormClosed += (o, ea) => Acc_bill_Manage = null;
                Acc_bill_Manage.Show();
            }
        }
        #endregion

        #region 마크 탭

        #region 설정

        Setup_Shortcut Mac_shortcut;
        Set_Macro Mac_set_Macro;
        Nonverbal Mac_nonverbal;
        Job_Order Job_Order;

        private void 단축키설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_shortcut != null)
            {
                Mac_shortcut.Focus();
            }
            else
            {
                Mac_shortcut = new Setup_Shortcut(this);
                Mac_shortcut.MdiParent = this;
                Mac_shortcut.WindowState = FormWindowState.Maximized;
                Mac_shortcut.FormClosed += (o, ea) => Mac_shortcut = null;
                Mac_shortcut.Show();
            }
        }
        private void 매크로문구설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_set_Macro != null)
            {
                Mac_set_Macro.Focus();
            }
            else
            {
                Mac_set_Macro = new Set_Macro(this);
                Mac_set_Macro.MdiParent = this;
                Mac_set_Macro.WindowState = FormWindowState.Maximized;
                Mac_set_Macro.FormClosed += (o, ea) => Mac_set_Macro = null;
                Mac_set_Macro.Show();
            }
        }
        private void 불용어ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_nonverbal != null)
            {
                Mac_nonverbal.Focus();
            }
            else
            {
                Mac_nonverbal = new Nonverbal(this);
                Mac_nonverbal.MdiParent = this;
                Mac_nonverbal.WindowState = FormWindowState.Maximized;
                Mac_nonverbal.FormClosed += (o, ea) => Mac_nonverbal = null;
                Mac_nonverbal.Show();
            }
        }

        private void 작업지시서ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Job_Order != null)
            {
                Job_Order.Focus();
            }
            else
            {
                Job_Order = new Job_Order(this);
                Job_Order.MdiParent = this;
                Job_Order.WindowState = FormWindowState.Maximized;
                Job_Order.FormClosed += (o, ea) => Job_Order = null;
                Job_Order.Show();
            }
        }
        #endregion

        #region 마크 작업

        AddMarc AddMarc;
        Mac_List Mac_List;
        Marc_Plan Marc_Plan;
        Search_Infor Mac_search_Infor;
        Check_copy Mac_check_Copy;
        Check_ISBN Mac_check_ISBN;

        private void 마크작성ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (AddMarc != null)
                AddMarc.Focus();
            else
            {
                AddMarc = new AddMarc(this);
                AddMarc.MdiParent = this;
                AddMarc.WindowState = FormWindowState.Maximized;
                AddMarc.FormClosed += (o, ea) => AddMarc = null;
                AddMarc.Show();
            }

        }
        private void 마크목록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_List != null)
            {
                Mac_List.Focus();
            }
            else
            {
                Mac_List = new Mac_List(this);
                Mac_List.MdiParent = this;
                Mac_List.WindowState = FormWindowState.Maximized;
                Mac_List.FormClosed += (o, ea) => Mac_List = null;
                Mac_List.Show();
            }
        }
        private void 소장자료검색ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_search_Infor != null)
            {
                Mac_search_Infor.Focus();
            }
            else
            {
                Mac_search_Infor = new Search_Infor(this);
                Mac_search_Infor.MdiParent = this;
                Mac_search_Infor.WindowState = FormWindowState.Maximized;
                Mac_search_Infor.FormClosed += (o, ea) => Mac_search_Infor = null;
                Mac_search_Infor.Show();
            }
        }
        private void 마크정리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Marc_Plan != null)
            {
                Marc_Plan.Focus();
            }
            else
            {
                Marc_Plan = new Marc_Plan(this);
                Marc_Plan.MdiParent = this;
                Marc_Plan.WindowState = FormWindowState.Maximized;
                Marc_Plan.FormClosed += (o, ea) => Marc_Plan = null;
                Marc_Plan.Show();
            }
        }
        private void 복본조사ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //if (Mac_check_Copy != null)
            //{
            //    Mac_check_Copy.Focus();
            //}
            //else
            //{
            //    Mac_check_Copy = new Check_copy(this);
            //    Mac_check_Copy.MdiParent = this;
            //    Mac_check_Copy.WindowState = FormWindowState.Maximized;
            //    Mac_check_Copy.FormClosed += (o, ea) => Mac_check_Copy = null;
            //    Mac_check_Copy.Show();
            //}

            Mac_check_Copy = new Check_copy(this);
            Mac_check_Copy.Show();
        }

        private void iSBN조회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_check_ISBN != null)
            {
                Mac_check_ISBN.Focus();
            }
            else
            {
                Mac_check_ISBN = new Check_ISBN(this);
                Mac_check_ISBN.MdiParent = this;
                Mac_check_ISBN.WindowState = FormWindowState.Maximized;
                Mac_check_ISBN.FormClosed += (o, ea) => Mac_check_ISBN = null;
                Mac_check_ISBN.tb_list_name.Enabled = true;
                Mac_check_ISBN.Show();
            }
        }

        #endregion

        #region DVD / CD / LP

        CD_LP cl;
        CD_LP_List cll;

        private void 편목ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cl != null)
            {
                cl.Focus();
            }
            else
            {
                cl = new CD_LP(this);
                cl.MdiParent = this;
                cl.WindowState = FormWindowState.Maximized;
                cl.FormClosed += (o, ea) => cl = null;
                cl.Show();
            }
        }

        private void 목록_Click(object sender, EventArgs e)
        {
            if (cll != null)
            {
                cll.Focus();
            }
            else
            {
                cll = new CD_LP_List(this);
                cll.MdiParent = this;
                cll.WindowState = FormWindowState.Maximized;
                cll.FormClosed += (o, ea) => cll = null;
                cll.Show();
            }

        }
        #endregion

        #region 반입 및 반출

        Mac_Input Mac_Input;
        Mac_Output Mac_Output;

        private void 반입ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_Input != null)
            {
                Mac_Input.Focus();
            }
            else
            {
                Mac_Input = new Mac_Input(this);
                Mac_Input.MdiParent = this;
                Mac_Input.WindowState = FormWindowState.Maximized;
                Mac_Input.FormClosed += (o, ea) => Mac_Input = null;
                Mac_Input.Show();
            }
        }
        private void 반출ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_Output != null)
            {
                Mac_Output.Focus();
            }
            else
            {
                Mac_Output = new Mac_Output(this);
                Mac_Output.MdiParent = this;
                Mac_Output.WindowState = FormWindowState.Maximized;
                Mac_Output.FormClosed += (o, ea) => Mac_Output = null;
                Mac_Output.Show();
            }
        }

        #endregion

        #region 부가기능

        Collect_Mac Mac_Collect;
        All_Book_manage Mac_all_Book_Manage;
        Mac_Chack_Up Mac_Chack_Up;
        Symbol_Add Mac_symbol_Add;

        private void 마크수집ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_Collect != null)
            {
                Mac_Collect.Focus();
            }
            else
            {
                Mac_Collect = new Collect_Mac(this);
                Mac_Collect.MdiParent = this;
                Mac_Collect.WindowState = FormWindowState.Maximized;
                Mac_Collect.FormClosed += (o, ea) => Mac_Collect = null;
                Mac_Collect.Show();
            }
        }
        private void 전집관리ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Mac_all_Book_Manage != null)
            {
                Mac_all_Book_Manage.Focus();
            }
            else
            {
                Mac_all_Book_Manage = new All_Book_manage(this);
                Mac_all_Book_Manage.MdiParent = this;
                Mac_all_Book_Manage.WindowState = FormWindowState.Maximized;
                Mac_all_Book_Manage.FormClosed += (o, ea) => Mac_all_Book_Manage = null;
                Mac_all_Book_Manage.Show();
            }
        }
        private void 검수ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_Chack_Up != null)
            {
                Mac_Chack_Up.Focus();
            }
            else
            {
                Mac_Chack_Up = new Mac_Chack_Up(this);
                Mac_Chack_Up.MdiParent = this;
                Mac_Chack_Up.WindowState = FormWindowState.Maximized;
                Mac_Chack_Up.FormClosed += (o, ea) => Mac_Chack_Up = null;
                Mac_Chack_Up.Show();
            }
        }
        private void 저자기호ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_symbol_Add != null)
            {
                Mac_symbol_Add.Focus();
            }
            else
            {
                Mac_symbol_Add = new Symbol_Add(this);
                Mac_symbol_Add.MdiParent = this;
                Mac_symbol_Add.WindowState = FormWindowState.Maximized;
                Mac_symbol_Add.FormClosed += (o, ea) => Mac_symbol_Add = null;
                Mac_symbol_Add.Show();
            }
        }


        #endregion

        #region DLS

        School_Lookup DLS_school_Lookup;
        DLS_Copy Mac_dLS_Copy;

        private void dLS조회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DLS_school_Lookup != null)
            {
                DLS_school_Lookup.Focus();
            }
            else
            {
                DLS_school_Lookup = new School_Lookup(this);
                DLS_school_Lookup.MdiParent = this;
                DLS_school_Lookup.WindowState = FormWindowState.Maximized;
                DLS_school_Lookup.FormClosed += (o, ea) => DLS_school_Lookup = null;
                DLS_school_Lookup.Show();
            }
        }
        private void dLS복본조사ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_dLS_Copy != null)
            {
                Mac_dLS_Copy.Focus();
            }
            else
            {
                Mac_dLS_Copy = new DLS_Copy(this);
                Mac_dLS_Copy.MdiParent = this;
                Mac_dLS_Copy.WindowState = FormWindowState.Maximized;
                Mac_dLS_Copy.FormClosed += (o, ea) => Mac_dLS_Copy = null;
                Mac_dLS_Copy.Show();
            }
        }
        #endregion

        #region 기타

        Make_Document Make_Document;
        Mac_Stat Mac_Stat;
        Equip_manage Mac_equip_Manage;

        private void 서류작성ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Make_Document != null)
            {
                Make_Document.Focus();
            }
            else
            {
                Make_Document = new Make_Document(this);
                Make_Document.MdiParent = this;
                Make_Document.WindowState = FormWindowState.Maximized;
                Make_Document.FormClosed += (o, ea) => Make_Document = null;
                Make_Document.Show();
            }
        }
        private void 마크통계ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Mac_Stat != null)
            {
                Mac_Stat.Focus();
            }
            else
            {
                Mac_Stat = new Mac_Stat(this);
                Mac_Stat.MdiParent = this;
                Mac_Stat.WindowState = FormWindowState.Maximized;
                Mac_Stat.FormClosed += (o, ea) => Mac_Stat = null;
                Mac_Stat.Show();
            }
        }
        private void 장비관리ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Mac_equip_Manage != null)
            {
                Mac_equip_Manage.Focus();
            }
            else
            {
                Mac_equip_Manage = new Equip_manage(this);
                Mac_equip_Manage.MdiParent = this;
                Mac_equip_Manage.WindowState = FormWindowState.Maximized;
                Mac_equip_Manage.FormClosed += (o, ea) => Mac_equip_Manage = null;
                Mac_equip_Manage.Show();
            }
        }

        #endregion

        #endregion

        #region 작업일지 탭

        //  작업일지 탭 이벤트
        Work_Log work_Log;

        private void 작업일지ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (work_Log != null)
            {
                work_Log.Focus();
            }
            else
            {
                work_Log = new Work_Log(this);
                work_Log.MdiParent = this;
                work_Log.WindowState = FormWindowState.Maximized;
                work_Log.FormClosed += (o, ea) => work_Log = null;
                work_Log.Show();
            }
        }
        #endregion

        #region 편의기능 탭
        //  편의기능 탭 이벤트
        Calendar calendar;
        Talk talk;
        Quick_menu quick_Menu;
        Board board;
        Send_Notice send_Notice;
        Sale_Sale sale_Sale;
        Sale_Settlement sale_Settlement;
        Sale_End sale_End;
        Sale_Member_Manage sale_Member_Manage;

        private void 캘린더ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (calendar != null)
            {
                calendar.Focus();
            }
            else
            {
                calendar = new Calendar(this);
                calendar.MdiParent = this;
                calendar.WindowState = FormWindowState.Maximized;
                calendar.FormClosed += (o, ea) => calendar = null;
                calendar.Show();
            }
        }
        private void 채팅ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (talk != null)
            {
                talk.Focus();
            }
            else
            {
                talk = new Talk(this);
                talk.MdiParent = this;
                talk.WindowState = FormWindowState.Maximized;
                talk.FormClosed += (o, ea) => talk = null;
                talk.Show();
            }
        }
        private void 퀵메뉴ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (quick_Menu != null)
            {
                quick_Menu.Focus();
            }
            else
            {
                quick_Menu = new Quick_menu(this);
                quick_Menu.MdiParent = this;
                quick_Menu.WindowState = FormWindowState.Maximized;
                quick_Menu.FormClosed += (o, ea) => quick_Menu = null;
                quick_Menu.Show();
            }
        }
        private void 게시판ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (board != null)
            {
                board.Focus();
            }
            else
            {
                board = new Board(this);
                board.MdiParent = this;
                board.WindowState = FormWindowState.Maximized;
                board.FormClosed += (o, ea) => board = null;
                board.Show();
            }
        }
        private void 공지발송ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (send_Notice != null)
            {
                send_Notice.Focus();
            }
            else
            {
                send_Notice = new Send_Notice(this);
                send_Notice.MdiParent = this;
                send_Notice.WindowState = FormWindowState.Maximized;
                send_Notice.FormClosed += (o, ea) => send_Notice = null;
                send_Notice.Show();
            }
        }
        private void 판매ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (sale_Sale != null)
            {
                sale_Sale.Focus();
            }
            else
            {
                sale_Sale = new Sale_Sale(this);
                sale_Sale.MdiParent = this;
                sale_Sale.WindowState = FormWindowState.Maximized;
                sale_Sale.FormClosed += (o, ea) => sale_Sale = null;
                sale_Sale.Show();
            }
        }
        private void 정산ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sale_Settlement != null)
            {
                sale_Settlement.Focus();
            }
            else
            {
                sale_Settlement = new Sale_Settlement(this);
                sale_Settlement.MdiParent = this;
                sale_Settlement.WindowState = FormWindowState.Maximized;
                sale_Settlement.FormClosed += (o, ea) => sale_Settlement = null;
                sale_Settlement.Show();
            }
        }
        private void 판매마감ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sale_End != null)
            {
                sale_End.Focus();
            }
            else
            {
                sale_End = new Sale_End(this);
                sale_End.MdiParent = this;
                sale_End.WindowState = FormWindowState.Maximized;
                sale_End.FormClosed += (o, ea) => sale_End = null;
                sale_End.Show();
            }
        }
        private void 회원관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sale_Member_Manage != null)
            {
                sale_Member_Manage.Focus();
            }
            else
            {
                sale_Member_Manage = new Sale_Member_Manage(this);
                sale_Member_Manage.MdiParent = this;
                sale_Member_Manage.WindowState = FormWindowState.Maximized;
                sale_Member_Manage.FormClosed += (o, ea) => sale_Member_Manage = null;
                sale_Member_Manage.Show();
            }
        }
        #endregion

        #region 마스터 탭
        //  마스터 탭 이벤트
        User_manage master_user_Manage;
        Notice_Send master_notice_Send;
        Sales_Details master_sales_Details;
        User_account_inquiry master_user_Account_Inquiry;
        Mac_Setting master_mac_setting;
        Batch_processing master_batch_Processing;

        private void 신규사업자등록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (master_user_Manage != null)
            {
                master_user_Manage.Focus();
            }
            else
            {
                master_user_Manage = new User_manage(this);
                master_user_Manage.FormClosed += (o, ea) => master_user_Manage = null;
                master_user_Manage.Show();
            }
        }

        private void 기존사업자관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 공지발송ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (master_notice_Send != null)
            {
                master_notice_Send.Focus();
            }
            else
            {
                master_notice_Send = new Notice_Send(this);
                master_notice_Send.MdiParent = this;
                master_notice_Send.WindowState = FormWindowState.Maximized;
                master_notice_Send.FormClosed += (o, ea) => master_notice_Send = null;
                master_notice_Send.Show();
            }
        }
        private void 매출내역ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (master_sales_Details != null)
            {
                master_sales_Details.Focus();
            }
            else
            {
                master_sales_Details = new Sales_Details(this);
                master_sales_Details.MdiParent = this;
                master_sales_Details.WindowState = FormWindowState.Maximized;
                master_sales_Details.FormClosed += (o, ea) => master_sales_Details = null;
                master_sales_Details.Show();
            }
        }
        private void 이용자거래처조회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (master_user_Account_Inquiry != null)
            {
                master_user_Account_Inquiry.Focus();
            }
            else
            {
                master_user_Account_Inquiry = new User_account_inquiry(this);
                master_user_Account_Inquiry.MdiParent = this;
                master_user_Account_Inquiry.WindowState = FormWindowState.Maximized;
                master_user_Account_Inquiry.FormClosed += (o, ea) => master_user_Account_Inquiry = null;
                master_user_Account_Inquiry.Show();
            }
        }
        private void 마크설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (master_mac_setting != null)
            {
                master_mac_setting.Focus();
            }
            else
            {
                master_mac_setting = new Mac_Setting(this);
                master_mac_setting.MdiParent = this;
                master_mac_setting.WindowState = FormWindowState.Maximized;
                master_mac_setting.FormClosed += (o, ea) => master_mac_setting = null;
                master_mac_setting.Show();
            }
        }
        private void 일괄처리관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (master_batch_Processing != null)
            {
                master_batch_Processing.Focus();
            }
            else
            {
                master_batch_Processing = new Batch_processing(this);
                master_batch_Processing.MdiParent = this;
                master_batch_Processing.WindowState = FormWindowState.Maximized;
                master_batch_Processing.FormClosed += (o, ea) => master_batch_Processing = null;
                master_batch_Processing.Show();
            }
        }
        #endregion
    }
}