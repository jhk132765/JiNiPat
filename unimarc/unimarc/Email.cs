﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mail;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class Email
    {
        Helper_DB db = new Helper_DB();
        public bool cross_mail(string compidx, string filePath, string sender)
        {
            #region Setup

            db.DBcon();

            // 보내는 이 : 메일ID, 메일PW, smtp주소, 포트번호
            string sender_Area = "`comp_name`, `email_ID`, `email_PW`, `smtp`, `port`";
            string cmd = db.DB_Select_Search(sender_Area, "Comp", "idx", compidx);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] arr_db = db_res.Split('|');

            bool res = false;
            bool daum = false;

            if (arr_db[1].Contains("@daum.net") || arr_db[1].Contains("@hanmail.net"))
            {
                daum = true;
            }

            #endregion

            if (daum)
            {
                res = Daum_Send_Mail(arr_db, filePath, sender);
            }
            else { res = Send_mail(arr_db, filePath, sender); }

            if (res)
            {
                Resend_Mail(arr_db, filePath, sender, daum);
            }

            return res;
        }

        bool Daum_Send_Mail(string[] arr_db, string filePath, string sender)
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.From = arr_db[1];
            msg.To = sender;
            msg.Subject = string.Format("{0} 주문분입니다.", arr_db[0]);
            msg.Body = string.Format("{0} 주문분입니다.", arr_db[0]);
            msg.Attachments.Add(new MailAttachment(filePath));

            int cdoBasic = 1;
            string smtp_server = arr_db[3];
            int port = Convert.ToInt32(arr_db[4]);

            string field = "http://schemas.microsoft.com/cdo/configuration/";
            msg.Fields.Add(field + "smtpusessl", true);
            msg.Fields.Add(field + "smtpserverport", port);
            msg.Fields.Add(field + "smtpauthenticate", cdoBasic);
            msg.Fields.Add(field + "sendusername", arr_db[1]);
            msg.Fields.Add(field + "sendpassword", arr_db[2]);

            try
            {
                SmtpMail.SmtpServer = smtp_server;
                SmtpMail.Send(msg);
                MessageBox.Show("다음 메일 전송 성공");
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }
        }

        bool Send_mail(string[] arr_db, string filePath, string sender)
        {

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

            // 보내는 사람 이메일
            mail.From = new MailAddress(arr_db[1]);

            // 받는 사람 이메일
            mail.To.Add(sender);

            // 메일 제목
            mail.Subject = string.Format("{0} 주문분입니다.", arr_db[0]);

            // 메일 내용
            mail.Body = string.Format("{0} 주문분입니다.", arr_db[0]);

            // 첨부파일 붙이기
            Attachment attachment = new Attachment(filePath);
            mail.Attachments.Add(attachment);

            string smtp_server = arr_db[3];
            int port = Convert.ToInt32(arr_db[4]);

            // SMTP 및 포트 설정
            SmtpClient smtp = new SmtpClient(smtp_server, port);
            smtp.EnableSsl = true;

            // 계정 설정
            smtp.Credentials = new NetworkCredential(arr_db[1], arr_db[2]);

            try
            {
                smtp.Send(mail);
                MessageBox.Show("메일 전송 완료");
                return true;
            }
            catch (SmtpException e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }
        }
        void Resend_Mail(string[] arr_db, string filePath, string sender, bool daum)
        {
            if (daum)
            {
                System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
                msg.From = arr_db[1];
                msg.To = arr_db[1];
                msg.Subject = string.Format("(확인메일) {0} 주문분입니다.",arr_db[0]);
                msg.Body = string.Format("(확인메일) {0} 주문분입니다.", arr_db[0]);
                msg.Attachments.Add(new MailAttachment(filePath));

                int cdoBasic = 1;
                string smtp_server = arr_db[3];
                int port = Convert.ToInt32(arr_db[4]);

                string field = "http://schemas.microsoft.com/cdo/configuration/";
                msg.Fields.Add(field + "smtpusessl", true);
                msg.Fields.Add(field + "smtpserverport", port);
                msg.Fields.Add(field + "smtpauthenticate", cdoBasic);
                msg.Fields.Add(field + "sendusername", arr_db[1]);
                msg.Fields.Add(field + "sendpassword", arr_db[2]);

                try
                {
                    SmtpMail.SmtpServer = smtp_server;
                    SmtpMail.Send(msg);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
            else
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.From = new MailAddress(arr_db[1]);
                mail.To.Add(arr_db[1]);
                mail.Subject = string.Format("(확인메일) {0} 주문분입니다.", arr_db[0]);
                mail.Body = string.Format("(확인메일) {0} 주문분입니다.", arr_db[0]);

                // 첨부파일 붙이기
                Attachment attachment = new Attachment(filePath);
                mail.Attachments.Add(attachment);

                string smtp_server = arr_db[3];
                int port = Convert.ToInt32(arr_db[4]);

                // SMTP 및 포트 설정
                SmtpClient smtp = new SmtpClient(smtp_server, port);
                smtp.EnableSsl = true;

                // 계정 설정
                smtp.Credentials = new NetworkCredential(arr_db[1], arr_db[2]);

                try
                {
                    smtp.Send(mail);
                }
                catch (SmtpException e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
        }
    }
}
