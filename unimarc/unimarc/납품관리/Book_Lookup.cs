﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Mac;
using WindowsFormsApp1.납품관리;

namespace WindowsFormsApp1.Delivery
{
    public partial class Book_Lookup : Form
    {
        Helper_DB db = new Helper_DB();
        Order_input oin;
        Purchase pur;
        List_Lookup ll;
        Bring_Back bb;
        Check_ISBN cisbn;
        string compidx;
        string list_name;
        int idx;
        public Book_Lookup(Order_input _oin)
        {
            InitializeComponent();
            oin = _oin;
            idx = oin.grididx;
            compidx = oin.compidx;
        }
        public Book_Lookup(Purchase _pur)
        {
            InitializeComponent();
            pur = _pur;
            idx = pur.grididx;
            compidx = pur.compidx;
        }
        public Book_Lookup(List_Lookup _ll)
        {
            InitializeComponent();
            ll = _ll;
            idx = ll.grididx;
            compidx = ll.compidx;
        }
        public Book_Lookup(Bring_Back _bb)
        {
            InitializeComponent();
            bb = _bb;
            idx = bb.grididx;
            compidx = bb.compidx;
        }
        public Book_Lookup(Check_ISBN _isbn)
        {
            InitializeComponent();
            cisbn = _isbn;
            idx = cisbn.rowidx;
            compidx = cisbn.compidx;
        }
        private void Book_Lookup_Load(object sender, EventArgs e)
        {
            db.DBcon();
            tb_isbn.Text = isbn();
            list_db();
            this.Text = "도서 정보 - 『" + tb_book_name.Text + "』";
            mk_Grid();

            /* Obj_List_Book 
             * idx 도서명 저자 출판사 isbn 
             * 정가 수량 입고수 합계금액 비고 
             * 주문처 주문일자 */
            string[] List_book = { compidx, tb_book_name.Text, tb_author.Text, tb_book_comp.Text, tb_isbn.Text,
                tb_pay.Text, tb_count.Text, tb_stock.Text, tb_total.Text, tb_etc.Text,
                tb_order1.Text, tb_order_date.Text, tb_List_name.Text };
        }
        private void mk_Grid()
        {
            string Area = "`order`, `pay`, `count`, `persent`, " +                      // 0-3
                          "`order_date`, `import_date`, `chk_date`, `export_date`";     // 4-7
            string[] Search_col = { "compidx", "list_name", "book_name", "author", "book_comp" };
            string[] Search_data = { compidx, tb_List_name.Text, tb_book_name.Text, tb_author.Text, tb_book_comp.Text };
            string cmd = db.More_DB_Search("Obj_List_Book", Search_col, Search_data, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] db_data = db_res.Split('|');

            string[] add_grid = { "", "", db_data[0], db_data[1], db_data[2], db_data[3] };
            // 처리일자 구분 거래처명 단가 수량 %
            if (db_data[4] != "") {
                add_grid[0] = db_data[4];
                add_grid[1] = "주문";
                dataGridView1.Rows.Add(add_grid);
            }
            if (db_data[5] != "") {
                add_grid[0] = db_data[5];
                add_grid[1] = "입고";
                btn_printLine.Visible = true;
                dataGridView1.Rows.Add(add_grid);
            }
            if (db_data[6] != "") {
                add_grid[0] = db_data[6];
                add_grid[1] = "검수";
                dataGridView1.Rows.Add(add_grid);
            }
            if (db_data[7] != "") {
                add_grid[0] = db_data[7];
                add_grid[1] = "출고";
                dataGridView1.Rows.Add(add_grid);
            }
        }
        /// <summary>
        /// 폼에 들어갈 데이터를 사전에 준비하여야함.
        /// </summary>
        /// <param name="book_name"></param>
        /// <param name="author"></param>
        /// <param name="book_comp"></param>
        /// <param name="list_name"></param>
        public void Lookup_Load(string book_name, string author, string book_comp, string list_name)
        {
            this.list_name = list_name;
            db.DBcon();
            string search = "`book_name`, `author`, `book_comp`, `count`, `pay`, " +
                            "`total`, `header`, `num`, `order`, `etc`, " +
                            "`input_count`, `order_date`, `list_name`, `idx`";

            string[] data = { compidx, book_name, author, book_comp, list_name };
            string[] table = { "compidx", "book_name", "author", "book_comp", "list_name" };
            string cmd = db.More_DB_Search("Obj_List_Book", table, data, search);
            string db_res = db.DB_Send_CMD_Search(cmd);
            mk_Lookup(db_res);
        }
        public void Lookup_Load_tmp(string idx)
        {
            db.DBcon();
            string search = "`book_name`, `author`, `book_comp`, `count`, `pay`, " +
                            "`total`, `header`, `num`, `order`, `etc`, " +
                            "`input_count`, `order_date`, `list_name`, `idx`";

            string[] data = { compidx, idx };
            string[] table = { "compidx", "idx" };
            string cmd = db.More_DB_Search("Obj_List_Book", table, data, search);
            string db_res = db.DB_Send_CMD_Search(cmd);
            mk_Lookup(db_res);
        }
        private void mk_Lookup(string values)
        {
            string[] data = values.Split('|');

            tb_book_name.Text = data[0];
            tb_author.Text = data[1];
            tb_book_comp.Text = data[2];
            tb_count.Text = data[3];
            tb_pay.Text = data[4];
            tb_total.Text = data[5];
            tb_num.Text = data[6] + " " + data[7];
            tb_order1.Text = data[8];
            tb_etc.Text = data[9];
            tb_stock.Text = data[10];
            if (data[11].Length < 3) { tb_order_date.Text = ""; }
            else { tb_order_date.Text = data[11].Substring(0, 10); }
            this.list_name = data[12];
            lbl_idx.Text = data[13];
        }
        /// <summary>
        /// 목록db에서 불러온 값을 적용시키는 함수
        /// </summary>
        private void list_db()
        {
            string[] where_table = { "comp_num", "list_name" };
            string[] search_data = { compidx, list_name };
            string cmd = db.More_DB_Search("Obj_List", where_table, search_data, 
                "`clt`, `dly`, `charge`, `date`, `date_res`");
            cmd = db.DB_Send_CMD_Search(cmd);
            string[] data = cmd.Split('|');
            tb_List_name.Text = list_name;
            tb_charge.Text = data[2];
            tb_date.Text = data[3];
            tb_date_res.Text = data[4];
        }
        /// <summary>
        /// 목록도서DB에 저장된 isbn값을 가져오는 함수.
        /// </summary>
        /// <returns></returns>
        private string isbn()
        {
            string[] where_table = { "compidx", "list_name", "book_name", "author", "book_comp" };
            string[] search_data = { compidx, 
                list_name, tb_book_name.Text, tb_author.Text, tb_book_comp.Text };
            string cmd = db.More_DB_Search("Obj_List_Book", where_table, search_data, "`isbn`");
            string db_res = db.DB_Send_CMD_Search(cmd);
            db_res = db_res.Replace("|", "");
            return db_res;
        }
        private void Book_Lookup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btn_close_Click(null, null);
            }
        }
        private void btn_save_Click(object sender, EventArgs e)
        {
            /* Obj_List_Book 
             * idx 도서명 저자 출판사 isbn 
             * 정가 수량 입고수 합계금액 비고 
             * 주문처 주문일자 */
            string[] Table = { 
                "compidx", "book_name", "author", "book_comp", "isbn", 
                "pay", "count", "input_count", "total", "etc", 
                "order", "order_date", "list_name" };
            string[] List_book = { 
                compidx, tb_book_name.Text, tb_author.Text, tb_book_comp.Text, tb_isbn.Text, 
                tb_pay.Text, tb_count.Text, tb_stock.Text, tb_total.Text, tb_etc.Text, 
                tb_order1.Text, tb_order_date.Text, tb_List_name.Text };
            string[] idx_table = { "idx" };
            string[] idx_col = { lbl_idx.Text };

            string U_cmd = db.More_Update("Obj_List_Book", Table, List_book, idx_table, idx_col);
            db.DB_Send_CMD_reVoid(U_cmd);
            MessageBox.Show("저장되었습니다.");
        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btn_stock_Click(object sender, EventArgs e)
        {
            tb_stock.Text = tb_count.Text;

            string[] edit_tbl = { "input_count", "import", "import_date" };
            string[] edit_col = { tb_stock.Text, "입고", DateTime.Today.ToString("yyyy-MM-dd") };
            string[] search_tbl = { "compidx", "list_name", "book_name", "author", "book_comp",  "isbn" };
            string[] search_col = { compidx, 
                        tb_List_name.Text, tb_book_name.Text, tb_author.Text, tb_book_comp.Text, tb_isbn.Text };
            string U_cmd = db.More_Update("Obj_List_Book", edit_tbl, edit_col, search_tbl, search_col);
            db.DB_Send_CMD_reVoid(U_cmd);
            MessageBox.Show(tb_book_name.Text + "가 입고처리되었습니다.");
            mk_Grid();
        }

        private void btn_unstock_Click(object sender, EventArgs e)
        {
            tb_stock.Text = "0";

            string[] edit_tbl = { "input_count", "import", "import_date" };
            string[] edit_col = { tb_stock.Text, "미입고", "" };
            string[] search_tbl = { "compidx", 
                "list_name", "book_name", "author", "book_comp", "isbn" };
            string[] search_col = { compidx,
                tb_List_name.Text, tb_book_name.Text, tb_author.Text, tb_book_comp.Text, tb_isbn.Text };
            string U_cmd = db.More_Update("Obj_List_Book", edit_tbl, edit_col, search_tbl, search_col);
            db.DB_Send_CMD_reVoid(U_cmd);

            MessageBox.Show(tb_book_name.Text + "가 미입고처리되었습니다.");
            mk_Grid();
        }
        private void btn_order_ccl_Click(object sender, EventArgs e)
        {
            tb_order1.Text = "";
            tb_order_date.Text = "";
            tb_order_idx.Text = "";
        }
        private void btn_order_Click(object sender, EventArgs e)
        {
            tb_order_date.Text = DateTime.Now.ToString("G").Substring(0, 10);
        }

        private void btn_printLine_Click(object sender, EventArgs e)
        {
            PrintDocument pd = new PrintDocument();
            pd.DocumentName = "띠지 출력";
            pd.DefaultPageSettings.Landscape = true;
            pd.PrintPage += new PrintPageEventHandler(this.pd_Print_Page);
            pd.Print();
        }
        #region PrintLine_Sub
        private void pd_Print_Page(object sender, PrintPageEventArgs e)
        {
            string list_name = tb_List_name.Text.Replace("[", "");
            list_name = list_name.Replace("]", "-");

            string book_name = tb_book_name.Text;
            string price = tb_pay.Text.Replace(",", "");
            price = string.Format("{0:#,###}", Convert.ToInt32(price));
            tb_pay.Text = price;
            string[] text = { tb_num.Text, tb_count.Text, list_name, book_name, price, tb_book_comp.Text, tb_isbn.Text };

            Graphics g = e.Graphics;

            int xPos = 0;
            int yPos = 0;

            int MaxX = 133;

            Pen p = new Pen(Color.Black);

            Font title = new Font("굴림", 22, FontStyle.Bold);
            Font count = new Font("굴림", 16, FontStyle.Bold);
            Font list = new Font("굴림", 14, FontStyle.Regular);
            Font book = new Font("굴림", 18, FontStyle.Regular);
            Font price_f = new Font("굴림", 14, FontStyle.Regular);

            RectangleF title_Area = new RectangleF(xPos, yPos, MaxX, 35);
            yPos += 30;
            RectangleF count_Area = new RectangleF(xPos, yPos, MaxX, 35);
            yPos += 25;
            RectangleF list_Area = new RectangleF(xPos, yPos, MaxX, 70);
            yPos += 65;
            RectangleF book_Area1 = new RectangleF(xPos, yPos, MaxX, 120);
            yPos += 115;
            RectangleF price_Area = new RectangleF(xPos, yPos, MaxX, 70);

            //g.DrawLine(p, MaxX, 0, MaxX, yPos);

            using (SolidBrush drawBrush = new SolidBrush(Color.Black))
            {
                g.DrawString(text[0], title, drawBrush, title_Area);
                g.DrawString(text[1], count, drawBrush, count_Area);
                g.DrawString(text[2], list, drawBrush, list_Area);
                g.DrawString(text[3], book, drawBrush, book_Area1);
                g.DrawString(text[4], price_f, drawBrush, price_Area);
            }
        }
        #endregion
    }
}
