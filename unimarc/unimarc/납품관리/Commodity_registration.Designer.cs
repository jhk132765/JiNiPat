﻿namespace WindowsFormsApp1.Delivery
{
    partial class Commodity_registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_FilePath = new System.Windows.Forms.TextBox();
            this.btn_FileOpen = new System.Windows.Forms.Button();
            this.start_date = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_dvy1 = new System.Windows.Forms.TextBox();
            this.tb_clt1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_UserName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_ListNum = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_dvy_method = new System.Windows.Forms.TextBox();
            this.tb_Work_method = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.end_date = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.header = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chk_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.list_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dly = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.list_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cout = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stat2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid_btn_ISBN = new System.Windows.Forms.DataGridViewButtonColumn();
            this.chk_marc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grid_Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btn_morge = new System.Windows.Forms.Button();
            this.btn_Edit = new System.Windows.Forms.Button();
            this.btn_Del = new System.Windows.Forms.Button();
            this.btn_Complet = new System.Windows.Forms.Button();
            this.btn_ing = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.btn_lockup = new System.Windows.Forms.Button();
            this.tb_dvy2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_clt2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_user2 = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.rbtn_marc = new System.Windows.Forms.RadioButton();
            this.rbtn_div = new System.Windows.Forms.RadioButton();
            this.rbtn_all = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.btn_total = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "읽기";
            // 
            // tb_FilePath
            // 
            this.tb_FilePath.Location = new System.Drawing.Point(49, 13);
            this.tb_FilePath.Name = "tb_FilePath";
            this.tb_FilePath.Size = new System.Drawing.Size(150, 21);
            this.tb_FilePath.TabIndex = 2;
            // 
            // btn_FileOpen
            // 
            this.btn_FileOpen.Location = new System.Drawing.Point(151, 45);
            this.btn_FileOpen.Name = "btn_FileOpen";
            this.btn_FileOpen.Size = new System.Drawing.Size(48, 23);
            this.btn_FileOpen.TabIndex = 3;
            this.btn_FileOpen.Text = "열  기";
            this.btn_FileOpen.UseVisualStyleBackColor = true;
            this.btn_FileOpen.Click += new System.EventHandler(this.btn_FileOpen_Click);
            // 
            // start_date
            // 
            this.start_date.CustomFormat = "yyyy-MM-dd";
            this.start_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.start_date.Location = new System.Drawing.Point(264, 13);
            this.start_date.Name = "start_date";
            this.start_date.Size = new System.Drawing.Size(98, 21);
            this.start_date.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(211, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "목록일자";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(369, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "납품목록";
            // 
            // tb_dvy1
            // 
            this.tb_dvy1.Location = new System.Drawing.Point(421, 13);
            this.tb_dvy1.Name = "tb_dvy1";
            this.tb_dvy1.Size = new System.Drawing.Size(98, 21);
            this.tb_dvy1.TabIndex = 8;
            // 
            // tb_clt1
            // 
            this.tb_clt1.Location = new System.Drawing.Point(565, 13);
            this.tb_clt1.Name = "tb_clt1";
            this.tb_clt1.Size = new System.Drawing.Size(98, 21);
            this.tb_clt1.TabIndex = 10;
            this.tb_clt1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_clt1_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(523, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "거래처";
            // 
            // tb_UserName
            // 
            this.tb_UserName.Location = new System.Drawing.Point(720, 13);
            this.tb_UserName.Name = "tb_UserName";
            this.tb_UserName.Size = new System.Drawing.Size(98, 21);
            this.tb_UserName.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(677, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "담당자";
            // 
            // tb_ListNum
            // 
            this.tb_ListNum.Location = new System.Drawing.Point(861, 13);
            this.tb_ListNum.Name = "tb_ListNum";
            this.tb_ListNum.Size = new System.Drawing.Size(98, 21);
            this.tb_ListNum.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(832, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 24);
            this.label6.TabIndex = 13;
            this.label6.Text = "목록\n번호";
            // 
            // tb_dvy_method
            // 
            this.tb_dvy_method.Location = new System.Drawing.Point(663, 46);
            this.tb_dvy_method.Name = "tb_dvy_method";
            this.tb_dvy_method.Size = new System.Drawing.Size(114, 21);
            this.tb_dvy_method.TabIndex = 21;
            // 
            // tb_Work_method
            // 
            this.tb_Work_method.Location = new System.Drawing.Point(412, 46);
            this.tb_Work_method.Name = "tb_Work_method";
            this.tb_Work_method.Size = new System.Drawing.Size(210, 21);
            this.tb_Work_method.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(211, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 17;
            this.label9.Text = "완료일자";
            // 
            // end_date
            // 
            this.end_date.Checked = false;
            this.end_date.CustomFormat = "yyyy-MM-dd";
            this.end_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_date.Location = new System.Drawing.Point(264, 46);
            this.end_date.Name = "end_date";
            this.end_date.ShowCheckBox = true;
            this.end_date.Size = new System.Drawing.Size(99, 21);
            this.end_date.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(377, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 24);
            this.label10.TabIndex = 22;
            this.label10.Text = "작업\n방법";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(628, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 24);
            this.label11.TabIndex = 23;
            this.label11.Text = "배송\n방법";
            // 
            // btn_Exit
            // 
            this.btn_Exit.Location = new System.Drawing.Point(1089, 45);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(50, 23);
            this.btn_Exit.TabIndex = 27;
            this.btn_Exit.Text = "닫   기";
            this.btn_Exit.UseVisualStyleBackColor = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(1036, 45);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(50, 23);
            this.btn_Save.TabIndex = 28;
            this.btn_Save.Text = "저   장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.header,
            this.num,
            this.book_name,
            this.author,
            this.book_comp,
            this.count,
            this.unit,
            this.total,
            this.etc,
            this.isbn,
            this.order,
            this.stat,
            this.chk_time});
            this.dataGridView1.Location = new System.Drawing.Point(13, 82);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 30;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1126, 229);
            this.dataGridView1.TabIndex = 29;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // header
            // 
            this.header.HeaderText = "머리글";
            this.header.Name = "header";
            this.header.Width = 50;
            // 
            // num
            // 
            this.num.HeaderText = "번호";
            this.num.Name = "num";
            this.num.Width = 35;
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 300;
            // 
            // author
            // 
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            this.author.Width = 130;
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            this.book_comp.Width = 130;
            // 
            // count
            // 
            dataGridViewCellStyle2.Format = "C0";
            dataGridViewCellStyle2.NullValue = null;
            this.count.DefaultCellStyle = dataGridViewCellStyle2;
            this.count.HeaderText = "수량";
            this.count.Name = "count";
            this.count.Width = 35;
            // 
            // unit
            // 
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.unit.DefaultCellStyle = dataGridViewCellStyle3;
            this.unit.HeaderText = "단가";
            this.unit.Name = "unit";
            this.unit.Width = 70;
            // 
            // total
            // 
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = null;
            this.total.DefaultCellStyle = dataGridViewCellStyle4;
            this.total.HeaderText = "합계";
            this.total.Name = "total";
            this.total.Width = 80;
            // 
            // etc
            // 
            this.etc.HeaderText = "비고";
            this.etc.Name = "etc";
            // 
            // isbn
            // 
            this.isbn.HeaderText = "ISBN13";
            this.isbn.Name = "isbn";
            this.isbn.Width = 80;
            // 
            // order
            // 
            this.order.HeaderText = "주문처";
            this.order.Name = "order";
            this.order.Width = 50;
            // 
            // stat
            // 
            this.stat.HeaderText = "상태";
            this.stat.Name = "stat";
            this.stat.Visible = false;
            // 
            // chk_time
            // 
            this.chk_time.HeaderText = "검수시간";
            this.chk_time.Name = "chk_time";
            this.chk_time.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 346);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 1;
            this.label7.Text = "구분";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(60, 342);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(83, 20);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.list_date,
            this.pur,
            this.dly,
            this.charge,
            this.list_name,
            this.cout,
            this.tol,
            this.stat2,
            this.Grid_btn_ISBN,
            this.chk_marc,
            this.Grid_Check});
            this.dataGridView2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dataGridView2.Location = new System.Drawing.Point(13, 372);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 30;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(1098, 229);
            this.dataGridView2.TabIndex = 29;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            this.dataGridView2.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // list_date
            // 
            this.list_date.HeaderText = "목록일자";
            this.list_date.Name = "list_date";
            this.list_date.Width = 70;
            // 
            // pur
            // 
            this.pur.HeaderText = "거래처명";
            this.pur.Name = "pur";
            this.pur.Width = 150;
            // 
            // dly
            // 
            this.dly.HeaderText = "납품목록";
            this.dly.Name = "dly";
            this.dly.Width = 150;
            // 
            // charge
            // 
            this.charge.HeaderText = "담당자";
            this.charge.Name = "charge";
            // 
            // list_name
            // 
            this.list_name.HeaderText = "목록명";
            this.list_name.Name = "list_name";
            this.list_name.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.list_name.Width = 250;
            // 
            // cout
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.cout.DefaultCellStyle = dataGridViewCellStyle6;
            this.cout.HeaderText = "수량";
            this.cout.Name = "cout";
            this.cout.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cout.Width = 50;
            // 
            // tol
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.NullValue = null;
            this.tol.DefaultCellStyle = dataGridViewCellStyle7;
            this.tol.HeaderText = "합계";
            this.tol.Name = "tol";
            this.tol.Width = 80;
            // 
            // stat2
            // 
            this.stat2.HeaderText = "상태";
            this.stat2.Name = "stat2";
            this.stat2.Width = 50;
            // 
            // Grid_btn_ISBN
            // 
            this.Grid_btn_ISBN.HeaderText = "ISBN조회";
            this.Grid_btn_ISBN.Name = "Grid_btn_ISBN";
            this.Grid_btn_ISBN.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Grid_btn_ISBN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // chk_marc
            // 
            this.chk_marc.HeaderText = "MarcCheck";
            this.chk_marc.Name = "chk_marc";
            this.chk_marc.ReadOnly = true;
            this.chk_marc.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_marc.Visible = false;
            // 
            // Grid_Check
            // 
            this.Grid_Check.HeaderText = "체크";
            this.Grid_Check.Name = "Grid_Check";
            this.Grid_Check.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Grid_Check.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Grid_Check.Width = 40;
            // 
            // btn_morge
            // 
            this.btn_morge.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_morge.Location = new System.Drawing.Point(1049, 341);
            this.btn_morge.Name = "btn_morge";
            this.btn_morge.Size = new System.Drawing.Size(62, 23);
            this.btn_morge.TabIndex = 28;
            this.btn_morge.Text = "병   합";
            this.btn_morge.UseVisualStyleBackColor = true;
            this.btn_morge.Click += new System.EventHandler(this.btn_morge_Click);
            // 
            // btn_Edit
            // 
            this.btn_Edit.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Edit.Location = new System.Drawing.Point(981, 341);
            this.btn_Edit.Name = "btn_Edit";
            this.btn_Edit.Size = new System.Drawing.Size(62, 23);
            this.btn_Edit.TabIndex = 28;
            this.btn_Edit.Text = "수   정";
            this.btn_Edit.UseVisualStyleBackColor = true;
            this.btn_Edit.Click += new System.EventHandler(this.btn_Edit_Click);
            // 
            // btn_Del
            // 
            this.btn_Del.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Del.Location = new System.Drawing.Point(913, 341);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.Size = new System.Drawing.Size(62, 23);
            this.btn_Del.TabIndex = 28;
            this.btn_Del.Text = "삭   제";
            this.btn_Del.UseVisualStyleBackColor = true;
            this.btn_Del.Click += new System.EventHandler(this.btn_Del_Click);
            // 
            // btn_Complet
            // 
            this.btn_Complet.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Complet.Location = new System.Drawing.Point(845, 341);
            this.btn_Complet.Name = "btn_Complet";
            this.btn_Complet.Size = new System.Drawing.Size(62, 23);
            this.btn_Complet.TabIndex = 28;
            this.btn_Complet.Text = "완료처리";
            this.btn_Complet.UseVisualStyleBackColor = true;
            this.btn_Complet.Click += new System.EventHandler(this.btn_Complet_Click);
            // 
            // btn_ing
            // 
            this.btn_ing.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_ing.Location = new System.Drawing.Point(777, 341);
            this.btn_ing.Name = "btn_ing";
            this.btn_ing.Size = new System.Drawing.Size(62, 23);
            this.btn_ing.TabIndex = 28;
            this.btn_ing.Text = "진행처리";
            this.btn_ing.UseVisualStyleBackColor = true;
            this.btn_ing.Click += new System.EventHandler(this.btn_ing_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button7.Location = new System.Drawing.Point(709, 341);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(62, 23);
            this.button7.TabIndex = 28;
            this.button7.Text = "작업내용";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // btn_lockup
            // 
            this.btn_lockup.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_lockup.Location = new System.Drawing.Point(641, 341);
            this.btn_lockup.Name = "btn_lockup";
            this.btn_lockup.Size = new System.Drawing.Size(62, 23);
            this.btn_lockup.TabIndex = 28;
            this.btn_lockup.Text = "조   회";
            this.btn_lockup.UseVisualStyleBackColor = true;
            this.btn_lockup.Click += new System.EventHandler(this.btn_lockup_Click);
            // 
            // tb_dvy2
            // 
            this.tb_dvy2.Location = new System.Drawing.Point(199, 342);
            this.tb_dvy2.Name = "tb_dvy2";
            this.tb_dvy2.Size = new System.Drawing.Size(115, 21);
            this.tb_dvy2.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(156, 346);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "납품처";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(324, 346);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 9;
            this.label12.Text = "거래처";
            // 
            // tb_clt2
            // 
            this.tb_clt2.Location = new System.Drawing.Point(366, 342);
            this.tb_clt2.Name = "tb_clt2";
            this.tb_clt2.Size = new System.Drawing.Size(115, 21);
            this.tb_clt2.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(494, 346);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 11;
            this.label13.Text = "담당자";
            // 
            // tb_user2
            // 
            this.tb_user2.Location = new System.Drawing.Point(537, 342);
            this.tb_user2.Name = "tb_user2";
            this.tb_user2.Size = new System.Drawing.Size(98, 21);
            this.tb_user2.TabIndex = 12;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(983, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "자동목록번호";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // rbtn_marc
            // 
            this.rbtn_marc.AutoSize = true;
            this.rbtn_marc.Location = new System.Drawing.Point(72, 4);
            this.rbtn_marc.Name = "rbtn_marc";
            this.rbtn_marc.Size = new System.Drawing.Size(59, 16);
            this.rbtn_marc.TabIndex = 33;
            this.rbtn_marc.TabStop = true;
            this.rbtn_marc.Text = "마크만";
            this.rbtn_marc.UseVisualStyleBackColor = true;
            // 
            // rbtn_div
            // 
            this.rbtn_div.AutoSize = true;
            this.rbtn_div.Location = new System.Drawing.Point(3, 4);
            this.rbtn_div.Name = "rbtn_div";
            this.rbtn_div.Size = new System.Drawing.Size(59, 16);
            this.rbtn_div.TabIndex = 34;
            this.rbtn_div.TabStop = true;
            this.rbtn_div.Text = "납품만";
            this.rbtn_div.UseVisualStyleBackColor = true;
            // 
            // rbtn_all
            // 
            this.rbtn_all.AutoSize = true;
            this.rbtn_all.Location = new System.Drawing.Point(141, 4);
            this.rbtn_all.Name = "rbtn_all";
            this.rbtn_all.Size = new System.Drawing.Size(63, 16);
            this.rbtn_all.TabIndex = 34;
            this.rbtn_all.TabStop = true;
            this.rbtn_all.Text = "양쪽 다";
            this.rbtn_all.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.rbtn_marc);
            this.panel2.Controls.Add(this.rbtn_all);
            this.panel2.Controls.Add(this.rbtn_div);
            this.panel2.Location = new System.Drawing.Point(821, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(211, 26);
            this.panel2.TabIndex = 35;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(790, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 24);
            this.label14.TabIndex = 22;
            this.label14.Text = "작업\n대상";
            // 
            // btn_total
            // 
            this.btn_total.Location = new System.Drawing.Point(1089, 12);
            this.btn_total.Name = "btn_total";
            this.btn_total.Size = new System.Drawing.Size(72, 23);
            this.btn_total.TabIndex = 28;
            this.btn_total.Text = "합계 계산";
            this.btn_total.UseVisualStyleBackColor = true;
            this.btn_total.Click += new System.EventHandler(this.btn_Total_Click);
            // 
            // Commodity_registration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 613);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_morge);
            this.Controls.Add(this.btn_Edit);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_Del);
            this.Controls.Add(this.btn_total);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.btn_Complet);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.btn_ing);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btn_lockup);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tb_dvy2);
            this.Controls.Add(this.tb_dvy_method);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tb_Work_method);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tb_clt2);
            this.Controls.Add(this.end_date);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tb_user2);
            this.Controls.Add(this.tb_ListNum);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tb_UserName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_clt1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_dvy1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.start_date);
            this.Controls.Add(this.btn_FileOpen);
            this.Controls.Add(this.tb_FilePath);
            this.Controls.Add(this.label1);
            this.Name = "Commodity_registration";
            this.Text = "목록등록";
            this.Load += new System.EventHandler(this.Commodity_registration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_FilePath;
        private System.Windows.Forms.Button btn_FileOpen;
        private System.Windows.Forms.DateTimePicker start_date;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_dvy1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_UserName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_ListNum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_dvy_method;
        private System.Windows.Forms.TextBox tb_Work_method;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker end_date;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btn_Edit;
        private System.Windows.Forms.Button btn_Del;
        private System.Windows.Forms.Button btn_Complet;
        private System.Windows.Forms.Button btn_ing;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btn_lockup;
        private System.Windows.Forms.TextBox tb_dvy2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_clt2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_user2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.TextBox tb_clt1;
        private System.Windows.Forms.Button btn_morge;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton rbtn_marc;
        private System.Windows.Forms.RadioButton rbtn_div;
        private System.Windows.Forms.RadioButton rbtn_all;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewTextBoxColumn header;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn order;
        private System.Windows.Forms.DataGridViewTextBoxColumn stat;
        private System.Windows.Forms.DataGridViewTextBoxColumn chk_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn pur;
        private System.Windows.Forms.DataGridViewTextBoxColumn dly;
        private System.Windows.Forms.DataGridViewTextBoxColumn charge;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn cout;
        private System.Windows.Forms.DataGridViewTextBoxColumn tol;
        private System.Windows.Forms.DataGridViewTextBoxColumn stat2;
        private System.Windows.Forms.DataGridViewButtonColumn Grid_btn_ISBN;
        private System.Windows.Forms.DataGridViewTextBoxColumn chk_marc;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Grid_Check;
        private System.Windows.Forms.Button btn_total;
    }
}