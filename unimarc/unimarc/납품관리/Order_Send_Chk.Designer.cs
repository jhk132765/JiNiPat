﻿
namespace WindowsFormsApp1.납품관리
{
    partial class Order_Send_Chk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_pur = new System.Windows.Forms.TextBox();
            this.Start_DatePicker = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.End_DatePicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Lookup = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.dataGrid_Fax = new System.Windows.Forms.DataGridView();
            this.FaxKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f_state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f_page1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f_page2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f_filename = new System.Windows.Forms.DataGridViewButtonColumn();
            this.f_res = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGrid_Email = new System.Windows.Forms.DataGridView();
            this.m_clt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_sender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_taker = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_filename = new System.Windows.Forms.DataGridViewButtonColumn();
            this.m_sendres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Fax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Email)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tb_pur);
            this.panel1.Controls.Add(this.Start_DatePicker);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.End_DatePicker);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(538, 35);
            this.panel1.TabIndex = 0;
            // 
            // tb_pur
            // 
            this.tb_pur.Location = new System.Drawing.Point(381, 6);
            this.tb_pur.Name = "tb_pur";
            this.tb_pur.Size = new System.Drawing.Size(147, 21);
            this.tb_pur.TabIndex = 21;
            // 
            // Start_DatePicker
            // 
            this.Start_DatePicker.CustomFormat = "yyyy-MM-dd";
            this.Start_DatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Start_DatePicker.Location = new System.Drawing.Point(96, 6);
            this.Start_DatePicker.Name = "Start_DatePicker";
            this.Start_DatePicker.Size = new System.Drawing.Size(85, 21);
            this.Start_DatePicker.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(184, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 12);
            this.label7.TabIndex = 19;
            this.label7.Text = "~";
            // 
            // End_DatePicker
            // 
            this.End_DatePicker.CustomFormat = "yyyy-MM-dd";
            this.End_DatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.End_DatePicker.Location = new System.Drawing.Point(200, 6);
            this.End_DatePicker.Name = "End_DatePicker";
            this.End_DatePicker.Size = new System.Drawing.Size(85, 21);
            this.End_DatePicker.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(322, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "거래처명";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "발송 요청일";
            // 
            // btn_Lookup
            // 
            this.btn_Lookup.Location = new System.Drawing.Point(569, 15);
            this.btn_Lookup.Name = "btn_Lookup";
            this.btn_Lookup.Size = new System.Drawing.Size(85, 28);
            this.btn_Lookup.TabIndex = 1;
            this.btn_Lookup.Text = "조    회";
            this.btn_Lookup.UseVisualStyleBackColor = true;
            this.btn_Lookup.Click += new System.EventHandler(this.btn_Lookup_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(660, 15);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(85, 28);
            this.btn_Close.TabIndex = 1;
            this.btn_Close.Text = "닫    기";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // dataGrid_Fax
            // 
            this.dataGrid_Fax.AllowUserToAddRows = false;
            this.dataGrid_Fax.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_Fax.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGrid_Fax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGrid_Fax.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FaxKey,
            this.f_comp,
            this.f_num,
            this.f_date,
            this.f_state,
            this.f_page1,
            this.f_page2,
            this.f_filename,
            this.f_res});
            this.dataGrid_Fax.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGrid_Fax.Location = new System.Drawing.Point(12, 53);
            this.dataGrid_Fax.Name = "dataGrid_Fax";
            this.dataGrid_Fax.RowHeadersWidth = 10;
            this.dataGrid_Fax.RowTemplate.Height = 23;
            this.dataGrid_Fax.Size = new System.Drawing.Size(890, 511);
            this.dataGrid_Fax.TabIndex = 2;
            this.dataGrid_Fax.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_Email_CellContentClick);
            // 
            // FaxKey
            // 
            this.FaxKey.HeaderText = "FaxKey";
            this.FaxKey.Name = "FaxKey";
            this.FaxKey.Visible = false;
            // 
            // f_comp
            // 
            this.f_comp.HeaderText = "수신자회사명";
            this.f_comp.Name = "f_comp";
            this.f_comp.Width = 150;
            // 
            // f_num
            // 
            this.f_num.HeaderText = "수신번호";
            this.f_num.Name = "f_num";
            // 
            // f_date
            // 
            this.f_date.HeaderText = "전송일시";
            this.f_date.Name = "f_date";
            // 
            // f_state
            // 
            this.f_state.HeaderText = "전송상태";
            this.f_state.Name = "f_state";
            // 
            // f_page1
            // 
            this.f_page1.HeaderText = "Page";
            this.f_page1.Name = "f_page1";
            this.f_page1.Width = 80;
            // 
            // f_page2
            // 
            this.f_page2.HeaderText = "Page";
            this.f_page2.Name = "f_page2";
            this.f_page2.Width = 80;
            // 
            // f_filename
            // 
            this.f_filename.HeaderText = "전송파일명";
            this.f_filename.Name = "f_filename";
            this.f_filename.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.f_filename.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.f_filename.Width = 150;
            // 
            // f_res
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.f_res.DefaultCellStyle = dataGridViewCellStyle5;
            this.f_res.HeaderText = "전송결과";
            this.f_res.Name = "f_res";
            // 
            // dataGrid_Email
            // 
            this.dataGrid_Email.AllowUserToAddRows = false;
            this.dataGrid_Email.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_Email.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGrid_Email.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid_Email.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_clt,
            this.m_date,
            this.m_time,
            this.m_sender,
            this.m_taker,
            this.m_filename,
            this.m_sendres});
            this.dataGrid_Email.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGrid_Email.Location = new System.Drawing.Point(12, 53);
            this.dataGrid_Email.Name = "dataGrid_Email";
            this.dataGrid_Email.RowHeadersWidth = 10;
            this.dataGrid_Email.RowTemplate.Height = 23;
            this.dataGrid_Email.Size = new System.Drawing.Size(890, 493);
            this.dataGrid_Email.TabIndex = 3;
            this.dataGrid_Email.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_Email_CellContentClick);
            // 
            // m_clt
            // 
            this.m_clt.HeaderText = "거래처명";
            this.m_clt.Name = "m_clt";
            this.m_clt.Width = 150;
            // 
            // m_date
            // 
            this.m_date.HeaderText = "날짜";
            this.m_date.Name = "m_date";
            // 
            // m_time
            // 
            this.m_time.HeaderText = "시간";
            this.m_time.Name = "m_time";
            // 
            // m_sender
            // 
            this.m_sender.HeaderText = "보낸 메일주소";
            this.m_sender.Name = "m_sender";
            this.m_sender.Width = 130;
            // 
            // m_taker
            // 
            this.m_taker.HeaderText = "받는 메일주소";
            this.m_taker.Name = "m_taker";
            this.m_taker.Width = 130;
            // 
            // m_filename
            // 
            this.m_filename.HeaderText = "전송파일명";
            this.m_filename.Name = "m_filename";
            this.m_filename.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.m_filename.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.m_filename.Width = 150;
            // 
            // m_sendres
            // 
            this.m_sendres.HeaderText = "전송결과";
            this.m_sendres.Name = "m_sendres";
            // 
            // Order_Send_Chk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 576);
            this.Controls.Add(this.dataGrid_Email);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Lookup);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGrid_Fax);
            this.Name = "Order_Send_Chk";
            this.Load += new System.EventHandler(this.Order_Send_Chk_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Fax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Email)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_pur;
        private System.Windows.Forms.DateTimePicker Start_DatePicker;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker End_DatePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Lookup;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.DataGridView dataGrid_Fax;
        private System.Windows.Forms.DataGridView dataGrid_Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn FaxKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn f_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn f_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn f_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn f_state;
        private System.Windows.Forms.DataGridViewTextBoxColumn f_page1;
        private System.Windows.Forms.DataGridViewTextBoxColumn f_page2;
        private System.Windows.Forms.DataGridViewButtonColumn f_filename;
        private System.Windows.Forms.DataGridViewTextBoxColumn f_res;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_clt;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_sender;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_taker;
        private System.Windows.Forms.DataGridViewButtonColumn m_filename;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_sendres;
    }
}