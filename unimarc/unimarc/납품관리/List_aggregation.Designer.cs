﻿namespace WindowsFormsApp1.Delivery
{
    partial class List_aggregation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(List_aggregation));
            this.btn_lookup = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.combo_state = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_clt = new System.Windows.Forms.TextBox();
            this.tb_dlv = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.list_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.list_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_res = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.work_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.work_way = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.send_way = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stock_money = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chk_label = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk_Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.combo_user = new System.Windows.Forms.ComboBox();
            this.btn_process = new System.Windows.Forms.Button();
            this.btn_ISBN = new System.Windows.Forms.Button();
            this.btn_checkup = new System.Windows.Forms.Button();
            this.btn_print = new System.Windows.Forms.Button();
            this.combo_year = new System.Windows.Forms.ComboBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.date_res_Value = new System.Windows.Forms.DateTimePicker();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rtb_etc = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tb_work_way = new System.Windows.Forms.RichTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.lbl_send_n = new System.Windows.Forms.Label();
            this.lbl_l_n = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lbl_send_y = new System.Windows.Forms.Label();
            this.lbl_l_y = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tb_send_way = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.tb_fax = new System.Windows.Forms.TextBox();
            this.tb_tel = new System.Windows.Forms.TextBox();
            this.tb_boss = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_gu = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_addr = new System.Windows.Forms.TextBox();
            this.tb_client = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_work_name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_apply = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbl_total = new System.Windows.Forms.Label();
            this.lbl_count = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_lookup
            // 
            this.btn_lookup.Location = new System.Drawing.Point(718, 11);
            this.btn_lookup.Name = "btn_lookup";
            this.btn_lookup.Size = new System.Drawing.Size(65, 23);
            this.btn_lookup.TabIndex = 10;
            this.btn_lookup.Text = "조   회";
            this.btn_lookup.UseVisualStyleBackColor = true;
            this.btn_lookup.Click += new System.EventHandler(this.btn_lookup_Click);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(789, 11);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(65, 23);
            this.btn_save.TabIndex = 11;
            this.btn_save.Text = "저   장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "상 태";
            // 
            // combo_state
            // 
            this.combo_state.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_state.FormattingEnabled = true;
            this.combo_state.Location = new System.Drawing.Point(47, 12);
            this.combo_state.Name = "combo_state";
            this.combo_state.Size = new System.Drawing.Size(67, 20);
            this.combo_state.TabIndex = 2;
            this.combo_state.SelectedIndexChanged += new System.EventHandler(this.combo_state_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(349, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "거래처명";
            // 
            // tb_clt
            // 
            this.tb_clt.Location = new System.Drawing.Point(400, 12);
            this.tb_clt.Name = "tb_clt";
            this.tb_clt.Size = new System.Drawing.Size(126, 21);
            this.tb_clt.TabIndex = 7;
            this.tb_clt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_clt_KeyDown);
            // 
            // tb_dlv
            // 
            this.tb_dlv.BackColor = System.Drawing.SystemColors.Window;
            this.tb_dlv.Location = new System.Drawing.Point(586, 12);
            this.tb_dlv.Name = "tb_dlv";
            this.tb_dlv.Size = new System.Drawing.Size(126, 21);
            this.tb_dlv.TabIndex = 9;
            this.tb_dlv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_clt_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(534, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "납품처명";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx,
            this.list_name,
            this.list_date,
            this.date_res,
            this.charge,
            this.work_name,
            this.work_way,
            this.send_way,
            this.total,
            this.stock_money,
            this.count,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column18,
            this.Column20,
            this.chk_label,
            this.clt,
            this.Chk_Column});
            this.dataGridView1.Location = new System.Drawing.Point(12, 40);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 20;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(984, 567);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.Visible = false;
            // 
            // list_name
            // 
            this.list_name.HeaderText = "목록명";
            this.list_name.Name = "list_name";
            this.list_name.Width = 230;
            // 
            // list_date
            // 
            this.list_date.HeaderText = "목록일자";
            this.list_date.Name = "list_date";
            this.list_date.Width = 80;
            // 
            // date_res
            // 
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.date_res.DefaultCellStyle = dataGridViewCellStyle2;
            this.date_res.HeaderText = "완료일자";
            this.date_res.Name = "date_res";
            this.date_res.Width = 80;
            // 
            // charge
            // 
            this.charge.HeaderText = "담당자";
            this.charge.Name = "charge";
            this.charge.Width = 50;
            // 
            // work_name
            // 
            this.work_name.HeaderText = "작업명";
            this.work_name.Name = "work_name";
            this.work_name.Visible = false;
            this.work_name.Width = 150;
            // 
            // work_way
            // 
            this.work_way.HeaderText = "작업방법";
            this.work_way.Name = "work_way";
            this.work_way.Visible = false;
            // 
            // send_way
            // 
            this.send_way.HeaderText = "배송방법";
            this.send_way.Name = "send_way";
            this.send_way.Visible = false;
            // 
            // total
            // 
            this.total.HeaderText = "합계금액";
            this.total.Name = "total";
            this.total.Width = 90;
            // 
            // stock_money
            // 
            this.stock_money.HeaderText = "입고금액";
            this.stock_money.Name = "stock_money";
            this.stock_money.Width = 90;
            // 
            // count
            // 
            this.count.HeaderText = "수량";
            this.count.Name = "count";
            this.count.Width = 50;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "입고";
            this.Column11.Name = "Column11";
            this.Column11.Width = 50;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "미입고";
            this.Column12.Name = "Column12";
            this.Column12.Width = 50;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "입고율";
            this.Column13.Name = "Column13";
            this.Column13.Width = 50;
            // 
            // Column18
            // 
            this.Column18.HeaderText = "택배";
            this.Column18.Name = "Column18";
            this.Column18.Width = 50;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "목록번호";
            this.Column20.Name = "Column20";
            this.Column20.Width = 70;
            // 
            // chk_label
            // 
            this.chk_label.HeaderText = "라벨";
            this.chk_label.Name = "chk_label";
            this.chk_label.Visible = false;
            // 
            // clt
            // 
            this.clt.HeaderText = "거래처명";
            this.clt.Name = "clt";
            this.clt.Visible = false;
            // 
            // Chk_Column
            // 
            this.Chk_Column.HeaderText = "체크";
            this.Chk_Column.Name = "Chk_Column";
            this.Chk_Column.Visible = false;
            this.Chk_Column.Width = 40;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(212, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "담당자";
            // 
            // combo_user
            // 
            this.combo_user.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_user.FormattingEnabled = true;
            this.combo_user.Location = new System.Drawing.Point(252, 12);
            this.combo_user.Name = "combo_user";
            this.combo_user.Size = new System.Drawing.Size(89, 20);
            this.combo_user.TabIndex = 5;
            // 
            // btn_process
            // 
            this.btn_process.Location = new System.Drawing.Point(306, 6);
            this.btn_process.Name = "btn_process";
            this.btn_process.Size = new System.Drawing.Size(65, 23);
            this.btn_process.TabIndex = 4;
            this.btn_process.Text = "완료처리";
            this.btn_process.UseVisualStyleBackColor = true;
            this.btn_process.Click += new System.EventHandler(this.btn_process_Click);
            // 
            // btn_ISBN
            // 
            this.btn_ISBN.Location = new System.Drawing.Point(212, 6);
            this.btn_ISBN.Name = "btn_ISBN";
            this.btn_ISBN.Size = new System.Drawing.Size(65, 23);
            this.btn_ISBN.TabIndex = 2;
            this.btn_ISBN.Text = "ISBN조회";
            this.btn_ISBN.UseVisualStyleBackColor = true;
            this.btn_ISBN.Click += new System.EventHandler(this.btn_ISBN_Click);
            // 
            // btn_checkup
            // 
            this.btn_checkup.Location = new System.Drawing.Point(118, 6);
            this.btn_checkup.Name = "btn_checkup";
            this.btn_checkup.Size = new System.Drawing.Size(65, 23);
            this.btn_checkup.TabIndex = 1;
            this.btn_checkup.Text = "검수작업";
            this.btn_checkup.UseVisualStyleBackColor = true;
            this.btn_checkup.Click += new System.EventHandler(this.btn_checkup_Click);
            // 
            // btn_print
            // 
            this.btn_print.Location = new System.Drawing.Point(860, 11);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(65, 23);
            this.btn_print.TabIndex = 12;
            this.btn_print.Text = "인   쇄";
            this.btn_print.UseVisualStyleBackColor = true;
            this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
            // 
            // combo_year
            // 
            this.combo_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_year.FormattingEnabled = true;
            this.combo_year.Location = new System.Drawing.Point(120, 12);
            this.combo_year.Name = "combo_year";
            this.combo_year.Size = new System.Drawing.Size(84, 20);
            this.combo_year.TabIndex = 3;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(931, 11);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(65, 23);
            this.btn_close.TabIndex = 13;
            this.btn_close.Text = "닫   기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.date_res_Value);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.lbl_send_n);
            this.panel1.Controls.Add(this.lbl_l_n);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.lbl_send_y);
            this.panel1.Controls.Add(this.lbl_l_y);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.tb_send_way);
            this.panel1.Controls.Add(this.tb_email);
            this.panel1.Controls.Add(this.tb_fax);
            this.panel1.Controls.Add(this.tb_tel);
            this.panel1.Controls.Add(this.tb_boss);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.tb_gu);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.tb_addr);
            this.panel1.Controls.Add(this.tb_client);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.tb_work_name);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.btn_ISBN);
            this.panel1.Controls.Add(this.btn_apply);
            this.panel1.Controls.Add(this.btn_checkup);
            this.panel1.Controls.Add(this.btn_process);
            this.panel1.Location = new System.Drawing.Point(1002, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(381, 634);
            this.panel1.TabIndex = 14;
            // 
            // date_res_Value
            // 
            this.date_res_Value.Checked = false;
            this.date_res_Value.CustomFormat = "yyyy-MM-dd";
            this.date_res_Value.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_res_Value.Location = new System.Drawing.Point(74, 360);
            this.date_res_Value.Name = "date_res_Value";
            this.date_res_Value.ShowCheckBox = true;
            this.date_res_Value.Size = new System.Drawing.Size(109, 21);
            this.date_res_Value.TabIndex = 19;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.rtb_etc);
            this.panel3.Location = new System.Drawing.Point(74, 176);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(299, 74);
            this.panel3.TabIndex = 18;
            // 
            // rtb_etc
            // 
            this.rtb_etc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_etc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtb_etc.Location = new System.Drawing.Point(0, 0);
            this.rtb_etc.Name = "rtb_etc";
            this.rtb_etc.Size = new System.Drawing.Size(297, 72);
            this.rtb_etc.TabIndex = 17;
            this.rtb_etc.Text = "";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tb_work_way);
            this.panel2.Location = new System.Drawing.Point(74, 466);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(299, 74);
            this.panel2.TabIndex = 18;
            // 
            // tb_work_way
            // 
            this.tb_work_way.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_way.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_work_way.Location = new System.Drawing.Point(0, 0);
            this.tb_work_way.Name = "tb_work_way";
            this.tb_work_way.Size = new System.Drawing.Size(297, 72);
            this.tb_work_way.TabIndex = 17;
            this.tb_work_way.Text = "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(10, 415);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 13);
            this.label21.TabIndex = 16;
            this.label21.Text = "라벨여부";
            // 
            // lbl_send_n
            // 
            this.lbl_send_n.AutoSize = true;
            this.lbl_send_n.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_send_n.Location = new System.Drawing.Point(327, 415);
            this.lbl_send_n.Name = "lbl_send_n";
            this.lbl_send_n.Size = new System.Drawing.Size(24, 13);
            this.lbl_send_n.TabIndex = 16;
            this.lbl_send_n.Text = "No";
            this.lbl_send_n.Click += new System.EventHandler(this.lbl_l_y_Click);
            // 
            // lbl_l_n
            // 
            this.lbl_l_n.AutoSize = true;
            this.lbl_l_n.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_l_n.Location = new System.Drawing.Point(148, 415);
            this.lbl_l_n.Name = "lbl_l_n";
            this.lbl_l_n.Size = new System.Drawing.Size(24, 13);
            this.lbl_l_n.TabIndex = 16;
            this.lbl_l_n.Text = "No";
            this.lbl_l_n.Click += new System.EventHandler(this.lbl_l_y_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(306, 415);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "/";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(127, 415);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 13);
            this.label23.TabIndex = 16;
            this.label23.Text = "/";
            // 
            // lbl_send_y
            // 
            this.lbl_send_y.AutoSize = true;
            this.lbl_send_y.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_send_y.Location = new System.Drawing.Point(268, 415);
            this.lbl_send_y.Name = "lbl_send_y";
            this.lbl_send_y.Size = new System.Drawing.Size(31, 13);
            this.lbl_send_y.TabIndex = 16;
            this.lbl_send_y.Text = "Yes";
            this.lbl_send_y.Click += new System.EventHandler(this.lbl_l_y_Click);
            // 
            // lbl_l_y
            // 
            this.lbl_l_y.AutoSize = true;
            this.lbl_l_y.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_l_y.Location = new System.Drawing.Point(88, 415);
            this.lbl_l_y.Name = "lbl_l_y";
            this.lbl_l_y.Size = new System.Drawing.Size(31, 13);
            this.lbl_l_y.TabIndex = 16;
            this.lbl_l_y.Text = "Yes";
            this.lbl_l_y.Click += new System.EventHandler(this.lbl_l_y_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(219, 415);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 13);
            this.label19.TabIndex = 16;
            this.label19.Text = "택배";
            // 
            // tb_send_way
            // 
            this.tb_send_way.BackColor = System.Drawing.SystemColors.Window;
            this.tb_send_way.Location = new System.Drawing.Point(74, 437);
            this.tb_send_way.Name = "tb_send_way";
            this.tb_send_way.Size = new System.Drawing.Size(299, 21);
            this.tb_send_way.TabIndex = 12;
            // 
            // tb_email
            // 
            this.tb_email.BackColor = System.Drawing.SystemColors.Window;
            this.tb_email.Location = new System.Drawing.Point(74, 122);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(298, 21);
            this.tb_email.TabIndex = 10;
            // 
            // tb_fax
            // 
            this.tb_fax.BackColor = System.Drawing.SystemColors.Window;
            this.tb_fax.Location = new System.Drawing.Point(254, 95);
            this.tb_fax.Name = "tb_fax";
            this.tb_fax.Size = new System.Drawing.Size(119, 21);
            this.tb_fax.TabIndex = 10;
            // 
            // tb_tel
            // 
            this.tb_tel.BackColor = System.Drawing.SystemColors.Window;
            this.tb_tel.Location = new System.Drawing.Point(74, 95);
            this.tb_tel.Name = "tb_tel";
            this.tb_tel.Size = new System.Drawing.Size(119, 21);
            this.tb_tel.TabIndex = 10;
            // 
            // tb_boss
            // 
            this.tb_boss.BackColor = System.Drawing.SystemColors.Window;
            this.tb_boss.Location = new System.Drawing.Point(254, 68);
            this.tb_boss.Name = "tb_boss";
            this.tb_boss.Size = new System.Drawing.Size(119, 21);
            this.tb_boss.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(36, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "메일";
            // 
            // tb_gu
            // 
            this.tb_gu.BackColor = System.Drawing.SystemColors.Window;
            this.tb_gu.Location = new System.Drawing.Point(74, 68);
            this.tb_gu.Name = "tb_gu";
            this.tb_gu.Size = new System.Drawing.Size(119, 21);
            this.tb_gu.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(219, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "팩스";
            // 
            // tb_addr
            // 
            this.tb_addr.BackColor = System.Drawing.SystemColors.Window;
            this.tb_addr.Location = new System.Drawing.Point(74, 149);
            this.tb_addr.Name = "tb_addr";
            this.tb_addr.Size = new System.Drawing.Size(299, 21);
            this.tb_addr.TabIndex = 10;
            // 
            // tb_client
            // 
            this.tb_client.BackColor = System.Drawing.SystemColors.Window;
            this.tb_client.Location = new System.Drawing.Point(74, 41);
            this.tb_client.Name = "tb_client";
            this.tb_client.Size = new System.Drawing.Size(299, 21);
            this.tb_client.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(10, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "전화번호";
            // 
            // tb_work_name
            // 
            this.tb_work_name.BackColor = System.Drawing.SystemColors.Window;
            this.tb_work_name.Location = new System.Drawing.Point(74, 386);
            this.tb_work_name.Name = "tb_work_name";
            this.tb_work_name.Size = new System.Drawing.Size(299, 21);
            this.tb_work_name.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(206, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "대표자";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(10, 439);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "배송방법";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(36, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "구분";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(36, 180);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "비고";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(36, 153);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "주소";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(10, 466);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "작업방법";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(23, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "업체명";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(10, 365);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "완료일자";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(23, 390);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "작업명";
            // 
            // btn_apply
            // 
            this.btn_apply.Location = new System.Drawing.Point(24, 6);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(65, 23);
            this.btn_apply.TabIndex = 0;
            this.btn_apply.Text = "적   용";
            this.btn_apply.UseVisualStyleBackColor = true;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lbl_total);
            this.panel4.Controls.Add(this.lbl_count);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Location = new System.Drawing.Point(12, 613);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(984, 26);
            this.panel4.TabIndex = 15;
            // 
            // lbl_total
            // 
            this.lbl_total.AutoSize = true;
            this.lbl_total.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_total.Location = new System.Drawing.Point(683, 5);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(50, 13);
            this.lbl_total.TabIndex = 0;
            this.lbl_total.Text = "합계 : ";
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_count.Location = new System.Drawing.Point(357, 5);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.Size = new System.Drawing.Size(50, 13);
            this.lbl_count.TabIndex = 0;
            this.lbl_count.Text = "수량 : ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(21, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "목록 총 합계";
            // 
            // List_aggregation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1395, 651);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tb_dlv);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_clt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.combo_user);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.combo_year);
            this.Controls.Add(this.combo_state);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_print);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_lookup);
            this.Name = "List_aggregation";
            this.Text = "목록집계";
            this.Load += new System.EventHandler(this.List_aggregation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox combo_state;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_dlv;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox combo_user;
        private System.Windows.Forms.Button btn_process;
        private System.Windows.Forms.Button btn_ISBN;
        private System.Windows.Forms.Button btn_checkup;
        private System.Windows.Forms.Button btn_print;
        private System.Windows.Forms.ComboBox combo_year;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_l_n;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lbl_l_y;
        private System.Windows.Forms.TextBox tb_send_way;
        private System.Windows.Forms.TextBox tb_work_name;
        private System.Windows.Forms.Label lbl_send_n;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lbl_send_y;
        private System.Windows.Forms.Button btn_apply;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        public System.Windows.Forms.TextBox tb_clt;
        private System.Windows.Forms.RichTextBox tb_work_way;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btn_lookup;
        private System.Windows.Forms.TextBox tb_gu;
        private System.Windows.Forms.TextBox tb_client;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_boss;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_fax;
        private System.Windows.Forms.TextBox tb_tel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_addr;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox rtb_etc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker date_res_Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_res;
        private System.Windows.Forms.DataGridViewTextBoxColumn charge;
        private System.Windows.Forms.DataGridViewTextBoxColumn work_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn work_way;
        private System.Windows.Forms.DataGridViewTextBoxColumn send_way;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn stock_money;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn chk_label;
        private System.Windows.Forms.DataGridViewTextBoxColumn clt;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Chk_Column;
    }
}