﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Mac;

namespace WindowsFormsApp1.Delivery
{
    public partial class List_aggregation : Form
    {
        Helper_DB db = new Helper_DB();
        public int EditNumber;
        public string compidx;
        int row = 0;
        Main main;
        public List_aggregation(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        /////// TODO: 
        // 새로운 폼 작업 필요.
        // "목록집계등록"
        private void List_aggregation_Load(object sender, EventArgs e)
        {
            db.DBcon();

            for (int a = 0; a < 16; a++)
            {
                if (a == 2 || a == 4 || a == 5 || a == 6 || a == 15)
                    dataGridView1.Columns[a].ReadOnly = false;

                else
                    dataGridView1.Columns[a].ReadOnly = true;
            }
            string[] state = { "진행", "완료", "전체" };
            combo_state.Items.AddRange(state);
            combo_state.SelectedIndex = 0;

            #region 연도삽입
            int Years = DateTime.Now.Year;

            List<string> Year = new List<string>();
            while (Years != 2009)
            {
                string AddYear = Years.ToString();
                Year.Add(AddYear);
                Years--;
            }
            string[] years = Year.ToArray();
            combo_year.Items.AddRange(years);
            combo_year.SelectedIndex = 0;
            combo_year.Visible = false;
            #endregion

            combo_user.Items.Add("전체");
            string cmd = db.self_Made_Cmd("SELECT `name` FROM `User_Data` WHERE `affil` = '" + main.toolStripLabel2.Text + "';");
            string[] user_name = cmd.Split('|');
            for(int a = 0; a < user_name.Length; a++)
            {
                if (a == user_name.Length - 1) { break; }
                combo_user.Items.Add(user_name[a]);
            }
            combo_user.SelectedIndex = 0;
        }
        public void btn_lookup_Click(object sender, EventArgs e)   // 조회
        {
            /// idx / 목록명 / 목록일자 / 완료일자 / 담당자 / 작업명
            /// 작업방법 / 배송방법 / 합계금액 / 입고금액 / 수량
            /// 입고 / 미입고 / 목록번호 / 마크여부 / 거래처명
            string searchdb = "`idx`, `list_name`, `date`, `date_res`, `charge`, `work_name`, " +
                              "`work_way`, `send_way`, `total`, `stock_money`, `vol`, " +
                              "`stock`, `unstock`, `list_num`, `chk_marc`, `clt`";

            List<string> search_L_tbl = new List<string>();
            List<string> search_L_col = new List<string>();

            search_L_tbl.Add("comp_num");
            search_L_col.Add(compidx);

            if (combo_state.SelectedItem.ToString() != "전체") {
                search_L_tbl.Add("state");
                search_L_col.Add(combo_state.SelectedItem.ToString());
            }

            string[] searchdb1 = search_L_tbl.ToArray();
            string[] searchdb2 = search_L_col.ToArray();

            string cmd = db.More_DB_Search("Obj_List", searchdb1, searchdb2, searchdb);
            string db_res = db.DB_Send_CMD_Search(cmd);
            Made_Grid(db_res);
            Print_lbl_Total();
            dataGridView1.Focus();
        }
        #region Lookup_Click_Sub
        /// <summary>
        /// 라벨에 총 합계 출력
        /// </summary>
        private void Print_lbl_Total()
        {
            int count = 0;
            int total = 0;
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                count += Convert.ToInt32(dataGridView1.Rows[a].Cells["count"].Value.ToString());
                total += Convert.ToInt32(dataGridView1.Rows[a].Cells["total"].Value.ToString());
            }
            lbl_count.Text = string.Format("수량 : {0:#,###}", count);
            lbl_total.Text = string.Format("합계 : {0:#,###}", total);
        }
        /// <summary>
        /// 표에 값을 채우는 함수.
        /// </summary>
        /// <param name="tmp">분할을 하지않은 데이터베이스 값</param>
        private void Made_Grid(string tmp)
        {
            dataGridView1.Rows.Clear();
            string[] db_data = tmp.Split('|');
            string[] grid = { 
                "", "", "", "", "",
                "", "", "", "", "",
                "", "", "", "", "",
                "", "", "" };
            int count = 16;
            for (int a = 0; a < db_data.Length; a++)
            {
                if (a % count == 0) { grid[0] = db_data[a]; }
                if (a % count == 1) { grid[1] = db_data[a]; }
                if (a % count == 2)
                {
                    if (db_data[a].Length < 3) {
                        grid[2] = db_data[a];
                    }
                    else { grid[2] = db_data[a].Substring(0, 10); }
                }
                if (a % count == 3) {
                    if (db_data[a].Length < 3) {
                        grid[3] = db_data[a];
                    }
                    else { grid[3] = db_data[a].Substring(0, 10); }
                }
                if (a % count == 4) { grid[4] = db_data[a]; }
                if (a % count == 5) { grid[5] = db_data[a]; }
                if (a % count == 6) { grid[6] = db_data[a]; }
                if (a % count == 7) { grid[7] = db_data[a]; }
                if (a % count == 8) { grid[8] = db_data[a]; }
                if (a % count == 9) { grid[9] = db_data[a]; }
                if (a % count == 10) { grid[10] = db_data[a]; }
                if (a % count == 11) { grid[11] = db_data[a]; }
                if (a % count == 12) { grid[12] = db_data[a]; }

                if (a % count == 13) { grid[15] = db_data[a]; }
                if (a % count == 14) { grid[16] = db_data[a]; }
                if (a % count == 15) { 
                    grid[17] = db_data[a];
                    if (Grid_filter(grid))
                    {
                        dataGridView1.Rows.Add(grid);
                    }
                }
            }
            GridColorChange();
        }
        private bool Grid_filter(string[] arr)
        {
            String_Text st = new String_Text();
            string db_clt = st.GetMiddelString(arr[0], "[", "]");
            string cb_year = combo_year.SelectedItem.ToString();
            string db_year = arr[1].Substring(0, 4);
            if (!arr[0].Contains(tb_dlv.Text))
            {
                return false;
            }
            if (combo_state.SelectedIndex == 1)
            {
                if (cb_year != db_year)
                {
                    return false;
                }
            }
            if (combo_user.SelectedIndex != 0)
            {
                if (combo_user.SelectedItem.ToString() != arr[3])
                {
                    return false;
                }
            }
            if (tb_clt.Text != "")
            {
                if (tb_clt.Text == db_clt)
                {
                    return true;
                }
                return false;
            }
            return true;
        }
        /// <summary>
        /// 데이터그리드뷰 색상변경
        /// </summary>
        public void GridColorChange()
        {
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Empty;
            }
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (dataGridView1.Rows[a].Cells["chk_label"].Value.ToString() == "라벨")
                {
                    dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Yellow;
                }
            }
        }
        #endregion
        private void btn_save_Click(object sender, EventArgs e)     // 저장
        {
            string table = "Obj_List";
            string[] Edit_col = { "work_name", "work_way", "send_way", "date_res" };
            string[] Search_col = { "idx" };
            string[] Edit_Data = { "", "", "", "" };
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (dataGridView1.Rows[a].Cells["work_name"].Value.ToString() == null) { Edit_Data[0] = ""; }
                else { Edit_Data[0] = dataGridView1.Rows[a].Cells["work_name"].Value.ToString(); }

                if (dataGridView1.Rows[a].Cells["work_way"].Value.ToString() == null) { Edit_Data[1] = ""; }
                else { Edit_Data[1] = dataGridView1.Rows[a].Cells["work_way"].Value.ToString(); }

                if (dataGridView1.Rows[a].Cells["send_way"].Value.ToString() == null) { Edit_Data[2] = ""; }
                else { Edit_Data[2] = dataGridView1.Rows[a].Cells["send_way"].Value.ToString(); }

                if (dataGridView1.Rows[a].Cells["date_res"].Value.ToString() == null) { Edit_Data[3] = ""; }
                else { Edit_Data[3] = dataGridView1.Rows[a].Cells["date_res"].Value.ToString(); }

                string[] Search_Data = {
                    dataGridView1.Rows[a].Cells["idx"].Value.ToString()
                };
                string U_cmd = db.More_Update(table, Edit_col, Edit_Data, Search_col, Search_Data);
                db.DB_Send_CMD_reVoid(U_cmd);
            }
            MessageBox.Show("저장되었습니다.");
        }
        private void btn_process_Click(object sender, EventArgs e)  // 완료처리
        {
            Skill_Search_Text sst = new Skill_Search_Text();
            string value = "";
            if (sst.InputBox("출고율을 입력해주세요!", "출고율 입력", ref value) != DialogResult.OK) return;

            value = Regex.Replace(value, @"[^0-9]", "");

            if (value == "") return;

            string[] Edit_col = { "state" };
            string[] Edit_data = { "완료" };
            string[] Search_col = { "compidx", "idx" };
            string[] Search_data = { 
                compidx, 
                dataGridView1.Rows[row].Cells["idx"].Value.ToString()
            };

            string Area = "`isbn`, `isbn_import`, `book_name`, `author`, `book_comp`, `price`, `input_count`, `persent`";
            string cmd = db.More_DB_Search("Obj_List_Book", Search_col, Search_data, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            Sales(db_res, value);

            string U_cmd = db.More_Update("Obj_List_Marc", Edit_col, Edit_data, Search_col, Search_data);
            db.DB_Send_CMD_reVoid(U_cmd);
            Search_col[0] = "comp_num";
            U_cmd = db.More_Update("Obj_List", Edit_col, Edit_data, Search_col, Search_data);
            db.DB_Send_CMD_reVoid(U_cmd);

            MessageBox.Show("정상적으로 완료처리 되었습니다.");
        }
        /// <summary>
        /// 목록도서DB 내용을 들고와서 SalesDB에 출고율을 추가하여 집어넣는다.
        /// </summary>
        private void Sales(string book_list, string out_per)
        {
            string[] tmp = book_list.Split('|');
            List<string> l_book = new List<string>();
            
            for (int a = 0; a < tmp.Length; a++)
            {
                if (a % 8 == 0) { l_book.Add(tmp[a]); }     // isbn
                if (a % 8 == 1) { l_book.Add(tmp[a]); }     // isbn_import
                if (a % 8 == 2) { l_book.Add(tmp[a]); }     // book_name
                if (a % 8 == 3) { l_book.Add(tmp[a]); }     // author
                if (a % 8 == 4) { l_book.Add(tmp[a]); }     // book_comp
                if (a % 8 == 5) { l_book.Add(tmp[a]); }     // price
                if (a % 8 == 6) { l_book.Add(tmp[a]); }     // input_count
                if (a % 8 == 7) { l_book.Add(tmp[a]);       // persent
                    string[] book = db_data_filter(l_book);
                    input_sales(book, out_per);
                }
            }
        }
        private void input_sales(string[] book, string out_per)
        {
            String_Text st = new String_Text();
            DateTime date_tmp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            string date = date_tmp.ToString().Substring(0, 10);
            string clt = st.GetMiddelString(dataGridView1.Rows[row].Cells["list_name"].Value.ToString(), "[", "]");
            string dly = dataGridView1.Rows[row].Cells["list_name"].Value.ToString().Substring(dataGridView1.Rows[row].Cells["list_name"].Value.ToString().IndexOf(']') + 1);
            // 목록일자 + " " + 납품처
            string etc = dataGridView1.Rows[row].Cells["list_date"].Value.ToString() + " " + dly;

            string[] total = Cal_out_price(book[4], book[5], out_per, book[6]);

            string[] col_name = { "compidx", "date", "list_date", "list_name", "client", 
                                  "isbn", "book_name", "author", "book_comp", "price", 
                                  "count", "out_per", "total", "in_price", "out_price", "in_per", "etc" };
            string[] insert_data = { compidx, date, dataGridView1.Rows[row].Cells["list_date"].Value.ToString(), 
                dataGridView1.Rows[row].Cells["list_name"].Value.ToString(), clt, 
                book[0], book[1], book[2], book[3], book[4], 
                book[5], out_per, total[0], total[1], total[2], book[6], etc };

            string Incmd = db.DB_INSERT("Sales", col_name, insert_data);
            db.DB_Send_CMD_reVoid(Incmd);
        }
        private string[] Cal_out_price(string price_st, string count_st, string out_per_st, string in_per_st)
        {
            int price = Convert.ToInt32(price_st);
            int count = Convert.ToInt32(count_st);
            int in_per = Convert.ToInt32(in_per_st);
            int out_per = Convert.ToInt32(out_per_st);

            int total = price * count;
            double in_price_db = total * in_per / 100;
            int in_price = Convert.ToInt32(in_price_db);
            double out_price_db = total * out_per / 100;
            int out_price = Convert.ToInt32(out_price_db);

            string[] result = { total.ToString(), in_price.ToString(), out_price.ToString() };
            return result;
        }
        private string[] db_data_filter(List<string> l_book)
        {
            if (l_book[0] == "")
                l_book.RemoveAt(0);
            else if (l_book[0] != l_book[1])
                l_book.RemoveAt(1);
            string[] book = l_book.ToArray();
            return book;
        }
        int cnt = 0; int pageNo = 1;
        private void btn_print_Click(object sender, EventArgs e)    // 인쇄
        {
            printPreviewDialog1.Document = printDocument1;
            printDocument1.DefaultPageSettings.Landscape = true;
            if (printPreviewDialog1.ShowDialog() == DialogResult.Cancel)
            {
                cnt = 0;
                pageNo = 1;
            }
        }
        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            int dialogWidth = 528;  // 페이지 전체넓이 printPreviewDialog.Width

            StringFormat sf = new StringFormat();   // 컬럼 안에 있는 값들 가운데 정렬
            sf.Alignment = StringAlignment.Center;
            int width, width1;      // 시작점위치, datagrid 1개의 컬럼 가로길이
            int startWidth = 10;    // 시작 x좌표
            int startHeight = 140;  // 시작 y좌표
            int avgHeight = dataGridView1.Rows[0].Height;   // gridview 컬럼 하나의 높이
            int temp = 0;
            avgHeight = avgHeight / 3 * 4;

            e.Graphics.DrawString("제목", new Font("Arial", 20, FontStyle.Bold), Brushes.Black, dialogWidth / 2, 40);
            e.Graphics.DrawString("인쇄일 : " + DateTime.Now.ToString("yyyy/MM/dd"), new Font("Arial", 13), Brushes.Black, dialogWidth - 20, 80);
            e.Graphics.DrawString("페이지번호 : " + pageNo, new Font("Arial",13), Brushes.Black,dialogWidth-20, 100);

            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                if (!dataGridView1.Columns[i].Visible) continue;
                if (i == 0)
                {
                    width = 0;
                    width1 = this.dataGridView1.Columns[i].Width + 15;
                }
                else if (i >= 1 && i < dataGridView1.ColumnCount - 2)
                {
                    width = this.dataGridView1.Columns[i - 1].Width + 15;
                    width1 = this.dataGridView1.Columns[i].Width + 15;
                }
                else
                {
                    width = this.dataGridView1.Columns[i - 1].Width + 15;
                    width1 = this.dataGridView1.Columns[i].Width + 40;
                }
                RectangleF drawRect = new RectangleF((float)(startWidth + width) / 4 * 3, 
                    (float)startHeight, (float)width1 / 4 * 3, avgHeight);
                e.Graphics.DrawRectangle(Pens.Black, (float)(startWidth + width) / 4 * 3, 
                    (float)startHeight, (float)width1 / 4 * 3, avgHeight);
                e.Graphics.DrawString(dataGridView1.Columns[i].HeaderText, 
                    new Font("Arial", 12, FontStyle.Bold), Brushes.Black, drawRect, sf);

                startWidth += width;
            }
            startHeight += avgHeight;

            for (int i = cnt; i < dataGridView1.RowCount - 1; i++) 
            {
                startWidth = 10;
                for(int j = 0; j < dataGridView1.ColumnCount; j++) {
                    if (!dataGridView1.Columns[j].Visible) continue;
                    if (j == 0) {
                        width = 0;
                        width1 = this.dataGridView1.Columns[j].Width + 15;
                    }
                    else if (j >= 1 && j <= dataGridView1.ColumnCount - 2) {
                        width = this.dataGridView1.Columns[j - 1].Width + 15;
                        width1 = this.dataGridView1.Columns[j].Width + 15;
                    }
                    else {
                        width = this.dataGridView1.Columns[j - 1].Width + 15;
                        width1 = this.dataGridView1.Columns[j].Width + 40;
                    }
                    RectangleF drawRect = new RectangleF((float)(startWidth + width) / 4 * 3,
                        (float)startHeight, (float)width1 / 4 * 3, avgHeight);
                    e.Graphics.DrawRectangle(Pens.Black, (float)(startWidth + width) / 4 * 3,
                        (float)startHeight, (float)width1 / 4 * 3, avgHeight);
                    e.Graphics.DrawString(dataGridView1.Rows[i].Cells[j].FormattedValue.ToString(),
                        new Font("Arial", 9), Brushes.Black, drawRect, sf);
                    startWidth += width;
                }
                startHeight += avgHeight;
                temp++;
                cnt++;

                if (temp % 40 == 0)
                {
                    e.HasMorePages = true;
                    pageNo++;
                    return;
                }
            }
        }
        private void btn_apply_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[row].Cells["work_name"].Value = tb_work_name.Text;
            dataGridView1.Rows[row].Cells["work_way"].Value = tb_work_way.Text;
            dataGridView1.Rows[row].Cells["send_way"].Value = tb_send_way.Text;
            if (date_res_Value.Checked)
                dataGridView1.Rows[row].Cells["date_res"].Value = date_res_Value.Value.ToString("yyyy-MM-dd");
        }
        private void btn_checkup_Click(object sender, EventArgs e)  // 검수작업 (내부 작업필요)
        {
            List_Chk_Work chk_work = new List_Chk_Work(this);
            chk_work.TopMost = true;
            chk_work.data[0] = dataGridView1.Rows[row].Cells["list_name"].Value.ToString();
            chk_work.data[1] = compidx;
            chk_work.Show();
        }
        private void btn_ISBN_Click(object sender, EventArgs e)     // ISBN조회
        {
            Check_ISBN isbn = new Check_ISBN(this);
            string listname = dataGridView1.Rows[row].Cells["list_name"].Value.ToString();
            string l_idx = dataGridView1.Rows[row].Cells["idx"].Value.ToString();
            isbn.DataLoad(listname, l_idx);
            isbn.Show();
        }

        //////////////////////////////////////////////////////////////////////////////////////

        private void tb_clt_KeyDown(object sender, KeyEventArgs e)
        {
            string textBox = ((TextBox)sender).Name;
            switch (textBox) {
                case "tb_clt":
                    if (e.KeyCode == Keys.Enter) {
                        Commodity_Search sea = new Commodity_Search(this);
                        sea.Clinet_name = tb_clt.Text;
                        sea.Show();
                    }
                    break;
                case "tb_dlv":
                    if (e.KeyCode == Keys.Enter)
                        btn_lookup_Click(null, null);
                    break;
            }
        }
        private void combo_state_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combo_state.Text == "진행")
            {
                combo_year.Visible = false;
            }
            else if (combo_state.Text == "완료")
            {
                combo_year.Visible = true;
            }
        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            Skill_Grid skill = new Skill_Grid();
            if(e.KeyCode == Keys.Delete)
            {
                skill.DataGrid_to_Delete(sender, e);
            }

            // grid 선택시 우측란에 해당 정보로 초기화 및 최신화
            if(e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
            {
                int row = dataGridView1.CurrentRow.Index;
                infor_Update(row);
            }
        }
        private void infor_reset()
        {
            TextBox[] box = { tb_client, tb_gu, tb_boss, tb_tel, tb_fax, tb_email, tb_addr, tb_work_name, tb_send_way };
            foreach (TextBox tb in box)
                tb.Text = "";

            rtb_etc.Text = "";
            tb_work_way.Text = "";
        }
        private void infor_Update(int rowidx)
        {
            if (rowidx < 0) { return; }
            row = rowidx;
            infor_reset();
            string client = dataGridView1.Rows[rowidx].Cells["clt"].Value.ToString();
            if (client == "") return;
            client_Setting(client);
            // 작업명
            tb_work_name.Text = dataGridView1.Rows[rowidx].Cells["work_name"].Value.ToString();
            // 작업방법
            tb_work_way.Text = dataGridView1.Rows[rowidx].Cells["work_way"].Value.ToString();
            // 배송방법
            tb_send_way.Text = dataGridView1.Rows[rowidx].Cells["send_way"].Value.ToString();
            // 택배여부
            if (dataGridView1.Rows[rowidx].Cells["Column18"].Value.ToString() == "") {
                lbl_send_n.Font = new Font(this.Font, FontStyle.Bold);
                lbl_send_y.Font = new Font(this.Font, FontStyle.Regular);
            }
            else {
                lbl_send_y.Font = new Font(this.Font, FontStyle.Bold);
                lbl_send_n.Font = new Font(this.Font, FontStyle.Regular);
            }
            // 라벨여부
            if (dataGridView1.Rows[rowidx].Cells["chk_label"].Value.ToString() == "라벨") {
                lbl_l_y.Font = new Font(this.Font, FontStyle.Bold);
                lbl_l_n.Font = new Font(this.Font, FontStyle.Regular);
            }
            else { 
                lbl_l_n.Font = new Font(this.Font, FontStyle.Bold);
                lbl_l_y.Font = new Font(this.Font, FontStyle.Regular);
            }
        }
        private void client_Setting(string client) 
        {
            TextBox[] box = { tb_client, tb_gu, tb_boss, tb_tel, tb_fax, tb_email, tb_addr };
            string Area = "`c_sangho`, `c_gu`, `c_boss`, `c_tel`, `c_fax`, `c_email`, `c_addr`, `c_etc`";
            string cmd = db.DB_Select_Search(Area, "Client", "campanyidx", compidx, "c_sangho", client);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] arr = db_res.Split('|');

            for (int a = 0; a < box.Length + 1; a++)
            {
                if (a == box.Length) {
                    rtb_etc.Text = arr[a];
                    return;
                }
                box[a].Text = arr[a];
            }
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 2 || e.ColumnIndex != 4 || e.ColumnIndex != 5 || 
                e.ColumnIndex != 6 || e.ColumnIndex != 15)
            {
                List_Lookup list_l = new List_Lookup(this)
                {
                    call_base = dataGridView1.Rows[e.RowIndex].Cells["list_name"].Value.ToString()
                };
                list_l.MdiParent = main;
                list_l.WindowState = FormWindowState.Maximized;
                list_l.Show();
            }
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            row = e.RowIndex;
            infor_Update(e.RowIndex);
        }

        private void lbl_l_y_Click(object sender, EventArgs e)
        {
            if (((Label)sender).Name == "lbl_l_y") {
                lbl_l_y.Font = new Font(this.Font, FontStyle.Bold);
                lbl_l_n.Font = new Font(this.Font, FontStyle.Regular);
            }

            if (((Label)sender).Name == "lbl_l_n") {
                lbl_l_y.Font = new Font(this.Font, FontStyle.Regular);
                lbl_l_n.Font = new Font(this.Font, FontStyle.Bold);
            }

            if (((Label)sender).Name == "lbl_send_y") {
                lbl_send_y.Font = new Font(this.Font, FontStyle.Bold);
                lbl_send_n.Font = new Font(this.Font, FontStyle.Regular);
            }

            if (((Label)sender).Name == "lbl_send_n") {
                lbl_send_y.Font = new Font(this.Font, FontStyle.Regular);
                lbl_send_n.Font = new Font(this.Font, FontStyle.Bold);
            }

        }
    }
}
