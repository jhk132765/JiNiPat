﻿namespace WindowsFormsApp1.Delivery
{
    partial class Book_Lookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_idx = new System.Windows.Forms.Label();
            this.tb_isbn = new System.Windows.Forms.TextBox();
            this.tb_order_idx = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_etc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_num = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tb_order1 = new System.Windows.Forms.TextBox();
            this.tb_charge = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_total = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_pay = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_stock = new System.Windows.Forms.TextBox();
            this.tb_count = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_order_date = new System.Windows.Forms.TextBox();
            this.tb_book_comp = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_author = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_book_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_date = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_stock = new System.Windows.Forms.Button();
            this.btn_order_ccl = new System.Windows.Forms.Button();
            this.btn_order = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_printLine = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tb_List_name = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_date_res = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_unstock = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_idx);
            this.panel1.Controls.Add(this.tb_isbn);
            this.panel1.Controls.Add(this.tb_order_idx);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.tb_etc);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.tb_num);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.tb_order1);
            this.panel1.Controls.Add(this.tb_charge);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.tb_total);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.tb_pay);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tb_stock);
            this.panel1.Controls.Add(this.tb_count);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tb_order_date);
            this.panel1.Controls.Add(this.tb_book_comp);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tb_author);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tb_book_name);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(9, 106);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(648, 199);
            this.panel1.TabIndex = 0;
            // 
            // lbl_idx
            // 
            this.lbl_idx.AutoSize = true;
            this.lbl_idx.Location = new System.Drawing.Point(212, 10);
            this.lbl_idx.Name = "lbl_idx";
            this.lbl_idx.Size = new System.Drawing.Size(62, 12);
            this.lbl_idx.TabIndex = 2;
            this.lbl_idx.Text = "idx 들어감";
            // 
            // tb_isbn
            // 
            this.tb_isbn.Location = new System.Drawing.Point(414, 7);
            this.tb_isbn.Name = "tb_isbn";
            this.tb_isbn.Size = new System.Drawing.Size(215, 21);
            this.tb_isbn.TabIndex = 1;
            this.tb_isbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // tb_order_idx
            // 
            this.tb_order_idx.BackColor = System.Drawing.SystemColors.Control;
            this.tb_order_idx.Enabled = false;
            this.tb_order_idx.Location = new System.Drawing.Point(214, 168);
            this.tb_order_idx.Name = "tb_order_idx";
            this.tb_order_idx.Size = new System.Drawing.Size(43, 21);
            this.tb_order_idx.TabIndex = 1;
            this.tb_order_idx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label9.Location = new System.Drawing.Point(374, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "ISBN";
            // 
            // tb_etc
            // 
            this.tb_etc.Location = new System.Drawing.Point(69, 141);
            this.tb_etc.Name = "tb_etc";
            this.tb_etc.Size = new System.Drawing.Size(560, 21);
            this.tb_etc.TabIndex = 1;
            this.tb_etc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label10.Location = new System.Drawing.Point(22, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "번호";
            // 
            // tb_num
            // 
            this.tb_num.BackColor = System.Drawing.SystemColors.Control;
            this.tb_num.Enabled = false;
            this.tb_num.Location = new System.Drawing.Point(69, 6);
            this.tb_num.Name = "tb_num";
            this.tb_num.Size = new System.Drawing.Size(124, 21);
            this.tb_num.TabIndex = 1;
            this.tb_num.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label17.Location = new System.Drawing.Point(22, 145);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "비고";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label20.Location = new System.Drawing.Point(14, 172);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "주문처";
            // 
            // tb_order1
            // 
            this.tb_order1.Location = new System.Drawing.Point(69, 168);
            this.tb_order1.Name = "tb_order1";
            this.tb_order1.Size = new System.Drawing.Size(139, 21);
            this.tb_order1.TabIndex = 1;
            this.tb_order1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // tb_charge
            // 
            this.tb_charge.BackColor = System.Drawing.SystemColors.Control;
            this.tb_charge.Enabled = false;
            this.tb_charge.Location = new System.Drawing.Point(487, 87);
            this.tb_charge.Name = "tb_charge";
            this.tb_charge.Size = new System.Drawing.Size(142, 21);
            this.tb_charge.TabIndex = 1;
            this.tb_charge.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label12.Location = new System.Drawing.Point(430, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "담당자";
            // 
            // tb_total
            // 
            this.tb_total.Location = new System.Drawing.Point(487, 114);
            this.tb_total.Name = "tb_total";
            this.tb_total.Size = new System.Drawing.Size(142, 21);
            this.tb_total.TabIndex = 1;
            this.tb_total.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.Location = new System.Drawing.Point(424, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "합계금액";
            // 
            // tb_pay
            // 
            this.tb_pay.Location = new System.Drawing.Point(69, 114);
            this.tb_pay.Name = "tb_pay";
            this.tb_pay.Size = new System.Drawing.Size(124, 21);
            this.tb_pay.TabIndex = 1;
            this.tb_pay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label5.Location = new System.Drawing.Point(22, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "정가";
            // 
            // tb_stock
            // 
            this.tb_stock.Location = new System.Drawing.Point(357, 114);
            this.tb_stock.Name = "tb_stock";
            this.tb_stock.Size = new System.Drawing.Size(57, 21);
            this.tb_stock.TabIndex = 1;
            this.tb_stock.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // tb_count
            // 
            this.tb_count.Location = new System.Drawing.Point(242, 114);
            this.tb_count.Name = "tb_count";
            this.tb_count.Size = new System.Drawing.Size(57, 21);
            this.tb_count.TabIndex = 1;
            this.tb_count.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label13.Location = new System.Drawing.Point(314, 118);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "입고수";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label22.Location = new System.Drawing.Point(349, 172);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 0;
            this.label22.Text = "주문일자";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label4.Location = new System.Drawing.Point(211, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "수량";
            // 
            // tb_order_date
            // 
            this.tb_order_date.BackColor = System.Drawing.SystemColors.Control;
            this.tb_order_date.Enabled = false;
            this.tb_order_date.Location = new System.Drawing.Point(409, 168);
            this.tb_order_date.Name = "tb_order_date";
            this.tb_order_date.Size = new System.Drawing.Size(220, 21);
            this.tb_order_date.TabIndex = 1;
            this.tb_order_date.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // tb_book_comp
            // 
            this.tb_book_comp.Location = new System.Drawing.Point(69, 87);
            this.tb_book_comp.Name = "tb_book_comp";
            this.tb_book_comp.Size = new System.Drawing.Size(345, 21);
            this.tb_book_comp.TabIndex = 1;
            this.tb_book_comp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label3.Location = new System.Drawing.Point(16, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "출판사";
            // 
            // tb_author
            // 
            this.tb_author.Location = new System.Drawing.Point(69, 60);
            this.tb_author.Name = "tb_author";
            this.tb_author.Size = new System.Drawing.Size(345, 21);
            this.tb_author.TabIndex = 1;
            this.tb_author.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(22, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "저자";
            // 
            // tb_book_name
            // 
            this.tb_book_name.Location = new System.Drawing.Point(69, 33);
            this.tb_book_name.Name = "tb_book_name";
            this.tb_book_name.Size = new System.Drawing.Size(560, 21);
            this.tb_book_name.TabIndex = 1;
            this.tb_book_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(16, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "도서명";
            // 
            // tb_date
            // 
            this.tb_date.Enabled = false;
            this.tb_date.Location = new System.Drawing.Point(69, 34);
            this.tb_date.Name = "tb_date";
            this.tb_date.Size = new System.Drawing.Size(230, 21);
            this.tb_date.TabIndex = 1;
            this.tb_date.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label7.Location = new System.Drawing.Point(10, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "목록일자";
            // 
            // btn_stock
            // 
            this.btn_stock.Location = new System.Drawing.Point(90, 6);
            this.btn_stock.Name = "btn_stock";
            this.btn_stock.Size = new System.Drawing.Size(75, 23);
            this.btn_stock.TabIndex = 3;
            this.btn_stock.Text = "입고처리";
            this.btn_stock.UseVisualStyleBackColor = true;
            this.btn_stock.Click += new System.EventHandler(this.btn_stock_Click);
            // 
            // btn_order_ccl
            // 
            this.btn_order_ccl.Location = new System.Drawing.Point(171, 6);
            this.btn_order_ccl.Name = "btn_order_ccl";
            this.btn_order_ccl.Size = new System.Drawing.Size(75, 23);
            this.btn_order_ccl.TabIndex = 3;
            this.btn_order_ccl.Text = "주문취소";
            this.btn_order_ccl.UseVisualStyleBackColor = true;
            this.btn_order_ccl.Click += new System.EventHandler(this.btn_order_ccl_Click);
            // 
            // btn_order
            // 
            this.btn_order.Location = new System.Drawing.Point(252, 6);
            this.btn_order.Name = "btn_order";
            this.btn_order.Size = new System.Drawing.Size(75, 23);
            this.btn_order.TabIndex = 3;
            this.btn_order.Text = "주문처리";
            this.btn_order.UseVisualStyleBackColor = true;
            this.btn_order.Click += new System.EventHandler(this.btn_order_Click);
            // 
            // btn_close
            // 
            this.btn_close.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_close.Location = new System.Drawing.Point(564, 6);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 3;
            this.btn_close.Text = "닫    기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_save
            // 
            this.btn_save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_save.Location = new System.Drawing.Point(483, 6);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 3;
            this.btn_save.Text = "저    장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_printLine
            // 
            this.btn_printLine.Location = new System.Drawing.Point(333, 6);
            this.btn_printLine.Name = "btn_printLine";
            this.btn_printLine.Size = new System.Drawing.Size(75, 23);
            this.btn_printLine.TabIndex = 3;
            this.btn_printLine.Text = "띠지출력";
            this.btn_printLine.UseVisualStyleBackColor = true;
            this.btn_printLine.Visible = false;
            this.btn_printLine.Click += new System.EventHandler(this.btn_printLine_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tb_List_name);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.tb_date_res);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.tb_date);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(9, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(648, 64);
            this.panel2.TabIndex = 4;
            // 
            // tb_List_name
            // 
            this.tb_List_name.Enabled = false;
            this.tb_List_name.Location = new System.Drawing.Point(69, 7);
            this.tb_List_name.Name = "tb_List_name";
            this.tb_List_name.Size = new System.Drawing.Size(560, 21);
            this.tb_List_name.TabIndex = 1;
            this.tb_List_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label11.Location = new System.Drawing.Point(16, 11);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "목록명";
            // 
            // tb_date_res
            // 
            this.tb_date_res.Enabled = false;
            this.tb_date_res.Location = new System.Drawing.Point(399, 34);
            this.tb_date_res.Name = "tb_date_res";
            this.tb_date_res.Size = new System.Drawing.Size(230, 21);
            this.tb_date_res.TabIndex = 1;
            this.tb_date_res.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label8.Location = new System.Drawing.Point(340, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "완료일자";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(9, 311);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 15;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(648, 190);
            this.dataGridView1.TabIndex = 5;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "처리일자";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "구분";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "거래처명";
            this.Column3.Name = "Column3";
            this.Column3.Width = 250;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "단가";
            this.Column4.Name = "Column4";
            this.Column4.Width = 80;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "수량";
            this.Column5.Name = "Column5";
            this.Column5.Width = 40;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "%";
            this.Column6.Name = "Column6";
            this.Column6.Width = 40;
            // 
            // btn_unstock
            // 
            this.btn_unstock.Location = new System.Drawing.Point(9, 6);
            this.btn_unstock.Name = "btn_unstock";
            this.btn_unstock.Size = new System.Drawing.Size(75, 23);
            this.btn_unstock.TabIndex = 6;
            this.btn_unstock.Text = "미입고처리";
            this.btn_unstock.UseVisualStyleBackColor = true;
            this.btn_unstock.Click += new System.EventHandler(this.btn_unstock_Click);
            // 
            // Book_Lookup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 510);
            this.Controls.Add(this.btn_unstock);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_printLine);
            this.Controls.Add(this.btn_order);
            this.Controls.Add(this.btn_order_ccl);
            this.Controls.Add(this.btn_stock);
            this.Controls.Add(this.panel1);
            this.Name = "Book_Lookup";
            this.Text = "도서 정보";
            this.Load += new System.EventHandler(this.Book_Lookup_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Book_Lookup_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tb_isbn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_etc;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tb_charge;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_num;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_date;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_total;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_pay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_count;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_book_comp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_author;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_book_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_order_idx;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tb_order1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tb_order_date;
        private System.Windows.Forms.Button btn_stock;
        private System.Windows.Forms.Button btn_order_ccl;
        private System.Windows.Forms.Button btn_order;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_printLine;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tb_date_res;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_List_name;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.TextBox tb_stock;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbl_idx;
        private System.Windows.Forms.Button btn_unstock;
    }
}