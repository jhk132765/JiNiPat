﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Account;
using WindowsFormsApp1.DLS;
using WindowsFormsApp1.Home;
using WindowsFormsApp1.회계;

namespace WindowsFormsApp1.Delivery
{
    public partial class Commodity_Search : Form
    {
        public string Clinet_name { get; internal set; }    // 거래처 텍스트박스 내용
        Commodity_registration com;
        Commodity_Edit edit;
        Purchase pur;
        List_aggregation la;
        Sales_Input si;
        Sales_Lookup sb;
        Sales_In_Pay sip;
        List_Lookup ll;
        Mac.DLS_Copy dc;
        School_Lookup sl;

        public Commodity_Search(Purchase _pur)
        {
            InitializeComponent();
            pur = _pur;
        }
        public Commodity_Search(Commodity_Edit _edit)
        {
            InitializeComponent();
            edit = _edit;
        }
        public Commodity_Search(Commodity_registration _com)
        {
            InitializeComponent();
            com = _com;
        }
        public Commodity_Search(List_aggregation _la)
        {
            InitializeComponent();
            la = _la;
        }
        public Commodity_Search(Sales_Input _si)
        {
            InitializeComponent();
            si = _si;
        }
        public Commodity_Search(Sales_Lookup _sb)
        {
            InitializeComponent();
            sb = _sb;
        }
        public Commodity_Search(Sales_In_Pay _sip)
        {
            InitializeComponent();
            sip = _sip;
        }
        public Commodity_Search(List_Lookup _ll)
        {
            InitializeComponent();
            ll = _ll;
        }
        public Commodity_Search(Mac.DLS_Copy _dc)
        {
            InitializeComponent();
            dc = _dc;
        }
        public Commodity_Search(School_Lookup _sl)
        {
            InitializeComponent();
            sl = _sl;
        }

        #region 거래처 가져오는 폼의 코드

        private void Commodity_Sub_Load(object sender, EventArgs e)
        {
            string compidx = Set_compidx();
            Helper_DB db = new Helper_DB();
            string Area = "`c_sangho`, `c_boss`, `c_tel`, `c_man`, `c_mantel`, `c_dlsArea`, `c_dlsID`, `c_dlsPW`";
            db.DBcon();
            string dbcmd;
            if (dc != null || sl != null)
                dbcmd = String.Format("SELECT {0} FROM `Client` WHERE campanyidx = {1} AND c_sangho LIKE \"%{2}%\" AND c_gu LIKE \"%학교%\" OR c_gu = \"유치원\";", Area, compidx, Clinet_name);
            else
                dbcmd = db.DB_Contains("Client", compidx, "c_sangho", Clinet_name, Area);
            string dbcon = db.DB_Send_CMD_Search(dbcmd);
            string[] res = dbcon.Split('|');
            string[] grid = { "", "", "", "", "", "", "", "" };
            for(int a = 0; a < res.Length; a++)
            {
                if (a % 8 == 0) { grid[0] = res[a]; }
                if (a % 8 == 1) { grid[1] = res[a]; }
                if (a % 8 == 2) { grid[2] = res[a]; }
                if (a % 8 == 3) { grid[3] = res[a]; }
                if (a % 8 == 4) { grid[4] = res[a]; }
                if (a % 8 == 5) { grid[5] = res[a]; }
                if (a % 8 == 6) { grid[6] = res[a]; }
                if (a % 8 == 7) { grid[7] = res[a];
                    dataGridView1.Rows.Add(grid);
                }
            }
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string value = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();

            Set_data(value, e.RowIndex);

            Close();
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                int gridIdx = dataGridView1.CurrentRow.Index;
                string value = "";
                if (dataGridView1.Rows[gridIdx].Cells[0].Value != null)
                {
                    value = dataGridView1.Rows[gridIdx].Cells[0].Value.ToString();
                }
                else { value = ""; }

                Set_data(value, gridIdx);

                Close();
            }
            if (e.KeyCode == Keys.Escape) {
                Close();
            }
        }
        private void Commodity_Search_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (pur != null) {
                pur.Made_Grid(pur.tb_clt.Text, pur.tb_clt.Name);
            }
        }
        #endregion

        /// 여기 밑으로만 건들 것.

        /// <summary>
        /// 회사 인덱스 설정
        /// </summary>
        /// <returns></returns>
        private string Set_compidx()
        {
            string compidx = "";

            if (edit != null) { edit.New_Clit.Text = ""; compidx = edit.compidx; }
            if (com != null) { com.tb_clt1.Text = ""; compidx = com.comp_idx; }
            if (pur != null) { pur.tb_clt.Text = ""; compidx = pur.compidx; }
            if (la != null) { la.tb_clt.Text = ""; compidx = la.compidx; }
            if (si != null) { si.tb_clt.Text = ""; compidx = si.compidx; }
            if (sb != null) { sb.tb_clt.Text = ""; compidx = sb.compidx; }
            if (sip != null) { sip.tb_clt.Text = ""; compidx = sip.compidx; }
            if (ll != null) { ll.tb_clt.Text = ""; compidx = ll.compidx; }
            if (dc != null) { dc.tb_SearchClient.Text = ""; compidx = dc.compidx; }
            if (sl != null) { sl.tb_SearchClient.Text = ""; compidx = sl.compidx; }

            return compidx;
        }

        /// <summary>
        /// 검색결과 원하는 창에 표출
        /// </summary>
        /// <param name="value">검색 결과</param>
        /// <param name="idx">선택 인덱스</param>
        private void Set_data(string value, int idx)
        {
            if (edit != null) { edit.New_Clit.Text = value; }
            if (com != null) { com.tb_clt1.Text = value; }
            if (pur != null) { pur.tb_clt.Text = value; }
            if (la != null) { la.tb_clt.Text = value; la.btn_lookup_Click(null, null); }
            if (si != null) { si.tb_clt.Text = value; si.lbl_tel.Text = dataGridView1.Rows[idx].Cells[2].Value.ToString(); }
            if (sb != null) { sb.tb_clt.Text = value; sb.btn_Lookup_Click(null, null); }
            if (sip != null) { sip.tb_clt.Text = value; }
            if (ll != null) { ll.tb_clt.Text = value; }
            if (dc != null) { 
                dc.tb_SearchClient.Text = value;
                dc.lbl_Client.Text = value;
                dc.lbl_ID.Text = dataGridView1.Rows[idx].Cells["DLS_ID"].Value.ToString();
                dc.lbl_PW.Text = dataGridView1.Rows[idx].Cells["DLS_PW"].Value.ToString();
                dc.lbl_Area.Text = dataGridView1.Rows[idx].Cells["DLS_Area"].Value.ToString();
                dc.SetArea(dataGridView1.Rows[idx].Cells["DLS_Area"].Value.ToString(), true);
            }
            if (sl != null) { 
                sl.tb_SearchClient.Text = value;
                sl.lbl_Client.Text = value;
                sl.lbl_ID.Text = dataGridView1.Rows[idx].Cells["DLS_ID"].Value.ToString();
                sl.lbl_PW.Text = dataGridView1.Rows[idx].Cells["DLS_PW"].Value.ToString();
                sl.lbl_Area.Text = dataGridView1.Rows[idx].Cells["DLS_Area"].Value.ToString();
                sl.SetArea(dataGridView1.Rows[idx].Cells["DLS_Area"].Value.ToString(), true);
            }
        }
    }
}
