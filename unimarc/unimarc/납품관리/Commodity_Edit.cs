﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Delivery
{
    public partial class Commodity_Edit : Form
    {
        Commodity_registration com;
        Helper_DB DB = new Helper_DB();
        public string compidx;
        public Commodity_Edit(Commodity_registration _com)
        {
            InitializeComponent();
            com = _com;
        }

        private void Commodity_Edit_Load(object sender, EventArgs e)
        {
            DB.DBcon();
            Old_Date.Text= com.dataGridView2.Rows[com.EditNumber].Cells["list_date"].Value.ToString();
            Old_Clit.Text= com.dataGridView2.Rows[com.EditNumber].Cells["pur"].Value.ToString();
            Old_Dlv.Text = com.dataGridView2.Rows[com.EditNumber].Cells["dly"].Value.ToString();
            Old_User.Text= com.dataGridView2.Rows[com.EditNumber].Cells["charge"].Value.ToString();
            Old_Name.Text = com.dataGridView2.Rows[com.EditNumber].Cells["list_name"].Value.ToString();

            New_Date.Value = DateTime.Parse(Old_Date.Text);
            New_Clit.Text = Old_Clit.Text;
            New_Dlv.Text = Old_Dlv.Text;
            New_User.Text = Old_User.Text;
            New_Name.Text = Old_Name.Text;

            Old_Date.Enabled = false;
            Old_Clit.Enabled = false;
            Old_Dlv.Enabled = false;
            Old_User.Enabled = false;
            Old_Name.Enabled = false;

            compidx = com.comp_idx;
        }
        private void btn_OK_Click(object sender, EventArgs e)
        {
            /* 변경되어야함
             * Obj_List = 목록일자, 목록명, 납품목록, 거래처, 담당자
             * Obj_List_Book = 목록일자, 목록명
             * Obj_List_Marc = connect_data, 목록명, 담당자
             */
            string date = New_Date.Value.ToString().Substring(0,10);

            string[] book_search_col = { "compidx", "date", "list_name" };
            string[] book_search_data = { compidx, Old_Date.Text, Old_Name.Text };

            string[] book_col = { "date", "list_name" };
            string[] book_data = { date, New_Name.Text };

            string U_cmd = DB.More_Update("Obj_List_Book", book_col, book_data, book_search_col, book_search_data);
            DB.DB_Send_CMD_reVoid(U_cmd);

            ////////////////////////////////////////

            string[] list_search_col = { "comp_num", "date", "list_name", "clt", "dly", "charge" };
            string[] list_search_data = { compidx, Old_Date.Text, Old_Name.Text, Old_Clit.Text, Old_Dlv.Text, Old_User.Text };

            string[] list_col = { "date", "clt", "dly", "charge", "list_name" };
            string[] list_data = { date, New_Clit.Text, New_Dlv.Text, New_User.Text, New_Name.Text };

            U_cmd = DB.More_Update("Obj_List", list_col, list_data, list_search_col, list_search_data);
            DB.DB_Send_CMD_reVoid(U_cmd);

            Close();
        }
        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void New_Clit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Commodity_Search sea = new Commodity_Search(this);
                sea.Clinet_name = New_Clit.Text;
                sea.Show();
            }
        }
    }
}
