﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Delivery
{
    public partial class List_Chk_Work : Form
    {
        Helper_DB db = new Helper_DB();
        List_aggregation list_a;
        public string[] data = { "", "" };
        int grididx = 0;
        public List_Chk_Work(List_aggregation list_)
        {
            InitializeComponent();
            list_a = list_;
        }
        public List_Chk_Work()
        {
            InitializeComponent();
        }
        private void List_Chk_Work_Load(object sender, EventArgs e)
        {
            string[] combodata = { "전체", "미검수", "검수" };
            cb_chk_stat.Items.AddRange(combodata);
            cb_chk_stat.SelectedIndex = 0;

            string[] readtable = { "list_name", "comp_num" };
            string[] searchtable = { "list_name", "compidx" };
            string[] searchdata = { data[0], data[1] };
            string readdata = "`clt`,`dly`,`date`";
            string getdata = "`header`, `num`, `isbn`, `book_name`, `author`, " +
                             "`book_comp`, `count`, `pay`, `total`, `stat`, " +
                             "`etc`, `chk_date`";
            db.DBcon();
            string cmd = db.More_DB_Search("Obj_List",readtable,searchdata,readdata);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] load = db_res.Split('|');
            tb_clt.Text = load[0];
            tb_dlv.Text = load[1];
            tb_date.Text = load[2];
            richTextBox1.Text = db_res;
            cmd = db.DB_Select_Search(getdata, "Obj_List_Book", 
                searchtable[0], searchdata[0], searchtable[1], searchdata[1]);
            db_res = db.DB_Send_CMD_Search(cmd);
            Made_Grid(db_res);
        }
        public int stack = 1;
        private void btn_chk_Click(object sender, EventArgs e)
        {
            lbl_isbn.Text = tb_ISBN.Text;
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (tb_ISBN.Text == dataGridView1.Rows[a].Cells["isbn"].Value.ToString()) {
                    int GridCount = Convert.ToInt32(dataGridView1.Rows[a].Cells["vol"].Value.ToString());
                    lbl_Count.Text = string.Format("수량 : {0}/{1}", stack, GridCount);
                    if (stack != GridCount) {
                        stack++;
                    }
                    else {
                        dataGridView1.Rows[a].Cells["date"].Value = DateTime.Now.ToString("G");
                        dataGridView1.Rows[a].Cells["stat"].Value = "검수";

                        GridColorChange();
                        stack = 1;
                        tb_ISBN.Text = "";
                    }
                    try
                    {
                        string isbn = dataGridView1.Rows[a].Cells["isbn"].Value.ToString();
                        Show_Image(isbn);
                    }
                    catch { }
                }
            }
        }
        private void Show_Image(string isbn)
        {
            string isbn3 = isbn.Substring(isbn.Length - 3, 3);
            pictureBox1.ImageLocation = string.Format("http://image.kyobobook.co.kr/images/book/xlarge/{0}/x{1}.jpg", isbn3, isbn);
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                string[] edit_col = { "chk_date", "stat" };
                string[] edit_data = { dataGridView1.Rows[a].Cells["date"].Value.ToString(), 
                                       dataGridView1.Rows[a].Cells["stat"].Value.ToString() };
                string[] search_col = { "list_name", "compidx", "header", "num", "book_name" };
                string[] search_data = { data[0], data[1], 
                    dataGridView1.Rows[a].Cells["header"].Value.ToString(), 
                    dataGridView1.Rows[a].Cells["num"].Value.ToString(), 
                    dataGridView1.Rows[a].Cells["book_name"].Value.ToString() };

                string U_cmd = db.More_Update("Obj_List_Book", edit_col, edit_data, search_col, search_data);
                db.DB_Send_CMD_reVoid(U_cmd);
            }
        }
        /// <summary>
        /// 데이터그리드뷰 색상변경
        /// </summary>
        public void GridColorChange()
        {
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Empty;
            }
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (dataGridView1.Rows[a].Cells["stat"].Value.ToString() == "검수")
                {
                    dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Yellow;
                }
            }
        }
        private void btn_Lookup_Click(object sender, EventArgs e)       // 조회버튼
        {
            string comboValue = cb_chk_stat.Text;
            string[] searchtable = { "list_name", "compidx", "stat" };
            string[] searchdata = { data[0], data[1], comboValue };
            string getdata = "`header`, `num`, `isbn`, `book_name`, `author`, " +
                             "`book_comp`, `count`, `pay`, `total`, `stat`, " +
                             "`etc`, `chk_date`";
            string cmd;
            if(comboValue == "전체") {
                cmd = db.DB_Select_Search(getdata, "Obj_List_Book",
                    searchtable[0], searchdata[0], searchtable[1], searchdata[1]);
            }
            else {
                cmd = db.More_DB_Search("Obj_List_Book",
                    searchtable, searchdata, getdata);
            }
            string db_res = db.DB_Send_CMD_Search(cmd);
            Made_Grid(db_res);
        }
        private void Made_Grid(String Value)
        {
            dataGridView1.Rows.Clear();
            string[] db_data = Value.Split('|');
            string[] grid = { "", "", "", "", "",
                              "", "", "", "", "", 
                              "", ""};
            /*
             * 번호, 도서명, ISBN, 도서명, 저자
             * 출판사, 수량, 단가, 합계, 상태
             * 비고, 검수시간
            */
            int ea = 0;
            for(int a = 0; a < db_data.Length; a++)
            {
                if (ea == 11) { grid[ea] = db_data[a]; dataGridView1.Rows.Add(grid); ea = 0; }
                if (a % 12 == ea) { grid[ea] = db_data[a]; ea++; }
            }
            GridColorChange();
        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Skill_Grid skill = new Skill_Grid();
            skill.Print_Grid_Num(sender, e);
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btn_chk_Click(null, null);
            }
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Show_Image(dataGridView1.Rows[e.RowIndex].Cells["isbn"].Value.ToString());
            grididx = e.RowIndex;
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Up)
                grididx--;

            if (e.KeyCode == Keys.Down)
                grididx++;
        }
    }
}
