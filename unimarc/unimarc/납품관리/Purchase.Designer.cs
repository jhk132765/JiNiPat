﻿namespace WindowsFormsApp1.Delivery
{
    partial class Purchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_clt = new System.Windows.Forms.TextBox();
            this.tb_dly = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_order = new System.Windows.Forms.TextBox();
            this.chk_order_book = new System.Windows.Forms.CheckBox();
            this.chk_stock = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_book_name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_book_comp = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_persent = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_grid = new System.Windows.Forms.Label();
            this.lbl_count = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tb_isbn = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.DateTimePicker();
            this.endTime = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_search_book_name = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_search_book_comp = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cb_charge = new System.Windows.Forms.ComboBox();
            this.btn_lookup = new System.Windows.Forms.Button();
            this.lbl_grid2 = new System.Windows.Forms.Label();
            this.lbl_count2 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.isbn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pay3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.persent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.list_name3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Order3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.load_tmplist = new System.Windows.Forms.Button();
            this.lbl_pay = new System.Windows.Forms.Label();
            this.lbl_total3 = new System.Windows.Forms.Label();
            this.lbl_grid3 = new System.Windows.Forms.Label();
            this.lbl_inven_count = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.idx1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.list_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.import = new System.Windows.Forms.DataGridViewButtonColumn();
            this.isbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.order_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idx2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Book_name2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Book_comp2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pay2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.list_name2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.edasd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.order2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.isbn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "거래처명";
            // 
            // tb_clt
            // 
            this.tb_clt.Location = new System.Drawing.Point(61, 6);
            this.tb_clt.Name = "tb_clt";
            this.tb_clt.Size = new System.Drawing.Size(100, 21);
            this.tb_clt.TabIndex = 8;
            this.tb_clt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tb_dly
            // 
            this.tb_dly.Location = new System.Drawing.Point(222, 6);
            this.tb_dly.Name = "tb_dly";
            this.tb_dly.Size = new System.Drawing.Size(100, 21);
            this.tb_dly.TabIndex = 7;
            this.tb_dly.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(168, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "납품처명";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(596, 6);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowCheckBox = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(100, 21);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(565, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "일자";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(338, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "주문처";
            // 
            // tb_order
            // 
            this.tb_order.Location = new System.Drawing.Point(381, 6);
            this.tb_order.Name = "tb_order";
            this.tb_order.Size = new System.Drawing.Size(145, 21);
            this.tb_order.TabIndex = 13;
            this.tb_order.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // chk_order_book
            // 
            this.chk_order_book.AutoSize = true;
            this.chk_order_book.Location = new System.Drawing.Point(10, 3);
            this.chk_order_book.Name = "chk_order_book";
            this.chk_order_book.Size = new System.Drawing.Size(60, 28);
            this.chk_order_book.TabIndex = 14;
            this.chk_order_book.Text = "주문한\n도서만";
            this.chk_order_book.UseVisualStyleBackColor = true;
            // 
            // chk_stock
            // 
            this.chk_stock.AutoSize = true;
            this.chk_stock.Location = new System.Drawing.Point(90, 3);
            this.chk_stock.Name = "chk_stock";
            this.chk_stock.Size = new System.Drawing.Size(84, 28);
            this.chk_stock.TabIndex = 15;
            this.chk_stock.Text = "재고有도서\n재고가감";
            this.chk_stock.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(348, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 16;
            this.label6.Text = "도서명";
            // 
            // tb_book_name
            // 
            this.tb_book_name.Location = new System.Drawing.Point(391, 5);
            this.tb_book_name.Name = "tb_book_name";
            this.tb_book_name.Size = new System.Drawing.Size(219, 21);
            this.tb_book_name.TabIndex = 17;
            this.tb_book_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(620, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 19;
            this.label7.Text = "출판사";
            // 
            // tb_book_comp
            // 
            this.tb_book_comp.Location = new System.Drawing.Point(663, 5);
            this.tb_book_comp.Name = "tb_book_comp";
            this.tb_book_comp.Size = new System.Drawing.Size(167, 21);
            this.tb_book_comp.TabIndex = 20;
            this.tb_book_comp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx1,
            this.num,
            this.count,
            this.book_name,
            this.book_comp,
            this.author,
            this.pay,
            this.total,
            this.list_name,
            this.etc,
            this.order,
            this.import,
            this.isbn,
            this.order_date});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(9, 82);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersWidth = 21;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1192, 218);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(731, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 25;
            this.label8.Text = "매입률";
            // 
            // tb_persent
            // 
            this.tb_persent.Location = new System.Drawing.Point(774, 6);
            this.tb_persent.Name = "tb_persent";
            this.tb_persent.Size = new System.Drawing.Size(52, 21);
            this.tb_persent.TabIndex = 26;
            this.tb_persent.Text = "60";
            this.tb_persent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_persent_KeyPress);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(1020, 4);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 27;
            this.btn_save.Text = "저    장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tb_clt);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_save);
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tb_persent);
            this.panel1.Controls.Add(this.tb_dly);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tb_order);
            this.panel1.Location = new System.Drawing.Point(9, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1192, 33);
            this.panel1.TabIndex = 29;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(1111, 4);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 30;
            this.btn_close.Text = "닫   기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(907, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 24);
            this.label3.TabIndex = 25;
            this.label3.Text = "저장버튼을 눌러야\r\nDB에 적용됨!";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbl_grid);
            this.panel2.Controls.Add(this.lbl_count);
            this.panel2.Controls.Add(this.chk_order_book);
            this.panel2.Controls.Add(this.chk_stock);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.tb_isbn);
            this.panel2.Controls.Add(this.tb_book_name);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.tb_book_comp);
            this.panel2.Location = new System.Drawing.Point(9, 45);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1192, 33);
            this.panel2.TabIndex = 29;
            // 
            // lbl_grid
            // 
            this.lbl_grid.AutoSize = true;
            this.lbl_grid.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_grid.Location = new System.Drawing.Point(853, 10);
            this.lbl_grid.Name = "lbl_grid";
            this.lbl_grid.Size = new System.Drawing.Size(71, 12);
            this.lbl_grid.TabIndex = 31;
            this.lbl_grid.Text = "목록 수 : 0";
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_count.Location = new System.Drawing.Point(990, 10);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.Size = new System.Drawing.Size(58, 12);
            this.lbl_count.TabIndex = 31;
            this.lbl_count.Text = "권 수 : 0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(181, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 12);
            this.label18.TabIndex = 16;
            this.label18.Text = "ISBN";
            // 
            // tb_isbn
            // 
            this.tb_isbn.Location = new System.Drawing.Point(216, 5);
            this.tb_isbn.Name = "tb_isbn";
            this.tb_isbn.Size = new System.Drawing.Size(116, 21);
            this.tb_isbn.TabIndex = 17;
            this.tb_isbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(9, 310);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "입고List";
            // 
            // startTime
            // 
            this.startTime.CustomFormat = "yyyy-MM-dd";
            this.startTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startTime.Location = new System.Drawing.Point(81, 306);
            this.startTime.Name = "startTime";
            this.startTime.Size = new System.Drawing.Size(85, 21);
            this.startTime.TabIndex = 10;
            // 
            // endTime
            // 
            this.endTime.CustomFormat = "yyyy-MM-dd";
            this.endTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endTime.Location = new System.Drawing.Point(184, 306);
            this.endTime.Name = "endTime";
            this.endTime.Size = new System.Drawing.Size(85, 21);
            this.endTime.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(168, 310);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 12);
            this.label10.TabIndex = 25;
            this.label10.Text = "~";
            // 
            // tb_search_book_name
            // 
            this.tb_search_book_name.Location = new System.Drawing.Point(330, 306);
            this.tb_search_book_name.Name = "tb_search_book_name";
            this.tb_search_book_name.Size = new System.Drawing.Size(145, 21);
            this.tb_search_book_name.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(287, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 12;
            this.label11.Text = "도서명";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(488, 310);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 12;
            this.label12.Text = "출판사";
            // 
            // tb_search_book_comp
            // 
            this.tb_search_book_comp.Location = new System.Drawing.Point(531, 306);
            this.tb_search_book_comp.Name = "tb_search_book_comp";
            this.tb_search_book_comp.Size = new System.Drawing.Size(145, 21);
            this.tb_search_book_comp.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(694, 310);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 12;
            this.label13.Text = "작업자";
            // 
            // cb_charge
            // 
            this.cb_charge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_charge.FormattingEnabled = true;
            this.cb_charge.Location = new System.Drawing.Point(741, 306);
            this.cb_charge.Name = "cb_charge";
            this.cb_charge.Size = new System.Drawing.Size(121, 20);
            this.cb_charge.TabIndex = 31;
            // 
            // btn_lookup
            // 
            this.btn_lookup.Location = new System.Drawing.Point(880, 305);
            this.btn_lookup.Name = "btn_lookup";
            this.btn_lookup.Size = new System.Drawing.Size(75, 23);
            this.btn_lookup.TabIndex = 27;
            this.btn_lookup.Text = "조    회";
            this.btn_lookup.UseVisualStyleBackColor = true;
            this.btn_lookup.Click += new System.EventHandler(this.btn_lookup_Click);
            // 
            // lbl_grid2
            // 
            this.lbl_grid2.AutoSize = true;
            this.lbl_grid2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_grid2.Location = new System.Drawing.Point(970, 310);
            this.lbl_grid2.Name = "lbl_grid2";
            this.lbl_grid2.Size = new System.Drawing.Size(71, 12);
            this.lbl_grid2.TabIndex = 12;
            this.lbl_grid2.Text = "입고 수 : 0";
            // 
            // lbl_count2
            // 
            this.lbl_count2.AutoSize = true;
            this.lbl_count2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_count2.Location = new System.Drawing.Point(1082, 310);
            this.lbl_count2.Name = "lbl_count2";
            this.lbl_count2.Size = new System.Drawing.Size(58, 12);
            this.lbl_count2.TabIndex = 12;
            this.lbl_count2.Text = "권 수 : 0";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idx2,
            this.num2,
            this.count2,
            this.Book_name2,
            this.Book_comp2,
            this.author2,
            this.pay2,
            this.total2,
            this.list_name2,
            this.edasd,
            this.order2,
            this.Column13,
            this.isbn2,
            this.Date});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView2.Location = new System.Drawing.Point(9, 332);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.RowHeadersWidth = 21;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(1086, 117);
            this.dataGridView2.TabIndex = 2;
            this.dataGridView2.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // dataGridView3
            // 
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.isbn3,
            this.count3,
            this.book_name3,
            this.book_comp3,
            this.author3,
            this.pay3,
            this.persent,
            this.total3,
            this.etc3,
            this.list_name3,
            this.Order3,
            this.Date3,
            this.num3});
            this.dataGridView3.Location = new System.Drawing.Point(9, 455);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView3.RowHeadersWidth = 21;
            this.dataGridView3.RowTemplate.Height = 23;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView3.Size = new System.Drawing.Size(961, 117);
            this.dataGridView3.TabIndex = 0;
            this.dataGridView3.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellValueChanged);
            // 
            // isbn3
            // 
            this.isbn3.HeaderText = "isbn";
            this.isbn3.Name = "isbn3";
            // 
            // count3
            // 
            this.count3.HeaderText = "수량";
            this.count3.Name = "count3";
            this.count3.Width = 40;
            // 
            // book_name3
            // 
            this.book_name3.HeaderText = "도서명";
            this.book_name3.Name = "book_name3";
            this.book_name3.Width = 250;
            // 
            // book_comp3
            // 
            this.book_comp3.HeaderText = "출판사";
            this.book_comp3.Name = "book_comp3";
            // 
            // author3
            // 
            this.author3.HeaderText = "저자";
            this.author3.Name = "author3";
            // 
            // pay3
            // 
            this.pay3.HeaderText = "정가";
            this.pay3.Name = "pay3";
            this.pay3.Width = 80;
            // 
            // persent
            // 
            this.persent.HeaderText = "매입율";
            this.persent.Name = "persent";
            this.persent.Width = 70;
            // 
            // total3
            // 
            this.total3.HeaderText = "합계";
            this.total3.Name = "total3";
            this.total3.Width = 80;
            // 
            // etc3
            // 
            this.etc3.HeaderText = "비고";
            this.etc3.Name = "etc3";
            // 
            // list_name3
            // 
            this.list_name3.HeaderText = "구분";
            this.list_name3.Name = "list_name3";
            this.list_name3.Visible = false;
            // 
            // Order3
            // 
            this.Order3.HeaderText = "주문처";
            this.Order3.Name = "Order3";
            this.Order3.Visible = false;
            // 
            // Date3
            // 
            this.Date3.HeaderText = "날짜";
            this.Date3.Name = "Date3";
            this.Date3.Visible = false;
            // 
            // num3
            // 
            this.num3.HeaderText = "번호";
            this.num3.Name = "num3";
            this.num3.Visible = false;
            this.num3.Width = 60;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Info;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.load_tmplist);
            this.panel3.Controls.Add(this.lbl_pay);
            this.panel3.Controls.Add(this.lbl_total3);
            this.panel3.Controls.Add(this.lbl_grid3);
            this.panel3.Location = new System.Drawing.Point(976, 455);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(164, 117);
            this.panel3.TabIndex = 32;
            // 
            // load_tmplist
            // 
            this.load_tmplist.Location = new System.Drawing.Point(11, 81);
            this.load_tmplist.Name = "load_tmplist";
            this.load_tmplist.Size = new System.Drawing.Size(75, 23);
            this.load_tmplist.TabIndex = 35;
            this.load_tmplist.Text = "목록읽기";
            this.load_tmplist.UseVisualStyleBackColor = true;
            this.load_tmplist.Click += new System.EventHandler(this.load_tmplist_Click);
            // 
            // lbl_pay
            // 
            this.lbl_pay.AutoSize = true;
            this.lbl_pay.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_pay.ForeColor = System.Drawing.Color.Red;
            this.lbl_pay.Location = new System.Drawing.Point(9, 59);
            this.lbl_pay.Name = "lbl_pay";
            this.lbl_pay.Size = new System.Drawing.Size(77, 12);
            this.lbl_pay.TabIndex = 13;
            this.lbl_pay.Text = "결제 금액 : ";
            // 
            // lbl_total3
            // 
            this.lbl_total3.AutoSize = true;
            this.lbl_total3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_total3.Location = new System.Drawing.Point(9, 37);
            this.lbl_total3.Name = "lbl_total3";
            this.lbl_total3.Size = new System.Drawing.Size(84, 12);
            this.lbl_total3.TabIndex = 0;
            this.lbl_total3.Text = "입고 금액 : 0";
            // 
            // lbl_grid3
            // 
            this.lbl_grid3.AutoSize = true;
            this.lbl_grid3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_grid3.Location = new System.Drawing.Point(22, 15);
            this.lbl_grid3.Name = "lbl_grid3";
            this.lbl_grid3.Size = new System.Drawing.Size(71, 12);
            this.lbl_grid3.TabIndex = 12;
            this.lbl_grid3.Text = "입고 수 : 0";
            // 
            // lbl_inven_count
            // 
            this.lbl_inven_count.AutoSize = true;
            this.lbl_inven_count.BackColor = System.Drawing.SystemColors.Info;
            this.lbl_inven_count.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_inven_count.ForeColor = System.Drawing.Color.Red;
            this.lbl_inven_count.Location = new System.Drawing.Point(1119, 390);
            this.lbl_inven_count.Name = "lbl_inven_count";
            this.lbl_inven_count.Size = new System.Drawing.Size(19, 12);
            this.lbl_inven_count.TabIndex = 33;
            this.lbl_inven_count.Text = "-1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(1101, 369);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 12);
            this.label14.TabIndex = 34;
            this.label14.Text = "책 재고 수";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(1208, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(161, 291);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // idx1
            // 
            this.idx1.HeaderText = "idx1";
            this.idx1.Name = "idx1";
            this.idx1.Width = 30;
            // 
            // num
            // 
            this.num.HeaderText = "번호";
            this.num.Name = "num";
            this.num.Width = 50;
            // 
            // count
            // 
            this.count.HeaderText = "수량";
            this.count.Name = "count";
            this.count.Width = 40;
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 250;
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            // 
            // author
            // 
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            // 
            // pay
            // 
            this.pay.HeaderText = "단가";
            this.pay.Name = "pay";
            this.pay.Width = 80;
            // 
            // total
            // 
            this.total.HeaderText = "합계";
            this.total.Name = "total";
            this.total.Width = 80;
            // 
            // list_name
            // 
            this.list_name.HeaderText = "구분";
            this.list_name.Name = "list_name";
            this.list_name.Width = 120;
            // 
            // etc
            // 
            this.etc.HeaderText = "비고";
            this.etc.Name = "etc";
            // 
            // order
            // 
            this.order.HeaderText = "주문처명";
            this.order.Name = "order";
            this.order.Width = 90;
            // 
            // import
            // 
            this.import.HeaderText = "입고";
            this.import.Name = "import";
            this.import.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.import.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.import.Width = 60;
            // 
            // isbn
            // 
            this.isbn.HeaderText = "isbn";
            this.isbn.Name = "isbn";
            this.isbn.Visible = false;
            // 
            // order_date
            // 
            this.order_date.HeaderText = "주문일자";
            this.order_date.Name = "order_date";
            this.order_date.Width = 80;
            // 
            // idx2
            // 
            this.idx2.HeaderText = "idx2";
            this.idx2.Name = "idx2";
            this.idx2.Width = 30;
            // 
            // num2
            // 
            this.num2.HeaderText = "번호";
            this.num2.Name = "num2";
            this.num2.Width = 60;
            // 
            // count2
            // 
            this.count2.HeaderText = "수량";
            this.count2.Name = "count2";
            this.count2.Width = 40;
            // 
            // Book_name2
            // 
            this.Book_name2.HeaderText = "도서명";
            this.Book_name2.Name = "Book_name2";
            this.Book_name2.Width = 250;
            // 
            // Book_comp2
            // 
            this.Book_comp2.HeaderText = "출판사";
            this.Book_comp2.Name = "Book_comp2";
            // 
            // author2
            // 
            this.author2.HeaderText = "저자";
            this.author2.Name = "author2";
            // 
            // pay2
            // 
            this.pay2.HeaderText = "단가";
            this.pay2.Name = "pay2";
            this.pay2.Width = 80;
            // 
            // total2
            // 
            this.total2.HeaderText = "합계";
            this.total2.Name = "total2";
            this.total2.Width = 80;
            // 
            // list_name2
            // 
            this.list_name2.HeaderText = "구분";
            this.list_name2.Name = "list_name2";
            // 
            // edasd
            // 
            this.edasd.HeaderText = "비고";
            this.edasd.Name = "edasd";
            // 
            // order2
            // 
            this.order2.HeaderText = "주문처명";
            this.order2.Name = "order2";
            this.order2.Width = 90;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "취소";
            this.Column13.Name = "Column13";
            this.Column13.Width = 45;
            // 
            // isbn2
            // 
            this.isbn2.HeaderText = "isbn";
            this.isbn2.Name = "isbn2";
            this.isbn2.Visible = false;
            // 
            // Date
            // 
            this.Date.HeaderText = "입고시간";
            this.Date.Name = "Date";
            this.Date.Visible = false;
            // 
            // Purchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1381, 580);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lbl_inven_count);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.cb_charge);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btn_lookup);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.endTime);
            this.Controls.Add(this.startTime);
            this.Controls.Add(this.tb_search_book_comp);
            this.Controls.Add(this.lbl_grid2);
            this.Controls.Add(this.lbl_count2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tb_search_book_name);
            this.Controls.Add(this.label11);
            this.Name = "Purchase";
            this.Text = "입고작업";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Purchase_FormClosing);
            this.Load += new System.EventHandler(this.Purchase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_dly;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_order;
        private System.Windows.Forms.CheckBox chk_order_book;
        private System.Windows.Forms.CheckBox chk_stock;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_book_name;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_book_comp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_persent;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker startTime;
        private System.Windows.Forms.DateTimePicker endTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_search_book_name;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_search_book_comp;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cb_charge;
        private System.Windows.Forms.Button btn_lookup;
        private System.Windows.Forms.Label lbl_grid2;
        private System.Windows.Forms.Label lbl_count2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tb_isbn;
        public System.Windows.Forms.TextBox tb_clt;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.Label lbl_grid;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        public System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.Label lbl_total3;
        private System.Windows.Forms.Label lbl_grid3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_pay;
        private System.Windows.Forms.Label lbl_inven_count;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button load_tmplist;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn count3;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name3;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp3;
        private System.Windows.Forms.DataGridViewTextBoxColumn author3;
        private System.Windows.Forms.DataGridViewTextBoxColumn pay3;
        private System.Windows.Forms.DataGridViewTextBoxColumn persent;
        private System.Windows.Forms.DataGridViewTextBoxColumn total3;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc3;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_name3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Order3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date3;
        private System.Windows.Forms.DataGridViewTextBoxColumn num3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx1;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
        private System.Windows.Forms.DataGridViewTextBoxColumn order;
        private System.Windows.Forms.DataGridViewButtonColumn import;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn order_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx2;
        private System.Windows.Forms.DataGridViewTextBoxColumn num2;
        private System.Windows.Forms.DataGridViewTextBoxColumn count2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Book_name2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Book_comp2;
        private System.Windows.Forms.DataGridViewTextBoxColumn author2;
        private System.Windows.Forms.DataGridViewTextBoxColumn pay2;
        private System.Windows.Forms.DataGridViewTextBoxColumn total2;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_name2;
        private System.Windows.Forms.DataGridViewTextBoxColumn edasd;
        private System.Windows.Forms.DataGridViewTextBoxColumn order2;
        private System.Windows.Forms.DataGridViewButtonColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
    }
}