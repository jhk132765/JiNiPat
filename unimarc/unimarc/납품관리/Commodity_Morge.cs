﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Delivery
{
    public partial class Commodity_Morge : Form
    {
        Commodity_registration com;
        Helper_DB DB = new Helper_DB();
        string[] list1 = { "", "", "", "", "", "", "", "", "" };
        string[] list2 = { "", "", "", "", "", "", "", "", "" };
        public Commodity_Morge(Commodity_registration _com)
        {
            InitializeComponent();
            com = _com;
        }

        private void Commodity_Morge_Load(object sender, EventArgs e)
        {
            rbtn5_1.Checked = true;

            list1[0] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["list_date"].Value.ToString();
            list1[1] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["pur"].Value.ToString();
            list1[2] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["dly"].Value.ToString();
            list1[3] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["charge"].Value.ToString();
            list1[4] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["list_name"].Value.ToString();
            list1[5] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["chk_marc"].Value.ToString();
            list1[6] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["stat2"].Value.ToString();
            list1[7] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["cout"].Value.ToString();
            list1[8] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["tol"].Value.ToString();

            list2[0] = com.dataGridView2.Rows[com.MorgeNum[1]].Cells["list_date"].Value.ToString();
            list2[1] = com.dataGridView2.Rows[com.MorgeNum[1]].Cells["pur"].Value.ToString();
            list2[2] = com.dataGridView2.Rows[com.MorgeNum[1]].Cells["dly"].Value.ToString();
            list2[3] = com.dataGridView2.Rows[com.MorgeNum[1]].Cells["charge"].Value.ToString();
            list2[4] = com.dataGridView2.Rows[com.MorgeNum[1]].Cells["list_name"].Value.ToString();
            list2[5] = com.dataGridView2.Rows[com.MorgeNum[1]].Cells["chk_marc"].Value.ToString();
            list2[6] = com.dataGridView2.Rows[com.MorgeNum[1]].Cells["stat2"].Value.ToString();
            list2[7] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["cout"].Value.ToString();
            list2[8] = com.dataGridView2.Rows[com.MorgeNum[0]].Cells["tol"].Value.ToString();

            tb_list1.Text = list1[4];
            tb_list2.Text = list2[4];

            rbtn1_2_Click(null, null);
        }
        private void rbtn1_2_Click(object sender, EventArgs e)
        {
            if (rbtn5_1.Checked) { tb_list_res.Text = tb_list1.Text; }
            else { tb_list_res.Text = tb_list2.Text; }
        }
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btn_OK_Click(object sender, EventArgs e)
        {
            string area = "`date`, `list_name`, `dly`, `clt`, `charge`, `chk_marc`, `state`";

            string[] up_col = { "compidx", "date", "list_name" };
            List<string> tmp_col = new List<string>();
            tmp_col.AddRange(up_col);

            List<string> tmp1 = new List<string>();
            string[] up_data1 = { com.comp_idx, list1[0], list1[4] };
            tmp1.AddRange(up_data1);

            List<string> tmp2 = new List<string>();
            string[] up_data2 = { com.comp_idx, list2[0], list2[4] };
            tmp2.AddRange(up_data2);

            DB.DBcon();

            if (rbtn5_1.Checked) {
                // 2 => 1로
                TwoToOne(tmp_col, tmp1, tmp2);
            }
            else {
                // 1 => 2로
                OneToTwo(tmp_col, tmp1, tmp2);
            }
            this.Close();
        }
        /// <summary>
        /// data2가 data1으로 적용됨. 2 => 1로
        /// </summary>
        private void TwoToOne(List<string> col, List<string> tmp1, List<string> tmp2)
        {
            string[] up_col = col.ToArray();
            string[] up_data1 = tmp1.ToArray();
            string[] up_data2 = tmp2.ToArray();

            string cmd = "";
            cmd = DB.More_Update("Obj_List_Book", up_col, up_data1, up_col, up_data2);
            DB.DB_Send_CMD_reVoid(cmd);
            int cout1 = Convert.ToInt32(list1[7]);
            int cout2 = Convert.ToInt32(list2[7]);
            int cout = cout1 + cout2;

            int tol1 = Convert.ToInt32(list1[8]);
            int tol2 = Convert.ToInt32(list2[8]);
            int tol = tol1 + tol2;

            string[] edit_col = { "total", "compidx" };
            string[] edit_data = { tol.ToString(), com.comp_idx };
            cmd = DB.More_Update("Obj_List_Marc", edit_col, edit_data, up_col, up_data1);
            DB.DB_Send_CMD_reVoid(cmd);
            cmd = DB.DB_Delete_More_term("Obj_List_Marc", "compidx", com.comp_idx, up_col, up_data2);
            DB.DB_Send_CMD_reVoid(cmd);

            edit_col[1] = "vol";
            edit_data[1] = cout.ToString();
            up_col[0] = "comp_num";
            cmd = DB.More_Update("Obj_List", edit_col, edit_data, up_col, up_data1);
            DB.DB_Send_CMD_reVoid(cmd);
            cmd = DB.DB_Delete_More_term("Obj_List", "comp_num", com.comp_idx, up_col, up_data2);
            DB.DB_Send_CMD_reVoid(cmd);
        }
        /// <summary>
        /// data1가 data2으로 적용됨. 1 => 2로
        /// </summary>
        private void OneToTwo(List<string> col, List<string> tmp1, List<string> tmp2)
        {
            string[] up_col = col.ToArray();
            string[] up_data1 = tmp1.ToArray();
            string[] up_data2 = tmp2.ToArray();
            string cmd = "";
            cmd = DB.More_Update("Obj_List_Book", up_col, up_data2, up_col, up_data1);
            DB.DB_Send_CMD_reVoid(cmd);
            int cout1 = Convert.ToInt32(list1[7]);
            int cout2 = Convert.ToInt32(list2[7]);
            int cout = cout1 + cout2;

            int tol1 = Convert.ToInt32(list1[8]);
            int tol2 = Convert.ToInt32(list2[8]);
            int tol = tol1 + tol2;

            string[] edit_col = { "total", "compidx" };
            string[] edit_data = { tol.ToString(), com.comp_idx };
            cmd = DB.More_Update("Obj_List_Marc", edit_col, edit_data, up_col, up_data2);
            DB.DB_Send_CMD_reVoid(cmd);
            cmd = DB.DB_Delete_More_term("Obj_List_Marc", "compidx", com.comp_idx, up_col, up_data1);
            DB.DB_Send_CMD_reVoid(cmd);

            edit_col[1] = "vol";
            edit_data[1] = cout.ToString();
            up_col[0] = "comp_num";
            cmd = DB.More_Update("Obj_List", edit_col, edit_data, up_col, up_data2);
            DB.DB_Send_CMD_reVoid(cmd);
            cmd = DB.DB_Delete_More_term("Obj_List", "comp_num", com.comp_idx, up_col, up_data1);
            DB.DB_Send_CMD_reVoid(cmd);
        }
    }
}
