﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Delivery
{
    public partial class Input_Lookup_Stock : Form
    {
        Main main;
        Helper_DB db = new Helper_DB();
        public string compidx;
        string table_name = "Inven";
        int rowidx=0;
        public Input_Lookup_Stock(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        private void Input_Lookup_Stock_Load(object sender, EventArgs e)
        {
            db.DBcon();
        }
        private void btn_Lookup_Click(object sender, EventArgs e)
        {
            string Area = "`isbn`, `book_name`, `author`, `book_comp`, `pay`, " +
                          "`count`, `order`, `order_date`";
            string cmd = db.DB_Select_Search(Area, table_name, "compidx", compidx);
            string db_res = db.DB_Send_CMD_Search(cmd);
            mk_grid(db_res);
            for(int a= 0; a < dataGridView1.Rows.Count; a++)
            {
                if (chk_stock(dataGridView1.Rows[a].Cells["ISBN"].Value.ToString()) == true)
                {
                    dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.YellowGreen;
                }
            }
        }
        private void mk_grid(string db_data)
        {
            dataGridView1.Rows.Clear();
            string[] data = db_data.Split('|');
            string[] input_grid = { "", "", "", "", "",
                                    "", "", "" };
            for(int a = 0; a < data.Length; a++) {
                if (a % 8 == 0) { input_grid[0] = data[a]; }
                if (a % 8 == 1) { input_grid[1] = data[a]; }
                if (a % 8 == 2) { input_grid[2] = data[a]; }
                if (a % 8 == 3) { input_grid[3] = data[a]; }
                if (a % 8 == 4) { input_grid[4] = data[a]; }
                if (a % 8 == 5) { input_grid[5] = data[a]; }
                if (a % 8 == 6) { input_grid[6] = data[a]; }
                if (a % 8 == 7) {
                    if (data[a].Length < 3) { input_grid[7] = data[a]; }
                    else { input_grid[7] = data[a].Substring(0, 10); }
                    dataGridView1.Rows.Add(input_grid); }
            }
        }
        private void btn_Add_Click(object sender, EventArgs e)
        {
            string[] data = { "", "", "", "",
                              "", "", "", "" };
            input_TextBox(data);
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (tb_book_name.Text == "") { MessageBox.Show("도서명을 작성해주세요."); return; }
            if (tb_isbn.Text == "")      { MessageBox.Show("ISBN을 작성해주세요."); return; }
            if (tb_book_comp.Text == "") { MessageBox.Show("출판사를 작성해주세요."); return; }
            if (tb_author.Text == "")    { MessageBox.Show("저자를 작성해주세요."); return; }
            if (tb_pay.Text == "")       { MessageBox.Show("정가를 작성해주세요."); return; }
            if (tb_order.Text == "")     { MessageBox.Show("매입처를 작성해주세요."); return; }
            if (tb_count.Text == "")     { MessageBox.Show("수량을 작성해주세요."); return; }

            if (!ask_SaveChk(grid_text_savechk())) { return; }

            string[] input = { tb_book_name.Text, tb_isbn.Text, tb_book_comp.Text, tb_author.Text, tb_pay.Text,
                               tb_order.Text, tb_count.Text, DateTime.Now.ToString("g"), compidx };
            string[] where = { "book_name", "isbn", "book_comp", "author", "pay",
                               "order", "count", "import_date", "compidx" };
            string Incmd = db.DB_INSERT(table_name, where, input);
            db.DB_Send_CMD_reVoid(Incmd);
            MessageBox.Show("저장되었습니다!");
        }
        private bool grid_text_savechk()
        {
            int row = dataGridView1.CurrentCell.RowIndex;
            string[] grid_data = { dataGridView1.Rows[row].Cells["ISBN"].Value.ToString(),
                                   dataGridView1.Rows[row].Cells["book_name"].Value.ToString(),
                                   dataGridView1.Rows[row].Cells["author"].Value.ToString(),
                                   dataGridView1.Rows[row].Cells["book_comp"].Value.ToString(),
                                   dataGridView1.Rows[row].Cells["pay"].Value.ToString().Replace(",",""),
                                   dataGridView1.Rows[row].Cells["count"].Value.ToString(),
                                   dataGridView1.Rows[row].Cells["order"].Value.ToString(),
                                   dataGridView1.Rows[row].Cells["Order_date"].Value.ToString() };

            if (tb_book_name.Text == grid_data[1]) { return false; }
            if (tb_isbn.Text == grid_data[0]) { return false; }
            return true;
        }
        private bool ask_SaveChk(bool ask)
        {
            if (!ask) {
                if (MessageBox.Show("저장하실 내용이 입고에 저장되어있습니다. 저장하시겠습니까?", "경고!", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) {

                    return false;
                }
            }
            return true;
        }
        private void btn_list_in_stock_Click(object sender, EventArgs e)
        {
            /* 1. 재고DB를 불러온다.(isbn, 도서명, 저자, 출판사, 정가, 할인율, 수량, 매입처명)
             * 2. 목록내DB를 불러온다.(isbn)
             * 3. 재고DB와 목록DB의 isbn을 for문으로 검사한다. (일치 true, 불일치 false)
             * 4. true일 경우에만 재고DB에서 불러온 내용을 표에 집어넣는다. */

            dataGridView1.Rows.Clear();
            string Area = "`isbn`, `book_name`, `author`, `book_comp`, `pay`, " +
                          "`count`, `order`, `order_date`";
            string[] colm = { "compidx" };
            string[] data = { compidx };
            string cmd = db.More_DB_Search(table_name, colm, data, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] stock = db_res.Split('|');
            stock_check(stock);
        }
        private void stock_check(string[] stock)
        {
            string[] gird = { "", "", "", "", "", "", "", "" };
            for(int a = 0; a < stock.Length - 1; a++)
            {
                if (a % 8 == 0) { gird[0] = stock[a]; }
                if (a % 8 == 1) { gird[1] = stock[a]; }
                if (a % 8 == 2) { gird[2] = stock[a]; }
                if (a % 8 == 3) { gird[3] = stock[a]; }
                if (a % 8 == 4) { gird[4] = stock[a]; }
                if (a % 8 == 5) { gird[5] = stock[a]; }
                if (a % 8 == 6) { gird[6] = stock[a]; }
                if (a % 8 == 7) {
                    if (stock[a].Length < 3) { gird[7] = stock[a]; }
                    else { gird[7] = stock[a].Substring(0, 10); }
                    if (chk_stock(gird[0]) == true) {
                        dataGridView1.Rows.Add(gird);
                    }
                }
            }
        }
        private bool chk_stock(string isbn)
        {
            string[] table = { "compidx", "isbn" };
            string[] colmn = { compidx, isbn };
            string cmd = db.More_DB_Search("Obj_List_Book", table, colmn, "isbn");
            string db_res = db.DB_Send_CMD_Search(cmd);
            if (db_res.Length < 3) { return false; }
            else if (isbn == "") { return false; }
            else { return true; }
        }
        private void btn_Edit_Click(object sender, EventArgs e)
        {
            string[] ser_data = { compidx,
                                  dataGridView1.Rows[rowidx].Cells["book_name"].Value.ToString(),
                                  dataGridView1.Rows[rowidx].Cells["isbn"].Value.ToString(),
                                  dataGridView1.Rows[rowidx].Cells["book_comp"].Value.ToString(),
                                  dataGridView1.Rows[rowidx].Cells["author"].Value.ToString(),
                                  dataGridView1.Rows[rowidx].Cells["pay"].Value.ToString(),
                                  dataGridView1.Rows[rowidx].Cells["order"].Value.ToString(),
                                  dataGridView1.Rows[rowidx].Cells["count"].Value.ToString() };

            string[] ser_where = { "compidx", "book_name", "isbn", "book_comp", "author",
                                   "pay", "order", "count" };
            string[] edit_data = { tb_book_name.Text, tb_isbn.Text, tb_book_comp.Text, tb_author.Text, tb_pay.Text,
                                   tb_order.Text, tb_count.Text };
            string[] edit_where = { "book_name", "isbn", "book_comp", "author", "pay",
                                    "order", "count" };
            string U_cmd = db.More_Update(table_name, edit_where, edit_data, ser_where, ser_data);
            db.DB_Send_CMD_reVoid(U_cmd);

            dataGridView1.Rows[rowidx].Cells["book_name"].Value = tb_book_name.Text;
            dataGridView1.Rows[rowidx].Cells["isbn"].Value = tb_isbn.Text;
            dataGridView1.Rows[rowidx].Cells["book_comp"].Value = tb_book_comp.Text;
            dataGridView1.Rows[rowidx].Cells["author"].Value = tb_author.Text;
            dataGridView1.Rows[rowidx].Cells["pay"].Value = tb_pay.Text;
            dataGridView1.Rows[rowidx].Cells["order"].Value = tb_order.Text;
            dataGridView1.Rows[rowidx].Cells["count"].Value = tb_count.Text;
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowidx = e.RowIndex;
            string[] data = { dataGridView1.Rows[rowidx].Cells["book_name"].Value.ToString(),
                              dataGridView1.Rows[rowidx].Cells["isbn"].Value.ToString(),
                              dataGridView1.Rows[rowidx].Cells["book_comp"].Value.ToString(),
                              dataGridView1.Rows[rowidx].Cells["author"].Value.ToString(),
                              dataGridView1.Rows[rowidx].Cells["pay"].Value.ToString(),
                              dataGridView1.Rows[rowidx].Cells["order"].Value.ToString(),
                              dataGridView1.Rows[rowidx].Cells["count"].Value.ToString() };
            input_TextBox(data);
        }
        private void input_TextBox(string[] Text)
        {
            tb_book_name.Text = Text[0];
            tb_isbn.Text      = Text[1];
            tb_book_comp.Text = Text[2];
            tb_author.Text    = Text[3];
            tb_pay.Text       = Text[4];
            tb_order.Text     = Text[5];
            tb_count.Text     = Text[6];
        }
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up) { rowidx--; }
            if (e.KeyCode == Keys.Down) { rowidx++; }
        }
        private void tb_pay_TextChanged(object sender, EventArgs e)
        {
            String_Text st = new String_Text();
            st.Int_Comma(sender, e);
        }
        private void tb_pay_KeyPress(object sender, KeyPressEventArgs e)
        {
            String_Text st = new String_Text();
            st.Only_Int(sender, e);
        }
    }
}
