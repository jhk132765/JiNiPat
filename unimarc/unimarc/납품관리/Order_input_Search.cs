﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Account;
using WindowsFormsApp1.Mac;

namespace WindowsFormsApp1.Delivery
{
    public partial class Order_input_Search : Form
    {
        Order_input oin;
        Purchase_Book pb;
        Remit_reg2 rem2;
        Check_ISBN isbn;
        Helper_DB db = new Helper_DB();

        public int[] oin_grid_idx = { 0, 0 };
        public string Where_Open;
        public string searchText;
        public bool OnlyMarc = false;
        string compidx;

        public Order_input_Search(Order_input o_in)
        {
            InitializeComponent();
            oin = o_in;
            compidx = oin.compidx;
        }
        public Order_input_Search(Remit_reg2 _rem2)
        {
            InitializeComponent();
            rem2 = _rem2;
            compidx = rem2.compidx;
            searchText = rem2.tb_purchase.Text;
        }
        public Order_input_Search(Purchase_Book _pb)
        {
            InitializeComponent();
            pb = _pb;
            compidx = pb.compidx;
            searchText = pb.tb_purchase.Text;
        }
        public Order_input_Search(Check_ISBN _isbn)
        {
            InitializeComponent();
            isbn = _isbn;
            compidx = isbn.compidx;
            searchText = isbn.tb_list_name.Text;
        }

        public Order_input_Search()
        {
            InitializeComponent();
        }
        private void Order_input_Search_Load(object sender, EventArgs e)
        {
            string cmd, db_res;
            db.DBcon();
            if (Where_Open == "book_list")
            {
                string table = "Obj_List";
                string Area = "`idx`, `list_name`, `charge`, `date`, `date_res`";
                cmd = string.Format("SELECT {0} FROM {1} WHERE `comp_num` = {2} AND `state` = \"진행\"", Area, table, compidx);
                if (searchText != "")
                {
                    cmd += " AND `list_name` like \"%" + searchText + "%\"";
                    // cmd = db.DB_Contains("Obj_List", compidx,
                    //     "", "", "`list_name`, `charge`, `date`, `date_res`");
                }
                if (OnlyMarc)
                {
                    cmd += " AND `chk_marc` > 0";
                    // cmd = db.DB_Contains("Obj_List", compidx,
                    //     "list_name", searchText, "`list_name`, `charge`, `date`, `date_res`");
                }
                cmd += ";";
                db_res = db.DB_Send_CMD_Search(cmd);
                made_grid(db_res);
                this.Text = "목록 검색";
            }
            else if (Where_Open.Contains("Order"))
            {
                list_name.HeaderText = "주문처";
                charge.HeaderText = "대표자";
                date.HeaderText = "종목";
                date_res.HeaderText = "업태";

                cmd = db.DB_Contains("Purchase", compidx,
                    "sangho", searchText, "`idx`, `sangho`, `boss`, `jongmok`, `uptae`");
                db_res = db.DB_Send_CMD_Search(cmd);
                made_grid(db_res);
                this.Text = "주문처 검색";
            }
        }
        private void made_grid(string strValue)
        {
            string[] data = strValue.Split('|');
            dataGridView1.Rows.Clear();
            string[] res = { "", "", "", "", "" };
            for (int a = 0; a < data.Length; a++)
            {
                if (a % 5 == 0) { res[0] = data[a]; }
                if (a % 5 == 1) { res[1] = data[a]; }
                if (a % 5 == 2) { res[2] = data[a]; }
                if (a % 5 == 3) { res[3] = data[a]; }
                if (a % 5 == 4) { res[4] = data[a];
                    if (res[1].Contains(searchText))
                        dataGridView1.Rows.Add(res);
                }
            }
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                if (dataGridView1.Rows.Count == 0)
                    return;
                dataGridView1_CellDoubleClick(null, null);
            }
            if (e.KeyCode == Keys.Escape) { Close(); }
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = 0;

            if (Where_Open == "") { row = 0; }
            else row = dataGridView1.CurrentCell.RowIndex;

            if (e != null) { row = e.RowIndex; }
            else row = dataGridView1.CurrentCell.RowIndex;

            if (oin != null) {
                oin_result(row);
            }
            else if (pb != null) {
                pb_result(row);
            }
            else if (rem2 != null) {
                rem2.tb_purchase.Text = dataGridView1.Rows[row].Cells["list_name"].Value.ToString();
                rem2.mk_base(rem2.tb_purchase.Text);
            }
            else if (isbn != null) {
                string listName = dataGridView1.Rows[row].Cells["list_name"].Value.ToString();
                string l_idx = dataGridView1.Rows[row].Cells["idx"].Value.ToString();
                isbn.tb_list_name.Text = listName;
                isbn.DataLoad(listName, l_idx);
            }
            Close();
        }
        private void oin_result(int grididx)
        {
            if (Where_Open == "book_list")
            {
                string ListName = dataGridView1.Rows[grididx].Cells["list_name"].Value.ToString();
                string DBdate = dataGridView1.Rows[grididx].Cells["date"].Value.ToString();

                string[] tmp_col = { "compidx", "list_name", "date" };
                string[] tmp_data = { compidx, ListName, DBdate };
                string takedata = "`order`, `order_stat`, `isbn`, `book_name`, `author`, " +
                                  "`book_comp`, `order_count`, `count`, `pay`, `total`, " +
                                  "`etc`, `list_name`, `order_date`, `send_date`, `header`, " +
                                  "`num`, `idx`";
                string cmd = db.More_DB_Search("Obj_List_Book", tmp_col, tmp_data, takedata);
                string db_res = db.DB_Send_CMD_Search(cmd);
                oin.made_grid(db_res, false);
                oin.tb_search_book_list.Text = ListName;

                DateTime setDate = DateTime.ParseExact(DBdate, "yyyy-MM-dd", null);

                oin.dtp_listBegin.Checked = true;
                oin.dtp_listEnd.Checked = true;

                oin.dtp_listBegin.Value = setDate;
                oin.dtp_listEnd.Value = setDate;
            }

            else if (Where_Open.Contains("Order"))
            {
                if (Where_Open == "Order_Grid") {
                    oin.dataGridView1.Rows[oin_grid_idx[0]].Cells[oin_grid_idx[1]].Value =
                        dataGridView1.Rows[grididx].Cells["list_name"].Value.ToString();
                }
                else
                {
                    try
                    {
                        oin.tb_orderText.Text = dataGridView1.Rows[grididx].Cells["list_name"].Value.ToString();
                    }
                    catch { }
                }
            }
        }
        private void pb_result(int grididx)
        {
            pb.tb_purchase.Text = dataGridView1.Rows[grididx].Cells["list_name"].Value.ToString();
            pb.btn_Lookup_Click(null, null);
        }
    }
}
