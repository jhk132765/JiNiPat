﻿namespace WindowsFormsApp1.Delivery
{
    partial class Order_input
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_search_order = new System.Windows.Forms.TextBox();
            this.cb_user = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_state = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_order = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtp_orderBegin = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.dtp_orderEnd = new System.Windows.Forms.DateTimePicker();
            this.dtp_listEnd = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.dtp_listBegin = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_PS = new System.Windows.Forms.TextBox();
            this.btn_lookup = new System.Windows.Forms.Button();
            this.btn_Reset_Order = new System.Windows.Forms.Button();
            this.btn_AutoOrder = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.btn_All_chk = new System.Windows.Forms.Button();
            this.btn_All_unchk = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.cb_ordersend = new System.Windows.Forms.ComboBox();
            this.btn_order_send = new System.Windows.Forms.Button();
            this.btn_send_chk = new System.Windows.Forms.Button();
            this.tb_orderText = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_order_list_change = new System.Windows.Forms.Button();
            this.btn_order_chk = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.book_comp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.order_count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.list_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.order_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.send_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_search_book_comp = new System.Windows.Forms.TextBox();
            this.tb_search_book_name = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tb_search_book_list = new System.Windows.Forms.TextBox();
            this.btn_order_empty = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_Excel = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lbl_OrderStat = new System.Windows.Forms.Label();
            this.cb_OrderStat = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "주문처";
            // 
            // tb_search_order
            // 
            this.tb_search_order.Location = new System.Drawing.Point(63, 5);
            this.tb_search_order.Name = "tb_search_order";
            this.tb_search_order.Size = new System.Drawing.Size(115, 21);
            this.tb_search_order.TabIndex = 2;
            this.tb_search_order.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_search_KeyDown);
            // 
            // cb_user
            // 
            this.cb_user.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_user.FormattingEnabled = true;
            this.cb_user.Location = new System.Drawing.Point(577, 5);
            this.cb_user.Name = "cb_user";
            this.cb_user.Size = new System.Drawing.Size(73, 20);
            this.cb_user.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(536, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "담당자";
            // 
            // cb_state
            // 
            this.cb_state.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_state.FormattingEnabled = true;
            this.cb_state.Location = new System.Drawing.Point(689, 5);
            this.cb_state.Name = "cb_state";
            this.cb_state.Size = new System.Drawing.Size(73, 20);
            this.cb_state.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(659, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "입고";
            // 
            // cb_order
            // 
            this.cb_order.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_order.FormattingEnabled = true;
            this.cb_order.Location = new System.Drawing.Point(806, 5);
            this.cb_order.Name = "cb_order";
            this.cb_order.Size = new System.Drawing.Size(73, 20);
            this.cb_order.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(776, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "주문";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "작업명단";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(376, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "주문일";
            // 
            // dtp_orderBegin
            // 
            this.dtp_orderBegin.CustomFormat = "yyyy-MM-01";
            this.dtp_orderBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_orderBegin.Location = new System.Drawing.Point(417, 4);
            this.dtp_orderBegin.Name = "dtp_orderBegin";
            this.dtp_orderBegin.Size = new System.Drawing.Size(87, 21);
            this.dtp_orderBegin.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(506, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "~";
            // 
            // dtp_orderEnd
            // 
            this.dtp_orderEnd.CustomFormat = "yyyy-MM-dd";
            this.dtp_orderEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_orderEnd.Location = new System.Drawing.Point(522, 4);
            this.dtp_orderEnd.Name = "dtp_orderEnd";
            this.dtp_orderEnd.Size = new System.Drawing.Size(87, 21);
            this.dtp_orderEnd.TabIndex = 17;
            // 
            // dtp_listEnd
            // 
            this.dtp_listEnd.Checked = false;
            this.dtp_listEnd.CustomFormat = "yyyy-MM-dd";
            this.dtp_listEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_listEnd.Location = new System.Drawing.Point(776, 4);
            this.dtp_listEnd.Name = "dtp_listEnd";
            this.dtp_listEnd.ShowCheckBox = true;
            this.dtp_listEnd.Size = new System.Drawing.Size(103, 21);
            this.dtp_listEnd.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(760, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 12);
            this.label8.TabIndex = 20;
            this.label8.Text = "~";
            // 
            // dtp_listBegin
            // 
            this.dtp_listBegin.Checked = false;
            this.dtp_listBegin.CustomFormat = "yyyy-MM-01";
            this.dtp_listBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_listBegin.Location = new System.Drawing.Point(655, 4);
            this.dtp_listBegin.Name = "dtp_listBegin";
            this.dtp_listBegin.ShowCheckBox = true;
            this.dtp_listBegin.Size = new System.Drawing.Size(103, 21);
            this.dtp_listBegin.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(614, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 18;
            this.label9.Text = "목록일";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 12);
            this.label10.TabIndex = 22;
            this.label10.Text = "주문시 요청사항";
            // 
            // tb_PS
            // 
            this.tb_PS.Location = new System.Drawing.Point(104, 31);
            this.tb_PS.Name = "tb_PS";
            this.tb_PS.Size = new System.Drawing.Size(624, 21);
            this.tb_PS.TabIndex = 23;
            // 
            // btn_lookup
            // 
            this.btn_lookup.Location = new System.Drawing.Point(902, 12);
            this.btn_lookup.Name = "btn_lookup";
            this.btn_lookup.Size = new System.Drawing.Size(85, 25);
            this.btn_lookup.TabIndex = 25;
            this.btn_lookup.Text = "조    회";
            this.btn_lookup.UseVisualStyleBackColor = true;
            this.btn_lookup.Click += new System.EventHandler(this.btn_lookup_Click);
            // 
            // btn_Reset_Order
            // 
            this.btn_Reset_Order.Location = new System.Drawing.Point(18, 110);
            this.btn_Reset_Order.Name = "btn_Reset_Order";
            this.btn_Reset_Order.Size = new System.Drawing.Size(77, 23);
            this.btn_Reset_Order.TabIndex = 33;
            this.btn_Reset_Order.Text = "주문처 리셋";
            this.btn_Reset_Order.UseVisualStyleBackColor = true;
            this.btn_Reset_Order.Click += new System.EventHandler(this.btn_Reset_Order_Click);
            // 
            // btn_AutoOrder
            // 
            this.btn_AutoOrder.Location = new System.Drawing.Point(172, 110);
            this.btn_AutoOrder.Name = "btn_AutoOrder";
            this.btn_AutoOrder.Size = new System.Drawing.Size(75, 23);
            this.btn_AutoOrder.TabIndex = 34;
            this.btn_AutoOrder.Text = "자동주문처";
            this.btn_AutoOrder.UseVisualStyleBackColor = true;
            this.btn_AutoOrder.Click += new System.EventHandler(this.btn_AutoOrder_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(101, 109);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(72, 28);
            this.checkBox3.TabIndex = 35;
            this.checkBox3.Text = "isbn자동\n주문처";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Visible = false;
            // 
            // btn_All_chk
            // 
            this.btn_All_chk.Location = new System.Drawing.Point(251, 110);
            this.btn_All_chk.Name = "btn_All_chk";
            this.btn_All_chk.Size = new System.Drawing.Size(85, 23);
            this.btn_All_chk.TabIndex = 36;
            this.btn_All_chk.Text = "전체주문선택";
            this.btn_All_chk.UseVisualStyleBackColor = true;
            this.btn_All_chk.Click += new System.EventHandler(this.btn_All_chk_Click);
            // 
            // btn_All_unchk
            // 
            this.btn_All_unchk.Location = new System.Drawing.Point(340, 110);
            this.btn_All_unchk.Name = "btn_All_unchk";
            this.btn_All_unchk.Size = new System.Drawing.Size(85, 23);
            this.btn_All_unchk.TabIndex = 37;
            this.btn_All_unchk.Text = "전체선택취소";
            this.btn_All_unchk.UseVisualStyleBackColor = true;
            this.btn_All_unchk.Click += new System.EventHandler(this.btn_All_unchk_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(989, 12);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(85, 25);
            this.btn_Save.TabIndex = 38;
            this.btn_Save.Text = "저    장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // cb_ordersend
            // 
            this.cb_ordersend.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_ordersend.FormattingEnabled = true;
            this.cb_ordersend.Location = new System.Drawing.Point(569, 111);
            this.cb_ordersend.Name = "cb_ordersend";
            this.cb_ordersend.Size = new System.Drawing.Size(77, 20);
            this.cb_ordersend.TabIndex = 40;
            // 
            // btn_order_send
            // 
            this.btn_order_send.Location = new System.Drawing.Point(719, 110);
            this.btn_order_send.Name = "btn_order_send";
            this.btn_order_send.Size = new System.Drawing.Size(75, 23);
            this.btn_order_send.TabIndex = 42;
            this.btn_order_send.Text = "주문보내기";
            this.btn_order_send.UseVisualStyleBackColor = true;
            this.btn_order_send.Click += new System.EventHandler(this.btn_order_send_Click);
            // 
            // btn_send_chk
            // 
            this.btn_send_chk.Location = new System.Drawing.Point(652, 110);
            this.btn_send_chk.Name = "btn_send_chk";
            this.btn_send_chk.Size = new System.Drawing.Size(63, 23);
            this.btn_send_chk.TabIndex = 41;
            this.btn_send_chk.Text = "전송현황";
            this.btn_send_chk.UseVisualStyleBackColor = true;
            this.btn_send_chk.Click += new System.EventHandler(this.btn_send_chk_Click);
            // 
            // tb_orderText
            // 
            this.tb_orderText.Location = new System.Drawing.Point(970, 61);
            this.tb_orderText.Name = "tb_orderText";
            this.tb_orderText.Size = new System.Drawing.Size(129, 21);
            this.tb_orderText.TabIndex = 44;
            this.tb_orderText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_orderText_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(917, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 43;
            this.label12.Text = "주문처명";
            // 
            // btn_order_list_change
            // 
            this.btn_order_list_change.Location = new System.Drawing.Point(1103, 60);
            this.btn_order_list_change.Name = "btn_order_list_change";
            this.btn_order_list_change.Size = new System.Drawing.Size(129, 23);
            this.btn_order_list_change.TabIndex = 45;
            this.btn_order_list_change.Text = "리스트 일괄변경";
            this.btn_order_list_change.UseVisualStyleBackColor = true;
            this.btn_order_list_change.Click += new System.EventHandler(this.btn_order_list_change_Click);
            // 
            // btn_order_chk
            // 
            this.btn_order_chk.Location = new System.Drawing.Point(1103, 87);
            this.btn_order_chk.Name = "btn_order_chk";
            this.btn_order_chk.Size = new System.Drawing.Size(129, 23);
            this.btn_order_chk.TabIndex = 46;
            this.btn_order_chk.Text = "선택주문 일괄입력";
            this.btn_order_chk.UseVisualStyleBackColor = true;
            this.btn_order_chk.Click += new System.EventHandler(this.btn_order_chk_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk,
            this.order,
            this.M,
            this.isbn,
            this.book_name,
            this.author,
            this.book_comp,
            this.order_count,
            this.count,
            this.pay,
            this.total,
            this.etc,
            this.list_name,
            this.order_date,
            this.send_date,
            this.num,
            this.idx});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView1.RowHeadersWidth = 10;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1261, 585);
            this.dataGridView1.TabIndex = 47;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // chk
            // 
            this.chk.HeaderText = "주문선택";
            this.chk.Name = "chk";
            this.chk.Width = 60;
            // 
            // order
            // 
            this.order.HeaderText = "주문처";
            this.order.Name = "order";
            this.order.Width = 80;
            // 
            // M
            // 
            this.M.HeaderText = "M";
            this.M.Name = "M";
            this.M.Width = 25;
            // 
            // isbn
            // 
            this.isbn.HeaderText = "ISBN13";
            this.isbn.Name = "isbn";
            // 
            // book_name
            // 
            this.book_name.HeaderText = "도서명";
            this.book_name.Name = "book_name";
            this.book_name.Width = 200;
            // 
            // author
            // 
            this.author.HeaderText = "저자";
            this.author.Name = "author";
            this.author.Width = 80;
            // 
            // book_comp
            // 
            this.book_comp.HeaderText = "출판사";
            this.book_comp.Name = "book_comp";
            // 
            // order_count
            // 
            this.order_count.HeaderText = "주문\n수";
            this.order_count.Name = "order_count";
            this.order_count.Width = 50;
            // 
            // count
            // 
            this.count.HeaderText = "원주\n문";
            this.count.Name = "count";
            this.count.Width = 50;
            // 
            // pay
            // 
            this.pay.HeaderText = "정가";
            this.pay.Name = "pay";
            this.pay.Width = 80;
            // 
            // total
            // 
            this.total.HeaderText = "합계";
            this.total.Name = "total";
            this.total.Width = 75;
            // 
            // etc
            // 
            this.etc.HeaderText = "비   고";
            this.etc.Name = "etc";
            // 
            // list_name
            // 
            this.list_name.HeaderText = "구분";
            this.list_name.Name = "list_name";
            this.list_name.Width = 80;
            // 
            // order_date
            // 
            this.order_date.HeaderText = "주문일자";
            this.order_date.Name = "order_date";
            this.order_date.Width = 80;
            // 
            // send_date
            // 
            this.send_date.HeaderText = "송금일자";
            this.send_date.Name = "send_date";
            this.send_date.Visible = false;
            this.send_date.Width = 80;
            // 
            // num
            // 
            this.num.HeaderText = "번호";
            this.num.Name = "num";
            this.num.Width = 50;
            // 
            // idx
            // 
            this.idx.HeaderText = "idx";
            this.idx.Name = "idx";
            this.idx.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tb_search_order);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cb_user);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cb_state);
            this.panel1.Controls.Add(this.tb_search_book_comp);
            this.panel1.Controls.Add(this.tb_search_book_name);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cb_order);
            this.panel1.Location = new System.Drawing.Point(8, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(887, 32);
            this.panel1.TabIndex = 48;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(355, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "출판사";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(185, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "도서명";
            // 
            // tb_search_book_comp
            // 
            this.tb_search_book_comp.Location = new System.Drawing.Point(398, 5);
            this.tb_search_book_comp.Name = "tb_search_book_comp";
            this.tb_search_book_comp.Size = new System.Drawing.Size(121, 21);
            this.tb_search_book_comp.TabIndex = 44;
            this.tb_search_book_comp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_search_KeyDown);
            // 
            // tb_search_book_name
            // 
            this.tb_search_book_name.Location = new System.Drawing.Point(228, 5);
            this.tb_search_book_name.Name = "tb_search_book_name";
            this.tb_search_book_name.Size = new System.Drawing.Size(119, 21);
            this.tb_search_book_name.TabIndex = 44;
            this.tb_search_book_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_search_KeyDown);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.dtp_orderBegin);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.dtp_orderEnd);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.dtp_listBegin);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.tb_search_book_list);
            this.panel2.Controls.Add(this.dtp_listEnd);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.tb_PS);
            this.panel2.Location = new System.Drawing.Point(8, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(887, 58);
            this.panel2.TabIndex = 48;
            // 
            // tb_search_book_list
            // 
            this.tb_search_book_list.Location = new System.Drawing.Point(63, 4);
            this.tb_search_book_list.Name = "tb_search_book_list";
            this.tb_search_book_list.Size = new System.Drawing.Size(307, 21);
            this.tb_search_book_list.TabIndex = 44;
            this.tb_search_book_list.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_search_book_list_KeyDown);
            // 
            // btn_order_empty
            // 
            this.btn_order_empty.Location = new System.Drawing.Point(970, 87);
            this.btn_order_empty.Name = "btn_order_empty";
            this.btn_order_empty.Size = new System.Drawing.Size(129, 23);
            this.btn_order_empty.TabIndex = 46;
            this.btn_order_empty.Text = "주문처빈칸 일괄입력";
            this.btn_order_empty.UseVisualStyleBackColor = true;
            this.btn_order_empty.Click += new System.EventHandler(this.btn_order_empty_Click);
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(1163, 12);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(85, 25);
            this.btn_close.TabIndex = 49;
            this.btn_close.Text = "닫    기";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_Excel
            // 
            this.btn_Excel.Location = new System.Drawing.Point(1076, 12);
            this.btn_Excel.Name = "btn_Excel";
            this.btn_Excel.Size = new System.Drawing.Size(85, 25);
            this.btn_Excel.TabIndex = 50;
            this.btn_Excel.Text = "엑    셀";
            this.btn_Excel.UseVisualStyleBackColor = true;
            this.btn_Excel.Click += new System.EventHandler(this.btn_Excel_Click);
            // 
            // lbl_OrderStat
            // 
            this.lbl_OrderStat.AutoSize = true;
            this.lbl_OrderStat.Location = new System.Drawing.Point(795, 115);
            this.lbl_OrderStat.Name = "lbl_OrderStat";
            this.lbl_OrderStat.Size = new System.Drawing.Size(53, 12);
            this.lbl_OrderStat.TabIndex = 51;
            this.lbl_OrderStat.Text = "            ";
            // 
            // cb_OrderStat
            // 
            this.cb_OrderStat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_OrderStat.FormattingEnabled = true;
            this.cb_OrderStat.Location = new System.Drawing.Point(452, 111);
            this.cb_OrderStat.Name = "cb_OrderStat";
            this.cb_OrderStat.Size = new System.Drawing.Size(111, 20);
            this.cb_OrderStat.TabIndex = 40;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.lbl_OrderStat);
            this.panel3.Controls.Add(this.btn_Reset_Order);
            this.panel3.Controls.Add(this.btn_Excel);
            this.panel3.Controls.Add(this.btn_AutoOrder);
            this.panel3.Controls.Add(this.btn_close);
            this.panel3.Controls.Add(this.checkBox3);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.btn_All_chk);
            this.panel3.Controls.Add(this.btn_All_unchk);
            this.panel3.Controls.Add(this.btn_Save);
            this.panel3.Controls.Add(this.btn_order_empty);
            this.panel3.Controls.Add(this.cb_ordersend);
            this.panel3.Controls.Add(this.btn_order_chk);
            this.panel3.Controls.Add(this.cb_OrderStat);
            this.panel3.Controls.Add(this.btn_order_list_change);
            this.panel3.Controls.Add(this.btn_lookup);
            this.panel3.Controls.Add(this.tb_orderText);
            this.panel3.Controls.Add(this.btn_send_chk);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.btn_order_send);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1261, 137);
            this.panel3.TabIndex = 52;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dataGridView1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 137);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1261, 585);
            this.panel4.TabIndex = 53;
            // 
            // Order_input
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 722);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Name = "Order_input";
            this.Text = "주문입력";
            this.Load += new System.EventHandler(this.Order_input_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_search_order;
        private System.Windows.Forms.ComboBox cb_user;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_state;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_order;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtp_orderBegin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtp_orderEnd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_PS;
        private System.Windows.Forms.Button btn_lookup;
        private System.Windows.Forms.Button btn_Reset_Order;
        private System.Windows.Forms.Button btn_AutoOrder;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Button btn_All_chk;
        private System.Windows.Forms.Button btn_All_unchk;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_order_send;
        private System.Windows.Forms.Button btn_send_chk;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_order_list_change;
        private System.Windows.Forms.Button btn_order_chk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_order_empty;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_search_book_comp;
        private System.Windows.Forms.TextBox tb_search_book_name;
        public System.Windows.Forms.TextBox tb_search_book_list;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.TextBox tb_orderText;
        private System.Windows.Forms.Button btn_Excel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.ComboBox cb_ordersend;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk;
        private System.Windows.Forms.DataGridViewTextBoxColumn order;
        private System.Windows.Forms.DataGridViewTextBoxColumn M;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn author;
        private System.Windows.Forms.DataGridViewTextBoxColumn book_comp;
        private System.Windows.Forms.DataGridViewTextBoxColumn order_count;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn etc;
        private System.Windows.Forms.DataGridViewTextBoxColumn list_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn order_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn send_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn idx;
        public System.Windows.Forms.DateTimePicker dtp_listEnd;
        public System.Windows.Forms.DateTimePicker dtp_listBegin;
        private System.Windows.Forms.Label lbl_OrderStat;
        private System.Windows.Forms.ComboBox cb_OrderStat;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
    }
}