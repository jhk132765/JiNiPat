﻿namespace WindowsFormsApp1.Delivery
{
    partial class Commodity_Morge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.tb_list1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_list2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_list_res = new System.Windows.Forms.TextBox();
            this.rbtn5_1 = new System.Windows.Forms.RadioButton();
            this.rbtn5_2 = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_OK
            // 
            this.btn_OK.Location = new System.Drawing.Point(163, 83);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(94, 34);
            this.btn_OK.TabIndex = 1;
            this.btn_OK.Text = "확   인";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(368, 83);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(94, 34);
            this.btn_Cancel.TabIndex = 1;
            this.btn_Cancel.Text = "취   소";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(209, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(2, 70);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(414, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(2, 70);
            this.panel3.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(19, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 1;
            this.label15.Text = "목록명";
            // 
            // tb_list1
            // 
            this.tb_list1.Enabled = false;
            this.tb_list1.Location = new System.Drawing.Point(21, 26);
            this.tb_list1.Name = "tb_list1";
            this.tb_list1.Size = new System.Drawing.Size(149, 21);
            this.tb_list1.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(225, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 1;
            this.label14.Text = "목록명";
            // 
            // tb_list2
            // 
            this.tb_list2.Enabled = false;
            this.tb_list2.Location = new System.Drawing.Point(227, 26);
            this.tb_list2.Name = "tb_list2";
            this.tb_list2.Size = new System.Drawing.Size(149, 21);
            this.tb_list2.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(432, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 12);
            this.label13.TabIndex = 1;
            this.label13.Text = "목록명 결과";
            // 
            // tb_list_res
            // 
            this.tb_list_res.Enabled = false;
            this.tb_list_res.Location = new System.Drawing.Point(433, 26);
            this.tb_list_res.Name = "tb_list_res";
            this.tb_list_res.Size = new System.Drawing.Size(149, 21);
            this.tb_list_res.TabIndex = 2;
            // 
            // rbtn5_1
            // 
            this.rbtn5_1.AutoSize = true;
            this.rbtn5_1.Location = new System.Drawing.Point(123, 9);
            this.rbtn5_1.Name = "rbtn5_1";
            this.rbtn5_1.Size = new System.Drawing.Size(47, 16);
            this.rbtn5_1.TabIndex = 3;
            this.rbtn5_1.TabStop = true;
            this.rbtn5_1.Text = "변경";
            this.rbtn5_1.UseVisualStyleBackColor = true;
            this.rbtn5_1.Click += new System.EventHandler(this.rbtn1_2_Click);
            // 
            // rbtn5_2
            // 
            this.rbtn5_2.AutoSize = true;
            this.rbtn5_2.Location = new System.Drawing.Point(327, 9);
            this.rbtn5_2.Name = "rbtn5_2";
            this.rbtn5_2.Size = new System.Drawing.Size(47, 16);
            this.rbtn5_2.TabIndex = 3;
            this.rbtn5_2.TabStop = true;
            this.rbtn5_2.Text = "변경";
            this.rbtn5_2.UseVisualStyleBackColor = true;
            this.rbtn5_2.Click += new System.EventHandler(this.rbtn1_2_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.rbtn5_2);
            this.panel7.Controls.Add(this.rbtn5_1);
            this.panel7.Controls.Add(this.tb_list_res);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.tb_list2);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.tb_list1);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Location = new System.Drawing.Point(8, 12);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(600, 59);
            this.panel7.TabIndex = 0;
            // 
            // Commodity_Morge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 127);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.panel7);
            this.Name = "Commodity_Morge";
            this.Text = "Commodity_Morge";
            this.Load += new System.EventHandler(this.Commodity_Morge_Load);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tb_list1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_list2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_list_res;
        private System.Windows.Forms.RadioButton rbtn5_1;
        private System.Windows.Forms.RadioButton rbtn5_2;
        private System.Windows.Forms.Panel panel7;
    }
}