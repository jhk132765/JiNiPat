﻿namespace WindowsFormsApp1.Delivery
{
    partial class Commodity_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Old_Date = new System.Windows.Forms.TextBox();
            this.Old_Dlv = new System.Windows.Forms.TextBox();
            this.Old_Clit = new System.Windows.Forms.TextBox();
            this.Old_User = new System.Windows.Forms.TextBox();
            this.Old_Name = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.New_Date = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.New_Name = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.New_Dlv = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.New_User = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.New_Clit = new System.Windows.Forms.TextBox();
            this.btn_OK = new System.Windows.Forms.Button();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Old_Date
            // 
            this.Old_Date.Enabled = false;
            this.Old_Date.Location = new System.Drawing.Point(69, 9);
            this.Old_Date.Name = "Old_Date";
            this.Old_Date.Size = new System.Drawing.Size(100, 21);
            this.Old_Date.TabIndex = 0;
            // 
            // Old_Dlv
            // 
            this.Old_Dlv.Enabled = false;
            this.Old_Dlv.Location = new System.Drawing.Point(69, 63);
            this.Old_Dlv.Name = "Old_Dlv";
            this.Old_Dlv.Size = new System.Drawing.Size(100, 21);
            this.Old_Dlv.TabIndex = 0;
            // 
            // Old_Clit
            // 
            this.Old_Clit.Enabled = false;
            this.Old_Clit.Location = new System.Drawing.Point(69, 90);
            this.Old_Clit.Name = "Old_Clit";
            this.Old_Clit.Size = new System.Drawing.Size(100, 21);
            this.Old_Clit.TabIndex = 0;
            // 
            // Old_User
            // 
            this.Old_User.Enabled = false;
            this.Old_User.Location = new System.Drawing.Point(69, 117);
            this.Old_User.Name = "Old_User";
            this.Old_User.Size = new System.Drawing.Size(100, 21);
            this.Old_User.TabIndex = 0;
            // 
            // Old_Name
            // 
            this.Old_Name.Enabled = false;
            this.Old_Name.Location = new System.Drawing.Point(69, 36);
            this.Old_Name.Name = "Old_Name";
            this.Old_Name.Size = new System.Drawing.Size(100, 21);
            this.Old_Name.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Old_Date);
            this.panel1.Controls.Add(this.Old_Name);
            this.panel1.Controls.Add(this.Old_Dlv);
            this.panel1.Controls.Add(this.Old_User);
            this.panel1.Controls.Add(this.Old_Clit);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(181, 150);
            this.panel1.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "담당자";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "거래처";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "납품목록";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "목록명";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "목록일자";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(206, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "====>";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.New_Date);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.New_Name);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.New_Dlv);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.New_User);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.New_Clit);
            this.panel2.Location = new System.Drawing.Point(267, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(181, 150);
            this.panel2.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 1;
            this.label11.Text = "담당자";
            // 
            // New_Date
            // 
            this.New_Date.CustomFormat = "yyyy-MM-dd";
            this.New_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.New_Date.Location = new System.Drawing.Point(69, 9);
            this.New_Date.Name = "New_Date";
            this.New_Date.Size = new System.Drawing.Size(100, 21);
            this.New_Date.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 1;
            this.label10.Text = "거래처";
            // 
            // New_Name
            // 
            this.New_Name.Location = new System.Drawing.Point(69, 36);
            this.New_Name.Name = "New_Name";
            this.New_Name.Size = new System.Drawing.Size(100, 21);
            this.New_Name.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 1;
            this.label9.Text = "납품목록";
            // 
            // New_Dlv
            // 
            this.New_Dlv.Location = new System.Drawing.Point(69, 63);
            this.New_Dlv.Name = "New_Dlv";
            this.New_Dlv.Size = new System.Drawing.Size(100, 21);
            this.New_Dlv.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "목록명";
            // 
            // New_User
            // 
            this.New_User.Location = new System.Drawing.Point(69, 117);
            this.New_User.Name = "New_User";
            this.New_User.Size = new System.Drawing.Size(100, 21);
            this.New_User.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 1;
            this.label7.Text = "목록일자";
            // 
            // New_Clit
            // 
            this.New_Clit.Location = new System.Drawing.Point(69, 90);
            this.New_Clit.Name = "New_Clit";
            this.New_Clit.Size = new System.Drawing.Size(100, 21);
            this.New_Clit.TabIndex = 0;
            this.New_Clit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.New_Clit_KeyDown);
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_OK.Location = new System.Drawing.Point(110, 168);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(88, 40);
            this.btn_OK.TabIndex = 2;
            this.btn_OK.Text = "확   인";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Btn_Cancel.Location = new System.Drawing.Point(262, 168);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.Size = new System.Drawing.Size(88, 40);
            this.Btn_Cancel.TabIndex = 2;
            this.Btn_Cancel.Text = "취   소";
            this.Btn_Cancel.UseVisualStyleBackColor = true;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // Commodity_Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 216);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label6);
            this.Name = "Commodity_Edit";
            this.Text = " ";
            this.Load += new System.EventHandler(this.Commodity_Edit_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Old_Date;
        private System.Windows.Forms.TextBox Old_Dlv;
        private System.Windows.Forms.TextBox Old_Clit;
        private System.Windows.Forms.TextBox Old_User;
        private System.Windows.Forms.TextBox Old_Name;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox New_Name;
        private System.Windows.Forms.TextBox New_Dlv;
        private System.Windows.Forms.TextBox New_User;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker New_Date;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Button Btn_Cancel;
        public System.Windows.Forms.TextBox New_Clit;
    }
}