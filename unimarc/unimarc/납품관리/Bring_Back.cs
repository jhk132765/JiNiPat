﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Delivery;

namespace WindowsFormsApp1.납품관리
{
    public partial class Bring_Back : Form
    {
        public string compidx;
        public int grididx;
        Main main;
        Helper_DB db = new Helper_DB();
        public Bring_Back(Main _main)
        {
            InitializeComponent();
            main = _main;
            compidx = main.com_idx;
        }
        private void Bring_Back_Load(object sender, EventArgs e)
        {
            db.DBcon();
            string Area = "`list_name`";
            string[] sear_col = { "comp_num", "state" };
            string[] sear_data = { compidx, "진행" };
            string cmd = db.More_DB_Search("Obj_List", sear_col, sear_data, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] arr_data = db_res.Split('|');
            
            for(int a = 0; a < arr_data.Length - 1; a++)
            {
                cb_list_name.Items.Add(arr_data[a]);
            }
        }
        private void btn_Lookup_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string Area = "`idx`, `num`, `book_name`, `author`, `book_comp`, " +
                          "`isbn`, `price`, `count`, `total`, `list_name`, `etc`";
            string[] sear_col = { "compidx", "list_name" };
            string[] sear_data = { compidx, cb_list_name.Text };
            string cmd = db.More_DB_Search("Obj_List_Book", sear_col, sear_data, Area);
            string db_res = db.DB_Send_CMD_Search(cmd);
            string[] arr_data = db_res.Split('|');

            input_Grid(arr_data);
        }
        #region Btn_Lookup_Click_Sub
        private void input_Grid(string[] data)
        {
            string[] grid = { "", "", "", "", "", 
                              "", "", "", "", "", "" };
            int c = grid.Length;
            for(int a = 0; a < data.Length; a++)
            {
                if (a % c == 0) { grid[0] = data[a]; }
                if (a % c == 1) { grid[1] = data[a]; }
                if (a % c == 2) { grid[2] = data[a]; }
                if (a % c == 3) { grid[3] = data[a]; }
                if (a % c == 4) { grid[4] = data[a]; }
                if (a % c == 5) { grid[5] = data[a]; }
                if (a % c == 6) { grid[6] = data[a]; }
                if (a % c == 7) { grid[7] = data[a]; }
                if (a % c == 8) { grid[8] = data[a]; }
                if (a % c == 9) { grid[9] = data[a]; }
                if (a % c == 10) { grid[10] = data[a];
                    dataGridView1.Rows.Add(grid);
                }
            }
        }
        #endregion
        private void btn_Return_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count <= 0) {
                return;
            }
            for (int a = 0; a < dataGridView1.Rows.Count; a++)
            {
                if (Convert.ToBoolean(dataGridView1.Rows[a].Cells["chkbox"].Value) == true)
                {
                    Return(a);
                }
            }
        }
        #region Btn_Return_Click_Sub
        private void Return(int a)
        {
            string[] sear_col = { "idx", "compidx" };
            string[] sear_data = { dataGridView1.Rows[a].Cells["idx"].Value.ToString(), compidx };
            string[] edit_col = { "import", "etc" };
            string[] edit_data = { "미입고", "반품처리 " + dataGridView1.Rows[a].Cells["etc"].Value.ToString() };
            string U_cmd = db.More_Update("Obj_List_Book", edit_col, edit_data, sear_col, sear_data);
            db.DB_Send_CMD_reVoid(U_cmd);
        }
        #endregion
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int col = e.ColumnIndex;
            if (dataGridView1.Columns["chkbox"].Index == col) return;

            grididx = e.RowIndex;
            Book_Lookup bl = new Book_Lookup(this);
            bl.TopMost = true;
            string book_name = ((DataGridView)sender).Rows[e.RowIndex].Cells["Book_name"].Value.ToString();
            string author = ((DataGridView)sender).Rows[e.RowIndex].Cells["author"].Value.ToString();
            string book_comp = ((DataGridView)sender).Rows[e.RowIndex].Cells["Book_comp"].Value.ToString();
            string list_name = ((DataGridView)sender).Rows[e.RowIndex].Cells["gu"].Value.ToString();
            bl.Lookup_Load(book_name, author, book_comp, list_name);
            bl.Show();
            return;
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int col = e.ColumnIndex;
            if (dataGridView1.Columns["chkbox"].Index == col) {
                dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
            }
        }
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
