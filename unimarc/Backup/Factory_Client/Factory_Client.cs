﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Collections;
using System.Diagnostics;

namespace Factory_Client
{
    public partial class Factory_Client : Form
    {
        //FTP아이디 패스워드
        private string Login_id = "kkura";
        private string Login_pw = "1234bb";

        //서버/클라이언트버전
        private string Sr_Vers = "";
        private string Cl_Vers = "";

        //서버아이피
        private string Server_Ip = "";

        //종료시 실행시킬 파일
        private string Start_Prg = "";

        //파일수
        private int Files_Count = 0;


        //업데이트 진행 파일수
        private int down_Count = 0;

        //업데이트여부
        bool tf = false;

        private string sLine = "";

        private string[] str = new string[2];
        int i = -1;

        DataSet ds = new DataSet("files");

        public Factory_Client()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {

                //파일목록 생성을 위한 데이터셋

                ds.Tables.Add("파일");

                ds.Tables["파일"].Columns.Add("file_name");
                ds.Tables["파일"].Columns.Add("chk");

                File_info();

                dataGridView1.DataSource = ds.Tables["파일"];

                //서버의 update.inf 파일에서 버전추출
                FtpWebRequest fwr = (FtpWebRequest)WebRequest.Create("ftp://" + Server_Ip + "/update.inf");
                fwr.Credentials = new NetworkCredential(Login_id, Login_pw);

                fwr.Method = WebRequestMethods.Ftp.DownloadFile;

                FtpWebResponse fr = (FtpWebResponse)fwr.GetResponse();
                StreamReader sr = new StreamReader(fr.GetResponseStream());

                while (sr.EndOfStream != true)
                {
                    sLine = sr.ReadLine();
                    i = sLine.IndexOf("count=", 0);

                    //서버버전 추출
                    if (sLine.IndexOf("count=", 0) != -1)
                    {
                        Sr_Vers = sLine.Replace("count=", "");
                        lbl_SerVer.Text = Sr_Vers;
                        break;
                    }
                }
                sr.Close();

                //버전이 같을경우에 버튼 변경
                if (Convert.ToDecimal(Sr_Vers) == Convert.ToDecimal(Cl_Vers))
                {
                    btn_ok.Text = "프로그램실행";

                    lbl_status.Text = "최신파일입니다!!";
                    lbl_status.ForeColor = Color.Blue;

                    lbl_filename.Text = "";
                    lbl_cnt.Text = "";
                    for (int j = 0; j < dataGridView1.Rows.Count; j++)
                    {
                        dataGridView1.Rows[j].Cells["update_status"].Value = "T";
                    }
                }
                else if (Convert.ToDecimal(Sr_Vers) > Convert.ToDecimal(Cl_Vers))
                {
                    btn_ok.Text = "업데이트";
                    tf = true;

                    lbl_cnt.Text = "(1/" + dataGridView1.Rows.Count.ToString() + ")";
                    lbl_status.Text = "업데이트가 존재합니다!!";
                    lbl_status.ForeColor = Color.DeepPink;
                }
            }
            catch (System.Exception ex)
            {

            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (tf)
            {

                //업데이트가 존재할때

                download(0);
            }
            else
            {
                //업데이트가 없을때
                string start_program = Application.StartupPath + "\\" + Start_Prg;
                Process prc = new Process();
                prc.StartInfo = new System.Diagnostics.ProcessStartInfo(start_program);
                prc.Start();

                this.Close();
            }
        }

        private void download(int cnt)
        {
            if (cnt < Convert.ToInt32(lbl_Files.Text))
            {
                WebClient clnt = new WebClient();

                clnt.Credentials = new NetworkCredential(Login_id, Login_pw);


                lbl_status.Text = "업데이트 진행중!!";
                progressBar1.Value = (progressBar1.Maximum / Convert.ToInt32(lbl_Files.Text)) * (down_Count + 1);

                lbl_filename.Text = dataGridView1.Rows[cnt].Cells["file_name"].Value.ToString();

                dataGridView1.Rows[cnt].Cells["update_status"].Value = "T";
                lbl_cnt.Text = "(" + (cnt + 1).ToString() + "/" + (dataGridView1.Rows.Count).ToString() + ")";

                File.Delete(Application.StartupPath + lbl_filename.Text);

                clnt.DownloadFileAsync(new Uri("ftp://" + Server_Ip + "/" + lbl_filename.Text), Application.StartupPath + "\\" + lbl_filename.Text);

                clnt.DownloadFileCompleted += new AsyncCompletedEventHandler(clnt_DownloadFileCompleted);
            }
            else
            {
                File_info();

                progressBar1.Value = progressBar1.Maximum;
                btn_ok.Text = "프로그램실행";

                lbl_status.Text = "최신파일입니다!!";
                lbl_status.ForeColor = Color.Blue;

                lbl_filename.Text = "";
                lbl_cnt.Text = "";
                tf = false;
            }
        }

        void  clnt_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {

            down_Count += 1;
            download(down_Count);

        }

        private void File_info()
        {
            //클라이언트 파일 정보
            //update.inf 파일에서 파일갯수,파일명,버전 추출
            StreamReader sr = new StreamReader(Application.StartupPath + "\\update.inf");
            int i = -1;
            while (sr.EndOfStream != true)
            {
                sLine = sr.ReadLine();
                i = sLine.IndexOf("count=", 0);

                //버전추출
                if (sLine.IndexOf("count=", 0) != -1)
                {
                    Cl_Vers = sLine.Replace("count=", "");
                    lbl_ClientVer.Text = Cl_Vers;
                }
                //서버아이피 추출
                else if (sLine.IndexOf("server_url=", 0) != -1)
                {
                    Server_Ip = sLine.Replace("server_url=", "");
                    lbl_Ip.Text = Server_Ip;
                }
                //종료시 실행파일
                else if (sLine.IndexOf("exe=", 0) != -1)
                    Start_Prg = sLine.Replace("exe=", "");
                //파일개수
                else if (sLine.IndexOf("Files=", 0) != -1)
                {
                    Files_Count = Convert.ToInt32(sLine.Replace("Files=", ""));
                    lbl_Files.Text = Files_Count.ToString();
                }
                //파일종류와 업데이트여부상태를 담아둠
                else if (sLine.IndexOf("\\", 0) != -1)
                {
                    str[0] = sLine.Replace("\\", "");
                    str[1] = "F";
                    ds.Tables["파일"].Rows.Add(str);
                }
                i = -1;
            }
            sr.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
