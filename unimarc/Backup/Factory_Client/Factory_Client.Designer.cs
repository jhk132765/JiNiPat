﻿namespace Factory_Client
{
    partial class Factory_Client
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ok = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.file_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.update_status = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_cnt = new System.Windows.Forms.Label();
            this.lbl_filename = new System.Windows.Forms.Label();
            this.lbl_ClientVer = new System.Windows.Forms.Label();
            this.lbl_SerVer = new System.Windows.Forms.Label();
            this.lbl_Files = new System.Windows.Forms.Label();
            this.lbl_Ip = new System.Windows.Forms.Label();
            this.lbl_status = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_ok
            // 
            this.btn_ok.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_ok.Location = new System.Drawing.Point(161, 335);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(146, 46);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "button1";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button2.Location = new System.Drawing.Point(426, 335);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 46);
            this.button2.TabIndex = 0;
            this.button2.Text = "취        소";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.file_name,
            this.update_status});
            this.dataGridView1.Location = new System.Drawing.Point(357, 33);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(399, 269);
            this.dataGridView1.TabIndex = 1;
            // 
            // file_name
            // 
            this.file_name.DataPropertyName = "file_name";
            this.file_name.HeaderText = "파일명";
            this.file_name.Name = "file_name";
            this.file_name.ReadOnly = true;
            this.file_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.file_name.Width = 200;
            // 
            // update_status
            // 
            this.update_status.DataPropertyName = "chk";
            this.update_status.FalseValue = "F";
            this.update_status.HeaderText = "업데이트완료";
            this.update_status.IndeterminateValue = "F";
            this.update_status.Name = "update_status";
            this.update_status.ReadOnly = true;
            this.update_status.TrueValue = "T";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_cnt);
            this.panel1.Controls.Add(this.lbl_filename);
            this.panel1.Controls.Add(this.lbl_ClientVer);
            this.panel1.Controls.Add(this.lbl_SerVer);
            this.panel1.Controls.Add(this.lbl_Files);
            this.panel1.Controls.Add(this.lbl_Ip);
            this.panel1.Controls.Add(this.lbl_status);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(14, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(325, 268);
            this.panel1.TabIndex = 2;
            // 
            // lbl_cnt
            // 
            this.lbl_cnt.AutoSize = true;
            this.lbl_cnt.Location = new System.Drawing.Point(265, 217);
            this.lbl_cnt.Name = "lbl_cnt";
            this.lbl_cnt.Size = new System.Drawing.Size(45, 12);
            this.lbl_cnt.TabIndex = 4;
            this.lbl_cnt.Text = "(10/10)";
            // 
            // lbl_filename
            // 
            this.lbl_filename.Location = new System.Drawing.Point(20, 217);
            this.lbl_filename.Name = "lbl_filename";
            this.lbl_filename.Size = new System.Drawing.Size(211, 12);
            this.lbl_filename.TabIndex = 3;
            // 
            // lbl_ClientVer
            // 
            this.lbl_ClientVer.AutoSize = true;
            this.lbl_ClientVer.Location = new System.Drawing.Point(142, 104);
            this.lbl_ClientVer.Name = "lbl_ClientVer";
            this.lbl_ClientVer.Size = new System.Drawing.Size(0, 12);
            this.lbl_ClientVer.TabIndex = 2;
            // 
            // lbl_SerVer
            // 
            this.lbl_SerVer.AutoSize = true;
            this.lbl_SerVer.Location = new System.Drawing.Point(142, 77);
            this.lbl_SerVer.Name = "lbl_SerVer";
            this.lbl_SerVer.Size = new System.Drawing.Size(0, 12);
            this.lbl_SerVer.TabIndex = 2;
            // 
            // lbl_Files
            // 
            this.lbl_Files.AutoSize = true;
            this.lbl_Files.Location = new System.Drawing.Point(142, 50);
            this.lbl_Files.Name = "lbl_Files";
            this.lbl_Files.Size = new System.Drawing.Size(0, 12);
            this.lbl_Files.TabIndex = 2;
            // 
            // lbl_Ip
            // 
            this.lbl_Ip.AutoSize = true;
            this.lbl_Ip.Location = new System.Drawing.Point(142, 23);
            this.lbl_Ip.Name = "lbl_Ip";
            this.lbl_Ip.Size = new System.Drawing.Size(0, 12);
            this.lbl_Ip.TabIndex = 2;
            // 
            // lbl_status
            // 
            this.lbl_status.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_status.ForeColor = System.Drawing.Color.DeepPink;
            this.lbl_status.Location = new System.Drawing.Point(-3, 166);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(317, 29);
            this.lbl_status.TabIndex = 0;
            this.lbl_status.Text = "최신파일입니다 !!";
            this.lbl_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(30, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "클라이언트버전 :";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(22, 232);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(288, 16);
            this.progressBar1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(69, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "서버버전 :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(20, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "업데이트 파일 수 :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(56, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "접속서버IP :";
            // 
            // Factory_Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 409);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btn_ok);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Factory_Client";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Factory 자동업데이트";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lbl_ClientVer;
        private System.Windows.Forms.Label lbl_SerVer;
        private System.Windows.Forms.Label lbl_Files;
        private System.Windows.Forms.Label lbl_Ip;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.DataGridViewTextBoxColumn file_name;
        private System.Windows.Forms.DataGridViewCheckBoxColumn update_status;
        private System.Windows.Forms.Label lbl_filename;
        private System.Windows.Forms.Label lbl_cnt;
    }
}

